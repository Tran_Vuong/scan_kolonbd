package gw.genumobile.com.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


import gw.genumobile.com.R;

/**
 * Created by anandbose on 09/06/15.
 */
public class NavigationMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int HEADER = 0;
    public static final int CHILD = 1;

    private List<Item> data;
    Context context;
    public NavigationMenuAdapter(List<Item> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = null;
         context = parent.getContext();
        float dp = context.getResources().getDisplayMetrics().density;
        int subItemPaddingLeft = (int) (18 * dp);
        int subItemPaddingTopAndBottom = (int) (5 * dp);
        switch (type) {
            case HEADER:
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.nav_item_parent, parent, false);
                ListHeaderViewHolder header = new ListHeaderViewHolder(view);
                return header;
            case CHILD:
                LayoutInflater inflater1 = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater1.inflate(R.layout.nav_item_child, parent, false);
                ListChildViewHolder chirl = new ListChildViewHolder(view);
                return chirl;

              /*  TextView itemTextView = new TextView(context);
                ImageView imgIcon = new ImageView(context);
                itemTextView.setPadding(subItemPaddingLeft, subItemPaddingTopAndBottom, 0, subItemPaddingTopAndBottom);
                itemTextView.setTextColor(0x88000000);
                itemTextView.setLayoutParams(
                        new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                return new RecyclerView.ViewHolder(itemTextView) {
                };*/
        }
        return null;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Item item = data.get(position);


        int idText=context.getResources().getIdentifier(item.getImg_name(),"string",context.getPackageName());
        if(idText==0)
            idText = context.getResources().getIdentifier("ic_menu_default","string",context.getPackageName());
        int idimg=context.getResources().getIdentifier(item.getImg_name(),"drawable",context.getPackageName());
        if(idimg==0)
            idimg = context.getResources().getIdentifier("ic_cat","drawable",context.getPackageName());

        switch (item.type) {
            case HEADER:
                final ListHeaderViewHolder itemController = (ListHeaderViewHolder) holder;
                itemController.refferalItem = item;
              //  itemController.imgiconParent.setImageResource(context.getResources().getIdentifier(item.getImg_name(),"drawable",context.getPackageName()));
                itemController.imgiconParent.setImageResource(R.drawable.ic_folder);
                itemController.header_title.setText(idText);
                if(item.name.length() > 0){
                    itemController.header_title.setText(item.name);
                }else {
                    itemController.header_title.setText(idText);
                }

                if (item.invisibleChildren == null) {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.ic_action_collapse);
                } else {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.ic_action_expand);
                }
                View.OnClickListener onClickListener = new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (item.invisibleChildren == null) {
                            item.invisibleChildren = new ArrayList<Item>();
                            int count = 0;
                            int pos = data.indexOf(itemController.refferalItem);
                            while (data.size() > pos + 1 && data.get(pos + 1).type == CHILD) {
                                item.invisibleChildren.add(data.remove(pos + 1));
                                count++;
                            }
                            notifyItemRangeRemoved(pos + 1, count);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.ic_action_expand);
                        } else {
                            int pos = data.indexOf(itemController.refferalItem);
                            int index = pos + 1;
                            for (Item i : item.invisibleChildren) {
                                data.add(index, i);
                                index++;
                            }
                            notifyItemRangeInserted(pos + 1, index - pos - 1);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.ic_action_collapse);
                            item.invisibleChildren = null;
                        }
                    }
                };
                itemController.btn_expand_toggle.setOnClickListener(onClickListener);
                itemController.header_title.setOnClickListener(onClickListener);
                itemController.parent_item.setOnClickListener(onClickListener);
                break;
            case CHILD:
                final ListChildViewHolder itemChildController = (ListChildViewHolder) holder;
                itemChildController.refferalItem = item;
                itemChildController.imgicon.setImageResource(R.drawable.ic_cat);
                if(item.name.length() > 0){
                    itemChildController.child_title.setText(item.name);
                }else {
                    itemChildController.child_title.setText(idText);
                }

               /* TextView itemTextView = (TextView) holder.itemView;
                itemTextView.setText(data.get(position).text);*/
                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).type;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private static class ListHeaderViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgiconParent;
        public TextView header_title;
        public ImageView btn_expand_toggle;
        public Item refferalItem;
        public RelativeLayout parent_item;

        public ListHeaderViewHolder(View itemView) {
            super(itemView);
            imgiconParent = (ImageView) itemView.findViewById(R.id.imgIconParent);
            header_title = (TextView) itemView.findViewById(R.id.parent_list_item_crime_title_text_view);
            btn_expand_toggle = (ImageView) itemView.findViewById(R.id.parent_list_item_expand_arrow);
            parent_item =(RelativeLayout) itemView.findViewById(R.id.parent_item);
        }
    }
    public static class Item {
        public int type;
        public String text;
        public int icdrawable;
        public List<Item> invisibleChildren;
        String classID,img_name, name;

        public Item(int type, String text, int icdrawable,String img_name,String classID ) {
            this.type = type;
            this.text = text;
            this.icdrawable=icdrawable;
            this.classID = classID;
            this.img_name = img_name;
            this.name = "";
        }

        public Item(int type, String text, int icdrawable,String img_name,String classID, String name ) {
            this.type = type;
            this.text = text;
            this.icdrawable=icdrawable;
            this.classID = classID;
            this.img_name = img_name;
            this.name = name;
        }
        public String getImg_name() {
            return img_name;
        }
        public String getClassID() {
            return classID;
        }

        public String getName() {
            return name;
        }

    }
    private static class ListChildViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgicon;
        public TextView child_title;
        public Item refferalItem;

        public ListChildViewHolder(View itemView) {
            super(itemView);
            imgicon = (ImageView) itemView.findViewById(R.id.imgIcon);
            child_title = (TextView) itemView.findViewById(R.id.itemText);
        }
    }


}
