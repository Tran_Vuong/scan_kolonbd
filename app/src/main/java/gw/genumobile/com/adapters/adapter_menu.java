package gw.genumobile.com.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import gw.genumobile.com.R;
import gw.genumobile.com.utils.ItemMenu;

/**
 * Created by admin on 16/01/2017.
 */

public class adapter_menu extends ArrayAdapter<ItemMenu> {

    LayoutInflater inflater;

    public adapter_menu(Context context, int textViewResourceId, List<ItemMenu> objects) {
        super(context, R.layout.item_menu, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public int getPosition(ItemMenu item) {
        return super.getPosition(item);
    }

    @Override
    public ItemMenu getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemMenu menu = getItem(position);
        if(convertView==null){
            convertView = inflater.inflate(R.layout.item_menu,parent,false);
        }

        ImageView img = (ImageView) convertView.findViewById(R.id.img_menu);
        img.setImageResource(getContext().getResources().getIdentifier(menu.getImg_name(),"drawable",getContext().getPackageName()));

        TextView txt_menu = (TextView) convertView.findViewById(R.id.txt_menu);
        txt_menu.setText(getContext().getResources().getIdentifier(menu.getImg_name(),"string",getContext().getPackageName()));
        // convertView.getResources().getIdentifier(menu , "drawable", )

        return convertView;
    }

}
