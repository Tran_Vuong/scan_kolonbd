package gw.genumobile.com.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import gw.genumobile.com.R;
import gw.genumobile.com.utils.ItemDefect;

/**
 * Created by admin on 11/02/2017.
 */

public class adapter_defect extends ArrayAdapter<ItemDefect> {

    LayoutInflater inflater;

    public adapter_defect(Context context, int resource, List<ItemDefect> objects) {
        super(context, R.layout.item_defect, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getPosition(ItemDefect item) {
        return super.getPosition(item);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public ItemDefect getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemDefect defect = getItem(position);
        if(convertView==null){
            convertView = inflater.inflate(R.layout.item_defect,parent,false);
        }

        TextView txt_defect = (TextView) convertView.findViewById(R.id.txt_defect_name);
        txt_defect.setText(defect.getDefect_name());
        txt_defect.setTextColor(Color.BLACK);
        txt_defect.setBackgroundColor(Color.LTGRAY);

        return convertView;
    }
}
