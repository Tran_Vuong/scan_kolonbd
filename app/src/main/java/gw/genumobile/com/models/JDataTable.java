package gw.genumobile.com.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * ***
 */
public class JDataTable {
    public List<String> columns = new ArrayList();
    public List<HashMap<String, Object>> records = new ArrayList();
    public int totalrows;
}