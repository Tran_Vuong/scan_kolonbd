package gw.genumobile.com.models;

/**
 * Created by admin on 2/22/2017.
 */

public enum gwfunction {
    //region Core
    Core_GET_WH_USER("LG_MPOS_M010_GET_WH_USER"),
    Core_GET_LOC("LG_MPOS_GET_LOC"),
    Core_LINE_USER("LG_MPOS_M010_GET_LINE_USER"),
    Core_UPD_INV_TR_DEL("LG_MPOS_UPD_INV_TR_DEL"),
    Core_GET_WH("LG_MPOS_M010_GET_WH_PK"),
    Core_GET_LINE ("LG_MPOS_M010_GET_LINE_PK"),
    Core_GET_LOC_PK   ("LG_MPOS_GET_LOC_PK"),
    Core_GET_BC ("LG_MPOS_M010_GET_BC"),
    Core_GET_ROLE  ("LG_MPOS_GET_ROLE"),
    //endregion

    //region Prod monitoring
    prodmonitoring_selGrgLine("LG_MPOS_GET_FACT_LINE"),
    prodmonitoring_q("LG_MPOS_PRO_MONITOR_Q"),
    //endregion
    //region SewingResultActivity
    SewingResultActivitysewingresult_gethour("LG_MPOS_GET_HOUR"),
    SewingResultActivity_getlineresult_h("LG_MPOS_SEL_LINE_RESULT_H"),
    SewingResultActivity_U("LG_MPOS_UPDATE_LINE_RESULT_H"),
    SewingResultActivity_I("LG_MPOS_INSERT_LINE_RESULT_H"),

    //endregion
    //region PopSewingDefectActivity
    PopSewingDefectActivity_S("LG_MPOS_UPD_DEFECT_RESULT"),


    //endregion

    //region ProductInc
    ProductInc_UPLOAD("LG_MPOS_UPLOAD_INC_V2"),
    ProductInc_MAKESLIP("LG_MPOS_PRO_INC_MAKESLIP"),
    ProductInc_MAKESLIP_MAP("LG_MPOS_PRO_INC_MAKESLIP_MAP"),
    //endregion
    //region ProductInc
    ProductOut_UPLOAD("LG_MPOS_UPL_PROD_OUT"),
    ProductOut_MAKESLIP("LG_MPOS_PRO_PROD_OUT_MSLIP"),
    ProductOut_GET_SLIP_TYPE_USER("LG_MPOS_GET_SLIP_TYPE_USER"),
    //region ProductResultInc
    ProductResultInc_UPLOAD("LG_MPOS_UPLOAD_RES_INC_V2"),
    ProductResultInc_MAKESLIP("LG_MPOS_PRO_RES_INC_MAKESLIP"),
    ProductResultInc_MAKESLIP_MAP("LG_MPOS_PRO_RES_INC_MAKESLIP_MAP"),
    //endregion
    //endregion
    StockTranfer_UPLOAD ("LG_MPOS_UPLOAD_STOCK_TRANSFER"),
    StockTranfer_MAKESLIP ("LG_MPOS_PRO_ST_TRANS_MSLIP"),
    StockTranfer_TRAN_TYPE("LG_MPOS_GET_TRANS_TYPE"),


    finall("trangtq");
    private final String value;

    gwfunction(String value) {
        this.value = value;
    }



    public String getValue() {
        return value;
    }
}
