package gw.genumobile.com.models;

public class Config {
    public static String PK = "";
    public static String USER_PK = "";
	public static String USER = "";
	public static String POINT = "";
	public static String URL = "";
	public static String dbName = "";
	public static String dbPwd ="";
	public static String dbUser = "";
	public static String deleteYN = "";
	public static String approvedYN = "";
    public static String formYN = "";
	public Config(){
		
	}
	public Config(String pk,String pk_id,String user,String point,String url,String dbName,String dbUser,String dbPwd )
	{
        this.PK=pk;
        this.USER_PK=pk_id;
		this.USER=user;
		this.POINT=point;
		this.URL=url;
		this.dbName=dbName;
		this.dbUser=dbUser;
		this.dbPwd=dbPwd;
	}
    public static String getPK(){return PK;}
    public static void setPK(String u_PK) {
        PK = u_PK;
    }
	public static String getUSER_PK(){return USER_PK;}
    public static void setUSER_PK(String uSER_PK) {
        USER_PK = uSER_PK;
    }
	public static String getUSER() {
		return USER;
	}
	public static void setUSER(String uSER) {
		USER = uSER;
	}
	public static String getPOINT() {
		return POINT;
	}
	public static void setPOINT(String pOINT) {
		POINT = pOINT;
	}
	public static String getURL() {
		return URL;
	}
	public static void setURL(String uRL) {
		URL = uRL;
	}
	public static String getDbName() {
		return dbName;
	}
	public static void setDbName(String dbName) {
		Config.dbName = dbName;
	}
	public static String getDbPwd() {
		return dbPwd;
	}
	public static void setDbPwd(String dbPwd) {
		Config.dbPwd = dbPwd;
	}
	public static String getDbUser() { return dbUser;}
	public static void setDbUser(String dbUser) {
		dbUser = dbUser;
	}
	public static String getDeleteYN() {return deleteYN;}
	public static void setDeleteYN(String delete) { deleteYN = delete;}
	public static String getApprovedYN() {return approvedYN;}
	public static void setApprovedYN(String approved) { approvedYN = approved;}
    public static String formYN() {return formYN;}
    public static void setFormYN(String frm) { formYN = frm;}
	
	/*---------------------------------------------------------------------*/
	/*---------------------READ FILE CONFIG--------------------------------*/
	/*---------------------------------------------------------------------*/
	public static boolean  ReadFileConfig()
	{
		/*
		  File dir1 = Environment.getExternalStorageDirectory();
	  	  //Get the text file  
	      File file = new File(dir1, "ConfigSetting.txt");  
	      // i have kept text.txt in the sd-card  		      
	      if(file.exists())   // check if file exist  
	      {  
	        	int i=0; 
	        	//Read text from file    
	            try 
	            {  
	                BufferedReader br = new BufferedReader(new FileReader(file));  
	                String line;  

	                while ((line = br.readLine()) != null) 
	                {  
	                       i++;
			               switch(i)
			               {	  
			               		  case 3:  
						        	  URL = splitContent(line);
						              break;
						          case 5:  
						        	   USER = splitContent(line);
						        	   break;
						          case 6:
						        	   POINT = splitContent(line);
						               break;
						          case 10:
					        	  	   dbName = splitContent(line);
					                   break;
						          case 11: 
					        	  	   dbUser = splitContent(line);
					                   break;
						          case 12: 
					        	       dbPwd = splitContent(line);
					                   break;
                                  case 20:
                                        formYN = splitContent(line);
                                        break;
							      case 24:
								   	   deleteYN = splitContent(line);
                                      break;
							      case 25:
								       approvedYN = splitContent(line);
								   break;
							   default:
						        	  break;	            	   
			               	}	                   
			         }
			         br.close();
	            }  
	            catch (IOException e) 
	            {  
	                //You'll need to add proper error handling here  
	            	return false;
	            } 
	       }
	      else 
	    	   return false;
	      //----------------------

	      return true;
	      */
		return true;
	}
	
	private  static String splitContent(String line)
	{
			String[] result = line.split("=");	
			return result[1];
	}
}
