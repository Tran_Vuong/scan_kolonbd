package gw.genumobile.com.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
/**
 *
 * Return Object JDataTable Form Json String By Format
 *    List<String> columns = new ArrayList();
 *    List<HashMap<String, Object>> records = new ArrayList();
 *    int totalrows;
 */


/***
 *
 */
public class JResultData {
    public String json;
    private String results;
    private int totals;

    private List<JDataTable> listODataTable = new ArrayList();
    private List<String> columns  =  new ArrayList();
    List<HashMap<String, Object>> error = new ArrayList();

    /***
     * Initiate Class
     * @param _json
     */
    public JResultData(String _json) {

        if(_json == null || _json.length() == 0){
            Log.e("Json is Null: ", json);
            return;
        }else{
            this.json = _json;
        }

        try {
            JSONObject jsonRootObject = new JSONObject(json);
            results = jsonRootObject.optString("results");
            totals = jsonRootObject.optInt("totals");
            JSONArray errorJS = jsonRootObject.optJSONArray("error");

            List<HashMap<String, Object>> lsterrors = new ArrayList();
            if (errorJS != null) {
                for (int r=0;r<errorJS.length();r++){
                    JSONObject jsonOpject = (JSONObject) errorJS.get(r);
                    lsterrors.add((HashMap<String, Object>) jsonToMap(jsonOpject));
                }
            }
            error = lsterrors;

            JSONArray _objcurdatasList = jsonRootObject.optJSONArray("objcurdatas");
            if(_objcurdatasList!=null)
            for (int o = 0; o < _objcurdatasList.length(); o++) {
                JSONObject _object = (JSONObject) _objcurdatasList.get(o);
                JSONArray _columnJS = _object.optJSONArray("columns");
                JSONArray _recordsJS = _object.optJSONArray("records");
                int _totalrowsJS = _object.optInt("totalrows");

                JDataTable objcurdata = new JDataTable();
                objcurdata.totalrows = _totalrowsJS;

                List<String> lstcolumn = new ArrayList();
                if (_columnJS != null) {
                    for (int c=0;c<_columnJS.length();c++){
                        lstcolumn.add(_columnJS.get(c).toString());
                    }
                }
                objcurdata.columns = lstcolumn;

                List<HashMap<String, Object>> lstRecords = new ArrayList();
                if (_recordsJS != null) {
                    for (int r=0;r<_recordsJS.length();r++){
                        JSONObject jsonOpject = (JSONObject) _recordsJS.get(r);
                        lstRecords.add((HashMap<String, Object>) jsonToMap(jsonOpject));
                    }
                }
                objcurdata.records = lstRecords;

                listODataTable.add(objcurdata);
            }
            Log.i("listObjcurdatas.size: ", String.valueOf(listODataTable.size()));

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JSONException", e.getCause().toString());
        }
    }

    /***
     * jsonToMap(JSONObject json)
     * @param json
     * @return
     * @throws JSONException
     */
    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    /***
     * toMap(JSONObject object)
     * @param object
     * @return
     * @throws JSONException
     */
    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    /***
     * toList(JSONArray array)
     * @param array
     * @return
     * @throws JSONException
     */
    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    /***
     * getColumns()
     * @return
     */
    public List<String> getColumns() {
        return columns;
    }

    /***
     * getErrors()
     * @return
     */
    public List<HashMap<String, Object>> getErrors() {
        return error;
    }

    /***
     * getResults()
     * @return
     */
    public List<JDataTable> getListODataTable() {
        return listODataTable;
    }

    /***
     * getResults()
     * @return
     */
    public String getResults() {
        return results;
    }

    /***
     * setResults(String results)
     * @param results
     */
    protected void setResults(String results) {
        this.results = results;
    }

    /***
     * int getTotals()
     * @return
     */
    public int getTotals() {
        return totals;
    }

    /***
     * setTotals(int totals)
     * @param totals
     */
    protected void setTotals(int totals) {
        this.totals = totals;
    }


}
