package gw.genumobile.com.models;

import android.content.pm.ActivityInfo;

/**
 * Created by trangtq on 2/22/2017.
 */
//1: SCREEN_ORIENTATION_PORTRAIT (DUNG); 0= SCREEN_ORIENTATION_LANDSCAPE(NGANG)
public enum gwform_info {
    gw(new FormInfo(0, 0,0, "Null_Form")),
    HomeFragment(new FormInfo(0, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "HomeFragment")),
    Incoming(new FormInfo(1, 1,0, "Incoming")),
    Outgoing(new FormInfo(2, 1,0, "Outgoing")),
    GoodsDelivery(new FormInfo(3, 1,0, "GoodsDelivery")),
    ProductInc(new FormInfo(4, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "ProductInc")),
    StockOutInv(new FormInfo(5, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "StockOutInv")),
    GoodsdeliveryV2(new FormInfo(6, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "GoodsDeliveryV2")),
    StockInInv(new FormInfo(7, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "StockInInv")),
    GoodsDeliNoReq(new FormInfo(8, 1,0, "GoodsDeliNoReq")),
    StockTransfer(new FormInfo(9, 1,0, "StockTransfer")),
    Mapping(new FormInfo(10, 1,0, "Mapping")),
    GoodsPartial(new FormInfo(11, 1,0, "GoodsPartial")),
    Preparation(new FormInfo(12, 1,0, "Preparation")),
    StockReturn(new FormInfo(13, 1,0, "StockReturn")),
    StockDiscard(new FormInfo(14, 1,0, "StockDiscard")),
    OthersInOut(new FormInfo(15, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "OthersInOut")),
    GoodsReturn(new FormInfo(16, 1,0, "GoodsReturn")),
    ReturnSupplier(new FormInfo(17, 1,0, "ReturnSupplier")),
    ProductOut(new FormInfo(18, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "ProductOut")),
    Evaluation(new FormInfo(19, 1,0, "Evaluation")),
    ExchangeProduct(new FormInfo(20, 1, 0,"ExchangeProduct")),
    UpdateLocation(new FormInfo(21, 1,0, "UpdateLocation")),
    StockInWithSupplierMapping(new FormInfo(22, 1,0, "Incoming")),
    StockTransferReq(new FormInfo(23, 1,0, "StockTransferReq")),
    StockInReq(new FormInfo(24, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "StockInReq")),
    StockOutReq(new FormInfo(25, 1,0, "StockOutReq")),
    SewingLineListActivity(new FormInfo(26, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE,0, "SewingLineListActivity")),
    GoodsDeliveryV4(new FormInfo(28, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "GoodsDeliveryV4")),
    StockTransferLocReq(new FormInfo(34, 1,0, "StockTransferLocReq")),
    MappingV2(new FormInfo(37, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "MappingV2"))   ,
    ProductResultInc(new FormInfo(38, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "ProductResultInc")),
    MappingV3(new FormInfo(40, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "MappingV3"))   ,
    DevideBC(new FormInfo(41, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "DevideBC"))  ,
    DevideBC_V2(new FormInfo(42, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "DevideBC_V2"))   ,
   // MappingReq(new FormInfo(43, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "MappingReq"))   ,
   StockOutReq_V2(new FormInfo(44, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "StockOutReq_V2")) ,
    CheckingReed(new FormInfo(59, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,0, "CheckingReed")) ;
    private final FormInfo value;

    gwform_info(FormInfo value) {
        this.value = value;
    }

    public FormInfo getValue() {
        return value;
    }

    //////
    public int Id() {
    return this.value.id;
    }
    public int Ori() {
        return this.value.ori;
    }
    public String Tag() {
        return this.value.tag;
    }
    public int Title() {
        return this.value.title;
    }
    public  void Title(int t)
    {
        this.value.title = t;
    }
}
