package gw.genumobile.com.models;

/**
 * Created by Administrator on 11-11-2015.
 */
public class FormInfo {
    public int      id;
    public int   ori; //1: SCREEN_ORIENTATION_PORTRAIT (DUNG); 0= SCREEN_ORIENTATION_LANDSCAPE(NGANG)
    public int title;
    public String   tag;

    public FormInfo(int id,int ori,int title,String tag ){
        this.id       = id;
        this.ori      = ori;
        this.tag       = tag;
        this.title       = title;
    }
}
