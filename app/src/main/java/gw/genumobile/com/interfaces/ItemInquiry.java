package gw.genumobile.com.interfaces;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;


public class ItemInquiry extends Activity {
    private SharedPreferences appPrefs;
    int fixedRowHeight = 70; //70|30
    int fixedHeaderHeight = 80; //80|40
    int[] colors = new int[]{0x30FF0000, 0x300000FF, 0xCCFFFF, 0x99CC33, 0x00FF99};
    Queue queue = new LinkedList();
    SQLiteDatabase db = null;
    Cursor cursor;
    Calendar c;
    SimpleDateFormat df;

    TextView _txtQty, _txtTotal,_txtHeader,recyclableTextView;
    EditText _edtKeySeach;
    Button btnSearch;
    String type="" ,sql = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_inquiry);

        //lock screen
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        appPrefs = getSharedPreferences("myConfig", MODE_PRIVATE);
        fixedRowHeight = Integer.parseInt(appPrefs.getString("rowHeight", "70"));
        fixedHeaderHeight = Integer.parseInt(appPrefs.getString("headerHeight", "80"));

        type = getIntent().getStringExtra("type");

        OnShowHeader();
    }

    public void onClickSelectItem(View view) throws InterruptedException,ExecutionException {
        OnSearch();
    }

    public void OnSearch()
    {
        _edtKeySeach=(EditText) findViewById(R.id.edit_ItemCode);
        String keyword=_edtKeySeach.getText().toString();
        int date_previous=Integer.parseInt(CDate.getDatePrevious(10));
        try{
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30,40};


            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_content);
            scrollablePart.removeAllViews();//remove all view child

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,ITEM_NAME, TR_QTY, TR_LOT_NO, SLIP_NO, LINE_NAME, TR_WH_IN_NAME, REMARKS " +
                    " FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '"+type+"' AND SCAN_DATE >" + date_previous + " AND SENT_YN = 'Y' AND STATUS='000' AND (ITEM_CODE like '%"+keyword+"%' or ITEM_BC like '%"+keyword+"%')  ORDER BY pk desc ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            float _qty=0;
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableRowWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight));

                    _qty=_qty+ Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            _txtTotal = (TextView) findViewById(R.id.txtTotalLabel);
            _txtTotal.setText("Total: " + count + " ");
            _txtTotal.setTextColor(Color.BLUE);
            _txtTotal.setTextSize((float) 18.0);
            //---------------------------------------------------------------------

            _txtQty = (TextView) findViewById(R.id.txtQty);
            _txtQty.setText("Qty: " + _qty + " ");
            _txtQty.setTextColor(Color.MAGENTA);
            _txtQty.setTextSize((float) 18.0);

        }catch (Exception ex) {
            //Toast.makeText(this,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            //Log.e("OnShowScanAccept Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
    //----------------------------------------------------------
    ///Load Header grid
    //----------------------------------------------------------
    public void OnShowHeader()
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30,40};
        //int fixedRowHeight = 40;
        //int fixedHeaderHeight = 30;

        TableRow row = new TableRow(this);
        TableLayout scrollablePart;


        // Accept Scan
        scrollablePart = (TableLayout) findViewById(R.id.scrollable_header);
        row = new TableRow(this);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(colors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText("STT", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item Name", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("WH Name", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Line Name", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(this);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.BLACK);
        recyclableTextView.setTextSize(16);
        recyclableTextView.setBackgroundColor(-1);
        recyclableTextView.setPadding(0, 5, 0, 5);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }

    public TextView makeTableRowHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(this);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(-256);
        recyclableTextView.setTextSize(18);
        recyclableTextView.setGravity(Gravity.CENTER);
        recyclableTextView.setBackgroundColor(colors[1]);
        recyclableTextView.setPadding(0, 2, 0, 2);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item_inquiry, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
