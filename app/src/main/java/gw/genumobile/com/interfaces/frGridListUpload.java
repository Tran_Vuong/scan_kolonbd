package gw.genumobile.com.interfaces;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.models.Config;
import gw.genumobile.com.services.ServerAsyncTask;
import gw.genumobile.com.services.UploadAsyncTask;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.views.gwFragmentDialog;


public class frGridListUpload extends gwFragmentDialog implements View.OnClickListener {
    //private int[] colors = new int[] { 0x30FF0000, 0x300000FF,0xCCFFFF,0x99CC33,0x00FF99 };
    SQLiteDatabase db=null,db2=null;
    Cursor cursor,cursor2;
    private Handler customHandler = new Handler();
    String  data[] = new String[0];

    Queue queueMid = new LinkedList();

    Calendar c;
    SimpleDateFormat df;

    TextView txt_error, txtTotalGridMid,txtHeader,recyclableTextView;
    Button btnClearAll,btnClear;
    String form_id,sql = "", scan_date = "";
    View vContent;
    String USER;
  Activity gwAc;
    public static frGridListUpload newInstance(int title, String _type) {
        frGridListUpload frag = new frGridListUpload();
        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putString("type", _type);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        form_id = getArguments().getString("type");
        gwAc=getActivity();
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vContent = inflater.inflate(R.layout.activity_grid_list_view_upload, container, false);
        View tv = vContent.findViewById(R.id.text);
        Toolbar toolbar = (Toolbar) vContent.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                    Intent i = new Intent()
                            .putExtra("month", "trang");
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                    dismiss();
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);


        //Toast.makeText(this,type, Toast.LENGTH_SHORT).show();
        txtHeader=(TextView) vContent.findViewById(R.id.txtHeader);

        btnClearAll=(Button) vContent.findViewById(R.id.btnClearAll);
        btnClearAll.setOnClickListener(this);
        btnClear=(Button) vContent.findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);
        txt_error=(TextView) vContent.findViewById(R.id.txt_error);

        if(form_id.equals("40"))
        {
            toolbar.setTitle("LIST UPLOAD MAPPING ITEM");
            OnShowGridHeaderMapingV3();
            OnShowScanMapping();
        }

        scan_date = CDate.getDateyyyyMMdd();


        return vContent;
    }
    public void OnShowScanMapping()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) vContent.findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();//remove all view child

            c = Calendar.getInstance();
            df = new SimpleDateFormat("yyyyMMdd");
            scan_date = df.format(c.getTime());

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE, TR_LOT_NO,TR_QTY,GRADE,WEIGHT,SCAN_DATE,SENT_YN, SLIP_NO, PK " +
                    " FROM INV_TR  "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '"+form_id+"' AND PARENT_PK='0' AND BC_TYPE='C'" +
                    " ORDER BY pk desc ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq= String.valueOf(count-i);
                    row.addView(makeTableRowWithText(seq , scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("GRADE")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("WEIGHT")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SCAN_DATE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SENT_YN"))     , scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO"))     , scrollableColumnWidths[5], fixedRowHeight));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }

                txtTotalGridMid=(TextView) vContent.findViewById(R.id.txtTotalLabel);
                txtTotalGridMid.setText("Total: " + count);
                txtTotalGridMid.setTextColor(Color.BLUE);
                txtTotalGridMid.setTextSize((float) 18.0);

            }
        } catch (Exception ex) {
            Toast.makeText(gwMActivity,"OnShowScanMapping: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
            cursor.close();
        }
    }
    //////////////////////////////////////////////////////////
    //-------upload-----------------
    public void  OnUploadMapping(View view) {
        //Read file config
        boolean read= Config.ReadFileConfig();
        USER=Config.USER;
        //
        db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        sql = "select ITEM_BC, ITEM_CODE, TR_LOT_NO,TR_QTY,GRADE,WEIGHT,SCAN_DATE,CHARGER, SLIP_NO, PK " +
                " FROM INV_TR  "+
                " WHERE DEL_IF = 0 AND TR_TYPE = '"+form_id+"' AND PARENT_PK='0' AND BC_TYPE='C' AND SENT_YN='N' AND SLIP_NO='-' " +
                " ORDER BY pk desc ";

        cursor = db.rawQuery(sql,null);
        System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
        if (cursor.moveToFirst()) {
            // INSERT DATA INTO DB
            // Show dialog Yes No
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwActivity);
            // Setting Dialog Title
            alertDialog.setTitle("Confirm Upload...");
            // Setting Dialog Message
            alertDialog.setMessage("Are you sure you want to upload data to server?");
            // Setting Icon to Dialog
            alertDialog.setIcon(R.drawable.cfm_up);
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {
                    // Write your code here to invoke YES event
                    //set value message
                    txt_error.setTextColor(Color.RED);
                    txt_error.setText("");

                    data=new String [cursor.getCount()];
                    int j=0;
                    do {
                        // labels.add(cursor.getString(1));
                        String para ="";
                        for(int i=0; i < cursor.getColumnCount();i++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(i)!= null)
                                    para += cursor.getString(i);
                                else
                                    para += "0";
                            }
                            else
                            {
                                if(cursor.getString(i)!= null)
                                    para += "|!" + cursor.getString(i);
                                else
                                    para += "|!";
                            }

                        }
                        para += "|!"+USER;

                        //System.out.print("\n\n ******para: "+para);
                        data[j++]=para;

                    } while (cursor.moveToNext());
                    //////////////////////////
                    cursor.close();
                    db.close();
                    doStart();

                    //clear data
                    //clearAllData();
                    //OnShowGW();

                }
            });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event
                    //Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                    //////////////////////////
                    cursor.close();
                    db.close();
                    dialog.cancel();
                }
            });
            // Showing Alert Message
            alertDialog.show();


        }else{
            cursor.close();
            db.close();
            txt_error.setTextColor(Color.RED);
            txt_error.setTextSize((float) 16.0);
            txt_error.setText("Data is empty!");
        }

    }

    private void doStart() {
        System.out.print("\n\n**********doStart********\n\n\n");

        UploadAsyncTask task = new UploadAsyncTask(gwMActivity, this);
        task.execute(data);
    }
    //----------------------------------------------------------
    ///Load Header grid
    //----------------------------------------------------------
    public void OnShowGridHeader()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Status Scan
        scrollablePart = (TableLayout) vContent.findViewById(R.id.scrollable_part1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Lot No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("QTY", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Grade", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Weight", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Scan Date", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Charger", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Slip No", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

    }
    public void OnShowGridHeaderMapingV3()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Status Scan
        scrollablePart = (TableLayout) vContent.findViewById(R.id.scrollable_part1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Lot No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("QTY", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Grade", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Weight", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Scan Date", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Sent YN", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Slip No", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

    }


    public TextView makeTableRowHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(gwMActivity);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.YELLOW);
        recyclableTextView.setTextSize(18);
        recyclableTextView.setGravity(Gravity.CENTER);
        recyclableTextView.setBackgroundColor(bsColors[1]);
        recyclableTextView.setPadding(0, 2, 0, 2);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }
    public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(gwMActivity);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.BLACK);
        recyclableTextView.setTextSize(16);
        recyclableTextView.setBackgroundColor(-1);
        recyclableTextView.setPadding(0, 2, 0, 2);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }


    ////////////////////////////////////////////////
    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event

                if (_type.equals("ClearALL")) {
                    ClearALL();
                }
                if (_type.equals("ClearSent")) {
                    ClearSent();

                    //ProcessMakeSlip();

                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_close) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onDelAll(View view) {

        String title = "Confirm Delete...";
        String mess = "Are you sure you want to delete all";
        alertDialogYN(title, mess, "ClearALL");
    }
    public void onDelAll1(View view) {

        String title = "Confirm Delete...";
        String mess = "Are you sure you want to delete data sent";
        alertDialogYN(title, mess, "ClearSent");
    }
    public void ClearALL() {
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String para = "";
            boolean flag = true;
            db.execSQL("DELETE FROM INV_TR where TR_TYPE ='" + form_id + "'");


        } catch (Exception ex) {
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
            OnShowScanMapping();
        }
    }
    public void ClearSent() {
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String para = "";
            boolean flag = true;
            db.execSQL("DELETE FROM INV_TR where TR_TYPE ='" + form_id + "' AND SENT_YN ='Y'");


        } catch (Exception ex) {
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
            OnShowScanMapping();
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClearAll:
                onDelAll(v);
                break;
            case R.id.btnClear:
                onDelAll1(v);
                break;
            default:
                break;
        }

        }
}
