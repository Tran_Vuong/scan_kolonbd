package gw.genumobile.com.interfaces;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import gw.genumobile.com.R;

public class GridListView extends Activity {
	private int[] colors = new int[] { 0x30FF0000, 0x300000FF,0xCCFFFF,0x99CC33,0x00FF99 };
	SQLiteDatabase db = null;
	Cursor cursor,cursor1;
	TextView txt_error, txtTotalLabel,txtHeader;
	Button btnBack;
	String type;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grid_list_view);


		//Intent intentObject = getIntent();
	    //String userName = intentObject.getStringExtra("UserName");
		type = getIntent().getStringExtra("type");
		//Toast.makeText(getApplicationContext(),value,Toast.LENGTH_SHORT).show();
		txtHeader=(TextView) findViewById(R.id.txtHeader);

		/*
		if(type.equals("1")){
			txtHeader.setText("LIST ALL ITEM INCOMING");
		}
		if(type.equals("3")){
				txtHeader.setText("LIST ALL ITEM GOODS DELIVERY");
			}
		if (type.equals("2")){
				txtHeader.setText("LIST ALL ITEM OUTGOING");
			}
		*/
		if (type.equals("4")){
			txtHeader.setText("LIST ALL ITEM PROD IN");
		}

		if (type.equals("10")){
			txtHeader.setText("LIST ALL ITEM MAPPING");
		}

		OnShowGW_Header();
		OnShowGW();
		
		btnBack = (Button) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// do something when the button is clicked
				finish();
				System.exit(0);
				
			}
		});
		///////////////////////////
	}


	
	// end get data return array--use array set to grid view
		@SuppressWarnings("deprecation")
		public void OnShowGW()// show data gridview
		{
			TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	        int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20,20};
	        int[] scrollableColumnWidths = new int[]{20, 20, 20, 30, 30};
	        int fixedRowHeight = 40;
	        int fixedHeaderHeight = 60;

	        TableRow row = new TableRow(this);
	        TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
	        scrollablePart.removeAllViews();//remove all view child
	        ///
	        db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);

			cursor1 = db.rawQuery("SELECT PK FROM INV_TR where TR_TYPE='" + type + "' order by PK desc ", null);// TLG_LABEL
			cursor = db.rawQuery("SELECT ITEM_BC,BC_TYPE,PARENT_PK, CHILD_PK, SENT_YN, STATUS,TR_TYPE, TR_QTY, TR_LOT_NO,WH_NAME,LINE_NAME,null WH_LOC,ITEM_CODE,ITEM_NAME FROM INV_TR where TR_TYPE='"+type+"' order by PK desc ",null);// TLG_LABEL

			int countList = cursor1.getCount();
	        int count = cursor.getCount();
			int l_total = cursor.getCount() - 1;
			txtTotalLabel = (TextView) findViewById(R.id.txtTotalLabel);
			txtTotalLabel.setText("Total:" + countList + " Label(s)");
			txtTotalLabel.setTextColor(Color.BLUE);
			txtTotalLabel.setTextSize((float) 18.0);
			// count la so dong,con so 5 la column,-->so phan tu cua mang =count*5
			String[] vals = new String[count * 7];
			if (cursor != null && cursor.moveToFirst()) {
				for (int i = 0; i < count; i++) {
		    		row = new TableRow(this);
			        row.setLayoutParams(wrapWrapTableRowParams);
			        row.setGravity(Gravity.CENTER);
			        row.setBackgroundColor(Color.LTGRAY);
					row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight));

					row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("BC_TYPE")), scrollableColumnWidths[3], fixedRowHeight));
					row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("PARENT_PK")), scrollableColumnWidths[2], fixedRowHeight));
					row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("CHILD_PK")), scrollableColumnWidths[2], fixedRowHeight));
					row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SENT_YN")), scrollableColumnWidths[2], fixedRowHeight));
					row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight));
					row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_TYPE")), scrollableColumnWidths[2], fixedRowHeight));

					row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[1], fixedRowHeight));
			        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight));
			        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("WH_NAME")), scrollableColumnWidths[3], fixedRowHeight));
			        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[4], fixedRowHeight));
			        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[4], fixedRowHeight));
			        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight));
			        
			        row.setOnClickListener(new OnClickListener() {

			            @Override
			            public void onClick(View v) {
			                TableRow tr1=(TableRow)v;
			                TextView tv1= (TextView)tr1.getChildAt(0);
			                int xx=tr1.getChildCount();
			                for (int i = 0; i < xx; i++) {
			                	TextView tv11= (TextView)tr1.getChildAt(i);
			                	tv11.setTextColor(-256);
			                }
			                //Toast.makeText(getApplicationContext(),tv1.getText().toString(),Toast.LENGTH_SHORT).show();
			                //bc2 = (EditText) findViewById(R.id.edit_Bc2);
			                //bc2.setText(tv1.getText().toString());
			            }
			        });
			        
			        scrollablePart.addView(row);
			        
					cursor.moveToNext();
				}
			}
			cursor1.close();
			cursor.close();
			db.close();        
		}
	/////////////////////////////////////
	////util method
	private TextView recyclableTextView;
	
	public void OnShowGW_Header()// show data gridview
	{
	TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20,20};
	int[] scrollableColumnWidths = new int[]{20, 20, 20, 30, 30};
	int fixedRowHeight = 40;
	int fixedHeaderHeight = 40;
	
	TableRow row = new TableRow(this);
	TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part1);
	
	
	row = new TableRow(this);
		row.setLayoutParams(wrapWrapTableRowParams);
	row.setGravity(Gravity.CENTER);
	row.setBackgroundColor(colors[1]);
	row.setVerticalGravity(50);
	
	row.addView(makeTableRowHeaderWithText("ITEM_BC", scrollableColumnWidths[3], fixedHeaderHeight));

	row.addView(makeTableRowHeaderWithText("BC_TYPE", scrollableColumnWidths[3], fixedHeaderHeight));
	row.addView(makeTableRowHeaderWithText("PARENT_PK", scrollableColumnWidths[2], fixedHeaderHeight));
	row.addView(makeTableRowHeaderWithText("CHILD_PK", scrollableColumnWidths[2], fixedHeaderHeight));
	row.addView(makeTableRowHeaderWithText("SENT_YN", scrollableColumnWidths[2], fixedHeaderHeight));
	row.addView(makeTableRowHeaderWithText("STATUS", scrollableColumnWidths[2], fixedHeaderHeight));
	row.addView(makeTableRowHeaderWithText("TR_TYPE", scrollableColumnWidths[2], fixedHeaderHeight));

	row.addView(makeTableRowHeaderWithText("TR_QTY", scrollableColumnWidths[1], fixedHeaderHeight));
	row.addView(makeTableRowHeaderWithText("TR_LOT_NO", scrollableColumnWidths[2], fixedHeaderHeight));
	row.addView(makeTableRowHeaderWithText("WH_NAME", scrollableColumnWidths[3], fixedHeaderHeight));
	row.addView(makeTableRowHeaderWithText("LINE_NAME", scrollableColumnWidths[4], fixedHeaderHeight));
	row.addView(makeTableRowHeaderWithText("ITEM_CODE", scrollableColumnWidths[4], fixedHeaderHeight));
	row.addView(makeTableRowHeaderWithText("ITEM_NAME", scrollableColumnWidths[4], fixedHeaderHeight));
	scrollablePart.addView(row);
	}
	
	public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
	int screenWidth = getResources().getDisplayMetrics().widthPixels;
	TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
	params.setMargins(1, 1, 1, 1);
	recyclableTextView = new TextView(this);
	recyclableTextView.setText(text);
	recyclableTextView.setTextColor(Color.BLACK);
	recyclableTextView.setTextSize(20);
	recyclableTextView.setBackgroundColor(-1);
	recyclableTextView.setPadding(0, 5, 0, 5);
	recyclableTextView.setLayoutParams(params);
	recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
	recyclableTextView.setHeight(fixedHeightInPixels);
	return recyclableTextView;
	}
	public TextView makeTableRowHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
	int screenWidth = getResources().getDisplayMetrics().widthPixels;
	TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
	params.setMargins(1, 1, 1, 1);
	recyclableTextView =new  TextView(this);
	recyclableTextView.setText(text);
	recyclableTextView.setTextColor(-256);
	recyclableTextView.setTextSize(20);
	recyclableTextView.setBackgroundColor(colors[1]);
	recyclableTextView.setPadding(0, 5, 0, 5);
	recyclableTextView.setLayoutParams(params);
	recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
	recyclableTextView.setHeight(fixedHeightInPixels);
	return recyclableTextView;
	}

/////////////////////////////////////////////////////////////////
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.grid_list_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
