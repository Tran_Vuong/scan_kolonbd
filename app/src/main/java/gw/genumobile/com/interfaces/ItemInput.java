package gw.genumobile.com.interfaces;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.services.ConnectThread;

public class ItemInput extends Activity {
    private SharedPreferences appPrefs;
    TextView recyclableTextView;
    int[] colors = new int[]{0x30FF0000, 0x300000FF, 0xCCFFFF, 0x99CC33, 0x00FF99};

    public Spinner myLstGroup;
    String grp_pk = "",grp_name="",pk_item="";
    String pk_user="aaa";

    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    EditText lotno,qty;
    TextView group_name,item_name;
    int xxx=1;
    int fixedRowHeight = 70; //70|30
    int fixedHeaderHeight = 80; //80|40
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_input);
        appPrefs = getSharedPreferences("myConfig", MODE_PRIVATE);

        fixedHeaderHeight = Integer.parseInt(appPrefs.getString("rowHeight", "70"));
        fixedHeaderHeight = Integer.parseInt(appPrefs.getString("headerHeight", "80"));
        myLstGroup     = (Spinner) findViewById(R.id.lstWH);
        LoadGroup();
        OnShowGridHeader();
        OnShowScanAccept();

        // onchange spinner group item
        myLstGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                grp_name = myLstGroup.getItemAtPosition(i).toString();

                List<String> lables = GetDataOnServer("GROUP_PK", grp_name);
                if (lables.size() > 0) {
                    grp_pk = lables.get(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    public  void alertToast(String alert){
        Toast toast=Toast.makeText(getApplicationContext(), alert , Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    private void LoadGroup() {
        List<String> lables = GetDataOnServer("GROUP", pk_user);

        if (lables.size() <= 0) {
            myLstGroup.setAdapter(null);
            grp_pk="0";
        } else {
            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, lables);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            myLstGroup.setAdapter(dataAdapter);

            grp_name = myLstGroup.getItemAtPosition(0).toString();
            lables = GetDataOnServer("GROUP_PK", grp_name);
            if (lables.size() > 0)
                grp_pk = lables.get(0);
        }
    }

    public List<String> GetDataOnServer(String type, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";

            if (type.endsWith("GROUP"))//LINE
            {
                l_para = "1,LG_MPOS_GET_GROUP_ITEM," + para;
            }

            if (type.endsWith("GROUP_PK"))//LINE PK
            {
                l_para = "1,LG_MPOS_GET_GROUP_ITEM_PK," + para;
            }

            ConnectThread networkThread = new ConnectThread(this);
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                if (type.endsWith("GROUP_PK")){
                    labels.add(result[i][0].toString());
                }
                else
                    labels.add(result[i][1].toString());
            }
        } catch (Exception ex) {
            alertToast(type + ": " + ex.getMessage());
        }
        return labels;
    }
    ///////////////////////////////////////////////
    public void onClickSave(View view) throws InterruptedException,ExecutionException {
        group_name=(TextView) findViewById(R.id.txtGroupName);
        item_name=(TextView) findViewById(R.id.txtItemName);
        lotno=(EditText) findViewById(R.id.txtLotNo);
        qty=(EditText) findViewById(R.id.txtQty);

        String st_grp=group_name.getText().toString();
        String st_item=item_name.getText().toString();
        String st_lotno=lotno.getText().toString();
        float st_qty=Float.parseFloat(qty.getText().toString());

        try {
            db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            if(st_grp.length() > 1 && st_item.length()>1 && st_lotno.length()>1 && st_qty > 0 ) {
                db.execSQL("INSERT INTO INV_TR_GRP(ITEM_GROUP_NAME,ITEM_NAME,TR_LOT_NO,TR_QTY) "
                        + "VALUES('"
                        + st_grp + "','"
                        + st_item + "','"
                        + st_lotno + "',"
                        + st_qty + ");");

                group_name.setText("");
                item_name.setText("");
                lotno.setText("");
                qty.setText("");
                alertToast("Save Finish");
            }else{
                alertToast("Value can not  Empty!!");
            }

        }catch (Exception ex)
        {
            alertToast("Save Error :: " + ex.getMessage());
            Log.e("Save", ex.getMessage());
        }
        finally {

            db.close();
            OnShowScanAccept();
        }
    }
    public void OnShowScanAccept(){
        try
        {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
            int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            String sql = "select  PK,ITEM_GROUP_NAME,ITEM_NAME,TR_LOT_NO,TR_QTY" +
                    " FROM INV_TR_GRP  WHERE DEL_IF = 0 ORDER BY PK desc LIMIT 20 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableRowWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_GROUP_NAME")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText("", 0, fixedRowHeight));
                    row.addView(makeTableRowWithText("", 0, fixedRowHeight));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }

            }
        } catch (Exception ex) {
            alertToast("Error Grid Accept: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
    ///////////////////////////////////////////////
    public void onClickSearch(View view) throws InterruptedException,ExecutionException {
        EditText keyword=(EditText) findViewById(R.id.ed_Item);
        String l_para = "1,LG_MPOS_SEARCH_GRP_ITEM," + grp_pk +"|"+keyword.getText();
        ConnectThread networkThread = new ConnectThread(this);
        String result[][] = networkThread.execute(l_para).get();
        OnShowScanLog(result);
    }

    // show data scan log
    public void OnShowScanLog(String a[][])
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
            int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            //db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
           //cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='5' and SENT_YN='N' order by PK desc LIMIT 20",null);// TLG_LABEL

            //int count = cursor.getCount();

            //if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < a.length; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableRowWithText(String.valueOf(i + 1), scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(a[i][1], scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(a[i][3], scrollableColumnWidths[4], fixedRowHeight));

                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(1); //SLIP_NO
                            TextView tvSlipNo1 = (TextView) tr1.getChildAt(2); //SLIP_NO
                            String st_grp = tvSlipNo.getText().toString();
                            String st_name = tvSlipNo1.getText().toString();

                            TextView grp=(TextView) findViewById(R.id.txtGroupName);
                            TextView item=(TextView) findViewById(R.id.txtItemName);
                            grp.setText(st_grp);
                            item.setText(st_name);
                        }
                    });

                    scrollablePart.addView(row);
                    //cursor.moveToNext();
                }
            //}
            /*
            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            String sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='5' and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtRemain=(TextView) findViewById(R.id.txtRemain);
            _txtRemain.setText("Re: " + count);
            */

        } catch (Exception ex) {
            alertToast(ex.getMessage());
        } finally {
            //db.close();
            //cursor.close();
        }
    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

        TableRow row = new TableRow(this);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) findViewById(R.id.grdData1);
        row = new TableRow(this);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(colors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Group Name", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item Name", scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);



        // Accept Scan
        scrollablePart = (TableLayout) findViewById(R.id.grdData3);
        row = new TableRow(this);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(colors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Group Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Lot No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Group_PK", 0, fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    // end get data return array--use array set to grid view

    public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(this);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.BLACK);
        recyclableTextView.setTextSize(16);
        recyclableTextView.setBackgroundColor(-1);
        recyclableTextView.setPadding(0, 5, 0, 5);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }

    public TextView makeTableRowHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(this);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.YELLOW);
        recyclableTextView.setTextSize(18);
        recyclableTextView.setGravity(Gravity.CENTER);
        recyclableTextView.setBackgroundColor(colors[1]);
        recyclableTextView.setPadding(0, 2, 0, 2);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item_input, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
