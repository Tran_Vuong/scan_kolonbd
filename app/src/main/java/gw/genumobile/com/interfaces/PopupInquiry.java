package gw.genumobile.com.interfaces;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;

import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.views.gwFragmentDialog;

/**
 * Created by Laptop-TL on 03/03/2017.
 */

public class PopupInquiry extends gwFragmentDialog implements View.OnClickListener {
    private SharedPreferences appPrefs;

    Queue queue = new LinkedList();
    SQLiteDatabase db = null;
    Cursor cursor;
    Calendar c;
    SimpleDateFormat df;

    TextView _txtQty, _txtTotal,_txtHeader,recyclableTextView;

    Button btnSearch;
    String type="" ,sql = "";

    //endregion
    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
    }
    //region Application Circle
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_popup_inquiry, container, false);
        type = getArguments().getString("type");

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                    Intent i = new Intent().putExtra("month", "trang");
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                    dismiss();
                }
                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);

        toolbar.setTitle("ITEM INQUIRY BARCODE");
        
        //Hidden
        btnSearch=(Button)rootView.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);


        OnShowGridHeader();

        //OnShowScanAccept();

        return rootView;
    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvRefNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvUom), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvMakeSlip), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSoNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhIn), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhLocIn), scrollableColumnWidths[2], fixedHeaderHeight));


        scrollablePart.addView(row);
    }
    public void  OnSearch(View v){

        EditText _edReqNo=(EditText) rootView.findViewById(R.id.edReqNo);
        String keywordReq=_edReqNo.getText().toString();
        EditText _edLoc=(EditText) rootView.findViewById(R.id.edLoc);
        String keywordLoc=_edLoc.getText().toString();
        EditText _edItem=(EditText) rootView.findViewById(R.id.edItem);
        String keywordItem=_edItem.getText().toString();
        EditText _edUom=(EditText) rootView.findViewById(R.id.edUom);
        String keywordUom=_edUom.getText().toString();
        EditText _edPartList=(EditText) rootView.findViewById(R.id.edPartList);
        String keywordPart=_edPartList.getText().toString();

        int date_previous=Integer.parseInt(CDate.getDatePrevious(10));
        try{
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,ITEM_NAME, TR_QTY,UOM, TR_LOT_NO,REQ_NO, SLIP_NO, SO_NO, LINE_NAME, TR_WH_IN_NAME,TLG_IN_WHLOC_IN_NAME, REMARKS " +
                    " FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '"+type+"' AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND STATUS='000' " +
                    " AND (ITEM_CODE like '%"+keywordItem+"%' or ITEM_BC like '%"+keywordItem+"%') " +
                    " AND (REQ_NO like '%"+keywordReq+"%' or '"+keywordReq+"'='')" +
                    " AND (TLG_IN_WHLOC_IN_NAME like '%"+keywordLoc+"%' or '"+keywordLoc+"'='') " +
                    " AND (UOM like '%"+keywordUom+"%' or '"+keywordUom+"'='') " +
                    " ORDER BY pk desc ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            float _qty=0;
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(i + 1), scrollableColumnWidths[0], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REQ_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[1], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UOM")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SO_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_NAME")), scrollableColumnWidths[2], fixedRowHeight, -1));

                    _qty=_qty+ Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            _txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
            _txtTotal.setText("Total: " + count + " ");
            _txtTotal.setTextColor(Color.BLUE);
            _txtTotal.setTextSize((float) 18.0);
            //---------------------------------------------------------------------

            _txtQty = (TextView) rootView.findViewById(R.id.txtQty);
            _txtQty.setText("Qty: " + _qty + " ");
            _txtQty.setTextColor(Color.BLUE);
            _txtQty.setTextSize((float) 18.0);

        }catch (Exception ex) {
            //Toast.makeText(gwMActivity,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            //Log.e("OnShowScanAccept Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSearch:
                OnSearch(view);
                break;

            case R.id.btnDeleteBC:
                //onClickDeleteBC(view);
                break;

            case R.id.btnMakeSlipBot:
                //onClickMakeSlipBot(view);
                break;
            default:
                break;
        }
    }
}
