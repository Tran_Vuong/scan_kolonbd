package gw.genumobile.com.interfaces;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.views.gwFragmentDialog;


public class GridListViewOnePK extends gwFragmentDialog implements View.OnClickListener {

    //region Argument

    Queue queue = new LinkedList();
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    String data[] = new String[0];

    Calendar c;
    SimpleDateFormat df;

    TextView txtQty, txtTotal, txtHeader;

    String ITEM_CODE, sql = "", scan_date = "", unique_id = "", wh_name_grid = "";
    String SLIP_NO = "", TR_ITEM_PK = "", TLG_GD_REQ_M_PK = "",TLG_GD_REQ_D_PK="";
    String formID="6";
    //endregion
    View rootView;
    Button btnDeleteBC,btnMakeSlipBot,btnGoodsPartial;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
    }
    //region Application Circle
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_grid_list_view_one_pk, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                    Intent i = new Intent().putExtra("month", "trang");
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                    dismiss();
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);


        //Toast.makeText(this,type, Toast.LENGTH_SHORT).show();
        //txtHeader=(TextView) rootView.findViewById(R.id.txtHeader);
        toolbar.setTitle("LIST ITEM BARCODE");


        formID  = getArguments().getString("form_id");
        ITEM_CODE = getArguments().getString("ITEM_CODE");
        SLIP_NO = getArguments().getString("SLIP_NO");
        TR_ITEM_PK = getArguments().getString("TR_ITEM_PK");
        TLG_GD_REQ_M_PK = getArguments().getString("TLG_GD_REQ_M_PK");
        TLG_GD_REQ_D_PK = getArguments().getString("TLG_GD_REQ_D_PK");
        scan_date = CDate.getDateyyyyMMdd();
        txtHeader=(TextView) rootView.findViewById(R.id.txtHeader);

        //Hidden
        btnGoodsPartial=(Button)rootView.findViewById(R.id.btnGoodsPartial);
        btnGoodsPartial.setOnClickListener(this);
        btnGoodsPartial.setVisibility(View.GONE);
        btnMakeSlipBot=(Button)rootView.findViewById(R.id.btnMakeSlipBot);
        btnMakeSlipBot.setOnClickListener(this);
        btnMakeSlipBot.setVisibility(View.GONE);
        btnDeleteBC=(Button)rootView.findViewById(R.id.btnDeleteBC);
        btnDeleteBC.setOnClickListener(this);



        OnShowHeaderGoodsDelivery();

        OnShowScanAccept();

        return rootView;
    }
    //endregion

    //region Method
    //----------------------------------------------------------
    ///Load Header grid
    //----------------------------------------------------------
    public void OnShowGridHeaderIncome()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    public void OnShowHeaderGoodsDelivery()
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;


        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remark", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("PK", 0, fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    ////////////////////////////////////////////////
    //POPUP Alert
    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str=str_alert;
        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                //Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    //----------------------------------------------------------
    ////show data scan accept Delivery
    //----------------------------------------------------------
    public void OnShowScanAccept()
    {
        ///// TODO: 09/01/2016 Giang
//        Button btnMSlip=(Button)rootView.findViewById(R.id.btnMakeSlipBot);
//        btnMSlip.setVisibility(View.VISIBLE);
        int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
        try {
            scan_date = CDate.getDateyyyyMMdd();

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            if(formID.equals("24")) {
                sql = "select ITEM_BC, ITEM_CODE,TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK,PK , REMARKS, TLG_GD_REQ_M_PK " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SCAN_DATE >" + date_previous + " AND SENT_YN = 'Y' AND SLIP_NO='" + SLIP_NO + "' AND STATUS='000' " +
                        " AND TR_ITEM_PK = " + TR_ITEM_PK + " AND TLG_GD_REQ_M_PK = '" + TLG_GD_REQ_M_PK +  "' AND TLG_GD_REQ_D_PK = '" + TLG_GD_REQ_D_PK +"'"+
                        " ORDER BY pk desc ";
            }else {
                sql = "select ITEM_BC, ITEM_CODE,TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK,PK , REMARKS, TLG_GD_REQ_M_PK " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SCAN_DATE >" + date_previous + " AND SENT_YN = 'Y' AND SLIP_NO='" + SLIP_NO + "' AND STATUS='000' " +
                        " AND TR_ITEM_PK = " + TR_ITEM_PK + " AND TLG_GD_REQ_M_PK = '" + TLG_GD_REQ_M_PK + "'" +
                        " ORDER BY pk desc ";
            }
            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            float _qty=0;

            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[5], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_POP_INV_TR_PK")), 0, fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK")), 0, fixedRowHeight,0));


                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
                            String st_slipNO=tvSlipNo.getText().toString();

                            if(st_slipNO.equals("-") || st_slipNO.equals("")){

                                TextView tv1 = (TextView) tr1.getChildAt(8); //PK
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255,99,71));
                                }

                                if (queue.size() > 0) {
                                    if (queue.contains(tv1.getText().toString())) {
                                        queue.remove(tv1.getText().toString());

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK );
                                        }

                                        duplicate = true;
                                    }
                                }

                                if (!duplicate)
                                queue.add(tv1.getText().toString());
                            }
                            gwMActivity.alertToastLong(String.valueOf(queue.size()));
                            
                        }
                    });
                    scrollablePart.addView(row);

                    _qty=_qty+Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }

            txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
            txtTotal.setText("Total: " + count + " ");
            txtTotal.setTextColor(Color.BLUE);
            txtTotal.setTextSize((float) 18.0);
            //---------------------------------------------------------------------

            txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
            txtQty.setText("Qty: " + _qty + " ");
            txtQty.setTextColor(Color.MAGENTA);
            txtQty.setTextSize((float) 18.0);

        } catch (Exception ex) {
            //Toast.makeText(gwMActivity,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            Log.e("OnShowScanAccept Error:", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }


    public void OnShowMakeSlip_Column()
    {

        int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
        try {
            scan_date = CDate.getDateyyyyMMdd();

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            sql = "select '' AS SEQ, T2.ITEM_BC, T2.ITEM_CODE, T2.TR_QTY, T2.TR_LOT_NO, T2.SLIP_NO, T2.TLG_POP_INV_TR_PK, T2.PK , T2.REMARKS, T2.TLG_GD_REQ_M_PK " +
                    " FROM INV_TR t2 WHERE DEL_IF = 0 AND T2.TR_TYPE = '"+formID+"' AND T2.SCAN_DATE ='"+ scan_date +"' AND T2.SENT_YN = 'Y' AND T2.STATUS='000' " +
                    " AND T2.TR_ITEM_PK = " + TR_ITEM_PK + " AND T2.TLG_GD_REQ_M_PK = '" + TLG_GD_REQ_M_PK +"'"+
                    " ORDER BY pk desc ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count-i), scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_POP_INV_TR_PK")), 0, fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[5], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK")), 0, fixedRowHeight,0));

//                    row.setOnClickListener(new View.OnClickListener() {
//                        TextView tv11;
//                        boolean duplicate = false;
//
//                        @Override
//                        public void onClick(View v) {
//
//                            duplicate = false;
//                            TableRow tr1 = (TableRow) v;
//                            TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
//                            String st_slipNO=tvSlipNo.getText().toString();
//
//                            if(st_slipNO.equals("-")){
//
//                                TextView tv1 = (TextView) tr1.getChildAt(7); //INV_TR_PK
//                                int xx = tr1.getChildCount();
//                                for (int i = 0; i < xx; i++) {
//                                    tv11 = (TextView) tr1.getChildAt(i);
//                                    tv11.setTextColor(Color.rgb(255,99,71));
//                                }
//
//                                if (queue.size() > 0) {
//                                    if (queue.contains(tv1.getText().toString())) {
//                                        queue.remove(tv1.getText().toString());
//
//                                        for (int i = 0; i < xx; i++) {
//                                            tv11 = (TextView) tr1.getChildAt(i);
//                                            tv11.setTextColor(Color.BLACK );
//                                        }
//
//                                        duplicate = true;
//                                    }
//                                }
//
//                                if (!duplicate)
//                                    queue.add(tv1.getText().toString());
//                            }
//                            Toast.makeText(getApplicationContext(), queue.size() + "", Toast.LENGTH_SHORT).show();
//                        }
//
//                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

        } catch (Exception ex) {
            Log.e("OnShowMakeLip Error:", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public boolean ProcessMakeSlipDelivery()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            //cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WAREHOUSE_PK,TLG_GD_REQ_M_PK,REMARKS   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='6' ",null);
            sql =       " SELECT TR_ITEM_PK, TLG_GD_REQ_M_PK,TR_QTY, UOM " +
                        " FROM " +
                        "       (   " +
                        "           SELECT  T2.ITEM_NAME, T2.ITEM_CODE, COUNT(*) TOTAL, SUM(T2.TR_QTY) TR_QTY, T2.UOM, T2.TR_ITEM_PK, T2.TLG_GD_REQ_M_PK " +

                        "           FROM    INV_TR T2 " +

                        "           WHERE   DEL_IF = 0 AND T2.TR_TYPE = '"+formID+"'  AND T2.SCAN_DATE = '" + scan_date + "' AND T2.SENT_YN = 'Y' AND T2.STATUS='000' " +
                        "                   AND T2.TR_ITEM_PK = " + TR_ITEM_PK + " AND T2.TLG_GD_REQ_M_PK = '" + TLG_GD_REQ_M_PK + "'"+

                        "           GROUP BY T2.ITEM_NAME, T2.ITEM_CODE, T2.UOM, T2.TR_ITEM_PK, T2.TLG_GD_REQ_M_PK" +
                        "       )";
            cursor = db.rawQuery(sql, null);
            // /int count=cursor.getCount();
            //data=new String [count];
            data=new String [1];
            int j=0;
            String para = "";
            boolean flag=false;
            if (cursor.moveToFirst()) {
                do {
                    flag=true;
                    for (int k = 0; k < cursor.getColumnCount(); k++) {
                        if (para.length() <= 0) {
                            if(cursor.getString(k)!= null)
                                para += cursor.getString(k)+"|";
                            else
                                para += "0|";
                        }
                        else
                        {
                            if(flag==true){
                                if(cursor.getString(k)!= null) {
                                    para += cursor.getString(k);
                                    flag=false;
                                }
                                else {
                                    para += "|";
                                    flag=false;
                                }
                            }
                            else{
                                if(cursor.getString(k)!= null)
                                    para += "|" + cursor.getString(k);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + unique_id;
                    para += "|" + bsUserID;
                    para += "|" + bsUserPK;
                    para += "|" + deviceID;
                    para +="*|*";

                }while (cursor.moveToNext());

                para += "|!LG_MPOS_PRO_GD_MAKESLIP";
                data[j++] = para;
                System.out.print("\n\n ******para make slip goods delivery: " + para);
            }
        }
        catch (Exception ex){
            alertToast("onMakeSlip :" + ex.getMessage());
            return false;
        }
        finally {
            cursor.close();
            db.close();
        }
        // asyns server
        MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
        task.execute(data);
        return true;
    }

    public void onClickDeleteBC(View view){
        if(queue.size()<=0) {
            gwMActivity.alertToastLong("Please select barcode!!!");
            return;
        }
        String title="Confirm Delete...";
        String mess="Are you sure to delete barcode ???";
        alertDialogYN(title, mess, "onDelBC");
    }

    public void alertDialogYN(String title,String mess,final String _type){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event

                if(_type.equals("onDelBC")){
                    ProcessDeleteBC();
                }
                /*
                if(_type.equals("onMakeSlip")){
                    Button btnMSlip=(Button)rootView.findViewById(R.id.btnMakeSlip);
                    btnMSlip.setVisibility(View.GONE);
                    _btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    _btnApprove.setVisibility(View.GONE);
                    ProcessMakeSlip();
                }
                */
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    public void ProcessDeleteBC(){
        int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String para="";
            boolean flag = true;
            if(queue.size()>0) {
                Object[] myLst = queue.toArray();
                //data=new String [myLst.length];
                data=new String [1];
                int j=0;
                for (int i = 0; i < myLst.length; i++) {

                    cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='"+formID+"' and pk="+myLst[i],null);
                    if (cursor.moveToFirst()) {
                        flag = true;
                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + bsUserID;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    }
                }
                para += "|!LG_MPOS_UPD_INV_TR_DEL";
                data[j++] = para;
            }
            else{
                cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='"+formID+"' and SLIP_NO IN ('-','') and STATUS ='000' AND SCAN_DATE > '" + date_previous + "'", null);
                int count = cursor.getCount();

                int j = 0;
                if (cursor.moveToFirst()) {
                    data = new String[1];
                    do {

                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + bsUserID;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_UPD_INV_TR_DEL";
                    data[j] = para;
                    Log.e("para update||delete: ", para);
                }
            }

        }catch (Exception ex){
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        }
        finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP,checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        } else
        {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }

    }

    public  void onDeleteBCFinish(){
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(queue.size()>0)
            {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();
                //data=new String [myLst.length];
                //for (int i = 0; i < myLst.length; i++) {
                    //db.execSQL("DELETE FROM INV_TR where TR_TYPE IN('"+formID+"') and pk="+myLst[i]);
                //}
            }else{
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='"+formID+"';");
            }
        }catch (Exception ex){
            Log.e("Error Delete All :", ex.getMessage());
        }
        finally {
            db.close();

            OnShowScanAccept();
        }
    }

    public  void alertToast(String alert){
        Toast toast=Toast.makeText(gwMActivity, alert, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
    //endregion

    //region Even



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onClickMakeSlipBot(View view) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Make Slip...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to Make Slip");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlipBot);
                btnMSlip.setVisibility(View.GONE);

                if (ProcessMakeSlipDelivery()) {
                    OnShowMakeSlip_Column();
                }

                //Toast.makeText(getApplicationContext(), "Make slip success..", Toast.LENGTH_SHORT).show();
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();


    }

    //endregion

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnGoodsPartial:
                //onGoodsPartial(view);
                break;

            case R.id.btnDeleteBC:
                onClickDeleteBC(view);
                break;

            case R.id.btnMakeSlipBot:
                onClickMakeSlipBot(view);
                break;
            default:
                break;
        }
    }
}
