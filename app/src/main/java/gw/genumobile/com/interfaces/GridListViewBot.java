package gw.genumobile.com.interfaces;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.models.gwform_info;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.views.gwFragmentDialog;
import gw.genumobile.com.views.gwcore.BaseGwActive;


public class GridListViewBot extends gwFragmentDialog implements View.OnClickListener {
   
    Queue queue = new LinkedList();
    Queue dlgqueue = new LinkedList();
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor,cursor2;
    String  data[] = new String[0];

    Calendar c;
    SimpleDateFormat df;

    TextView txtQty, txtTotal,_txtHeader,recyclableTextView;

    String type,sql = "", scan_date = "",unique_id="",wh_name_grid = "";
    String parentBC="";
    float total_qty=0;
    View rootView;
    Button btnDeleteBC,btnMakeSlipBot,btnGoodsPartial;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
    }
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_grid_list_view_bot, container, false);
        
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                    Intent i = new Intent().putExtra("month", "trang");
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                    dismiss();
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);
        
        //lock screen
        //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        type = getArguments().getString("type"); 

        scan_date = CDate.getDateyyyyMMdd();
        Button btnPartial=(Button)rootView.findViewById(R.id.btnGoodsPartial);

        if(type.equals("4"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM INCOMING PROD");
            btnPartial.setVisibility(View.GONE);
            OnShowGridHeaderIncome();
        }
        if(type.equals("5"))
        {
            toolbar.setTitle("LIST ALL ITEM OUTGOING ");
            btnPartial.setVisibility(View.GONE);
            OnShowGridHeaderOutInventory();
        }
        if(type.equals("6"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM GOODS DELIVERY");
            btnPartial.setVisibility(View.GONE);
            OnShowHeaderGoodsDelivery();
        }
        if(type.equals("12"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM PREPARATION");
            btnPartial.setVisibility(View.GONE);
            OnShowHeaderGoodsDelivery();
        }
        if(type.equals("7"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM STOCK IN");
            btnPartial.setVisibility(View.GONE);
            OnShowGridHeaderInInventory();
        }
        if(type.equals("8"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM GOODSDELI WITHOUT REQ");
            btnPartial.setVisibility(View.GONE);
            OnShowGridHeaderGoodsDeliNoReq();
        }
        if(type.equals("9"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM STOCK TRANSFER");
            btnPartial.setVisibility(View.GONE);
            OnShowGridHeaderStockTransfer();
        }
        if(type.equals("11"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM GOODS PARTIAL");
            btnPartial.setVisibility(View.VISIBLE);
            OnShowHeaderGoodsPartial();
        }
        if(type.equals("13"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM STOCK RETURN");
            btnPartial.setVisibility(View.GONE);
            OnShowGridHeaderStockReturn();
        }
        if(type.equals("15"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM STOCK OTHERS");
            btnPartial.setVisibility(View.GONE);
            OnShowGridHeaderStockOthers();
        }
        if(type.equals("16"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM GOODS RETURN");
            btnPartial.setVisibility(View.GONE);

            OnShowGridHeaderGoodsReturn();
        }
        if(type.equals("20"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM EXCHANGE PRODUCT");
            btnPartial.setVisibility(View.GONE);

            OnShowHeaderExchangeProduct();
        }
        if(type.equals("24"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM STOCK IN FROM REQ");
            btnPartial.setVisibility(View.GONE);

            OnShowGridHeaderInInventory();
        }
        if(type.equals("25"))
        {
            toolbar.setTitle("LIST ACCEPT ITEM STOCK OUT FROM REQ ");
            btnPartial.setVisibility(View.GONE);
            OnShowGridHeaderOutInventory();
        }
        OnShowScanAccept();

        return rootView;
    }


    public void onGoodsPartial(View view){
        if(!parentBC.equals("")){
            dlgqueue = new LinkedList();//reset
            //popup
            final Dialog dlg_Partial = new Dialog(gwMActivity);
            dlg_Partial.setContentView(R.layout.dialog_parent_partial);
            dlg_Partial.setCancelable(false);//not close when touch
            dlg_Partial.setTitle(" Config Setting");
            ///////
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
            int fixedRowHeight = 30;
            int fixedHeaderHeight = 30;
            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart;
            scrollablePart = (TableLayout) dlg_Partial.findViewById(R.id.grd_dlg_Header);
            row = new TableRow(gwMActivity);
            row.setLayoutParams(wrapWrapTableRowParams);
            row.setGravity(Gravity.CENTER);
            row.setBackgroundColor(bsColors[1]);
            row.setVerticalGravity(50);

            row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
            row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[4], fixedHeaderHeight));
            scrollablePart.addView(row);
            /////////////////////////////////////////////////////////////////
            try {
                String l_para = "1,LG_MPOS_GET_CHILDBC," + parentBC;
                ConnectThread networkThread = new ConnectThread(gwMActivity);

                String result[][] = networkThread.execute(l_para).get();

                TableLayout scrollablePart1 = (TableLayout) dlg_Partial.findViewById(R.id.grd_dlg_Content);
                scrollablePart1.removeAllViews();//remove all view child

                for (int i = 0; i < result.length; i++) {
                    TableRow row1 = new TableRow(gwMActivity);
                    row1.setLayoutParams(wrapWrapTableRowParams);
                    row1.setGravity(Gravity.CENTER);
                    row1.setBackgroundColor(Color.LTGRAY);
                    dlgqueue.add(result[i][0].toString());
                    row1.addView(makeTableColWithText(result[i][0].toString(), scrollableColumnWidths[1], fixedRowHeight,0));
                    row1.addView(makeTableColWithText(result[i][1].toString(), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row1.addView(makeTableColWithText(result[i][2].toString(), scrollableColumnWidths[4], fixedRowHeight,-1));
                    total_qty=total_qty +  Float.valueOf(result[i][2].toString());//Qty
                    row1.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            duplicate = false;
                            TableRow rw = (TableRow) v;
                            //////
                            TextView tv1 = (TextView) rw.getChildAt(0); //INV_TR_PK
                            TextView tvQty = (TextView) rw.getChildAt(2); //INV_TR_PK
                            int r = rw.getChildCount();
                            for (int i = 0; i < r; i++) {
                                tv11 = (TextView) rw.getChildAt(i);
                                tv11.setTextColor(Color.BLACK);
                            }

                            if (dlgqueue.size() > 0) {
                                if (dlgqueue.contains(tv1.getText().toString())) {
                                    dlgqueue.remove(tv1.getText().toString());
                                    total_qty = total_qty - Float.valueOf(tvQty.getText().toString());
                                    for (int i = 0; i < r; i++) {
                                        tv11 = (TextView) rw.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255, 99, 71));
                                    }
                                    duplicate = true;
                                }
                            }

                            if (!duplicate) {
                                dlgqueue.add(tv1.getText().toString());
                                total_qty = total_qty + Float.valueOf(tvQty.getText().toString());
                            }
                            // Toast.makeText(getApplicationContext(), "Size dlgqueue " + dlgqueue.size() + "", Toast.LENGTH_SHORT).show();
                        }
                    });
                    scrollablePart1.addView(row1);
                }
                //Toast.makeText(getApplicationContext(),"Size dlgqueue "+ dlgqueue.size() + "", Toast.LENGTH_SHORT).show();
            } catch (Exception ex) {
                Toast.makeText(gwMActivity, "Error get Child BC: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            }
            /////////////////////////////////////////////////////////////////
            Button btnSave = (Button) dlg_Partial.findViewById(R.id.btnSave);
            Button btnCancel = (Button) dlg_Partial.findViewById(R.id.btnCancel);

            // Attached listener for login GUI button
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Object[] lst = dlgqueue.toArray();
                        if(lst.length > 0) {
                            String pk_child="";
                            for(int i=0;i<lst.length;i++)
                            {
                                if(pk_child.length() <= 0)
                                {
                                    pk_child += lst[i];
                                }
                                else
                                {
                                    pk_child += ";" + lst[i];
                                }
                            }
                            //onPopAlert(pk_child);
                            sql = "UPDATE INV_TR set TR_DELI_QTY=" + total_qty + ",ITEM_BC_CHILD='"+ pk_child + "'  where ITEM_BC = " + parentBC;
                            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                            db.execSQL(sql);
                        }
                    } catch (Exception ex) {
                        gwMActivity.alertToastLong(ex.getMessage());
                        Log.e("Save Qty error:", ex.getMessage());
                    } finally {
                        parentBC="";
                        total_qty = 0;
                        db.close();
                        OnShowScanAccept();
                        dlg_Partial.dismiss();
                    }
                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    parentBC="";
                    total_qty = 0;
                    OnShowScanAccept();
                    dlg_Partial.dismiss();
                }
            });
            dlg_Partial.show();
        }else{
            onPopAlert("Please select Parent BC first!!!");
        }
    }


    public void onClickMakeSlipBot(View view){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Make Slip...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to Make Slip");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                // Write your code here to invoke YES event
                Button btnMSlip=(Button)rootView.findViewById(R.id.btnMakeSlipBot);
                btnMSlip.setVisibility(View.GONE);
                if(type.equals("4"))//INCOME PROD V2
                {
                    ProcessMakeSlipIncomeProd();
                }
                if(type.equals("5")) //STOCK OUT V2
                {
                    ProcessMakeSlipOutInventory();
                }
                if(type.equals("6")) //GOODSDELIVERY V2
                {
                    ProcessMakeSlipDelivery();
                }
                if(type.equals("7")) //STOCK IN V2
                {
                    ProcessMakeSlipInInventory();
                }
                if(type.equals("8")) //GOODS DELI NO REQ
                {
                    ProcessMakeSlipGoodsDeliNoReq();
                }
                if(type.equals("9")) //GOODS DELI NO REQ
                {
                    ProcessMakeSlipStockTransfer();
                }

                if(type.equals("11")) //GOODS DELI NO REQ
                {
                    ProcessMakeSlipGoodsPartial();
                }
                //Toast.makeText(getApplicationContext(), "Make slip success..", Toast.LENGTH_SHORT).show();
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();


    }

    public void ProcessMakeSlipDelivery()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WAREHOUSE_PK,TLG_GD_REQ_M_PK,REMARKS   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='6' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {

                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|"+unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "*|*";
                    }
                }
                para += "|!LG_MPOS_PRO_GD_MAKESLIP";
                data[j++]=para;
                //System.out.print("\n\n ******para make slip goods delivery: "+para);

            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WAREHOUSE_PK,TLG_GD_REQ_M_PK,REMARKS   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='6' ",null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para = "";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para +="*|*";

                    }while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_GD_MAKESLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip goods delivery: " + para);
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipIncomeProd()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++)
                {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_POP_LABEL_PK, SCAN_DATE, TR_WAREHOUSE_PK, TR_LINE_PK,REMARKS,PARENT_PK   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='4' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //get list child pk
                        String parent_pk=cursor.getString(cursor.getColumnIndex("PARENT_PK"));
                        //onPopAlert(parent_pk);
                        String lschild="";
                        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        cursor2 = db2.rawQuery("select CHILD_PK   from INV_TR where del_if=0 and CHILD_PK !=0 and PARENT_PK="+parent_pk,null);
                        int count1=cursor2.getCount();
                        if (cursor2.moveToFirst()) {
                            lschild = "";
                            do {

                                for (int col = 0; col < cursor2.getColumnCount(); col++) {
                                    if (lschild.length() <= 0) {
                                        if (cursor2.getString(col) != null)
                                            lschild += cursor2.getString(col);
                                        else
                                            lschild += "0";
                                    } else {
                                        if (cursor2.getString(col) != null)
                                            lschild += ";" + cursor2.getString(col);
                                        else
                                            lschild += ";";
                                    }
                                }
                            }while (cursor2.moveToNext());
                        }
                        db2.close();
                        cursor2.close();
                        ///////////////////////////////
                        para += "|" +lschild;
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                    }
                    para += "*|*";
                }
                para += "|!"+"LG_MPOS_PRO_INC_MAKESLIP_MAP";
                System.out.print("\n\n ******para make slip goods delivery: "+para);
                Log.e("Make slip para",para);
                data[j++]=para;
            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK,  TLG_POP_LABEL_PK, SCAN_DATE, TR_WAREHOUSE_PK, TR_LINE_PK,REMARKS,PARENT_PK   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='4' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //get list child pk
                        String parent_pk=cursor.getString(cursor.getColumnIndex("PARENT_PK"));
                        //onPopAlert(parent_pk);
                        String lschild="";
                        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        cursor2 = db2.rawQuery("select CHILD_PK   from INV_TR where del_if=0 and CHILD_PK !=0 and PARENT_PK="+parent_pk,null);
                        int count1=cursor2.getCount();
                        if (cursor2.moveToFirst()) {
                            lschild = "";
                            do {

                                for (int col = 0; col < cursor2.getColumnCount(); col++) {
                                    if (lschild.length() <= 0) {
                                        if (cursor2.getString(col) != null)
                                            lschild += cursor2.getString(col);
                                        else
                                            lschild += "0";
                                    } else {
                                        if (cursor2.getString(col) != null)
                                            lschild += ";" + cursor2.getString(col);
                                        else
                                            lschild += ";";
                                    }
                                }
                            }while (cursor2.moveToNext());

                        }
                        db2.close();
                        cursor2.close();

                        ////////////////////
                        para += "|" +lschild;
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        //para += "|!lg_mpos_pro_inc_makeslip";
                        para += "*|*";
                    }while (cursor.moveToNext());
                }
                para += "|!LG_MPOS_PRO_INC_MAKESLIP_MAP";
                //onPopAlert(para);
                System.out.print("\n\n ******para make slip income prod: " + para);
                data[j++] = para;
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipInInventory()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WAREHOUSE_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='7' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //para += "|!"+USER;
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                    }
                    para += "*|*";
                }
                para += "|!LG_MPOS_PRO_ST_INC_MAKESLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip goods delivery: "+para);
            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WAREHOUSE_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='7' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "*|*";
                    }while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_ST_INC_MAKESLIP";
                    System.out.print("\n\n ******para make slip in stock: " + para);
                    data[j++] = para;
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipOutInventory()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();
        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WAREHOUSE_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='5' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {
                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //para += "|!"+USER;
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "*|*";
                    }
                }
                para += "|!LG_MPOS_PRO_ST_OUT_MAKESLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip stock out: "+para);
            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WAREHOUSE_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='5' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para +="*|*";
                    }while (cursor.moveToNext());
                    para += "|!"+"LG_MPOS_PRO_ST_OUT_MAKESLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip stock out: " + para);
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipGoodsDeliNoReq()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WAREHOUSE_PK,REMARKS   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='8' and pk="+myLst[i],null);
                    if (cursor.moveToFirst()) {
                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|"+unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                    }
                    para += "*|*";
                }
                para += "|!"+"LG_MPOS_PRO_GD_NOREQ_MAKESLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip goods delivery: "+para);

            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WAREHOUSE_PK,REMARKS   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='8' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "*|*";
                    }while (cursor.moveToNext());

                    para += "|!"+"LG_MPOS_PRO_GD_NOREQ_MAKESLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip goods delivery: " + para);
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipStockTransfer()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_CODE,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, SCAN_DATE, TR_WAREHOUSE_PK,TR_LINE_PK, UNIT_PRICE   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='9' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //para += "|!"+USER;
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;

                    }
                    para += "*|*";
                }
                para += "|!"+"LG_MPOS_PRO_ST_TRANSFER_MSLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip stock out: "+para);

            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_CODE,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, SCAN_DATE, TR_WAREHOUSE_PK,TR_LINE_PK, UNIT_PRICE   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='9' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "*|*";
                    }while (cursor.moveToNext());

                    para += "|!"+"LG_MPOS_PRO_ST_TRANSFER_MSLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip stock out: " + para);
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipGoodsPartial()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();
        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_DELI_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WAREHOUSE_PK,TLG_GD_REQ_M_PK,REMARKS,ITEM_BC_CHILD   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='11' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|"+unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "*|*";

                    }
                }
                para += "|!LG_MPOS_PRO_GD_PARTIAL_MSLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip goods delivery: " + para);
                //onPopAlert(para);
            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
                onPopAlert(ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_DELI_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WAREHOUSE_PK,TLG_GD_REQ_M_PK,REMARKS,ITEM_BC_CHILD   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='11' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para = "";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;

                    }while (cursor.moveToNext());
                    para += "|!"+"LG_MPOS_PRO_GD_PARTIAL_MSLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip goods delivery: " + para);
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
    }
    //----------------------------------------------------------
    ////show data scan accept Delivery
    //----------------------------------------------------------
    public void OnShowScanAccept()
    {
        Button btnMSlip=(Button)rootView.findViewById(R.id.btnMakeSlipBot);
        btnMSlip.setVisibility(View.INVISIBLE);
        int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            //region ------Prod Income---------
            if(type.equals("4")) {
                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child


                sql = "select  PK, ITEM_BC, ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE = '4' AND STATUS IN('OK', '000') ORDER BY PK desc ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                float _qty=0;
                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));

                        _qty=_qty+ Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));

                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
                                String st_slipNO=tvSlipNo.getText().toString();
                                if(st_slipNO.equals("-")){
                                    TextView tv1 = (TextView) tr1.getChildAt(6); //TLG_POP_INV_TR_PK
                                    int xx = tr1.getChildCount();

                                    for (int i = 0; i < xx; i++) {
                                        tv11 = (TextView) tr1.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255,99,71));
                                    }

                                    if (queue.size() > 0) {
                                        if (queue.contains(tv1.getText().toString())) {
                                            queue.remove(tv1.getText().toString());

                                            for (int i = 0; i < xx; i++) {
                                                tv11 = (TextView) tr1.getChildAt(i);
                                                tv11.setTextColor(Color.BLACK);
                                            }
                                            duplicate = true;
                                        }
                                    }

                                    if (!duplicate)
                                        queue.add(tv1.getText().toString());

                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));
                            }
                        });

                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }

                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
                //---------------------------------------------------------------------

                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                //txtQty.setText("Qty: " + _qty + " ");
                txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);
            }
            //endregion
            //region --------Type 5--------------
            if(type.equals("5"))
            {
                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select  PK, ITEM_BC, ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK,UNIT_PRICE,SUPPLIER_NAME,PO_NO,TR_WH_IN_NAME " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE = '5' AND STATUS IN('OK', '000') ORDER BY PK ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                float _qty = 0;
                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UNIT_PRICE")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PO_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));

                        _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));

                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(6); //SLIP_NO
                                String st_slipNO=tvSlipNo.getText().toString();
                                if(st_slipNO.equals("-")) {
                                    TextView tv1 = (TextView) tr1.getChildAt(5); //TR_WH_IN_NAME
                                    if(wh_name_grid.equals("")){
                                        wh_name_grid=tv1.getText().toString();
                                    }
                                    if(tv1.getText().toString().equals(wh_name_grid)) {
                                        TextView tv2 = (TextView) tr1.getChildAt(9); //TLG_POP_INV_TR_PK
                                        int xx = tr1.getChildCount();

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.rgb(255,99,71));
                                        }
                                        String value =tv2.getText().toString();
                                        if (queue.size() > 0) {
                                            if (queue.contains(value)) {
                                                queue.remove(value);

                                                for (int i = 0; i < xx; i++) {
                                                    tv11 = (TextView) tr1.getChildAt(i);
                                                    tv11.setTextColor(Color.BLACK);
                                                }
                                                duplicate = true;
                                            }
                                        }
                                        if (!duplicate)
                                            queue.add(value);
                                    }
                                    else{
                                        gwMActivity.alertToastShort("WH is different");
                                    }
                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));
                            }
                        });
                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
                //---------------------------------------------------------------------

                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);
            }
            //endregion
            //region -------Type 6---------
            if(Integer.valueOf(type)== gwform_info.GoodsdeliveryV2.Id())
            {
                //btnMSlip.setVisibility(View.GONE);
                scan_date = CDate.getDateyyyyMMdd();

                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select (select COUNT(0)" +
                        "from INV_TR t1 " +
                        "where del_if = 0 and t1.pk <= t2.pk  AND T1.TR_TYPE = '"+gwform_info.GoodsdeliveryV2.Id()+"' AND T1.SCAN_DATE ='" + scan_date + "' AND T1.SENT_YN = 'Y' AND T1.STATUS ='000' " +
                        ") AS SEQ, T2.ITEM_BC, T2.ITEM_CODE, T2.TR_QTY, T2.TR_LOT_NO, T2.SLIP_NO, T2.TLG_POP_INV_TR_PK, T2.PK , T2.REMARKS " +
                        " FROM INV_TR t2 WHERE DEL_IF = 0 AND T2.TR_TYPE = '"+gwform_info.GoodsdeliveryV2.Id()+"' AND T2.SCAN_DATE ='"+ scan_date +"' AND T2.SENT_YN = 'Y' AND T2.STATUS='000' ORDER BY pk desc ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();

                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_POP_INV_TR_PK")), 0, fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[5], fixedRowHeight,-1));


                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
                                String st_slipNO=tvSlipNo.getText().toString();

                                if(st_slipNO.equals("-")){

                                    TextView tv1 = (TextView) tr1.getChildAt(7); //INV_TR_PK
                                    int xx = tr1.getChildCount();
                                    for (int i = 0; i < xx; i++) {
                                        tv11 = (TextView) tr1.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255,99,71));
                                    }

                                    if (queue.size() > 0) {
                                        if (queue.contains(tv1.getText().toString())) {
                                            queue.remove(tv1.getText().toString());

                                            for (int i = 0; i < xx; i++) {
                                                tv11 = (TextView) tr1.getChildAt(i);
                                                tv11.setTextColor(Color.BLACK );
                                            }

                                            duplicate = true;
                                        }
                                    }

                                    if (!duplicate)
                                        queue.add(tv1.getText().toString());
                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));
                            }
                        });

                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '6' AND SCAN_DATE ='" + scan_date + "' AND SENT_YN = 'Y' AND STATUS='000'";
                cursor = db.rawQuery(sql, null);
                count = cursor.getCount();
                float _qty=0;
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        _qty=_qty+Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                        cursor.moveToNext();
                    }
                }
                cursor.close();

                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
                //---------------------------------------------------------------------

                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);
            }
            //endregion----------
            //region --------Type 7---------
            if(type.equals("7")) {
                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select  PK, ITEM_BC, ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK,UNIT_PRICE,SUPPLIER_NAME,PO_NO,TR_WH_IN_NAME " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE = '7' AND STATUS IN('OK', '000') ORDER BY PK ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UNIT_PRICE")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PO_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,-1));

                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(6); //SLIP_NO
                                String st_slipNO = tvSlipNo.getText().toString();
                                if (st_slipNO.equals("-")) {
                                    TextView tv1 = (TextView) tr1.getChildAt(5); //TR_WH_IN_NAME
                                    if (wh_name_grid.equals("")) {
                                        wh_name_grid = tv1.getText().toString();
                                    }

                                    if (tv1.getText().toString().equals(wh_name_grid)) {
                                        TextView tv2 = (TextView) tr1.getChildAt(9); //TLG_POP_INV_TR_PK
                                        int xx = tr1.getChildCount();

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.rgb(255,99,71));
                                        }
                                        String value = tv2.getText().toString();
                                        if (queue.size() > 0) {
                                            if (queue.contains(value)) {
                                                queue.remove(value);

                                                for (int i = 0; i < xx; i++) {
                                                    tv11 = (TextView) tr1.getChildAt(i);
                                                    tv11.setTextColor(Color.BLACK);
                                                }
                                                duplicate = true;
                                            }
                                        }

                                        if (!duplicate)
                                            queue.add(value);
                                    } else {
                                        gwMActivity.alertToastShort("WH is different");
                                    }
                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));

                            }
                        });

                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
            }
            //endregion
            //region -----------Type 8--------------
            if(type.equals("8")) {

                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select (select COUNT(0)" +
                        "from INV_TR t1 " +
                        "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE > " + date_previous + " AND T1.SENT_YN = 'Y' AND T1.STATUS ='000' " +
                        ") AS SEQ, T2.ITEM_BC, T2.ITEM_CODE, T2.TR_QTY, T2.TR_LOT_NO, T2.SLIP_NO, T2.TLG_POP_INV_TR_PK, T2.PK , T2.REMARKS " +
                        " FROM INV_TR t2 WHERE DEL_IF = 0 AND T2.TR_TYPE = '8' AND T2.SCAN_DATE > " + date_previous + " AND T2.SENT_YN = 'Y' AND T2.STATUS='000' ORDER BY pk desc ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                float _qty=0;
                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count -i), scrollableColumnWidths[1], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_POP_INV_TR_PK")), 0, fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[5], fixedRowHeight,-1));
                        _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
                                String st_slipNO=tvSlipNo.getText().toString();

                                if(st_slipNO.equals("-")){

                                    TextView tv1 = (TextView) tr1.getChildAt(7); //INV_TR_PK
                                    int xx = tr1.getChildCount();
                                    for (int i = 0; i < xx; i++) {
                                        tv11 = (TextView) tr1.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255,99,71));
                                    }
                                    if (queue.size() > 0) {
                                        if (queue.contains(tv1.getText().toString())) {
                                            queue.remove(tv1.getText().toString());
                                            for (int i = 0; i < xx; i++) {
                                                tv11 = (TextView) tr1.getChildAt(i);
                                                tv11.setTextColor(Color.BLACK );
                                            }
                                            duplicate = true;
                                        }
                                    }
                                    if (!duplicate)
                                        queue.add(tv1.getText().toString());
                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));
                            }
                        });

                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
                //---------------------------------------------------------------------
                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);
            }
            //endregion
            //region ----------Type 9 , 13, 15 --------
            if(type.equals("9") || type.equals("13") || type.equals("15")) {
                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select  PK, ITEM_BC, ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK,UNIT_PRICE,SUPPLIER_NAME,PO_NO,TR_WH_IN_NAME,LINE_NAME " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE = '"+type+"' AND STATUS IN('OK', '000') ORDER BY PK desc ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                float _qty=0;
                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[0], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PO_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));

                        _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));

                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
                                String st_slipNO=tvSlipNo.getText().toString();
                                if(st_slipNO.equals("-")) {
                                    TextView tv1 = (TextView) tr1.getChildAt(6); //TR_WH_IN_NAME
                                    //if(wh_name_grid.equals("")){
                                    //    wh_name_grid=tv1.getText().toString();
                                    //}

                                    //if(tv1.getText().toString().equals(wh_name_grid)) {
                                    TextView tv2 = (TextView) tr1.getChildAt(10); //TLG_POP_INV_TR_PK
                                    int xx = tr1.getChildCount();

                                    for (int i = 0; i < xx; i++) {
                                        tv11 = (TextView) tr1.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255,99,71));
                                    }
                                    String value =tv2.getText().toString();
                                    if (queue.size() > 0) {
                                        if (queue.contains(value)) {
                                            queue.remove(value);

                                            for (int i = 0; i < xx; i++) {
                                                tv11 = (TextView) tr1.getChildAt(i);
                                                tv11.setTextColor(Color.BLACK);
                                            }
                                            duplicate = true;
                                        }
                                    }

                                    if (!duplicate)
                                        queue.add(value);
                                    //}
                                    //else{
                                    //    Toast.makeText(getApplicationContext(),"WH is different", Toast.LENGTH_SHORT).show();
                                    //}
                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));
                            }
                        });

                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
                //---------------------------------------------------------------------
                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);

            }
            //endregion
            //region ---------Type 11 -------------
            if(type.equals("11")) {
                date_previous=Integer.parseInt(CDate.getDatePrevious(2));
                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

               // db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select (select COUNT(0)" +
                        "from INV_TR t1 " +
                        "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE > '" + date_previous + "' AND T1.SENT_YN = 'Y' AND T1.STATUS ='000' " +
                        ") AS SEQ, T2.ITEM_BC,T2.BC_TYPE, T2.ITEM_CODE, T2.TR_QTY,T2.TR_DELI_QTY, T2.TR_LOT_NO, T2.SLIP_NO, T2.TLG_POP_INV_TR_PK, T2.PK , T2.REMARKS " +
                        " FROM INV_TR t2 WHERE DEL_IF = 0 AND T2.TR_TYPE = '11' AND T2.SCAN_DATE > '" + date_previous + "' AND T2.SENT_YN = 'Y' AND T2.STATUS='000' ORDER BY pk desc  ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                float _qty=0;
                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count-i), scrollableColumnWidths[1], fixedRowHeight,0));
                        if(cursor.getString(cursor.getColumnIndex("BC_TYPE")).equals("P"))
                            row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                        else
                            row.addView(makeTableColWithText("", scrollableColumnWidths[3], fixedRowHeight,0));
                        if(cursor.getString(cursor.getColumnIndex("BC_TYPE")).equals("C"))
                            row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                        else
                            row.addView(makeTableColWithText("", scrollableColumnWidths[3], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_DELI_QTY")), scrollableColumnWidths[2], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[5], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_POP_INV_TR_PK")), 0, fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,-1));
                        _qty=_qty+Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(7); //SLIP_NO
                                String st_slipNO=tvSlipNo.getText().toString();

                                if(st_slipNO.equals("-")){
                                    ///////check click parent pk
                                    TextView parent=(TextView) tr1.getChildAt(1);//Parent BC

                                    //////
                                    TextView tv1 = (TextView) tr1.getChildAt(10); //INV_TR_PK
                                    int xx = tr1.getChildCount();
                                    for (int i = 0; i < xx; i++) {
                                        tv11 = (TextView) tr1.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255,99,71));
                                    }

                                    if (queue.size() > 0) {
                                        if (queue.contains(tv1.getText().toString())) {
                                            queue.remove(tv1.getText().toString());

                                            for (int i = 0; i < xx; i++) {
                                                tv11 = (TextView) tr1.getChildAt(i);
                                                tv11.setTextColor(Color.BLACK );
                                            }
                                            parentBC="";
                                            duplicate = true;
                                        }
                                    }

                                    if (!duplicate) {
                                        queue.add(tv1.getText().toString());
                                        parentBC=parent.getText().toString();
                                    }
                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));
                            }
                        });

                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);

                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);
            }
            //endregion
            //region ---------Type 12 ----------
            if(type.equals("12"))
            {
                scan_date = CDate.getDateyyyyMMdd();

                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select (select COUNT(0)" +
                        "from INV_TR t1 " +
                        "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE > " + date_previous + " AND T1.SENT_YN = 'Y' AND T1.STATUS ='000' " +
                        ") AS SEQ, T2.ITEM_BC, T2.ITEM_CODE, T2.TR_QTY, T2.TR_LOT_NO, T2.SLIP_NO, T2.TLG_POP_INV_TR_PK, T2.PK , T2.REMARKS " +
                        " FROM INV_TR t2 WHERE DEL_IF = 0 AND T2.TR_TYPE = '12' AND T2.SCAN_DATE > " + date_previous + " AND T2.SENT_YN = 'Y' AND T2.STATUS='000' ORDER BY pk desc ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();

                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count-i), scrollableColumnWidths[1], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_POP_INV_TR_PK")), 0, fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[5], fixedRowHeight,0));


                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
                                String st_slipNO=tvSlipNo.getText().toString();

                                if(st_slipNO.equals("-")){

                                    TextView tv1 = (TextView) tr1.getChildAt(7); //INV_TR_PK
                                    int xx = tr1.getChildCount();
                                    for (int i = 0; i < xx; i++) {
                                        tv11 = (TextView) tr1.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255,99,71));
                                    }

                                    if (queue.size() > 0) {
                                        if (queue.contains(tv1.getText().toString())) {
                                            queue.remove(tv1.getText().toString());

                                            for (int i = 0; i < xx; i++) {
                                                tv11 = (TextView) tr1.getChildAt(i);
                                                tv11.setTextColor(Color.BLACK );
                                            }

                                            duplicate = true;
                                        }
                                    }

                                    if (!duplicate)
                                        queue.add(tv1.getText().toString());
                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));
                                
                            }
                        });

                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '12' AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND STATUS='000'";
                cursor = db.rawQuery(sql, null);
                count = cursor.getCount();
                float _qty=0;
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        _qty=_qty+Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
                //---------------------------------------------------------------------

                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);
            }
            //endregion
            //region ----------Type 16 ----------
            if(type.equals("16")) {
                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select  PK, ITEM_BC, ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK,UNIT_PRICE,SUPPLIER_NAME,PO_NO,TR_WH_IN_NAME,LINE_NAME " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE = '"+type+"' AND STATUS IN('OK', '000') ORDER BY PK desc ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                float _qty=0;
                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[0], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PO_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));

                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));

                        _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));



                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
                //---------------------------------------------------------------------
                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);

            }
            //endregion

            //region -------Type 20---------
            if(type.equals("20"))
            {
                //btnMSlip.setVisibility(View.GONE);
                scan_date = CDate.getDateyyyyMMdd();

                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select (select COUNT(0)" +
                        "from INV_TR t1 " +
                        "where del_if = 0 and t1.pk <= t2.pk  AND T1.TR_TYPE = '20' AND T1.SCAN_DATE ='" + scan_date + "' AND T1.SENT_YN = 'Y' AND T1.STATUS ='000' " +
                        ") AS SEQ, T2.ITEM_BC, T2.ITEM_CODE, T2.TR_QTY, T2.TR_LOT_NO, T2.SLIP_NO, T2.TLG_POP_INV_TR_PK, T2.PK , T2.REMARKS " +
                        " FROM INV_TR t2 WHERE DEL_IF = 0 AND T2.TR_TYPE = '20' AND T2.SCAN_DATE ='"+ scan_date +"' AND T2.SENT_YN = 'Y' AND T2.STATUS='000' ORDER BY pk desc ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();

                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_POP_INV_TR_PK")), 0, fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[5], fixedRowHeight,-1));


                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
                                String st_slipNO=tvSlipNo.getText().toString();

                                if(st_slipNO.equals("-")){

                                    TextView tv1 = (TextView) tr1.getChildAt(7); //INV_TR_PK
                                    int xx = tr1.getChildCount();
                                    for (int i = 0; i < xx; i++) {
                                        tv11 = (TextView) tr1.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255,99,71));
                                    }

                                    if (queue.size() > 0) {
                                        if (queue.contains(tv1.getText().toString())) {
                                            queue.remove(tv1.getText().toString());

                                            for (int i = 0; i < xx; i++) {
                                                tv11 = (TextView) tr1.getChildAt(i);
                                                tv11.setTextColor(Color.BLACK );
                                            }

                                            duplicate = true;
                                        }
                                    }

                                    if (!duplicate)
                                        queue.add(tv1.getText().toString());
                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));
                                
                            }
                        });

                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '20' AND SCAN_DATE ='" + scan_date + "' AND SENT_YN = 'Y' AND STATUS='000'";
                cursor = db.rawQuery(sql, null);
                count = cursor.getCount();
                float _qty=0;
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        _qty=_qty+Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                        cursor.moveToNext();
                    }
                }
                cursor.close();

                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
                //---------------------------------------------------------------------

                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);
            }
            //endregion----------

            //region --------Type 24---------
            if(type.equals("24")) {
                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select  PK, ITEM_BC, ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK,UNIT_PRICE,SUPPLIER_NAME,PO_NO,TR_WH_IN_NAME " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE = '24' AND STATUS IN('OK', '000') and ITEM_BC IS NOT NULL ORDER BY PK ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UNIT_PRICE")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PO_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,-1));

                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(6); //SLIP_NO
                                String st_slipNO = tvSlipNo.getText().toString();
                                if (st_slipNO.equals("-")) {
                                    TextView tv1 = (TextView) tr1.getChildAt(5); //TR_WH_IN_NAME
                                    if (wh_name_grid.equals("")) {
                                        wh_name_grid = tv1.getText().toString();
                                    }

                                    if (tv1.getText().toString().equals(wh_name_grid)) {
                                        TextView tv2 = (TextView) tr1.getChildAt(9); //TLG_POP_INV_TR_PK
                                        int xx = tr1.getChildCount();

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.rgb(255,99,71));
                                        }
                                        String value = tv2.getText().toString();
                                        if (queue.size() > 0) {
                                            if (queue.contains(value)) {
                                                queue.remove(value);

                                                for (int i = 0; i < xx; i++) {
                                                    tv11 = (TextView) tr1.getChildAt(i);
                                                    tv11.setTextColor(Color.BLACK);
                                                }
                                                duplicate = true;
                                            }
                                        }

                                        if (!duplicate)
                                            queue.add(value);
                                    } else {
                                        gwMActivity.alertToastShort("WH is different");
                                    }
                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));

                            }
                        });

                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
            }
            //endregion

            //region --------Type 5--------------
            if(type.equals("25"))
            {
                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                //db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select  PK, ITEM_BC, ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK,UNIT_PRICE,SUPPLIER_NAME,PO_NO,TR_WH_OUT_NAME " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE = '25' AND STATUS IN('OK', '000') ORDER BY PK ";

                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                float _qty = 0;
                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UNIT_PRICE")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PO_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));

                        _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));

                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tv11;
                            boolean duplicate = false;

                            @Override
                            public void onClick(View v) {

                                duplicate = false;
                                TableRow tr1 = (TableRow) v;
                                TextView tvSlipNo = (TextView) tr1.getChildAt(6); //SLIP_NO
                                String st_slipNO=tvSlipNo.getText().toString();
                                if(st_slipNO.equals("-")) {
                                    TextView tv1 = (TextView) tr1.getChildAt(5); //TR_WH_IN_NAME
                                    if(wh_name_grid.equals("")){
                                        wh_name_grid=tv1.getText().toString();
                                    }
                                    if(tv1.getText().toString().equals(wh_name_grid)) {
                                        TextView tv2 = (TextView) tr1.getChildAt(9); //TLG_POP_INV_TR_PK
                                        int xx = tr1.getChildCount();

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.rgb(255,99,71));
                                        }
                                        String value =tv2.getText().toString();
                                        if (queue.size() > 0) {
                                            if (queue.contains(value)) {
                                                queue.remove(value);

                                                for (int i = 0; i < xx; i++) {
                                                    tv11 = (TextView) tr1.getChildAt(i);
                                                    tv11.setTextColor(Color.BLACK);
                                                }
                                                duplicate = true;
                                            }
                                        }
                                        if (!duplicate)
                                            queue.add(value);
                                    }
                                    else{
                                        gwMActivity.alertToastShort("WH is different");
                                    }
                                }
                                gwMActivity.alertToastShort(String.valueOf(queue.size()));
                            }
                        });
                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
                //---------------------------------------------------------------------

                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);
            }

        } catch (Exception ex) {
            //Toast.makeText(gwMActivity,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            Log.e("OnShowScanAccept Error:", ex.getMessage());
        } finally {
            db.close();
            //cursor.close();
        }
    }




    //----------------------------------------------------------
    ///Load Header grid
    //----------------------------------------------------------
    public void OnShowGridHeaderIncome()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    public void OnShowHeaderGoodsDelivery()
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remark", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    public void OnShowHeaderExchangeProduct()
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remark", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    public void OnShowGridHeaderGoodsDeliNoReq()
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remark", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    public void OnShowGridHeaderInInventory()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Unit Price", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("PO NO", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    public void OnShowGridHeaderOutInventory()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Unit Price", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("PO NO", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    public void OnShowGridHeaderStockTransfer()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;
        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH.Out", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH.In", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Po No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }
    public void OnShowGridHeaderStockReturn()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;
        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Line Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Po No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }
    public void OnShowGridHeaderStockOthers()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;
        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Line Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Po No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }
    public void OnShowGridHeaderGoodsReturn()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;
        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Customer", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Po No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }
    public void OnShowHeaderGoodsPartial()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;
        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Parent Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Child Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remain Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Tr_Deli Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remark", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }



    ////////////////////////////////////////////////
    //POPUP Alert
    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str=str_alert;
        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                //Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnGoodsPartial:
                //onGoodsPartial(view);
                break;

            case R.id.btnDeleteBC:
                //onClickDeleteBC(view);
                break;

            case R.id.btnMakeSlipBot:
                onClickMakeSlipBot(view);
                break;
            default:
                break;
        }
    }
}
