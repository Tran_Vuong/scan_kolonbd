package gw.genumobile.com.interfaces;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;
import android.widget.AdapterView.OnItemSelectedListener;

import gw.genumobile.com.R;
import gw.genumobile.com.models.gwform_info;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.views.gwFragmentDialog;
import gw.genumobile.com.views.gwcore.BaseGwActive;


public class GridListViewPreparation extends gwFragmentDialog implements View.OnClickListener {

    Queue queue = new LinkedList();
    Queue dlgqueue = new LinkedList();
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor,cursor2;
    String  data[] = new String[0];

    Calendar c;
    SimpleDateFormat df;

    TextView txtQty, txtTotal,_txtHeader,recyclableTextView;

    public Spinner spReqPK;

    String type,sql = "", scan_date = "",unique_id="",value = "";
    String user="",user_id="",pk_user="",parentBC="", reqPK = "";
    float total_qty=0;
    boolean flagEvenSP = false;
    String approveYN = "N";
    public static final int tes_role_pk = 2442;//Role Approve "POP Tablet"

    TextView msg_error;
    HashMap<String, String> hMapRequestPK;
    View rootView;
    Button btnMakeSlipBot,btnGoodsPartial;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView =  inflater.inflate(R.layout.activity_grid_list_view_preparation, container, false);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                    Intent i = new Intent().putExtra("month", "trang");
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                    dismiss();
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);
        toolbar.setTitle("LIST ITEM BARCODE");
        //formID  = getArguments().getString("form_id");
        type = getArguments().getString("type");
        reqPK = getArguments().getString("reqPK");
        //lst_ReqPK = getIntent().getStringArrayListExtra("lst_ReqPK");

        hMapRequestPK = (HashMap<String, String>)getArguments().getSerializable("hMapRequestPK");

        //set header
        btnGoodsPartial=(Button)rootView.findViewById(R.id.btnGoodsPartial);
        btnGoodsPartial.setOnClickListener(this);
        btnMakeSlipBot=(Button)rootView.findViewById(R.id.btnMakeSlipBot);
        btnMakeSlipBot.setOnClickListener(this);

        _txtHeader=(TextView) rootView.findViewById(R.id.txtHeader);
        if(Integer.valueOf(type)== gwform_info.Preparation.Id())
        {
            toolbar.setTitle("LIST ACCEPT ITEM PREPARATION");
            btnGoodsPartial.setVisibility(View.GONE);
            OnShowHeaderGoodsDelivery();
        }


        Collection<String> vals = hMapRequestPK.values();
        String[] array_ReqPK = vals.toArray(new String[vals.size()]);
        spReqPK      = (Spinner) rootView.findViewById(R.id.spReqPK);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwActivity, android.R.layout.simple_spinner_item, array_ReqPK);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spReqPK.setAdapter(dataAdapter);

        if(reqPK.length()>0){
            value = hMapRequestPK.get(reqPK);
            for(int i = 0; i< hMapRequestPK.size(); i++){
                if(spReqPK.getItemAtPosition(i).toString().equalsIgnoreCase(value)){
                    spReqPK.setSelection(i);
                    OnShowScanAccept();
                    break;
                }
            }
        }else{
            spReqPK.setSelection(0);
            reqPK = "00";
            OnShowScanAccept();
        }

        spReqPK.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                try{
                    //if(flagEvenSP){
                    value = spReqPK.getItemAtPosition(i).toString();
                    reqPK =  getKeyFromValue(hMapRequestPK, value);
                    // alertToast("Click Request Pk: " + reqPK_);
                    OnShowScanAccept();
                    //}else{
                    //    flagEvenSP = true;
                    //}
                }catch (Exception e){
                    Log.e("Get Key Spinner:", e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        scan_date = CDate.getDateyyyyMMdd();
        //Toast.makeText(gwMActivity,type, Toast.LENGTH_SHORT).show();

        OnShowScanAccept();

        return rootView;
    }

    public static String getKeyFromValue(HashMap<String, String> hm, String value) {
        for (String o : hm.keySet()) {
            if (hm.get(o).equals(value)) {
                return o;
            }
        }
        return null;
    }



    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }


    public void onGoodsPartial(View view){
        if(!parentBC.equals("")){
            dlgqueue = new LinkedList();//reset
            //popup
            final Dialog dlg_Partial = new Dialog(gwMActivity);
            dlg_Partial.setContentView(R.layout.dialog_parent_partial);
            dlg_Partial.setCancelable(false);//not close when touch
            dlg_Partial.setTitle(" Config Setting");
            ///////
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart;
            scrollablePart = (TableLayout) dlg_Partial.findViewById(R.id.grd_dlg_Header);
            row = new TableRow(gwMActivity);
            row.setLayoutParams(wrapWrapTableRowParams);
            row.setGravity(Gravity.CENTER);
            row.setBackgroundColor(bsColors[1]);
            row.setVerticalGravity(50);

            row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
            row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[4], fixedHeaderHeight));
            scrollablePart.addView(row);
            /////////////////////////////////////////////////////////////////
            try {
                String l_para = "1,LG_MPOS_GET_CHILDBC," + parentBC;
                ConnectThread networkThread = new ConnectThread(gwMActivity);

                String result[][] = networkThread.execute(l_para).get();

                TableLayout scrollablePart1 = (TableLayout) dlg_Partial.findViewById(R.id.grd_dlg_Content);
                scrollablePart1.removeAllViews();//remove all view child

                for (int i = 0; i < result.length; i++) {
                    TableRow row1 = new TableRow(gwMActivity);
                    row1.setLayoutParams(wrapWrapTableRowParams);
                    row1.setGravity(Gravity.CENTER);
                    row1.setBackgroundColor(Color.LTGRAY);
                    dlgqueue.add(result[i][0].toString());
                    row1.addView(makeTableColWithText(result[i][0].toString(),  scrollableColumnWidths[1], fixedRowHeight,-1));
                    row1.addView(makeTableColWithText(result[i][1].toString(), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row1.addView(makeTableColWithText(result[i][2].toString(), scrollableColumnWidths[4], fixedRowHeight,-1));
                    total_qty=total_qty +  Float.valueOf(result[i][2].toString());//Qty
                    row1.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            duplicate = false;
                            TableRow rw = (TableRow) v;
                            //////
                            TextView tv1 = (TextView) rw.getChildAt(0); //INV_TR_PK
                            TextView tvQty = (TextView) rw.getChildAt(2); //INV_TR_PK
                            int r = rw.getChildCount();
                            for (int i = 0; i < r; i++) {
                                tv11 = (TextView) rw.getChildAt(i);
                                tv11.setTextColor(Color.BLACK);
                            }

                            if (dlgqueue.size() > 0) {
                                if (dlgqueue.contains(tv1.getText().toString())) {
                                    dlgqueue.remove(tv1.getText().toString());
                                    total_qty = total_qty - Float.valueOf(tvQty.getText().toString());
                                    for (int i = 0; i < r; i++) {
                                        tv11 = (TextView) rw.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255, 99, 71));
                                    }
                                    duplicate = true;
                                }
                            }

                            if (!duplicate) {
                                dlgqueue.add(tv1.getText().toString());
                                total_qty = total_qty + Float.valueOf(tvQty.getText().toString());
                            }
                            // Toast.makeText(getApplicationContext(), "Size dlgqueue " + dlgqueue.size() + "", Toast.LENGTH_SHORT).show();
                        }
                    });
                    scrollablePart1.addView(row1);
                }
                //Toast.makeText(getApplicationContext(),"Size dlgqueue "+ dlgqueue.size() + "", Toast.LENGTH_SHORT).show();
            } catch (Exception ex) {
                Toast.makeText(gwMActivity, "Error get Child BC: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            }
            /////////////////////////////////////////////////////////////////
            Button btnSave = (Button) dlg_Partial.findViewById(R.id.btnSave);
            Button btnCancel = (Button) dlg_Partial.findViewById(R.id.btnCancel);

            // Attached listener for login GUI button
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Object[] lst = dlgqueue.toArray();
                        if(lst.length > 0) {
                            String pk_child="";
                            for(int i=0;i<lst.length;i++)
                            {
                                if(pk_child.length() <= 0)
                                {
                                    pk_child += lst[i];
                                }
                                else
                                {
                                    pk_child += ";" + lst[i];
                                }
                            }
                            //onPopAlert(pk_child);
                            sql = "UPDATE INV_TR set TR_DELI_QTY=" + total_qty + ",ITEM_BC_CHILD='"+ pk_child + "'  where ITEM_BC = " + parentBC;
                            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                            db.execSQL(sql);
                        }
                    } catch (Exception ex) {
                        gwMActivity.alertToastLong(ex.getMessage().toString());
                        Log.e("Save Qty error:", ex.getMessage());
                    } finally {
                        parentBC="";
                        total_qty = 0;
                        db.close();
                        OnShowScanAccept();
                        dlg_Partial.dismiss();
                    }
                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    parentBC="";
                    total_qty = 0;
                    OnShowScanAccept();
                    dlg_Partial.dismiss();
                }
            });
            dlg_Partial.show();
        }else{
            onPopAlert("Please select Parent BC first!!!");
        }
    }

    public void ProcessMakeSlipDelivery()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WH_OUT_PK,TLG_GD_REQ_M_PK,REMARKS   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='6' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {

                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|"+unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                        para += "*|*";
                    }
                }
                para += "|!LG_MPOS_PRO_GD_MAKESLIP";
                data[j++]=para;
                //System.out.print("\n\n ******para make slip goods delivery: "+para);

            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WH_OUT_PK,TLG_GD_REQ_M_PK,REMARKS   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='6' ",null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para = "";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                        para +="*|*";

                    }while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_GD_MAKESLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip goods delivery: " + para);
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipInInventory()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WH_OUT_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='7' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //para += "|!"+USER;
                        para += "|" + unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                    }
                    para += "*|*";
                }
                para += "|!LG_MPOS_PRO_ST_INC_MAKESLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip goods delivery: "+para);
            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WH_OUT_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='7' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                        para += "*|*";
                    }while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_ST_INC_MAKESLIP";
                    System.out.print("\n\n ******para make slip in stock: " + para);
                    data[j++] = para;
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipOutInventory()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();
        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WH_OUT_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='5' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {
                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //para += "|!"+USER;
                        para += "|" + unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                        para += "*|*";
                    }
                }
                para += "|!LG_MPOS_PRO_ST_OUT_MAKESLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip stock out: "+para);
            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WH_OUT_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='5' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                        para +="*|*";
                    }while (cursor.moveToNext());
                    para += "|!"+"LG_MPOS_PRO_ST_OUT_MAKESLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip stock out: " + para);
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipGoodsDeliNoReq()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WH_OUT_PK,REMARKS   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='8' and pk="+myLst[i],null);
                    if (cursor.moveToFirst()) {
                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|"+unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                    }
                    para += "*|*";
                }
                para += "|!"+"LG_MPOS_PRO_GD_NOREQ_MAKESLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip goods delivery: "+para);

            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WH_OUT_PK,REMARKS   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='8' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                        para += "*|*";
                    }while (cursor.moveToNext());

                    para += "|!"+"LG_MPOS_PRO_GD_NOREQ_MAKESLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip goods delivery: " + para);
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipStockTransfer()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_CODE,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, SCAN_DATE, TR_WH_OUT_PK,TR_LINE_PK, UNIT_PRICE   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='9' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //para += "|!"+USER;
                        para += "|" + unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;

                    }
                    para += "*|*";
                }
                para += "|!"+"LG_MPOS_PRO_ST_TRANSFER_MSLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip stock out: "+para);

            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_CODE,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, SCAN_DATE, TR_WH_OUT_PK,TR_LINE_PK, UNIT_PRICE   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='9' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                        para += "*|*";
                    }while (cursor.moveToNext());

                    para += "|!"+"LG_MPOS_PRO_ST_TRANSFER_MSLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip stock out: " + para);
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
    }

    public void ProcessMakeSlipGoodsPartial()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();
        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_DELI_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WH_OUT_PK,TLG_GD_REQ_M_PK,REMARKS,ITEM_BC_CHILD   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='11' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|"+unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                        para += "*|*";

                    }
                }
                para += "|!LG_MPOS_PRO_GD_PARTIAL_MSLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip goods delivery: " + para);
                //onPopAlert(para);
            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
                onPopAlert(ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_DELI_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WH_OUT_PK,TLG_GD_REQ_M_PK,REMARKS,ITEM_BC_CHILD   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='11' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para = "";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;

                    }while (cursor.moveToNext());
                    para += "|!"+"LG_MPOS_PRO_GD_PARTIAL_MSLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip goods delivery: " + para);
                }
            }
            catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        }
    }
    //----------------------------------------------------------
    ////show data scan accept Delivery
    //----------------------------------------------------------
    public void OnShowScanAccept()
    {

        Button btnMSlip=(Button)rootView.findViewById(R.id.btnMakeSlipBot);
        btnMSlip.setVisibility(View.VISIBLE);
        int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
        try {
            if(Integer.valueOf(type) == gwform_info.Preparation.Id())
            {
                scan_date = CDate.getDateyyyyMMdd();

                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
                scrollablePart.removeAllViews();//remove all view child

                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                if(reqPK.equalsIgnoreCase("00")){
                    sql = "select (select COUNT(0)" +
                            "from INV_TR t1 " +
                            "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE > " + date_previous + " AND T1.SENT_YN = 'Y' AND T1.STATUS ='000' " +
                            ") AS SEQ, T2.ITEM_BC, T2.ITEM_CODE, T2.TR_QTY, T2.TR_LOT_NO, T2.SLIP_NO, T2.TLG_POP_INV_TR_PK, T2.PK , T2.REMARKS " +
                            " FROM INV_TR t2 " +
                            " WHERE DEL_IF = 0 AND T2.TR_TYPE = '"+ gwform_info.Preparation.Id()+"' AND T2.SCAN_DATE > " + date_previous + " " +
                            " AND T2.SENT_YN = 'Y' AND T2.STATUS='000' " +
                            " ORDER BY pk desc ";
                }else{
                    sql = "select (select COUNT(0)" +
                            "from INV_TR t1 " +
                            "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE > " + date_previous + " AND T1.SENT_YN = 'Y' AND T1.STATUS ='000' " +
                            ") AS SEQ, T2.ITEM_BC, T2.ITEM_CODE, T2.TR_QTY, T2.TR_LOT_NO, T2.SLIP_NO, T2.TLG_POP_INV_TR_PK, T2.PK , T2.REMARKS " +
                            " FROM INV_TR t2 " +
                            " WHERE DEL_IF = 0 AND T2.TR_TYPE = '"+ gwform_info.Preparation.Id()+"' AND T2.SCAN_DATE > " + date_previous + " " +
                            " AND T2.SENT_YN = 'Y' AND T2.STATUS='000' " +
                            " AND TLG_GD_REQ_M_PK = '" + reqPK + "'" +
                            " ORDER BY pk desc ";
                }


                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();

                queue.clear();
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        row = new TableRow(gwMActivity);
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);
                        row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_POP_INV_TR_PK")), 0, fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,-1));
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[5], fixedRowHeight,-1));

                        row.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                onClickGrid(v);

                            }
                        });

                        scrollablePart.addView(row);
                        cursor.moveToNext();
                    }
                }
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                if(reqPK.equalsIgnoreCase("00")){
                    sql = "select pk,TR_QTY FROM INV_TR  " +
                            " WHERE DEL_IF = 0 AND TR_TYPE = '"+ gwform_info.Preparation.Id()+"' AND SCAN_DATE > " + date_previous +
                            " AND SENT_YN = 'Y' AND STATUS='000'";
                }else{
                    sql = "select pk,TR_QTY FROM INV_TR  " +
                            " WHERE DEL_IF = 0 AND TR_TYPE = '"+ gwform_info.Preparation.Id()+"' AND SCAN_DATE > " + date_previous +
                            " AND SENT_YN = 'Y' AND STATUS='000'" +
                            " AND TLG_GD_REQ_M_PK = '" + reqPK + "'";
                }

                cursor = db.rawQuery(sql, null);
                count = cursor.getCount();
                float _qty=0;
                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {
                        _qty=_qty+Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                        cursor.moveToNext();
                    }
                }

                txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
                txtTotal.setText("Total: " + count + " ");
                txtTotal.setTextColor(Color.BLUE);
                txtTotal.setTextSize((float) 18.0);
                //---------------------------------------------------------------------

                txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
                txtQty.setText("Qty: " + _qty + " ");
                txtQty.setTextColor(Color.MAGENTA);
                txtQty.setTextSize((float) 18.0);
            }

        } catch (Exception ex) {
            //Toast.makeText(gwMActivity,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            Log.e("OnShowScanAccept Error:", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
    public void onClickGrid(View v){
        TextView tv11;
        boolean duplicate = false;
        duplicate = false;
        TableRow tr1 = (TableRow) v;
        TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
        String st_slipNO=tvSlipNo.getText().toString();
        if(st_slipNO==null || st_slipNO.equals("") || st_slipNO.equals("-")){

            TextView tv1 = (TextView) tr1.getChildAt(7); //INV_TR_PK
            int xx = tr1.getChildCount();
            for (int i = 0; i < xx; i++) {
                tv11 = (TextView) tr1.getChildAt(i);
                tv11.setTextColor(Color.rgb(255,99,71));
            }

            if (queue.size() > 0) {
                if (queue.contains(tv1.getText().toString())) {
                    queue.remove(tv1.getText().toString());

                    for (int i = 0; i < xx; i++) {
                        tv11 = (TextView) tr1.getChildAt(i);
                        tv11.setTextColor(Color.BLACK );
                    }

                    duplicate = true;
                }
            }

            if (!duplicate)
                queue.add(tv1.getText().toString());
        }
        gwMActivity.alertToastShort(String.valueOf(queue.size()));
    }

    public void OnShowScanAcceptIncomeProd()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_content);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select  PK, ITEM_BC, ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + scan_date + " AND SENT_YN = 'Y' AND TR_TYPE = '4' AND STATUS IN('OK', '000') ORDER BY PK desc ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            int _qty=0;
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count-i), scrollableColumnWidths[1], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));

                    _qty=_qty+Integer.parseInt(cursor.getString(cursor.getColumnIndex("TR_QTY")));

                    row.setOnClickListener(new View.OnClickListener() {


                        @Override
                        public void onClick(View v) {
                            onShowClickGridBC(v);

                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            txtTotal = (TextView) rootView.findViewById(R.id.txtTotalLabel);
            txtTotal.setText("Total: " + count + " ");
            txtTotal.setTextColor(Color.BLUE);
            txtTotal.setTextSize((float) 18.0);
            //---------------------------------------------------------------------

            txtQty = (TextView) rootView.findViewById(R.id.txtTotalQty);
            txtQty.setText("Qty: " + _qty + " ");
            txtQty.setTextColor(Color.MAGENTA);
            txtQty.setTextSize((float) 18.0);

        } catch (Exception ex) {
            Toast.makeText(gwMActivity,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
            cursor.close();
        }
    }
    public void onShowClickGridBC(View v){
        TextView tv11;
        boolean duplicate = false;
        TableRow tr1 = (TableRow) v;
        TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
        String st_slipNO=tvSlipNo.getText().toString();
        if(st_slipNO==null || st_slipNO.equals("") || st_slipNO.equals("-")){
            TextView tv1 = (TextView) tr1.getChildAt(6); //TLG_POP_INV_TR_PK
            int xx = tr1.getChildCount();

            for (int i = 0; i < xx; i++) {
                tv11 = (TextView) tr1.getChildAt(i);
                tv11.setTextColor(Color.YELLOW);
            }

            if (queue.size() > 0) {
                if (queue.contains(tv1.getText().toString())) {
                    queue.remove(tv1.getText().toString());

                    for (int i = 0; i < xx; i++) {
                        tv11 = (TextView) tr1.getChildAt(i);
                        tv11.setTextColor(Color.BLACK);
                    }
                    duplicate = true;
                }
            }

            if (!duplicate)
                queue.add(tv1.getText().toString());

        }
        gwMActivity.alertToastShort(String.valueOf(queue.size()));



    }
    //----------------------------------------------------------
    ///Load Header grid
    //----------------------------------------------------------
    public void OnShowGridHeaderIncome()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    public void OnShowHeaderGoodsDelivery()
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remark", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    public void OnShowGridHeaderGoodsDeliNoReq()
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remark", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    public void OnShowGridHeaderInInventory()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Unit Price", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("PO NO", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    public void OnShowGridHeaderOutInventory()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Unit Price", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("PO NO", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    public void OnShowGridHeaderStockTransfer()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;
        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH.Out", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH.In", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Po No", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    public void OnShowHeaderGoodsPartial()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;
        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Parent Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Child Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remain Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Tr_Deli Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remark", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }





    ////////////////////////////////////////////////
    //POPUP Alert
    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str=str_alert;
        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                //Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void ProcessMakeSlip() {
        unique_id = CDate.getDateyyyyMMddhhmmss();
        int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
        if (queue.size() > 0) {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            return;
        } else {
            try {
                ///// TODO: 2016-01-14 change sql
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                sql =       " SELECT TR_ITEM_PK, TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,TR_QTY, UOM,TR_LOT_NO,TR_WH_OUT_PK,TLG_GD_REQ_D_PK,EXECKEY " +
                        " FROM " +
                        "       (   " +
                        "           SELECT TR_ITEM_PK,  SUM(TR_QTY) TR_QTY, UOM,  TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,TR_LOT_NO,TR_WH_OUT_PK,TLG_GD_REQ_D_PK,EXECKEY " +
                        "           FROM INV_TR " +
                        "           WHERE DEL_IF = 0 AND TLG_GD_REQ_M_PK = '" + reqPK+"' AND  TR_TYPE = '"+gwform_info.Preparation.Id()+"'  AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND STATUS='000' AND SLIP_NO IN('-','') " +
                        "           GROUP BY   TR_ITEM_PK, UOM, TLG_GD_REQ_M_PK, TLG_SA_SALEORDER_D_PK, TR_LOT_NO,TR_WH_OUT_PK,TLG_GD_REQ_D_PK, EXECKEY" +
                        "       )";
                cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount()-1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey=cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if( execkey==null || execkey.equals("") ) {
                            int ex_item_pk=Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            int ex_req_m_pk=Integer.valueOf(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK")));
                            String ex_lot_no=cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            int ex_sale_order_d_pk=Integer.valueOf(cursor.getString(cursor.getColumnIndex("TLG_SA_SALEORDER_D_PK")));
                            hp.updateExeckeyGoodsDeli(type,ex_item_pk,ex_req_m_pk,ex_sale_order_d_pk,ex_lot_no,unique_id);

                            para += "|" + unique_id;
                        }else {
                            para += "|" + execkey;
                        }
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";

                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_PREPA_MSLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip preparation: " + para);

                    if (CNetwork.isInternet(tmpIP,checkIP)) {
                        // asyns server
                        MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                        task.execute(data);
                    } else
                    {
                        gwMActivity.alertToastLong("Network is broken. Please check network again !");
                    }
                }
            } catch (Exception ex) {
                gwMActivity.alertToastShort("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }

        }
    }
    //****************************************************************************
    public List<String> GetDataOnServer(String type, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";
            if (type.endsWith("SlipNo")) //WH
            {
                l_para = "1,LG_MPOS_GET_GD_PRE_REQ," + para;
            }
            if (type.endsWith("ROLE")) {
                l_para = "1,LG_MPOS_GET_ROLE," + para;
            }

            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                if (type.endsWith("SlipNo")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                }
                if (type.endsWith("ROLE")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                }
            }
        } catch (Exception ex) {
            gwMActivity.alertToastShort(type + ": " + ex.getMessage());
        }
        return labels;
    }

    /*public void OnPermissionApprove() {
        String para = pk_user + '|' + String.valueOf(tes_role_pk);
        List<String> arr_role = GetDataOnServer("ROLE", para);
        if (arr_role.size() > 0) {
            approveYN = arr_role.get(0);
            Log.e("approveYN", approveYN);
        }

        Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlipBot);
        btnMSlip.setVisibility(View.VISIBLE);
    }*/

    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event

                if (_type.equals("onMakeSlip")) {
                    //Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlipBot);
                    //btnMSlip.setVisibility(View.GONE);
                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    public void onClickMakeSlipBot(View view)  {
        boolean flag = true;
        if (!flag) {
            msg_error = (TextView) rootView.findViewById(R.id.txtError);
            msg_error.setText("Please, select Item.");
            msg_error.setTextColor(Color.RED);
            msg_error.setTextSize((float) 16.0);
            return;
        } else {
            String title = "Confirm Make Slip...";
            String mess = "Are you sure you want to Make Slip";
            alertDialogYN(title, mess, "onMakeSlip");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnGoodsPartial:
                //onGoodsPartial(view);
                break;

            case R.id.btnDeleteBC:
                //onClickDeleteBC(view);
                break;

            case R.id.btnMakeSlipBot:
                onClickMakeSlipBot(view);
                break;
            default:
                break;
        }
    }
}
