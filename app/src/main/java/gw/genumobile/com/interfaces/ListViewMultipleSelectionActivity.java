package gw.genumobile.com.interfaces;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.ListItem;
import gw.genumobile.com.utils.ViewAdapter;

public class ListViewMultipleSelectionActivity extends ListActivity  {
    Button btnSave;
    ListView listViewVietnamese;
    ArrayAdapter<String> adapter;

    SQLiteDatabase db = null;
    Cursor cursor;
    String sql = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_multiple_selection);

    }

    private void findViewsById() {
        try {

        /*
        int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
        listView = (ListView) findViewById(R.id.lstItemBC );
        btnSave = (Button) findViewById(R.id.onSave);

        db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
        sql = "select  PK, ITEM_BC,  TR_QTY  FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE IN ( '4') AND STATUS IN('OK', '000') AND SLIP_NO='-' ORDER BY PK desc LIMIT 20 ";
        cursor = db.rawQuery(sql, null);
        int count = cursor.getCount();

        //String[] sports = new String[]{"21511100001","21511100002","C","D","E","F","G"};
        String[] sports = new String[count];
        if (cursor != null && cursor.moveToFirst()) {
            for (int i = 0; i < count; i++) {
                sports[i] = cursor.getString(cursor.getColumnIndex("ITEM_BC")) +  "\t \t" + cursor.getString(cursor.getColumnIndex("TR_QTY"));
            }
        }

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, sports);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(adapter);
        */

            int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            sql = "select  PK, ITEM_BC, ITEM_CODE,  TR_QTY  FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE IN ( '4') AND STATUS IN('OK', '000') AND SLIP_NO='-' ORDER BY PK desc LIMIT 20 ";
            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            //tạo mảng động chứa các phần tử là ListItem
            ArrayList<ListItem> celebrities = new ArrayList<ListItem>();

            String[] sports = new String[count];
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    //thêm các phần tử vào mảng động celebrities
                    celebrities.add(new ListItem(cursor.getString(cursor.getColumnIndex("ITEM_BC")), R.drawable.br_main_new, cursor.getString(cursor.getColumnIndex("ITEM_CODE")), cursor.getString(cursor.getColumnIndex("TR_QTY"))));
                    cursor.moveToNext();
                }
            }
            //lấy về ListView đã đặt trên activity_vietnamese.xml
            listViewVietnamese = (ListView) findViewById(R.id.lstItemBC1);
            listViewVietnamese.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            //tạo một đối tượng adapter mới tạo
            ViewAdapter newAdapter = new ViewAdapter(this, listViewVietnamese.getId(), celebrities);

            //sử dụng newAdapter cho listViewVietnamese
            listViewVietnamese.setAdapter(newAdapter);
            listViewVietnamese.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String strName = (String)getListAdapter().getItem(position);
                    //Toast.makeText(this, strName, Toast.LENGTH_LONG).show();
                }
            });

        }catch (Exception ex)
        {
            Toast.makeText(this, "Error  : " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void onEditQty(View view){
        try {
            int cntChoice = listViewVietnamese.getCheckedItemCount();

            SparseBooleanArray checked = listViewVietnamese.getCheckedItemPositions();
            ArrayList<String> selectedItems = new ArrayList<String>();
            for (int i = 0; i < checked.size(); i++) {
                // Item position in adapter
                int position = checked.keyAt(i);
                // Add sport if it is checked i.e.) == TRUE!
                if (checked.valueAt(i))
                    selectedItems.add(adapter.getItem(position));
            }

            String[] outputStrArr = new String[selectedItems.size()];

            for (int i = 0; i < selectedItems.size(); i++) {
                outputStrArr[i] = selectedItems.get(i);
                Toast.makeText(this,selectedItems.get(i), Toast.LENGTH_LONG );
            }

        }catch (Exception ex)
        {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG );
        }
    }
}
