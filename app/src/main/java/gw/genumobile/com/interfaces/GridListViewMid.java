package gw.genumobile.com.interfaces;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.models.Config;
import gw.genumobile.com.services.ServerAsyncTask;
import gw.genumobile.com.views.gwcore.BaseGwActive;


public class GridListViewMid extends BaseGwActive {
    //private int[] colors = new int[] { 0x30FF0000, 0x300000FF,0xCCFFFF,0x99CC33,0x00FF99 };
    SQLiteDatabase db=null,db2=null;
    Cursor cursor,cursor2;
    private Handler customHandler = new Handler();
    String  data[] = new String[0];

    Queue queueMid = new LinkedList();

    Calendar c;
    SimpleDateFormat df;

    TextView txt_error, txtTotalGridMid,txtHeader,recyclableTextView;
    Button _btnSelect;
    String type,sql = "", scan_date = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_list_view_mid);
        //lock screen
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Intent intentObject = getIntent();
        //String userName = intentObject.getStringExtra("UserName");
        type = getIntent().getStringExtra("type");
        //Toast.makeText(this,type, Toast.LENGTH_SHORT).show();
        txtHeader=(TextView) findViewById(R.id.txtHeader);

        _btnSelect=(Button) findViewById(R.id.btnSelect);

        if(type.equals("4"))
        {
            txtHeader.setText("LIST ERROR ITEM INCOMING PROD");
            _btnSelect.setVisibility((View.GONE));
            OnShowGridHeader();
            OnShowScanIncomeProd();
        }
        if(type.equals("5"))
        {
            _btnSelect.setVisibility((View.GONE));
            txtHeader.setText("LIST ERROR ITEM STOCK OUT ");
            OnShowGridHeader();
            OnShowScanInOutInventory();
        }
        if(type.equals("6"))
        {
            txtHeader.setText("LIST ERROR ITEM GOODS DELIVERY");
            OnShowGridHeader();
            OnShowScanIn();
        }
        if(type.equals("12"))
        {
            txtHeader.setText("LIST ERROR ITEM PREPARATION");
            OnShowGridHeader();
            OnShowScanIn();
        }
        if(type.equals("7"))
        {
            txtHeader.setText("LIST ERROR ITEM STOCK IN");
            _btnSelect.setVisibility((View.GONE));
            OnShowGridHeader();
            OnShowScanInInventory();
        }
        if(type.equals("8"))
        {
            txtHeader.setText("LIST ERROR ITEM GOODS DELIVERY WITHOUT REQ");
            _btnSelect.setVisibility((View.GONE));
            OnShowGridHeaderNoReq();
            OnShowScanInGoodsDeliNoReq();
        }
        if(type.equals("9"))
        {
            txtHeader.setText("LIST ERROR ITEM STOCK TRANSFER");
            _btnSelect.setVisibility((View.GONE));
            OnShowGridHeader();
            OnShowScanInStockTransfer();
        }
        if(type.equals("11"))
        {
            txtHeader.setText("LIST ERROR ITEM GOODS PARTIAL");
            _btnSelect.setVisibility((View.GONE));
            OnShowGridHeader();
            OnShowScanInGoodsPartial();
        }
        if(type.equals("18"))
        {
            _btnSelect.setVisibility((View.GONE));
            txtHeader.setText("LIST ERROR ITEM PRODUCT OUT ");
            OnShowGridHeader();
            OnShowScanInOutInventory();
        }
        if(type.equals("20"))
        {
            _btnSelect.setVisibility(View.INVISIBLE);
            txtHeader.setText("LIST ERROR ITEM EXCHANGE PRODUCT");
            OnShowGridHeader();
            OnShowScanInExchangeProd();
        }
        scan_date = CDate.getDateyyyyMMdd();

    }

    public void onClickSelectItem(View view) throws InterruptedException,ExecutionException {

        String str="Are you sure you want to select ";

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Select ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);

        // Setting Negative "YES" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                // Write your code here to invoke YES event
                try{
                    Object[] myLst = queueMid.toArray();
                    queueMid = new LinkedList();
                    db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
                    for (int i = 0; i < myLst.length; i++) {
                        String val=myLst[i].toString();
                        String _pk=val.split("\\|")[0].toString();
                        String _status=val.split("\\|")[1].toString();
                        if(_status.equals("005")) {
                            sql="UPDATE INV_TR set  REMARKS='Not FIFO',SLIP_NO='-'  where PK = " + _pk;
                            db.execSQL(sql);

                            ProcessStatus005_008(_pk);
                        }
                        if(_status.equals("008"))
                        {
                            sql="UPDATE INV_TR set STATUS = '000', REMARKS='Qty larger'  where PK = " + _pk;
                            db.execSQL(sql);

                            OnShowScanIn();
                            //OnShowScanAccept();
                        }
                    }
                }catch (Exception ex){
                    Toast.makeText(getApplicationContext(), "Select Mid :" + ex.getMessage() , Toast.LENGTH_LONG).show();
                }
                finally {
                    db.close();
                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }

    public void ProcessStatus005_008(String _pk){
        db2=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
        sql="select PK, ITEM_BC,GD_SLIP_NO  from INV_TR where DEL_IF=0 and TR_TYPE='6' and PK = " + _pk;
        cursor2 = db2.rawQuery(sql,null);
        System.out.print("\n\n\n cursor2.count: " + String.valueOf(cursor2.getCount()));
        //data=new String [cursor2.getCount()];
        boolean read= Config.ReadFileConfig();

        if (cursor2.moveToFirst()) {

            data=new String [cursor2.getCount()];
            int j=0;
            do {
                // labels.add(cursor.getString(1));
                String para ="";
                for(int i=0; i < cursor2.getColumnCount();i++)
                {
                    if(para.length() <= 0)
                    {
                        if(cursor2.getString(i)!= null)
                            para += cursor2.getString(i);
                        else
                            para += "|!";
                    }
                    else
                    {
                        if(cursor2.getString(i)!= null)
                            para += "|!" + cursor2.getString(i);
                        else
                            para += "|!";
                    }
                }
                //para += "|!"+Config.USER;
                para += "|!"+ "LG_MPOS_UPLOAD_GD_V2_005";
                data[j++]=para;
                System.out.print("\n\n\n para upload: "+ para);
            } while (cursor2.moveToNext());
            //////////////////////////
            cursor2.close();
            db2.close();

            ServerAsyncTask task = new ServerAsyncTask(this);
            task.execute(data);
        }
    }

    // show data scan in
    public void OnShowScanIn()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();//remove all view child

            c = Calendar.getInstance();
            df = new SimpleDateFormat("yyyyMMdd");
            scan_date = df.format(c.getTime());

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            sql = "select (select COUNT(0)" +
                    "from INV_TR t1 " +
                    "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE = '" + scan_date + "' AND T1.STATUS NOT IN('000', ' ')" +
                    ") AS SEQ,T2.PK, T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.INCOME_DATE, T2.CHARGER, T2.WH_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK " +
                    " FROM INV_TR t2 "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '"+type+"' AND SCAN_DATE = '" + scan_date + "' AND T2.STATUS NOT IN('000', ' ')  " +
                    " GROUP BY T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.INCOME_DATE, T2.CHARGER, T2.WH_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK" +
                    " ORDER BY pk  ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            txtTotalGridMid=(TextView) findViewById(R.id.txtTotalLabel);
            txtTotalGridMid.setText("Total:" + count);
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("CHARGER"))     , scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("WH_NAME"))     , scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[6], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight));

                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(3); //STATUS
                            String st_status=tvSlipNo.getText().toString();

                            if(st_status.equals("005") ){

                                TextView tv1 = (TextView) tr1.getChildAt(9); //INV_TR_PK
                                String value=tv1.getText().toString()+"|"+st_status;

                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255,99,71));
                                }

                                if (queueMid.size() > 0) {

                                    if (queueMid.contains(value)) {
                                        queueMid.remove(value);

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK );
                                        }
                                        duplicate = true;
                                    }
                                }

                                if (!duplicate)
                                    queueMid.add(value);
                                //Toast.makeText(getApplicationContext(), value + "", Toast.LENGTH_SHORT).show();
                            }
                            Toast.makeText(getApplicationContext(), queueMid.size() + "", Toast.LENGTH_SHORT).show();

                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }

            }
        } catch (Exception ex) {
            //Toast.makeText(this,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            //Log.e("OnShowScanIn Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    //////////////////////////////////////
    public  void OnShowScanInExchangeProd(){
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();//remove all view child

            c = Calendar.getInstance();
            df = new SimpleDateFormat("yyyyMMdd");
            scan_date = df.format(c.getTime());

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, WH_NAME, LINE_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '20' AND SENT_YN='Y' AND SCAN_DATE = '" + scan_date + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq= String.valueOf(count-i);
                    row.addView(makeTableRowWithText(seq , scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("CHARGER"))     , scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("WH_NAME"))     , scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }


            }
            //total Grid Scan in
            txtTotalGridMid=(TextView) findViewById(R.id.txtTotalLabel);
            txtTotalGridMid.setText("Total: " + count);
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);

        } catch (Exception ex) {
            Toast.makeText(this,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in Incoming Prod
    public void OnShowScanIncomeProd()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();//remove all view child

            c = Calendar.getInstance();
            df = new SimpleDateFormat("yyyyMMdd");
            scan_date = df.format(c.getTime());

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, WH_NAME, LINE_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '4' AND SENT_YN='Y' AND SCAN_DATE = '" + scan_date + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq= String.valueOf(count-i);
                    row.addView(makeTableRowWithText(seq , scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("CHARGER"))     , scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("WH_NAME"))     , scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }

                txtTotalGridMid=(TextView) findViewById(R.id.txtTotalLabel);
                txtTotalGridMid.setText("Total: " + count);
                txtTotalGridMid.setTextColor(Color.BLUE);
                txtTotalGridMid.setTextSize((float) 18.0);

            }
        } catch (Exception ex) {
            Toast.makeText(this,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanInInventory()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();//remove all view child

            c = Calendar.getInstance();
            df = new SimpleDateFormat("yyyyMMdd");
            scan_date = df.format(c.getTime());

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, WH_NAME, LINE_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '7' AND SENT_YN='Y' AND SCAN_DATE = '" + scan_date + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq= String.valueOf(count-i);
                    row.addView(makeTableRowWithText(seq , scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("CHARGER"))     , scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("WH_NAME"))     , scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }


            }
            //total Grid Scan in
            txtTotalGridMid=(TextView) findViewById(R.id.txtTotalLabel);
            txtTotalGridMid.setText("Total: " + count);
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);

        } catch (Exception ex) {
            Toast.makeText(this,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanInOutInventory()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);


            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();//remove all view child

            c = Calendar.getInstance();
            df = new SimpleDateFormat("yyyyMMdd");
            scan_date = df.format(c.getTime());

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, WH_NAME, LINE_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '"+type+"' AND SENT_YN='Y' AND SCAN_DATE = '" + scan_date + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq= String.valueOf(count-i);
                    row.addView(makeTableRowWithText(seq , scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("CHARGER"))     , scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("WH_NAME"))     , scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            txtTotalGridMid=(TextView) findViewById(R.id.txtTotalLabel);
            txtTotalGridMid.setText("Total: " + count);
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);

        } catch (Exception ex) {
            Toast.makeText(this,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanInGoodsDeliNoReq()
    {
        try {
            scan_date = CDate.getDateyyyyMMdd();

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();//remove all view child

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            sql = "select (select COUNT(0)" +
                    "from INV_TR t1 " +
                    "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE = '" + scan_date + "' AND T1.STATUS NOT IN('000', ' ')" +
                    ") AS SEQ, T2.PK, T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.INCOME_DATE, T2.CHARGER, T2.WH_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK " +
                    " FROM INV_TR t2 "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '8' AND SCAN_DATE = '" + scan_date + "' AND T2.STATUS NOT IN('000', ' ')  " +
                    " GROUP BY T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.INCOME_DATE, T2.CHARGER, T2.WH_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK" +
                    " ORDER BY pk desc  ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("CHARGER"))     , scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("WH_NAME"))     , scrollableColumnWidths[5], fixedRowHeight));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            txtTotalGridMid=(TextView) findViewById(R.id.txtTotalLabel);
            txtTotalGridMid.setText("Total: " + count);
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);
        } catch (Exception ex) {
            //Toast.makeText(this,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            //Log.e("OnShowScanIn Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanInStockTransfer()
    {
        try {
            scan_date = CDate.getDateyyyyMMdd();
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();//remove all view child

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, WH_NAME, LINE_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '9' AND SENT_YN='Y' AND SCAN_DATE = '" + scan_date + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq= String.valueOf(count-i);
                    row.addView(makeTableRowWithText(seq, scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("CHARGER"))     , scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("WH_NAME"))     , scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            txtTotalGridMid=(TextView) findViewById(R.id.txtTotalLabel);
            txtTotalGridMid.setText("Total: " + count);
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);
        } catch (Exception ex) {
            Toast.makeText(this,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
            cursor.close();
        }
    }


    public void OnShowScanInGoodsPartial(){
        try {
            scan_date = CDate.getDateyyyyMMdd();
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(this);
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();//remove all view child

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            sql = "select (select COUNT(0)" +
                    "from INV_TR t1 " +
                    "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE = '" + scan_date + "' AND T1.STATUS NOT IN('000', ' ')" +
                    ") AS SEQ, T2.PK, T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.INCOME_DATE, T2.CHARGER, T2.WH_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK " +
                    " FROM INV_TR t2 "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '11' AND SCAN_DATE = '" + scan_date + "' AND T2.STATUS NOT IN('000', ' ')  " +
                    " GROUP BY T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.INCOME_DATE, T2.CHARGER, T2.WH_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK" +
                    " ORDER BY pk desc  ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(this);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight));

                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("CHARGER"))     , scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("WH_NAME"))     , scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(3); //STATUS
                            String st_slipNO=tvSlipNo.getText().toString();

                            //if(st_slipNO.equals("005") || st_slipNO.equals("008")){
                            if(st_slipNO.equals("005") ){
                                TextView tv1 = (TextView) tr1.getChildAt(9); //INV_TR_PK
                                String value=tv1.getText().toString()+"|"+st_slipNO;

                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255,99,71));
                                }

                                if (queueMid.size() > 0) {

                                    if (queueMid.contains(value)) {
                                        queueMid.remove(value);

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK );
                                        }
                                        duplicate = true;
                                    }
                                }

                                if (!duplicate)
                                    queueMid.add(value);

                            }
                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            txtTotalGridMid=(TextView) findViewById(R.id.txtTotalLabel);
            txtTotalGridMid.setText("Total: " + count);
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);
        } catch (Exception ex) {
            //Toast.makeText(this,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            //Log.e("OnShowScanIn Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
    //----------------------------------------------------------
    ///Load Header grid
    //----------------------------------------------------------
    public void OnShowGridHeader()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(this);
        TableLayout scrollablePart;

        // Status Scan
        scrollablePart = (TableLayout) findViewById(R.id.scrollable_part1);
        row = new TableRow(this);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Status", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Slip No", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Incoming Date", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Charger", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("W/H Name", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Line Name", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

    }


    public void OnShowGridHeaderNoReq()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(this);
        TableLayout scrollablePart;

        // Status Scan
        scrollablePart = (TableLayout) findViewById(R.id.scrollable_part1);
        row = new TableRow(this);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Status", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Slip No", scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Incoming Date", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Charger", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("WareHouse", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

    }

    public TextView makeTableRowHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(this);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.YELLOW);
        recyclableTextView.setTextSize(18);
        recyclableTextView.setGravity(Gravity.CENTER);
        recyclableTextView.setBackgroundColor(bsColors[1]);
        recyclableTextView.setPadding(0, 2, 0, 2);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }
    public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(this);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.BLACK);
        recyclableTextView.setTextSize(16);
        recyclableTextView.setBackgroundColor(-1);
        recyclableTextView.setPadding(0, 2, 0, 2);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_grid_list_view_mid, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
