package gw.genumobile.com.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import gw.genumobile.com.views.inventory.Download;

public class DownloadAsyncTask extends AsyncTask<String, Integer, String[][]>{
	String Procedure;
	public  Activity contextCha; 
	private ProgressDialog dialog;
	Download callDown;
//	Incoming callerIn;
//	Outgoing callerOut;
//	Goodsdelivery goodsDeli;
//	String frmActivity="";

	//constructor nay duoc truyen vao la MainActivity
	 public DownloadAsyncTask(Activity ctx)
	 {		
		 contextCha=ctx;
		 dialog = new ProgressDialog(contextCha);
		 
		 callDown=(Download)ctx;
//		 frmActivity=contextCha.getLocalClassName();
//		 if(frmActivity.equals("Incoming"))
//			 callerIn=(Incoming)ctx;
//		 if(frmActivity.equals("Outgoing"))
//			 callerOut=(Outgoing)ctx;
//		 if(frmActivity.equals("Goodsdelivery"))
//			 goodsDeli=(Goodsdelivery)ctx;
	 }
	   

	 //Ham nay se duoc thuc hien dau tien
    @Override
    protected void onPreExecute() {
    	Toast.makeText(contextCha, "Start here",
    			 Toast.LENGTH_SHORT).show();
    	
        dialog.setMessage("Download data from server, please wait.");
        dialog.setCancelable(false);//not close when touch
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setProgress(0);
   		
        dialog.show();
    }
     
   
    //sau do toi ham doInBackgroud
    //tuyet doi khong duoc cap nhat giao dien trong ham nay
    @Override
	protected String[][] doInBackground(String... params) 
	{
		// TODO Auto-generated method stub
		//Read file config for WebService
		boolean read = ReadFileConfig();
		
		 String result[][]=new String[0][0];
 
		 String domain = params[0];
		 String[] strArray = domain.split("\\,");
		 
		 for (String str : strArray) 		 
		 System.out.println(str);
		 
		 int i = Integer.parseInt(strArray[0]);
		 
		 Procedure = strArray[1];	
		 

	 System.out.print("*****\n***length procedure label msg in connectThread AAA:");

	    String para = strArray[2];	
   	    result = HandlerWebService.GetDataTableArg(Procedure, para);
   	    dialog.setMax(result.length);
   	 
	   	if(Procedure.equals("lg_mpos_m010_get_label")){
		    	
		    dialog.setProgress(0);
	   	   	for(int k=0;k<result.length;k++)
	  		{
	   	   		try{
	   	   			callDown.InsertDataIntoDBTLG_LABEL(result,k);
	       	   		SystemClock.sleep(100);
					// Update the progress bar
					publishProgress(k+1,0);
	   	   		}
	       	   	catch(Exception ex){
	   				Log.e("Download Lable error", ex.toString());
	   			}
	  		}
	   	   	SystemClock.sleep(5000);
	
		}
   	    if(Procedure.equals("lg_mpos_m010_get_gd_req")){
   	    	
   	    	dialog.setProgress(0);
       	   	for(int k=0;k<result.length;k++)
      		{
       	   		try{
       	   			callDown.InsertDataIntoDBTLG_GD_REQ(result,k);
	       	   		SystemClock.sleep(100);
					// Update the progress bar
					publishProgress(k+1);
       	   		}
	       	   	catch(Exception ex){
	   				Log.e("Upload error", ex.toString());
	   			}
      		}
       	   	SystemClock.sleep(500);

   	    }
   	   	if(Procedure.equals("lg_mpos_m010_get_wh")){

   	   		dialog.setProgress(0);
       	   	for(int k=0;k<result.length;k++)
      		{
       	   		try{
       	   			callDown.InsertDataWH(result,k);
	       	   		SystemClock.sleep(100);
					// Update the progress bar
					publishProgress(k+1,1);
       	   		}
	       	   	catch(Exception ex){
	   				Log.e("Upload error", ex.toString());
	   			}
      		}
       	   	SystemClock.sleep(5000);

   	    }
       	 if(Procedure.equals("lg_mpos_m010_get_line")){
       		
       		dialog.setProgress(0);
	       	   	for(int k=0;k<result.length;k++)
	      		{
	       	   		try{
	       	   			callDown.InsertDataWH_Line(result,k);
		       	   		SystemClock.sleep(100);
						// Update the progress bar
						publishProgress(k+1,2);
	       	   		}
		       	   	catch(Exception ex){
		   				Log.e("Upload error", ex.toString());
		   			}
	      		}
	       	   	SystemClock.sleep(500);
   	   	    }

		
		//System.out.print("Hello Viet Nam: "+ result.length);
		return null;
	}
    /**
	 * update IU
	 */
	 @Override
	 protected void onProgressUpdate(Integer... values) {
		 // TODO Auto-generated method stub
		 super.onProgressUpdate(values);
		 //thông qua contextCha để lấy được control trong MainActivity
		// Update the progress bar
		 if(values[1]==0)
			 dialog.setMessage("Download data LABEL from server, please wait.");
		 if(values[1]==1)
			 dialog.setMessage("Download data Warehouse from server, please wait.");
		 if(values[1]==2)
			 dialog.setMessage("Download data LINE from server, please wait.");
		 int giatri=values[0];
		dialog.setProgress(giatri);

	 }
    /**
   	 * sau khi tien trinh thuc hien xong thi ham nay se xay ra
   	 */
    @Override
    protected void onPostExecute(String[][] result) {
    	super.onPreExecute();

    	if (dialog.isShowing()) {
        	try {
				Thread.sleep(1500);							
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
            dialog.dismiss();
        }
    
//        if (dialog.isShowing()) {
//        	try {
//				Thread.sleep(1500);							
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//            dialog.dismiss();
//        }
//        if(frmActivity.equals("Incoming")){
//        	 callerIn.clearAllData();
//             callerIn.OnShowGW();
//        }
//		if(frmActivity.equals("Outgoing")){
//			 callerOut.clearAllData();
//			 callerOut.OnShowGW();
//    	}
//		if(frmActivity.equals("Goodsdelivery")){
//			 goodsDeli.clearAllData();
//			 goodsDeli.OnShowGW();
//		}
		
		this.cancel(true);
		 
        
    }
    

    /*---------------------------------------------------------------------*/
	/*---------------------READ FILE CONFIG--------------------------------*/
	/*---------------------------------------------------------------------*/
	private boolean  ReadFileConfig()
	{
		  File dir1 = Environment.getExternalStorageDirectory();
	  	  //Get the text file  
	      File file = new File(dir1, "ConfigSetting.txt");  
	      // i have kept text.txt in the sd-card  		      
	      if(file.exists())   // check if file exist  
	      {  
	        	int i=0; 
	        	//Read text from file    
	            try 
	            {  
	                BufferedReader br = new BufferedReader(new FileReader(file));  
	                String line;  

	                while ((line = br.readLine()) != null) 
	                {  
	                       i++;
			               switch(i)
			               {	               
						          case 3:  
						        	       HandlerWebService.URL = splitContent(line);
						                   break;		        
						          case 10:
						        	  	   HandlerWebService.dbName = splitContent(line);
						                   break;
						          case 11: 
						        	  	   HandlerWebService.dbUser = splitContent(line);
						                   break;
						          case 12: 
						        	       HandlerWebService.dbPwd = splitContent(line);
						                   break;
						          default:
						        	  break;	            	   
			               	}	                   
			         }
			         br.close();
	            }  
	            catch (IOException e) 
	            {  
	                //You'll need to add proper error handling here  
	            	return false;
	            } 
	       }
	      else 
	    	   return false;
	      
	      //----------------------
	      return true;
	}
	
	private  String splitContent(String line)
	{
			String[] result = line.split("=");	
			return result[1];
	}
     
}
