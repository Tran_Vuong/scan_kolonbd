package gw.genumobile.com.services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import gw.genumobile.com.models.JResultData;

/**
 * Created by admin on 1/18/2017.
 */

public class gwService extends AsyncTask<String, Void, JResultData> {
    public Activity contextCha;
    private ProgressDialog dialog;
    public Toast toast;
    private String _URL;
    private String _Company;
    private String _DBName;
    private String _DBUser;
    public gwService( String url, String company, String dbName, String dbUser)
    {
        _URL = url;
        _Company = company;
        _DBName = dbName;
        _DBUser = dbUser;
    }
    //constructor nay duoc truyen vao la MainActivity
    public gwService(Activity ctx)
    {
        contextCha=ctx;
        //dialog = new ProgressDialog(contextCha);
        //toast=new Toast(contextCha);

    }
    @Override
    protected void onPreExecute() {
        //  Log.i(TAG, "onPreExecute");
//		dialog.setMessage("Get  data into server, please wait.");
//        dialog.setCancelable(false);//not close when touch
//        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//        dialog.setProgress(0);
//
//        dialog.show();
        //Toast.makeText(contextCha, "Download finish!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected JResultData doInBackground(String... params)
    {
        // TODO Auto-generated method stub
        //Read file config for WebService
        //boolean read = ReadFileConfig();//Config from txtConfig
        boolean read = ReadFileConfigV2();

        JResultData result=null;

        String domain = params[0];
        String[] strArray = domain.split("\\,");

        for (String str : strArray)
            System.out.println(str);

        int i = Integer.parseInt(strArray[0]);
        String Procedure = strArray[1];
        switch(i)
        {
            case 1://use for GetDataTableArg method
                System.out.print("*****\n***length procedure label msg in connectThread AAA:");

                String para = strArray[3];
                int numcurr=1;
                if(strArray.length>2)
                    numcurr=Integer.parseInt(strArray[2]);
                result = HandlerWebService.GetData(Procedure, numcurr,para);
               // System.out.print("*****\n***length procedure label msg in connectThread:"+result.length);
                break;
            case 2://use for InsertTableArg method
                String pa = strArray[2];
                result =HandlerWebService.OnExcute(Procedure, pa);
                System.out.print("\n\n\n******InsertTableArg: "+result.getListODataTable().get(0).records.get(0).get("status"));
                break;
            case 3:
                //use for GetDataTableNoArg method
               // result = HandlerWebService.GetDataTableNoArg(Procedure);
                //  System.out.print("\n\n\n******GetDataTableNoArg -- result.length: "+result.length);
                break;
            default:
                break;
        }

        //System.out.print("Hello Viet Nam: "+ result.length);
        return result;
    }


    @Override
    protected void onPostExecute(JResultData result) {
        // TODO Update the UI thread with the final result
        super.onPreExecute();

        //dialog.dismiss();
        //Toast.makeText(contextCha, "Download finish!", Toast.LENGTH_SHORT).show();
    }



    @Override
    protected void onProgressUpdate(Void... values) {
        //  Log.i(TAG, "onProgressUpdate");
    }

    /*---------------------------------------------------------------------*/
 	/*---------------------READ FILE CONFIG--------------------------------*/
 	/*---------------------------------------------------------------------*/
    private boolean  ReadFileConfigV2()
    {
        if (_Company.isEmpty() )
            HandlerWebService.URL = "http://" + _URL + "/" + "gwWebservice.asmx";
        else
            HandlerWebService.URL = "http://" + _URL + "/" + _Company + "/gwWebservice.asmx";

        HandlerWebService.dbName = _DBName;
        HandlerWebService.dbUser = _DBUser;
        HandlerWebService.dbPwd = _DBUser + "2";
        return true;
    }
}
