package gw.genumobile.com.services;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.support.v4.app.NavUtils;
import android.util.Base64;
import android.util.Log;

import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;


public class HandlerWebService {
	private static final String NAMESPACE = "http://tempuri.org/";
	private static final String GetData_MethodName="GetDataByJson";
	private static final String GetDataTableArg_MethodName="Arg";
	private static final String OnExcute_MethodName="OnExcute";


	//private static  String URL = "http://192.168.1.100/gasp/gwwebservice.asmx";
	//private String SOAP_ACTION ="http://tempuri.org/";
	
	//private String URL;
	//private static String dbName = "gasp";
	//private static String dbUser = "genuwin";
	//private static String dbPwd = "genuwin2";

	public static String URL = "";
	public static String dbName = "";
	public static String dbPwd ="";
	public static String dbUser = "";
	public HandlerWebService(String url, String dbName,String dbUser,String dbPwd )
	{
		this.URL=url;
		this.dbName=dbName;
		this.dbUser=dbUser;
		this.dbPwd=dbPwd;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getDbPwd() {
		return dbPwd;
	}

	public void setDbPwd(String dbPwd) {
		this.dbPwd = dbPwd;
	}

	public static String EncodeMD5(String s)
    {
        MessageDigest digest;
        try
        {
        	String hash = "";
            digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(), 0, s.length());
            byte[] bytes = digest.digest();
            hash = Base64.encodeToString(bytes, 0); 
            return hash.replace('=', ' ').trim();
        }
        catch (NoSuchAlgorithmException e)
        {
        }

        return "";
    }
	
////////////////////////////////////	
	//Get max number column
	public static int GetMax( SoapObject a){
		int max=0;
		   for(int row=0;row<a.getPropertyCount();row++)
			   if(max < ((SoapObject) a.getProperty(row)).getPropertyCount())
				   max = ((SoapObject) a.getProperty(row)).getPropertyCount();
		return max;
	}
	public static String[][] GetDataTableArg(String Procedure, String para)//, String dbName, String dbUser, String dbPwd)
	{
		Log.e("**GetDataTable**URL: ",URL);
		Log.e("GetDataTableArg: ","Procedure: "+Procedure+"  --- para: "+para);
		String result[][]=new String[0][0];	
		//Create request		
		String SOAP_ACTION = NAMESPACE+ GetDataTableArg_MethodName;
		SoapObject request = new SoapObject(NAMESPACE, GetDataTableArg_MethodName);
		//add parameter for request
		request.addProperty("Procedure",Procedure);
		request.addProperty("para", para);
		request.addProperty("dbName", dbName);
		request.addProperty("dbUser",dbUser);
		request.addProperty("dbPwd",dbPwd);
		  
		//Create envelope
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		//Set output SOAP object
		envelope.setOutputSoapObject(request);		
		 
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		////////////////////////   //////////////////////////////////////////////////////
		SoapObject table = null; 
		SoapObject tableRow = null; 
		SoapObject responseBody = null; 
		try 
		{
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			//Log.e("requestDump:  ", androidHttpTransport.requestDump);
			//Log.e("responseDump:  ", androidHttpTransport.responseDump);	      
			System.out.print("\n\n ********* androidHttpTransport.requestDump: "+ androidHttpTransport.requestDump);
			System.out.print("\n\n ********* androidHttpTransport.responseDump: "+ androidHttpTransport.responseDump);
			
	        SoapObject response = (SoapObject)envelope.getResponse();// This step: get file XML	                 	        
	        responseBody = (SoapObject) response.getProperty(1);//remove information XML,only retrieved results that returned        
	      
	        if(responseBody.getPropertyCount()>0 )
            {   
            	table = (SoapObject) responseBody.getProperty(0);//get information XMl of tables that is returned          
	                       
	             int numberColumn ;
            	 if(table.getPropertyCount()>0){//check data response
  	               numberColumn = GetMax(table);//GET NUMBER COLUMNS
  	               
  	               Log.e("Total rows: ",String.valueOf(table.getPropertyCount()));  	               
  	               Log.e("Total columns: ",String.valueOf(numberColumn));
  	               result = new String[table.getPropertyCount()][numberColumn];
  	               for(int row = 0; row < table.getPropertyCount() ; row++){
  	            	   tableRow = (SoapObject) table.getProperty(row); 
  	                   for(int col = 0; col < ((SoapObject) table.getProperty(row)).getPropertyCount(); col++)
  	                        result[row][col] = tableRow.getProperty(col).toString();	                	                
  	                   if(((SoapObject) table.getProperty(row)).getPropertyCount() < numberColumn)
  	                	   for(int col=((SoapObject) table.getProperty(row)).getPropertyCount(); col<numberColumn;col++)
  		                   		result[row][col] = "";              
  	               }	             
            	 }	        
            }
              
		} catch (SoapFault e) {
			   System.out.print("\n\n **********GGGGGGGGGGGGGGGGGGGGGGGGG********* \n\n");
	        Log.e("\n\n SoapFault -- error", "", e.getCause());

	    } catch (IOException e) {
	    	   System.out.print("\n\n **********LLLLLLLLLLLLLLLLLLL********* \n\n");
	        Log.e("\n\n IOException -- error", "", e);

	    }catch (XmlPullParserException e)
        {
	    	System.out.print("\n\n **********KKKKKKKKKKKKKKKK********* \n\n");
	        Log.e("Xml Pull Parse -error:",e.toString());
	        e.printStackTrace();
	        
	        Log.e("MYAPP", "exception: " + e.getMessage());             
	        Log.e("MYAPP", "exception: " + e.toString());

	    }catch (Exception e) {
            System.out.print("\n\n **********EEEEEEEEE********* \n\n");
			Log.e("Exception", "exception: " + e.toString());

        }
        return result;
		
	}
	public static String ExecuteProcedureOutPara (String Procedure, String para)//, String dbName, String dbUser, String dbPwd)
	{
		Log.e("***ExProOutPara*URL: ",URL);
		Log.e("para: ",para);
		String result="";
		String SOAP_ACTION = NAMESPACE+"ExecuteProcedureOutPara";
		SoapObject request = new SoapObject(NAMESPACE, "ExecuteProcedureOutPara");

		request.addProperty("Procedure",Procedure);
		request.addProperty("para", para);
		request.addProperty("dbName", dbName);
		request.addProperty("dbUser", dbUser);
		request.addProperty("dbPwd",dbPwd);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.debug=false;//--tan
		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			result = response.toString();

		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService ExecuteProcedureOutPara: "+e);
			e.printStackTrace();
		}
		return result;

	}
	public static String InsertTableArg (String Procedure, String para)//, String dbName, String dbUser, String dbPwd)
	{
		Log.e("***InsertTableArg*URL: ",URL);
		Log.e("para: ",para);
		String result="";
		String SOAP_ACTION = NAMESPACE+"InsertTable";
		SoapObject request = new SoapObject(NAMESPACE, "InsertTable");

		request.addProperty("Procedure",Procedure);
		request.addProperty("para", para);
		request.addProperty("dbName", dbName);
		request.addProperty("dbUser", dbUser);
		request.addProperty("dbPwd",dbPwd);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.debug=false;//--tan
		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
	        result = response.toString();

		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService InsertTableArg: "+e);
			e.printStackTrace();
		}
		return result;
		
	}

	public static String TableReadOpenString (String Procedure, String para)//, String dbName, String dbUser, String dbPwd)
	{
		Log.e("**** TableRead***URL: ",URL);
		String result="";
		String SOAP_ACTION = NAMESPACE+"TableReadOpenString";
		SoapObject request = new SoapObject(NAMESPACE, "TableReadOpenString");

		request.addProperty("Procedure",Procedure);
		request.addProperty("para", para);
		request.addProperty("dbName", dbName);
		request.addProperty("dbUser",dbUser);
		request.addProperty("dbPwd",dbPwd);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.debug=false;//--tan
		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			result = response.toString();

		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService TableReadOpenString: "+e.toString());
			e.printStackTrace();
		}
		return result;
	}

	public static String UploadFile (byte[] f, String fileName)
	{
		String result="";

		String SOAP_ACTION = NAMESPACE+"UploadFile";
		SoapObject request = new SoapObject(NAMESPACE, "UploadFile");				

		request.addProperty("f",f);
		request.addProperty("fileName", fileName);
		
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);		
		
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
 
		try {	
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);	
	    	SoapPrimitive response = (SoapPrimitive) envelope.getResponse();		
	        result = response.toString();
	           
		} catch (Exception e) {
			Log.e("Exception e    ","Upload File error");
			e.printStackTrace();
		}
		return result;
		
	}
	
	
	public static String[][] GetDataTableNoArg (String Procedure)
	{
		Log.e("GetDataTableNoArg: ","Procedure: "+Procedure);
		String result[][] = new String[0][0];
		String SOAP_ACTION = NAMESPACE+"NoArg";
		//Create request
		SoapObject request = new SoapObject(NAMESPACE, "NoArg");			
		
		//add parameter for request
		request.addProperty("Procedure",Procedure);
		request.addProperty("dbName", dbName);
		request.addProperty("dbUser",dbUser);
		request.addProperty("dbPwd",dbPwd);

		//Create envelope
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		//Set output SOAP object
		envelope.setOutputSoapObject(request);		
		
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		
		SoapObject table = null; 
		SoapObject tableRow = null; 
		SoapObject responseBody = null;   
		try {
		
			//Create HTTP call object		
			 androidHttpTransport.debug=true;
			//Invole web service
			androidHttpTransport.call(SOAP_ACTION, envelope);
			
	        SoapObject response = (SoapObject)envelope.getResponse();// This step: get file XML	      
            responseBody = (SoapObject) response.getProperty(1);//remove information XML,only retrieved results that returned        
           
            if(responseBody.getPropertyCount()>0 )// data response <> null
            { 
            	table = (SoapObject) responseBody.getProperty(0);//get information XMl of tables that is returned 	           
            	if(table.getPropertyCount()>0){//check data response
            	   int  numberColumn = GetMax(table);//GET NUMBER COLUMNS               
  	               result = new String[table.getPropertyCount()][numberColumn];
  	               Log.e("Total rows: ",String.valueOf(table.getPropertyCount()));
	               Log.e("Total columns: ",String.valueOf(numberColumn));	 
  	               for(int row = 0; row < table.getPropertyCount() ; row++){
  	            	   tableRow = (SoapObject) table.getProperty(row);//Get information each row in table      
  	                   for(int col = 0; col < ((SoapObject) table.getProperty(row)).getPropertyCount(); col++)
  	                        result[row][col] = tableRow.getProperty(col).toString();//Get information each column in row               	                
  	                   if(((SoapObject) table.getProperty(row)).getPropertyCount() < numberColumn)
  	                	   for(int col=((SoapObject) table.getProperty(row)).getPropertyCount(); col<numberColumn;col++)
  		                   		result[row][col] = "";              
  	               }	             
            	 }	        
            }
            		
		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService GetDataTableNoArg : "+e);
			e.printStackTrace();
		}
		return result;
	}

	public static String UploadImageArg(String _procedure,
									 String _table_name, String _Master_pk, String _img_pk,
									 String p_filename, String p_filesize, String p_contenttype,
									 byte[] buff, String _login_user){

		Log.e("***UploadImage***", p_filename);

		String result="";
		String SOAP_ACTION = NAMESPACE+"UploadImage";
		SoapObject request = new SoapObject(NAMESPACE, "UploadImage");

		request.addProperty("dbName", dbName);

		request.addProperty("_dbuser", dbUser);
		request.addProperty("_dbpass",dbPwd);
		request.addProperty("_procedure",_procedure);

		request.addProperty("_table_name", _table_name);
		request.addProperty("_Master_pk", _Master_pk);
		request.addProperty("p_tc_fsbinary_pk", _img_pk);
		request.addProperty("buff", buff);
		request.addProperty("p_filename",p_filename);

		request.addProperty("p_filesize", p_filesize);
		request.addProperty("p_contenttype", p_contenttype);

		request.addProperty("_login_user", _login_user);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		//For 64bit build
		new MarshalBase64().register(envelope);

		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
	//	androidHttpTransport.debug=false;//--tan
		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			result = response.toString();
			Log.e("Upload Image: ", result);
		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService Upload Image: "+e);
			e.printStackTrace();
		}


		return result;
	}

	/**
	 * Get Image By Table Name & PK
	 * @param _table_name
	 * @param _pk
	 * @return
	 */
	public static String GetImage(String _table_name, String _pk){

		Log.e("***GetImage***", _table_name + " - " + _pk);

		String result="";
		String SOAP_ACTION = NAMESPACE+"GetImage";
		SoapObject request = new SoapObject(NAMESPACE, "GetImage");

		request.addProperty("dbName", dbName);

		request.addProperty("_dbuser", dbUser);
		request.addProperty("_dbpass",dbPwd);

		request.addProperty("_table_name", _table_name);

		request.addProperty("_pk", _pk);

		/*SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.debug=false;
		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			result = response.toString();

		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService TableReadOpenString: "+e.toString());
			e.printStackTrace();
		}
		return result;
*/
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		//For 64bit build
		new MarshalBase64().register(envelope);

		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		//	androidHttpTransport.debug=false;//--tan
		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			result = response.toString();
			//test
	//		String str =response.toString();
	//		byte[] decodedString = Base64.decode(str, Base64.DEFAULT);
	//		BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
			//end
			Log.e("Upload Image: ", result);
		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService Upload Image: "+e);
			e.printStackTrace();
		}

		return result;
	}

	public static String GetImageItem(String _pk){

		Log.e("***GetImage***", _pk);

		String result="";
		String SOAP_ACTION = NAMESPACE+"GetImageItem";
		SoapObject request = new SoapObject(NAMESPACE, "GetImageItem");

		request.addProperty("dbName", dbName);

		request.addProperty("_dbuser", dbUser);
		request.addProperty("_dbpass",dbPwd);

		request.addProperty("_pk", _pk);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		//For 64bit build
		new MarshalBase64().register(envelope);

		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			result = response.toString();
			Log.e("Upload Image: ", result);
		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService Upload Image: "+e);
			e.printStackTrace();
		}

		return result;
	}

	public static String GetImagePosition(String _table_name, String _pk, int postion){

		Log.e("***GetImage***", _table_name + " - " + _pk);

		String result="";
		String SOAP_ACTION = NAMESPACE+"GetImagePosition";
		SoapObject request = new SoapObject(NAMESPACE, "GetImagePosition");

		request.addProperty("dbName", dbName);

		request.addProperty("_dbuser", dbUser);
		request.addProperty("_dbpass",dbPwd);

		request.addProperty("_table_name", _table_name);

		request.addProperty("_pk", _pk);
		request.addProperty("_position", postion);

		/*SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.debug=false;
		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			result = response.toString();

		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService TableReadOpenString: "+e.toString());
			e.printStackTrace();
		}
		return result;
*/
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		//For 64bit build
		new MarshalBase64().register(envelope);

		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		//	androidHttpTransport.debug=false;//--tan
		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			result = response.toString();
			//test
			//		String str =response.toString();
			//		byte[] decodedString = Base64.decode(str, Base64.DEFAULT);
			//		BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
			//end
			Log.e("Upload Image: ", result);
		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService Upload Image: "+e);
			e.printStackTrace();
		}

		return result;
	}
	public static String WSTEST(String strPara){


		String result="";
		String SOAP_ACTION = NAMESPACE+"TEST";
		SoapObject request = new SoapObject(NAMESPACE, "TEST");
/*
		request.addProperty("dbName", dbName);

		request.addProperty("_dbuser", dbUser);
		request.addProperty("_dbpass",dbPwd);
		request.addProperty("_procedure",_procedure);

		request.addProperty("_table_name", _table_name);
		request.addProperty("_Master_pk", _Master_pk);
		request.addProperty("p_tc_fsbinary_pk", _img_pk);
		request.addProperty("buff", buff);
		request.addProperty("p_filename",p_filename);

		request.addProperty("p_filesize", p_filesize);
		request.addProperty("p_contenttype", p_contenttype);

		request.addProperty("_login_user", _login_user);*/

		request.addProperty("strPara", strPara);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

	//	new MarshalBase64().register(envelope);

		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.debug=false;//--tan
		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			result = response.toString();
			Log.e("result: ", result);
		} catch (Exception e) {
			Log.e("Exception e    ","ERROR WebService Upload Image: "+e);
			e.printStackTrace();
		}

		return result;
	}
////////TRANGTQ ADDING////////
public static JResultData GetData(String Procedure,int numcurr, String para)//, String dbName, String dbUser, String dbPwd)
{
	Log.e("**GetDataTable**URL: ",URL);
	Log.e("GetDataTableArg: ","Procedure: "+Procedure+"  --- para: "+para);
	JResultData result=null;
	//Create request
	String SOAP_ACTION = NAMESPACE+GetData_MethodName;
	SoapObject request = new SoapObject(NAMESPACE, GetData_MethodName);
	//add parameter for request
	request.addProperty("Procedure",Procedure);
	request.addProperty("para", para);
	request.addProperty("numcurr", numcurr);
	request.addProperty("dbName", dbName);
	request.addProperty("dbUser",dbUser);
	request.addProperty("dbPwd",dbPwd);

	//Create envelope
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	//Set output SOAP object
	envelope.setOutputSoapObject(request);

	HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
	////////////////////////   //////////////////////////////////////////////////////
	SoapObject table = null;
	SoapObject tableRow = null;
	SoapObject responseBody = null;
	try
	{
		androidHttpTransport.debug=true;
		androidHttpTransport.call(SOAP_ACTION, envelope);
		//Log.e("requestDump:  ", androidHttpTransport.requestDump);
		//Log.e("responseDump:  ", androidHttpTransport.responseDump);
		System.out.print("\n\n ********* androidHttpTransport.requestDump: "+ androidHttpTransport.requestDump);
		System.out.print("\n\n ********* androidHttpTransport.responseDump: "+ androidHttpTransport.responseDump);

		//SoapObject response = (SoapObject)envelope.getResponse();// This step: get file XML
		//	if(responseBody.getPropertyCount()>0 )
		//{
		//	String strJson2=response.toString();
		//	result = new JResultData(strJson2);
		//}
		SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
		result = new JResultData(response.toString());

	} catch (SoapFault e) {
		System.out.print("\n\n **********GGGGGGGGGGGGGGGGGGGGGGGGG********* \n\n");
		Log.e("\n\n SoapFault -- error", "", e.getCause());

	} catch (IOException e) {
		System.out.print("\n\n **********LLLLLLLLLLLLLLLLLLL********* \n\n");
		Log.e("\n\n IOException -- error", "", e);

	}catch (XmlPullParserException e)
	{
		System.out.print("\n\n **********KKKKKKKKKKKKKKKK********* \n\n");
		Log.e("Xml Pull Parse -error:",e.toString());
		e.printStackTrace();

		Log.e("MYAPP", "exception: " + e.getMessage());
		Log.e("MYAPP", "exception: " + e.toString());

	}catch (Exception e) {
		System.out.print("\n\n **********EEEEEEEEE********* \n\n");
		Log.e("Exception", "exception: " + e.toString());

	}
	return result;

}
	public static JResultData OnExcute (String Procedure, String para)//, String dbName, String dbUser, String dbPwd)
	{
		Log.e("***InsertTableArg*URL: ",URL);
		Log.e("para: ",para);
		JResultData result= null;
		String SOAP_ACTION = NAMESPACE+OnExcute_MethodName;
		SoapObject request = new SoapObject(NAMESPACE, OnExcute_MethodName);

		request.addProperty("Procedure",Procedure);
		request.addProperty("para", para);
		request.addProperty("dbName", dbName);
		request.addProperty("dbUser", dbUser);
		request.addProperty("dbPwd",dbPwd);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.debug=false;//--tan
		try {
			androidHttpTransport.debug=true;
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			result = new JResultData(response.toString());

		} catch (Exception e) {

			Log.e("Exception e    ","ERROR WebService InsertTableArg: "+e);
			e.printStackTrace();
		}
		return result;

	}




}
