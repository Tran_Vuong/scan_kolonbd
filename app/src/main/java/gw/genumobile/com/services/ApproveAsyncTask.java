package gw.genumobile.com.services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import gw.genumobile.com.views.inventory.GoodsDeliNoReq;
import gw.genumobile.com.views.inventory.GoodsPartial;
import gw.genumobile.com.views.inventory.GoodsdeliveryV2;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.views.inventory.ProductInc;
import gw.genumobile.com.views.inventory.Mapping;
import gw.genumobile.com.db.MySQLiteOpenHelper;
import gw.genumobile.com.views.inventory.StockInInv;
import gw.genumobile.com.views.inventory.StockOutInv;
import gw.genumobile.com.views.inventory.StockTransfer;

/**
 * Created by TL on 9/22/2015.
 */
public class ApproveAsyncTask extends AsyncTask<String[], Integer, Void> {
    public Activity contextCha;
    private ProgressDialog dialog;
    ProductInc callerInV2;
    StockInInv callerInInventoryV2;
    StockOutInv callerStockOutInv;
    GoodsdeliveryV2 goodsDeliV2;
    GoodsDeliNoReq goodsDeliNoReq;
    GridListViewBot grdListViewBot;
    StockTransfer stTransfer;
    Mapping mapping;
    GoodsPartial goodsPartial;

    String frmActivity="";
    String resultkq[][]=new String[0][0];

    String[] para1 = new String[0];
    String[] parakq = new String[0];
    String[] parakq1 = new String[0];
    String para,result11,procedure;
    Fragment target_fr;
    //constructor nay duoc truyen vao la MainActivity
    public ApproveAsyncTask(Activity ctx){}
    public ApproveAsyncTask(Activity ctx,Fragment fr)
    {
        contextCha=ctx;
        dialog = new ProgressDialog(contextCha);

       // frmActivity=contextCha.getLocalClassName();
        target_fr=fr;

        if (target_fr instanceof StockInInv) {
            callerInInventoryV2 = (StockInInv) target_fr;
        }
        if (target_fr instanceof StockOutInv) {
            callerStockOutInv = (StockOutInv) target_fr;
        }
        frmActivity= target_fr.getTag();

        if (target_fr instanceof ProductInc) {
            callerInV2 = (ProductInc) target_fr;
        }
        if (target_fr instanceof GoodsdeliveryV2) {
            goodsDeliV2 = (GoodsdeliveryV2) target_fr;
        }
        if (target_fr instanceof GridListViewBot) {
            grdListViewBot = (GridListViewBot) target_fr;
        }

        if (target_fr instanceof GoodsDeliNoReq) {
            goodsDeliNoReq = (GoodsDeliNoReq) target_fr;
        }
        if (target_fr instanceof StockTransfer) {
            stTransfer = (StockTransfer) target_fr;
        }

        if (target_fr instanceof Mapping) {
            mapping = (Mapping) target_fr;
        }
        if (target_fr instanceof GoodsPartial) {
            goodsPartial = (GoodsPartial) target_fr;
        }
    }
    //Ham nay se duoc thuc hien dau tien
    @Override
    protected void onPreExecute() {

        dialog.setMessage("Uploading  data into server, please wait.");
        dialog.setCancelable(false);//not close when touch
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setProgress(0);
        dialog.show();
    }
    //sau do toi ham doInBackgroud
    //tuyet doi khong duoc cap nhat giao dien trong ham nay
    @Override
    protected Void doInBackground(String[]... array) {
        boolean read = ReadFileConfig();
        String result[]= array[0];

        System.out.print("\n\n\n record doInBackground : "+result.length);
        System.out.print("\n");
        //Using call Insert,Update sqlite when AsyncTask running
        MySQLiteOpenHelper mySQLiteOpenHelper = MySQLiteOpenHelper.getInstance(contextCha);

        try
        {
            int checkApproved=0;
            for(int i=0;i<result.length;i++) {
                if(result[i]!=null)
                {
                    para = result[i].toString();

                    para1 = para.split("\\|!");
                    procedure = para1[para1.length - 1].toUpperCase();

                    para = para1[0];
                    if (para.endsWith("*|*")) {
                        para = para.substring(0, para.length() - 3);
                    }
                    para1 = para.split("\\*\\|\\*");
                    result11 = HandlerWebService.TableReadOpenString(procedure, para);
                    SystemClock.sleep(400);
                    //para1 = para.split("\\|!");
                    parakq = result11.split("\\*!");
                    for (int rw = 0; rw < parakq.length; rw++) {
                        if (!parakq[rw].equals("ERROR") && parakq[rw] != "") {
                            parakq1 = parakq[rw].split("\\|!");
                            String slipNo = parakq1[1];
                            int pk = Integer.valueOf(parakq1[2]);
                            if (procedure.endsWith("lg_mpos_pro_mapping")) {
                                boolean kq = mySQLiteOpenHelper.updateMapping(pk);
                            } else {
                                boolean kq = mySQLiteOpenHelper.updateMakeSlip(pk, slipNo);
                                if (kq)
                                    checkApproved++;
                            }
                        }
                    }
                }
            }
            //Approved
            if(checkApproved ==para1.length && result.length > 0){
                if (procedure.endsWith("LG_MPOS_PRO_GD_MAKESLIP")){
                    String value_app= parakq1[0]+"|!0|!"+parakq1[3];
                    result11 = HandlerWebService.TableReadOpenString("LG_PRO_DSCD00090_3",value_app);
                    SystemClock.sleep(300);
                }
                if (procedure.endsWith("LG_MPOS_PRO_INC_MAKESLIP_MAP")){
                    String value_app= parakq1[0]+"|!"+parakq1[3];
                    result11 = HandlerWebService.TableReadOpenString("LG_PRO_FPPR00100",value_app);
                    SystemClock.sleep(300);
                }
                //
                if (procedure.endsWith("LG_MPOS_PRO_ST_INC_MAKESLIP")){
                    String value_app= parakq1[0]+"|!"+parakq1[3];
                    result11 = HandlerWebService.TableReadOpenString("LG_PRO_BINI00130_3",value_app);
                    SystemClock.sleep(300);
                }
                if (procedure.endsWith("LG_MPOS_PRO_ST_OUT_MAKESLIP")){
                    String value_app= parakq1[0]+"|!"+parakq1[3];
                    result11 = HandlerWebService.TableReadOpenString("LG_PRO_BINI00180_3",value_app);
                    SystemClock.sleep(300);
                }
                if (procedure.endsWith("LG_MPOS_PRO_GD_NOREQ_MAKESLIP")){
                    //String value_app= parakq[0]+"|!"+para1[13];
                    //result11 = HandlerWebService.TableReadOpenString("lg_pro_bini00180_3",value_app);
                    SystemClock.sleep(300);
                }
                if (procedure.endsWith("LG_MPOS_PRO_ST_TRANSFER_MSLIP")){
                    //String value_app= parakq[0]+"|!"+para1[13];
                    //result11 = HandlerWebService.TableReadOpenString("lg_pro_bini00180_3",value_app);
                    SystemClock.sleep(300);
                }
                if (procedure.endsWith("LG_MPOS_PRO_GD_PARTIAL_MSLIP")){
                    //String value_app= parakq[0]+"|!0|!"+para1[16];
                    //result11 = HandlerWebService.TableReadOpenString("lg_pro_dscd00090_3",value_app);
                    //SystemClock.sleep(300);
                }
            }
        }
        catch(Exception ex){
            Log.e("Approve error: ", ex.getMessage());
        }

        SystemClock.sleep(500);
        return null;
    }

    /**
     * update IU
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
        //thông qua contextCha để lấy được control trong MainActivity
        // Update the progress bar
        //int giatri=values[0];
        //dialog.setProgress(giatri);

    }
    /**
     * sau khi tien trinh thuc hien xong thi ham nay se xay ra
     */
    @Override
    protected void onPostExecute(Void result) {
        super.onPreExecute();

        if (dialog.isShowing()) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }

        if(frmActivity.equals("ProductInc")){
            callerInV2.OnShowScanAccept();
            callerInV2.OnPermissionApprove();
        }

        if(frmActivity.equals("GoodsdeliveryV2")){
            goodsDeliV2.OnShowScanAccept();
            goodsDeliV2.OnPermissionApprove();
        }

        if(frmActivity.equals("GridListViewBot")){
            grdListViewBot.OnShowScanAccept();
        }
        if(frmActivity.equals("StockInInv")){
            callerInInventoryV2.OnShowScanAccept();
            callerInInventoryV2.OnPermissionApprove();
        }
        if(frmActivity.equals("StockOutInv")){
            callerStockOutInv.OnShowScanAccept();
            callerStockOutInv.OnPermissionApprove();
        }
        if(frmActivity.equals("GoodsDeliNoReq")){
            goodsDeliNoReq.OnShowScanAccept();
        }
        if(frmActivity.equals("StockTransfer")){
            stTransfer.OnShowScanAccept();
        }
        if(frmActivity.equals("Mapping")){
            mapping.OnShowChild();
        }
        if(frmActivity.equals("GoodsPartial")){
            goodsPartial.OnShowScanAccept();
        }
        this.cancel(true);

    }

    /*---------------------------------------------------------------------*/
	/*---------------------READ FILE CONFIG--------------------------------*/
	/*---------------------------------------------------------------------*/
    private boolean  ReadFileConfig()
    {
        File dir1 = Environment.getExternalStorageDirectory();
        //Get the text file
        File file = new File(dir1, "ConfigSetting.txt");
        // i have kept text.txt in the sd-card
        if(file.exists())   // check if file exist
        {
            int i=0;
            //Read text from file
            try
            {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null)
                {
                    i++;
                    switch(i)
                    {
                        case 3:
                            HandlerWebService.URL = splitContent(line);
                            break;
                        case 10:
                            HandlerWebService.dbName = splitContent(line);
                            break;
                        case 11:
                            HandlerWebService.dbUser = splitContent(line);
                            break;
                        case 12:
                            HandlerWebService.dbPwd = splitContent(line);
                            break;
                        default:
                            break;
                    }
                }
                br.close();
            }
            catch (IOException e)
            {
                //You'll need to add proper error handling here
                return false;
            }
        }
        else
            return false;

        //----------------------
        return true;
    }

    private  String splitContent(String line)
    {
        String[] result = line.split("=");
        return result[1];
    }
}
