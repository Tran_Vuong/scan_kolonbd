package gw.genumobile.com.services;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;

import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.views.inventory.Evaluation;
import gw.genumobile.com.views.inventory.ExchangeProduct;
import gw.genumobile.com.views.inventory.GoodsDeliNoReq;
import gw.genumobile.com.views.inventory.GoodsDeliveryV4;
import gw.genumobile.com.views.inventory.GoodsPartial;
import gw.genumobile.com.views.inventory.GoodsReturn;
import gw.genumobile.com.views.inventory.GoodsdeliveryV2;
import gw.genumobile.com.interfaces.GridListViewMid;
import gw.genumobile.com.views.inventory.Incoming;
import gw.genumobile.com.views.inventory.MappingV3;
import gw.genumobile.com.views.inventory.ProductInc;
import gw.genumobile.com.db.MySQLiteOpenHelper;
import gw.genumobile.com.views.inventory.OthersInOut;
import gw.genumobile.com.views.inventory.Outgoing;
import gw.genumobile.com.views.inventory.Preparation;
import gw.genumobile.com.views.inventory.ProductOut;
import gw.genumobile.com.views.inventory.ProductResultInc;
import gw.genumobile.com.views.inventory.ReturnSupplier;
import gw.genumobile.com.views.inventory.StockDiscard;
import gw.genumobile.com.views.inventory.StockInReq;
import gw.genumobile.com.views.inventory.StockOutInv;
import gw.genumobile.com.views.inventory.StockOutReq;
import gw.genumobile.com.views.inventory.StockReturn;
import gw.genumobile.com.views.inventory.StockTransfer;
import gw.genumobile.com.views.inventory.StockTransferReq;
import gw.genumobile.com.views.inventory.UpdateLocation;
import gw.genumobile.com.views.inventory.StockInInv;
import gw.genumobile.com.views.qc_management.QC_Management;

/**
 * Created by TanLegend on 03/06/2015.
 */
public class ServerAsyncTask  extends AsyncTask<String[], Integer, Void> {
    public Activity contextCha;
    Incoming callerIn;
    ProductInc callerInV2;
    ProductResultInc callerResInV2;
    Outgoing callerOut;
    GoodsdeliveryV2 goodsDeliV2;
    GoodsDeliveryV4 goodsDeliV4;
    GridListViewMid grdListMid;
    StockInInv inInventoryV2;
    StockOutInv stockOutInv;
    GoodsDeliNoReq goodsDeliNoReq;
    StockTransfer stTransfer;
    GoodsPartial goodsPartial;
    Preparation preparation;
    StockReturn stockReturn;
    StockDiscard stockDiscard;
    OthersInOut othersInOut;
    GoodsReturn goodsReturn;
    ReturnSupplier returnSupplier;
    QC_Management qc_management;
    ProductOut productOut;
    Evaluation evaluation;
    ExchangeProduct exchangeProduct;
    StockInReq stockInReq;
    StockOutReq stockOutRequest;
    StockTransferReq stTransferReq;
    MappingV3 mappingv3;

    String frmActivity="";
    String resultkq[][]=new String[0][0];
    UpdateLocation updateLocation;
    Fragment target_fr;
    boolean flag_update=true;
    //constructor nay duoc truyen vao la MainActivity
    public ServerAsyncTask(Activity ctx)
    {

    }
    public ServerAsyncTask(Activity ctx,Fragment fr)
    {
        contextCha=ctx;
        //dialog = new ProgressDialog(contextCha);
        target_fr=fr;
        frmActivity=contextCha.getLocalClassName();

        if (target_fr instanceof StockInInv) {
            inInventoryV2 = (StockInInv) target_fr;
        }
        if (target_fr instanceof MappingV3) {
            mappingv3 = (MappingV3) target_fr;
        }
        if (target_fr instanceof StockOutInv) {
            stockOutInv = (StockOutInv) target_fr;
        }
        if (target_fr instanceof UpdateLocation) {
            updateLocation = (UpdateLocation) target_fr;
        }
        if (target_fr instanceof ProductOut) {
            productOut = (ProductOut) target_fr;
        }

        if (target_fr instanceof Evaluation) {
            evaluation = (Evaluation) target_fr;
        }
        if (target_fr instanceof ReturnSupplier) {
            returnSupplier = (ReturnSupplier) target_fr;
        }
        frmActivity= target_fr.getTag();

        if (target_fr instanceof ProductInc) {
            callerInV2 = (ProductInc) target_fr;
        }
        if (target_fr instanceof ProductResultInc) {
            callerResInV2 = (ProductResultInc) target_fr;
        }
        if (target_fr instanceof GoodsdeliveryV2) {
            goodsDeliV2 = (GoodsdeliveryV2) target_fr;
        }
        if (target_fr instanceof GoodsDeliveryV4) {
            goodsDeliV4 = (GoodsDeliveryV4) target_fr;
        }
        if(frmActivity.equals("GridListViewMid"))
            grdListMid=(GridListViewMid)ctx;

        if (target_fr instanceof GoodsDeliNoReq) {
            goodsDeliNoReq = (GoodsDeliNoReq) target_fr;
        }
        if (target_fr instanceof StockTransfer) {
            stTransfer = (StockTransfer) target_fr;
        }
        if (target_fr instanceof GoodsPartial) {
            goodsPartial = (GoodsPartial) target_fr;
        }
        if (target_fr instanceof Preparation) {
            preparation = (Preparation) target_fr;
        }
        if (target_fr instanceof StockReturn) {
            stockReturn = (StockReturn) target_fr;
        }
        if (target_fr instanceof StockDiscard) {
            stockDiscard = (StockDiscard) target_fr;
        }
        if (target_fr instanceof OthersInOut) {
            othersInOut = (OthersInOut) target_fr;
        }
        if (target_fr instanceof GoodsReturn) {
            goodsReturn = (GoodsReturn) target_fr;
        }
        if (target_fr instanceof QC_Management) {
            qc_management = (QC_Management) target_fr;
        }
        if (target_fr instanceof ExchangeProduct) {
            exchangeProduct = (ExchangeProduct) target_fr;
        }
        if (target_fr instanceof StockInReq) {
            stockInReq = (StockInReq) target_fr;
        }
        if (target_fr instanceof StockOutReq) {
            stockOutRequest = (StockOutReq) target_fr;
        }
        if (target_fr instanceof StockTransferReq) {
            stTransferReq = (StockTransferReq) target_fr;
        }

    }
    //Ham nay se duoc thuc hien dau tien
    @Override
    protected void onPreExecute() {

        //dialog.setMessage("Uploading  data into server, please wait.");
        //dialog.setCancelable(false);//not close when touch
        //dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        //dialog.setProgress(0);
        //dialog.show();
    }
    //sau do toi ham doInBackgroud
    //tuyet doi khong duoc cap nhat giao dien trong ham nay
    @Override
    protected Void doInBackground(String[]... array) {
        //boolean read = ReadFileConfig();
        String result[]= array[0];
        String[] para1 = new String[0];
        String para,result11,procedure;
        System.out.print("\n\n\n record doInBackground : "+result.length);
        System.out.print("\n");
        //Using call Insert,Update sqlite when AsyncTask running
        MySQLiteOpenHelper mySQLiteOpenHelper = MySQLiteOpenHelper.getInstance(contextCha);


        for(int i=0;i<result.length;i++)
        {
            try
            {
                ///// TODO: 08/01/2016  THIS BUG ... ^^ 
                para = result[i].toString();

                para1 = para.split("\\|!");
                procedure = para1[para1.length-1];
                para = para1[0];
                if (para.endsWith("*|*")) {
                    para = para.substring(0, para.length() - 3);
                }

                // region ExchangeProduct
                if(frmActivity.equals("ExchangeProduct")){
                    if (procedure.endsWith("LG_MPOS_UPL_EXCHANGE_PROD"))
                    {
                        JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                        if (jresultkq.getTotals() > 0) {

                            boolean kq = mySQLiteOpenHelper.updateExchangeProductOK(jresultkq);

                        }
//                        resultkq = HandlerWebService.GetDataTableArg(procedure, para);
//                        if (resultkq.length > 0) {
//                                boolean kq = mySQLiteOpenHelper.updateExchangeProductOK(resultkq);
//                        }
                    }
                }
                //endregion

                //region GoodsdeliveryV2
                if (frmActivity.equals("GoodsdeliveryV2") || frmActivity.equals("GridListViewMid") || frmActivity.equals("Preparation"))
                {
                    if (procedure.endsWith("LG_MPOS_UPLOAD_GD_V2") || procedure.endsWith("LG_MPOS_UPLOAD_GD_V2_005"))
                    {
                        JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                        if (jresultkq.getTotals() > 0) {
                            String paraBC="";
                            //for(int rw=0;rw<jresultkq.getTotals();rw++) {
                                //String item_bc = resultkq[rw][7];
                                boolean kq = mySQLiteOpenHelper.updateSendGoodsDeliV2OK(jresultkq,"6");
                                //if (kq == true && resultkq[rw][14].equals("000")) {
                                //    paraBC+=item_bc + "|update*|*";

                                //}
                           // }
                            //if (paraBC.endsWith("*|*")) {
                            //    paraBC = paraBC.substring(0, paraBC.length() - 3);
                            //    result11 = HandlerWebService.InsertTableArg("LG_MPOS_UPD_GD_V2", paraBC);
                            //}

                        }
                    }

                    if (procedure.endsWith("LG_MPOS_UPL_PREPARA") ||procedure.endsWith("LG_MPOS_UPL_PREPARA_005"))
                    {

                        JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                        if (jresultkq.getTotals() > 0) {

                            boolean kq = mySQLiteOpenHelper.updateSendGoodsDeliV2OK(jresultkq,"12");

                        }
                    }
                }
                if (frmActivity.equals("GoodsDeliveryV4"))
                {
                    if(flag_update==true) {
                        flag_update = false;
                        JResultData jresultkq = HandlerWebService.GetData(procedure, 1, para);
                        if (jresultkq.getTotals() > 0) {
                            boolean kq = mySQLiteOpenHelper.updateSendGoodsDeliV4OK(jresultkq, "28");
                        }
                    }
                }
                //endregion
                //region ProductInc
                if (frmActivity.equals("ProductInc")){
                    if (procedure.endsWith("LG_MPOS_UPLOAD_INC_V2") || procedure.endsWith("LG_MPOS_UPLOAD_INC_V2_TMP")){
                       JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                        if (jresultkq.getTotals() >0) {
                            boolean kq= mySQLiteOpenHelper.updateSendIncomingOK(jresultkq);
                        }
                    }
                }
                //endregion
                //region ProductResultInc
                if (frmActivity.equals("ProductResultInc")){
                    if (procedure.endsWith("LG_MPOS_UPLOAD_RES_INC_V2") || procedure.endsWith("LG_MPOS_UPLOAD_RES_INC_V2_TMP")){
                        JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                        if (jresultkq.getTotals() >0) {
                            boolean kq= mySQLiteOpenHelper.updateSendResultIncomingOK(jresultkq);
                        }
                    }
                }
                //endregion
                //region StockInInv

                    if (procedure.endsWith("LG_MPOS_UPLOAD_INC_INV_V2"))
                    {
                        JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                        if (jresultkq.getTotals() >0) {
                            boolean kq= mySQLiteOpenHelper.updateSendIncomingInventoryOK(jresultkq);
                        }
                    }

                //endregion
//region StockInInv

                if (procedure.endsWith("LG_MPOS_PRO_MAPP_UPLOAD_3"))
                {
                    JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                    if (jresultkq.getTotals() >0) {
                        boolean kq= mySQLiteOpenHelper.updateMappingV3(jresultkq);
                    }
                }

                //endregion
                //region StockOutInv
                if (frmActivity.equals("StockOutInv"))
                {

                    if (procedure.endsWith("LG_MPOS_UPLOAD_OUT_INV_V2") || procedure.endsWith("LG_MPOS_UPLOAD_OUT_ST_V2_005"))
                    {
                        JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                        if (jresultkq.getTotals() >0) {
                            boolean kq= mySQLiteOpenHelper.updateSendOutInventoryOK(jresultkq);
                        }
                    }
                }
                //endregion

                //region ProductOut
                if (frmActivity.equals("ProductOut"))
                {

                    if (procedure.endsWith("LG_MPOS_UPL_PROD_OUT") )
                    {
                        JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                        if (jresultkq.getTotals() >0) {
                            boolean kq= mySQLiteOpenHelper.updateSendOutProductOK(jresultkq);
                        }
                    }
                }
                //endregion

                if (frmActivity.equals("GoodsDeliNoReq"))
                {
                    resultkq = HandlerWebService.GetDataTableArg(procedure, para);
                    if (resultkq.length >0) {
                        boolean kq= mySQLiteOpenHelper.updateSendGoodsDeliNoReqOK(resultkq);
                    }

                }
                if(frmActivity.equals("StockTransfer"))
                {
                    JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                    if (jresultkq.getTotals() >0) {
                        boolean kq= mySQLiteOpenHelper.updateSendStockTransferOK(jresultkq);
                    }

                }

                if (frmActivity.equals("StockTransferReq"))
                {
                    JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);

                    if (jresultkq.getTotals() >0) {
                        boolean kq= mySQLiteOpenHelper.updateSendStockTransferRequestOK(jresultkq);
                    }
                }
                if(frmActivity.equals("GoodsPartial"))
                {
                    resultkq = HandlerWebService.GetDataTableArg(procedure, para);
                    if (resultkq.length > 0) {
                        String paraBC="";
                        for(int rw=0;rw<resultkq.length;rw++) {
                            String item_bc = resultkq[rw][7];
                            boolean kq = mySQLiteOpenHelper.updateSendGoodsPartialOK(rw, resultkq);
                            if (kq == true && resultkq[rw][14].equals("000")) {
                                paraBC+=item_bc + "|update*|*";

                            }
                        }
                        if (paraBC.endsWith("*|*")) {
                            paraBC = paraBC.substring(0, paraBC.length() - 3);
                            result11 = HandlerWebService.InsertTableArg("LG_MPOS_UPD_GD_V2", paraBC);
                        }

                    }
                }
                if (frmActivity.equals("StockReturn"))
                {
                    JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                    if (jresultkq.getTotals() >0) {
                        boolean kq= mySQLiteOpenHelper.updateSendStockReturnOK(jresultkq);
                    }
                }

                if (frmActivity.equals("StockDiscard"))
                {
                    JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                    if (jresultkq.getTotals() >0) {
                        boolean kq= mySQLiteOpenHelper.updateSendStockDiscardOK(jresultkq);
                    }
                }
                if (frmActivity.equals("OthersInOut"))
                {
                    resultkq = HandlerWebService.GetDataTableArg(procedure, para);
                    if (resultkq.length >0) {
                        boolean kq= mySQLiteOpenHelper.updateSendStockOtherInOutOK(resultkq);
                    }

                }
                if (frmActivity.equals("GoodsReturn"))
                {
                    JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                    if (jresultkq.getTotals() >0) {
                        boolean kq = mySQLiteOpenHelper.updateSendGoodsReturnOK(jresultkq);
                    }
//                    resultkq = HandlerWebService.GetDataTableArg(procedure, para);
//                    if (resultkq.length >0) {
//                        boolean kq= mySQLiteOpenHelper.updateSendGoodsReturnOK(resultkq);
//                    }
                }
                if (frmActivity.equals("ReturnSupplier"))
                {
                    JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                    if (jresultkq.getTotals() >0) {
                        boolean kq = mySQLiteOpenHelper.updateSendReturnSupplierOK(jresultkq);
                    }
                }
                //region Evaluation
                if (frmActivity.equals("Evaluation"))
                {
                    JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                    if (jresultkq.getTotals() >0) {
                        String kq = mySQLiteOpenHelper.updateSendEvaluationOK(jresultkq);
                        if(kq.equals("012")){
                            evaluation.isLockWH();
                        }
                    }

                }
                //endregion

                if (frmActivity.equals("QC_Management"))
                {

                    resultkq = HandlerWebService.GetDataTableArg(procedure, para);
                    if (resultkq.length >0) {
              //          boolean kq= mySQLiteOpenHelper.updateSendReturnSupplierOK(resultkq);
                    }
                }
                if (frmActivity.equals("UpdateLocation"))
                {
                    resultkq = HandlerWebService.GetDataTableArg(procedure, para);
                    if (resultkq.length >0) {
                        boolean kq= mySQLiteOpenHelper.updateSendUpdateLocationOK(resultkq);
                    }

                }

                // Test Process
                if (frmActivity.equals("TestProcess")){
                    if (procedure.endsWith("LG_MPOS_UPLOAD_INC_V2_TEST")){
                        resultkq = HandlerWebService.GetDataTableArg(procedure, para);
                        if (resultkq.length >0) {
                            boolean kq= mySQLiteOpenHelper.updateSendTestProcessOK(resultkq);
                        }
                    }
                }

                if (frmActivity.equals("StockInReq"))
                {
                    JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);

                    if (jresultkq.getTotals() >0) {
                        boolean kq= mySQLiteOpenHelper.updateSendIncomingRequestOK(jresultkq);
                    }
                }
                if (frmActivity.equals("StockOutReq"))
                {
                    JResultData jresultkq = HandlerWebService.GetData(procedure,1, para);
                    if (jresultkq.getTotals() >0) {
                        boolean kq= mySQLiteOpenHelper.updateSendOutRequestOK(jresultkq,"25");
                    }
                }
            }
            catch(Exception ex){
                Log.e("Upload error", ex.getMessage());
            }
        }
        SystemClock.sleep(500);
        return null;
    }

    /**
     * update IU
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
        //thông qua contextCha để lấy được control trong MainActivity
        // Update the progress bar
        //int giatri=values[0];
        //dialog.setProgress(giatri);

    }

    /**
     * sau khi tien trinh thuc hien xong thi ham nay se xay ra
     */
    @Override
    protected void onPostExecute(Void result) {
        super.onPreExecute();

        if (target_fr instanceof UpdateLocation) {
            updateLocation.OnShowScanLog();
            //updateLocation.OnShowScanIn();
            updateLocation.OnShowScanAccept();
            updateLocation.CountSendRecord();
        }

        if(frmActivity.equals("Incoming")){
            callerIn.clearAllData();
            callerIn.OnShowGW();
        }

        if (target_fr instanceof ProductInc) {
            callerInV2.OnShowScanLog();
            callerInV2.OnShowScanIn();
            callerInV2.OnShowScanAccept();
            callerInV2.CountSendRecord();
        }
        if (target_fr instanceof ProductResultInc) {
            callerResInV2.OnShowScanLog();
            callerResInV2.OnShowScanIn();
            callerResInV2.OnShowScanAccept();
            callerResInV2.CountSendRecord();
        }
        if (target_fr instanceof GoodsdeliveryV2) {
            goodsDeliV2.OnShowScanLog();
            goodsDeliV2.OnShowScanIn();
            goodsDeliV2.OnShowScanAccept();
            goodsDeliV2.CountSendRecord();
        }

        if (target_fr instanceof GoodsDeliNoReq) {
            goodsDeliNoReq = (GoodsDeliNoReq) target_fr;
        }
        if(frmActivity.equals("GridListViewMid")){
            grdListMid.OnShowScanIn();
        }
        if (target_fr instanceof MappingV3) {
            mappingv3.OnShowAllLog();
           // mappingv3.CountSendRecord();
        }
        if (target_fr instanceof StockInInv) {
            inInventoryV2.OnShowScanLog();
            inInventoryV2.OnShowScanIn();
            inInventoryV2.OnShowScanAccept();
            inInventoryV2.CountSendRecord();
        }
        if (target_fr instanceof StockOutInv) {
            stockOutInv.OnShowScanLog();
            stockOutInv.OnShowScanIn();
            stockOutInv.OnShowScanAccept();
            stockOutInv.CountSendRecord();
        }

        if (target_fr instanceof GoodsDeliNoReq) {
            goodsDeliNoReq.OnShowScanLog();
            goodsDeliNoReq.OnShowScanIn();
            goodsDeliNoReq.OnShowScanAccept();
        }

        if (target_fr instanceof GoodsPartial) {
            goodsPartial.OnShowScanLog();
            goodsPartial.OnShowScanIn();
            goodsPartial.OnShowScanAccept();
            goodsPartial.CountSendRecord();
        }

        if (target_fr instanceof Preparation) {
            preparation.OnShowScanLog();
            preparation.OnShowScanIn();
            preparation.OnShowScanAccept();
            preparation.CountSendRecord();
        }

        if (target_fr instanceof StockTransfer) {
            stTransfer.OnShowScanLog();
            stTransfer.OnShowScanIn();
            stTransfer.OnShowScanAccept();
            stTransfer.CountSendRecord();
        }

        if (target_fr instanceof StockReturn) {
            stockReturn.OnShowScanLog();
            stockReturn.OnShowScanIn();
            stockReturn.OnShowScanAccept();
            stockReturn.CountSendRecord();
        }
        if (target_fr instanceof StockDiscard) {
            stockDiscard.OnShowScanLog();
            stockDiscard.OnShowScanIn();
            stockDiscard.OnShowScanAccept();
            stockDiscard.CountSendRecord();
        }

        if (target_fr instanceof OthersInOut) {
            othersInOut.OnShowScanLog();
            othersInOut.OnShowScanIn();
            othersInOut.OnShowScanAccept();
            othersInOut.CountSendRecord();
        }

        if (target_fr instanceof GoodsReturn) {
            goodsReturn.OnShowScanLog();
            goodsReturn.OnShowScanIn();
            goodsReturn.OnShowScanAccept();
            goodsReturn.CountSendRecord();
        }

        if (target_fr instanceof ReturnSupplier) {
            returnSupplier.OnShowScanLog();
            returnSupplier.OnShowScanIn();
            returnSupplier.OnShowScanAccept();
            returnSupplier.CountSendRecord();
        }
        if (target_fr instanceof ProductOut) {
            productOut.OnShowScanLog();
            productOut.OnShowScanIn();
            productOut.OnShowScanAccept();
            productOut.CountSendRecord();
        }
        if (target_fr instanceof Evaluation) {
            evaluation.OnShowScanLog();
            evaluation.OnShowScanAccept("000");
            evaluation.CountSendRecord();
        }

        if (target_fr instanceof ExchangeProduct) {
            exchangeProduct.OnShowScanLog();
            exchangeProduct.OnShowScanIn();
            exchangeProduct.OnShowScanAccept();
            exchangeProduct.CountSendRecord();
        }

        if (target_fr instanceof UpdateLocation) {
            updateLocation.OnShowScanLog();
            //updateLocation.OnShowScanIn();
            updateLocation.OnShowScanAccept();
            updateLocation.CountSendRecord();
        }

        if (target_fr instanceof StockInReq) {
            stockInReq.OnShowGridDetail();
            stockInReq.OnShowScanLog();
            stockInReq.OnShowScanIn();
            stockInReq.CountSendRecord();
        }

        if (target_fr instanceof StockOutReq) {
            stockOutRequest.OnShowGridDetail();
            stockOutRequest.OnShowScanLog();
            stockOutRequest.OnShowScanIn();
            stockOutRequest.CountSendRecord();
        }
        if (target_fr instanceof StockTransferReq) {
            stTransferReq.OnShowGridDetail();
            stTransferReq.OnShowScanLog();
            stTransferReq.OnShowScanIn();
            stTransferReq.CountSendRecord();
        }

        if (target_fr instanceof GoodsDeliveryV4) {
            flag_update=true;
            goodsDeliV4.OnShowGridDetail();
            goodsDeliV4.OnShowScanLog();
            goodsDeliV4.OnShowScanIn();
            goodsDeliV4.CountSendRecord();
        }


        this.cancel(true);

    }

    /*---------------------------------------------------------------------*/
	/*---------------------READ FILE CONFIG--------------------------------*/
	/*---------------------------------------------------------------------*/
    /*
    private boolean  ReadFileConfig()
    {
        File dir1 = Environment.getExternalStorageDirectory();
        //Get the text file
        File file = new File(dir1, "ConfigSetting.txt");
        // i have kept text.txt in the sd-card
        if(file.exists())   // check if file exist
        {
            int i=0;
            //Read text from file
            try
            {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null)
                {
                    i++;
                    switch(i)
                    {
                        case 3:
                            HandlerWebService.URL = splitContent(line);
                            break;
                        case 10:
                            HandlerWebService.dbName = splitContent(line);
                            break;
                        case 11:
                            HandlerWebService.dbUser = splitContent(line);
                            break;
                        case 12:
                            HandlerWebService.dbPwd = splitContent(line);
                            break;
                        default:
                            break;
                    }
                }
                br.close();
            }
            catch (IOException e)
            {
                //You'll need to add proper error handling here
                return false;
            }
        }
        else
            return false;

        //----------------------
        return true;
    }

    private  String splitContent(String line)
    {
        String[] result = line.split("=");
        return result[1];
    }
    */
}
