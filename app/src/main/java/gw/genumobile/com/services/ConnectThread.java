package gw.genumobile.com.services;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class ConnectThread  extends AsyncTask<String, Void, String[][]>{

	public  Activity contextCha;
	private ProgressDialog dialog;
	public Toast toast;
	private String _URL;
	private String _Company;
	private String _DBName;
	private String _DBUser;
	 public ConnectThread( String url, String company, String dbName, String dbUser)
	 {
		 _URL = url;
		 _Company = company;
		 _DBName = dbName;
		 _DBUser = dbUser;
	 }
	//constructor nay duoc truyen vao la MainActivity
	 public ConnectThread(Activity ctx)
	 {		
		 contextCha=ctx;
		 //dialog = new ProgressDialog(contextCha);
		 //toast=new Toast(contextCha);

	 }
	 @Override
     protected void onPreExecute() {
       //  Log.i(TAG, "onPreExecute");
//		dialog.setMessage("Get  data into server, please wait.");
//        dialog.setCancelable(false);//not close when touch
//        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//        dialog.setProgress(0);
//   		
//        dialog.show();
		 //Toast.makeText(contextCha, "Download finish!", Toast.LENGTH_SHORT).show();
     }
	
	@Override
	protected String[][] doInBackground(String... params) 
	{
		// TODO Auto-generated method stub
		//Read file config for WebService
		//boolean read = ReadFileConfig();//Config from txtConfig
		boolean read = ReadFileConfigV2();

		 String result[][]=new String[0][0];
 
		 String domain = params[0];
		 String[] strArray = domain.split("\\,");
		 
		 for (String str : strArray) 		 
		 System.out.println(str);
		 
		 int i = Integer.parseInt(strArray[0]);
		 
		 String Procedure = strArray[1];

		 switch(i)
		 {	               
		         case 1://use for GetDataTableArg method 
		        	 	System.out.print("*****\n***length procedure label msg in connectThread AAA:");

		    		    String para = strArray[2];	
		       	   	    result = HandlerWebService.GetDataTableArg(Procedure, para);
		       	   	    System.out.print("*****\n***length procedure label msg in connectThread:"+result.length);
		                break;		        
		         case 2://use for InsertTableArg method 
		    		    String pa = strArray[2];
					    String kq = HandlerWebService.InsertTableArg(Procedure, pa);
					    result =new String[1][1];
					    result[0][0]=kq;
		        	 	System.out.print("\n\n\n******InsertTableArg: "+result[0][0]);
		                break;
		         case 3: //use for GetDataTableNoArg method 
		        	    result = HandlerWebService.GetDataTableNoArg(Procedure);
		        	  //  System.out.print("\n\n\n******GetDataTableNoArg -- result.length: "+result.length);
		                break;
			     default:
		       	  break;	            	   
		 }
		
		//System.out.print("Hello Viet Nam: "+ result.length);
		return result;
	}
	
	
	@Override
	protected void onPostExecute(String[][] result) {
	   // TODO Update the UI thread with the final result
		super.onPreExecute();
		
		//dialog.dismiss();
		//Toast.makeText(contextCha, "Download finish!", Toast.LENGTH_SHORT).show();
	}
	
	

     @Override
     protected void onProgressUpdate(Void... values) {
       //  Log.i(TAG, "onProgressUpdate");
     }
     
     /*---------------------------------------------------------------------*/
 	/*---------------------READ FILE CONFIG--------------------------------*/
 	/*---------------------------------------------------------------------*/
	private boolean  ReadFileConfigV2()
	{
		if (_Company.isEmpty() )
			HandlerWebService.URL = "http://" + _URL + "/" + "gwWebservice.asmx";
		else
			HandlerWebService.URL = "http://" + _URL + "/" + _Company + "/gwWebservice.asmx";

		HandlerWebService.dbName = _DBName;
		HandlerWebService.dbUser = _DBUser;
		HandlerWebService.dbPwd = _DBUser + "2";
		return true;
	}
/*
 	private boolean  ReadFileConfig()
 	{ 
 		  File dir1 = Environment.getExternalStorageDirectory();
 	  	  //Get the text file  
 	      File file = new File(dir1, "ConfigSetting.txt");  
 	      // i have kept text.txt in the sd-card  		      
 	      if(file.exists())   // check if file exist  
 	      {  
 	        	int i=0; 
 	        	//Read text from file    
 	            try 
 	            {  
 	                BufferedReader br = new BufferedReader(new FileReader(file));  
 	                String line;  

 	                while ((line = br.readLine()) != null) 
 	                {  
 	                       i++;
 			               switch(i)
 			               {	               
 						          case 3:
									       HandlerWebService.URL = splitContent(line);
 						                   break;
 						          case 10:
									       HandlerWebService.dbName = splitContent(line);
 						                   break;
 						          case 11:
 						        	  	   HandlerWebService.dbUser = splitContent(line);
 						                   break;
 						          case 12:
 						        	       HandlerWebService.dbPwd = splitContent(line);
 						                   break;
							   	  case 24:
								   		Config.deleteYN = splitContent(line);
								   		  break;
							   default:
 						        	  break;	            	   
 			               	}	                   
 			         }
 			         br.close();
 	            }  
 	            catch (IOException e) 
 	            {  
 	                //You'll need to add proper error handling here  
 	            	return false;
 	            } 
 	       }
 	      else 
 	    	   return false;
 	      
 	      //----------------------
 	      return true;
 	}
 	
 	private  String splitContent(String line)
 	{
 			String[] result = line.split("=");	
 			return result[1];
 	}
 	*/
}
