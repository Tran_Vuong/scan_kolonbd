package gw.genumobile.com.services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;

import gw.genumobile.com.views.inventory.DevideBC;
import gw.genumobile.com.views.inventory.DevideBC_V2;
import gw.genumobile.com.views.inventory.Evaluation;
import gw.genumobile.com.views.inventory.ExchangeProduct;
import gw.genumobile.com.views.inventory.GoodsDeliveryV4;
import gw.genumobile.com.views.inventory.GoodsPartial;
import gw.genumobile.com.views.inventory.GoodsReturn;
import gw.genumobile.com.views.inventory.GoodsdeliveryV2;
import gw.genumobile.com.interfaces.GridListViewOnePK;
import gw.genumobile.com.views.inventory.MappingV2;
import gw.genumobile.com.views.inventory.ProductInc;
import gw.genumobile.com.views.inventory.Mapping;
import gw.genumobile.com.db.MySQLiteOpenHelper;
import gw.genumobile.com.views.inventory.OthersInOut;
import gw.genumobile.com.views.inventory.Preparation;
import gw.genumobile.com.views.inventory.ProductOut;
import gw.genumobile.com.views.inventory.ProductResultInc;
import gw.genumobile.com.views.inventory.ReturnSupplier;
import gw.genumobile.com.views.inventory.StockDiscard;
import gw.genumobile.com.views.inventory.StockInInv;
import gw.genumobile.com.views.inventory.StockInReq;
import gw.genumobile.com.views.inventory.StockOutInv;
import gw.genumobile.com.views.inventory.StockOutReq;
import gw.genumobile.com.views.inventory.StockReturn;
import gw.genumobile.com.views.inventory.StockTransfer;
import gw.genumobile.com.views.PopDialogGrid;
import gw.genumobile.com.views.inventory.StockTransferReq;

/**
 * Created by TanLegend on 03/06/2015.
 */
public class InsertUpdAsyncTask extends AsyncTask<String[], Integer, Void> {
    public Activity contextCha;
    private ProgressDialog dialog;
    GoodsDeliveryV4 goodsDeliV4;
    GoodsdeliveryV2 goodsDeliV2;
    GoodsPartial goodsPartial;
    ProductInc callerInV2;
    ProductResultInc callerInResV2;
    StockInInv inInventoryV2;
    Preparation preparation;
    StockOutInv stockOutInv;
    PopDialogGrid popDialogGrid;
    GridListViewOnePK gridListViewOnePK;
    StockTransfer stTransfer;
    StockReturn stockReturn;
    StockDiscard stockDiscard;
    OthersInOut othersInOut;
    GoodsReturn goodsReturn;
    ReturnSupplier returnSupplier;
    ProductOut productOut;
    Evaluation evaluation;
    Mapping mapping;
    MappingV2 mappingv2;
    DevideBC devideBC;
    DevideBC_V2 devideBC_V2;
    ExchangeProduct exchangeProduct;
    StockInReq stockInReq;
    StockOutReq stockOutReq;
    StockTransferReq stTransferReq;

    String frmActivity="";
    String resultkq[][]=new String[0][0];
    Fragment target_fr;
    //constructor nay duoc truyen vao la MainActivity
    public InsertUpdAsyncTask(Activity ctx){

    }
    public InsertUpdAsyncTask(Activity ctx,Fragment fr)
    {
        contextCha=ctx;
        dialog = new ProgressDialog(contextCha);

      //  frmActivity=contextCha.getLocalClassName();
        target_fr=fr;
        frmActivity= target_fr.getTag();

        if (target_fr instanceof StockInInv) {
            inInventoryV2 = (StockInInv) target_fr;
        }
        if (target_fr instanceof StockOutInv) {
            stockOutInv = (StockOutInv) target_fr;
        }
        if (target_fr instanceof Evaluation) {
            evaluation = (Evaluation) target_fr;
        }
        if (target_fr instanceof Evaluation) {
            evaluation = (Evaluation) target_fr;
        }
        if (target_fr instanceof ProductOut) {
            productOut = (ProductOut) target_fr;
        }
        if (target_fr instanceof ReturnSupplier) {
            returnSupplier = (ReturnSupplier) target_fr;
        }


        if (target_fr instanceof GoodsDeliveryV4) {
            goodsDeliV4 = (GoodsDeliveryV4) target_fr;
        }
        if (target_fr instanceof GoodsdeliveryV2) {
            goodsDeliV2 = (GoodsdeliveryV2) target_fr;
        }

        if (target_fr instanceof GoodsPartial) {
            goodsPartial = (GoodsPartial) target_fr;
        }
        if (target_fr instanceof ProductInc) {
            callerInV2 = (ProductInc) target_fr;
        }
        if (target_fr instanceof ProductResultInc) {
            callerInResV2 = (ProductResultInc) target_fr;
        }
        if (target_fr instanceof Preparation) {
            preparation = (Preparation) target_fr;
        }
        //if(frmActivity.equals("Views.PopDialogGrid"))
        //    popDialogGrid=(PopDialogGrid)ctx;
        if (target_fr instanceof PopDialogGrid) {
            popDialogGrid = (PopDialogGrid) target_fr;
        }
        if (target_fr instanceof GridListViewOnePK) {
            gridListViewOnePK = (GridListViewOnePK) target_fr;
        }


        if (target_fr instanceof StockTransfer) {
            stTransfer = (StockTransfer) target_fr;
        }
        if (target_fr instanceof StockReturn) {
            stockReturn = (StockReturn) target_fr;
        }

        if (target_fr instanceof StockDiscard) {
            stockDiscard = (StockDiscard) target_fr;
        }

        if (target_fr instanceof OthersInOut) {
            othersInOut = (OthersInOut) target_fr;
        }

        if (target_fr instanceof GoodsReturn) {
            goodsReturn = (GoodsReturn) target_fr;
        }
        if (target_fr instanceof Mapping) {
            mapping = (Mapping) target_fr;
        }
        if (target_fr instanceof MappingV2) {
            mappingv2 = (MappingV2) target_fr;
        }
        if (target_fr instanceof DevideBC) {
            devideBC = (DevideBC) target_fr;
        }
        if (target_fr instanceof DevideBC_V2) {
            devideBC_V2 = (DevideBC_V2) target_fr;
        }
        if (target_fr instanceof ExchangeProduct) {
            exchangeProduct = (ExchangeProduct) target_fr;
        }

        if (target_fr instanceof StockInReq) {
            stockInReq = (StockInReq) target_fr;
        }
        if (target_fr instanceof StockOutReq) {
            stockOutReq = (StockOutReq) target_fr;
        }
        if (target_fr instanceof StockTransferReq) {
            stTransferReq = (StockTransferReq) target_fr;
        }
    }
    //Ham nay se duoc thuc hien dau tien
    @Override
    protected void onPreExecute() {

        dialog.setMessage("Deleting data, please wait...");
        dialog.setCancelable(false);//not close when touch
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setProgress(0);
        dialog.show();
    }
    //sau do toi ham doInBackgroud
    //tuyet doi khong duoc cap nhat giao dien trong ham nay
    @Override
    protected Void doInBackground(String[]... array) {

        String result[]= array[0];
        String[] para1 = new String[0];
        String[] para2 = new String[0];
        String[] para21 = new String[0];
        String para,result11,procedure;
        System.out.print("\n\n\n record doInBackground : "+result.length);
        System.out.print("\n");
        //Using call Insert,Update sqlite when AsyncTask running
        MySQLiteOpenHelper mySQLiteOpenHelper = MySQLiteOpenHelper.getInstance(contextCha);

        for(int i=0;i<result.length;i++)
        {
            try
            {
                para = result[i].toString();
                para1 = para.split("\\|!");
                procedure = para1[para1.length-1];
                para = para1[0];
                if (para.endsWith("*|*")) {
                    para = para.substring(0, para.length() - 3);
                }
                if(procedure.equals("LG_MPOS_UPD_GD_V2")){
                    //if(frmActivity.equals("Preparation")){
                    result11= HandlerWebService.InsertTableArg(procedure, para);
                    para1 = result11.split("\\*\\|\\*",-1);
                    for(int j=0; j<para1.length;j++){
                        para2 = para1[j].split("\\|!",-1);
                        if (para2[1].equals("OK")) {
                            para21 = para2[0].split("\\|",-1);
                            mySQLiteOpenHelper.deleteBarcode(para21[0],para21[3]);
                        }

                    }

                    //}
                    //else if(frmActivity.equals("GoodsdeliveryV2")){
                    //    result11=HandlerWebService.InsertTableArg(procedure, para);
                    //}

                }
                if(procedure.equals("LG_MPOS_UPD_QTY")){
                    result11 = HandlerWebService.TableReadOpenString(procedure, para1[0]);
                    if (!result11.equals("ERROR") && result11.equals("OK")) {
                        para1 = para1[0].split("\\|",-1);
                       mySQLiteOpenHelper.updateQty(Integer.valueOf(para1[0]),Float.valueOf(para1[1]));
                    }
                }
                if(procedure.equals("LG_MPOS_UPD_INV_TR_DEL")){
                    result11=HandlerWebService.InsertTableArg(procedure, para);
                    para1 = result11.split("\\*\\|\\*",-1);
                    for(int j=0; j<para1.length;j++){
                        para2 = para1[j].split("\\|!",-1);
                        if (para2[1].equals("OK")) {
                            para21 = para2[0].split("\\|",-1);
                            mySQLiteOpenHelper.deleteBarcode(para21[0],para21[3]);
                        }

                    }

                }
            }
            catch(Exception ex){
                Log.e("Upload Delete error", ex.getMessage());
            }
        }
        SystemClock.sleep(100);
        return null;
    }

    /**
     * update IU
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
        //thông qua contextCha để lấy được control trong MainActivity
        // Update the progress bar
        //int giatri=values[0];
        //dialog.setProgress(giatri);

    }
    /**
     * sau khi tien trinh thuc hien xong thi ham nay se xay ra
     */
    @Override
    protected void onPostExecute(Void result) {
        super.onPreExecute();

        if (dialog.isShowing()) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }

        if (target_fr instanceof StockInInv) {
            inInventoryV2.onDeleteAllFinish();
        }
        if (target_fr instanceof StockOutInv) {
            stockOutInv.onDeleteAllFinish();
        }
        if (target_fr instanceof Evaluation) {
            evaluation.onDeleteAllFinish();
        }
        if (target_fr instanceof Evaluation) {
            evaluation.onDeleteAllFinish();
        }
        if (target_fr instanceof ProductOut) {
            productOut.onDeleteAllFinish();
        }
        if (target_fr instanceof ReturnSupplier) {
            returnSupplier.onDeleteAllFinish();
        }


        if (target_fr instanceof GoodsdeliveryV2) {
            goodsDeliV2.onDelAllFinish();
        }
        if (target_fr instanceof GoodsDeliveryV4) {
            goodsDeliV4.onDeleteAllFinish();
        }
        if (target_fr instanceof GoodsPartial) {
            goodsPartial.onDelAllFinish();
        }
        if (target_fr instanceof ProductInc) {
            callerInV2.onDeleteAllFinish();
        }
        if (target_fr instanceof ProductResultInc) {
            callerInResV2.onDeleteAllFinish();
        }
        if (target_fr instanceof Preparation) {
            preparation.onDelAllFinish();
        }

        if (target_fr instanceof PopDialogGrid) {
            popDialogGrid.onDeleteAllFinish();
        }
        if(target_fr instanceof GridListViewOnePK)
            gridListViewOnePK.onDeleteBCFinish();

        if (target_fr instanceof StockTransfer) {
            stTransfer.onDeleteAllFinish();
        }
        if (target_fr instanceof StockReturn) {
            stockReturn.onDeleteAllFinish();
        }

        if (target_fr instanceof StockDiscard) {
            stockDiscard = (StockDiscard) target_fr;
        }

        if (target_fr instanceof OthersInOut) {

        }

        if (target_fr instanceof GoodsReturn) {
            goodsReturn.onDeleteAllFinish();
        }
        if (target_fr instanceof Mapping) {
            mapping.onDelAllFinish();
        }
        if (target_fr instanceof MappingV2) {
            mappingv2.onDelAllFinish();
        }
        if (target_fr instanceof DevideBC) {
            devideBC.onDelAllFinish();
        }
        if (target_fr instanceof DevideBC_V2) {
            devideBC_V2.onDelAllFinish();
        }
        if (target_fr instanceof ExchangeProduct) {
            exchangeProduct.onDeleteAllFinish();
        }

        if (target_fr instanceof StockInReq) {
            stockInReq.onDeleteAllFinish();
        }

        if (target_fr instanceof StockOutReq) {
            stockOutReq.onDeleteAllFinish();
        }

        if (target_fr instanceof StockTransferReq) {
            stTransferReq.onDeleteAllFinish();
        }


        this.cancel(true);

    }


}
