package gw.genumobile.com.services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import gw.genumobile.com.interfaces.GridListViewOnePK;
import gw.genumobile.com.views.inventory.DevideBC;
import gw.genumobile.com.views.inventory.DevideBC_V2;
import gw.genumobile.com.views.inventory.Evaluation;
import gw.genumobile.com.views.inventory.ExchangeProduct;
import gw.genumobile.com.views.inventory.GoodsDeliNoReq;
import gw.genumobile.com.views.inventory.GoodsDeliveryV4;
import gw.genumobile.com.views.inventory.GoodsPartial;
import gw.genumobile.com.views.inventory.GoodsReturn;
import gw.genumobile.com.views.inventory.GoodsdeliveryV2;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.GridListViewPreparation;
import gw.genumobile.com.views.inventory.MappingV2;
import gw.genumobile.com.views.inventory.MappingV3;
import gw.genumobile.com.views.inventory.ProductInc;
import gw.genumobile.com.views.inventory.Mapping;
import gw.genumobile.com.db.MySQLiteOpenHelper;
import gw.genumobile.com.views.inventory.OthersInOut;
import gw.genumobile.com.views.inventory.Preparation;
import gw.genumobile.com.views.inventory.ProductOut;
import gw.genumobile.com.views.inventory.ProductResultInc;
import gw.genumobile.com.views.inventory.ReturnSupplier;
import gw.genumobile.com.views.inventory.StockDiscard;
import gw.genumobile.com.views.inventory.StockInInv;
import gw.genumobile.com.views.inventory.StockInReq;
import gw.genumobile.com.views.inventory.StockOutInv;
import gw.genumobile.com.views.inventory.StockOutReq;
import gw.genumobile.com.views.inventory.StockReturn;
import gw.genumobile.com.views.inventory.StockTransfer;
import gw.genumobile.com.views.inventory.StockTransferReq;
import gw.genumobile.com.views.qc_management.QC_Management;

/**
 * Created by TanLegend on 11/06/2015.
 */
public class MakeSlipAsyncTask extends AsyncTask<String[], Integer, Void> {
    SharedPreferences appPrefs;
    public Activity contextCha;
    private ProgressDialog dialog;
    ProductInc callerInV2;
    ProductResultInc callerResultInc;
    StockInInv callerInInventoryV2;
    StockOutInv callerStockOutInv;
    GoodsdeliveryV2 goodsDeliV2;
    GoodsDeliveryV4 goodsDeliveryV4;
    GoodsDeliNoReq goodsDeliNoReq;
    GridListViewBot grdListViewBot;
    StockTransfer stTransfer;
    Mapping mapping;
    MappingV2 mappingv2;
    MappingV3 mappingv3;
    GoodsPartial goodsPartial;
    Preparation preparation;
    GridListViewPreparation gridlistviewpreparation;
    StockReturn stockReturn;
    StockDiscard stockDiscard;
    GoodsReturn goodsReturn;
    OthersInOut stockOthers;
    ReturnSupplier returnSupplier;
    QC_Management qc_management;
    ProductOut productOut;
    Evaluation evaluation;
    ExchangeProduct exchangeProduct;
    StockInReq stockInReq;
    StockOutReq stockOutRequest;
    StockTransferReq stTransferReq;
    DevideBC devidebc;
    DevideBC_V2 devidebcv2;

    String frmActivity="";
    String resultkq[][]=new String[0][0];

    String[] para1 = new String[0];
    String[] parakq = new String[0];
    String[] parakq1 = new String[0];
    String[] parareturn =new  String[0];
    String para,result11,procedure,ErrStr="";
    Fragment target_fr;
    //constructor nay duoc truyen vao la MainActivity
    public MakeSlipAsyncTask(Activity ctx){}
    public MakeSlipAsyncTask(Activity ctx,Fragment fr)
    {
        contextCha=ctx;
        dialog = new ProgressDialog(contextCha);

        //frmActivity=contextCha.getLocalClassName();
        target_fr=fr;

        if (target_fr instanceof StockInInv) {
            callerInInventoryV2 = (StockInInv) target_fr;
        }
        if (target_fr instanceof StockOutInv) {
            callerStockOutInv = (StockOutInv) target_fr;
        }
        if (target_fr instanceof ProductOut) {
            productOut = (ProductOut) target_fr;
        }
        if (target_fr instanceof Evaluation) {
            evaluation = (Evaluation) target_fr;
        }
        if (target_fr instanceof ReturnSupplier) {
            returnSupplier = (ReturnSupplier) target_fr;
        }
        frmActivity= target_fr.getTag();

        if (target_fr instanceof ProductResultInc) {
            callerResultInc = (ProductResultInc) target_fr;
        }
        if (target_fr instanceof ProductInc) {
            callerInV2 = (ProductInc) target_fr;
        }
        if (target_fr instanceof GoodsdeliveryV2) {
            goodsDeliV2 = (GoodsdeliveryV2) target_fr;
        }
        if (target_fr instanceof GoodsDeliveryV4) {
            goodsDeliveryV4 = (GoodsDeliveryV4) target_fr;
        }
        if (target_fr instanceof GridListViewBot) {
            grdListViewBot = (GridListViewBot) target_fr;
        }
        if (target_fr instanceof GoodsDeliNoReq) {
            goodsDeliNoReq = (GoodsDeliNoReq) target_fr;
        }
        if (target_fr instanceof StockTransfer) {
            stTransfer = (StockTransfer) target_fr;
        }
        if (target_fr instanceof Mapping) {
            mapping = (Mapping) target_fr;
        }
        if (target_fr instanceof MappingV2) {
            mappingv2 = (MappingV2) target_fr;
        }
        if (target_fr instanceof MappingV3) {
            mappingv3= (MappingV3) target_fr;
        }
        if (target_fr instanceof GoodsPartial) {
            goodsPartial = (GoodsPartial) target_fr;
        }
        if (target_fr instanceof Preparation) {
            preparation = (Preparation) target_fr;
        }

        if(target_fr instanceof GridListViewPreparation) {
            gridlistviewpreparation = (GridListViewPreparation) target_fr;
        }

        if (target_fr instanceof StockReturn) {
            stockReturn = (StockReturn) target_fr;
        }
        if (target_fr instanceof StockDiscard) {
            stockDiscard = (StockDiscard) target_fr;
        }
        if (target_fr instanceof GoodsReturn) {
            goodsReturn = (GoodsReturn) target_fr;
        }

        if (target_fr instanceof OthersInOut) {
            stockOthers = (OthersInOut) target_fr;
        }
        if (target_fr instanceof QC_Management) {
            qc_management = (QC_Management) target_fr;
        }

        if (target_fr instanceof ExchangeProduct) {
            exchangeProduct = (ExchangeProduct) target_fr;
        }

        if (target_fr instanceof StockInReq) {
            stockInReq = (StockInReq) target_fr;
        }

        if (target_fr instanceof StockOutReq) {
            stockOutRequest = (StockOutReq) target_fr;
        }
        if (target_fr instanceof StockTransferReq) {
            stTransferReq = (StockTransferReq) target_fr;
        }
        if (target_fr instanceof DevideBC) {
            devidebc = (DevideBC) target_fr;
        }
        if (target_fr instanceof  DevideBC_V2){
            devidebcv2=(DevideBC_V2)target_fr;
        }
    }
    //Ham nay se duoc thuc hien dau tien
    @Override
    protected void onPreExecute() {

        dialog.setMessage("Uploading  data into server, please wait.");
        dialog.setCancelable(false);//not close when touch
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setProgress(0);
        dialog.show();
    }
    //sau do toi ham doInBackgroud
    //tuyet doi khong duoc cap nhat giao dien trong ham nay
    @Override
    protected Void doInBackground(String[]... array) {
        //boolean read = ReadFileConfig();
        String result[]= array[0];

        System.out.print("\n\n\n record doInBackground : "+result.length);
        System.out.print("\n");
        //Using call Insert,Update sqlite when AsyncTask running
        MySQLiteOpenHelper mySQLiteOpenHelper = MySQLiteOpenHelper.getInstance(contextCha);

        try
        {
            int autoIncomYN=0;
            for(int i=0;i<result.length;i++) {
                if(result[i]!=null) {
                    para = result[i].toString();

                    para1 = para.split("\\|!");
                    procedure = para1[para1.length - 1].toUpperCase();
                    para = para1[0];
                    if (para.endsWith("*|*")) {
                        para = para.substring(0, para.length() - 3);
                    }

                    result11 = HandlerWebService.TableReadOpenString(procedure, para);
                    //JResultData jresult = HandlerWebService.OnExcute(procedure, para);
                    SystemClock.sleep(400);

                    parakq = result11.split("\\*!");
                    for (int rw = 0; rw < parakq.length; rw++) {
                          if (parakq[rw].length()>5 && parakq[rw].substring(0,4).equalsIgnoreCase("ORA-"))
                          { ErrStr=parakq[rw]; SystemClock.sleep(500); return null;}

                        if (!parakq[rw].equals("ERROR") && parakq[rw] != "") {
                            parakq1 = parakq[rw].split("\\|!",-1);

                            if (procedure.endsWith("LG_MPOS_PRO_MAPPING")) {
                                int pk = Integer.valueOf(parakq1[2]);
                                boolean kq = mySQLiteOpenHelper.updateMapping(pk);
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_MAPP_MSLIP")) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String item_bc = parakq1[1];
                                String lotNo = parakq1[2];
                                String slipNo = parakq1[3];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipMapping(item_pk, item_bc,lotNo,slipNo);
                                if (kq) {
                                    result11=HandlerWebService.ExecuteProcedureOutPara("LG_MPOS_PRO_MAPP_LABEL", parakq1[4]);
                                  parareturn = result11.split("\\|!");
                                }
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_GD_MAKESLIP")) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String slipNo = parakq1[1];
                                int req_m_pk = Integer.valueOf(parakq1[2]);
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipGD(item_pk,req_m_pk, slipNo);
                                if (kq) {
                                    autoIncomYN++;
                                    //result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[5]+"|6 ");
                                }
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_GD_MAKESLIP_V4") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String slipNo = parakq1[1];
                                int req_m_pk = Integer.valueOf(parakq1[2]);
                                String req_no = parakq1[6];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipGDV4(item_pk, req_m_pk, slipNo, req_no);
                                if (kq) {
                                    autoIncomYN++;
                                    result11 = HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[5] + "|28 ");
                                }
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_PREPA_MSLIP")) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String slipNo = parakq1[1];
                                int req_m_pk = Integer.valueOf(parakq1[2]);
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipPreparation(item_pk, req_m_pk, slipNo);
                                if (kq) {
                                    autoIncomYN++;
                                    //result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[5]+"|6 ");
                                }
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_INC_MAKESLIP")) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String wh_pk = parakq1[1];
                                String lotNo=parakq1[2];
                                String slipNo = parakq1[3];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipProdIn(item_pk, wh_pk,lotNo, slipNo);
                                if (kq) {
                                    autoIncomYN++;
                                    //result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", slipNo+"|4 ");
                                }

                        }else if(procedure.endsWith("LG_MPOS_PRO_RES_INC_MAKESLIP")) {
                            int item_pk = Integer.valueOf(parakq1[0]);
                            String wh_pk = parakq1[1];
                            String lotNo=parakq1[2];
                            String slipNo = parakq1[3];
                            boolean kq = mySQLiteOpenHelper.updateMakeSlipProdResIn(item_pk, wh_pk,lotNo, slipNo);
                            if (kq) {
                                autoIncomYN++;
                                //result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", slipNo+"|4 ");
                            }
                        }
                            else if(procedure.endsWith("LG_MPOS_PROD_EXCHANGE_MSLIP")) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String req_m_pk = parakq1[1];
                                String lotNo=parakq1[2];
                                String slipEntry = parakq1[3];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipExchangeProd(item_pk, req_m_pk,lotNo, slipEntry);

                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_PROD_OUT_MSLIP") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String wh_pk = parakq1[1];
                                String lotNo=parakq1[2];
                                String slipNo = parakq1[3];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipProdOut(item_pk, wh_pk,lotNo, slipNo);
                                if (kq)
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", slipNo+"|18 ");
                            }
                            else if( procedure.endsWith("LG_MPOS_PRO_ST_INC_MAKESLIP")) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String wh_pk = parakq1[1];
                                String lotNo=parakq1[2];
                                String slipNo = parakq1[3];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipStockIn(item_pk, wh_pk,lotNo, slipNo);
                                if (kq)
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[4]+"|7 ");
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_ST_OUT_MAKESLIP")) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String wh_pk = parakq1[1];
                                String lotNo=parakq1[2];
                                String slipNo = parakq1[3];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipStockOut(item_pk, wh_pk,lotNo, slipNo);
                                if (kq)
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[4]+"|5 ");

                            }else if(procedure.endsWith("LG_MPOS_PRO_ST_TRANS_MSLIP") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String wh_out_pk = parakq1[1];
                                String wh_in_pk = parakq1[2];
                                String lotNo=parakq1[3];
                                String slipNo = parakq1[4];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipStockTransfer(item_pk, wh_out_pk,wh_in_pk,lotNo, slipNo);
                                if (kq) {
                                    autoIncomYN++;
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[5]+"|9");
                                }
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_GD_RETURN_MSLIP") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                int wh_pk = Integer.valueOf(parakq1[1]);
                                int cust_pk = Integer.valueOf(parakq1[2]);
                                String lotNo=parakq1[3];
                                String slipNo = parakq1[4];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipGoodsReturn(item_pk, wh_pk, cust_pk, lotNo, slipNo);
                                if (kq) {
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[5]+"|16");
                                }
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_ST_RETURN_MSLIP") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                int wh_pk = Integer.valueOf(parakq1[1]);
                                int line_pk = Integer.valueOf(parakq1[2]);
                                String lotNo=parakq1[3];
                                String slipNo = parakq1[4];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipStockReturn(item_pk, wh_pk, line_pk, lotNo, slipNo);
                                if (kq) {
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[5]+"|13");
                                }
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_ST_DISCARD_MSLIP") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                int wh_pk = Integer.valueOf(parakq1[1]);
                                String lotNo=parakq1[2];
                                String slipNo = parakq1[3];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipDiscard(item_pk, wh_pk,lotNo, slipNo);
                                if (kq) {
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[4]+"|14");
                                }
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_ST_OTHERS_MSLIP") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                int wh_pk = Integer.valueOf(parakq1[1]);
                                String slipNo =parakq1[4];
                                String lotNo = parakq1[3];
                                String line = parakq1[2];
                               // String tesst2 = parakq1[5];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipStockOthers(item_pk, wh_pk, lotNo, slipNo,line);

                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_EVALUATION_MSLIP")) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String wh_pk = parakq1[1];
                                String lotNo=parakq1[2];
                                String slipNo = parakq1[3];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipEvaluation(item_pk, wh_pk,lotNo, slipNo);
                                if (kq) {
                                    autoIncomYN++;
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_EVALUATION_OK",  parakq1[4]+"|"+wh_pk);
                                }
                            }

                            else if(procedure.endsWith("LG_MPOS_PRO_SUPPLIER_MSLIP") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String wh_pk = parakq1[1];
                                String supplier_pk = parakq1[2];
                                String lotNo=parakq1[3];
                                String slipNo = parakq1[4];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipReturnSupplier(item_pk, wh_pk,supplier_pk, lotNo, slipNo);
                                if (kq) {
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[5]+"|17");
                                }
                            }

                            else if(procedure.endsWith("LG_MPOS_PRO_ST_INC_REQ_MSLIP") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String wh_pk = parakq1[1];
                                String lotNo=parakq1[2];
                                String slipNo = parakq1[3];
                                String req_no = parakq1[5];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipStockInReq(item_pk, wh_pk, lotNo, slipNo,req_no);

                            }

                            else if(procedure.endsWith("LG_MPOS_PRO_ST_OUT_REQ_MSLIP") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String wh_pk = parakq1[1];
                                String lotNo=parakq1[2];
                                String slipNo = parakq1[3];
                                String req_no = parakq1[5];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipStockOutReq(item_pk, wh_pk, lotNo, slipNo, req_no);
                                if (kq) {
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[4]+"|24");
                                }
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_ST_TRANS_REQ_MSLIP") ) {
                                int item_pk = Integer.valueOf(parakq1[0]);
                                String wh_out_pk = parakq1[1];
                                String wh_in_pk=parakq1[2];
                                String lotNo = parakq1[3];
                                String slipNo = parakq1[4];
                                String req_no = parakq1[7];
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipStockTransferReq(item_pk, wh_out_pk, wh_in_pk, lotNo, slipNo, req_no);
                                if (kq) {
                                    result11=HandlerWebService.InsertTableArg("LG_MPOS_UPD_MAKESLIP_OK", parakq1[5]+"|23");
                                }
                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_DEVIDE_MSLIP") ) {
                                String status = parakq1[0];
                                int parent_pk = Integer.valueOf(parakq1[1]);
                                String slip_no=parakq1[2];
                                parareturn=parakq1;
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipDevideBc(parent_pk,slip_no);

                            }
                            else if(procedure.endsWith("LG_MPOS_PRO_DEVIDE_MSLIP_V2") ) {
                                String status = parakq1[0];
                                int parent_pk = Integer.valueOf(parakq1[1]);
                                String slip_no=parakq1[2];
                                parareturn=parakq1;
                                boolean kq = mySQLiteOpenHelper.updateMakeSlipDevideBcV2(parent_pk,slip_no);

                            }

                            else{
                                String slipNo = parakq1[1];
                                int pk = Integer.valueOf(parakq1[2]);
                                boolean kq = mySQLiteOpenHelper.updateMakeSlip(pk, slipNo);
                                if (kq)
                                    autoIncomYN++;
                            }
                        }
                    }
                }
            }

            //Auto Income Item GoodsDeli
            if(frmActivity.equals("GoodsdeliveryV2")) {
                if (autoIncomYN == result.length && result.length > 0 && goodsDeliV2.autoInYN == true) {
                    if (procedure.endsWith("LG_MPOS_PRO_GD_MAKESLIP")) {

                        //String value_app = parakq1[4];
                        //result11 = HandlerWebService.TableReadOpenString("lg_mpos_pro_auto_income_gd", value_app);
                        SystemClock.sleep(300);
                    }

                }
            }
            if(autoIncomYN ==result.length && result.length > 0 ){
                if (procedure.endsWith("LG_MPOS_PRO_PREPA_MSLIP")){

                    //String value_app= parakq1[4];
                    //result11 = HandlerWebService.TableReadOpenString("LG_MPOS_PRO_ST_TRANSFER_APPR",value_app);
                    //SystemClock.sleep(300);
                }

            }

            //// TODO: 4/15/2016 insert detail
            if(frmActivity.equals("QC_Management")){
                if(parakq1.length>1){
                    String pk = parakq1[0];
                    String inspect_req_m_pk = parakq1[1];


                }
            }
            /**/
        }
        catch(Exception ex){
            Log.e("Make Slip error: ", ex.getMessage());
            Log.e("With Param: ", para);
        }

        SystemClock.sleep(500);
        return null;
    }

    /**
     * update IU
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
        //thông qua contextCha để lấy được control trong MainActivity
        // Update the progress bar
        //int giatri=values[0];
        //dialog.setProgress(giatri);

    }
    /**
     * sau khi tien trinh thuc hien xong thi ham nay se xay ra
     */
    @Override
    protected void onPostExecute(Void result) {
        super.onPreExecute();

        if (dialog.isShowing()) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }
        if(ErrStr.length()>1){
            Toast toast=Toast.makeText(contextCha.getApplicationContext(), ErrStr, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            this.cancel(true);
            return;
        }
        if(frmActivity.equals("ProductInc")){
            callerInV2.OnShowScanAccept();
            callerInV2.OnPermissionApprove();
        }
        if(frmActivity.equals("ProductResultInc")){
            callerResultInc.OnShowScanAccept();
            callerResultInc.OnPermissionApprove();
        }
        if(frmActivity.equals("GoodsdeliveryV2")){
            goodsDeliV2.OnShowScanAccept();
            goodsDeliV2.OnPermissionApprove();
        }
        if(frmActivity.equals("GoodsDeliveryV4")){
            goodsDeliveryV4.OnShowGridDetail();
        }
        if(frmActivity.equals("GridListViewBot")){
            grdListViewBot.OnShowScanAccept();
        }
        if(frmActivity.equals("StockInInv")){
            callerInInventoryV2.OnShowScanAccept();
            callerInInventoryV2.OnPermissionApprove();
        }
        if(frmActivity.equals("StockOutInv")){
            callerStockOutInv.OnShowScanAccept();
            callerStockOutInv.OnPermissionApprove();
        }
        if(frmActivity.equals("GoodsDeliNoReq")){
            goodsDeliNoReq.OnShowScanAccept();

        }
        if(frmActivity.equals("StockTransfer")){
            stTransfer.OnShowScanAccept();
        }
        if(frmActivity.equals("Mapping")){
            mapping.OnShowChild();
        }
        if(frmActivity.equals("MappingV2")){
            if (parareturn[0]!="ERROR")
            mappingv2.OnShowChild(parareturn);
        }
        if(frmActivity.equals("MappingV3")){
                mappingv3.OnShowAllLog();
        }
        if(frmActivity.equals("GoodsPartial")){
            goodsPartial.OnShowScanAccept();
        }

        if(frmActivity.equals("Preparation")){
            preparation.OnShowScanAccept();
            preparation.OnPermissionApprove();
        }

        if(target_fr instanceof GridListViewPreparation){
            gridlistviewpreparation.OnShowScanAccept();
            //gridlistviewpreparation.OnPermissionApprove();
        }
        if(frmActivity.equals("StockReturn")){
            stockReturn.OnShowScanAccept();
            stockReturn.OnPermissionApprove();
        }
        if(frmActivity.equals("StockDiscard")){
            stockDiscard.OnShowScanAccept();
            stockDiscard.OnPermissionApprove();
        }
        if(frmActivity.equals("GoodsReturn")){
            goodsReturn.OnShowScanAccept();
            //goodsReturn.OnPermissionApprove();
        }
        if(frmActivity.equals("OthersInOut")){
            stockOthers.OnShowScanAccept();
        }
        if(frmActivity.equals("ReturnSupplier")){
            returnSupplier.OnShowScanAccept();
        }
        if(frmActivity.equals("ProductOut")){
            productOut.OnShowScanAccept();
    }
        if(frmActivity.equals("Evaluation")){
            evaluation.OnShowScanAccept("002");
        }
        if(frmActivity.equals("ExchangeProduct")){
            exchangeProduct.OnShowScanAccept();
        }
        if(frmActivity.equals("StockInReq")){
            stockInReq.OnShowGridDetail();
        }
        if(frmActivity.equals("StockOutReq")){
            stockOutRequest.OnShowGridDetail();
        }
        if(frmActivity.equals("StockTransferReq")){
            stTransferReq.OnShowGridDetail();
        }
        if(frmActivity.equals("DevideBC")){
            if (parareturn[0]!="ERROR")
                devidebc.OnShowChild(parareturn);
        }
        if(frmActivity.equals("DevideBC_V2")){
            if (parareturn[0]!="ERROR")
                devidebcv2.OnShowChild(parareturn);
        }
        this.cancel(true);

    }

    /*---------------------------------------------------------------------*/
	/*---------------------READ FILE CONFIG--------------------------------*/
	/*---------------------------------------------------------------------*/
    private boolean  ReadFileConfig()
    {
        File dir1 = Environment.getExternalStorageDirectory();
        //Get the text file
        File file = new File(dir1, "ConfigSetting.txt");
        // i have kept text.txt in the sd-card
        if(file.exists())   // check if file exist
        {
            int i=0;
            //Read text from file
            try
            {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null)
                {
                    i++;
                    switch(i)
                    {
                        case 3:
                            HandlerWebService.URL = splitContent(line);
                            break;
                        case 10:
                            HandlerWebService.dbName = splitContent(line);
                            break;
                        case 11:
                            HandlerWebService.dbUser = splitContent(line);
                            break;
                        case 12:
                            HandlerWebService.dbPwd = splitContent(line);
                            break;
                        default:
                            break;
                    }
                }
                br.close();
            }
            catch (IOException e)
            {
                //You'll need to add proper error handling here
                return false;
            }
        }
        else
            return false;

        //----------------------
        return true;
    }

    private  String splitContent(String line)
    {
        String[] result = line.split("=");
        return result[1];
    }
}
