package gw.genumobile.com.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;

import gw.genumobile.com.interfaces.frGridListUpload;
import gw.genumobile.com.views.inventory.Goodsdelivery;
import gw.genumobile.com.views.inventory.Incoming;
import gw.genumobile.com.views.inventory.ProductInc;
import gw.genumobile.com.db.MySQLiteOpenHelper;
import gw.genumobile.com.views.inventory.Outgoing;

public class UploadAsyncTask extends AsyncTask<String[], Integer, Void>{

	public  Activity contextCha; 
	private ProgressDialog dialog;

	Incoming callerIn;
	ProductInc callerInV2;
	Outgoing callerOut;
	Goodsdelivery goodsDeli;
	frGridListUpload frUpload;
	String frmActivity="";
	Fragment target_fr;
	//constructor nay duoc truyen vao la MainActivity
	public UploadAsyncTask(Activity ctx){}
	public UploadAsyncTask(Activity ctx,Fragment fr)
	 {		
		 contextCha=ctx;
		 target_fr=fr;
		 frmActivity= target_fr.getTag();
		 dialog = new ProgressDialog(contextCha);
		 
		 frmActivity=contextCha.getLocalClassName();

		 if(frmActivity.equals("Incoming"))
			 callerIn=(Incoming)ctx;
		 if (target_fr instanceof ProductInc) {
			 callerInV2 = (ProductInc) target_fr;
		 }
		 if(frmActivity.equals("Outgoing"))
			 callerOut=(Outgoing)ctx;
		 if(frmActivity.equals("Goodsdelivery"))
			 goodsDeli=(Goodsdelivery)ctx;
	 }
	   

	 //Ham nay se duoc thuc hien dau tien
    @Override
    protected void onPreExecute() {
    	
        dialog.setMessage("Uploading  data into server, please wait.");
        dialog.setCancelable(false);//not close when touch
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setProgress(0);
   		
        dialog.show();
    }
     
   
    //sau do toi ham doInBackgroud
    //tuyet doi khong duoc cap nhat giao dien trong ham nay
    @Override
    protected Void doInBackground(String[]... array) {
    	boolean read = ReadFileConfig();
        String result[]= array[0];
		String[] para1 = new String[0];
   		String para,result11,procedure;
   		System.out.print("\n\n\n record doInBackground : "+result.length);
   		System.out.print("\n");
   		//Using call Insert,Update sqlite when AsyncTask running
   		MySQLiteOpenHelper mySQLiteOpenHelper = MySQLiteOpenHelper.getInstance(contextCha);
   		
   		dialog.setMax(result.length);
   		
   		for(int i=0;i<result.length;i++)
		{
   			try {
				para = result[i].toString();

				if (frmActivity.equals("ProductInc"))
                {
					para1 = para.split("\\|!");
					procedure = para1[para1.length-1];

					para = "";
					for(int t = 0; t<para1.length-1;t++)
					{
						if (t == 0)
							para += para1[t].toString();
						else
							para += "|!" + para1[t].toString();
					}

					if (procedure.endsWith("lg_mpos_m020_upload_v2"))
					{
						result11 = HandlerWebService.TableReadOpenString(procedure, para);
						if (!result11.equals("ERROR")) {
							mySQLiteOpenHelper.updateInv2(Integer.valueOf(para1[13]), result11);
							//nghỉ 100 milisecond thì tiến hành update UI
							SystemClock.sleep(100);
							// Update the progress bar
							publishProgress(i + 1);
						} else {
								mySQLiteOpenHelper.updateInv2(Integer.valueOf(para1[13]), "N");
						}
					}

					if (procedure.endsWith("lg_mpos_m010_pro_inv_tr"))
					{
						result11 = HandlerWebService.TableReadOpenString(procedure, para);
						if (result11.split("\\|!")[0].toString().equals("OK"))
						{
							//result11: OK
							mySQLiteOpenHelper.updatePopTran(1, para1[0].toString(), result11);
						}
						else {
							//result11: PK|ERROR_CODE
							//ex: 10|001 Item Barcode da nhap kho
							//ex: 10|002 Item Barcode khong ton tai
							mySQLiteOpenHelper.updatePopTran(2, para1[0].toString(), result11);
							//Log.e("Upload error", result11);
						}

						//nghỉ 100 milisecond thì tiến hành update UI
						SystemClock.sleep(100);
						// Update the progress bar
						publishProgress(i + 1);
					}
				}
				else
				{
					result11 = HandlerWebService.InsertTableArg("lg_mpos_m020_upload", para);
					para1 = para.split("\\|!");
					if (result11.equals("OK")) {
						mySQLiteOpenHelper.updateInv(Integer.valueOf(para1[13]));
						//nghỉ 100 milisecond thì tiến hành update UI
						SystemClock.sleep(100);
						// Update the progress bar
						publishProgress(i + 1);
					}
				}
			}
   			catch(Exception ex){
   				Log.e("Upload error", ex.getMessage() );
   			}
		 }
   		SystemClock.sleep(500);
        return null;
    }
    /**
	 * update IU
	 */
	 @Override
	 protected void onProgressUpdate(Integer... values) {
		 // TODO Auto-generated method stub
		 super.onProgressUpdate(values);
		 //thông qua contextCha để lấy được control trong MainActivity
		// Update the progress bar
		 int giatri=values[0];
		dialog.setProgress(giatri);

	 }
    /**
   	 * sau khi tien trinh thuc hien xong thi ham nay se xay ra
   	 */
    @Override
    protected void onPostExecute(Void result) {
    	super.onPreExecute();
        if (dialog.isShowing()) {
        	try {
				Thread.sleep(1500);							
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
            dialog.dismiss();
        }
        if(frmActivity.equals("Incoming")){
        	 callerIn.clearAllData();
             callerIn.OnShowGW();
        }
        if(frmActivity.equals("ProductInc")){
        	 //callerIn.clearAllData();
             callerInV2.OnShowScanLog();
             callerInV2.OnShowScanAccept();
			 callerInV2.OnShowScanIn();
        }
		if(frmActivity.equals("Outgoing")){
			 callerOut.clearAllData();
			 callerOut.OnShowGW();
    	}
		if(frmActivity.equals("Goodsdelivery")){
			 goodsDeli.clearAllData();
			 goodsDeli.OnShowGW();
		}
		
		this.cancel(true);
		 
        
    }
    

    /*---------------------------------------------------------------------*/
	/*---------------------READ FILE CONFIG--------------------------------*/
	/*---------------------------------------------------------------------*/
	private boolean  ReadFileConfig()
	{
		  File dir1 = Environment.getExternalStorageDirectory();
	  	  //Get the text file  
	      File file = new File(dir1, "ConfigSetting.txt");  
	      // i have kept text.txt in the sd-card  		      
	      if(file.exists())   // check if file exist  
	      {  
	        	int i=0; 
	        	//Read text from file    
	            try 
	            {  
	                BufferedReader br = new BufferedReader(new FileReader(file));  
	                String line;  

	                while ((line = br.readLine()) != null) 
	                {  
	                       i++;
			               switch(i)
			               {	               
						          case 3:  
						        	       HandlerWebService.URL = splitContent(line);
						                   break;		        
						          case 10:
						        	  	   HandlerWebService.dbName = splitContent(line);
						                   break;
						          case 11: 
						        	  	   HandlerWebService.dbUser = splitContent(line);
						                   break;
						          case 12: 
						        	       HandlerWebService.dbPwd = splitContent(line);
						                   break;
						          default:
						        	  break;	            	   
			               	}	                   
			         }
			         br.close();
	            }  
	            catch (IOException e) 
	            {  
	                //You'll need to add proper error handling here  
	            	return false;
	            } 
	       }
	      else 
	    	   return false;
	      
	      //----------------------
	      return true;
	}
	
	private  String splitContent(String line)
	{
			String[] result = line.split("=");	
			return result[1];
	}
     
}
