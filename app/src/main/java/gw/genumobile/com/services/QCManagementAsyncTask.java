package gw.genumobile.com.services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;

import gw.genumobile.com.services.HandlerWebService;
import gw.genumobile.com.views.qc_management.QC_Management;

/**
 *
 */
public class QCManagementAsyncTask extends AsyncTask<String[], Integer, Void> {
    public Activity contextCha;
    private ProgressDialog dialog;
    private String pk_Master="";



    private int countRow=0;
    public String getSlipNo() {
        return SlipNo;
    }
    public void setSlipNo(String slipNo) {
        SlipNo = slipNo;
    }
    private String SlipNo="";
    QC_Management qc_management;
    String frmActivity="";

    String[] para1 = new String[0];
    String[] parakq = new String[0];
    String[] parakq1 = new String[0];
    String para,result11,procedure;
    Fragment target_fr;
    public String getParaMaster() {
        return paraMaster;
    }

    public void setParaMaster(String paraMaster) {
        this.paraMaster = paraMaster;
    }

    public String getParaDetail() {
        return paraDetail;
    }

    public void setParaDetail(String paraDetail) {
        this.paraDetail = paraDetail;
    }

    String paraMaster = "", paraDetail = "";

    public String getPk_Master() {
        return pk_Master;
    }

    public void setPk_Master(String pk_Master) {
        this.pk_Master = pk_Master;
    }

    public int getCountRow() {
        return countRow;
    }

    public void setCountRow(int countRow) {
        this.countRow = countRow;
    }

    public QCManagementAsyncTask(Activity ctx,Fragment fr)
    {
        contextCha=ctx;
        target_fr=fr;
        dialog = new ProgressDialog(contextCha);

        frmActivity=contextCha.getLocalClassName();
        if (target_fr instanceof QC_Management) {
            qc_management = (QC_Management) target_fr;
        }

    }
    public QCManagementAsyncTask(Activity ctx)
    {
        contextCha=ctx;
        dialog = new ProgressDialog(contextCha);

        frmActivity=contextCha.getLocalClassName();

    }

    @Override
    protected void onPreExecute() {

        dialog.setMessage("Uploading  data into server, please wait.");
        dialog.setCancelable(false);//not close when touch
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setProgress(0);
        dialog.show();
    }

    @Override
    protected Void doInBackground(String[]... array) {
        String result[]= array[0];

        try
        {
            int rwResult=0;
            for(int i=0;i<result.length;i++) {
                if(result[i]!=null) {
                    para = result[i].toString();

                    para1 = para.split("\\|!");
                    procedure = para1[para1.length - 1].toUpperCase();
                    para = para1[0];
                    if (para.endsWith("*|*")) {
                        para = para.substring(0, para.length() - 3);
                    }

                    result11 = HandlerWebService.TableReadOpenString(procedure, para);


                    parakq = result11.split("\\*!");

                    for (int j = 0; j < parakq.length; j++) {
                        Log.i("Result "+(j+1)+" : ", parakq[j]);
                    }

                    for (int rw = 0; rw < parakq.length; rw++) {
                        if (!parakq[rw].equals("ERROR") && parakq[rw] != "") {
                            parakq1 = parakq[rw].split("\\*");
                            if(parakq1.length == 2){
                                this.setPk_Master(parakq1[0]);
                                this.setSlipNo(parakq1[1]);
                            }
                            rwResult++;
                        }

                    }
                }
            }
            this.setCountRow(rwResult);

        }
        catch(Exception ex){
            Log.e("QC MANAGEMENT Error: ", ex.getMessage());
        }

        SystemClock.sleep(100);
        return null;
    }

    /**
     * update IU
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
        //thông qua contextCha để lấy được control trong MainActivity
        // Update the progress bar
        //int giatri=values[0];
        //dialog.setProgress(giatri);

    }
    /**
     * sau khi tien trinh thuc hien xong thi ham nay se xay ra
     */
    @Override
    protected void onPostExecute(Void result) {
        super.onPreExecute();

        if (dialog.isShowing()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }
        this.cancel(true);
    }
}