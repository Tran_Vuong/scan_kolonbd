package gw.genumobile.com.db;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;


import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.models.gwform_info;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.views.inventory.ExchangeProduct;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {
    // Instances
    private static HashMap<Context, MySQLiteOpenHelper> mInstances;

    // Member object
    private Context mContext;

    // Database metadata
    public static final String DATABASE_NAME = "gasp";
    public static final int DATABASE_VERSION = 1;

    // Table names
    public static final String TABLE_ONE = "TABLE_ONE";
    public static final String TABLE_TWO = "TABLE_TWO";

    // Create table querys
    private static final String QUERY_CREATE_TABLE_ONE = String.format(
            "CREATE TABLE IF NOT EXISTS INV_TR ( "
                    + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "TLG_POP_LABEL_PK INTEGER, " + "TR_WAREHOUSE_PK INTEGER, "
                    + "TR_LOC_ID TEXT, " + "TR_QTY INTEGER, " + "TR_LOT_NO TEXT, "
                    + "TR_TYPE TEXT, " + "ITEM_BC TEXT, " + "TR_DATE TEXT, "
                    + "TR_ITEM_PK INTEGER, " + "TR_LINE_PK INTEGER, "
                    + "ITEM_CODE TEXT, " + "ITEM_NAME TEXT, " + "ITEM_TYPE TEXT, "
                    + "TLG_GD_REQ_D_PK INTEGER, " + "WH_NAME TEXT, "
                    + "LOC_NAME TEXT, " + "LINE_NAME TEXT, " + "SLIP_NO TEXT, DEL_IF INTEGER default 0 )");

    public MySQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContext = context;

    }

    public static MySQLiteOpenHelper getInstance(Context context) {
        if (mInstances == null)
            mInstances = new HashMap<Context, MySQLiteOpenHelper>();

        if (mInstances.get(context) == null)
            mInstances.put(context, new MySQLiteOpenHelper(context));

        return mInstances.get(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //db.execSQL(QUERY_CREATE_TABLE_ONE);
        //db.execSQL(QUERY_CREATE_TABLE_TWO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO Upgrade your database here

    }

    @Override
    public synchronized SQLiteDatabase getReadableDatabase() {
        return super.getReadableDatabase();
    }

    /////////////////////////////////////////////////////////////////////////////////////
    //SQL DATA
    ////////////////////////////////////////////////////////////////////////////////////
    public boolean isExistBarcode(String l_bc_item, String type) {
        boolean flag = false;
        SQLiteDatabase db = getReadableDatabase();
        try {
            //String countQuery = "SELECT PK FROM INV_TR where tr_type='"+type+"'  AND del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";
            //String countQuery = "SELECT PK FROM INV_TR where  tr_type='"+type+"' AND (status='000' or status=' ') and del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";
            String countQuery = "SELECT PK FROM INV_TR where  tr_type='" + type + "' AND (status='000' or status=' ')  and ITEM_BC ='" + l_bc_item + "' ";
            Cursor cursor = db.rawQuery(countQuery, null);
            if (cursor != null && cursor.moveToFirst()) {
                flag = true;
            }
            cursor.close();
        } catch (Exception ex) {
            //Toast.makeText(this, "Check_Exist_PK :" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
        }
        return flag;
    }

    public boolean isMakeSlip(String type) {
        boolean flag = false;
        SQLiteDatabase db = getReadableDatabase();
        try {
            String countQuery = "SELECT PK FROM INV_TR where  tr_type='" + type + "' AND status='000' and del_if=0 and SLIP_NO IN('-','',null)  ";
            Cursor cursor = db.rawQuery(countQuery, null);
            if (cursor != null && cursor.moveToFirst()) {
                flag = true;
            }
            cursor.close();
        } catch (Exception ex) {
            //Toast.makeText(this, "Check_Exist_PK :" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
        }
        return flag;
    }

    public boolean isMakeSlipStatus(String type, String status) {
        boolean flag = false;
        SQLiteDatabase db = getReadableDatabase();
        try {
            String countQuery = "SELECT PK FROM INV_TR where  tr_type='" + type + "' AND status='" + status + "' and del_if=0 and SLIP_NO IN('-','') ";
            Cursor cursor = db.rawQuery(countQuery, null);
            if (cursor != null && cursor.moveToFirst()) {
                flag = true;
            }
            cursor.close();
        } catch (Exception ex) {
            //Toast.makeText(this, "Check_Exist_PK :" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
        }
        return flag;
    }

    public boolean isCheckDelete(String type) {
        boolean flag = false;
        SQLiteDatabase db = getReadableDatabase();
        try {
            String countQuery = "SELECT PK FROM INV_TR where  tr_type='" + type + "' ";
            Cursor cursor = db.rawQuery(countQuery, null);
            if (cursor != null && cursor.moveToFirst()) {
                flag = true;
            }
            cursor.close();
        } catch (Exception ex) {
            //Toast.makeText(this, "Check_Exist_PK :" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            db.close();
        }
        return flag;
    }

    //region -------------COUNT and QTY------------------
    public int CountGridLog(String type) {
        int countRow = 0;
        SQLiteDatabase dba = getReadableDatabase();
        try {
            String sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + type + "' and SENT_YN='N' ; ";
            Cursor cur = dba.rawQuery(sql, null);
            if (cur != null && cur.moveToFirst()) {
                countRow = cur.getCount();
            }
            cur.close();
        } catch (Exception ex) {
            Toast.makeText(mContext, "Count Grid Log:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            dba.close();
        }
        return countRow;
    }

    public String[] CountGridAccept(String type, String scan_date) {
        String[] data = new String[2];
        int count = 0;
        float qty = 0;

        SQLiteDatabase dba = getReadableDatabase();
        try {
            String sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '" + type + "' AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND STATUS='000'";
            Cursor cur = dba.rawQuery(sql, null);
            if (cur != null && cur.moveToFirst()) {
                count = cur.getCount();
                for (int i = 0; i < count; i++) {
                    qty = qty + Float.valueOf(cur.getString(cur.getColumnIndex("TR_QTY")));
                    cur.moveToNext();
                }
            }
            data[0] = String.valueOf(count);
            data[1] = String.valueOf(qty);

            cur.close();
        } catch (Exception ex) {
            Toast.makeText(mContext, "Count Grid Accept:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            dba.close();
        }
        return data;
    }

    public int GetTimeAsyncData() {
        int time = 3;
        SQLiteDatabase dba = getReadableDatabase();
        try {
            String sql = "Select * FROM TR_CONFIG  where del_if=0";
            Cursor cur = dba.rawQuery(sql, null);
            if (cur != null && cur.moveToFirst()) {
                time = Integer.parseInt(cur.getString(cur.getColumnIndex("TIME_UPLOAD")));
            }
            cur.close();
        } catch (Exception ex) {
            Toast.makeText(mContext, "SQL get time:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            dba.close();
        }
        return time;
    }

    public int CountRequest(String formID) {

        String scan_date = CDate.getDateyyyyMMdd();
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
        int countRow = 0;
        SQLiteDatabase dba = getReadableDatabase();
        try {
            //String sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+type+"' and SENT_YN='N' ; ";
            String sql = " select DISTINCT TLG_GD_REQ_M_PK, GD_SLIP_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' " +
                    " AND SCAN_DATE >" + date_previous + " AND SENT_YN = 'Y' AND STATUS='000' ";
            Cursor cur = dba.rawQuery(sql, null);
            if (cur != null && cur.moveToFirst()) {
                countRow = cur.getCount();
            }
            cur.close();
        } catch (Exception ex) {
            Toast.makeText(mContext, "Count Grid Log:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            dba.close();
        }
        return countRow;
    }

    public float QtyRequest(String type, Integer pk) {
        int count = 0;
        float qty = 0;
        String scan_date = CDate.getDateyyyyMMdd();
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
        SQLiteDatabase dba = getReadableDatabase();
        try {
            String sql = "select TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '" + type + "' " +
                    "AND TLG_GD_REQ_M_PK='" + pk + "'   AND SCAN_DATE >" + date_previous + " AND SENT_YN = 'Y' AND STATUS='000'";
            Cursor cur = dba.rawQuery(sql, null);
            if (cur != null && cur.moveToFirst()) {
                count = cur.getCount();
                for (int i = 0; i < count; i++) {
                    qty = qty + Float.valueOf(cur.getString(cur.getColumnIndex("TR_QTY")));
                    cur.moveToNext();
                }
            }

            cur.close();
        } catch (Exception ex) {
            Toast.makeText(mContext, "Count Grid Accept:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            dba.close();
        }
        return qty;
    }

    //endregion
    //region -------------UPDATE-------------------
    public boolean updateInv(Integer id) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set DEL_IF=" + id + "  where PK = " + id;
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateInv2(Integer id, String mystr) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        if (mystr.endsWith("N"))
            sql = "UPDATE INV_TR set SENT_YN = 'N'  where PK = " + id;
        else
            sql = "UPDATE INV_TR set SENT_YN = 'Y', SENT_TIME = '" + sent_time + "', TLG_POP_INV_TR_PK = '" + mystr + "'  where PK = " + id;


        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateSendOK(Integer id, String mystr) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        if (mystr.endsWith("N"))
            sql = "UPDATE INV_TR set SENT_YN = 'N'  where PK = " + id;
        else
            sql = "UPDATE INV_TR set SENT_YN = 'Y', SENT_TIME = '" + sent_time + "', TLG_POP_INV_TR_PK = '" + mystr + "'  where PK = " + id;

        db.execSQL(sql);
        db.close();

        return true;
    }

    public boolean updatePopTran(int type, String id, String mystr) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        String tmpId[] = new String[0];
        int countId = 0;
        switch (type) {
            case 1: // OK

                sql = "UPDATE INV_TR set STATUS = '" + mystr.split("\\|!")[0].toString() + "' , SLIP_NO =  '" + mystr.split("\\|!")[1].toString() + "' where TLG_POP_INV_TR_PK IN (" + id + " )";
                db.execSQL(sql);
                /*
                for(int i = 0; i < countId; i++) {
                    sql = "UPDATE INV_TR set STATUS = '" + mystr + "' where TLG_POP_INV_TR_PK IN ( " + tmpId[i].toString() + ")";
                    db.execSQL(sql);
                }
                */
                break;

            case 2: // ERROR

                if (mystr.split("\\|!")[0].toString().equals("001")) {
                    sql = "UPDATE INV_TR set STATUS = '" + mystr.split("\\|!")[0] + "'"
                            + " , SLIP_NO = '" + mystr.split("\\|!")[2] + "'"
                            + " , INCOME_DATE = '" + mystr.split("\\|!")[3] + "'"
                            + " , CHARGER = '" + mystr.split("\\|!")[4] + "'"
                            + " , WH_NAME = '" + mystr.split("\\|!")[5] + "'"
                            + " , LINE_NAME = '" + mystr.split("\\|!")[6] + "'"
                            + " where TLG_POP_INV_TR_PK IN ('" + mystr.split("\\|!")[1] + "')";
                }

                db.execSQL(sql);

                break;
        }
        db.close();

        return true;
    }

//endregion

    //region -------------UPDATE EXECKEY------------
    public boolean updateExeckeyProd(String formID, Integer item_pk, String wh_pk, String line_pk, String lot_no, String saleorder_d_pk, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TR_WH_IN_PK = '" + wh_pk + "' and" +
                "        TR_LINE_PK = '" + line_pk + "' and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        TLG_SA_SALEORDER_D_PK = '" + saleorder_d_pk + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateExeckeyExchangeProd(String formID, Integer item_pk, String from_saleorder, String to_saleorder, String ex_req_d_pk, String req_m_pk, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TLG_GD_REQ_M_PK = " + req_m_pk + " and" +
                "        TLG_SA_SALEORDER_D_PK = '" + from_saleorder + "' and" +
                "        SUPPLIER_PK = '" + to_saleorder + "' and" +
                "        TLG_GD_REQ_D_PK = '" + ex_req_d_pk + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateExeckeyGoodsDeli(String formID, Integer item_pk, Integer req_m_Pk, Integer saleorder_d_pk, String lot_no, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TLG_GD_REQ_M_PK = '" + req_m_Pk + "' and" +
                "        TLG_SA_SALEORDER_D_PK = '" + saleorder_d_pk + "' and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }
    public boolean updateExeckeyGoodsDeli(String formID, Integer item_pk, String req_m_Pk, String saleorder_d_pk, String lot_no, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TLG_GD_REQ_M_PK = '" + req_m_Pk + "' and" +
                "        TLG_SA_SALEORDER_D_PK = '" + saleorder_d_pk + "' and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateExeckeyIncome(String formID, Integer item_pk, String wh_pk, String supplier_pk, String lot_no, String po_d_pk, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TR_WH_IN_PK = '" + wh_pk + "' and" +
                "        SUPPLIER_PK = '" + supplier_pk + "' and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        TLG_PO_PO_D_PK = '" + po_d_pk + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateExeckeyStockOutReq(String formID, Integer item_pk, String wh_pk, String lot_no, String po_d_pk, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TR_WH_IN_PK = '" + wh_pk + "' and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        TLG_PO_PO_D_PK = '" + po_d_pk + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "       ITEM_BC IS NOT NULL AND" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateExeckeyOutInventory(String formID, Integer item_pk, String wh_pk, String supplier_pk, String lot_no, String po_d_pk, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TR_WH_OUT_PK = '" + wh_pk + "' and" +
                "        SUPPLIER_PK = '" + supplier_pk + "' and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        TLG_PO_PO_D_PK = '" + po_d_pk + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateExeckeyStockDiscard(String formID, Integer item_pk, Integer wh_pk, String lot_no, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TR_WH_OUT_PK = " + wh_pk + " and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateExeckeyStockReturn(String formID, Integer item_pk, String wh_pk, String lot_no, String line_pk, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TR_WH_IN_PK = '" + wh_pk + "' and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        TR_LINE_PK = '" + line_pk + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateExeckeyGoodsReturn(String formID, Integer item_pk, String wh_pk, String cust_pk, String lot_no, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TR_WH_IN_PK = " + wh_pk + " and" +
                "        CUST_PK = " + cust_pk + " and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +

                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateExeckeyStTransfer(String formID, Integer item_pk, String wh_pk, String in_wh_pk, String lot_no, String bc_type, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TR_WH_OUT_PK = '" + wh_pk + "' and" +
                "        TR_WH_IN_PK = '" + in_wh_pk + "' and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        BC_TYPE = '" + bc_type + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateExeckeyMapping(String formID, Integer item_pk, String item_bc, String lot_no, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        ITEM_BC = '" + item_bc + "' and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }
    public boolean updateExeckeyDevide(String formID, Integer parent_pk,  String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where parent_pk = " + parent_pk + " and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }
    //endregion

    //region------------UPDATE MAKE SLIP----------------
    public boolean updateMakeSlipMapping(Integer item_pk, String bc, String lotNo, String slipNO) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "',MAPPING_YN='Y'  " +
                " where TR_TYPE='"+gwform_info.MappingV2.Id()+"' AND SLIP_NO IN('-',' ',null) and ITEM_BC='" + bc + "' and TR_ITEM_PK=" + item_pk  ;
        db.execSQL(sql);
        db.close();
        return true;
    }


    public boolean updateMakeSlipGD(Integer item_pk, Integer req_m_pk, String slipNO) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where SLIP_NO IN('-','') and TR_ITEM_PK=" + item_pk + " and TLG_GD_REQ_M_PK= '" + req_m_pk +"'";
        db.execSQL(sql);
        db.close();

        return true;
    }

    public boolean updateMakeSlipGDV4(Integer item_pk, Integer req_m_pk, String slipNO, String req_no) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where SLIP_NO IN('-','') and TR_ITEM_PK=" +
                item_pk + " and TR_TYPE='" + gwform_info.GoodsDeliveryV4.Id() +"' and TLG_GD_REQ_M_PK= '" + req_m_pk +
                "' and REQ_NO = '" + req_no + "'";
        db.execSQL(sql);
        db.close();

        return true;
    }

    public boolean updateMakeSlipPreparation(Integer item_pk, Integer req_m_pk, String slipNO) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where TR_TYPE='"+gwform_info.Preparation.Id()+"' and SLIP_NO IN('-','') and TR_ITEM_PK=" + item_pk + " and TLG_GD_REQ_M_PK= '" + req_m_pk+"'";
        db.execSQL(sql);
        db.close();

        return true;
    }

    public boolean updateMakeSlip(Integer id, String slipNO) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where PK = " + id;
        db.execSQL(sql);
        db.close();

        return true;
    }

    public boolean updateMakeSlipProdIn(Integer item_pk, String wh_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where SLIP_NO IN('-','') AND TR_TYPE='"+gwform_info.ProductInc.Id()+"' and TR_ITEM_PK=" + item_pk + " and TR_WH_IN_PK= '" + wh_Pk + "' and trim(TR_LOT_NO)='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    public boolean updateMakeSlipProdResIn(Integer item_pk, String wh_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where SLIP_NO IN('-','') AND TR_TYPE='"+gwform_info.ProductResultInc.Id()+"' and TR_ITEM_PK=" + item_pk + " and TR_WH_IN_PK= '" + wh_Pk + "' and trim(TR_LOT_NO)='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    public boolean updateMakeSlipStockTransferReq(Integer item_pk, String wh_out_pk, String wh_in_pk, String lotNo, String slipNO, String req_no) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            //gwform_info.StockOutReq.Id();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where TR_TYPE='"+gwform_info.StockTransferReq.Id()+"' AND SLIP_NO IN('-','')  and TR_ITEM_PK=" + item_pk + " and TR_WH_OUT_PK= '" + wh_out_pk + "' and REQ_NO= '" + req_no + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    public boolean updateMakeSlipDevideBc(Integer parent_pk, String slip_no) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            //gwform_info.StockOutReq.Id();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slip_no +  "',MAPPING_YN='Y'   where TR_TYPE='"+gwform_info.DevideBC.Id()+"' AND SLIP_NO IN('-','')  and PARENT_PK=" + parent_pk ;
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    public boolean updateMakeSlipDevideBcV2(Integer parent_pk, String slip_no) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            //gwform_info.StockOutReq.Id();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slip_no +  "',MAPPING_YN='Y'   where TR_TYPE='"+gwform_info.DevideBC_V2.Id()+"' AND SLIP_NO IN('-','')  and PARENT_PK=" + parent_pk ;
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    public boolean updateMakeSlipExchangeProd(Integer item_pk, String req_m_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where SLIP_NO IN('-','') AND TR_TYPE='"+gwform_info.ExchangeProduct.Id()+"' and TR_ITEM_PK=" + item_pk + " and TLG_GD_REQ_M_PK= '" + req_m_Pk + "' and trim(TR_LOT_NO)='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMakeSlipEvaluation(Integer item_pk, String wh_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where SLIP_NO IN('-','') AND TR_TYPE='"+gwform_info.Evaluation.Id()+"' and TR_ITEM_PK=" + item_pk + " and TR_WH_IN_PK= '" + wh_Pk + "' and trim(TR_LOT_NO)='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMakeSlipProdOut(Integer item_pk, String wh_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where SLIP_NO IN('-','') AND TR_TYPE='"+gwform_info.ProductOut.Id()+"' and TR_ITEM_PK=" + item_pk + " and TR_WH_OUT_PK= '" + wh_Pk + "' and trim(TR_LOT_NO)='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMakeSlipStockIn(Integer item_pk, String wh_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where SLIP_NO IN('-','') AND TR_TYPE='"+gwform_info.StockInInv.Id()+"' and TR_ITEM_PK=" + item_pk + " and TR_WH_IN_PK= '" + wh_Pk + "' and trim(TR_LOT_NO)='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMakeSlipStockOut(Integer item_pk, String wh_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where SLIP_NO IN('-','') AND TR_TYPE='"+gwform_info.StockOutInv.Id()+"' and TR_ITEM_PK=" + item_pk + " and TR_WH_OUT_PK= '" + wh_Pk + "' and trim(TR_LOT_NO)='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMakeSlipStockOthers(Integer item_pk, Integer wh_Pk, String lotNo, String slipNO, String line) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where  TR_TYPE='"+gwform_info.OthersInOut.Id()+"' AND SLIP_NO IN('-','') and TR_ITEM_PK=" + item_pk + " and TR_WH_IN_PK= '" + wh_Pk + "' and TR_LINE_PK='" + line + "' and TR_LOT_NO='" + lotNo + "'";
            db.execSQL(sql);

             db.close();

            return true;
        } catch (Exception ex) {
            return false;

        }
    }

    public boolean updateMakeSlipReturnSupplier(Integer item_pk, String wh_Pk, String supplier_pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where TR_TYPE='"+gwform_info.ReturnSupplier.Id()+"' AND SLIP_NO IN('-','')  and TR_ITEM_PK=" + item_pk + " and TR_WH_IN_PK= '" + wh_Pk + "' and SUPPLIER_PK=" + supplier_pk + " and TR_LOT_NO='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMakeSlipStockInReq(Integer item_pk, String wh_Pk, String lotNo, String slipNO, String req_no) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where TR_TYPE='"+gwform_info.StockInReq.Id()+"' AND SLIP_NO IN('-','')  and TR_ITEM_PK=" + item_pk + " and TR_WH_IN_PK= '" + wh_Pk + "' and REQ_NO= '" + req_no + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMakeSlipStockOutReq(Integer item_pk, String wh_Pk, String lotNo, String slipNO, String req_no) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            //gwform_info.StockOutReq.Id();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where TR_TYPE='"+gwform_info.StockOutReq.Id()+"' AND SLIP_NO IN('-','')  and TR_ITEM_PK=" + item_pk + " and TR_WH_OUT_PK= '" + wh_Pk + "' and REQ_NO= '" + req_no + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }


    public boolean updateMakeSlipDiscard(Integer item_pk, Integer wh_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  where SLIP_NO IN('-','') AND TR_TYPE='"+gwform_info.StockDiscard.Id()+"' and TR_ITEM_PK=" + item_pk + " and TR_WH_OUT_PK= '" + wh_Pk + "' and trim(TR_LOT_NO)='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMakeSlipStockTransfer(Integer item_pk, String wh_out_Pk, String wh_in_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  " +
                    " where TR_TYPE='"+gwform_info.StockTransfer.Id()+"' AND SLIP_NO IN('-','') and TR_ITEM_PK=" + item_pk + " " +
                    " and TR_WH_OUT_PK= '" + wh_out_Pk + "' and TR_WH_IN_PK='" + wh_in_Pk + "' and TR_LOT_NO='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMakeSlipGoodsReturn(Integer item_pk, Integer wh_Pk, Integer cust_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  " +
                    "where TR_TYPE='"+gwform_info.GoodsReturn.Id()+"' AND SLIP_NO IN('-','') and TR_ITEM_PK=" + item_pk + " " +
                    "and TR_WH_IN_PK= '" + wh_Pk + "' and CUST_PK='" + cust_Pk + "' and TR_LOT_NO='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMakeSlipStockReturn(Integer item_pk, Integer wh_Pk, Integer line_Pk, String lotNo, String slipNO) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNO + "'  " +
                    "where TR_TYPE='"+gwform_info.StockReturn.Id()+"' AND SLIP_NO IN('-','') and TR_ITEM_PK=" + item_pk + " " +
                    "and TR_WH_IN_PK= '" + wh_Pk + "' and TR_LINE_PK='" + line_Pk + "' and TR_LOT_NO='" + lotNo + "'";
            db.execSQL(sql);

            db.close();

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean updateMapping(Integer id) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR SET MAPPING_YN='Y'  WHERE PK=" + id;
        db.execSQL(sql);
        db.close();

        return true;
    }

    public boolean updateQty(Integer id, float qty) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set TR_QTY=" + qty + "  where  PK = " + id;
        db.execSQL(sql);

        db.close();

        return true;
    }

    //endregion
    //region -----------UPDATE SEND DO START--------------
    public boolean updateExchangeProductOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String sent_time = df.format(c.getTime());
        String scan_date = CDate.getDateyyyyMMdd();
        String scan_time = CDate.getDateYMDHHmmss();

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);
        int row = dt.totalrows;
        for (int i = 0; i < row; i++) {
            String item_bc = dt.records.get(i).get("item_bc").toString();
            String item_code = dt.records.get(i).get("item_code").toString();
            String item_name = dt.records.get(i).get("item_name").toString();
            float _req_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());
            String lot_no = dt.records.get(i).get("lot_no").toString();
            String label_pk = dt.records.get(i).get("label_pk").toString();
            int _item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());
            String _status = dt.records.get(i).get("status").toString();
            String _slip_no = dt.records.get(i).get("slip_no").toString();
            String _income_date = dt.records.get(i).get("income_dt").toString();
            String _crt_by = dt.records.get(i).get("crt_by").toString();
            String _wh_name =dt.records.get(i).get("wh_name").toString();
            String _charger = dt.records.get(i).get("cust_pk").toString();
            String _line_name = dt.records.get(i).get("line_name").toString();
            float _unit_price = Float.parseFloat(dt.records.get(i).get("unit_price").toString());
            String _uom = dt.records.get(i).get("uom").toString();
            String tlg_req_exchange_d_pk = dt.records.get(i).get("req_exchange_d_pk").toString();
            String pkID = dt.records.get(i).get("inv_tr_pk").toString();
            String _to_item_pk = dt.records.get(i).get("to_item_pk").toString();
            String _to_uom = dt.records.get(i).get("to_uom").toString();
            String _to_lot_no = dt.records.get(i).get("to_lot_no").toString();
            String _to_uom_qty = dt.records.get(i).get("exchange_uom_qty").toString();
            String _prod_d_pk = dt.records.get(i).get("income_d_pk").toString();
            if (_status.equals("000")) {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + _req_qty
                        + ", TR_ITEM_PK = " + _item_pk
                        + ", TLG_GD_REQ_D_PK = " + tlg_req_exchange_d_pk

                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', LINE_NAME = '" + _line_name
                        + "', TO_ITEM_PK = '" + _to_item_pk
                        + "', TO_UOM = '" + _to_uom
                        + "', TO_LOTNO = '" + _to_lot_no
                        + "', TO_UOM_QTY = '" + _to_uom_qty
                        + "', PROD_D_PK = '" + _prod_d_pk
                        + "', UNIT_PRICE = " + _unit_price
                        + "  where PK = " + pkID;
            } else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + _req_qty
                        + ", TR_ITEM_PK = " + _item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', TR_WH_IN_NAME = '" + _wh_name
                        + "', LINE_NAME = '" + _line_name

                        + "'  where PK = " + pkID;
            }
            db.execSQL(sql);
        }
        db.close();
        return true;
    }

    public boolean updateSendGoodsDeliV2OK(JResultData ja ,String formID) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String sent_time = df.format(c.getTime());
        String scan_date = CDate.getDateyyyyMMdd();
        String scan_time = CDate.getDateYMDHHmmss();

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        List<JDataTable> jdt = ja.getListODataTable();
        JDataTable dt = jdt.get(0);
        for (int i = 0; i < dt.totalrows; i++) {
            String slipNO = dt.records.get(i).get("slip_no").toString();//SLIP_NO
            String tlg_gd_req_d_pk = dt.records.get(i).get("tlg_gd_req_d_pk").toString();//TLG_GD_REQ_D_PK
            int req_item_pk = Integer.parseInt(dt.records.get(i).get("req_item_pk").toString());//REQ_ITEM_PK
            float req_qty = Float.parseFloat(dt.records.get(i).get("req_qty").toString());//REQ_QTY
            String out_wh_pk = dt.records.get(i).get("out_wh_pk").toString();//OUT_WH_PK
            int tlg_pop_lable_pk = Integer.parseInt(dt.records.get(i).get("tlg_pop_label_pk").toString());//TLG_POP_LABEL_PK
            int tlg_it_item_pk = Integer.parseInt(dt.records.get(i).get("tlg_it_item_pk").toString());//tlg_it_item_pk
            String item_bc = dt.records.get(i).get("item_bc").toString();//item_bc
            String label_uom = dt.records.get(i).get("uom").toString();//label_uom
            String yymmdd = dt.records.get(i).get("yymmdd").toString();//yymmdd
            String item_code = dt.records.get(i).get("item_code").toString();//item_code
            String item_name = dt.records.get(i).get("item_name").toString();//item_name
            String item_type = dt.records.get(i).get("item_type").toString();//item_type
            float label_qty = Float.parseFloat(dt.records.get(i).get("label_qty").toString());//label_qty
            String status = dt.records.get(i).get("status").toString();//status
            String cust_pk = dt.records.get(i).get("tco_buspartner_pk").toString();//TCO_BUSPARTNER_PK
            String tlg_sa_saleorder_d_pk = dt.records.get(i).get("tlg_sa_saleorder_d_pk").toString();//TLG_SA_SALEORDER_D_PK
            String lotNO = dt.records.get(i).get("lot_no").toString();//lot_no
            String tlg_gd_req_m_pk = dt.records.get(i).get("tlg_gd_req_m_pk").toString();//TLG_GD_REQ_M_PK
            String income_date =dt.records.get(i).get("income_dt").toString();//INCOME_DATE
            String wh_name = dt.records.get(i).get("warehouse").toString();//WAREHOUSE
            String charger = dt.records.get(i).get("charger").toString();//CHARGER
            int pk = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());///PK return
            String _parent_pk = dt.records.get(i).get("parent_pk").toString();//PARENT_PK
            if (status.equals("008")) {
                sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time + "', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                        item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + req_qty + ",TLG_GD_REQ_M_PK='" + tlg_gd_req_m_pk + "', TLG_GD_REQ_D_PK = '" + tlg_gd_req_d_pk + "', TR_ITEM_PK = " +
                        tlg_it_item_pk + ", TR_WH_OUT_PK = " + out_wh_pk + ",TR_WH_OUT_NAME='" + wh_name + "',CHARGER='" + charger + "',INCOME_DATE='" + income_date + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                        label_uom + "',TLG_SA_SALEORDER_D_PK='" + tlg_sa_saleorder_d_pk + "',CUST_PK='" + cust_pk + "', SLIP_NO = '" + slipNO + "'  where STATUS=' ' and PK = " + pk;
                db.execSQL(sql);
            } else {
                if (status.equals("000") && !_parent_pk.equals("")) {
                    boolean kq = isExistBarcode(item_bc, formID);//check Exits
                    if (!kq) {
                        sql = "INSERT INTO INV_TR(ITEM_BC,TR_WH_OUT_PK,TR_WH_OUT_NAME,GD_SLIP_NO, CUST_PK,TLG_GD_REQ_M_PK,SLIP_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE, " +
                                "TLG_POP_LABEL_PK,ITEM_CODE,ITEM_NAME,TR_QTY,TR_ITEM_PK,CHARGER,TR_LOT_NO,UOM,TLG_SA_SALEORDER_D_PK,TLG_GD_REQ_D_PK ) "
                                + "VALUES('"
                                + item_bc + "','"
                                + out_wh_pk + "','"
                                + wh_name + "','"
                                + slipNO + "','"
                                + cust_pk + "','"
                                + tlg_gd_req_m_pk + "','"
                                + "-" + "','"
                                + scan_date + "','"
                                + scan_time + "','"
                                + "Y" + "','"
                                + status + "','"
                                + formID + "',"
                                + tlg_pop_lable_pk + ",'"
                                + item_code + "','"
                                + item_name + "',"
                                + req_qty + ","
                                + tlg_it_item_pk + ",'"
                                + charger + "','"
                                + lotNO + "','"
                                + label_uom + "','"
                                + tlg_sa_saleorder_d_pk + "','"
                                + tlg_gd_req_d_pk + "'"
                                + ")";
                        db.execSQL(sql);

                        //update parent bc
                        sql = "UPDATE INV_TR set STATUS=' '"
                                + ", DEL_IF = " + pk
                                + ",SENT_YN = 'Y'"
                                + "  where PK = " + pk;
                        db.execSQL(sql);
                    }

                } else {
                    sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time + "', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                            item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + req_qty + ",TLG_GD_REQ_M_PK='" + tlg_gd_req_m_pk + "', TLG_GD_REQ_D_PK = '" + tlg_gd_req_d_pk + "', TR_ITEM_PK = " +
                            tlg_it_item_pk + ", TR_WH_OUT_PK = '" + out_wh_pk + "',TR_WH_OUT_NAME='" + wh_name + "',CHARGER='" + charger + "',PO_NO='" + income_date + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                            label_uom + "',TLG_SA_SALEORDER_D_PK='" + tlg_sa_saleorder_d_pk + "',CUST_PK='" + cust_pk + "', SLIP_NO = '" + slipNO + "'  where PK = " + pk;
                    db.execSQL(sql);
                }
            }
        }
        db.close();

        return true;
    }

    public boolean updateSendGoodsDeliveryOK(Integer rw, String a[][], String formID) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String sent_time = df.format(c.getTime());
        String scan_date = CDate.getDateyyyyMMdd();
        String scan_time = CDate.getDateYMDHHmmss();

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";

        String slipNO = a[rw][0];//SLIP_NO
        int tlg_gd_req_d_pk = Integer.parseInt(a[rw][1]);//TLG_GD_REQ_D_PK
        int req_item_pk = Integer.parseInt(a[rw][2]);//REQ_ITEM_PK
        float req_qty = Float.parseFloat(a[rw][3]);//REQ_QTY
        int out_wh_pk = Integer.parseInt(a[rw][4]);//OUT_WH_PK
        int tlg_pop_lable_pk = Integer.parseInt(a[rw][5]);//TLG_POP_LABEL_PK
        int tlg_it_item_pk = Integer.parseInt(a[rw][6]);//tlg_it_item_pk
        String item_bc = a[rw][7];//item_bc
        String label_uom = a[rw][8];//label_uom
        String yymmdd = a[0][rw];//yymmdd
        String item_code = a[rw][10];//item_code
        String item_name = a[rw][11];//item_name
        int item_type = Integer.parseInt(a[rw][12]);//item_type
        float label_qty = Float.parseFloat(a[rw][13]);//label_qty
        String status = a[rw][14];//status
        int cust_pk = Integer.parseInt(a[rw][15]);//TCO_BUSPARTNER_PK
        int tlg_sa_saleorder_d_pk = Integer.parseInt(a[rw][16]);//TLG_SA_SALEORDER_D_PK
        String lotNO = a[rw][17];//lot_no
        int tlg_gd_req_m_pk = Integer.parseInt(a[rw][18]);//TLG_GD_REQ_M_PK
        String income_date = a[rw][19];//INCOME_DATE
        String wh_name = a[rw][20];//WAREHOUSE
        String charger = a[rw][21];//CHARGER
        int pk = Integer.parseInt(a[rw][22]);///PK return
        String _parent_pk = a[rw][23];//PARENT_PK
        if (status.equals("008")) {
            sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time + "', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                    item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + req_qty + ",TLG_GD_REQ_M_PK=" + tlg_gd_req_m_pk + ", TLG_GD_REQ_D_PK = " + tlg_gd_req_d_pk + ", TR_ITEM_PK = " +
                    tlg_it_item_pk + ", TR_WH_OUT_PK = " + out_wh_pk + ",TR_WH_OUT_NAME='" + wh_name + "',CHARGER='" + charger + "',INCOME_DATE='" + income_date + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                    label_uom + "',TLG_SA_SALEORDER_D_PK=" + tlg_sa_saleorder_d_pk + ",CUST_PK=" + cust_pk + ", SLIP_NO = '" + slipNO + "'  where STATUS=' ' and PK = " + pk;
            db.execSQL(sql);
        } else {
            if (status.equals("000") && !_parent_pk.equals("0")) {
                boolean kq = isExistBarcode(item_bc, formID);//check Exits
                if (!kq) {
                    sql = "INSERT INTO INV_TR(ITEM_BC,TR_WH_OUT_PK,TR_WH_OUT_NAME,GD_SLIP_NO, CUST_PK,TLG_GD_REQ_M_PK,SLIP_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE, " +
                            "TLG_POP_LABEL_PK,ITEM_CODE,ITEM_NAME,TR_QTY,TR_ITEM_PK,CHARGER,TR_LOT_NO,UOM,TLG_SA_SALEORDER_D_PK,TLG_GD_REQ_D_PK ) "
                            + "VALUES('"
                            + item_bc + "',"
                            + out_wh_pk + ",'"
                            + wh_name + "','"
                            + slipNO + "',"
                            + cust_pk + ","
                            + tlg_gd_req_m_pk + ",'"
                            + "-" + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "Y" + "','"
                            + status + "','"
                            + formID + "',"
                            + tlg_pop_lable_pk + ",'"
                            + item_code + "','"
                            + item_name + "',"
                            + req_qty + ","
                            + tlg_it_item_pk + ",'"
                            + charger + "','"
                            + lotNO + "','"
                            + label_uom + "',"
                            + tlg_sa_saleorder_d_pk + ","
                            + tlg_gd_req_d_pk
                            + ")";
                    db.execSQL(sql);

                    //update parent bc
                    sql = "UPDATE INV_TR set STATUS=' '"
                            + ", DEL_IF = " + pk
                            + ",SENT_YN = 'Y'"
                            + "  where PK = " + pk;
                    db.execSQL(sql);
                }

            } else {
                sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time + "', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                        item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + req_qty + ",TLG_GD_REQ_M_PK=" + tlg_gd_req_m_pk + ", TLG_GD_REQ_D_PK = " + tlg_gd_req_d_pk + ", TR_ITEM_PK = " +
                        tlg_it_item_pk + ", TR_WH_OUT_PK = " + out_wh_pk + ",TR_WH_OUT_NAME='" + wh_name + "',CHARGER='" + charger + "',PO_NO='" + income_date + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                        label_uom + "',TLG_SA_SALEORDER_D_PK=" + tlg_sa_saleorder_d_pk + ",CUST_PK=" + cust_pk + ", SLIP_NO = '" + slipNO + "'  where PK = " + pk;
                db.execSQL(sql);
            }
        }

        db.close();

        return true;
    }

    public boolean updateSendIncomingOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());
        String scan_date = CDate.getDateyyyyMMdd();
        String scan_time = CDate.getDateYMDHHmmss();

        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";

        for (int i = 0; i < dt.totalrows; i++) {
            String item_bc =  dt.records.get(i).get("item_bc").toString();//ITEM_BC
            String item_code =  dt.records.get(i).get("item_code").toString();//ITEM_CODE
            String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
            float tr_qty = Float.parseFloat( dt.records.get(i).get("qty").toString());//QTY
            String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
            int label_pk = Integer.parseInt( dt.records.get(i).get("label_pk").toString());//LABEL_PK
            int item_pk = Integer.parseInt( dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
            String _status =  dt.records.get(i).get("status").toString();//status
            String _slip_no =  dt.records.get(i).get("slip_no").toString();//slip no
            String _income_date =  dt.records.get(i).get("income_dt").toString();//income date
            String _charger = dt.records.get(i).get("crt_by").toString();
            String _wh_name = dt.records.get(i).get("wh_name").toString();
            String _wh_pk = dt.records.get(i).get("wh_pk").toString();
            String _line_name = dt.records.get(i).get("line_name").toString();
            String _line_pk = dt.records.get(i).get("line_pk").toString();
            String _uom =  dt.records.get(i).get("uom").toString();//UOM
            String _tsa_saleorder_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();
            int _pkID = Integer.parseInt(dt.records.get(i).get("tr_pk").toString());//PK_TR
            String _parent_pk = dt.records.get(i).get("parent_pk").toString();
            String wi_plan_pk = dt.records.get(i).get("wi_plan_pk").toString();
            String grade = dt.records.get(i).get("grade").toString();
            if(_status.equals("000")){
                if(_parent_pk.equals("0")) {
                    if (_line_pk == "") {
                        sql = "UPDATE INV_TR set STATUS='" + _status
                                + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                                + "', TLG_POP_LABEL_PK = " + label_pk
                                + ", ITEM_CODE = '" + item_code
                                + "', ITEM_NAME = '" + item_name
                                + "', TR_QTY = " + tr_qty
                                + ", TR_ITEM_PK = " + item_pk
                                + ",CHARGER='" + _charger
                                + "',INCOME_DATE='" + _income_date
                                + "', TR_LOT_NO='" + lot_no
                                + "', SLIP_NO = '" + _slip_no
                                + "', UOM = '" + _uom
                                + "', PO_NO = '" + _wh_name
                                + "', TLG_SA_SALEORDER_D_PK = '" + _tsa_saleorder_d_pk
                                + "',WI_PLAN_PK = '" + wi_plan_pk
                                + "',GRADE = '" + grade
                                + "'  where PK = " + _pkID;
                        db.execSQL(sql);

                        // update mapping
                        sql = "UPDATE INV_TR SET SENT_YN = 'Y' WHERE BC_TYPE = 'P' AND ITEM_BC = '" + item_bc + "'";
                        db.execSQL(sql);
                    } else {
                        sql = "UPDATE INV_TR set STATUS='" + _status
                                + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                                + "', TLG_POP_LABEL_PK = " + label_pk
                                + ", ITEM_CODE = '" + item_code
                                + "', ITEM_NAME = '" + item_name
                                + "', TR_QTY = " + tr_qty
                                + ", TR_ITEM_PK = " + item_pk
                                + ",CHARGER='" + _charger
                                + "',INCOME_DATE='" + _income_date
                                + "', TR_LOT_NO='" + lot_no
                                + "', SLIP_NO = '" + _slip_no
                                + "', UOM = '" + _uom
                                + "', PO_NO = '" + _wh_name
                                + "', TLG_SA_SALEORDER_D_PK = '" + _tsa_saleorder_d_pk
                                + "',WI_PLAN_PK = '" + wi_plan_pk
                                + "',GRADE = '" + grade
                                + "'  where PK = " + _pkID;
                        db.execSQL(sql);
                    }
                }else{
                    boolean kq= isExistBarcode(item_bc,"4");
                    if(!kq) {
                        SQLiteDatabase db2= getWritableDatabase();//db is close when check isExistBarcode
                        sql = "INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK, TLG_IN_WHLOC_IN_NAME,TR_LINE_PK,LINE_NAME,SLIP_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE, " +
                                "TLG_POP_LABEL_PK,ITEM_CODE,ITEM_NAME,TR_QTY,TR_ITEM_PK,CHARGER,TR_LOT_NO,UOM,TLG_SA_SALEORDER_D_PK,WI_PLAN_PK,GRADE ) "
                                + "VALUES('"
                                + item_bc + "','"
                                + _wh_pk + "','"
                                + _wh_name + "','"
                                + _slip_no + "','"
                                + "-" + "','"
                                + _line_pk + "','"
                                + _line_name + "','"
                                + "-" + "','"
                                + scan_date + "','"
                                + scan_time + "','"
                                + "Y" + "','"
                                + _status + "','"
                                + "4" + "',"
                                + label_pk + ",'"
                                + item_code + "','"
                                + item_name + "',"
                                + tr_qty + ","
                                + item_pk + ",'"
                                + _charger + "','"
                                + lot_no + "','"
                                + _uom + "','"
                                + _tsa_saleorder_d_pk + "','"
                                + wi_plan_pk+ "','"
                                + grade
                                + "')";

                        db2.execSQL(sql);

                        //update parent bc
                        sql = "UPDATE INV_TR set STATUS='001'"
                                + ", DEL_IF = " + _pkID
                                + ",SENT_YN = 'Y'"
                                + "  where PK = " + _pkID;
                        db2.execSQL(sql);
                        db2.close();
                    }

                }
            }
            else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', TR_WH_IN_NAME = '" + _wh_name
                        + "', LINE_NAME = '" + _line_name
                        + "', TLG_SA_SALEORDER_D_PK = '" + _tsa_saleorder_d_pk
                        + "',WI_PLAN_PK = '" + wi_plan_pk
                        + "',GRADE = '" + grade
                        + "'  where PK = " + _pkID;
                db.execSQL(sql);
            }
        }

        db.close();

        return true;
    }
    public boolean updateSendResultIncomingOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());
        String scan_date = CDate.getDateyyyyMMdd();
        String scan_time = CDate.getDateYMDHHmmss();

        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";

        for (int i = 0; i < dt.totalrows; i++) {
            String item_bc =  dt.records.get(i).get("item_bc").toString();//ITEM_BC
            String item_code =  dt.records.get(i).get("item_code").toString();//ITEM_CODE
            String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
            float tr_qty = Float.parseFloat( dt.records.get(i).get("qty").toString());//QTY
            String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
            int label_pk = Integer.parseInt( dt.records.get(i).get("label_pk").toString());//LABEL_PK
            int item_pk = Integer.parseInt( dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
            String _status =  dt.records.get(i).get("status").toString();//status
            String _slip_no =  dt.records.get(i).get("slip_no").toString();//slip no
            String _income_date =  dt.records.get(i).get("income_dt").toString();//income date
            String _charger = dt.records.get(i).get("crt_by").toString();
            String _wh_name = dt.records.get(i).get("wh_name").toString();
            String _wh_pk = dt.records.get(i).get("wh_pk").toString();
            String _line_name = dt.records.get(i).get("line_name").toString();
            String _line_pk = dt.records.get(i).get("line_pk").toString();
            String _uom =  dt.records.get(i).get("uom").toString();//UOM
            String _tsa_saleorder_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();
            int _pkID = Integer.parseInt(dt.records.get(i).get("tr_pk").toString());//PK_TR
            String _parent_pk = dt.records.get(i).get("parent_pk").toString();
            String wi_plan_pk = dt.records.get(i).get("wi_plan_pk").toString();
            String grade = dt.records.get(i).get("grade").toString();
            if(_status.equals("000")){
                if(_parent_pk.equals("0")) {
                    if (_line_pk == "") {
                        sql = "UPDATE INV_TR set STATUS='" + _status
                                + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                                + "', TLG_POP_LABEL_PK = " + label_pk
                                + ", ITEM_CODE = '" + item_code
                                + "', ITEM_NAME = '" + item_name
                                + "', TR_QTY = " + tr_qty
                                + ", TR_ITEM_PK = " + item_pk
                                + ",CHARGER='" + _charger
                                + "',INCOME_DATE='" + _income_date
                                + "', TR_LOT_NO='" + lot_no
                                + "', SLIP_NO = '" + _slip_no
                                + "', UOM = '" + _uom
                                + "', PO_NO = '" + _wh_name
                                + "', TLG_SA_SALEORDER_D_PK = '" + _tsa_saleorder_d_pk
                                + "',WI_PLAN_PK = '" + wi_plan_pk
                                + "',GRADE = '" + grade
                                + "'  where PK = " + _pkID;
                        db.execSQL(sql);

                        // update mapping
                        sql = "UPDATE INV_TR SET SENT_YN = 'Y' WHERE BC_TYPE = 'P' AND ITEM_BC = '" + item_bc + "'";
                        db.execSQL(sql);
                    } else {
                        sql = "UPDATE INV_TR set STATUS='" + _status
                                + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                                + "', TLG_POP_LABEL_PK = " + label_pk
                                + ", ITEM_CODE = '" + item_code
                                + "', ITEM_NAME = '" + item_name
                                + "', TR_QTY = " + tr_qty
                                + ", TR_ITEM_PK = " + item_pk
                                + ",CHARGER='" + _charger
                                + "',INCOME_DATE='" + _income_date
                                + "', TR_LOT_NO='" + lot_no
                                + "', SLIP_NO = '" + _slip_no
                                + "', UOM = '" + _uom
                                + "', PO_NO = '" + _wh_name
                                + "', TLG_SA_SALEORDER_D_PK = '" + _tsa_saleorder_d_pk
                                + "',WI_PLAN_PK = '" + wi_plan_pk
                                + "',GRADE = '" + grade
                                + "'  where PK = " + _pkID;
                        db.execSQL(sql);
                    }
                }else{
                    boolean kq= isExistBarcode(item_bc,"4");
                    if(!kq) {
                        SQLiteDatabase db2= getWritableDatabase();//db is close when check isExistBarcode
                        sql = "INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK, TLG_IN_WHLOC_IN_NAME,TR_LINE_PK,LINE_NAME,SLIP_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE, " +
                                "TLG_POP_LABEL_PK,ITEM_CODE,ITEM_NAME,TR_QTY,TR_ITEM_PK,CHARGER,TR_LOT_NO,UOM,TLG_SA_SALEORDER_D_PK,WI_PLAN_PK ,GRADE) "
                                + "VALUES('"
                                + item_bc + "','"
                                + _wh_pk + "','"
                                + _wh_name + "','"
                                + _slip_no + "','"
                                + "-" + "','"
                                + _line_pk + "','"
                                + _line_name + "','"
                                + "-" + "','"
                                + scan_date + "','"
                                + scan_time + "','"
                                + "Y" + "','"
                                + _status + "','"
                                + "38" + "',"
                                + label_pk + ",'"
                                + item_code + "','"
                                + item_name + "',"
                                + tr_qty + ","
                                + item_pk + ",'"
                                + _charger + "','"
                                + lot_no + "','"
                                + _uom + "','"
                                + _tsa_saleorder_d_pk + "','"
                                + wi_plan_pk + "','"
                                +grade
                                + "')";

                        db2.execSQL(sql);

                        //update parent bc
                        sql = "UPDATE INV_TR set STATUS='001'"
                                + ", DEL_IF = " + _pkID
                                + ",SENT_YN = 'Y'"
                                + "  where PK = " + _pkID;
                        db2.execSQL(sql);
                        db2.close();
                    }

                }
            }
            else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', TR_WH_IN_NAME = '" + _wh_name
                        + "', LINE_NAME = '" + _line_name
                        + "', TLG_SA_SALEORDER_D_PK = '" + _tsa_saleorder_d_pk
                        + "',WI_PLAN_PK = '" + wi_plan_pk
                        + "',GRADE = '" + grade
                        + "'  where PK = " + _pkID;
                db.execSQL(sql);
            }
        }

        db.close();

        return true;
    }
    public boolean updateMappingV3(JResultData  a) {
        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);
        SQLiteDatabase db = getWritableDatabase();
        for (int i = 0; i < dt.totalrows; i++) {
            String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
            String item_pk = dt.records.get(i).get("item_pk").toString();//ITEM_pk
            String lot_no = dt.records.get(i).get("lot_no").toString();//ITEM_pk
            String slip_no = dt.records.get(i).get("slip_no").toString();//ITEM_pk
            String mapp_m_pk = dt.records.get(i).get("mapp_m_pk").toString();//ITEM_pk

            String sql = "UPDATE INV_TR set SENT_YN = 'Y', SLIP_NO= '" + slip_no + "',MAPPING_YN='Y'  " +
                    " where TR_TYPE='" + gwform_info.MappingV3.Id() + "' AND SLIP_NO IN('-',' ',null) and ITEM_BC='" + item_bc + "'";
            db.execSQL(sql);
        }
        db.close();
        return true;
    }
    public boolean updateSendIncomingInventoryOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);
        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        try {
            for (int i = 0; i < dt.totalrows; i++) {
                String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
                String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
                String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
                float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
                String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
                int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
                int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
                String _status = dt.records.get(i).get("status").toString();//status
                String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
                String _income_date = dt.records.get(i).get("income_dt").toString();//income date
                String _charger = dt.records.get(i).get("crt_by").toString();//charger
                String _supplier_name = dt.records.get(i).get("supplier_name").toString();//Supplier
                String _supplier_pk = dt.records.get(i).get("supplier_pk").toString();//Supplier_PK
                String _po_no = dt.records.get(i).get("po_no").toString();//po_no
                Float _unit_price = Float.parseFloat(dt.records.get(i).get("line_pk").toString());//unit_price
                String _uom = dt.records.get(i).get("uom").toString();//UOM
                String _tlg_po_po_d_pk = dt.records.get(i).get("po_d_pk").toString();//tlg_po_po_d_pk
                int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());
                if (_status.equals("000")) {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ", SUPPLIER_PK = '" + _supplier_pk
                            + "',CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', UOM = '" + _uom
                            + "', PO_NO = '" + _po_no
                            + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                            + "', UNIT_PRICE = " + _unit_price
                            + "  where PK = " + id;
                } else {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ",CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', UOM = '" + _uom
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', PO_NO = '" + _po_no
                            + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                            + "'  where PK = " + id;
                }
                db.execSQL(sql);
            }
            db.close();
        }catch(NullPointerException ex){
            Log.e("sqlHelperInventoryOK", ex.getMessage());
            return false;
        }
        return true;
    }

    public boolean updateSendIncomingRequestOK(JResultData a) {
        String sql1="";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);
        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        try {
            for (int i = 0; i < dt.totalrows; i++) {
                String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
                String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
                String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
                float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
                String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
                int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
                int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
                String _status = dt.records.get(i).get("status").toString();//status
                String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
                String _income_date = dt.records.get(i).get("income_dt").toString();//income date
                String _charger = dt.records.get(i).get("crt_by").toString();//charger
                String _supplier_name = dt.records.get(i).get("supplier_name").toString();//Supplier
                String _supplier_pk = dt.records.get(i).get("supplier_pk").toString();//Supplier_PK
                String _po_no = dt.records.get(i).get("po_no").toString();//po_no
                Float _unit_price = Float.parseFloat(dt.records.get(i).get("unit_price").toString());//unit_price
                String _uom = dt.records.get(i).get("uom").toString();//UOM
                String _tlg_po_po_d_pk = dt.records.get(i).get("po_d_pk").toString();//tlg_po_po_d_pk
                int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());
                String req_no = dt.records.get(i).get("req_no").toString();
                String tlg_req_m_pk = dt.records.get(i).get("tlg_req_m_pk").toString();
                String tlg_req_d_pk = dt.records.get(i).get("tlg_req_d_pk").toString();
                String so_no = dt.records.get(i).get("so_no").toString();
                String wh_in_pk = dt.records.get(i).get("wh_in_pk").toString();
                if (_status.equals("000")) {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ", SUPPLIER_PK = '" + _supplier_pk
                            + "',CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', UOM = '" + _uom
                            + "', PO_NO = '" + _po_no
                            + "', SO_NO = '" + so_no
                            + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                            + "', TLG_GD_REQ_M_PK = '" + tlg_req_m_pk
                            + "', TLG_GD_REQ_D_PK = '" + tlg_req_d_pk
                            + "', TR_WH_IN_PK = '" + wh_in_pk
                            + "', UNIT_PRICE = " + _unit_price
                            + "  where PK = " + id;
                    sql1 = "UPDATE INV_TR set REQ_BAL = CAST((CAST(REQ_BAL AS INTEGER)-" + tr_qty + ") AS TEXT), SCANNED = CAST((CAST(SCANNED AS INTEGER)+" + tr_qty + ") AS TEXT) " +
                            " WHERE REQ_NO = '" + req_no +"' AND TR_ITEM_PK = "+ item_pk +" AND ITEM_BC is null AND TLG_GD_REQ_D_PK='"+tlg_req_d_pk+"'";
                } else {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ",CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', UOM = '" + _uom
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', PO_NO = '" + _po_no
                            + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                            + "'  where PK = " + id;
                }
                db.execSQL(sql);
                db.execSQL(sql1);
            }
            db.close();
        }catch(NullPointerException ex){
            Log.e("sqlHelperInventoryOK", ex.getMessage());
            return false;
        }
        return true;
    }

    public boolean updateSendOutProductOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);
        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        try {
            for (int i = 0; i < dt.totalrows; i++) {
                String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
                String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
                String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
                float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
                String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
                int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
                int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
                String _status = dt.records.get(i).get("status").toString();//status
                String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
                String _income_date = dt.records.get(i).get("income_dt").toString();//income date
                String _charger = dt.records.get(i).get("crt_by").toString();//charger
                String _supplier_name = dt.records.get(i).get("wh_name").toString();//Supplier
                String _supplier_pk = dt.records.get(i).get("wh_pk").toString();//Supplier_PK
                String _po_no = dt.records.get(i).get("line_name").toString();//po_no
                Float _unit_price = Float.parseFloat(dt.records.get(i).get("line_pk").toString());//unit_price
                String _uom = dt.records.get(i).get("uom").toString();//UOM
                String _saleorder_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();//tlg_po_po_d_pk
                int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());
                if (_status.equals("000")) {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ", SUPPLIER_PK = '" + _supplier_pk
                            + "',CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', UOM = '" + _uom
                            + "', PO_NO = '" + _po_no
                            + "', TLG_SA_SALEORDER_D_PK = '" + _saleorder_d_pk
                            + "', UNIT_PRICE = " + _unit_price
                            + "  where PK = " + id;
                } else {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ",CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', UOM = '" + _uom
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', PO_NO = '" + _po_no
                            + "', TLG_SA_SALEORDER_D_PK = '" + _saleorder_d_pk
                            + "'  where PK = " + id;
                }
                db.execSQL(sql);
            }
            db.close();
        }catch(NullPointerException ex){
            Log.e("sqlHelperInventoryOK", ex.getMessage());
            return false;
        }
        return true;
    }

    public boolean updateSendOutInventoryOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt=jdt.get(0);

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        for (int i = 0; i < dt.totalrows; i++) {
            String item_bc =dt.records.get(i).get("item_bc").toString();//a[i][0];//ITEM_BC
            String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
            String item_name =dt.records.get(i).get("item_name").toString();//ITEM_NAME
            float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
            String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
            int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
            int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
            String _status = dt.records.get(i).get("status").toString();//status
            String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
            String _income_date = dt.records.get(i).get("income_dt").toString();//income date
            String _charger = dt.records.get(i).get("crt_by").toString();//CRT_BY
            String _supplier_name = dt.records.get(i).get("wh_name").toString();//WH_NAME,Supplier
            String _supplier_pk = dt.records.get(i).get("wh_pk").toString();//WH_PK,Supplier_PK
            String _line_name = dt.records.get(i).get("line_name").toString();//po_no
            Float _unit_price = Float.parseFloat(dt.records.get(i).get("line_pk").toString());//unit_price
            String _uom = dt.records.get(i).get("uom").toString();//UOM
            String _tlg_po_po_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();//tlg_po_po_d_pk
            int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());
            String whLoc_pk = dt.records.get(i).get("loc_pk").toString();
            if (_status.equals("000")) {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = '" + label_pk
                        + "', ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ", SUPPLIER_PK = '" + _supplier_pk
                        + "',CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', UOM = '" + _uom
                        + "', PO_NO = '" + _line_name
                        + "', TLG_IN_WHLOC_OUT_PK = '" + whLoc_pk
                        + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                        + "', UNIT_PRICE = " + _unit_price
                        + "  where PK = " + id;
            } else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', LINE_NAME = '" + _line_name
                        + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                        + "'  where PK = " + id;
            }
            db.execSQL(sql);
        }
        db.close();

        return true;
    }


    public boolean updateSendOutRequestOK(JResultData a,String formID) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt=jdt.get(0);

        SQLiteDatabase db = getWritableDatabase();
        String sql = "",sql1="";
        for (int i = 0; i < dt.totalrows; i++) {
            String item_bc =dt.records.get(i).get("item_bc").toString();//a[i][0];//ITEM_BC
            String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
            String item_name =dt.records.get(i).get("item_name").toString();//ITEM_NAME
            float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
            String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
            int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
            int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
            String _status = dt.records.get(i).get("status").toString();//status
            String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
            String _income_date = dt.records.get(i).get("income_dt").toString();//income date
            String _charger = dt.records.get(i).get("crt_by").toString();//CRT_BY
            String _wh_out_name = dt.records.get(i).get("wh_name").toString();//WH_NAME
            String _wh_out_pk = dt.records.get(i).get("wh_pk").toString();//WH_PK
            String _line_name = dt.records.get(i).get("line_name").toString();//po_no
            String _line_pk = dt.records.get(i).get("line_pk").toString();
//            Float _unit_price = Float.parseFloat(dt.records.get(i).get("unit_price").toString());//unit_price
            String _uom = dt.records.get(i).get("uom").toString();//UOM
           // String _tlg_po_po_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();//tlg_po_po_d_pk
            String saleorder_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();
            String saleorder_m_pk = dt.records.get(i).get("saleorder_m_pk").toString();
            String po_no = dt.records.get(i).get("po_no").toString();
            int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());
            String whLoc_pk = dt.records.get(i).get("loc_pk").toString();
            String req_m_pk = dt.records.get(i).get("req_m_pk").toString();
            String req_d_pk = dt.records.get(i).get("req_d_pk").toString();
            String req_no = dt.records.get(i).get("req_no").toString();
            String wi_pk = dt.records.get(i).get("wi_plan_pk").toString();
            String grade = dt.records.get(i).get("grade").toString();
            if (_status.equals("000")) {
                boolean kq = isExistBarcode000(item_bc,formID);
                if(kq==false) {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = '" + label_pk
                            + "', ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
//                        + ", TR_WH_OUT_PK = '" + _wh_out_pk
                            + ",CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
//                        + "', TR_WH_OUT_NAME = '" + _wh_out_name
                            + "', UOM = '" + _uom
                            + "', PO_NO = '" + po_no
//                        + "', TR_LINE_PK = '" + _line_pk
                            + "', TLG_IN_WHLOC_OUT_PK = '" + whLoc_pk
                          //  + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                          + "', TLG_SA_SALEORDER_D_PK = '" + saleorder_d_pk

                           + "', TLG_SA_SALEORDER_M_PK = '" + saleorder_m_pk
                            + "', TLG_GD_REQ_M_PK = '" + req_m_pk
                            + "', TLG_GD_REQ_D_PK = '" + req_d_pk
                            + "', WI_PLAN_PK = '" + wi_pk
                            + "', GRADE = '" + grade

                            + "', REQ_NO = '" + req_no

                            + "'  where PK = " + id;
                    sql1 = "UPDATE INV_TR set REQ_BAL = CAST(ROUND((CAST(REQ_BAL AS REAL)-" + tr_qty + "),2) AS TEXT), SCANNED = CAST(ROUND((CAST(SCANNED AS REAL)+" + tr_qty + "),2) AS TEXT) " +
                            " WHERE REQ_NO = '" + req_no + "' AND TR_ITEM_PK = " + item_pk + " AND ITEM_BC IS NULL  AND TLG_GD_REQ_D_PK='" + req_d_pk + "'";
                    db.execSQL(sql);
                    db.execSQL(sql1);
                }
            } else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', TR_WH_OUT_NAME = '" + _wh_out_name
                        + "', LINE_NAME = '" + _line_name
                       // + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                        + "', WI_PLAN_PK = '" + wi_pk
                        + "'  where PK = " + id;
                db.execSQL(sql);
            }

        }
        db.close();

        return true;
    }

    public boolean updateSendGoodsDeliNoReqOK(String a[][]) {

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        for (int i = 0; i < a.length; i++) {
            String slipNO = a[i][0];//SLIP_NO
            int req_item_pk = Integer.parseInt(a[i][1]);//REQ_ITEM_PK
            float req_qty = Float.parseFloat(a[i][2]);//REQ_QTY
            int tlg_pop_lable_pk = Integer.parseInt(a[i][3]);//TLG_POP_LABEL_PK
            int tlg_it_item_pk = Integer.parseInt(a[i][4]);//tlg_it_item_pk
            String item_bc = a[i][5];//item_bc
            String label_uom = a[i][6];//label_uom
            String yymmdd = a[i][7];//yymmdd
            String item_code = a[i][8];//item_code
            String item_name = a[i][9];//item_name
            int item_type = Integer.parseInt(a[i][10]);//item_type
            float label_qty = Float.parseFloat(a[i][11]);//label_qty
            String status = a[i][12];//status
            String lotNO = a[i][13];//lot_no
            String income_date = a[i][14];//INCOME_DATE
            String charger = a[i][15];//CHARGER
            String wh_name = a[i][16];//WH_NAME
            int id = Integer.parseInt(a[i][17]);// pk_inv_tr
            if (status.equals("000")) {
                sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                        item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + req_qty + ", TR_ITEM_PK = " +
                        tlg_it_item_pk + ",CHARGER='" + charger + "',INCOME_DATE='" + income_date + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                        label_uom + "', SLIP_NO = '" + slipNO + "'  where PK = " + id;
            } else {
                sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                        item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + req_qty + ", TR_ITEM_PK = " +
                        tlg_it_item_pk + ",CHARGER='" + charger + "',INCOME_DATE='" + income_date + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                        label_uom + "', SLIP_NO = '" + slipNO + "', TR_WH_OUT_NAME = '" + wh_name + "'  where PK = " + id;
            }
            db.execSQL(sql);
        }
        db.close();

        return true;
    }

    public boolean updateSendStockTransferOK(JResultData a) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            List<JDataTable> jdt = a.getListODataTable();
            JDataTable dt = jdt.get(0);

            String sql = "";
            for (int i = 0; i < dt.totalrows; i++) {
                String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
                String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
                String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
                float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
                String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
                int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
                int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
                String _status = dt.records.get(i).get("status").toString();//status
                String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
                String _income_date = dt.records.get(i).get("income_dt").toString();//income date
                String _charger = dt.records.get(i).get("crt_by").toString();//charger
                String supplier_name = dt.records.get(i).get("wh_name").toString();//wh_name
                String supplier_pk = dt.records.get(i).get("wh_out_pk").toString();//wh_pk
                String _line_name = dt.records.get(i).get("line_name").toString();//po_no
                float unit_price = Float.parseFloat(dt.records.get(i).get("unit_price").toString());//unit_price
                String _uom = dt.records.get(i).get("uom").toString();//UOM
                String _tlg_po_po_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();//tlg_po_po_d_pk
                int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());//inv_tr_pk
                if (_status.equals("000")) {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y'"
                            + ", TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ", SUPPLIER_PK = '" + supplier_pk
                            + "', CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', SUPPLIER_NAME = '" + supplier_name
                            + "', UOM = '" + _uom
                            + "', PO_NO = '" + _line_name
                            + "', TLG_PO_PO_D_PK = " + _tlg_po_po_d_pk
                            + ", UNIT_PRICE = " + unit_price
                            + "  where PK = " + id;
                } else {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y'"
                            + ", TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ",CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', UOM = '" + _uom
                            //+ "', WH_NAME = '" + _supplier_name
                            + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                            + "'  where PK = " + id;
                }
                db.execSQL(sql);
            }
        } catch (Exception ex) {
            return false;
        } finally {
            db.close();
        }


        return true;
    }

    public boolean updateSendStockTransferRequestOK(JResultData a) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            List<JDataTable> jdt = a.getListODataTable();
            JDataTable dt = jdt.get(0);

            String sql = "",sql1="";
            for (int i = 0; i < dt.totalrows; i++) {
//                String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
                String req_m_pk = dt.records.get(i).get("trans_req_m_pk").toString();
                String req_d_pk = dt.records.get(i).get("trans_req_d_pk").toString();
                String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
                String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
                float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
                String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
                int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
                int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
                String _status = dt.records.get(i).get("status").toString();//status
                String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
                String _income_date = dt.records.get(i).get("income_dt").toString();//income date
                String _charger = dt.records.get(i).get("crt_by").toString();//charger
                String wh_out_name = dt.records.get(i).get("out_wh_name").toString();//wh_name
                String wh_out_pk = dt.records.get(i).get("out_wh_pk").toString();//wh_out_pk
                String wh_in_pk = dt.records.get(i).get("in_wh_pk").toString();//wh_in_pk
                String wh_in_name = dt.records.get(i).get("in_wh_name").toString();//wh_in_name
                String loc_out_pk = dt.records.get(i).get("out_loc_pk").toString();//wh_pk
                String loc_in_pk = dt.records.get(i).get("in_loc_pk").toString();//wh_pk
                String _line_name = dt.records.get(i).get("line_name").toString();//po_no
                float unit_price = Float.parseFloat(dt.records.get(i).get("unit_price").toString());//unit_price
                String _uom = dt.records.get(i).get("uom").toString();//UOM
                String _tlg_po_po_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();//tlg_po_po_d_pk
                int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());//inv_tr_pk
                String req_no = dt.records.get(i).get("req_no").toString();//wh_pk
                String grade = dt.records.get(i).get("grade").toString();//wh_pk
                if (_status.equals("000")) {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y'"
                            + ", TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TLG_GD_REQ_M_PK = '" + req_m_pk
                            + "', TLG_GD_REQ_D_PK = '" + req_d_pk
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ", TR_WH_OUT_PK = '" + wh_out_pk
                            + "', CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', TR_WH_OUT_NAME = '" + wh_out_name
                            + "', UOM = '" + _uom
                            + "', TR_WH_IN_PK = '" + wh_in_pk
                            + "', TR_WH_IN_NAME = '" + wh_in_name
                            + "', TLG_SA_SALEORDER_D_PK = '" + _tlg_po_po_d_pk
                            + "', TLG_IN_WHLOC_OUT_PK = '" + loc_out_pk
                            + "', TLG_IN_WHLOC_IN_PK = '" + loc_in_pk
                            + "', REQ_NO = '" + req_no
                            + "', GRADE = '" + grade
                            + "', UNIT_PRICE = " + unit_price
                            + "  where PK = " + id;
                    sql1 = "UPDATE INV_TR set REQ_BAL = CAST((CAST(REQ_BAL AS INTEGER)-" + tr_qty + ") AS TEXT), SCANNED = CAST((CAST(SCANNED AS INTEGER)+" + tr_qty + ") AS TEXT) " +
                            " WHERE REQ_NO = '" + req_no +"' AND TR_ITEM_PK = "+ item_pk + " AND ITEM_BC IS NULL";
                } else {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y'"
                            + ", TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ",CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', UOM = '" + _uom
                            + "', GRADE = '" + grade
                            + "', TLG_IN_WHLOC_OUT_PK = '" + loc_out_pk
                            + "', TLG_IN_WHLOC_IN_PK = '" + loc_in_pk
                            //+ "', WH_NAME = '" + _supplier_name
                            + "', TLG_SA_SALEORDER_D_PK = '" + _tlg_po_po_d_pk
                            + "'  where PK = " + id;
                }
                db.execSQL(sql);
                db.execSQL(sql1);
            }
        } catch (Exception ex) {
            return false;
        } finally {
            db.close();
        }


        return true;
    }

    public boolean updateSendGoodsPartialOK(Integer rw, String a[][]) {
        String sent_time = CDate.getDateYMDHHmmss();

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";

        String slipNO = a[rw][0];//SLIP_NO
        int tlg_gd_req_d_pk = Integer.parseInt(a[rw][1]);//TLG_GD_REQ_D_PK
        int req_item_pk = Integer.parseInt(a[rw][2]);//REQ_ITEM_PK
        float req_qty = Float.parseFloat(a[rw][3]);//REQ_QTY
        int out_wh_pk = Integer.parseInt(a[rw][4]);//OUT_WH_PK
        int tlg_pop_lable_pk = Integer.parseInt(a[rw][5]);//TLG_POP_LABEL_PK
        int tlg_it_item_pk = Integer.parseInt(a[rw][6]);//tlg_it_item_pk
        String item_bc = a[rw][7];//item_bc
        String label_uom = a[rw][8];//label_uom
        String yymmdd = a[0][rw];//yymmdd
        String item_code = a[rw][10];//item_code
        String item_name = a[rw][11];//item_name
        int item_type = Integer.parseInt(a[rw][12]);//item_type
        float label_qty = Float.parseFloat(a[rw][13]);//label_qty
        String status = a[rw][14];//status
        int cust_pk = Integer.parseInt(a[rw][15]);//TCO_BUSPARTNER_PK
        int tlg_sa_saleorder_d_pk = Integer.parseInt(a[rw][16]);//TLG_SA_SALEORDER_D_PK
        String lotNO = a[rw][17];//lot_no
        int tlg_gd_req_m_pk = Integer.parseInt(a[rw][18]);//TLG_GD_REQ_M_PK
        String income_date = a[rw][19];//INCOME_DATE
        String wh_name = a[rw][20];//WAREHOUSE
        String charger = a[rw][21];//CHARGER
        int pk = Integer.parseInt(a[rw][22]);///PK return
        int parent_pk = Integer.parseInt(a[rw][23]);///Parent_PK return
        if (status.equals("008")) {
            sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time + "', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                    item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + req_qty + ",TLG_GD_REQ_M_PK=" + tlg_gd_req_m_pk + ", TLG_GD_REQ_D_PK = " + tlg_gd_req_d_pk + ", TR_ITEM_PK = " +
                    tlg_it_item_pk + ", TR_WH_OUT_PK = " + out_wh_pk + ",TR_WH_OUT_NAME='" + wh_name + "',CHARGER='" + charger + "',INCOME_DATE='" + income_date + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                    label_uom + "',TLG_SA_SALEORDER_D_PK=" + tlg_sa_saleorder_d_pk + ",CUST_PK=" + cust_pk + ", SLIP_NO = '" + slipNO + "'  where STATUS=' ' and PK = " + pk;
        } else {
            if (parent_pk < 0) {
                sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y',BC_TYPE='P', SENT_TIME = '" + sent_time + "', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                        item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + req_qty + ", TR_DELI_QTY = " + req_qty + ",TLG_GD_REQ_M_PK=" + tlg_gd_req_m_pk + ", TLG_GD_REQ_D_PK = " + tlg_gd_req_d_pk + ", TR_ITEM_PK = " +
                        tlg_it_item_pk + ", TR_WH_OUT_PK = " + out_wh_pk + ",TR_WH_OUT_NAME='" + wh_name + "',CHARGER='" + charger + "',INCOME_DATE='" + income_date + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                        label_uom + "',TLG_SA_SALEORDER_D_PK=" + tlg_sa_saleorder_d_pk + ",CUST_PK=" + cust_pk + ", SLIP_NO = '" + slipNO + "'  where PK = " + pk;
            } else {
                sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y',BC_TYPE='C', SENT_TIME = '" + sent_time + "', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                        item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + req_qty + ", TR_DELI_QTY = " + req_qty + ",TLG_GD_REQ_M_PK=" + tlg_gd_req_m_pk + ", TLG_GD_REQ_D_PK = " + tlg_gd_req_d_pk + ", TR_ITEM_PK = " +
                        tlg_it_item_pk + ", TR_WH_OUT_PK = " + out_wh_pk + ",TR_WH_OUT_NAME='" + wh_name + "',CHARGER='" + charger + "',INCOME_DATE='" + income_date + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                        label_uom + "',TLG_SA_SALEORDER_D_PK=" + tlg_sa_saleorder_d_pk + ",CUST_PK=" + cust_pk + ", SLIP_NO = '" + slipNO + "'  where PK = " + pk;
            }
        }

        db.execSQL(sql);
        db.close();

        return true;
    }

    public boolean updateSendStockReturnOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);
        for (int i = 0; i < dt.totalrows; i++) {
            String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
            String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
            String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
            float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
            String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
            int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
            int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
            String _status = dt.records.get(i).get("status").toString();//status
            String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
            String _income_date = dt.records.get(i).get("income_dt").toString();//income date
            String _charger = dt.records.get(i).get("crt_by").toString();//charger
            String _supplier_name = dt.records.get(i).get("wh_name").toString();//Supplier
            String _supplier_pk = dt.records.get(i).get("wh_pk").toString();//Supplier_PK
            String _line_name = dt.records.get(i).get("line_name").toString();//LINE NAME
            String _line_pk = dt.records.get(i).get("line_pk").toString();//LINE PK
            String _uom = dt.records.get(i).get("uom").toString();//UOM
            String _tlg_po_po_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();//tlg_po_po_d_pk
            int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString()); 
			String _grade = dt.records.get(i).get("grade").toString();//_grade      
			if (_status.equals("000")) {
                if (_line_pk.equals("")) {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ", SUPPLIER_PK = '" + _supplier_pk
                            + "',CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', UOM = '" + _uom
                            + "', GRADE = '" + _grade
                            // + "', LINE_NAME = '" + _line_name
                            // + "', TR_LINE_PK = " + _line_pk
                            + "', TLG_SA_SALEORDER_D_PK = " + _tlg_po_po_d_pk
                            + "  where PK = " + id;
                } else {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = " + label_pk
                            + ", ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = " + tr_qty
                            + ", TR_ITEM_PK = " + item_pk
                            + ", SUPPLIER_PK = " + _supplier_pk
                            + ",CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', UOM = '" + _uom
                            + "', GRADE = '" + _grade
                            + "', LINE_NAME = '" + _line_name
                            + "', TR_LINE_PK = '" + _line_pk
                            + "', TLG_SA_SALEORDER_D_PK = '" + _tlg_po_po_d_pk
                            + "  where PK = " + id;
                }
            } else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', LINE_NAME = '" + _line_name
                        + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                        + "'  where PK = " + id;
            }
            db.execSQL(sql);
        }
        db.close();

        return true;
    }

    public boolean updateSendStockDiscardOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);
        for (int i = 0; i < dt.totalrows; i++) {
            String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
            String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
            String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
            float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
            String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
            int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
            int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
            String _status = dt.records.get(i).get("status").toString();//status
            String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
            String _income_date = dt.records.get(i).get("income_dt").toString();//income date
            String _charger = dt.records.get(i).get("crt_by").toString();//charger
            String _supplier_name = dt.records.get(i).get("wh_name").toString();//Supplier
            String _supplier_pk = dt.records.get(i).get("wh_pk").toString();//Supplier_PK
            String _line_name = dt.records.get(i).get("line_name").toString();//LINE NAME
            String _line_pk = dt.records.get(i).get("line_pk").toString();//LINE PK
            String _uom = dt.records.get(i).get("uom").toString();//UOM
            String _tlg_po_po_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();//tlg_po_po_d_pk
            String _inv_tr_pl = dt.records.get(i).get("inv_tr_pk").toString() ;
            int id = Integer.parseInt(_inv_tr_pl);
            if (_status.equals("000")) {
                if (_line_pk.equals("")) {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = '" + label_pk
                            + "', ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = '" + tr_qty
                            + "', TR_ITEM_PK = '" + item_pk
                            + "', SUPPLIER_PK = '" + _supplier_pk
                            + "',CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', UOM = '" + _uom
                            // + "', LINE_NAME = '" + _line_name
                            // + "', TR_LINE_PK = " + _line_pk
                            + "',  TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                            + "'  where PK = " + id;
                } else {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = '" + label_pk
                            + "', ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = '" + tr_qty
                            + "', TR_ITEM_PK = '" + item_pk
                            + "', SUPPLIER_PK = '" + _supplier_pk
                            + "',CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', UOM = '" + _uom
                            + "', LINE_NAME = '" + _line_name
                            + "', TR_LINE_PK = '" + _line_pk
                            + "',  TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                            + "'  where PK = " + id;
                }
            }
//            else if(_status.equals("001")){
//                sql = "UPDATE INV_TR set STATUS='" + _status
//                        + "', TR_TYPE='" + 14
//                        + "',  sent_yn = 'N'"
//                        + "  where PK = " + id;
//
//
//            }
            else { //TODO
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = '" + tr_qty
                        + "', TR_ITEM_PK = '" + item_pk
                        + "',CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', LINE_NAME = '" + _line_name
                        + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                        + "'  where PK = " + id;
            }
            db.execSQL(sql);
        }
        db.close();

        return true;
    }

    public boolean updateSendStockOtherInOutOK(String a[][]) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        for (int i = 0; i < a.length; i++) {
            String item_bc =  a[i][0].toString();//ITEM_BC
            String item_code =  a[i][1];//ITEM_CODE
            String item_name =  a[i][2];//ITEM_NAME
            float tr_qty = Float.parseFloat( a[i][3]);//QTY
            String lot_no =  a[i][4];//LOT_NO
            int label_pk = Integer.parseInt( a[i][5]);//LABEL_PK
            int item_pk = Integer.parseInt( a[i][6]);//TLG_IT_ITEM_PK
            String _status =  a[i][7];//status
            String _slip_no =  a[i][8];//slip no
            String _income_date =  a[i][9];//income date
            String _charger =  a[i][10];//charger
            String _supplier_name =  a[i][11];//Supplier
            String _supplier_pk =  a[i][12];//Supplier_PK
            String _loc_name =  a[i][13];//LOC NAME
            String _loc_pk =  a[i][14];//LOC PK
            String _grade =  a[i][15]; //GRADE
            String _line_name =  a[i][16];//LINE NAME
            String _line_pk =  a[i][17];//LINE PK
            String _uom =  a[i][18];//UOM
            String _tlg_po_po_d_pk =  a[i][19];//tlg_po_po_d_pk
            String _inv_tr_pl =  a[i][20] ;
            int id = Integer.parseInt(_inv_tr_pl);
            if (_status.equals("000")) {
                if (_line_pk.equals("")) {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = '" + label_pk
                            + "', ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = '" + tr_qty
                            + "', TR_ITEM_PK = '" + item_pk
                            + "', SUPPLIER_PK = '" + _supplier_pk
                            + "',CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', GRADE = '" + _grade
//                            + "', TLG_IN_WHLOC_IN_PK = '" + _loc_pk
//                            + "', TLG_IN_WHLOC_IN_NAME = '" + _loc_name
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', UOM = '" + _uom
                             + "', LINE_NAME = '" + _line_name
                             + "', TR_LINE_PK = " + _line_pk
                            + "',  TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                            + "'  where PK = " + id;
                } else {
                    sql = "UPDATE INV_TR set STATUS='" + _status
                            + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                            + "', TLG_POP_LABEL_PK = '" + label_pk
                            + "', ITEM_CODE = '" + item_code
                            + "', ITEM_NAME = '" + item_name
                            + "', TR_QTY = '" + tr_qty
                            + "', TR_ITEM_PK = '" + item_pk
                            + "', SUPPLIER_PK = '" + _supplier_pk
                            + "',CHARGER='" + _charger
                            + "',INCOME_DATE='" + _income_date
                            + "', TR_LOT_NO='" + lot_no
                            + "', SLIP_NO = '" + _slip_no
                            + "', GRADE = '" + _grade
//                            + "', TLG_IN_WHLOC_IN_PK = '" + _loc_pk
//                            + "', TLG_IN_WHLOC_IN_NAME = '" + _loc_name
                            + "', SUPPLIER_NAME = '" + _supplier_name
                            + "', UOM = '" + _uom
                            + "', LINE_NAME = '" + _line_name
                            + "', TR_LINE_PK = '" + _line_pk
                            + "',  TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                            + "'  where PK = " + id;
                }
            }
//            else if(_status.equals("001")){
//                sql = "UPDATE INV_TR set STATUS='" + _status
//                        + "', TR_TYPE='" + 14
//                        + "',  sent_yn = 'N'"
//                        + "  where PK = " + id;
//
//
//            }
            else { //TODO
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = '" + tr_qty
                        + "', TR_ITEM_PK = '" + item_pk
                        + "',CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', GRADE = '" + _grade
//                        + "', TLG_IN_WHLOC_IN_PK = '" + _loc_pk
//                        + "', TLG_IN_WHLOC_IN_NAME = '" + _loc_name
                        + "', UOM = '" + _uom
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', LINE_NAME = '" + _line_name
                        + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                        + "'  where PK = " + id;
            }
            db.execSQL(sql);
        }
        db.close();

        return true;
    }


    public boolean updateSendStockOtherOK(String a[][]) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        for (int i = 0; i < a.length; i++) {
            String item_bc = a[i][0];//ITEM_BC
            String item_code = a[i][1];//ITEM_CODE
            String item_name = a[i][2];//ITEM_NAME
            float tr_qty = Float.parseFloat(a[i][3]);//QTY
            String lot_no = a[i][4];//LOT_NO
            int label_pk = Integer.parseInt(a[i][5]);//LABEL_PK
            int item_pk = Integer.parseInt(a[i][6]);//TLG_IT_ITEM_PK
            String _status = a[i][7];//status
            String _slip_no = a[i][8];//slip no
            String _income_date = a[i][9];//income date
            String _charger = a[i][10];//charger
            String _supplier_name = a[i][11];//Supplier
            String _supplier_pk = a[i][12];//Supplier_PK
            String _line_name = a[i][13];//LINE NAME
            Float _line_pk = Float.parseFloat(a[i][14]);//LINE PK
            String _uom = a[i][15];//UOM
            String _tlg_po_po_d_pk = a[i][16];//tlg_po_po_d_pk
            int id = Integer.parseInt(a[i][17]);
            if (_status.equals("000")) {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ", SUPPLIER_PK = " + _supplier_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', UOM = '" + _uom
                        + "', LINE_NAME = '" + _line_name
                        + "', TR_LINE_PK = " + _line_pk
                        + "',  TLG_PO_PO_D_PK = " + _tlg_po_po_d_pk
                        + "  where PK = " + id;
            } else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', LINE_NAME = '" + _line_name
                        + "', TLG_PO_PO_D_PK = " + _tlg_po_po_d_pk
                        + "  where PK = " + id;
            }
            db.execSQL(sql);
        }
        db.close();

        return true;
    }

    public boolean updateSendGoodsReturnOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);
        for (int i = 0; i < dt.totalrows; i++) {
            String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
            String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
            String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
            float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
            String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
            int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
            int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
            String _status = dt.records.get(i).get("status").toString();//status
            String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
            String _income_date = dt.records.get(i).get("income_dt").toString();//income date
            String _charger = dt.records.get(i).get("crt_by").toString();//charger
            String _supplier_name = dt.records.get(i).get("wh_name").toString();//Supplier
            String _supplier_pk = dt.records.get(i).get("cust_pk").toString();//Supplier_PK
            String _line_name = dt.records.get(i).get("line_name").toString();//LINE NAME
            Float _unit_price = Float.parseFloat(dt.records.get(i).get("unit_price").toString());
            String _uom = dt.records.get(i).get("uom").toString();//UOM
            String _tlg_po_po_d_pk = dt.records.get(i).get("saleorder_d_pk").toString();//tlg_po_po_d_pk
            int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());
            if (_status.equals("000")) {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ", SUPPLIER_PK = '" + _supplier_pk
                        + "',CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        //+ "', SUPPLIER_NAME = '" + _supplier_name
                        + "', UOM = '" + _uom
                        //+ "', LINE_NAME = '" + _line_name
                        //+ "', TR_LINE_PK = " + _line_pk
                        + "',  TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                        + "'  where PK = " + id;
            } else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', LINE_NAME = '" + _line_name
                        + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                        + "'  where PK = " + id;
            }
            db.execSQL(sql);
        }
        db.close();

        return true;
    }

    public boolean updateSendReturnSupplierOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);
        for (int i = 0; i < dt.totalrows; i++) {
            String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
            String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
            String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
            float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
            String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
            int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
            int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
            String _status = dt.records.get(i).get("status").toString();//status
            String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
            String _income_date = dt.records.get(i).get("income_dt").toString();//income date
            String _charger = dt.records.get(i).get("crt_by").toString();//charger
            String _wh_name = dt.records.get(i).get("wh_name").toString();//WH_NAME
            String _wh_pk = dt.records.get(i).get("wh_pk").toString();//WH_PK
            String _po_no = dt.records.get(i).get("po_no").toString();//po_no
            String _supplier_pk = dt.records.get(i).get("supplier_pk").toString();//SUPPLIER_PK
            String _supplier_name = dt.records.get(i).get("supplier_name").toString();//SUPPLIER_NAME
            String _uom = dt.records.get(i).get("uom").toString();//UOM
            String _tlg_po_po_d_pk = dt.records.get(i).get("po_d_pk").toString();//tlg_po_po_d_pk
            int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());
            if (_status.equals("000")) {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ", TR_WH_IN_PK = " + _wh_pk
                        + ", SUPPLIER_PK = " + _supplier_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', TR_WH_IN_NAME = '" + _wh_name
                        + "', UOM = '" + _uom
                        + "', PO_NO = '" + _po_no
                        + "', TLG_PO_PO_D_PK = " + _tlg_po_po_d_pk
                        //+ ", UNIT_PRICE = " + _unit_price
                        + "  where PK = " + id;
            } else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', TR_WH_IN_NAME = '" + _wh_name
                        + "', LINE_NAME = '" + _supplier_name
                        + "', TLG_PO_PO_D_PK = " + _tlg_po_po_d_pk
                        + "  where PK = " + id;
            }
            db.execSQL(sql);
        }
        db.close();

        return true;
    }

    public String updateSendEvaluationOK(JResultData a) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());
        String _status = "";
        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        List<JDataTable> jdt = a.getListODataTable();
        JDataTable dt = jdt.get(0);
        for (int i = 0; i < dt.totalrows; i++) {
            String item_bc = dt.records.get(i).get("item_bc").toString();//ITEM_BC
            String item_code = dt.records.get(i).get("item_code").toString();//ITEM_CODE
            String item_name = dt.records.get(i).get("item_name").toString();//ITEM_NAME
            float tr_qty = Float.parseFloat(dt.records.get(i).get("qty").toString());//QTY
            String lot_no = dt.records.get(i).get("lot_no").toString();//LOT_NO
            int label_pk = Integer.parseInt(dt.records.get(i).get("label_pk").toString());//LABEL_PK
            int item_pk = Integer.parseInt(dt.records.get(i).get("item_pk").toString());//TLG_IT_ITEM_PK
            _status = dt.records.get(i).get("status").toString();//status
            String _slip_no = dt.records.get(i).get("slip_no").toString();//slip no
            String _income_date = dt.records.get(i).get("income_dt").toString();//income date
            String _charger = dt.records.get(i).get("crt_by").toString();//charger
            String _supplier_name = dt.records.get(i).get("wh_name").toString();//Supplier
            String _supplier_pk = dt.records.get(i).get("wh_pk").toString();//Supplier_PK
            String _line_name = dt.records.get(i).get("line_name").toString();//po_no
            Float _unit_price = Float.parseFloat(dt.records.get(i).get("line_pk").toString());//unit_price
            String _uom = dt.records.get(i).get("uom").toString();//UOM
            String _tlg_po_po_d_pk = dt.records.get(i).get("po_d_pk").toString();//tlg_po_po_d_pk
            String _count_child = dt.records.get(i).get("count_child").toString();
            int id = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());
            if (_status.equals("000") || _status.equals("002")) {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ", SUPPLIER_PK = '" + _supplier_pk
                        + "',CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', UOM = '" + _uom
                        + "', PO_NO = '" + _line_name
                        + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                        + "', UNIT_PRICE = '" + _unit_price
                        + "', ITEM_BC_CHILD = '" + _count_child
                        + "'  where PK = " + id;
            } else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + label_pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', SUPPLIER_NAME = '" + _supplier_name
                        + "', LINE_NAME = '" + _line_name
                        + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                        + "', ITEM_BC_CHILD = '" + _count_child
                        + "'  where PK = " + id;
            }
            db.execSQL(sql);
        }
        db.close();

        return _status;
    }

    public boolean updateSendGoodsDeliV4OK(JResultData ja, String formID) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String sent_time = df.format(c.getTime());
        String scan_date = CDate.getDateyyyyMMdd();
        String scan_time = CDate.getDateYMDHHmmss();

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        String sql1 = "";
        List<JDataTable> jdt = ja.getListODataTable();
        JDataTable dt = jdt.get(0);
        for (int i = 0; i < dt.totalrows; i++) {
            String slipNO = dt.records.get(i).get("slip_no").toString();//SLIP_NO
            String tlg_gd_req_d_pk = dt.records.get(i).get("tlg_gd_req_d_pk").toString();//TLG_GD_REQ_D_PK
            int req_item_pk = Integer.parseInt(dt.records.get(i).get("req_item_pk").toString());//REQ_ITEM_PK
            float req_qty = Float.parseFloat(dt.records.get(i).get("req_qty").toString());//REQ_QTY
            String out_wh_pk = dt.records.get(i).get("out_wh_pk").toString();//OUT_WH_PK
            int tlg_pop_lable_pk = Integer.parseInt(dt.records.get(i).get("tlg_pop_label_pk").toString());//TLG_POP_LABEL_PK
            int tlg_it_item_pk = Integer.parseInt(dt.records.get(i).get("tlg_it_item_pk").toString());//tlg_it_item_pk
            String item_bc = dt.records.get(i).get("item_bc").toString();//item_bc
            String label_uom = dt.records.get(i).get("uom").toString();//label_uom
            String yymmdd = dt.records.get(i).get("yymmdd").toString();//yymmdd
            String item_code = dt.records.get(i).get("item_code").toString();//item_code
            String item_name = dt.records.get(i).get("item_name").toString();//item_name
            String item_type = dt.records.get(i).get("item_type").toString();//item_type
            float label_qty = Float.parseFloat(dt.records.get(i).get("label_qty").toString());//label_qty
            String status = dt.records.get(i).get("status").toString();//status
            String cust_pk = dt.records.get(i).get("tco_buspartner_pk").toString();//TCO_BUSPARTNER_PK
            String tlg_sa_saleorder_d_pk = dt.records.get(i).get("tlg_sa_saleorder_d_pk").toString();//TLG_SA_SALEORDER_D_PK
            String lotNO = dt.records.get(i).get("lot_no").toString();//lot_no
            String tlg_gd_req_m_pk = dt.records.get(i).get("tlg_gd_req_m_pk").toString();//TLG_GD_REQ_M_PK
            String income_date = dt.records.get(i).get("income_dt").toString();//INCOME_DATE
            String wh_name = dt.records.get(i).get("warehouse").toString();//WAREHOUSE
            String charger = dt.records.get(i).get("charger").toString();//CHARGER
            int pk = Integer.parseInt(dt.records.get(i).get("inv_tr_pk").toString());///PK return
            String _parent_pk = dt.records.get(i).get("parent_pk").toString();//PARENT_PK
            String req_no = dt.records.get(i).get("req_no").toString();
            String grade = dt.records.get(i).get("grade").toString();
            String loc_pk = dt.records.get(i).get("whloc_pk").toString();
            if (status.equals("008")) {
                sql = "UPDATE INV_TR set STATUS='" + status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk
                        + ", ITEM_CODE = '" +item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + req_qty
                        + ",TLG_GD_REQ_M_PK='" + tlg_gd_req_m_pk
                        + "', TLG_GD_REQ_D_PK = '" + tlg_gd_req_d_pk
                        + "', TR_ITEM_PK = " + tlg_it_item_pk
                        + ", TR_WH_OUT_PK = " + out_wh_pk
                        + ",TR_WH_OUT_NAME='" + wh_name
                        + "',CHARGER='" + charger
                        + "',INCOME_DATE='" + income_date
                        + "',TR_DATE='" + yymmdd
                        + "', TR_LOT_NO='"  + lotNO
                        + "',UOM = '" +  label_uom
                        + "',TLG_SA_SALEORDER_D_PK='" + tlg_sa_saleorder_d_pk
                        + "',CUST_PK='" + cust_pk
                        + "', TLG_IN_WHLOC_IN_PK = '" + loc_pk
                        + "', GRADE = '" + grade
                        + "', SLIP_NO = '" + slipNO
                        + "'  where STATUS=' ' and PK = " + pk;
                db.execSQL(sql);

            } else {
                if (status.equals("000") && !_parent_pk.equals("")) {
                    boolean kq = isExistBarcode(item_bc, formID);//check Exits
                    db = getWritableDatabase();
                    if (!kq) {
                        sql = "INSERT INTO INV_TR(ITEM_BC,TR_WH_OUT_PK,TR_WH_OUT_NAME,GD_SLIP_NO, CUST_PK,TLG_GD_REQ_M_PK,SLIP_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE, " +
                                "TLG_POP_LABEL_PK,ITEM_CODE,ITEM_NAME,TR_QTY,TR_ITEM_PK,CHARGER,TR_LOT_NO,UOM,TLG_SA_SALEORDER_D_PK,TLG_IN_WHLOC_IN_PK,GRADE,TLG_GD_REQ_D_PK, ) "
                                + "VALUES('"
                                + item_bc + "','"
                                + out_wh_pk + "','"
                                + wh_name + "','"
                                + slipNO + "','"
                                + cust_pk + "','"
                                + tlg_gd_req_m_pk + "','"
                                + "-" + "','"
                                + scan_date + "','"
                                + scan_time + "','"
                                + "Y" + "','"
                                + status + "','"
                                + formID + "',"
                                + tlg_pop_lable_pk + ",'"
                                + item_code + "','"
                                + item_name + "',"
                                + req_qty + ","
                                + tlg_it_item_pk + ",'"
                                + charger + "','"
                                + lotNO + "','"
                                + label_uom + "','"
                                + tlg_sa_saleorder_d_pk + "','"
                                + loc_pk + "','"
                                + grade + "','"
                                + tlg_gd_req_d_pk + "'"
                                + ")";
                        db.execSQL(sql);
                        if (status.equals("000")) {
                            sql1 = "UPDATE INV_TR set REQ_BAL = CAST((CAST(REQ_BAL AS INTEGER)-" + req_qty + ") AS TEXT), SCANNED = CAST((CAST(SCANNED AS INTEGER)+" + req_qty + ") AS TEXT) " +
                                    " WHERE REQ_NO = '" + req_no + "' AND TR_ITEM_PK = " + tlg_it_item_pk + " AND ITEM_BC IS NULL AND TLG_GD_REQ_D_PK = '" + tlg_gd_req_d_pk + "'";
                            db.execSQL(sql1);
                        }

                        //update parent bc
                        sql = "UPDATE INV_TR set STATUS=' '"
                                + ", DEL_IF = " + pk
                                + ",SENT_YN = 'Y'"
                                + "  where PK = " + pk;
                        db.execSQL(sql);
                    }

                } else {
                    boolean kq = isExistBarcode000(item_bc, formID);//check Exits
                    if (kq == false) {
                        sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time + "', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                                item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + req_qty + ",TLG_GD_REQ_M_PK='" + tlg_gd_req_m_pk + "', TLG_GD_REQ_D_PK = '" + tlg_gd_req_d_pk + "', TR_ITEM_PK = " +
                                tlg_it_item_pk + ", TR_WH_OUT_PK = '" + out_wh_pk + "',TR_WH_OUT_NAME='" + wh_name + "',CHARGER='" + charger + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                                label_uom + "',TLG_SA_SALEORDER_D_PK='" + tlg_sa_saleorder_d_pk + "',CUST_PK='" + cust_pk
                                + "', TLG_IN_WHLOC_IN_PK = '" + loc_pk
                                + "', GRADE = '" + grade
                                + "', SLIP_NO = '" + slipNO + "'  where PK = " + pk;
                        db.execSQL(sql);
                        if (status.equals("000")) {
                            sql1 = "UPDATE INV_TR set REQ_BAL = CAST((CAST(REQ_BAL AS INTEGER)-" + req_qty + ") AS TEXT), SCANNED = CAST((CAST(SCANNED AS INTEGER)+" + req_qty + ") AS TEXT) " +
                                    " WHERE REQ_NO = '" + req_no + "' AND TR_ITEM_PK = " + tlg_it_item_pk + " AND ITEM_BC IS NULL AND TLG_GD_REQ_D_PK = '" + tlg_gd_req_d_pk + "'";
                            db.execSQL(sql1);
                        }
                    }
                }
            }
        }
        db.close();

        return true;
    }
    public boolean isExistBarcode000(String l_bc_item, String type) {
        boolean flag = false;
        SQLiteDatabase db1 = getReadableDatabase();
        try {
            //String countQuery = "SELECT PK FROM INV_TR where tr_type='"+type+"'  AND del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";
            //String countQuery = "SELECT PK FROM INV_TR where  tr_type='"+type+"' AND (status='000' or status=' ') and del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";
            String countQuery = "SELECT PK FROM INV_TR where  tr_type='" + type + "' AND status='000'  and ITEM_BC ='" + l_bc_item + "' ";
            Cursor cursor = db1.rawQuery(countQuery, null);
            if (cursor != null && cursor.moveToFirst()) {
                flag = true;
            }
            cursor.close();
        } catch (Exception ex) {
            //Toast.makeText(this, "Check_Exist_PK :" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
//            db1.close();
        }
        return flag;
    }

    public boolean updateSendUpdateLocationOK(String a[][]) {

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        for (int i = 0; i < a.length; i++) {

            String item_bc = a[i][0];//item_bc
            String item_code = a[i][1];//item_code
            String item_name = a[i][2];//item_name
            float label_qty = Float.parseFloat(a[i][3]);//label_qty
            String lotNO = a[i][4];//lot_no
            int tlg_it_item_pk = Integer.parseInt(a[i][5]);//tlg_it_item_pk
            String status = a[i][6];//status
            int id = Integer.parseInt(a[i][7]);

            if (status.equals("000")) {
                sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y', ITEM_CODE = '" +
                        item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + label_qty + ", TR_ITEM_PK = " +
                        tlg_it_item_pk + ", TR_LOT_NO='" + lotNO + "" +
                        "'  where PK = " + id;
            } else {
                sql = "UPDATE INV_TR set STATUS='" + status + "',SENT_YN = 'Y', ITEM_CODE = '" +
                        item_code + "', ITEM_NAME = '" + item_name + "', TR_QTY = " + label_qty + ", TR_ITEM_PK = " +
                        tlg_it_item_pk + ", TR_LOT_NO='" + lotNO + "',TR_WH_IN_NAME='', TLG_IN_WHLOC_IN_NAME='' " +
                        "  where PK = " + id;
            }
            db.execSQL(sql);
        }
        db.close();

        return true;
    }

    //endregion
    //region -------------DELETE BARCODE-----------
    public boolean deleteBarcode(String item_bc, String formID) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "DELETE FROM INV_TR where TR_TYPE ='" + formID + "' and ITEM_BC='" + item_bc + "'";
        db.execSQL(sql);

        db.close();

        return true;
    }

    public boolean updateSendTestProcessOK(String a[][]) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sent_time = df.format(c.getTime());
        String scan_date = CDate.getDateyyyyMMdd();
        String scan_time = CDate.getDateYMDHHmmss();

        SQLiteDatabase db = getWritableDatabase();
        String sql = "";
        int row = a.length;
        for (int i = 0; i < row; i++) {
            String item_bc = a[i][0];//ITEM_BC
            String item_code = a[i][1];//ITEM_CODE
            String item_name = a[i][2];//ITEM_NAME
            float tr_qty = Float.parseFloat(a[i][3]);//QTY
            String lot_no = a[i][4];//LOT_NO
            int label_pk = Integer.parseInt(a[i][5]);//LABEL_PK
            int item_pk = Integer.parseInt(a[i][6]);//TLG_IT_ITEM_PK
            String _status = a[i][7];//status
            String _slip_no = a[i][8];//slip no
            String _income_date = a[i][9];//income date
            String _charger = a[i][10];
            String _wh_name = a[i][11];
            String _wh_pk = a[i][12];
            String _line_name = a[i][13];
            int _line_pk = Integer.parseInt(a[i][14]);
            String _uom = a[i][15];//UOM
            String _tsa_saleorder_d_pk = a[i][16];
            int _pkID = Integer.parseInt(a[i][17]);//PK_TR
            String _parent_pk = a[i][18];
            String _pk = a[i][19];
            if (_status.equals("000")) {
                if (_parent_pk.equals("0")) {
                    if (_line_pk == 0) {
                        sql = "UPDATE INV_TR set STATUS='" + _status
                                + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                                + "', TLG_POP_LABEL_PK = " + _pk
                                + ", ITEM_CODE = '" + item_code
                                + "', ITEM_NAME = '" + item_name
                                + "', TR_QTY = " + tr_qty
                                + ", TR_ITEM_PK = " + item_pk
                                + ",CHARGER='" + _charger
                                + "',INCOME_DATE='" + _income_date
                                + "', TR_LOT_NO='" + lot_no
                                + "', SLIP_NO = '" + _slip_no
                                + "', UOM = '" + _uom
                                + "', PO_NO = '" + _wh_name
                                + "', TLG_SA_SALEORDER_D_PK = " + _tsa_saleorder_d_pk
                                + "  where PK = " + _pkID;
                        db.execSQL(sql);

                        // update mapping
                        sql = "UPDATE INV_TR SET SENT_YN = 'Y' WHERE BC_TYPE = 'P' AND ITEM_BC = '" + item_bc + "'";
                        db.execSQL(sql);
                    } else {
                        sql = "UPDATE INV_TR set STATUS='" + _status
                                + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                                + "', TLG_POP_LABEL_PK = " + _pk
                                + ", ITEM_CODE = '" + item_code
                                + "', ITEM_NAME = '" + item_name
                                + "', TR_QTY = " + tr_qty
                                + ", TR_ITEM_PK = " + item_pk
                                + ",CHARGER='" + _charger
                                + "',INCOME_DATE='" + _income_date
                                + "', TR_LOT_NO='" + lot_no
                                + "', SLIP_NO = '" + _slip_no
                                + "', UOM = '" + _uom
                                + "', LINE_NAME = '" + _line_name
                                + "', TR_LINE_PK = " + _line_pk
                                + ", PO_NO = '" + _wh_name
                                + "', TLG_SA_SALEORDER_D_PK = " + _tsa_saleorder_d_pk
                                + "  where PK = " + _pkID;
                        db.execSQL(sql);
                    }
                } else {

                    sql = "INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK, TLG_IN_WHLOC_IN_NAME,TR_LINE_PK,LINE_NAME,SLIP_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE, " +
                            "TLG_POP_LABEL_PK,ITEM_CODE,ITEM_NAME,TR_QTY,TR_ITEM_PK,CHARGER,TR_LOT_NO,UOM,TLG_SA_SALEORDER_D_PK ) "
                            + "VALUES('"
                            + item_bc + "',"
                            + _wh_pk + ",'"
                            + _wh_name + "','"
                            + _slip_no + "','"
                            + "-" + "',"
                            + _line_pk + ",'"
                            + _line_name + "','"
                            + "-" + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "Y" + "','"
                            + _status + "','"
                            + "4" + "',"
                            + _pk + ",'"
                            + item_code + "','"
                            + item_name + "',"
                            + tr_qty + ","
                            + item_pk + ",'"
                            + _charger + "','"
                            + lot_no + "','"
                            + _uom + "',"
                            + _tsa_saleorder_d_pk
                            + ")";

                    db.execSQL(sql);
                    //update parent bc
                    sql = "UPDATE INV_TR set STATUS='001'"
                            + ", DEL_IF = " + _pkID
                            + ",SENT_YN = 'Y'"
                            + "  where PK = " + _pkID;
                    db.execSQL(sql);

                }
            } else {
                sql = "UPDATE INV_TR set STATUS='" + _status
                        + "',SENT_YN = 'Y', SENT_TIME = '" + sent_time
                        + "', TLG_POP_LABEL_PK = " + _pk
                        + ", ITEM_CODE = '" + item_code
                        + "', ITEM_NAME = '" + item_name
                        + "', TR_QTY = " + tr_qty
                        + ", TR_ITEM_PK = " + item_pk
                        + ",CHARGER='" + _charger
                        + "',INCOME_DATE='" + _income_date
                        + "', TR_LOT_NO='" + lot_no
                        + "', SLIP_NO = '" + _slip_no
                        + "', UOM = '" + _uom
                        + "', TR_WH_IN_NAME = '" + _wh_name
                        + "', LINE_NAME = '" + _line_name
                        + "', TLG_SA_SALEORDER_D_PK = " + _tsa_saleorder_d_pk
                        + "  where PK = " + _pkID;
                db.execSQL(sql);
            }
        }

        db.close();

        return true;
    }

    public boolean updateExeckeyStTransfer(String formID, Integer item_pk, Integer wh_pk, Integer in_wh_pk, String lot_no, String bc_type, String exec) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE INV_TR set EXECKEY='" + exec + "' " +
                "  where TR_ITEM_PK = " + item_pk + " and" +
                "        TR_WAREHOUSE_PK = " + wh_pk + " and" +
                "        TR_LINE_PK = " + in_wh_pk + " and" +
                "        TR_LOT_NO = '" + lot_no + "' and" +
                "        BC_TYPE = '" + bc_type + "' and" +
                "        TR_TYPE = '" + formID + "' and" +
                "      (EXECKEY is null or EXECKEY=' ' )";
        db.execSQL(sql);

        db.close();

        return true;
    }
}
