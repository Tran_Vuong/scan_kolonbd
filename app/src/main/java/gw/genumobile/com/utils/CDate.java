package gw.genumobile.com.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by TanLegend on 09/07/2015.
 */
public class CDate {
    public static Calendar c;
    public static SimpleDateFormat df;

    public CDate(){

    }
    public static String getDateFormat(String format){
        String formattedDate="";
        c = Calendar.getInstance();
        try {
            df = new SimpleDateFormat(format);
            formattedDate = df.format(c.getTime());
        } catch (Exception e) {
            df = new SimpleDateFormat("yyyyMMdd");
            formattedDate = df.format(c.getTime());
        }

        return formattedDate;
    }
    public static String getDateyyyyMMdd(){
        c = Calendar.getInstance();
        df = new SimpleDateFormat("yyyyMMdd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String getDateIncline(){
        c = Calendar.getInstance();
        df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    public static String getDateFormatyyMMdd(){
        c = Calendar.getInstance();
        df = new SimpleDateFormat("yy/MM/dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    public static String getDateFormatyyyyMMdd(){
        c = Calendar.getInstance();
        df = new SimpleDateFormat("yyyy/MM/dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    public static String getDateFormatHHmm(){
        c = Calendar.getInstance();
        df = new SimpleDateFormat("HH:mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    public static String getDateyyyyMMddhhmmss(){
        c = Calendar.getInstance();
        df = new SimpleDateFormat("yyyyMMddhhmmss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    public static String getDateyyMMddhhmmsss(){
        c = Calendar.getInstance();
        df = new SimpleDateFormat("yyMMddhhmmssSSS");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    public static String getDateyyMMdd(){
        c = Calendar.getInstance();
        df = new SimpleDateFormat("yyMMdd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    public static String getDateYMDHHmmss(){
        c = Calendar.getInstance();
        df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String getDatePrevious (int dtime){
        Calendar curDate = Calendar.getInstance();
        curDate.add(Calendar.DATE, -dtime);
        df = new SimpleDateFormat("yyyyMMdd");
        String formattedDate = df.format(curDate.getTime());
        return formattedDate;
    }

    public static String getFormatYYYYMMDD(String yymmdd){

        SimpleDateFormat dt = new SimpleDateFormat("yyyyMMdd");
        Date date = null;
        try {
            date = dt.parse(yymmdd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
        return dt1.format(date);

    }





}
