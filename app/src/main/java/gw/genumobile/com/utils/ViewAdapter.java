package gw.genumobile.com.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by Administrator on 11-11-2015.
 */
public class ViewAdapter extends ArrayAdapter<ListItem> {

    private List<ListItem> _listItems;
    private Context         _context;

    public ViewAdapter(Context context, int textViewResourceId,List<ListItem> objects) {
        super(context, textViewResourceId, objects);
        this._context   = context;
        this._listItems = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DataView view = new DataView(this._context);
        view.setListItem(this._listItems.get(position));
        return view;
    }

    @Override
    public ListItem getItem(int position)
    {
        return _listItems.get(position);
    }
}
