package gw.genumobile.com.utils;

import android.app.Service;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import gw.genumobile.com.interfaces.ListViewMultipleSelectionActivity;
import gw.genumobile.com.R;

/**
 * Created by Administrator on 11-11-2015.
 */
public class DataView extends LinearLayout {
    public ImageView photo;
    public TextView     name;
    public TextView     life;
    public TextView     carrer;

    public DataView(Context context) {
        super(context);
        //đọc tập tin list_item.xml để lấy các thành phần
        LayoutInflater linflater = (LayoutInflater) ((ListViewMultipleSelectionActivity)context).getSystemService(Service.LAYOUT_INFLATER_SERVICE);
        linflater.inflate(R.layout.list_item, this);

        //lấy các thành  phần tương ứng
        this.photo  = (ImageView) findViewById(R.id.itemPhoto);
        this.name   = (TextView) findViewById(R.id.itemName);
        this.life   = (TextView) findViewById(R.id.itemLife);
        this.carrer = (TextView) findViewById(R.id.itemCarrer);
    }

    public void setListItem(ListItem item){
        this.photo.setImageResource(item.photo);
        this.name.setText(item.name);
        this.life.setText(item.life);
        this.carrer.setText(item.career);
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }
}
