package gw.genumobile.com.utils;

import android.util.Log;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by TL on 08/18/2015.
 */
public class CNumber {

    public CNumber(){

    }

    public static String getNumberDecinal2(double x){
        NumberFormat formatter = new DecimalFormat("#0.00");
        //DecimalFormat df = new DecimalFormat("#.##");
        String val =formatter.format(x);
        return val;
    }
    public static String getNumberDecinal2(float x){

        String val =String.format("%.02f", x);
        return val;
    }

    public static boolean checkNumberFloat(String x){

        try{
            double c = Double.parseDouble(x);
        }catch (Exception ex){
            return false;
        }
        return  true;
    }

    static public String customFormat(String pattern, String value ) {
        String output = value;
        try{
            Double db_value = Double.valueOf(value);

            DecimalFormat myFormatter = new DecimalFormat(pattern);
            output = myFormatter.format(db_value);
        }catch (Exception ex){
            Log.e("customFormatL: ", ex.getMessage());
        }

        return output;
    }

    public static boolean isStringInt(String s)
    {
        try
        {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException ex)
        {
            return false;
        }
    }

    public static String isPushNumber(int x){
        String temp;
        if(x<10){
            temp="0"+String.valueOf(x);
        }
        else
            temp=String.valueOf(x);
        return temp;
    }
}
