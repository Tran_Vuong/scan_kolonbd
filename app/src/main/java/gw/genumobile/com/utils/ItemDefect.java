package gw.genumobile.com.utils;

/**
 * Created by admin on 11/02/2017.
 */

public class ItemDefect {
    String pk,defect_name,defect_id;

    public ItemDefect() {
    }

    public ItemDefect(String pk, String defect_name, String defect_id) {
        this.pk = pk;
        this.defect_name = defect_name;
        this.defect_id = defect_id;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getDefect_name() {
        return defect_name;
    }

    public void setDefect_name(String defect_name) {
        this.defect_name = defect_name;
    }

    public String getDefect_id() {
        return defect_id;
    }

    public void setDefect_id(String defect_id) {
        this.defect_id = defect_id;
    }
}
