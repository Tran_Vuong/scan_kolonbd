package gw.genumobile.com.utils;

/**
 * Created by admin on 16/01/2017.
 */

public class ItemMenu {
    String classID,img_name;

    public ItemMenu() {
    }

    public ItemMenu(String classID, String img_name) {

        this.classID = classID;
        this.img_name = img_name;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getImg_name() {
        return img_name;
    }

    public void setImg_name(String img_name) {
        this.img_name = img_name;
    }
}
