package gw.genumobile.com.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.apache.commons.logging.LogFactory;

/**
 * Created by TL on 12/22/2015.
 */
public class CNetwork {

    public static boolean isInternet(String ip,Boolean  checkIP)
    {
        if(checkIP) {
            boolean reachable = false;
            try {
                if (ip.indexOf(":") > 0)
                    ip = ip.substring(0, ip.indexOf(":"));

                Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 " + ip);

                int returnVal = p1.waitFor();
                if (returnVal == 0)
                    reachable = true;
                else
                    reachable = false;
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
            }
            return reachable;
        }
        else {
            return true;
        }
    }

    public static boolean haveNetworkConnection(Activity acct) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) acct.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected()) {
                    WifiManager wifiManager =(WifiManager) acct.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                    if (wifiInfo != null) {
                        Integer linkSpeed = wifiInfo.getLinkSpeed(); //measured using WifiInfo.LINK_SPEED_UNITS
                        Log.i("haveNetworkConnection::",linkSpeed.toString());
                    }
                }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
   public  static  boolean speedtest(){
        long mStartRX = 0;
        long mStartTX = 0;
       mStartRX = TrafficStats.getTotalRxBytes();
       mStartTX = TrafficStats.getTotalTxBytes();
       if (mStartRX == TrafficStats.UNSUPPORTED || mStartTX == TrafficStats.UNSUPPORTED) {
           Log.i("speedtest: ","Your device does not support traffic stat monitoring");
       } else {
           long rxBytes = TrafficStats.getTotalRxBytes() - mStartRX;
           Log.i("speedtest:RX ",Long.toString(rxBytes));
           long txBytes = TrafficStats.getTotalTxBytes() - mStartTX;
           Log.i("speedtest:TX ",Long.toString(txBytes));
       }
       return  true;
   }
    public static boolean isConnectedFast(Activity acct){
        ConnectivityManager cm = (ConnectivityManager) acct.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return (info != null && info.isConnected() && isConnectionFast(info.getType(),info.getSubtype()));
    }
    public static boolean isConnectionFast(int type, int subType){
        if(type==ConnectivityManager.TYPE_WIFI){
            return true;
        }else if(type==ConnectivityManager.TYPE_MOBILE){
            switch(subType){
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return true; // ~ 400-7000 kbps
			/*
			 * Above API level 7, make sure to set android:targetSdkVersion
			 * to appropriate level to use these
			 */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return false;
            }
        }else{
            return false;
        }
    }

    public static boolean getConnectedNetWorkInformation(Activity acct)  {
        WifiManager wifiManager = (WifiManager) acct.getSystemService(Context.WIFI_SERVICE);
        WifiInfo mWifiInfo = wifiManager.getConnectionInfo();
        String message;
        if(mWifiInfo != null){
            String ssid = mWifiInfo.getSSID();
            int wifiSignalStrength = WifiManager.calculateSignalLevel(mWifiInfo.getRssi(), 4);
            int link_speed = mWifiInfo.getLinkSpeed(); //'Mbps
            message = String.format("connected to %s with signal strength %s and link speed of %s", ssid, wifiSignalStrength, link_speed);
            if(wifiSignalStrength==0||link_speed==0)
                return  false;
                return true;
        }
        else{

            message = "Not Conntected to Any Wifi Network";
            return  false;
        }

      ////  gsmAsuToSignal  ->mobile
    }
}
