package gw.genumobile.com.utils;

/**
 * Created by Administrator on 11-11-2015.
 */
public class ListItem {
    public String   name;
    public int      photo;
    public String   life;
    public String   career;

    public ListItem(String name, int photo, String life, String carrer){
        this.name       = name;
        this.photo      = photo;
        this.life       = life;
        this.career     = carrer;
    }
}
