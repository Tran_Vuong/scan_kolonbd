package gw.genumobile.com.views.qc_management;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.utils.Utils;

import java.net.SocketImplFactory;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import gw.genumobile.com.R;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.utils.CNumber;
import gw.genumobile.com.views.gwFragmentDialog;
import gw.genumobile.com.views.gwcore.BaseGwActive;


/** Created by HNDGiang on 08/01/2016. **/

/*
*   Load Detail With: PK, ITEM CODE
*   Update: 09/01/2016
*   Author: HNDGiang
*   drivtldrlgtl0001_s_10 --customer
*   drivtldrlgtl0001_s_11 --inspector
*   drivtldrlgtl0001_s_12 --checked
*   drivtldrlgtl0001_s_13 --approve
*/
public class Popup_QC_Management_Detail extends gwFragmentDialog {

    //region Avarible
    static final int DATE_PICKER_INCOME_ID = 1113;
    protected SharedPreferences appPrefs;

    public static final int REQUEST_CODE = 1;
    int indexPrevious = -1;

    Queue queue = new LinkedList();
    int orangeColor = Color.rgb(255,99,71);

    private String
                    INSPECTOR_NAME,
                    CHECKED_NAME,
                    APPROVE_NAME,
                    LINE_GROUP_NAME,
                    LINE_NAME,
                    LOT_NO,
                    LOT_NO_SUPPLIER,
                    LOT_QTY;

    private int
                    CUSTOMER_PK,
                    INSPECTOR_PK,
                    CHECKED_PK,
                    APPROVE_PK,
                    LINE_GROUP_PK,
                    LINE_PK, STATUS = 0;

    private static final int STATUS_APPROVE = 3;

    boolean postBack = false;
    private String  CUSTOMER_NAME, ITEM_PK,MOLD_CODE="",incomeDT_db="";

    String SHIFT_A = "F", SHIFT_B= "F", SHIFT_C = "F";

    private CheckBox cbSHIFT_A, cbSHIFT_B, cbSHIFT_C;

    Spinner spLineGroup, spLine;

    LinkedHashMap hashMapGroupLinePK = new LinkedHashMap();
    LinkedHashMap hashMapLinePK = new LinkedHashMap();
    LinkedHashMap hsCavity = new LinkedHashMap();

    private Button btnIncomeDt;
    private int ACTION_POPUP_INSPECTOR = 2;
    private int ACTION_POPUP_CHECKED = 3;
    private int ACTION_POPUP_APPROVE = 4;

    List<String> lstGroupName;
    List<String> lstLineName;

    EditText etLotNo,etIncomeDt,etLotQty,etLotNoSupplier;
    //endregion
    private int year;
    private int month;
    private int day;
    //region Application Circle
    View vContent;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //type = getArguments().getString("type");
        //gwAc=getActivity();
        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_TITLE, getTheme());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vContent = inflater.inflate(R.layout.popup_qc_management_detail, container, false);

        Toolbar toolbar = (Toolbar) vContent.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                    Intent i = new Intent()
                            .putExtra("month", "trang");
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                    dismiss();
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);
        toolbar.setTitle("Detail ");

        gwMActivity.AddEvenKeyboardHiddenWhenClickOutSide(vContent.findViewById(R.id.POPUP_QC_DETAIL));        
        spLineGroup = (Spinner)vContent.findViewById(R.id.spLineGroup);
        spLine = (Spinner)vContent.findViewById(R.id.spLine);

        etLotNo = (EditText) vContent.findViewById(R.id.etLotNo);
        etLotQty = (EditText) vContent.findViewById(R.id.etLotQty);
        etIncomeDt=(EditText) vContent.findViewById(R.id.etIncomeDate);
        etLotNoSupplier=(EditText) vContent.findViewById(R.id.etLotNoSupplier);

        btnIncomeDt = (Button) vContent.findViewById(R.id.btnIncomeDate);
        btnIncomeDt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Calendar c1 = Calendar.getInstance();
                c1.add(Calendar.DATE, -1);
                year  = c1.get(Calendar.YEAR);
                month = c1.get(Calendar.MONTH);
                day   = c1.get(Calendar.DAY_OF_MONTH);
                incomeDT_db=String.valueOf(year)+String.valueOf(CNumber.isPushNumber(month+1))+String.valueOf(CNumber.isPushNumber(day));
                etIncomeDt.setText(new StringBuilder()
                        // Month is 0 based, just add 1
                        .append(CNumber.isPushNumber(day)).append("-").append(CNumber.isPushNumber(month + 1)).append("-").append(year).append(" "));
                gwMActivity.showDialog(DATE_PICKER_INCOME_ID);
            }
        });

        cbSHIFT_A = (CheckBox)vContent.findViewById(R.id.cbA);
        cbSHIFT_A.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(STATUS!= STATUS_APPROVE) {
                    SHIFT_A = cbSHIFT_A.isChecked() ? "T" : "F";
                    if (cbSHIFT_A.isChecked() == true) {
                        cbSHIFT_A.setTextSize(27);
                        cbSHIFT_A.setTextColor(Color.BLUE);

                        cbSHIFT_B.setChecked(false);
                        cbSHIFT_B.setTextSize(20);
                        cbSHIFT_B.setTextColor(Color.BLACK);

                        cbSHIFT_C.setChecked(false);
                        cbSHIFT_C.setTextSize(20);
                        cbSHIFT_C.setTextColor(Color.BLACK);
                    } else {
                        cbSHIFT_A.setTextSize(20);
                        cbSHIFT_A.setTextColor(Color.BLACK);
                    }
                }else{
                    boolean setBoolean = SHIFT_A.equals("T")?true:false;
                    cbSHIFT_A.setChecked(setBoolean);
                }
            }
        });

        cbSHIFT_B = (CheckBox)vContent.findViewById(R.id.cbB);
        cbSHIFT_B.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(STATUS!= STATUS_APPROVE){
                    SHIFT_B = cbSHIFT_B.isChecked()?"T":"F";
                    if(cbSHIFT_B.isChecked() == true){
                        cbSHIFT_B.setTextSize(27);
                        cbSHIFT_B.setTextColor(Color.BLUE);

                        cbSHIFT_A.setChecked(false);
                        cbSHIFT_A.setTextSize(20);
                        cbSHIFT_A.setTextColor(Color.BLACK);

                        cbSHIFT_C.setChecked(false);
                        cbSHIFT_C.setTextSize(20);
                        cbSHIFT_C.setTextColor(Color.BLACK);
                    }else{
                        cbSHIFT_B.setTextSize(20);
                        cbSHIFT_B.setTextColor(Color.BLACK);
                    }
                }else{
                    boolean setBoolean = SHIFT_B.equals("T")?true:false;
                    cbSHIFT_B.setChecked(setBoolean);
                }
            }
        });

        cbSHIFT_C = (CheckBox)vContent.findViewById(R.id.cbC);
        cbSHIFT_C.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(STATUS!= STATUS_APPROVE) {
                    SHIFT_C = cbSHIFT_C.isChecked() ? "T" : "F";
                    if (cbSHIFT_C.isChecked() == true) {
                        cbSHIFT_C.setTextSize(27);
                        cbSHIFT_C.setTextColor(Color.BLUE);

                        cbSHIFT_B.setChecked(false);
                        cbSHIFT_B.setTextSize(20);
                        cbSHIFT_B.setTextColor(Color.BLACK);

                        cbSHIFT_A.setChecked(false);
                        cbSHIFT_A.setTextSize(20);
                        cbSHIFT_A.setTextColor(Color.BLACK);
                    } else {
                        cbSHIFT_C.setTextSize(20);
                        cbSHIFT_C.setTextColor(Color.BLACK);
                    }
                }else{
                    boolean setBoolean = SHIFT_C.equals("T")?true:false;
                    cbSHIFT_C.setChecked(setBoolean);
                }
            }
        });



        ShowLineGroup();
        Bundle bundleDetail = gwMActivity.getIntent().getExtras();


        if(bundleDetail != null){

            STATUS = bundleDetail.getInt("STATUS");

            CUSTOMER_PK = bundleDetail.getInt("CUSTOMER_PK");
            CUSTOMER_NAME = bundleDetail.getString("CUSTOMER_NAME");
            if(!CUSTOMER_NAME.equals("-1") && CUSTOMER_NAME.length() > 0){
                ((EditText)vContent.findViewById(R.id.etCustomer)).setText(CUSTOMER_NAME);
            }

            INSPECTOR_PK = bundleDetail.getInt("INSPECTOR_PK");
            INSPECTOR_NAME = bundleDetail.getString("INSPECTOR_NAME");
            if(!INSPECTOR_NAME.equals("-1") && INSPECTOR_NAME.length() > 0){
                ((EditText)vContent.findViewById(R.id.etInspector)).setText(INSPECTOR_NAME);
            }

            CHECKED_PK = bundleDetail.getInt("CHECKED_PK");
            CHECKED_NAME= bundleDetail.getString("CHECKED_NAME");
            if(!CHECKED_NAME.equals("-1") && CHECKED_NAME.length() > 0){
                ((EditText)vContent.findViewById(R.id.etChecked)).setText(CHECKED_NAME);
            }

            APPROVE_PK= bundleDetail.getInt("APPROVE_PK");
            APPROVE_NAME= bundleDetail.getString("APPROVE_NAME");
            if(!APPROVE_NAME.equals("-1") && APPROVE_NAME.length() > 0){
                ((EditText)vContent.findViewById(R.id.etApprove)).setText(APPROVE_NAME);
            }

            LINE_GROUP_PK = bundleDetail.getInt("LINE_GROUP_PK");
            LINE_GROUP_NAME = bundleDetail.getString("LINE_GROUP_NAME");
            LINE_PK     = bundleDetail.getInt("LINE_PK");
            LINE_NAME   = bundleDetail.getString("LINE_NAME");

            LOT_NO      = bundleDetail.getString("LOT_NO");
            if(!LOT_NO.equals("-")){
                ((EditText)vContent.findViewById(R.id.etLotNo)).setText(LOT_NO);
            }else{
                ((EditText)vContent.findViewById(R.id.etLotNo)).setText("");
            }
            LOT_QTY      = bundleDetail.getString("LOT_QTY");
            if(!LOT_QTY.equals("-")){
                etLotQty.setText(LOT_QTY);
            }else{
                etLotQty.setText("");
            }
            incomeDT_db = bundleDetail.getString("INCOME_DT");
            etIncomeDt.setText(incomeDT_db);

            LOT_NO_SUPPLIER=bundleDetail.getString("LOT_NO_SUPPLIER");
            etLotNoSupplier.setText(LOT_NO_SUPPLIER);

            ITEM_PK = bundleDetail.getString("ITEM_PK");

            SHIFT_A    = bundleDetail.getString("SHIFT_A");
            SHIFT_B    = bundleDetail.getString("SHIFT_B");
            SHIFT_C    = bundleDetail.getString("SHIFT_C");



            if(SHIFT_A.equals("T")){
                cbSHIFT_A.setChecked(true);
            }else{
                cbSHIFT_A.setChecked(false);
            }

            if(SHIFT_B.equals("T")){
                cbSHIFT_B.setChecked(true);
            }else{
                cbSHIFT_B.setChecked(false);
            }
            if(SHIFT_C.equals("T")){
                cbSHIFT_C.setChecked(true);
            }else{
                cbSHIFT_C.setChecked(false);
            }

            GetLineByLineGroup(LINE_GROUP_PK);
            SetSelectByLine_GroupLine(LINE_GROUP_PK, LINE_PK);

            //Even Choose Group
            spLineGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                        Object pkGroup = hashMapGroupLinePK.get(i);
                        if (pkGroup != null) {
                            LINE_GROUP_PK = Integer.valueOf(pkGroup.toString());
                            if (LINE_GROUP_PK != 0) {
                                if (postBack) {
                                    GetLineByLineGroup(LINE_GROUP_PK);
                                }
                                postBack = true;
                                TextView tv = (TextView) selectedItemView;
                                tv.setTextColor(Color.BLACK);
                            } else {
                                ((Spinner) vContent.findViewById(R.id.spLine)).setAdapter(null);
                            }
                        } else {
                            Log.i("LINE_GROUP_PK: ", "Null");
                        }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {

                }

            });

            //Even Choose Group
            spLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                        Object pk = hashMapLinePK.get(i);
                        if (pk != null) {
                            LINE_PK = Integer.valueOf(pk.toString());
                            TextView tv = (TextView) selectedItemView;
                            if (tv != null && tv.getText().length() > 0) {
                                tv.setTextColor(Color.BLACK);
                            }
                        } else {
                            Log.i("LINE_PK: ", "Null");
                        }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {

                }

            });
        }

        CreatGridHeader();
        GetDataCavityByItem(ITEM_PK);

        if(STATUS == 3){
            vContent.findViewById(R.id.btnSaveAndClose).setVisibility(View.GONE);
        }else{
            vContent.findViewById(R.id.btnSaveAndClose).setVisibility(View.VISIBLE);
        }

        //region Set Screen Size
        int Measuredheight = 0, Measuredwidth = 0;
        Point size = new Point();
    /*    WindowManager w = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
            w.getDefaultDisplay().getSize(size);
            Measuredheight = size.y;
            Measuredwidth = size.x;
            final LinearLayout root = (LinearLayout) vContent.findViewById(R.id.POPUP_QC_DETAIL);
            FrameLayout.LayoutParams rootParams = (FrameLayout.LayoutParams)root.getLayoutParams();
            rootParams.height = Measuredheight*75/100;
            rootParams.width =Measuredwidth*95/100;
            System.out.println("H: " + rootParams.height + "   W: " + rootParams.width);

        }
*/

        //endregion

        checkStatusSlipNo(this.STATUS);
        //endregion
        return  vContent;
    }

    private void checkStatusSlipNo(Integer status){
        if(status == STATUS_APPROVE){
            spLineGroup.setEnabled(false);
            spLine.setEnabled(false);
            etLotNo.setEnabled(false);
        }
    }

    private void SetSelectByLine_GroupLine(int GroupLinePk, int LinePk){

        if(GroupLinePk != 0){
            Iterator<Map.Entry<String,String>> itr = hashMapGroupLinePK.entrySet().iterator();
            int i = 0;
            while (itr.hasNext()) {

                Map.Entry<String,String> entry = itr.next();
                String value= entry.getValue();

                if(Integer.valueOf(value) == GroupLinePk){
                    spLineGroup.setSelection(i);
                    break;
                }
                i++;
            }
        }

        if(LinePk != 0){
            Iterator<Map.Entry<String,String>> itr = hashMapLinePK.entrySet().iterator();
            int i = 0;
            while (itr.hasNext()) {

                Map.Entry<String,String> entry = itr.next();
                String value= entry.getValue();

                if(Integer.valueOf(value) == LinePk){
                    ((Spinner)vContent.findViewById(R.id.spLine)).setSelection(i);
                    break;
                }
                i++;
            }
        }
    }

    private void GetDataCavityByItem(String itemCode){
        String l_para = "1,drivtldrlgtl0001_s_12," + itemCode;

        Log.i("GetDataCavityByItem: ", "drivtldrlgtl0001_s_12: " + itemCode);
        try{
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String resultSearch[][] = networkThread.execute(l_para).get();

            TableLayout scrollablePart = (TableLayout) vContent.findViewById(R.id.grdData);
            scrollablePart.removeAllViews();

            if(resultSearch!= null && resultSearch.length > 0){
                ShowResultSearch(resultSearch, scrollablePart);
            }
        }catch(Exception ex){
            Log.e("Searh Error: ", ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    //endregion

    private void CreatGridHeader(){
        //Init varible
        TableRow.LayoutParams trParam = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        TableRow tRow = new TableRow(gwMActivity);
        TableLayout tLayout;
        int height = 30;
        //Design Grid
        tLayout = (TableLayout)vContent.findViewById(R.id.grdHeader);
        tRow = new TableRow(gwMActivity);
        tRow.setLayoutParams(trParam);
        tRow.setGravity(Gravity.CENTER);
        tRow.setBackgroundColor(bsColors[1]);
        tRow.setVerticalGravity(50);

        //Add Column
        tRow.addView(makeTableColHeaderWithText("PK", 0, fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[0], fixedRowHeight));
        /*tRow.addView(makeTableColHeaderWithText("Mold Name", scrollableColumnWidths[4], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Cavity Code", scrollableColumnWidths[4], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Cavity Name", scrollableColumnWidths[4], fixedRowHeight));*/
        tRow.addView(makeTableColHeaderWithText("Mold ID", scrollableColumnWidths[4], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Mold Name", scrollableColumnWidths[4], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Cavity Code", scrollableColumnWidths[4], fixedRowHeight));
        tLayout.addView(tRow);
    }

    public void onChoose(View view){
        Intent intent = gwMActivity.getIntent();
        Bundle bundle = new Bundle();
        intent.putExtra("ResultSlipNo", bundle);

        gwMActivity.setResult(2, intent);
        dismiss();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("Call Function: ", "onActivityResult(int requestCode, int resultCode, Intent data)");
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTION_POPUP_INSPECTOR
                && resultCode == Activity.RESULT_OK) {

            Bundle bundle = data.getBundleExtra("RESULT_INSPECTOR");
            if(bundle!=null){
                INSPECTOR_PK = bundle.getInt("INSPECTOR_PK");
                INSPECTOR_NAME = bundle.getString("INSPECTOR_NAME");

                ((EditText)vContent.findViewById(R.id.etInspector)).setText(INSPECTOR_NAME);
            }
        }else if (requestCode == ACTION_POPUP_CHECKED
                && resultCode == Activity.RESULT_OK) {

            Bundle bundle = data.getBundleExtra("RESULT_CHECKED");
            if(bundle!=null){
                CHECKED_PK = bundle.getInt("CHECKED_PK");
                CHECKED_NAME = bundle.getString("CHECKED_NAME");

                ((EditText)vContent.findViewById(R.id.etChecked)).setText(CHECKED_NAME);
            }
        }else if (requestCode == ACTION_POPUP_APPROVE
                && resultCode == Activity.RESULT_OK) {

            Bundle bundle = data.getBundleExtra("RESULT_APPROVE");
            if(bundle!=null){
                APPROVE_PK = bundle.getInt("APPROVE_PK");
                APPROVE_NAME = bundle.getString("APPROVE_NAME");

                ((EditText)vContent.findViewById(R.id.etApprove)).setText(APPROVE_NAME);
            }
        }
    }

    public void onInspector(View view){

        if(STATUS!= STATUS_APPROVE) {
            Intent openNewActivity = new Intent(view.getContext(), Popup_QC_Management_Inspector.class);
            startActivityForResult(openNewActivity, ACTION_POPUP_INSPECTOR);
        }
    }

    public void onChecked(View view){
        if(STATUS!= STATUS_APPROVE) {
            Intent openNewActivity = new Intent(view.getContext(), Popup_QC_Management_Checker.class);
            startActivityForResult(openNewActivity, ACTION_POPUP_CHECKED);
        }
    }

    public void onApprove(View view){
        if(STATUS!= STATUS_APPROVE){
            Intent openNewActivity = new Intent(view.getContext(), Popup_QC_Management_Approve.class);
            startActivityForResult(openNewActivity, ACTION_POPUP_APPROVE);
        }
    }

    public void ShowLineGroup(){
        String l_para = "1,DRIVTLDRLGTL0001_S_09," + "-";

        try{
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String dataLineGroup[][] = networkThread.execute(l_para).get();
            lstGroupName = new ArrayList<String>();

            for (int i = 0; i < dataLineGroup.length; i++) {
                    //name
                    lstGroupName.add(dataLineGroup[i][1].toString());
                    //pk
                    hashMapGroupLinePK.put(i, dataLineGroup[i][0]);
            }

            if(dataLineGroup!= null && dataLineGroup.length > 0){
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spLineGroup.setAdapter(dataAdapter);

            }
        }catch(Exception ex){
            Log.e("Searh Error: ", ex.getMessage());
        }

    }

    public void GetLineByLineGroup(int PK_LineGroup){
        String l_para = "1,DRIVTLDRLGTL0001_S_08," + PK_LineGroup;

        try{
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String dataLineByGroup[][] = networkThread.execute(l_para).get();
            lstLineName = new ArrayList<String>();

            for (int i = 0; i < dataLineByGroup.length; i++) {
                lstLineName.add(dataLineByGroup[i][1].toString());
                hashMapLinePK.put(i, dataLineByGroup[i][0]);
            }

            if(dataLineByGroup!= null && dataLineByGroup.length > 0){
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstLineName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spLine.setAdapter(dataAdapter);

            }

        }catch(Exception ex){
            Log.e("Searh Error: ", ex.getMessage());
        }

    }

    public void ShowResultSearch(String result[][], final TableLayout scrollablePart){
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);


        TableRow row = null;
        //remove all view child
        int count = result.length;
        for(int i = 0;i < result.length; i++){
            row = new TableRow(gwMActivity);
            row.setLayoutParams(wrapWrapTableRowParams);
            row.setGravity(Gravity.CENTER);
            row.setBackgroundColor(Color.LTGRAY);
            row.addView(makeTableColWithText(result[i][0], 0, fixedRowHeight, -1));
            row.addView(makeTableColWithText(String.valueOf(i+1), scrollableColumnWidths[0], fixedRowHeight, 0));
            row.addView(makeTableColWithText(result[i][1], scrollableColumnWidths[4], fixedRowHeight, -1));
            row.addView(makeTableColWithText(result[i][2], scrollableColumnWidths[4], fixedRowHeight, -1));
            row.addView(makeTableColWithText(result[i][3], scrollableColumnWidths[4], fixedRowHeight, -1));
            row.setOnClickListener(new View.OnClickListener() {
                TextView tvCavityPK;
                TextView tvCavityCode, tvMoldName, tvMoldCode;

                @Override
                public void onClick(View v) {
                    if(STATUS!= STATUS_APPROVE) {
                        TableRow tr1 = (TableRow) v;
                        tvCavityPK = (TextView) tr1.getChildAt(0); //cavity code
                        tvMoldCode = (TextView) tr1.getChildAt(2);
                        tvMoldName = (TextView) tr1.getChildAt(3);
                        tvCavityCode = (TextView) tr1.getChildAt(4);

                        String strCavatyPK = tvCavityCode.getText().toString();
                        String  strMoldCode=tvMoldCode.getText().toString();
                         if(MOLD_CODE.equals(strMoldCode) || MOLD_CODE.equals("") || MOLD_CODE=="") {
                             if (!hsCavity.containsKey(strCavatyPK)) {
                                 MOLD_CODE=strMoldCode;
                                 hsCavity.put(strCavatyPK, "CAV# " + tvCavityCode.getText().toString());
                                 for (int i = 0; i < tr1.getChildCount(); i++) {
                                     tvCavityPK = (TextView) tr1.getChildAt(i);
                                     tvCavityPK.setTextColor(orangeColor);
                                 }
                             } else {
                                if(hsCavity.size()==1){
                                    MOLD_CODE="";
                                }
                                 hsCavity.remove(strCavatyPK);

                                 for (int i = 0; i < tr1.getChildCount(); i++) {
                                     tvCavityPK = (TextView) tr1.getChildAt(i);
                                     tvCavityPK.setTextColor(Color.BLACK);
                                 }
                             }
                         }
                        else{
                             gwMActivity.alertToastShort("Can not select Mold ID different");
                         }
                    }
            }
            });
            scrollablePart.addView(row);
        }
    }

    public void onClose(View view){
       dismiss();
 //       overridePendingTransition(R.anim.abc_slide_out_bottom, R.anim.abc_slide_out_top);
    }


    public boolean Validate(){
        if(!cbSHIFT_A.isChecked() && !cbSHIFT_B.isChecked() && !cbSHIFT_C.isChecked()){
            gwMActivity.alertToastShort("Please Choose A Shift To QC!!!");
            return false;
        }/*else if(hsCavity.size() == 0){
            alertToastShort("Please Choose Cavity To QC!!!");
            return false;
        }*/
        return true;
    }

    public void onSaveAndClose(View view){

        if(Validate()){
            Intent intent = gwMActivity.getIntent();
            Bundle bundle = new Bundle();

            bundle.putInt("INSPECTOR_PK", INSPECTOR_PK);
            bundle.putString("INSPECTOR_NAME", INSPECTOR_NAME);

            bundle.putInt("CHECKED_PK", CHECKED_PK);
            bundle.putString("CHECKED_NAME", CHECKED_NAME);

            bundle.putInt("APPROVE_PK", APPROVE_PK);
            bundle.putString("APPROVE_NAME", APPROVE_NAME);

            bundle.putInt("LINE_GROUP_PK", LINE_GROUP_PK);
            bundle.putInt("LINE_PK", LINE_PK);

            bundle.putString("LOT_NO", ((EditText)vContent.findViewById(R.id.etLotNo)).getText().toString());

            SHIFT_A = ((CheckBox)vContent.findViewById(R.id.cbA)).isChecked()?"T":"F";
            SHIFT_B = ((CheckBox)vContent.findViewById(R.id.cbB)).isChecked()?"T":"F";
            SHIFT_C = ((CheckBox)vContent.findViewById(R.id.cbC)).isChecked()?"T":"F";
            bundle.putString("SHIFT_A", SHIFT_A);
            bundle.putString("SHIFT_B", SHIFT_B);
            bundle.putString("SHIFT_C", SHIFT_C);

            bundle.putString("LOT_QTY", etLotQty.getText().toString());
            bundle.putString("INCOME_DT", incomeDT_db);

            List<String> listPKCavity=  new ArrayList(hsCavity.keySet());;
            List<String> listNameCavity = new ArrayList(hsCavity.values());;


            bundle.putStringArrayList("CAVITY_LIST_PK", (ArrayList<String>) listPKCavity);
            bundle.putStringArrayList("CAVITY_LIST_NAME", (ArrayList<String>) listNameCavity);

            intent.putExtra("RESULT_DETAIL", bundle);
            gwMActivity.setResult(Activity.RESULT_OK, intent);
            dismiss();
            // overridePendingTransition(R.anim.abc_slide_out_bottom, R.anim.abc_slide_out_top);
        }
    }    

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnIncomeDate:
                gwMActivity.showDialog(DATE_PICKER_INCOME_ID);
                break;


        }

        //endregion
    }

    private DatePickerDialog.OnDateSetListener pickerFrom = new DatePickerDialog.OnDateSetListener() {
        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;
            // Show selected date
            incomeDT_db =String.valueOf(year)+String.valueOf(CNumber.isPushNumber(month+1))+String.valueOf(CNumber.isPushNumber(day));
            etIncomeDt.setText(new StringBuilder()
                    //.append(year).append("-").append(isPushNumber(month + 1)).append("-").append(isPushNumber(day)).append(""));
                    .append(CNumber.isPushNumber(day)).append("-").append(CNumber.isPushNumber(month + 1)).append("-").append(year).append(" "));
        }
    };

}
