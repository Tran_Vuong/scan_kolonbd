package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import gw.genumobile.com.R;
import gw.genumobile.com.interfaces.GridListView;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.frGridListUpload;
import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.AppConfig;

public class MappingV3 extends gwFragment implements View.OnClickListener {
    protected Boolean flagUpload = true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;
    private SharedPreferences mappingPrefs;

    Queue queue = new LinkedList();
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    String data[] = new String[0], process_type="01";
    String sql = "", scan_date = "", scan_time = "", unique_id = "", parent_item_pk = "", parent_item_name = "", str_valid_same = "",grade_item="";
    EditText bc;
    EditText currentBC, _edItemCode, _edQty, _edLotNo, edt_item_code,edt_item_name,edt_qty,edt_weight,edt_remark,edt_dofing_no,edt_gross_weight;
    CheckBox _ckbLotNo, _ckbItemCode;
    TextView recyclableTextView, _txtError, _txtTotalGridBot, _txtTotalQtyBot, _txtTotalGridMid,txtprocess_type,txtSent,txtRemain,txtPallet;
    public Spinner spn_sewing;
    int pkParent = 0;
    int indexDialog = 0;
    HashMap hashMapItem = new HashMap();

    private Handler customHandler = new Handler();

    CharSequence seqColor[] = new CharSequence[]{"red", "green", "blue", "black", "red", "green", "blue", "black", "red", "green", "blue", "black", "red", "green", "blue", "black", "red", "green", "blue", "black", "red", "green", "blue", "black"};
    Button btnViewStt, btnList,  btnDelAll, btnDelBC, btnscan_log, btnListBot,btn_new,btn_save,btn_view,btn_bclist,btn_Upload,btnMapping;
    View rootView;
    HashMap hashMapGrade= new HashMap();
    String[][] ls_grade = new String[][]{
            {"A", "A"},
            {"B", "B"},
            {"C", "C"}};
    String[] lableinfo = new  String[0];
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_mapping_off, container, false);
        gwMActivity.getWindow().setSoftInputMode(1);
        //lock screen
        //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mappingPrefs = gwActivity.getSharedPreferences("MappingConfig", gwActivity.MODE_PRIVATE);
        //
        InitActivity();
        OnShowGridHeader();
        OnShowChild();
        init_color();
        LoadGrade(ls_grade);
        bc.setFocusable(true);
        bc.requestFocus();
        bc.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        _txtError = (TextView) rootView.findViewById(R.id.txtError);
                        _txtError.setTextColor(Color.RED);
                        _txtError.setTextSize((float) 16.0);
                        if (bc.getText().toString().equals("")) {
                            _txtError.setText("Pls Scan barcode!");
                            bc.requestFocus();
                        } else {
                            OnSaveData();
                        }
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // gwMActivity is for backspace
                    bc.clearFocus();
                    Thread.interrupted();
                    //finish();
                    //System.exit(0);
                    //Log.e("IME_TEST", "BACK VIRTUAL");

                }
                return false;
            }
        });
        bc.requestFocus();
        return rootView;
    }

    protected void InitActivity() {

        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnscan_log = (Button) rootView.findViewById(R.id.btnscan_log);
        btnscan_log.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnMapping.setOnClickListener(this);

        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);
        btn_Upload = (Button) rootView.findViewById(R.id.btn_Upload);
        btn_Upload.setOnClickListener(this);
        btn_new=(Button) rootView.findViewById(R.id.btn_new);
        btn_new.setOnClickListener(this);
        btn_save=(Button) rootView.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);
        btn_view=(Button) rootView.findViewById(R.id.btn_view);
        btn_view.setOnClickListener(this);
        btn_bclist=(Button) rootView.findViewById(R.id.btn_bclist);
        btn_bclist.setOnClickListener(this);
        edt_item_code = (EditText) rootView.findViewById(R.id.edt_item_code);
        edt_item_name = (EditText) rootView.findViewById(R.id.edt_item_name);
        edt_qty = (EditText) rootView.findViewById(R.id.edt_qty);
        edt_weight = (EditText) rootView.findViewById(R.id.edt_weight);
        edt_gross_weight = (EditText) rootView.findViewById(R.id.edt_gross_weight);
        edt_remark = (EditText) rootView.findViewById(R.id.edt_remark);
        txtPallet = (TextView) rootView.findViewById(R.id.txtPallet);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");
        currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
        _edLotNo = (EditText) rootView.findViewById(R.id.edt_lot_no);
        edt_dofing_no = (EditText) rootView.findViewById(R.id.edt_dofing_no);
        txtprocess_type = (TextView) rootView.findViewById(R.id.txtprocess_type);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setTextColor(Color.RED);
        _txtError.setTextSize((float) 16.0);
        _ckbLotNo = (CheckBox) rootView.findViewById(R.id.ckbLotNo);
        _ckbItemCode = (CheckBox) rootView.findViewById(R.id.ckbItemCode);
        spn_sewing = (Spinner) rootView.findViewById(R.id.spn_sewing);
        txtSent =(TextView) rootView.findViewById(R.id.txtSent);
        txtRemain  =(TextView) rootView.findViewById(R.id.txtRemain);
        if (!mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {
            _ckbItemCode.setChecked(false);
            _ckbLotNo.setChecked(false);
        }
        bc = (EditText) rootView.findViewById(R.id.ed_BC);
    }

    private void init_color() {
        //region ------Set color tablet----
//        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
//        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
//        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
//        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdParent);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdChild);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private void LoadGrade(String[][] _data) {
        try {
            String lg_code="LGPC0071";
            String l_para = "1,LG_MPOS_GET_LG_CODE," + lg_code;

                ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                 String data[][] = networkThread.execute(l_para).get();
                List<String> lstGroupName = new ArrayList<String>();
          //    List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < data.length; i++) {
                lstGroupName.add(data[i][1]);
                hashMapGrade.put(i, data[i][0]);
            }

            if (data != null && data.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spn_sewing.setAdapter(dataAdapter);
                //  wh_name = myLstWH.getItemAtPosition(0).toString();
            } else {
                spn_sewing.setAdapter(null);
                grade_item = "0";
            }
        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
        //-----------

        // onchange spinner wh
        spn_sewing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                // wh_name = myLstWH.getItemAtPosition(i).toString();
                // Log.e("WH_name",wh_name);
                Object pkGroup = hashMapGrade.get(i);
                if (pkGroup != null) {
                    grade_item = String.valueOf(pkGroup);
                    if (!grade_item.equals("0")) {

                        //dosomehitng

                    }
                } else {
                    grade_item = "0";
                    Log.e("grade_item: ", grade_item);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    //****************************************************************************
    public void OnSaveData() {
        bc = (EditText) rootView.findViewById(R.id.ed_BC);
        currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setTextColor(Color.RED);
        _txtError.setTextSize((float) 16.0);
        _ckbLotNo = (CheckBox) rootView.findViewById(R.id.ckbLotNo);
        _ckbItemCode = (CheckBox) rootView.findViewById(R.id.ckbItemCode);
        if (bc.getText().equals("")) {
            _txtError.setText("Pls Scan barcode!");
            return;
            //bc1.requestFocus();
        }

        String str_scan = bc.getText().toString().toUpperCase();
        int l_lenght = str_scan.length();


        //region  Parent YN = Y
        if (mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {

        }
        //endregion
        else {
            try {
                if (str_scan.equals("")) {
                    _txtError.setText("Vui lòng scan barcode!");
                    return;
                }
                scan_date = CDate.getDateyyyyMMdd();
                scan_time = CDate.getDateYMDHHmmss();
               String P_ITEM_BC =str_scan;
              //  currentBC.setText(P_ITEM_BC);
                String P_ITEM_CODE = str_scan.substring(0,8);
                String P_TR_LOT_NO=CDate.getDateyyMMdd();
                if(str_scan.substring(0,3)=="2YV")
                    process_type = "01";
                else  process_type = "02";

                int cnt = Check_Exist_PK(str_scan);// check barcode exist in data
                        if (cnt > 0)// exist data
                        {    alertRingMedia();
                            _txtError.setText("Barcode này đã scan!");
                            bc.getText().clear();
                            bc.requestFocus();
                            return;
                        } else {

                            String _bc_Type = "C";//child
                                String lotNoParent = _edLotNo.getText().toString();

                                    sql = "INSERT INTO INV_TR(ITEM_BC,PARENT_PK,TR_PARENT_ITEM_PK,ITEM_CODE,ITEM_NAME,TR_LOT_NO,TR_QTY,SCAN_DATE,SCAN_TIME,SENT_YN,BC_TYPE,MAPPING_YN,SLIP_NO,PROCESS_TYPE,TR_TYPE)"
                                            + "VALUES('"
                                            + P_ITEM_BC + "',"
                                            + pkParent + ",'"
                                            + parent_item_pk + "','"
                                            + P_ITEM_CODE + "','"
                                            + P_ITEM_CODE + "','"
                                            + P_TR_LOT_NO + "','"
                                            + 1 + "','"
                                            + scan_date + "','"
                                            + scan_time + "','"
                                            + "N" + "','"
                                            + _bc_Type + "','"
                                            + "N" + "','"
                                            + "-" + "','"
                                            + process_type + "','"
                                            + formID + "')";

                                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                                    db.execSQL(sql);
                                    db.close();

                            _txtError.setText("Save success.");
                            bc.getText().clear();
                            bc.requestFocus();
                            OnShowChild();
                        }

            } catch (Exception ex) {
                Log.e("Save error:", ex.toString());

            }
        }
    }
    public void onDelAll(View view) {

        String title = "Confirm Delete...";
        String mess = "Are you sure you want to delete all";
        alertDialogYN(title, mess, "onDelAll");
    }

    public void ProcessDeleteAll() {
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String para = "";
            boolean flag = true;
            db.execSQL("DELETE FROM INV_TR where TR_TYPE ='" + formID + "' AND (EXECKEY is null or EXECKEY=' ' )");


        } catch (Exception ex) {
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
            currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
            currentBC.setText("");
            pkParent = 0;
            OnShowChild();
            gwMActivity.alertToastShort("Delete success..");
        }
    }


    public void onSave(View view) {

        if (!checkRecordGridView(R.id.grdChild)) return;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Mapping...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to Save Mapping");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                //Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
                //btnMapping.setVisibility(View.GONE);
                ProcessIssueOff();
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public  void  OnNew(View view){
        edt_item_code.getText().clear();
        edt_item_name.getText().clear();
        edt_qty.getText().clear();
        edt_remark.getText().clear();
        edt_weight.getText().clear();
        edt_gross_weight.getText().clear();
        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
        scrollablePart.removeAllViews();
        bc.requestFocus();
    }
    public  void  OnMapping(View view){
        if (mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {

        }
        //endregion
        else {
            try {

                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("SELECT ITEM_CODE,ITEM_NAME,TR_ITEM_PK,UOM,TR_LOT_NO,TR_QTY,ITEM_BC,EXECKEY" +    //,PARENT_PK,TR_PARENT_ITEM_PK
                        "    FROM INV_TR " +
                        "    WHERE DEL_IF =0 AND BC_TYPE='C' AND TR_TYPE='" + formID + "' AND SENT_YN='N' AND MAPPING_YN='N' AND PARENT_PK=0 AND   (EXECKEY is null or EXECKEY=' ' )" +
                        "   ORDER BY PK ASC ", null);
                int count = cursor.getCount();
                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                String qty,item_code, item_name, p_lot_no;
                List<String> ls_lotno = new ArrayList<String>();

                // cursor.moveToFirst();
                Integer _qty=0;
                if (cursor != null && cursor.moveToFirst()) {

                    edt_item_code.setText(cursor.getString(0));
                    edt_item_name.setText(cursor.getString(1));

                    p_lot_no =cursor.getString(4);
                    ls_lotno.add(p_lot_no);
                    for (int i = 0; i < count; i++) {
                        _qty = _qty + Integer.parseInt(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                       if (!ls_lotno.contains(cursor.getString(cursor.getColumnIndex("TR_LOT_NO"))))
                           ls_lotno.add(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")));
                        cursor.moveToNext();
                    }
                }
                edt_qty.setText(String.valueOf(_qty));

                if(process_type=="01") {
                    edt_dofing_no.setEnabled(false);
                    txtprocess_type.setText("YARN:");
                    _edLotNo.setText(ls_lotno.get(0));

                }
                else {
                    edt_dofing_no.setEnabled(true);
                    txtprocess_type.setText("R/C:");
                    StringBuilder sb = new StringBuilder();
                    for (String s : ls_lotno)
                    {
                        sb.append(s);
                        sb.append("-");
                    }
                    sb.setLength(sb.length() - 1);
                    _edLotNo.setText( sb.toString());
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("Upload Mapping :" + ex.getMessage());
                Log.e(" Error Upload Mapping :", ex.getMessage().toString());
            } finally {
                cursor.close();
                db.close();
            }
        }
    }
    public void ProcessIssueOff() {
        //region PARENT YN=Y
        if (mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {

        }
        //endregion
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int _parent_pk=0;
                String _bc_Type="P";
                String P_ITEM_CODE=edt_item_code.getText().toString();
                String  P_TR_LOT_NO =_edLotNo.getText().toString();
                String _grade_i= grade_item;
                String _weight= edt_weight.getText().toString();
                String _grossweight= edt_gross_weight.getText().toString();
                String _remark=edt_remark.getText().toString();
                String _doff= edt_dofing_no.getText().toString();
                String unique_id = CDate.getDateyyMMddhhmmsss();
                String p_qty= edt_qty.getText().toString();
                txtPallet.setText(unique_id);
               /* sql = "INSERT INTO INV_TR(ITEM_BC,PARENT_PK,TR_PARENT_ITEM_PK,ITEM_CODE,ITEM_NAME,TR_LOT_NO,TR_QTY,SCAN_DATE,SCAN_TIME,SENT_YN,BC_TYPE,MAPPING_YN,SLIP_NO,PROCESS_TYPE,TR_TYPE,REMARKS,GRADE,WEIGHT,DOFING_NO)"
                        + "VALUES('"
                        + P_ITEM_BC + "',"
                        + pkParent + ",'"
                        + parent_item_pk + "',"
                        + P_ITEM_CODE + "','"
                        + P_ITEM_CODE + "','"
                        + P_TR_LOT_NO + "','"
                        + 1 + "','"
                        + scan_date + "','"
                        + scan_time + "','"
                        + "N" + "','"
                        + _bc_Type + "','"
                        + "N" + "','"
                        + "-" + "','"
                        + formID + "','"
                        + _remark + "','"
                        + _grade_i + "','"
                        + _weight + "','"
                        + _doff + "')";

                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                db.execSQL(sql);
                cursor = db.rawQuery("select last_insert_rowid()",null);

                if (cursor.moveToFirst())
                    _parent_pk = cursor.getInt(0);
*/
                String sql = "UPDATE INV_TR set EXECKEY='" + unique_id + "',GRADE='" + _grade_i + "',WEIGHT='" + _weight+ "',GROSS_WEIGHT='" + _grossweight+ "',DOFING_NO='" + _doff+ "',TO_LOTNO='" + P_TR_LOT_NO+ "',PARENT_QTY='" + p_qty+"',REMARKS='" + _remark+"'" +
                        "  where PARENT_PK = " + 0 + " and" +
                        "        ITEM_CODE = '" + P_ITEM_CODE + "' and" +
                        "        TR_TYPE = '" + formID + "' and" +
                        "      (EXECKEY is null or EXECKEY=' ' )";
                db.execSQL(sql);


                db.close();

            } catch (Exception ex) {
                gwMActivity.alertToastLong("Upload Mapping :" + ex.getMessage());
                Log.e(" Error Upload Mapping :", ex.getMessage().toString());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity, this);
                task.execute(data);
                str_valid_same="";
            } else {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        }

    }
    public void CountSendRecord() {
        flagUpload = true;
        // total scan log
        txtSent = (TextView) rootView.findViewById(R.id.txtSent);
        _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid = Integer.parseInt(_txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        int countBot = Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        txtSent.setText("Send: " + (countMid + countBot));

        txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(txtRemain.getText().toString().replaceAll("[\\D]", ""));


    }
 private void doStart() {
        flagUpload = false;
        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
     cursor = db.rawQuery("SELECT ITEM_CODE,ITEM_BC,GRADE,WEIGHT,GROSS_WEIGHT,DOFING_NO,TO_LOTNO,PARENT_QTY,REMARKS,EXECKEY" +    //,PARENT_PK,TR_PARENT_ITEM_PK
             "    FROM INV_TR " +
             "    WHERE DEL_IF =0 AND BC_TYPE='C' AND TR_TYPE='" + formID + "' AND SENT_YN='N' AND SLIP_NO='-' AND MAPPING_YN='N'" +
             "   ORDER BY PK ASC ", null);
        if (cursor.moveToFirst()) {

            // Write your code here to invoke YES event
            System.out.print("\n\n**********doStart********\n\n\n");

            //set value message
            _txtError = (TextView) rootView.findViewById(R.id.txtError);
            _txtError.setTextColor(Color.RED);
            _txtError.setText("");

            //  data=new String [cursor2.getCount()];
            data = new String[1];
            int j = 0;
            String para = "";
            boolean flag = false;
            do {
                flag = true;
                for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                    if (para.length() <= 0) {
                        if (cursor.getString(k) != null)
                            para += cursor.getString(k) + "|";
                        else
                            para += "0|";
                    } else {
                        if (flag == true) {
                            if (cursor.getString(k) != null) {
                                para += cursor.getString(k);
                                flag = false;
                            } else {
                                para += "|";
                                flag = false;
                            }
                        } else {
                            if (cursor.getString(k) != null)
                                para += "|" + cursor.getString(k);
                            else
                                para += "|";
                        }
                    }
                }
                String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                if (execkey == null || execkey.equals("")) {
                    int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                    String ex_item_bc = cursor.getString(cursor.getColumnIndex("ITEM_BC"));
                    String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                    hp.updateExeckeyMapping(formID, ex_item_pk, ex_item_bc, ex_lot_no, unique_id);

                    para += "|" + unique_id;
                } else {
                    para += "|" + execkey;
                }

                para += "|" + bsUserID;
                para += "|" + bsUserPK;
                para += "|" + deviceID;
                para += "*|*";///multi row

            } while (cursor.moveToNext());
            para += "|!" + "LG_MPOS_PRO_MAPP_UPLOAD_3";
            data[j++] = para;
            System.out.print("\n\n\n para upload: " + para);
            Log.e("para upload: ", para);

            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                task.execute(data);
                gwMActivity.alertToastShort(getString(R.string.send_server));
            } else {
                flagUpload = true;
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }
        } else {
            flagUpload = true;
        }
        cursor.close();
        db.close();
    }
    public void UploadAndIssue() {
        unique_id = CDate.getDateyyMMddhhmmsss();
        //region PARENT YN=Y
        if (mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {

        }
        //endregion
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("SELECT ITEM_CODE,ITEM_BC,GRADE,WEIGHT,GROSS_WEIGHT,DOFING_NO,TO_LOTNO,PARENT_QTY,REMARKS,EXECKEY" +    //,PARENT_PK,TR_PARENT_ITEM_PK
                        "    FROM INV_TR " +
                        "    WHERE DEL_IF =0 AND BC_TYPE='C' AND TR_TYPE='" + formID + "' AND SENT_YN='N' AND SLIP_NO='-' AND MAPPING_YN='N'" +
                        "   ORDER BY PK ASC ", null);
                int count = cursor.getCount();
                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_item_bc = cursor.getString(cursor.getColumnIndex("ITEM_BC"));
                            String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            hp.updateExeckeyMapping(formID, ex_item_pk, ex_item_bc, ex_lot_no, unique_id);

                            para += "|" + unique_id;
                        } else {
                            para += "|" + execkey;
                        }

                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";///multi row

                    } while (cursor.moveToNext());
                    para += "|!" + "LG_MPOS_PRO_MAPP_MSLIP_3";
                    data[j++] = para;

                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("Upload Mapping :" + ex.getMessage());
                Log.e(" Error Upload Mapping :", ex.getMessage().toString());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
             //   MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity, this);
              ///  task.execute(data);
                str_valid_same="";
            } else {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        }

    }

    // show data scan in
    public  void  OnViewBC(View view){
        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = new MappingV2_Pop();
        Bundle args = new Bundle();
        String[] arr = {
                txtPallet.getText().toString(),
                edt_qty.getText().toString(),
                edt_item_code.getText().toString(),
                grade_item,
                edt_weight.getText().toString(),
                edt_gross_weight.getText().toString(),
               CDate.getDateFormatyyMMdd(),
               CDate.getDateFormatHHmm(),
                bsUserID
        };
            args.putString("type", "offline");
            args.putStringArray("para1", arr);
            //  args.putStringArray("LINE_DETAIL", line_detail);

            dialogFragment.setArguments(args);
            dialogFragment.setTargetFragment(this, 1);
            dialogFragment.setCancelable(false);
            dialogFragment.show(fm.beginTransaction(), "dialog");

    }
    public  void  OnListBC(View view){
        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = new MappingV2_Pop2();
        Bundle args = new Bundle();
     //   args.putStringArray("para1",lableinfo);
        //  args.putStringArray("LINE_DETAIL", line_detail);

        dialogFragment.setArguments(args);
        dialogFragment.setTargetFragment(this, 1);
        dialogFragment.setCancelable(false);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    public void OnShowAllLog() {
        Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnMapping.setVisibility(View.VISIBLE);
    try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select PK,ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,MAPPING_YN,TR_LOT_NO,SLIP_NO,EXECKEY " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and BC_TYPE='C' AND (SLIP_NO='-' OR SLIP_NO='') AND MAPPING_YN='N' AND TR_TYPE='" + formID + "' " +
                    " ORDER BY pk desc";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), 0, fixedRowHeight, 1));
                    if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y"))
                        row.addView(makeTableColWithText("YES", scrollableColumnWidths[2], fixedRowHeight, 0));
                    else
                        row.addView(makeTableColWithText("-", scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("EXECKEY")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, -1));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(7); //SLIP_NO
                            String st_slipNO = tvSlipNo.getText().toString();

                            if (st_slipNO.equals("-")) {

                                TextView tv1 = (TextView) tr1.getChildAt(8); //PK
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255, 99, 71));
                                }

                                if (queue.size() > 0) {
                                    if (queue.contains(tv1.getText().toString())) {
                                        queue.remove(tv1.getText().toString());

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK);
                                        }
                                        duplicate = true;
                                    }
                                }
                                if (!duplicate)
                                    queue.add(tv1.getText().toString());
                            }
                            gwMActivity.alertToastShort(String.valueOf(queue.size()));

                        }
                    });
                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND BC_TYPE = 'C' AND TR_TYPE='" + formID + "'  ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowChild() {
        Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnMapping.setVisibility(View.VISIBLE);
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select PK,ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,MAPPING_YN,TR_LOT_NO,SLIP_NO,EXECKEY " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and BC_TYPE='C' AND (EXECKEY is null or EXECKEY=' ' ) AND TR_TYPE='" + formID + "' " +
                    " ORDER BY pk desc";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(bsColors[1]);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), 0, fixedRowHeight, 1));
                    if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y"))
                        row.addView(makeTableColWithText("YES", scrollableColumnWidths[2], fixedRowHeight, 0));
                    else
                        row.addView(makeTableColWithText("-", scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));

                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("EXECKEY")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, -1));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(7); //SLIP_NO
                            String st_slipNO = tvSlipNo.getText().toString();

                            if (st_slipNO.equals("-")) {

                                TextView tv1 = (TextView) tr1.getChildAt(8); //PK
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255, 99, 71));
                                }

                                if (queue.size() > 0) {
                                    if (queue.contains(tv1.getText().toString())) {
                                        queue.remove(tv1.getText().toString());

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK);
                                        }
                                        duplicate = true;
                                    }
                                }
                                if (!duplicate)
                                    queue.add(tv1.getText().toString());
                            }
                            gwMActivity.alertToastShort(String.valueOf(queue.size()));

                        }
                    });
                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND BC_TYPE = 'C' AND (EXECKEY is null or EXECKEY=' ' ) AND TR_TYPE='" + formID + "'  ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowChildParent(int parent) {
        Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnMapping.setVisibility(View.VISIBLE);
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            // int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,MAPPING_YN,TR_LOT_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and BC_TYPE='C'and BC_TYPE='C' AND TR_TYPE='" + formID + "' and PARENT_PK=" + parent +
                    " ORDER BY pk desc  ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), 0, fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, 0));

                    if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y"))
                        row.addView(makeTableColWithText("YES", scrollableColumnWidths[2], fixedRowHeight, 0));
                    else
                        row.addView(makeTableColWithText("-", scrollableColumnWidths[2], fixedRowHeight, 0));

                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.MAGENTA);
            _txtTotalQtyBot.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }


    public void onListGridMid(View view) {

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  frGridListUpload.newInstance(1,formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    /**
     * Xử lý kết quả trả về ở đây
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiểm tra có đúng requestCode =REQUEST_CODE_INPUT hay không
        //Vì ta có thể mở Activity với những RequestCode khác nhau
        if (requestCode == REQUEST_CODE) {
            //Kiểm trả ResultCode trả về, cái này ở bên InputDataActivity truyền về
            OnShowChild();
            switch (resultCode) {
                case RESULT_CODE:

                    break;
            }
        }
    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("lOT_NO", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Mapping", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("EXC KEY", scrollableColumnWidths[2], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    // end get data return array--use array set to grid view


    /////////////////////////
    // check data inv_tr
    public int Check_Exist_PK(String l_bc_item) {
        int countRow = 0;
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String countQuery = "SELECT PK FROM INV_TR where del_if=0 and TR_TYPE='" + formID + "' and ITEM_BC ='" + l_bc_item + "' ";
            // db = gwMActivity.getReadableDatabase();
            cursor = db.rawQuery(countQuery, null);
            // Check_Exist_PK=cursor.getCount();
            if (cursor != null && cursor.moveToFirst()) {
                countRow = cursor.getCount();
            }

        } catch (Exception ex) {
            gwMActivity.alertToastLong("Check_Exist_PK :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return countRow;

    }

    ////////////////////////////////////////////////
    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event

                if (_type.equals("onDelAll")) {
                    ProcessDeleteAll();
                }
                if (_type.equals("onMakeSlip")) {


                    //ProcessMakeSlip();

                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str = str_alert;

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            // onLoadFull();
            Intent i = new Intent(gwMActivity, AppConfig.class);
            i.putExtra("type", formID);
            i.putExtra("user", bsUserID);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onLoadFull() {
        Intent openNewActivity = new Intent(gwMActivity, GridListView.class);
        //send data into Activity
        openNewActivity.putExtra("type", "10");
        startActivity(openNewActivity);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:


                break;
            case R.id.btnUploadMapping:
                OnMapping(view);

                break;
            case R.id.btnViewStt:

                break;
            case R.id.btnSelectMid:


                break;
            case R.id.btnListBot:


                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnList:


                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnscan_log:
                onListGridMid(view);
                break;
            case R.id.btn_new:
                OnNew(view);
                break;
            case R.id.btn_save:
                onSave(view);
                break;
            case R.id.btn_view:
                OnViewBC(view);
                break;
            case R.id.btn_bclist:
                OnListBC(view);
                break;
            case R.id.btn_Upload:
                OnShowAllLog();
                doStart();
                break;

            default:
                break;
        }
    }
}
