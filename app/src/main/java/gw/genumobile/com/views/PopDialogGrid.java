package gw.genumobile.com.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.Queue;

import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.views.gwcore.BaseGwActive;

public class PopDialogGrid extends gwFragmentDialog implements View.OnClickListener{
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    Queue queue = new LinkedList();
    TextView txtQty, txtTotal,txtHeader;
    String sql="", TR_ITEM_PK = "", WH_PK = "", LOT_NO = "",FORMID="",USER="",SLIP_NO;
    String  data[] = new String[0];
    View vContent;
    Button btnDelBC;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //type = getArguments().getString("type");
        //gwAc=getActivity();
        FORMID = getArguments().getString("TYPE");
        TR_ITEM_PK = getArguments().getString("ITEM_PK");
        LOT_NO =getArguments().getString("LOT_NO");
        WH_PK  =getArguments().getString("WH_PK");
        SLIP_NO= getArguments().getString("SLIP_NO");
        USER= getArguments().getString("USER");
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vContent = inflater.inflate(R.layout.activity_pop_dialog_grid, container, false);

        Toolbar toolbar = (Toolbar) vContent.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                    Intent i = new Intent().putExtra("month", "trang");
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                    dismiss();
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);


        //Toast.makeText(this,type, Toast.LENGTH_SHORT).show();
        txtHeader=(TextView) vContent.findViewById(R.id.txtHeader);
        toolbar.setTitle(getResources().getString(R.string.tv_gridList));
        btnDelBC=(Button) vContent.findViewById(R.id.btnDelBC);
        btnDelBC.setOnClickListener(this);
        //lock screen
        //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
        OnShowGridHeaderProd();
        OnShowScanAccept();
        
        return vContent;
    }
    public void onDelBC(View view){
        if(queue.size()<=0) return;
        String title="Confirm Delete..";
        String mess="Are you sure you want to delete ???";
        alertDialogYN(title,mess,"onDelAll");

    }
    public  void ProcessDelete(){

        String para="";
        boolean flag = true;
        data = new String[1];
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(queue.size()>0)
            {
                Object[] myLst = queue.toArray();
                for (int i = 0; i < myLst.length; i++) {
                    cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='"+FORMID+"' and SLIP_NO IN ('-','') and pk="+myLst[i] +" and STATUS ='000' ", null);
                    int count = cursor.getCount();
                    if (cursor.moveToFirst()) {

                        do {
                            flag = true;
                            for (int k = 0; k < cursor.getColumnCount(); k++) {
                                if (para.length() <= 0) {
                                    if (cursor.getString(k) != null)
                                        para += cursor.getString(k);
                                    else
                                        para += "|";
                                } else {
                                    if (flag == true) {
                                        if (cursor.getString(k) != null) {
                                            para += cursor.getString(k);
                                            flag = false;
                                        } else {
                                            para += "|";
                                            flag = false;
                                        }
                                    } else {
                                        if (cursor.getString(k) != null)
                                            para += "|" + cursor.getString(k);
                                        else
                                            para += "|";
                                    }
                                }
                            }
                            para += "|" + deviceID;
                            para += "|" + USER;
                            para += "|" + FORMID;
                            para += "|delete";
                            para += "*|*";
                        } while (cursor.moveToNext());


                    }
                }
                para += "|!LG_MPOS_UPD_INV_TR_DEL";
                data[0] = para;
                //System.out.print("\n\n\n para update||delete: " + para);
                Log.e("para update||delete: ", para);
            }

        }catch (Exception ex){
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        }
        finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        }
        else
        {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }

    }
    public  void onDeleteAllFinish(){
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(queue.size()>0)
            {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();
                data=new String [myLst.length];
                for (int i = 0; i < myLst.length; i++) {
                    db.execSQL("DELETE FROM INV_TR where TR_TYPE IN('"+FORMID+"') and pk="+myLst[i]);
                }
            }else{
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='"+FORMID+"';");
            }
        }catch (Exception ex){
            Log.e("Error Delete All :", ex.getMessage());
        }
        finally {
            db.close();

            OnShowScanAccept();
        }
    }

    public void OnShowGridHeaderProd()
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Accept Scan
        scrollablePart = (TableLayout) vContent.findViewById(R.id.scrollable_header);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH Name", scrollableColumnWidths[4], fixedHeaderHeight));

        scrollablePart.addView(row);
    }


    public void OnShowScanAccept()
    {

        try {
            int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) vContent.findViewById(R.id.scrollable_content);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(FORMID.equals("5")) {
                sql = " select PK, TR_ITEM_PK,ITEM_BC,  ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_OUT_NAME as TR_WH_NAME " +
                        " FROM INV_TR  " +
                        " WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE ='" + FORMID + "' AND STATUS IN('OK', '000')" +
                        "      AND TR_ITEM_PK=" + TR_ITEM_PK + " AND TR_LOT_NO ='" + LOT_NO + "' AND SLIP_NO='" + SLIP_NO + "' AND TR_WH_OUT_PK='" + WH_PK + "'";
            }
            else{
                sql = " select PK, TR_ITEM_PK,ITEM_BC,  ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_IN_NAME as TR_WH_NAME" +
                        " FROM INV_TR  " +
                        " WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND TR_TYPE ='" + FORMID + "' AND STATUS IN('OK', '000')" +
                        "      AND TR_ITEM_PK=" + TR_ITEM_PK + " AND TR_LOT_NO ='" + LOT_NO + "' AND SLIP_NO='" + SLIP_NO + "' AND TR_WH_IN_PK='" + WH_PK + "'";
            }
            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            float _qty=0;
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WAREHOUSE_PK")), 0, fixedRowHeight));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;
                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
                            String st_slipNO=tvSlipNo.getText().toString();

                            if(st_slipNO.equals("-") || st_slipNO.equals("")){
                                TextView tv1 = (TextView) tr1.getChildAt(7); //PK
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255,99,71));
                                }

                                if (queue.size() > 0) {
                                    if (queue.contains(tv1.getText().toString())) {
                                        queue.remove(tv1.getText().toString());

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK );
                                        }
                                        duplicate = true;
                                    }
                                }

                                if (!duplicate) {
                                    queue.add(tv1.getText().toString());
                                }
                                gwMActivity.alertToastShort(tv1.getText().toString());

                            }

                        }
                    });

                    _qty = _qty+ Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }


            txtTotal = (TextView) vContent.findViewById(R.id.txtTotalLabel);
            txtTotal.setText("Total: " + count + " ");
            txtTotal.setTextColor(Color.BLUE);
            txtTotal.setTextSize((float) 18.0);
            //---------------------------------------------------------------------

            txtQty = (TextView) vContent.findViewById(R.id.txtTotalQty);
            txtQty.setText("Qty: " + String.format("%.02f", _qty)+ " ");
            txtQty.setTextColor(Color.MAGENTA);
            txtQty.setTextSize((float) 18.0);

        } catch (Exception ex) {
            gwMActivity.alertToastLong("Error Grid Accept: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }



    public void alertDialogYN(String title,String mess,final String _type){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event

                if(_type.equals("onDelAll")){
                    ProcessDelete();
                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnApprove:

                break;

            case R.id.btnDelBC:
                onDelBC(view);
                break;

            case R.id.btnMakeSlip:

                break;
            default:
                break;
        }
    }
}
