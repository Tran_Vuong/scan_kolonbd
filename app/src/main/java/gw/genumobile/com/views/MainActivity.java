package gw.genumobile.com.views;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;


import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import gw.genumobile.com.BuildConfig;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.HandlerWebService;
import gw.genumobile.com.views.gwcore.AppConfig;
import gw.genumobile.com.views.gwcore.DebugActivity;

import static gw.genumobile.com.R.id.tvVersion;


public class MainActivity extends AppCompatActivity {
    private SharedPreferences appPrefs;
    private Handler customHandler = new Handler();
    private ListView obj;

    TextView txt_error;
    EditText editPass;
    String str_Longin;
    SQLiteDatabase db;

    String jsonURL="";
    String newVerName = "";
    String appUrl = "";
    int newVerCode = -1;
    ProgressDialog pd = null;
    String UPDATE_SERVERAPK = "GenuMobile.apk";
    String txtPass="";
    WebView myBrowser;
    String tmpIP = "";
    Boolean checkIP=false;
    @Override
    protected void onResume(){
        super.onResume();
        boolean read = ReadFileConfig();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set Config
        appPrefs = getSharedPreferences("myConfig", MODE_PRIVATE);
        //set Language
        setLanguageApplication();
        //set Layout
        setContentView(R.layout.activity_main);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txt_error =(TextView)findViewById(R.id.txt_error);

        boolean read = ReadFileConfig();
        String ipServer = HandlerWebService.URL;
        tmpIP = ipServer.substring(ipServer.indexOf("//")+2);
        tmpIP = tmpIP.substring(0, tmpIP.indexOf("/"));
        checkIP=appPrefs.getBoolean("ckbInternet", Boolean.FALSE);
        //CheckUpdateVersionAuto
        CheckUpdateVersionAuto();

        myBrowser = (WebView)findViewById(R.id.mybrowser);
        final MyJavaScriptInterface myJavaScriptInterface = new MyJavaScriptInterface(this);
        myBrowser.addJavascriptInterface(myJavaScriptInterface, "AndroidFunction");
        myBrowser.getSettings().setJavaScriptEnabled(true);
        myBrowser.loadUrl("file:///android_asset/mypage.html");

        customHandler.postDelayed(updateParsePassword, 500);

        //region Set user from SharePreference
        EditText username = (EditText) findViewById(R.id.editUsername);
        String userList = appPrefs.getString("userSave", "");
        if(!userList.equals(""))
        {
            username.setText(userList);
            EditText pass = (EditText) findViewById(R.id.editPass);
            pass.requestFocus();
        }

        TextView ver = (TextView) findViewById(R.id.tvVersion);

        try {
            int verCode = getBaseContext().getPackageManager().getPackageInfo("gw.genumobile.com", 0).versionCode;
            ver.setText("Ver. Code: " + String.valueOf(verCode));
        }catch (Exception ex){}
        //endregion
    }

    private Runnable updateParsePassword = new Runnable() {
        public void run()
        {
            EditText pass = (EditText) findViewById(R.id.editPass);
            txtPass = pass.getText().toString();
            myBrowser.loadUrl("javascript:callFromActivity(\"" + txtPass + "\")");
            customHandler.postDelayed(this, 500);
        }
    };

    public class MyJavaScriptInterface {

        Context mContext;

        MyJavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface   // must be added for API 17 or higher
        public void showToast(String toast){
            txtPass=toast;
            //Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        }

        public void openAndroidDialog(){
            AlertDialog.Builder myDialog= new AlertDialog.Builder(MainActivity.this);
            myDialog.setTitle("Alert!");
            myDialog.setMessage("You can do what you want!");
            myDialog.setPositiveButton("ON", null);
            myDialog.show();
        }
    }

    public void CheckUpdateVersionAuto(){
        final int verCode = this.getVerCode(this);
       new Thread(new Runnable() {
           @Override
           public void run() {
               try {
                   if (getServerVer()) {

                       if (newVerCode > verCode) {
                           doNewVersionUpdate();//Updated version
                       } else {
                           //notNewVersionUpdate();//No Updated version
                       }
                   }
               }catch (Exception ex) {
                    //
               }
           }
       }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {

            final Dialog dialog_confirm = new Dialog(this);
            dialog_confirm.setContentView(R.layout.dialog_confirm);
            dialog_confirm.setTitle("Confirm!!!");
            dialog_confirm.setCanceledOnTouchOutside(false);
            dialog_confirm.show();

            Button btn_Ok = (Button) dialog_confirm.findViewById(R.id.btn_OK);
            final EditText edt_confirm = (EditText) dialog_confirm.findViewById(R.id.edt_confirm);
            btn_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(edt_confirm.getText().toString().equals("vina@123")){
                        dialog_confirm.dismiss();
                        Intent i = new Intent(getApplicationContext(), AppConfig.class);
                        i.putExtra("type","00" );
                        i.putExtra("user", "admin");
                        startActivity(i);
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Password is incorrect!!!",Toast.LENGTH_LONG).show();
                        edt_confirm.getText().clear();
                    }
                }
            });

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void OnLogin( View view) throws InterruptedException, ExecutionException
    {
        if (CNetwork.isInternet(tmpIP,checkIP)){
            Button btnLogin = (Button) findViewById(R.id.btnLogin);

            EditText user = (EditText) findViewById(R.id.editUsername);
            String txtUser = user.getText().toString();
            EditText pwd = (EditText) findViewById(R.id.editPass);
            String txtPwd = pwd.getText().toString();
            //myBrowser.loadUrl("javascript:callFromActivity(\"" + txtPwd + "\")");

            txt_error = (TextView) findViewById(R.id.txt_error);

            //txt_error.setText("Please,input ID!!!."+txtUser.length());
            if (txtUser.length() < 1 || txtPass.length() < 1) {
                if (txtUser.length() < 1) {
                    txt_error.setText("Please,input User ID!!!.");
                    return;
                }
                if (txtPass.length() < 1) {
                    txt_error.setText("Please,input PASS!!!.");
                    return;
                }
            }

            //String passMd5 = HandlerWebService.EncodeMD5(txtPass);

            String para = "1,LG_MPOS_M010_CHECK_LOGIN_V2," + txtUser + "|" + txtPass;

            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(para).get();

            //end insert data login
            if (result.length > 0 ) {
                if(result[0].length<2) {
                    txt_error.setText(result[0][0]);
                    return;
                }
                //region Save config SharedPreferences
                SharedPreferences.Editor editor = appPrefs.edit();
                editor.putString("userSave", txtUser);
                editor.putString("userInfo", result[0][0]+"|"+result[0][3]+"|"+result[0][4]);
                // Commit the edits!
                editor.commit();
                //endregion
                //Intent i = new Intent(view.getContext(), OptionOnline.class);
                //i.putExtra("user", result[0][0]);
                //i.putExtra("user_id", result[0][2]);
                //i.putExtra("user_pk", result[0][3]);
                Intent i = new Intent(view.getContext(), gwMainActivity.class);
                //     Intent i = new Intent(view.getContext(), MasterMenu.class);
                startActivity(i);
            } else {
                txt_error.setText("Login incorrect!!!.");
            }
        }
        else{
            txt_error.setText("Unable to connect with the server. Check your internet connect and try again!");
            Toast.makeText(getApplicationContext(), "Unable to connect with the server. Check your internet connect and try again.", Toast.LENGTH_LONG).show();
        }
       /* */
    }
    /*---------------------------------------------------------------------*/
	/*---------------------READ FILE CONFIG--------------------------------*/
	/*---------------------------------------------------------------------*/
    private boolean  ReadFileConfig()
    {
        try{
            HandlerWebService.URL       = "http://" + appPrefs.getString("server", "") + "/" + appPrefs.getString("company", "") + "/gwWebservice.asmx";
            HandlerWebService.dbName    = appPrefs.getString("dbName", "");
            HandlerWebService.dbUser    = appPrefs.getString("dbUser", "");
            if(appPrefs.getString("dbPass", "")!=""){
                HandlerWebService.dbPwd     = appPrefs.getString("dbPass", "");
            }
            else{
                HandlerWebService.dbPwd     = appPrefs.getString("dbUser", "") + "2";
            }

            if(appPrefs.getString("company", "")!="")
                jsonURL="http://" + appPrefs.getString("server", "") + "/" + appPrefs.getString("company", "") + "/live_update/android/"+appPrefs.getString("company", "")+".aspx";
               // jsonURL="http://genuclouding.com/android/"+appPrefs.getString("company", "")+".aspx";
            else{
                jsonURL="http://genuclouding.com/android/";
            }
        }catch (Exception ex) {
            return false;
        }

        return true;

    }

    public boolean isInternet(String ip)
    {
        boolean reachable = false;
        try {
            if (ip.indexOf(":") > 0)
                ip = ip.substring(0,ip.indexOf(":") );

            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 " + ip);
            int returnVal = p1.waitFor();

            if (returnVal==0)
                reachable = true;
            else
                reachable = false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return reachable;
    }

    private  String splitContent(String line)
    {
        String[] result = line.split("=");
        return result[1];
    }

	/*---------------------------------------------------------------------*/
    //region*---------------------CHECK AUTO UPDATE--------------------------------*/

    public void OnCheckUpdate( View view) throws InterruptedException, ExecutionException
    {
        //doNewVersionUpdate();

        //doNewVersionUpdate();//Updated version

        if(getServerVer()){
            int verCode = this.getVerCode(this);
            if(newVerCode>verCode){
                doNewVersionUpdate();//Updated version
            }else{
                notNewVersionUpdate();//No Updated version
            }
        }
        /**/
    }

    /**
     * Get the version number
     */
    public int getVerCode(Context context){
        int verCode = -1;
        try {
            verCode = context.getPackageManager().getPackageInfo("gw.genumobile.com", 0).versionCode;
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            Log.e("Get version code error:", e.getMessage());
        }
        return verCode;
    }

    /**
     * Get the version name
     */
    public String getVerName(Context context){
        String verName = "";
        try {
            verName = context.getPackageManager().getPackageInfo("gw.genumobile.com", 0).versionName;
        } catch (NameNotFoundException e) {
            Log.e("Get version name error:", e.getMessage());
        }
        return verName;
    }

    //region Get the version number from the server name and version
    public boolean getServerVer(){
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);

            URL url = new URL(jsonURL);
            //URL url = new URL("http://laptoptheduy.com/updater/version.aspx");
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(true);
            httpConnection.setRequestMethod("GET");
            httpConnection.connect();
            InputStreamReader reader = new InputStreamReader(httpConnection.getInputStream());
            BufferedReader bReader = new BufferedReader(reader);
            String json="[";
            String js = bReader.readLine();
            json=json+js+"]";
            JSONArray array = new JSONArray(json);
            JSONObject jsonObj = array.getJSONObject(0);
            newVerCode = Integer.parseInt(jsonObj.getString("verCode"));
            newVerName = jsonObj.getString("verName");
            appUrl ="http://" + appPrefs.getString("server", "") + "/" + appPrefs.getString("company", "") +  "/live_update/android/"+  jsonObj.getString("appURI");
            UPDATE_SERVERAPK=newVerName+".apk";
        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.out.print("Error:"+e.toString());

            e.printStackTrace();
            return false;
        }
        return true;
    }
    //endregion

    //region No updated version

    public void notNewVersionUpdate(){
        int verCode = this.getVerCode(this);
        String verName = this.getVerName(this);
        StringBuffer sb = new StringBuffer();
        sb.append("Current version：");
        sb.append(verName);
        sb.append(" - - Code: ");
        sb.append(verCode);
        sb.append("\nIs the latest version, no update");
        Dialog dialog = new AlertDialog.Builder(this)
                .setIcon(R.drawable.cfm_upgrade)
                .setTitle("Software Updates")
                .setMessage(sb.toString())
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.cancel();
                        //finish();
                    }
                }).create();
        dialog.show();
    }
    //endregion

    //region Updated version

    public void doNewVersionUpdate(){
        int verCode = this.getVerCode(this);
        String verName = this.getVerName(this);
        StringBuffer sb = new StringBuffer();
        sb.append("Current version：");
        sb.append(verName);
        sb.append(" - - Code: ");
        sb.append(verCode);
        sb.append("\nFound in version：");
        sb.append(newVerName);
        sb.append(" - - Code: ");
        sb.append(newVerCode);
        sb.append("\nPlease update now!");
        Dialog dialog = new AlertDialog.Builder(this)
                .setIcon(R.drawable.cfm_upgrade)
                .setTitle("Software Updates")
                .setMessage(sb.toString())
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        pd = new ProgressDialog(MainActivity.this);
                        pd.setIcon(R.drawable.cfm_download);
                        pd.setTitle("Downloading");
                        pd.setMessage("Please wait while your phone is download 。。。");
                        pd.setCancelable(false);
                        //downFile("https://dl.dropboxusercontent.com/s/33064mcpo49q558/GenuMobile-v2.0-release.apk");
                        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        downFile(appUrl);
                        //downFile("https://dl.dropbox.com/s/e3bw2rikaawfr62/genuwin.apk");
                        //downFile("https://dl.dropboxusercontent.com/s/c8ys3hh56o4l1ez/release20160401.apk");
                    }
                })
                .setNegativeButton("Don't Update", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        //finish();
                        dialog.cancel();
                    }
                }).create();
        //Show Updates box
        dialog.show();
    }
    //endregion

    //region ------ Download apk--------
    public void downFile(final String url){
        pd.show();
        new Thread(){
            public void run(){
                HttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(url);
                HttpResponse response;
                try {
                    response = client.execute(get);
                    HttpEntity entity = response.getEntity();
                    long length = entity.getContentLength();
                    InputStream is =  entity.getContent();
                    FileOutputStream fileOutputStream = null;
                    if(is != null){
                        File file = new File(Environment.getExternalStorageDirectory(),UPDATE_SERVERAPK);
                        fileOutputStream = new FileOutputStream(file);
                        byte[] b = new byte[1024];
                        int charb = -1;
                        int count = 0;
                        while((charb = is.read(b))!=-1){
                            fileOutputStream.write(b, 0, charb);
                            count += charb;
                        }
                    }
                    fileOutputStream.flush();
                    if(fileOutputStream!=null){
                        fileOutputStream.close();
                    }
                    down();
                }  catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }.start();
    }
    //endregion

    //region handler
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            super.handleMessage(msg);
            pd.cancel();
            update();
        }
    };
    //endregion

    //region  --------The download is complete, the download dialog canceled by handler--------

    public void down(){
        new Thread(){
            public void run(){
                Message message = handler.obtainMessage();
                handler.sendMessage(message);
            }
        }.start();
    }
    //endregion

    //region ----------Install the application------------
    public void update(){
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            //Uri _uri =Uri.fromFile(new File(Environment.getExternalStorageDirectory(), UPDATE_SERVERAPK));
//            Uri _uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", new File(Environment.getExternalStorageDirectory(), UPDATE_SERVERAPK));
           Uri _uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider",new File(Environment.getExternalStorageDirectory(), UPDATE_SERVERAPK));
            intent.setDataAndType(_uri
                    , "application/vnd.android.package-archive");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        }
            catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "apk file launch error: " +   e.getMessage(), Toast.LENGTH_LONG).show();
            }
    }
    //endregion
    //endregion

    //region set Language
    protected void setLanguageApplication(){

        String strLang = "en";
        String languageApp = appPrefs.getString("language", "");
        if(languageApp!=""){
            strLang = languageApp;
        }


        Resources res = this.getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(strLang.toLowerCase());
        res.updateConfiguration(conf, dm);

        //this.setContentView(activity);
    }
    //endregion
}
