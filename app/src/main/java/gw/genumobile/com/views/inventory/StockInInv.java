package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.GridListViewMid;
import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.services.ApproveAsyncTask;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.views.PopDialogGrid;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.BaseGwActive;


public class StockInInv extends gwFragment implements View.OnClickListener {
    protected Boolean flagUpload = true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;
    public static final int tes_role_pk = 2204;
    private static final String formID = "7";
    //int[] colors = new int[]{0x30FF0000, 0x300000FF, 0xCCFFFF, 0x99CC33, 0x00FF99};
    
    Button btnViewStt, btnInquiry, btnMakeSlip, btnDelAll, btnApprove, btnEditQty, btnListBot;
    
    Button btnList;

    Queue queue = new LinkedList();
    Calendar c;
    SimpleDateFormat df;

    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    TextView recyclableTextView, _txtDate, _txtError, _txtTT, _txtSent, _txtRemain,_txtTotalGridBot,_txtTotalQtyBot,_txtTotalGridMid,_txtTime;
    EditText myBC, bc2, code, name, lotNo,edQty,edBarcod ;
    public Spinner myLstWH,myLstLoc, myLstLine;
    List<String> arrLoc;
    String sql = "", tr_date = "",scan_date="",scan_time="",unique_id="",pk_user="",user="",user_id="";
    String wh_pk = "",wh_name = "",wh_name_grid = "", line_pk = "",line_name="", loc_pk = "",loc_name="",approveYN="N";
    String  data[] = new String[0];
    String dlg_pk="",dlg_barcode="",dlg_qty="";
    int timeUpload=0;
    HashMap hashMapWH = new HashMap();
    HashMap hashMapLoc = new HashMap();
    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_in_inventory_v2, container, false);
        //   gwActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
        btnApprove.setOnClickListener(this);
        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);

        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);

        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);

        myBC = (EditText) rootView.findViewById(R.id.editBC);
        myLstWH = (Spinner) rootView.findViewById(R.id.lstWH);
        myLstLoc = (Spinner) rootView.findViewById(R.id.lstLoc);
        //myLstLine   = (Spinner) rootView.findViewById(R.id.lstLine);

        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        _txtDate = (TextView) rootView.findViewById(R.id.txtDate);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");

        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        _txtDate.setText(formattedDate);
        _txtTime = (TextView) rootView.findViewById(R.id.txtTime);
        if (Measuredwidth <= 600) {
            _txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);
        }
        OnShowGridHeader();
        OnShowScanLog();
        OnShowScanIn();
        OnShowScanAccept();
        CountSendRecord();
        LoadWH();
        init_color();


        OnPermissionApprove();

        myBC.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (wh_pk.equals("0")) {
                            myBC.getText().clear();
                            myBC.requestFocus();

                            _txtError.setTextColor(Color.RED);
                            _txtError.setText("Warehouse was wrong!!");
                            gwMActivity.alertToastShort("Warehouse was wrong. Plz check Wifi !!!");
                        } else {
                            OnSaveBC();
                        }

                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // this is for backspace
                    myBC.clearFocus();
                    //Log.e("IME_TEST", "BACK VIRTUAL");
                }
                return false;
            }
        });


        timeUpload = hp.GetTimeAsyncData();

        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);
        return rootView;
    }


    private void init_color() {
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run() {
            if (flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload * 1000);
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        customHandler.removeCallbacks(updateDataToServer);

    }

    @Override
    public void onResume() {
        super.onResume();
        customHandler.post(updateDataToServer);
//        customHandler.postDelayed(updateDataToServer, timeUpload*1000);
    }

    private void doStart() {
        flagUpload = false;
        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        //cursor2 = db2.rawQuery("select TLG_POP_LABEL_PK, TR_WAREHOUSE_PK,TR_LOC_ID, TR_QTY , TR_LOT_NO, TR_TYPE, ITEM_BC, TR_DATE, TR_ITEM_PK, TR_LINE_PK, ITEM_CODE, ITEM_NAME,ITEM_TYPE,PK,TLG_GD_REQ_D_PK, SLIP_NO  from INV_TR where DEL_IF=0 and TR_TYPE='6' and sent_yn = 'N' order by PK asc LIMIT 1",null);
        cursor2 = db2.rawQuery("select PK, ITEM_BC,TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK  from INV_TR where DEL_IF=0 and TR_TYPE='" + formID + "' and sent_yn = 'N' order by PK asc LIMIT 10", null);
        //System.out.print("\n\n\n cursor2.count: "+ String.valueOf(cursor2.getCount()));
        //data=new String [cursor2.getCount()];
        //boolean read=Config.ReadFileConfig();

        if (cursor2.moveToFirst()) {

            // Write your code here to invoke YES event
            System.out.print("\n\n**********doStart********\n\n\n");

            //set value message
            _txtError = (TextView) rootView.findViewById(R.id.txtError);
            _txtError.setTextColor(Color.RED);
            _txtError.setText("");

            //  data=new String [cursor2.getCount()];
            data = new String[1];
            int j = 0;
            String para = "";
            boolean flag = false;
            do {
                flag = true;
                for (int i = 0; i < cursor2.getColumnCount(); i++) {
                    if (para.length() <= 0) {
                        if (cursor2.getString(i) != null)
                            para += cursor2.getString(i) + "|";
                        else
                            para += "|";
                    } else {

                        if (flag == true) {
                            if (cursor2.getString(i) != null) {
                                para += cursor2.getString(i);
                                flag = false;
                            } else {
                                para += "|";
                                flag = false;
                            }
                        } else {
                            if (cursor2.getString(i) != null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }
                }
                para += "|" + deviceID;
                para += "|" + bsUserID;
                para += "*|*";
            } while (cursor2.moveToNext());
            //////////////////////////
            para += "|!" + "LG_MPOS_UPLOAD_INC_INV_V2";
            data[j++] = para;
            System.out.print("\n\n\n para upload: " + para);
            Log.e("para upload: ", para);

            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                task.execute(data);
                gwMActivity.alertToastShort(getString(R.string.send_server));
            } else {
                flagUpload = true;
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }
        } else {
            flagUpload = true;
        }
        cursor2.close();
        db2.close();
    }

    ////////////////////////////////////////////////
    public void OnSaveBC() {
        if (myBC.getText().toString().equals("")) {
            _txtError.setTextColor(Color.RED);
            _txtError.setText(getString(R.string.pls_scanBC));
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        } else {

            String str_scanBC = myBC.getText().toString().toUpperCase();

            try {
                if (str_scanBC.length() > 20) {
                    _txtError.setText(getString(R.string.BC_more20));
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                }

                boolean isExists = hp.isExistBarcode(str_scanBC, formID);// check barcode exist in data
                if (isExists)// exist data
                {
                    alertRingMedia();
                    _txtError.setText(getString(R.string.BC_exist));
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                } else {
                    if (str_scanBC.indexOf("L") == 0) {
                        String str_loc = str_scanBC.substring(1, str_scanBC.length());
                        if (arrLoc.indexOf(str_loc) < 0) {
                            alertRingMedia2();
                            onPopAlert("Location không hợp lệ !!!");
                        } else {
                            myLstLoc.setSelection(arrLoc.indexOf(str_loc));
                        }
                    } else {
                        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        scan_date = CDate.getDateyyyyMMdd();
                        scan_time = CDate.getDateYMDHHmmss();

                        db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK, TLG_IN_WHLOC_IN_NAME,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                                + "VALUES('"
                                + str_scanBC + "','"
                                + wh_pk + "','"
                                + wh_name + "','"
                                + loc_pk + "','"
                                + loc_name + "','"

                                + scan_date + "','"
                                + scan_time + "','"
                                + "N" + "','"
                                + " " + "','"
                                + formID + "');");
                    }
                }
            } catch (Exception ex) {
                _txtError.setText(getString(R.string.save_error));
                Log.e("OnSaveBC", ex.getMessage());
            } finally {
                db.close();
                myBC.getText().clear();
                myBC.requestFocus();
            }
        }
        OnShowScanLog();
        CountSendRecord();
    }

    ////////////////////////////////////////////////
    private void LoadWH() {
        String l_para = "1,LG_MPOS_M010_GET_WH_USER," + bsUserPK + "|wh_ord_2";
        try {
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String dataWHGroup[][] = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < dataWHGroup.length; i++) {
                lstGroupName.add(dataWHGroup[i][1].toString());
                hashMapWH.put(i, dataWHGroup[i][0]);
            }

            if (dataWHGroup != null && dataWHGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstWH.setAdapter(dataAdapter);
                wh_name = myLstWH.getItemAtPosition(0).toString();
            } else {
                myLstWH.setAdapter(null);
                wh_pk = "";
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
        //-----------

        // onchange spinner wh
        myLstWH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                wh_name = myLstWH.getItemAtPosition(i).toString();
                Log.e("WH_name", wh_name);
                Object pkGroup = hashMapWH.get(i);
                if (pkGroup != null) {
                    wh_pk = String.valueOf(pkGroup);
                    Log.e("WH_PK: ", wh_pk);
                    if (!wh_pk.equals("0")) {
                        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
                            TextView tvLoc = (TextView) rootView.findViewById(R.id.tvLoc);
                            tvLoc.setVisibility(View.VISIBLE);
                            myLstLoc.setVisibility(View.VISIBLE);
                        }
                        LoadLocation();
                    }
                } else {
                    wh_pk = "";
                    Log.e("WH_PK: ", wh_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    ////////////////////////////////////////////////


    ////////////////////////////////////////////////
    private void LoadLocation() {
        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {

            String l_para = "1,LG_MPOS_GET_LOC," + wh_pk;
            try {
                ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                String dtLocGroup[][] = networkThread.execute(l_para).get();
                List<String> lstLocName = new ArrayList<String>();

                for (int i = 0; i < dtLocGroup.length; i++) {
                    lstLocName.add(dtLocGroup[i][1].toString());
                    hashMapLoc.put(i, dtLocGroup[i][0]);
                }

                if (dtLocGroup != null && dtLocGroup.length > 0) {
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstLocName);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    myLstLoc.setAdapter(dataAdapter);
                    loc_name = myLstLoc.getItemAtPosition(0).toString();
                } else {
                    myLstLoc.setAdapter(null);
                    loc_pk = "";
                    Log.e("Loc_PK: ", loc_pk);
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong(ex.getMessage().toString());
                Log.e("Loc Error: ", ex.getMessage());
            }
            //-----------
            // onchange spinner LOC
            myLstLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView,
                                           View selectedItemView, int i, long id) {
                    // your code here
                    loc_name = myLstLoc.getItemAtPosition(i).toString();
                    Log.e("Loc_name", loc_name);
                    Object pkGroup = hashMapLoc.get(i);
                    if (pkGroup != null) {
                        loc_pk = String.valueOf(pkGroup);
                        Log.e("Loc_PK: ", loc_pk);

                    } else {
                        loc_pk = "";
                        Log.e("Loc_PK: ", loc_pk);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }
            });


        } else {
            TextView tvLoc = (TextView) rootView.findViewById(R.id.tvLoc);
            tvLoc.setVisibility(View.INVISIBLE);
            myLstLoc.setVisibility(View.INVISIBLE);
        }

    }

    ////////////////////////////////////////////////
    public List<String> GetDataOnServer(String type, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";

            if (type.endsWith("WH_PK")) //WH PK
            {
                l_para = "1,LG_MPOS_M010_GET_WH_PK," + para;
            }

            if (type.endsWith("LINE_PK"))//LINE PK
            {
                l_para = "1,LG_MPOS_M010_GET_LINE_PK," + para;
            }
            if (type.endsWith("LOC_PK"))//LINE PK
            {
                l_para = "1,LG_MPOS_GET_LOC_PK," + para;
            }
            if (type.endsWith("ROLE")) {
                l_para = "1,LG_MPOS_GET_ROLE," + para;
            }

            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                if (type.endsWith("BC")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                } else if (type.endsWith("WH_PK")) {
                    labels.add(result[i][0].toString());
                } else if (type.endsWith("LINE_PK")) {
                    labels.add(result[i][0].toString());
                } else if (type.endsWith("LOC_PK")) {
                    labels.add(result[i][0].toString());
                } else if (type.endsWith("ROLE")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                } else
                    labels.add(result[i][1].toString());
            }
        } catch (Exception ex) {
            //gwMActivity.alertToastLong(type + ": " + ex.getMessage());
        }
        return labels;
    }

    public void onClickViewStt(View view) {

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ////////////////////////////////////////////////
    public void onMakeSlip(View view) {

        //Check het Barcode send to server
        if (checkRecordGridView(R.id.grdScanLog)) {
            gwMActivity.alertToastLong(this.getResources().getString(R.string.tvAlertMakeSlip));
            return;
        }
        //Check have value make slip
        if (hp.isMakeSlip(formID) == false) return;
//        if(!checkRecordGridView(R.id.grdScanAccept)) return;

        String title = getString(R.string.tvConfirmMakeSlip);
        String mess = getString(R.string.mesConfirmMakeSlip);
        alertDialogYN(title, mess, "onMakeSlip");
    }

    public void ProcessMakeSlip() {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if (queue.size() > 0) {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data = new String[myLst.length];


        } else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "  select TR_ITEM_PK,SUM(TR_QTY) as TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, TR_WH_IN_PK, TLG_IN_WHLOC_IN_PK, UNIT_PRICE, PO_NO, EXECKEY " +
                        " FROM INV_TR  WHERE DEL_IF = 0  AND SENT_YN = 'Y' AND TR_TYPE ='" + formID + "' AND STATUS='000' and SLIP_NO in ('-','') " +
                        " GROUP BY TR_ITEM_PK,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK,  TR_WH_IN_PK, TLG_IN_WHLOC_IN_PK, UNIT_PRICE, PO_NO, EXECKEY ";
                cursor = db.rawQuery(sql, null);

                //cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WH_IN_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='7' ", null);

                int count = cursor.getCount();
                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_wh_pk = cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK"));
                            String ex_supp_pk = cursor.getString(cursor.getColumnIndex("SUPPLIER_PK"));
                            String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            String ex_po_d_pk = cursor.getString(cursor.getColumnIndex("TLG_PO_PO_D_PK"));
                            hp.updateExeckeyIncome(formID, ex_item_pk, ex_wh_pk, ex_supp_pk, ex_lot_no, ex_po_d_pk, unique_id);

                            para += "|" + unique_id;
                        } else {
                            para += "|" + execkey;
                        }
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_ST_INC_MAKESLIP";
                    System.out.print("\n\n ******para make slip in stock: " + para);
                    //data[j++] = para;
                    data[j] = para;
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                task.execute(data);
            } else {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        }
    }

    ////////////////////////////////////////////////
    public void onApprove(View view)  {
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        String title = "Confirm Approve...";
        String mess = "Are you sure you want to Approve";
        alertDialogYN(title, mess, "onApprove");
    }

    public void ProcessApprove() {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if (queue.size() > 0) {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data = new String[myLst.length];
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j = 0;
                String para = "";
                boolean flag = false;
                for (int i = 0; i < myLst.length; i++) {
                    flag = true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WH_IN_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and SLIP_NO in('-','') and TR_TYPE='" + formID + "' and pk=" + myLst[i], null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //para += "|!"+USER;
                        para += "|" + unique_id;
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                    }
                    para += "*|*";
                }
                para += "|!LG_MPOS_PRO_ST_INC_MAKESLIP";
                data[j++] = para;
                System.out.print("\n\n ******para make slip goods delivery: " + para);
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ApproveAsyncTask task = new ApproveAsyncTask(gwMActivity,this);
                task.execute(data);
            } else {
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }

        } else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, SCAN_DATE, TR_WH_IN_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO in('-','')   and TR_TYPE='" + formID + "' ", null);
                int count = cursor.getCount();
                data = new String[count];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_ST_INC_MAKESLIP";
                    System.out.print("\n\n ******para make slip in stock: " + para);
                    data[j++] = para;
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ApproveAsyncTask task = new ApproveAsyncTask(gwMActivity,this);
                task.execute(data);
            } else {
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }
        }
    }

    ////////////////////////////////////////////////
    public void onDelAll(View view) {
        // if(!checkRecordGridView(R.id.grdScanAccept) && !checkRecordGridView(R.id.grdScanIn)) return;
        if (!hp.isCheckDelete(formID)) return;
        String title = "Confirm Delete..";
        String mess = "Are you sure you want to delete ???";
        alertDialogYN(title, mess, "onDelAll");
    }

    public void ProcessDelete() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();
                data = new String[myLst.length];
                for (int i = 0; i < myLst.length; i++) {
                    db.execSQL("DELETE FROM INV_TR where TR_TYPE='" + formID + "' and pk=" + myLst[i]);
                }

            } else {
                cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "' and SLIP_NO in('-','') and STATUS ='000' ", null);
                int count = cursor.getCount();
                String para = "";
                boolean flag = false;
                int j = 0;
                if (cursor.moveToFirst()) {
                    data = new String[1];
                    do {

                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + bsUserID;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_UPD_INV_TR_DEL";
                    data[j] = para;
                    Log.e("para update||delete: ", para);
                }
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("Delete All :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        } else {
            gwMActivity.alertToastLong(getString(R.string.network_broken));
        }
    }

    public void onDeleteAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();

            } else {
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='" + formID + "';");
            }
        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            OnShowScanLog();
            OnShowScanIn();
            OnShowScanAccept();
        }
    }

    ////////////////////////////////////////////////
    public void onEditQty(View view) {
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        if (dlg_pk != "") {
            final Dialog dlg_EditQty = new Dialog(gwMActivity);
            dlg_EditQty.setContentView(R.layout.dialog_edit_qty);
            dlg_EditQty.setCancelable(false);//not close when touch
            dlg_EditQty.setTitle(" Edit Qty");

            edBarcod = (EditText) dlg_EditQty.findViewById(R.id.dlg_txt_Barcode);
            edQty = (EditText) dlg_EditQty.findViewById(R.id.dlg_txt_Qty);

            edBarcod.setText(dlg_barcode);
            edQty.setText(dlg_qty);
            Button btnSave = (Button) dlg_EditQty.findViewById(R.id.dlg_btnSave);
            Button btnCancel = (Button) dlg_EditQty.findViewById(R.id.dlg_btnCancel);

            // Attached listener for login GUI button
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlg_qty = edQty.getText().toString();
                    if (!dlg_qty.equals("")) {
                        processUpdateQty(dlg_pk, dlg_qty, dlg_barcode);
                        dlg_EditQty.dismiss();
                    } else {
                        onPopAlert("Qty is empty!!!");
                    }
                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlg_EditQty.dismiss();
                }
            });
            dlg_EditQty.show();
        } else {
            gwMActivity.alertToastShort("Please select row first !!!");
        }
    }

    public void processUpdateQty(String pk, String qty, String bc) {
        String para = pk + "|" + qty + "|" + bc + "|!LG_MPOS_UPD_QTY";
        data = new String[1];
        data[0] = para;
        InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
        task.execute(data);
    }

    public void processUpdateQtyFinish() {
        dlg_pk = "";
        OnShowScanAccept();
        gwMActivity.alertToastLong("Save finish!!");
        //dlg_EditQty.dismiss();

    }

    ////////////////////////////////////////////////
    public void onClickInquiry(View view){
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        startActivity(openNewActivity);
    }

    ///////////////////////////////////////////////
    public void onListGridMid(View view){
        if (!checkRecordGridView(R.id.grdScanIn)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  frGridListViewMid.newInstance(1,formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    ///////////////////////////////////////////////
    public void onListGridBot(View view){
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new GridListViewBot();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }



    // show data scan log
    public void OnShowScanLog() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' order by PK desc LIMIT 20", null);// TLG_LABEL

            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
            _txtRemain.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanIn() {
        try {
            int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, TR_WH_IN_NAME, LINE_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SENT_YN='Y' AND SCAN_DATE > '" + date_previous + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > '" + date_previous + "' AND TR_TYPE = '" + formID + "' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
            _txtTotalGridMid.setText("Total: " + count);
            _txtTotalGridMid.setTextColor(Color.BLUE);
            _txtTotalGridMid.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan accept
    public void OnShowScanAccept() {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "  SELECT  TR_ITEM_PK,  ITEM_CODE,COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_IN_NAME,EXECKEY,TLG_IN_WHLOC_IN_PK  " +
                    " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND TR_TYPE = '" + formID + "' AND STATUS IN('OK', '000') " +
                    " GROUP BY TR_ITEM_PK,ITEM_CODE,TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_IN_NAME,EXECKEY,TLG_IN_WHLOC_IN_PK " +
                    " ORDER BY EXECKEY desc,TR_ITEM_PK  LIMIT 30 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TOTAL")), scrollableColumnWidths[2], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight, 1));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UNIT_PRICE")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PO_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")), 0, fixedRowHeight, 0));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            onShowClickGridBot(v);

                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND STATUS='000'";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 16.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 16.0);

        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanAccept: " + ex.getMessage());
            Log.e("GridScanAccept: ", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public  void onShowClickGridBot(View v){
        boolean duplicate = false;

        TableRow tr1 = (TableRow) v;
        TextView tvLotNo = (TextView) tr1.getChildAt(4); //LOT_NO
        String st_LotNO = tvLotNo.getText().toString();
        TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP NO
        String st_SlipNO = tvSlipNo.getText().toString();

        TextView tvItemPK = (TextView) tr1.getChildAt(7); //TR_ITEM_PK
        dlg_pk = tvItemPK.getText().toString();
        TextView tvWhPk = (TextView) tr1.getChildAt(8); //WH_PK
        String whPK = tvWhPk.getText().toString();

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new PopDialogGrid();
        Bundle args = new Bundle();

        args.putString("TYPE", formID);
        args.putString("ITEM_PK", dlg_pk);
        args.putString("LOT_NO",st_LotNO);
        args.putString("WH_PK",whPK);
        args.putString("SLIP_NO",st_SlipNO);
        args.putString("USER",bsUserID);
        dialogFragment.setArguments(args);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLineName), scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        //row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTotal), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        //row.addView(makeTableColHeaderWithText("Unit Price", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[4], fixedHeaderHeight));
        //row.addView(makeTableColHeaderWithText("PO NO", scrollableColumnWidths[2], fixedHeaderHeight));
        //row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }


    //////////////////////////

    public void OnPermissionApprove() {
        String para = bsUserPK + '|' + String.valueOf(tes_role_pk);
        List<String> arr_role = GetDataOnServer("ROLE", para);
        if (arr_role.size() > 0) {
            approveYN = arr_role.get(0);
            Log.e("approveYN", approveYN);
        }
        btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
        if (approveYN.equals("Y")) {
            btnApprove.setVisibility(View.VISIBLE);
        } else
            btnApprove.setVisibility(View.GONE);

        Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMSlip.setVisibility(View.VISIBLE);
    }

    public void CountSendRecord() {
        flagUpload = true;
        // total scan log
        _txtSent = (TextView) rootView.findViewById(R.id.txtSent);
        _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid = Integer.parseInt(_txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        int countBot = Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Send: " + (countMid + countBot));

        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]", ""));

        _txtTT = (TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: " + (countMid + countBot + countRe));

    }

    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str = str_alert;

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ///////////////////////////////////////////////
    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onApprove")) {
                    Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    btnMSlip.setVisibility(View.GONE);
                    btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    btnApprove.setVisibility(View.GONE);
                    ProcessApprove();
                }
                if (_type.equals("onDelAll")) {
                    ProcessDelete();
                }
                if (_type.equals("onMakeSlip")) {
                    //Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    //btnMSlip.setVisibility(View.GONE);
                    btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    btnApprove.setVisibility(View.GONE);
                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiem tra coi trong requestCode =REQUEST_CODE_INPUT hay khong
        //V� ta c� th? m? Activity v?i nh?ng RequestCode khac nhau
        if(requestCode==REQUEST_CODE)
        {
            //Kiem tra ResultCode tra ve, cai nay ? ben InputDataActivity truyen ve
            OnShowScanAccept();
            switch(resultCode)
            {
                case RESULT_CODE:

                    break;
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //testSetting();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
                onClickInquiry(view);

                break;

            case R.id.btnApprove:

                break;
            case R.id.btnViewStt:
                onClickViewStt(view);

                break;

            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnList:
                onListGridMid(view);

                break;
            case R.id.btnDelAll:

                onDelAll(view);
                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);
                break;
            default:
                break;
        }
    }
}
