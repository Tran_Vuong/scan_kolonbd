package gw.genumobile.com.views.mold;


//RI20160406-001

import android.graphics.Color;
import android.os.Bundle;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;

import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.R;


/**
 *
 */
public class Mold_Monitoring_Pie_Chart extends BaseGwActive {

    //region Variable

    //endregion

    public static final int[] COLORFUL_COLORS_CHART = {
            Color.rgb(69, 113, 50),Color.rgb(224, 157, 2),Color.rgb(162, 16, 21),Color.rgb(255, 255, 255)
    };

    public static final int[] COLORFUL_COLORS_LINE = {
            Color.rgb(69, 113, 50)
    };
    public static final int[] COLORFUL_COLORS_LINE_STORE = {
            Color.rgb(224, 157, 2)
    };
    public static final int[] COLORFUL_COLORS_PROBLEM = {
            Color.rgb(162, 16, 21)
    };
    public static final int[] COLORFUL_COLORS_WH = {
            Color.rgb(255, 255, 255)
    };
    public static final int[] COLORFUL_COLORS_HOLOR = {
            Color.rgb(69, 72, 64)
    };
    // we're going to display pie chart for smartphones martket shares
    private int[] _yData = { 6, 7, 26, 4 };



   /// row.addView(makeTableColWithText(df.format(percent) + "%", WIDTH_QCResult, fixedRowHeight, 1));



    private float[] yData = { (float)(6*100)/(float)43, (float)(7*100)/(float)43, (float)(26*100)/(float)43, (float)(4*100)/(float)43 };
    private String[] xData = { "Line", "Line Store", "Problem", "Warehouse Location" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mold_monitoring_3);

        PieChart pieChart = (PieChart) findViewById(R.id.chart);

        ArrayList<Entry> entries = new ArrayList<Entry>();
        entries.add(new Entry(yData[0], 0));
        entries.add(new Entry(yData[1], 1));
        entries.add(new Entry(yData[2], 2));
        entries.add(new Entry(yData[3], 3));



        //Giang

        // enable hole and configure
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleRadius(20);
        pieChart.setHoleColor(Color.TRANSPARENT);
        pieChart.setTransparentCircleRadius(10);
        pieChart.setCenterText("Monthly Quality              Report");
        // enable rotation of the chart by touch
        pieChart.setRotationAngle(0);
        pieChart.setRotationEnabled(true);

        // customize legends
        Legend l = pieChart.getLegend();
        l.setPosition(LegendPosition.RIGHT_OF_CHART);
        l.setXEntrySpace(7);
        l.setYEntrySpace(5);
        l.setTextSize(17);

        //End Giang
        pieChart.setBackgroundColor(Color.parseColor("#b1b7a3"));
        pieChart.animateY(1500);


        pieChart.setCenterTextSize(16);

   //     pieChart.saveToGallery("/sd/mychart.jpg", 85);
    }


}
