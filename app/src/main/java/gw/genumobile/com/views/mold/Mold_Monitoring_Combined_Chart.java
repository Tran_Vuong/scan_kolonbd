package gw.genumobile.com.views.mold;


//RI20160406-001

import android.graphics.Color;
import android.os.Bundle;


import com.github.mikephil.charting.charts.CombinedChart;


import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.R;


/**
 *
 */
public class Mold_Monitoring_Combined_Chart extends BaseGwActive {

    //region Variable

    //endregion
    private CombinedChart mChart;
    private final int itemcount = 13;
    int ORANGE = Color.rgb(99, 60, 49);
    int BLUE = Color.rgb(49, 88, 99);
    double[] LineNumber={0.5, 0.8, 0.7, 0.7, 0.7, 0.6, 0.6, 0.5, 0.5, 0.4, 0.4, 0.3, 0.2};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mold_monitoring_combined_chart);


    }
}
