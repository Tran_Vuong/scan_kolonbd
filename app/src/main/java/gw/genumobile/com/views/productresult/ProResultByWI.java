package gw.genumobile.com.views.productresult;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Locale;

import gw.genumobile.com.R;
import gw.genumobile.com.views.gwFragment;

public class ProResultByWI extends gwFragment {
    int target = 0, prod = 0, balance = 0, plan_prod = 0;
    String prod_time = "",line_group_pk = "", line_pk = "",lst_WI="";
    String line_detail[] = new String[0];
    View rootView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public  View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        setLanguageApplication();
        rootView = inflater.inflate(R.layout.activity_pro_result_by_wi, container, false);
        line_detail = this.getArguments().getStringArray("line");
        // line_detail = getIntent().getStringArrayExtra("line");
        target = line_detail[1].equals("") ? 0 : Integer.parseInt(line_detail[1]);
        prod = line_detail[3].equals("") ? 0 : Integer.parseInt(line_detail[3]);
        balance = line_detail[4].equals("") ? 0 : Integer.parseInt(line_detail[4]);
        plan_prod = line_detail[5].equals("") ? 0 : Integer.parseInt(line_detail[5]);
        lst_WI = line_detail[7];
        line_pk = line_detail[8];
      //  prod_time = line_detail[2];
        return rootView;
    }

    protected void setLanguageApplication(){

        String strLang = "en";
        String languageApp = appPrefs.getString("language", "");
        if(languageApp!=""){
            strLang = languageApp;
        }


        Resources res = this.getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(strLang.toLowerCase());
        res.updateConfiguration(conf, dm);

        //this.setContentView(activity);
    }
}
