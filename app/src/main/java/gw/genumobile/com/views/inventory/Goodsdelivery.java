package gw.genumobile.com.views.inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import gw.genumobile.com.interfaces.GridListView;
import gw.genumobile.com.R;
import gw.genumobile.com.models.Config;
import gw.genumobile.com.services.UploadAsyncTask;

public class Goodsdelivery extends Activity {
	
	private int[] colors = new int[] { 0x30FF0000, 0x300000FF };
	private String USER="Android";
	
	public Editable str_scan;
	private EditText edittext;
	
	String _drCurrentScan = null,lbl_SlipNo1_Tag="";
	
	String P_ITEM_BC,P_ITEM_CODE,P_ITEM_NAME,P_TR_LOT_NO,P_TR_QTY,P_SLIP_NO,P_TR_ITEM_PK,P_REQ_D_PK,P_WH_PK,P_TR_WH_IN_NAME;
	int cnt=0;
	String _Scan_Type = "3";//gd request
	SQLiteDatabase db=null;
	SQLiteDatabase mydb=null;
	Cursor cursor;
	SimpleCursorAdapter adapter;
	
	SQLiteDatabase db1 = null;
	Cursor cursor1;

	//--grid gridlayout
	GridView gridView;
	ListView lv1;
	Button btnDelete,btnClose;
	Button btnUploadGood;
	TextView txtWh_Pk,txtWh_Name, txtTotalLabel;
	LinearLayout Linear;
	EditText bc1,tr_qty,msg_error; 
	EditText bc2,reqNo,code,name,lotNo,req_qty,reqBal2;
	String CREATE_TABLE_DUAL;

	String data[]=new String [0];
	int count;
	ProgressDialog progressBar;
	private int progressBarStatus = 0;
	private Handler progressBarHandler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_goodsdelivery);
		
		//OnCreateTableINV_TR();
		
		btnDelete = (Button) findViewById(R.id.btn_delete);
        btnClose = (Button) findViewById(R.id.btnClose_Good);
        btnUploadGood = (Button) findViewById(R.id.btnUploadGood);
        
		bc1 =(EditText)findViewById(R.id.edit_Bc1);
		msg_error=(EditText)findViewById(R.id.txt_error);
		 
		 bc2 =(EditText)findViewById(R.id.edit_Bc2);
		 reqNo =(EditText)findViewById(R.id.edit_reqNo);
		 code =(EditText)findViewById(R.id.edit_Code);
		 name =(EditText)findViewById(R.id.edit_Name);
		 lotNo =(EditText)findViewById(R.id.edit_LotNo);
		 req_qty =(EditText)findViewById(R.id.edit_reqBal1);
		 reqBal2 =(EditText)findViewById(R.id.edit_ReqBal2);

		bc1.findFocus();
		str_scan=bc1.getText();
		
//		btnDelete.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				// do something when the button is clicked
//				msg_error.setTextColor(Color.RED);
//				msg_error.setTextSize((float) 16.0);
//				P_ITEM_BC = bc2.getText().toString();
//				if (P_ITEM_BC.length() < 1) {
//					msg_error.setText("Please select B/C!:");
//				} else {
//					OnDeleteBC(P_ITEM_BC);
//					OnShowGW_Header();
//					OnShowGW();
//					msg_error.setText("Delete data sucess."+ P_ITEM_BC.toString());
//
//				}
//			}
//
//		});
		btnClose.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// do something when the button is clicked
				finish();
				System.exit(0);
			}

		});
//		addKeyListener();
		bc1.setOnKeyListener(new OnKeyListener() {
			 public boolean onKey(View v, int keyCode, KeyEvent event) {
				 if (keyCode == KeyEvent.KEYCODE_ENTER) 
			     { 
		        	if (event.getAction() == KeyEvent.ACTION_DOWN)
		            {
		        		msg_error =(EditText)findViewById(R.id.txt_error);
		        		msg_error.setTextColor(Color.RED);
		        		msg_error.setTextSize((float) 16.0);
			    		if(bc1.getText().toString().equals(""))
			    		{
			    			msg_error.setText("Pls Scan barcode!");		    			
			            	bc1.requestFocus();
			    		}
			    		else
			    		{
			    			OnSaveData();
			    		}
		            }
		        	return true;//important for event onKeyDown
			     }
				 if (keyCode == KeyEvent.KEYCODE_BACK) {
	                // this is for backspace
					bc1.clearFocus();
					finish();
					System.exit(0);
	                //Log.e("IME_TEST", "BACK VIRTUAL");
		         }
			     return false;
			 }
			  
		});
		
		OnShowGW_Header();
		OnShowGW();
        
	}
	///////////////////////////////////////////////////////////
//	public void OnCreateTableINV_TR()
//    {
//		db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
//        
//		String CREATE_INV_TR = "CREATE TABLE IF NOT EXISTS INV_TR ( " +
//                "PK INTEGER PRIMARY KEY AUTOINCREMENT, " + 
//                "TLG_POP_LABEL_PK INTEGER, "+
//                "TR_WAREHOUSE_PK INTEGER, "+
//                "TR_LOC_ID TEXT, "+
//                "TR_QTY INTEGER, "+
//                "TR_LOT_NO TEXT, "+ 
//                "TR_TYPE TEXT, "+
//                "ITEM_BC TEXT, "+
//                "TR_DATE TEXT, "+
//                "TR_ITEM_PK INTEGER, "+
//                "TR_LINE_PK INTEGER, "+
//                "ITEM_CODE TEXT, "+
//                "ITEM_NAME TEXT, "+
//                "ITEM_TYPE TEXT, "+
//                "TLG_GD_REQ_D_PK INTEGER, "+
//                "SLIP_NO TEXT )";
//        db.execSQL(CREATE_INV_TR);
//        
//        db.close();
//    }
	////////////////////////////////////////////////
	//
	///////////////////////////////////////////////
	public void onClickSearch(View view) throws InterruptedException,ExecutionException {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		cursor = db.rawQuery("SELECT  ITEM_CODE,count(ITEM_CODE) as COUNT_ITEM,SUM(TR_QTY) AS QTY FROM INV_TR where TR_TYPE='3' GROUP BY ITEM_CODE ",null);// TLG_LABEL
		int count = cursor.getCount();
		String str="";
		if (cursor != null && cursor.moveToFirst()) {
			str="ITEM \t\t\t COUNT  \t\t\t QTY\n----------------------------------------------------------------\n";
			for (int i = 0; i < count; i++) {
				str=str+cursor.getString(cursor.getColumnIndex("ITEM_CODE"))+"\t\t\t\t "+cursor.getString(cursor.getColumnIndex("COUNT_ITEM"))+"\t\t\t\t "+cursor.getString(cursor.getColumnIndex("QTY"))+"\n";
				str=str+"----------------------------------------------------------------\n";
				cursor.moveToNext();
			}
		}else{
			str="Not found data.";
		}
		cursor.close();
		db.close(); 
		//Dialog
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);		 
		// Setting Dialog Title
		alertDialog.setTitle("List Item ...");
		// Setting Dialog Message
		alertDialog.setMessage(str); 
		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.cfm_diagram); 
		
		// Setting Negative "NO" Button
		alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
			// Write your code here to invoke NO event
			//Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
			dialog.cancel();
			}
		}); 
		// Showing Alert Message
		alertDialog.show();
			
	
	}
	////////////////////////////////////////////////
	//
	///////////////////////////////////////////////
	public void onListGrid(View view) throws InterruptedException,ExecutionException {
		Intent openNewActivity = new Intent(view.getContext(), GridListView.class);
		//send data into Activity
		openNewActivity.putExtra("type", "3");
		startActivity(openNewActivity);
	}
	////////////////////////////////////////////////
	//
	///////////////////////////////////////////////
	public void Delete(View view) throws InterruptedException,ExecutionException {
		
		msg_error.setTextColor(Color.RED);
		msg_error.setTextSize((float) 16.0);
		P_ITEM_BC = bc2.getText().toString();
		if (P_ITEM_BC.length() < 1) {
			msg_error.setText("Please select B/C!:");
		} else {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);		 
			// Setting Dialog Title
			alertDialog.setTitle("Confirm Delete...");
			// Setting Dialog Message
			alertDialog.setMessage("Are you sure you want to delete item B/C: \n"+P_ITEM_BC+"?"); 
			// Setting Icon to Dialog
			alertDialog.setIcon(R.drawable.cfm_del); 
			// Setting Positive "Yes" Button
			alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) { 
				// Write your code here to invoke YES event
				OnDeleteBC(P_ITEM_BC);
				bc2.getText().clear();
				code.getText().clear();
				name.getText().clear();
				lotNo.getText().clear();
				//tr_qty.getText().clear();
				msg_error.setText("Delete data sucess."+ P_ITEM_BC.toString());
				
				OnShowGW();
				Toast.makeText(getApplicationContext(), "Delete success..", Toast.LENGTH_SHORT).show();
			  }
			});
			// Setting Negative "NO" Button
			alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// Write your code here to invoke NO event
				//Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
				dialog.cancel();
			 }
			}); 
			// Showing Alert Message
			alertDialog.show();
//
		}
		

	}
	//////////////////////////////////////////////////////////
	//-------upload-----------------
	public void  onClickUploadDataGoods(View view) throws InterruptedException, ExecutionException{
		//Read file config
   	  	boolean read= Config.ReadFileConfig();
   	  	USER=Config.USER;
   	  	//
		db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
        cursor = db.rawQuery("select TLG_POP_LABEL_PK, TR_WAREHOUSE_PK,TR_LOC_ID, TR_QTY , TR_LOT_NO, TR_TYPE, ITEM_BC, TR_DATE, TR_ITEM_PK, TR_LINE_PK, ITEM_CODE, ITEM_NAME,ITEM_TYPE,PK,TLG_GD_REQ_D_PK, SLIP_NO  from INV_TR",null);
   	  	System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
		if (cursor.moveToFirst()) {
			// INSERT DATA INTO DB
    		// Show dialog Yes No
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);		 
	        // Setting Dialog Title
	        alertDialog.setTitle("Confirm Upload...");
	        // Setting Dialog Message
	        alertDialog.setMessage("Are you sure you want to upload data to server?"); 
	        // Setting Icon to Dialog
	        alertDialog.setIcon(R.drawable.cfm_up); 
	        // Setting Positive "Yes" Button
	        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int which) { 
	            // Write your code here to invoke YES event
	            	//set value message		
	        		msg_error = (EditText) findViewById(R.id.txt_error);
	        		msg_error.setTextColor(Color.RED);
	        		msg_error.setText("");
	        		
	        		data=new String [cursor.getCount()];
	           	  	int j=0;
	        		do {
	                       // labels.add(cursor.getString(1));
	                  	  String para ="";
	          	          for(int i=0; i < cursor.getColumnCount();i++)
	          	 			{
	          		        		if(para.length() <= 0)
	          		        		{
	          		        			if(cursor.getString(i)!= null)	  	        			
	          		        				para += cursor.getString(i);
	          		        			else
	          		        				para += "0";	  	        			
	          		        		}
	          		        		else
	          		        		{
	          		        			if(cursor.getString(i)!= null)
	          		        				para += "|!" + cursor.getString(i);
	          		        			else
	          		        				para += "|!";	  	        			
	          		        		}
	          	 				
	          	 			}
	          	            para += "|!"+USER;

	          		        //System.out.print("\n\n ******para: "+para);
	                  	    data[j++]=para;
	                  	  
	                    } while (cursor.moveToNext());
	        		 //////////////////////////
	        		cursor.close();   
	               db.close();
	               doStart();
	               
	               	//clear data
	       			//clearAllData();
	       			//OnShowGW();
	               
	            }
	        });
	        // Setting Negative "NO" Button
	        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            // Write your code here to invoke NO event
	            	//Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
	            	 //////////////////////////
	        	      cursor.close();   
	        	      db.close();
	            	dialog.cancel();		
	            }
	        }); 
	        // Showing Alert Message
	        alertDialog.show();
	        
	       
		}else{
			cursor.close();   
  	        db.close();
			msg_error =(EditText)findViewById(R.id.txt_error);
			msg_error.setTextColor(Color.RED);
			msg_error.setTextSize((float) 16.0);
			msg_error.setText("Data is empty!");
		}
		
	}
	
	private void doStart()
	{
		  System.out.print("\n\n**********doStart********\n\n\n");
		 
		  UploadAsyncTask task = new UploadAsyncTask(this);
      	  task.execute(data);
	}
	
	public void clearData(){
		
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		db.delete("INV_TR", null, null);
		db.close();
	}
	public void clearAllData(){
		CheckBox ck = (CheckBox)findViewById(R.id.ckbClearData);
		if(ck.isChecked() == true)
		{
			db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
			//db.delete("INV_TR", null, null);
			db.execSQL("DELETE from INV_TR where DEL_IF != '0'");
			db.close();
		}
		else
			Log.e("ck.isChecked() == else: ","ck.isChecked() == else");
		
	}
	//////////////////////////////////////////////////////////
	// Delete BC
	public void OnDeleteBC(String p_pk) {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		
		db.execSQL("DELETE from INV_TR where ITEM_BC = '" + p_pk + "'");
		//System.out.println("delete ok");
		db.close();
	}

	//////////////////////////////////////////////////////////
	//save data in control
	public void SaveData(View view)  throws InterruptedException,ExecutionException
	{		
		OnSaveData();
		EditText bc2 = (EditText) findViewById(R.id.edit_Bc2);
		msg_error = (EditText) findViewById(R.id.txt_error);
		if (bc2.length() < 1) {
			msg_error.setText("Please, scan one barcode to update.");
			msg_error.setTextColor(Color.RED);
			msg_error.setTextSize((float) 16.0);
			return;
		} else {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);		 
	        // Setting Dialog Title
	        alertDialog.setTitle("Confirm update...");
	        // Setting Dialog Message
	        alertDialog.setMessage("Are you sure you want to update item B/C: \n"+P_ITEM_BC+"?"); 
	        // Setting Icon to Dialog
	        alertDialog.setIcon(R.drawable.cfm_save); 
	        // Setting Positive "Yes" Button
	        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int which) { 
	            // Write your code here to invoke YES event
	            	OnUpdateData();
	    			OnShowGW();
	            	Toast.makeText(getApplicationContext(), "Update success..", Toast.LENGTH_SHORT).show();
	            }
	        });
	        // Setting Negative "NO" Button
	        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            // Write your code here to invoke NO event
	            	dialog.cancel();
	            }
	        }); 
	        // Showing Alert Message
	        alertDialog.show();
		}
	}
	// update data in control
		public void OnUpdateData() {
			db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
			bc1 =(EditText)findViewById(R.id.edit_Bc1);
			String str_scan_bc=bc1.getText().toString();
			int P_PK= Check_Exist_PK(str_scan_bc);
			 EditText bc2 =(EditText)findViewById(R.id.edit_Bc2);
			 //EditText reqNo =(EditText)findViewById(R.id.edit_reqNo);
			 EditText code =(EditText)findViewById(R.id.edit_Code);
			 EditText name =(EditText)findViewById(R.id.edit_Name);
			 EditText LotNo =(EditText)findViewById(R.id.edit_LotNo);
			 EditText req_qty =(EditText)findViewById(R.id.edit_reqBal1);
			 EditText reqBal2 =(EditText)findViewById(R.id.edit_ReqBal2); 
			 tr_qty =(EditText)findViewById(R.id.edit_Qty);
			 
			// get data input control to var then update db
			 P_ITEM_BC  = bc2.getText().toString();
			 P_ITEM_CODE= code.getText().toString();
			 P_ITEM_NAME= name.getText().toString();
			 P_TR_LOT_NO= LotNo.getText().toString();
			 P_TR_QTY   =tr_qty.getText().toString();
			 db.execSQL("update INV_TR set TR_QTY='"+P_TR_QTY+"' where pk ='"+P_PK+"' "); 
			 db.close(); 
		}
	//----------------------------------------------------------
	///
	//
	public void OnSaveData()
	{
		bc1 =(EditText)findViewById(R.id.edit_Bc1);
		EditText reqNo =(EditText)findViewById(R.id.edit_reqNo);
		msg_error =(EditText)findViewById(R.id.txt_error);
		msg_error.setTextColor(Color.RED);
		msg_error.setTextSize((float) 16.0);
		if(bc1.getText().toString().equals(""))
		{
			msg_error.setText("Pls Scan barcode!");
			return;
        	//bc1.requestFocus();
		}
		
		String str_scan=bc1.getText().toString().toUpperCase();
		int l_lenght=bc1.getText().toString().length();
		String str_req_no=bc1.getText().toString().substring(1, l_lenght);
		System.out.println("str_req_no " +str_req_no);
		try
        {
            if (str_scan.equals(""))
            {
            	msg_error.setText("Pls Scan barcode!");
            	//bc1.requestFocus();
                //txt_BC.SelectAll();
                return;
            }
            //scan slip no
            if (lbl_SlipNo1_Tag.equals("") && str_scan.indexOf("S") == 0)
            {
            	//System.out.println("str_req_no " +str_req_no);
            	boolean l_HaveData = OnLoadSlipNo(str_req_no);
                //System.out.println("slip no 33:"+l_HaveData);
                if (!l_HaveData)
                {
                    msg_error.setText("Slip No is not found!");
                   // bc1.getText().clear();
                	bc1.setText("");
                	bc1.requestFocus();
                	lbl_SlipNo1_Tag=null;
                    return;
                }
                else
                {
                	//bc1.setText("");
                	bc1.getText().clear();
                	bc1.requestFocus();
                	msg_error.setText("");
                	//ClearControl();
                    return;
                }
            }
            //scan already slip no
            if (lbl_SlipNo1_Tag.equals(""))
            {
                msg_error.setText("Pls Scan Slip No!");
                bc1.getText().clear();
                //bc1.setText("");
            	bc1.requestFocus();
                //txt_BC.SelectAll();
                return;
            }
            System.out.println("str_scan " +str_scan);
            System.out.println("lbl_SlipNo1_Tag " +lbl_SlipNo1_Tag);
            
            /*db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
    		String SQL_SEL_LABEL_REQ = "SELECT A.ITEM_BC "+
                                           " FROM TLG_LABEL AS A INNER JOIN"+
                                           " TLG_GD_REQ AS B ON A.TLG_IT_ITEM_PK = B.REQ_ITEM_PK";//+
                                          // " WHERE     (A.ITEM_BC = '"+l_bc_item+"') AND (B.SLIP_NO = '"+l_slip_no+"') ";//='"+l_bc_item+"'
    		// " WHERE     (A.ITEM_BC = '"+l_bc_item+"') AND (B.SLIP_NO = '"+l_slip_no+"') ";//='"+l_bc_item+"'
    		cursor = db.rawQuery(SQL_SEL_LABEL_REQ, null);
    		
    		System.out.println("data all label :"+cursor.getCount());
            db.close();*/
            boolean l_rs = OnLoadBC(str_scan, lbl_SlipNo1_Tag);
            
            if (l_rs)//scan barcode,has data then 
            {
            	int cnt=Check_Exist_PK(str_scan);//check barcode exist in data base
            	if(cnt>0)//exist data
            	{
            		msg_error.setText("Please,scan barcode other.So barcode "+str_scan+" existed!!");
            		bc1.getText().clear();
            		bc1.requestFocus();
            	}
            	else//save data scanned
            	{
                	if(OnSave())
                	{
                		msg_error.setText("Save success.");
                		bc1.getText().clear();
                		bc1.requestFocus();
                		//OnShowGW_Header();
                		OnShowGW();
                	}
            	}
            }
            else//barcode error,or barcode not yet download to mobile
            {                	
        		msg_error.setText("Item Bacode "+str_scan+ " has not exist.");
        		//bc1.getText().clear();
        		bc1.setText("");
        		bc1.requestFocus();
            }


        }
        catch (Exception ex)
        {
        	//save data error write to file log
        	
        }	
	}
	//end save data in control
	private boolean OnSave()
    {
		db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);	
        try
        {
        	 bc1 =(EditText)findViewById(R.id.edit_Bc1);
    		 EditText bc2 =(EditText)findViewById(R.id.edit_Bc2);
    		 EditText reqNo =(EditText)findViewById(R.id.edit_reqNo);
    		 EditText code =(EditText)findViewById(R.id.edit_Code);
    		 EditText name =(EditText)findViewById(R.id.edit_Name);
    		 EditText LotNo =(EditText)findViewById(R.id.edit_LotNo);
    		 EditText reqBal1 =(EditText)findViewById(R.id.edit_reqBal1);
    		 EditText reqBal2 =(EditText)findViewById(R.id.edit_ReqBal2); 
    		 tr_qty =(EditText)findViewById(R.id.edit_Qty);
    		 
    		 TextView wh_pk=(TextView)findViewById(R.id.txt_WH_PK);
    		 TextView wh_name=(TextView)findViewById(R.id.txt_WH_NAME);
    		 //bc2.setText(bc1.getText());
    		 
    		// get data input control to var then insert db	
    		 P_SLIP_NO  = reqNo.getText().toString();
    		 P_ITEM_BC  = bc2.getText().toString();
    		 P_ITEM_CODE= code.getText().toString();
    		 P_ITEM_NAME= name.getText().toString();
    		 P_TR_LOT_NO= LotNo.getText().toString();
    		 P_TR_QTY   =tr_qty.getText().toString();
    		 P_TR_ITEM_PK=reqBal2.getText().toString();
    		 P_WH_PK= wh_pk.getText().toString();
    		 P_TR_WH_IN_NAME= wh_name.getText().toString();
    		 
    		 db.execSQL("INSERT INTO INV_TR(ITEM_BC,ITEM_CODE,ITEM_NAME,TR_LOT_NO,TR_QTY,SLIP_NO,TR_TYPE,TR_ITEM_PK,TLG_GD_REQ_D_PK,TR_WAREHOUSE_PK,TR_WH_IN_NAME) "
    		 +"VALUES('"
    		 +P_ITEM_BC+"','"+
    		 P_ITEM_CODE+"','"
    		 +P_ITEM_NAME+"','"
    		 +P_TR_LOT_NO+"','"
    		 +P_TR_QTY+"','"
    		 +P_SLIP_NO+"','"
    		 +_Scan_Type+"','"
    		 +P_TR_ITEM_PK+"','"
    		 +P_REQ_D_PK+"','"
    		 +P_WH_PK+"','"
    		 +P_TR_WH_IN_NAME
    		 +"');");
    		 //bc1.setText("");
    		 //bc1.requestFocus();
    		 //msg_error.setText("Save Error!!!");
    		 db.close();
            return true;
        }
        catch (Exception ex)
        {
            msg_error.setText("Save Error!!!");
            //return false;
        }
        return true;
    }
	
	/////////////////////
	public boolean OnLoadBC(String l_bc_item,String l_slip_no)
    {
		boolean l_RS = false;
		//String l_bc_item = null,l_slip_no=null;
		bc1 =(EditText)findViewById(R.id.edit_Bc1);
		
		//l_bc_item=bc1.getText().toString();

		EditText reqNo =(EditText)findViewById(R.id.edit_reqNo);
		//l_slip_no=reqNo.getText().toString();
		
		System.out.println("Onload BC");
		
		db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		String SQL_SEL_LABEL_REQ = "SELECT A.ITEM_BC,A.ITEM_CODE, A.ITEM_NAME, A.ITEM_TYPE, A.LOT_NO, A.LABEL_QTY, A.PK AS LABEL_PK, A.TLG_IT_ITEM_PK AS TR_ITEM_PK, A.LABEL_UOM, B.SLIP_NO,"+ 
                                              "B.REQ_QTY, B.OUT_WH_PK,B.TLG_GD_REQ_D_PK, B.TLG_GD_REQ_M_PK"+
                                       " FROM TLG_LABEL AS A INNER JOIN"+
                                       " TLG_GD_REQ AS B ON A.TLG_IT_ITEM_PK = B.REQ_ITEM_PK"+
                                       " WHERE     (A.ITEM_BC = '"+l_bc_item+"') AND (B.SLIP_NO = '"+l_slip_no+"') ";//='"+l_bc_item+"'
		// " WHERE     (A.ITEM_BC = '"+l_bc_item+"') AND (B.SLIP_NO = '"+l_slip_no+"') ";//='"+l_bc_item+"'
		cursor = db.rawQuery(SQL_SEL_LABEL_REQ, null);
		System.out.println("Onload BC SQL selete OK");
        int i = 0;
        
		  EditText bc2 =(EditText)findViewById(R.id.edit_Bc2);
		  //EditText reqNo =(EditText)findViewById(R.id.edit_reqNo);
		  EditText code =(EditText)findViewById(R.id.edit_Code);
		  EditText name =(EditText)findViewById(R.id.edit_Name);
		  EditText LotNo =(EditText)findViewById(R.id.edit_LotNo);
		  EditText req_qty =(EditText)findViewById(R.id.edit_reqBal1);
		  EditText reqBal2 =(EditText)findViewById(R.id.edit_ReqBal2);
		  tr_qty =(EditText)findViewById(R.id.edit_Qty);
		  
		  TextView wh_Pk = (TextView) findViewById(R.id.txt_WH_PK);
		  TextView wh_Name = (TextView) findViewById(R.id.txt_WH_NAME);
		 
       if(cursor != null && cursor.moveToFirst())
        {		          	
   			bc2.setText(cursor.getString(cursor.getColumnIndex("ITEM_BC")));
   			code.setText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")));
   			name.setText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")));
   			tr_qty.setText(cursor.getString(cursor.getColumnIndex("LABEL_QTY")));
   			LotNo.setText(cursor.getString(cursor.getColumnIndex("LOT_NO")));
   			req_qty.setText(cursor.getString(cursor.getColumnIndex("REQ_QTY")));
   			reqBal2.setText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
   			P_REQ_D_PK=cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_D_PK"));
   			wh_Pk.setText(cursor.getString(cursor.getColumnIndex("OUT_WH_PK")));
   			
   			int pkwh=Integer.parseInt(wh_Pk.getText().toString());
   			List<String> ListWH = GetDataWH(pkwh);
   			//System.out.println("ware house name:"+ListWH.get(0));
   			wh_Name.setText(ListWH.get(0));
	        cursor.moveToNext();
	        cursor.close();
	        l_RS= true;
	     }	            
		db.close();
        return l_RS;
    }
	
	public List<String> GetDataWH(int p_pk) {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		List<String> labels = new ArrayList<String>();
		// Select All Query tlg_in_warehouse
		String selectQuery = "SELECT  pk,wh_id||'-'||wh_name,wh_name FROM tlg_in_warehouse where pk="+p_pk+" order by  wh_id";// tlg_in_warehouse
		// db = this.getReadableDatabase();
		cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor != null && cursor.moveToFirst()) {
			do {
				labels.add(cursor.getString(2));
			} while (cursor.moveToNext());
		}
		// closing connection
		cursor.close();
		db.close();
		return labels;
	}
	////////////////////////
    //check data exist
	 public  int Check_Exist_PK( String l_bc_item) 
	 {
		 db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		 
		 bc1 =(EditText)findViewById(R.id.edit_Bc1);
		 String countQuery = "SELECT PK FROM INV_TR where tr_type='3' and ITEM_BC ='"+l_bc_item+"' ";//='"+l_bc_item+"'
         int Check_Exist_PK=0;

        cursor = db.rawQuery(countQuery, null);
        //Check_Exist_PK=cursor.getCount();
        if(cursor != null && cursor.moveToFirst()){	        	      	        
        	Check_Exist_PK = Integer.parseInt(cursor.getString(cursor.getColumnIndex("PK")));
	            cursor.moveToNext();
	        cursor.close();
	            return Check_Exist_PK;
	    }
        db.close();
	    return Check_Exist_PK;

    }
	////////On Load Slip No/////////
	private boolean OnLoadSlipNo( String str_scan_req)
    {
        boolean l_RS = false;
        bc1 =(EditText)findViewById(R.id.edit_Bc1);
        EditText reqNo =(EditText)findViewById(R.id.edit_reqNo);
        tr_qty =(EditText)findViewById(R.id.edit_Qty);
        //l_bc_item=bc1.getText().toString();
        try
        {
        	db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
   		 	String SQL_SEL_GD_REQ = "SELECT DISTINCT TLG_GD_REQ_M_PK, SLIP_NO, SUM(REQ_QTY) AS REQ_QTY,OVER_RATIO FROM TLG_GD_REQ WHERE SLIP_NO ='"+str_scan_req+"' GROUP BY TLG_GD_REQ_M_PK, SLIP_NO,OVER_RATIO  ";//='"+l_bc_item+"'
   		 	 		 	
   		    cursor = db.rawQuery(SQL_SEL_GD_REQ, null);

            if (cursor.getCount() < 1)
            {
            	msg_error.setText("No have data! Pls Check download form again!!");
                l_RS = false;
            }
            if(cursor != null && cursor.moveToFirst())
            {	        	      	        
            	reqNo.setText(cursor.getString(cursor.getColumnIndex("SLIP_NO")));
            	lbl_SlipNo1_Tag=reqNo.getText().toString();
            	
            	System.out.println("******lbl_SlipNo1_Tag:"+lbl_SlipNo1_Tag);
            	
            	tr_qty.setText(cursor.getString(cursor.getColumnIndex("REQ_QTY")));
    	        cursor.moveToNext();
    	        cursor.close();
    	        l_RS = true;
    	     }
            db.close();
        }
        catch (Exception ex)
        {
           l_RS = false;
           bc1.getText().clear();
           bc1.requestFocus();
        }
        return l_RS;
    }
	//////////////////////////////////////////////////////////
	@SuppressWarnings("deprecation")
	public void OnShowGW_Header()// show data gridview
	{		
		TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20,20};
        int[] scrollableColumnWidths = new int[]{20, 20, 20, 30, 40};
        int fixedRowHeight = 40;
        int fixedHeaderHeight = 40;

        TableRow row = new TableRow(this);
        TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part1);
        scrollablePart.removeAllViews();//remove all view child
        
        row = new TableRow(this);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(colors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText("ITEM_BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("TR_QTY", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("TR_LOT_NO", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("TR_WH_IN_NAME", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("LINE_NAME", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("ITEM_CODE", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("ITEM_NAME", scrollableColumnWidths[3], fixedHeaderHeight));
        scrollablePart.addView(row);              
	}
	//end get data return array--use array set to grid view
	@SuppressWarnings("deprecation")
	public void OnShowGW()//show data gridview
	{	
		TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20,20};
        int[] scrollableColumnWidths = new int[]{20, 20, 20, 30, 40};
        int fixedRowHeight = 40;
        int fixedHeaderHeight = 60;

        TableRow row = new TableRow(this);
        TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
        scrollablePart.removeAllViews();//remove all view child
        ///
        db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
        cursor1 = db.rawQuery("SELECT PK FROM INV_TR where TR_TYPE='3' order by PK desc ",null);// TLG_LABEL
		cursor = db.rawQuery("SELECT ITEM_BC, TR_QTY, TR_LOT_NO,TR_WH_IN_NAME,LINE_NAME,ITEM_CODE,ITEM_NAME FROM INV_TR where TR_TYPE='3' order by PK desc LIMIT 20",null);// TLG_LABEL
		int l_total = cursor1.getCount();
		int count = cursor.getCount();
		
		txtTotalLabel = (TextView) findViewById(R.id.txtTotalLabel);
		txtTotalLabel.setText("Total :" + l_total + " (Labels.)");
		txtTotalLabel.setTextColor(Color.BLUE);
		txtTotalLabel.setTextSize((float) 18.0);
		
		if (cursor != null && cursor.moveToFirst()) {
			for (int i = 0; i < count; i++) {
	    		row = new TableRow(this);
		        row.setLayoutParams(wrapWrapTableRowParams);
		        row.setGravity(Gravity.CENTER);
		        row.setBackgroundColor(Color.LTGRAY);
		        
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[1], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[3], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[3], fixedRowHeight));
		        
		        row.setOnClickListener(new OnClickListener() {

		            @Override
		            public void onClick(View v) {
		                TableRow tr1=(TableRow)v;
		                TextView tv1= (TextView)tr1.getChildAt(0);
		                int xx=tr1.getChildCount();
		                for (int i = 0; i < xx; i++) {
		                	TextView tv11= (TextView)tr1.getChildAt(i);
		                	tv11.setTextColor(-256);
		                }
		                //Toast.makeText(getApplicationContext(),tv1.getText().toString(),Toast.LENGTH_SHORT).show();
		                bc2 = (EditText) findViewById(R.id.edit_Bc2);
		                bc2.setText(tv1.getText().toString());
		            }
		        });
		        
		        scrollablePart.addView(row);
		        
				cursor.moveToNext();
			}
		}
		cursor1.close();
		cursor.close();
		db.close();     
	}
	//util method
    private TextView recyclableTextView;

	public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
		params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(this);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.BLACK);
        recyclableTextView.setTextSize(20);
        recyclableTextView.setBackgroundColor(-1);
        recyclableTextView.setPadding(0, 5, 0, 5);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }
    
    public TextView makeTableRowHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
    	int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
		params.setMargins(1, 1, 1, 1);
		recyclableTextView =new  TextView(this);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(-256);
        recyclableTextView.setTextSize(20);
        recyclableTextView.setBackgroundColor(colors[1]);
        recyclableTextView.setPadding(0, 5, 0, 5);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }
	//////////////////////////////////////////////////////////
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.goodsdelivery, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
