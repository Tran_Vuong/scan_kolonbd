package gw.genumobile.com.views;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.adapters.NavigationMenuAdapter;
import gw.genumobile.com.db.CSVFile;
import gw.genumobile.com.db.MySQLiteOpenHelper;
import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.models.gwform_info;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.views.gwcore.DebugActivity;
import gw.genumobile.com.views.inventory.MappingV2_Pop2;


public class gwMainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener,gwFragment.OnFragmentInteractionListener  {

    private static String TAG = gwMainActivity.class.getSimpleName();

    private Handler handler = new Handler();

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    public MySQLiteOpenHelper hp;
    SQLiteDatabase db;
    protected SharedPreferences appPrefs;
    public String bsUserPK="",bsUserID="",bsUserName="";
    public String getBsUserPK() {
        return bsUserPK;
    }
    public void setBsUserPK(String bsUserPK) {
        this.bsUserPK = bsUserPK;
    }
    public String getBsUserID() {
        return bsUserID;
    }
    public void setBsUserID(String bsUserID) {
        this.bsUserID = bsUserID;
    }

    public String getBsUserName() {
        return bsUserName;
    }
    public void setBsUserName(String bsUserName) {
        this.bsUserName = bsUserName;
    }
    private  List<NavigationMenuAdapter.Item> data = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gwmain);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        DrawerLayout mDrawerLayout =(DrawerLayout) findViewById(R.id.drawer_layout);
      //  load_menu();
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
        data =  drawerFragment.getData();
        hp=new MySQLiteOpenHelper(this);
        appPrefs = getSharedPreferences("myConfig", MODE_PRIVATE);
        String userInfo=appPrefs.getString("userInfo", "");
        String[] strArray = userInfo.split("\\|");
        setBsUserID(strArray[0]);
        setBsUserPK(strArray[1]);
        setBsUserName(strArray[2]);
        OnCreateTableINV_TR();
        // display the first navigation drawer view on app launch
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(TAG);
        if(fragment == null) {
            displayView(0);
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
        else    {


        }



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() <2) {
            //super.onBackPressed();
        //   return;
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        else {
            getSupportFragmentManager().popBackStackImmediate();
            FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1);
            String fragmentName =entry.getName();
          //  Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(fragmentName);
          //  String str= currentFragment.getTag();
          //  int title=Integer.parseInt(fragmentName);
            gwform_info finfo=null;
            try {
                finfo= gwform_info.valueOf(fragmentName);
            } catch (IllegalArgumentException iae) {
                finfo= gwform_info.valueOf("gw");
            }
            setRequestedOrientation(finfo.getValue().ori);
            getSupportActionBar().setTitle(finfo.Title());


            return;
        }
       // return;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_help )
        {
            Toast.makeText(getApplicationContext(), "Please contact Vinagenuwin.com!", Toast.LENGTH_LONG).show();
            return true;
        }
        else if (id == R.id.action_close) {
           if( getSupportFragmentManager().getBackStackEntryCount()>1)
           {

               new AlertDialog.Builder(this)
                       .setTitle("Genuwin")
                       .setMessage("Do you want to close this form?")
                       .setIcon(android.R.drawable.ic_dialog_alert)
                       .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int whichButton) {
                               getSupportFragmentManager().popBackStack();
                           }})
                       .setNegativeButton(android.R.string.no, null).show();
           }

        }

       else if(id == R.id.action_refresh){
            Toast.makeText(getApplicationContext(), "Search action is selected!", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.action_debug) {

            final Dialog dialog_confirm = new Dialog(this);
            dialog_confirm.setContentView(R.layout.dialog_confirm);
            dialog_confirm.setTitle("Confirm!!!");
            dialog_confirm.setCanceledOnTouchOutside(false);
            dialog_confirm.show();

            Button btn_Ok = (Button) dialog_confirm.findViewById(R.id.btn_OK);
            final EditText edt_confirm = (EditText) dialog_confirm.findViewById(R.id.edt_confirm);
            btn_Ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(edt_confirm.getText().toString().equals("vina@123")){
                        dialog_confirm.dismiss();
//                        Intent i = new Intent(getApplicationContext(), DebugActivity.class);
//                        i.putExtra("type","00" );
//                        i.putExtra("user", "admin");
//                        startActivity(i);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                FragmentManager fm = getSupportFragmentManager();
                                DialogFragment dialogFragment = new DebugActivity();
                                Bundle args = new Bundle();
                                //   args.putStringArray("para1",lableinfo);
                                //  args.putStringArray("LINE_DETAIL", line_detail);

                                dialogFragment.setArguments(args);
                                dialogFragment.setTargetFragment(fm.getFragments().get(0), 1);
                                dialogFragment.setCancelable(false);
                                dialogFragment.show(fm.beginTransaction(), "dialog");
                            }
                        });


                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Password is incorrect!!!",Toast.LENGTH_LONG).show();
                        edt_confirm.getText().clear();
                    }
                }
            });

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        gwFragment fragment = null;
        int idText=0;
        String tag="";
        gwform_info finfo=null;
      if(position==0) {
          fragment = new HomeFragment();
          idText= getResources().getIdentifier( "ic_home","string",getPackageName());
          tag= "HomeFragment";
          try {
              finfo= gwform_info.valueOf(tag);
          } catch (IllegalArgumentException iae) {
              finfo= gwform_info.valueOf("gw");
          }
          FragmentManager fragmentManager = getSupportFragmentManager();
          FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
          fragmentTransaction.replace(R.id.container_body, fragment,tag);
        //  fragmentTransaction.add(R.id.container_body, fragment,tag);

          fragmentTransaction.addToBackStack(tag);
          fragmentTransaction.commit();
          // set the toolbar title
          getSupportActionBar().setTitle(idText);
          finfo.Title(idText);
          TAG =tag;
      }
      if(data.get(position).type==0)
            return;
        Intent intent = new Intent();
        String class_name =  data.get(position).getClassID();
        if(class_name.equals("0")){
            return;
        }
        idText= getResources().getIdentifier( data.get(position).text,"string",getPackageName());
        if(idText==0)
            idText = getResources().getIdentifier("ic_menu_default","string",getPackageName());
         tag=class_name.split("\\.")[class_name.split("\\.").length-1];
        try {
            fragment= (gwFragment) Class.forName(class_name).newInstance();
        }   catch (Exception e){
            return;
        }

        if (fragment != null) {





            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
            boolean fragmentPopped=  fragmentManager.popBackStackImmediate(tag , 0);
          //  fragmentManager.getBackStackEntryAt()

            try {
                finfo= gwform_info.valueOf(tag);
            } catch (IllegalArgumentException iae) {
                finfo= gwform_info.valueOf("gw");
            }

            try {

                String strLang = "en";
                String languageApp = appPrefs.getString("language", "");
                if(languageApp!=""){
                    strLang = languageApp;
                }


                ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));

                String result[][] = networkThread.execute("1,lg_mpos_list_status," + tag + "|" + strLang).get();

                String data = "";
                for(int i = 0 ; i < result.length; i++){
                    data = data + result[i][0].toString() + ":\t" + result[i][1].toString()  + "\n";
                    data = data + "----------------------------------------------------------------------------------------------------" + "\n" ;
                }

                fragment.setDataStatus( data);
            }catch  (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }





            if (!fragmentPopped ) {

                finfo.Title(idText);
                setRequestedOrientation(finfo.getValue().ori);
                fragmentTransaction.replace(R.id.container_body, fragment, tag);
                // fragmentTransaction.add(R.id.container_body, fragment,tag);

                fragmentTransaction.addToBackStack(tag);
               // fragmentTransaction.addToBackStack(String.valueOf(idText));
                fragmentTransaction.commit();
               // fragmentManager.executePendingTransactions();
                // set the toolbar title

                if(data.get(position).getName().length() > 0 ){
                    getSupportActionBar().setTitle(data.get(position).getName());
                }else{
                    getSupportActionBar().setTitle(idText);
                }
                //getSupportActionBar().setTitle(idText);
                TAG = tag;
            }
            else
            {
                setRequestedOrientation(finfo.getValue().ori);


                if(data.get(position).getName().length() > 0 ){
                    getSupportActionBar().setTitle(data.get(position).getName());
                }else{
                    getSupportActionBar().setTitle(idText);
                }

            }
        }
    }
   public void showDialog(String formID) {

        DialogFragment newFragment = frGridListViewMid.newInstance(
                1,formID);
     //  newFragment.getDialog().getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);
        newFragment.show(getSupportFragmentManager().beginTransaction(), "dialog");
    }
   public  void ShowForm(String classForm,int title,Bundle bundle)
    {


        try {





            Fragment fragment = (Fragment) Class.forName(classForm).newInstance();
            fragment.setArguments(bundle);
           String tag=classForm.split("\\.")[classForm.split("\\.").length-1];
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left,R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container_body, fragment,tag);
           // fragmentTransaction.add(R.id.container_body, fragment,tag);
            fragmentTransaction.addToBackStack(String.valueOf(title));
            fragmentTransaction.commit();

            getSupportActionBar().setTitle(title);
            TAG =tag;

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
    public void  Goto_From(String classForm,int title, Build build)
    {
       // getSupportFragmentManager().popBackStack
    }
    public  void PopBackStack(){
        if(getSupportFragmentManager().getBackStackEntryCount() >=2) {
            getSupportFragmentManager().popBackStackImmediate();
            String fragmentName =getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
            gwform_info finfo=null;
            try {
                finfo= gwform_info.valueOf(fragmentName);
            } catch (IllegalArgumentException iae) {
                finfo= gwform_info.valueOf("gw");
            }
            setRequestedOrientation(finfo.getValue().ori);
            getSupportActionBar().setTitle(finfo.Title());
            TAG =fragmentName;
        }
    }
  public  void  LogOut(){
      Intent intent = new Intent(gwMainActivity.this,
              MainActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
  }

    public void AddEvenKeyboardHiddenWhenClickOutSide(View view) {

        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    HideKeyboard(gwMainActivity.this);
                    return false;
                }

            });
        }

        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                AddEvenKeyboardHiddenWhenClickOutSide(innerView);
            }
        }
    }
    public static void HideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
    }
    public  void alertToastShort(String alert){
        Toast toast=Toast.makeText(getApplicationContext(), alert, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
    public  void alertToastLong(String alert){
        Toast toast=Toast.makeText(getApplicationContext(), alert, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
    public void onPopAlert(String str_alert) {

        String str=str_alert;

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }
    public void OnCreateTableINV_TR() {
        try{

            db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
            // CREATE TABLE IF NOT EXISTS INV_TR
            // db.execSQL("DROP TABLE IF EXISTS INV_TR ");//khi chep file qua may
            // khac khoi tao lai table.

            if (appPrefs.getBoolean("ckbDropTable", Boolean.FALSE)) {
                //db.execSQL("DELETE FROM INV_TR;");
                db.execSQL("DROP TABLE IF EXISTS INV_TR ");
            }

            String CREATE_INV_TR = "CREATE TABLE IF NOT EXISTS INV_TR ( "
                    + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "TLG_POP_LABEL_PK INTEGER, "
                    + "TR_ITEM_PK INTEGER, "
                    + "TR_DATE TEXT, "
                    + "ITEM_CODE TEXT, "
                    + "ITEM_NAME TEXT, "
                    + "ITEM_BC TEXT, "
                    + "TR_QTY REAL, "
                    + "TR_LOT_NO TEXT, "
                    + "UNIT_PRICE REAL, "
                    + "UOM TEXT, "
                    + "ITEM_TYPE TEXT, "
                    + "TR_LINE_PK TEXT, "
                    + "LINE_NAME TEXT, "
                    + "TLG_IN_WHLOC_OUT_PK TEXT, "
                    + "TLG_IN_WHLOC_OUT_NAME TEXT, "
                    + "TLG_IN_WHLOC_IN_PK TEXT, "
                    + "TLG_IN_WHLOC_IN_NAME TEXT, "
                    + "TR_WH_IN_PK TEXT, "
                    + "TR_WH_IN_NAME TEXT, "
                    + "TR_WH_OUT_PK TEXT, "
                    + "TR_WH_OUT_NAME TEXT, "
                    + "TR_TYPE TEXT, "
                    + "STATUS TEXT, "
                    + "TLG_GD_REQ_M_PK TEXT, "
                    + "TLG_GD_REQ_D_PK TEXT, "
                    + "TLG_SA_SALEORDER_D_PK TEXT, "   // SO From Pk ExchangeProduct
                    + "TO_SA_SALEORDER_D_PK TEXT, "
                    + "SLIP_NO TEXT,"
                    + "TLG_PO_PO_D_PK TEXT, "
                    + "PO_NO TEXT, "
                    + "CUST_PK TEXT, "
                    + "CUST_NAME TEXT, "
                    + "SLIP_TYPE TEXT, "
                    + "SUPPLIER_PK TEXT, "       // SO To Pk ExchangeProduct
                    + "SUPPLIER_NAME TEXT, "
                    + "TR_DELI_QTY REAL, "
                    + "ITEM_BC_CHILD TEXT, "
                    + "GD_SLIP_NO TEXT, "
                    + "SCAN_DATE TEXT, "
                    + "SCAN_TIME TEXT, "
                    + "SENT_YN TEXT, "
                    + "SENT_TIME TEXT,"
                    + "PARENT_PK TEXT, "         // location in PK StockTransfer
                    + "CHILD_PK TEXT, "
                    + "BC_TYPE TEXT, "
                    + "REMARKS TEXT,"
                    + "CHARGER TEXT,"
                    + "INCOME_DATE TEXT, "
                    + "TLG_POP_INV_TR_PK TEXT, "
                    + "MAPPING_YN TEXT,"
                    + "EXECKEY TEXT,"
                    + "TRANS_PK TEXT,"
                    + "TRANS_NAME TEXT,"
                    + "SLIP_PK TEXT,"
                    + "SLIP_NAME TEXT,"
                    + "TO_ITEM_PK TEXT,"
                    + "TO_UOM TEXT,"
                    + "TO_LOTNO TEXT,"
                    + "TO_UOM_QTY TEXT,"
                    + "PROD_D_PK TEXT,"
                    + "REQ_NO TEXT,"
                    + "REQ_QTY TEXT,"
                    + "REQ_BAL TEXT,"
                    + "SCANNED TEXT,"
                    + "MANUAL TEXT,"
                    + "SO_NO TEXT,"
                    + "WI_PLAN_PK TEXT,"
                    + "TR_PARENT_ITEM_PK TEXT,"
                    + "PROCESS_TYPE TEXT,"
                    + "TLG_SA_SALEORDER_M_PK TEXT, "
                    //+ "TR_LOC_ID TEXT, "        //location out pk StockTransfer
                    //+ "LOC_NAME TEXT, "         //location out name StockTransfer
                    //+ "TR_WAREHOUSE_PK INTEGER, "
                    //+ "WH_NAME TEXT, "
                    //+ "QC_REQ_NO TEXT,"
                    //+ "QC_SLIP_NO TEXT,"
                    //+ "QC_CUSTOMER TEXT,"
                    //+ "QC_ITEM TEXT,"               // location in name StockTransfer
                    //+ "QC_DATE TEXT,"
                    //+ "QC_INSPECTOR TEXT,"
                    //+ "QC_CHECKED TEXT,"
                    //+ "QC_APPROVE TEXT,"
                    //+ "QC_JEDGMENT TEXT,"

                    // for maping offline kolonbd
                    + "GRADE TEXT,"
                    + "WEIGHT TEXT,"
                    + "GROSS_WEIGHT TEXT,"
                    + "DOFING_NO TEXT,"
                    + "PARENT_QTY TEXT,"
                    + "EVAL_QTY TEXT,"
                    + "EVAL_NUM_CHILD TEXT,"
                    + "DEL_IF INTEGER default 0 )";

            db.execSQL(CREATE_INV_TR);


            String CREATE_INV_TR_GRP = "CREATE TABLE IF NOT EXISTS INV_TR_GRP ( "
                    + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "TR_QTY FLOAT, "
                    + "TR_LOT_NO TEXT, "
                    + "TR_ITEM_PK INTEGER, "
                    + "ITEM_CODE TEXT, "
                    + "ITEM_NAME TEXT, "
                    + "ITEM_GROUP_PK INTEGER, "
                    + "ITEM_GROUP_NAME TEXT, " + "DEL_IF INTEGER default 0 )";

            db.execSQL(CREATE_INV_TR_GRP);

            String CREATE_TR_CONFIG = "CREATE TABLE IF NOT EXISTS TR_CONFIG ( "
                    + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "TIME_UPLOAD INTEGER, "
                    + "APPROVE_YN TEXT, "
                    + "DEL_IF INTEGER default 0 )";

            db.execSQL(CREATE_TR_CONFIG);

            //DELETE DATA AFTER 10 days
            int date_previous=Integer.parseInt(CDate.getDatePrevious(10));
            db.execSQL("DELETE FROM INV_TR where SCAN_DATE < "+date_previous);
            ///////////////


            // CREATE TABLE TLG_LABEL
            db.execSQL("CREATE TABLE IF NOT EXISTS TLG_LABEL(PK NUMBER, TLG_IT_ITEM_PK NUMBER, ITEM_BC VARCHAR2,"
                    + " LABEL_UOM VARCHAR2, YYYYMMDD VARCHAR2, ITEM_CODE VARCHAR2, ITEM_NAME VARCHAR2, ITEM_TYPE VARCHAR2,"
                    + "LABEL_QTY NUMBER, LOT_NO VARCHAR2);");

            // CREATE TABLE TLG_GD_REQ
            db.execSQL("CREATE TABLE IF NOT EXISTS TLG_GD_REQ(TLG_GD_REQ_M_PK NUMBER, REQ_DATE VARCHAR2, SLIP_NO VARCHAR2"
                    + ", TLG_GD_REQ_D_PK NUMBER, REQ_ITEM_PK NUMBER, REQ_QTY NUMBER,REQ_UOM VARCHAR2, OUT_WH_PK NUMBER, REF_NO VARCHAR2, OVER_RATIO NUMBER);");

            // CREATE TABLE tlg_in_warehouse
            db.execSQL("CREATE TABLE IF NOT EXISTS tlg_in_warehouse(PK NUMBER,wh_id VARCHAR2,wh_name VARCHAR2);");

            // CREATE TABLE tlg_in_whloc
            db.execSQL("CREATE TABLE IF NOT EXISTS tlg_in_whloc(PK NUMBER, loc_id VARCHAR2, loc_name VARCHAR2);");

            // CREATE TABLE tlg_pb_line
            db.execSQL("CREATE TABLE IF NOT EXISTS tlg_pb_line(PK NUMBER,line_id VARCHAR2,line_name VARCHAR2);");

            // CREATE TABLE tlg_lg_user_wh
            db.execSQL("CREATE TABLE IF NOT EXISTS tlg_lg_user_wh(PK NUMBER,user_pk VARCHAR2,wh_name VARCHAR2);");

            //Product Line Result Mr Trang
            db.execSQL("DROP TABLE IF EXISTS hourly ");
            CREATE_INV_TR = "CREATE TABLE IF NOT EXISTS hourly ( "
                    + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "CODE INTEGER, "
                    + "NAME TEXT, "
                    + "TYPE TEXT "
                    +" )";
            db.execSQL(CREATE_INV_TR);
            db.execSQL("DROP TABLE IF EXISTS line_result ");
            CREATE_INV_TR = "CREATE TABLE IF NOT EXISTS line_result ( "
                    + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "PRO_DATE TEXT, "
                    + "LINE_PK TEXT, "
                    + "LINE_NAME TEXT, "
                    + "TARGET TEXT, "
                    + "PRO_TIME TEXT, "
                    + "PRO TEXT, "
                    + "BAL TEXT, "
                    + "R_PRO TEXT, "

                    + "ROTATION TEXT, "
                    + "BUYER TEXT, "
                    + "STYLE TEXT, "
                    + "PO TEXT, "
                    + "ORD_QTY TEXT, "
                    + "PRO_QTY TEXT,"
                    + "WI_LINE_TARGET_PK TEXT,"
                    + " DEL_IF INTEGER default 0"
                    +" )";
            db.execSQL(CREATE_INV_TR);
            db.execSQL("DROP TABLE IF EXISTS line_wi ");
            CREATE_INV_TR = "CREATE TABLE IF NOT EXISTS line_wi ( "
                    + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "PRO_DATE TEXT, "
                    + "LINE TEXT, "
                    + "PO TEXT, "
                    + "WI TEXT, "
                    + "CODE TEXT, "
                    + "ITEM_CODE TEXT, "
                    + "ITEM_NAME TEXT, "
                    + "COLOR TEXT, "
                    + "SIZE TEXT, "
                    + "BUYER TEXT, "
                    + "BUYER_CODE TEXT, "  +
                    "PLAN TEXT "
                    +" )";
            db.execSQL(CREATE_INV_TR);
            db.execSQL("DROP TABLE IF EXISTS wi_result_hourly ");
            CREATE_INV_TR = "CREATE TABLE IF NOT EXISTS wi_result_hourly ( "
                    + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "PRO_DATE TEXT, "
                    + "WI TEXT, "
                    + "HOURLY TEXT, "
                    + "QTY_00 TEXT, "
                    + "QTY_30 TEXT, "
                    + "DEF_00 TEXT, "
                    + "DEF_30 TEXT, "
                    + "PIC TEXT, "
                    + "DATE_INPUT TEXT"
                    +" )";
            db.execSQL(CREATE_INV_TR);

            db.execSQL("DROP TABLE IF EXISTS wi_def_result_hourly ");
            CREATE_INV_TR = "CREATE TABLE IF NOT EXISTS wi_def_result_hourly ( "
                    + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "PRO_DATE TEXT, "
                    + "WI TEXT, "
                    + "HOURLY TEXT, "
                    + "HOURLY_TYPE TEXT, "
                    + "QTY TEXT, "
                    + "TYPE TEXT, "
                    + "DEF_NAME TEXT, "
                    + "PIC TEXT, "
                    + "DATE_INPUT TEXT"
                    +" )";
            db.execSQL(CREATE_INV_TR);
            // db.close();
/*
            int[] raw = {R.raw.hourly,R.raw.line_result,R.raw.line_wi,R.raw.wi_result_hourly,R.raw.wi_def_result_hourly};

            String[] name = {"hourly","line_result","line_wi","wi_result_hourly","wi_def_result_hourly"};
            for(int i=0;i<raw.length;i++){
                InputStream in = getResources().openRawResource(raw[i]);
                CSVFile csvFile = new CSVFile(in);
                List scoreList = csvFile.read();
                String[] colums = (String[]) scoreList.get(0);
                for( int r=1;r<scoreList.size();r++) {
                    String[] arr = (String[]) scoreList.get(r);
                    ContentValues cv = new ContentValues(3);

                    for(int col=0;col<colums.length;col++) {
                        cv.put(colums[col], arr[col].trim());
                    }
                    db.insert(name[i], null, cv);

                }
            }
            */
            db.close();
        }catch (Exception ex)
        {
            Log.e("OnCreateTableINV_TR:", ex.getMessage());
            Toast.makeText(this, "OnCreateTableINV_TR:" + ex.getMessage(), Toast.LENGTH_LONG);
        }
    }
    /*private void load_menu(){
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            try {
                String l_para = "1,LG_MPOS_GET_AUTHORITY_ALL,2," + bsUserID+"|1"+"";
              //  String result[][] = this.getDataConnectThread(this, l_para);
                List<JDataTable> jdt=this.CallDataService(this, l_para);
                JDataTable dtRoot = jdt.get(0);
                JDataTable dtChilf = jdt.get(1);

*//*
                data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.HEADER, "INVENTORY", );
                data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Stock  In",R.drawable.ic_box));
                data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Stoc Out",R.drawable.ic_box));
                data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Update LOC",R.drawable.ic_box));

                data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.HEADER, "PRODUCT RESULT",R.drawable.ic_box));
                data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Line result",R.drawable.ic_box));
                data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Monitoring",R.drawable.ic_box));

                NavigationMenuAdapter.Item places = new NavigationMenuAdapter.Item(NavigationMenuAdapter.HEADER, "QC MANAGEMENT",R.drawable.ic_box);
                places.invisibleChildren = new ArrayList<>();
                places.invisibleChildren.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Scan",R.drawable.ic_box));
                places.invisibleChildren.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Barcode",R.drawable.ic_box));
                data.add(places);*//*

                        for(int i=0;i<dtRoot.totalrows;i++)
                        {
                            String ic_form=  dtRoot.records.get(i).get("icon_form").toString();
                            data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.HEADER,  dtRoot.records.get(i).get("icon_form").toString(),
                                    R.drawable.ic_box,
                                    dtRoot.records.get(i).get("icon_form").toString(),
                                    dtRoot.records.get(i).get("icon_form").toString() ));
                            for(int j=0;j<dtChilf.totalrows;j++)
                            {
                              if(dtChilf.records.get(j).get("parent_form").toString().equals(ic_form))
                                data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD,  dtChilf.records.get(j).get("icon_form").toString(),
                                        R.drawable.ic_box,
                                        dtChilf.records.get(j).get("icon_form").toString(),
                                        dtChilf.records.get(j).get("icon_form").toString()));

                            }
                        }
              *//*
                for (int i = 0; i < result.length; i++) {
                    if(!result[i][1].equals("0")) {
                        // ItemMenu item = new ItemMenu();
                      //  item.setImg_name(result[i][0]);
                       // item.setClassID(result[i][1]);

                       // lst_menu.add(item);
                    }
                }*//*
            }catch (Exception ex)
            {
                alertToastLong("AUTHORITY: " + ex.getMessage());
            }
        }
        else{
            alertToastLong("Network is broken. Please check network again !");
        }
    }*/

    @Override
    public void onFragmentInteraction(Uri uri) {
     int i=0;
    }
    @Override
    public void onResume() {
        super.onResume();

    }
}