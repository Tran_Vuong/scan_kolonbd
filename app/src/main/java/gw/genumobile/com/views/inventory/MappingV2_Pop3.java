package gw.genumobile.com.views.inventory;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bixolon.labelprinter.BixolonLabelPrinter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.TcpConnection;
import com.zebra.sdk.graphics.internal.ZebraImageAndroid;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import gw.genumobile.com.R;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.HandlerWebService;
import gw.genumobile.com.utils.CNumber;
import gw.genumobile.com.utils.UIHelper;
import gw.genumobile.com.views.gwFragmentDialog;
import gw.genumobile.com.views.productresult.EditCapture;

//ZEBRA

public class MappingV2_Pop3 extends gwFragmentDialog implements View.OnClickListener {
    //region Variable
    static BixolonLabelPrinter mBixolonLabelPrinter;

    static private String mDeviceBixolon = null;
    static private boolean flagPrinter = false;
    static private String ipPrint = "",ipPrintZebra = "", bluetoothMac = "", levelPrintZ = "";
    int levelPrintB = 0;
    boolean bixolon, zebraIP,zebraBT;
    static TextView tvStatus;
    private static final int ACTION_TAKE_PHOTO_B = 1;
    private static final int SIGNATURE_ACTIVITY = 2;
    public int REQUEST_CODE = 1;
    public int RESULT_CODE = 1;
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    protected int fixedRowHeight = 70; //70|30
    protected int fixedHeaderHeight = 80; //80|40
    SQLiteDatabase db = null;

    //endregion


    private Bitmap imageBitmap;
    private ImageView mImageView;
    Button btn_Print,btn_Close;
       int previous_position = 0;
    View vContent;
    Activity gwAc;
    //ZEBRA
    private Connection mZebraLabelPrinter;
  //  private UIHelper helper = new UIHelper(this);
    final String[] strStatus = {""};
    String type="online";
    private UIHelper helper;
    //endregion
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gwAc=getActivity();
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
        helper= new UIHelper(gwMActivity);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vContent = inflater.inflate(R.layout.activity_mapping_view_lable_rc, container, false);
        TextView myTextView = (TextView)vContent.findViewById(R.id.lb_date);
        SpannableString      text = new SpannableString(myTextView.getText());

        text.setSpan(new TextAppearanceSpan(null, 0, 35, null, null),0,4,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        text.setSpan(new TextAppearanceSpan(null, 0, 25, null, null),5,14,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        myTextView.setText(text, TextView.BufferType.SPANNABLE);

        Toolbar toolbar = (Toolbar) vContent.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                   OnComeBackResult(null);
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);
        toolbar.setTitle("LABEL");
        //this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        mImageView = new ImageView(gwMActivity);
        String P_PACKAGES_PK="",P_ITEM_BC="",P_QTY="",P_ITEM_CODE="",P_ITEM_NAME="",P_GRADE="",P_PROD_CODE="",P_NET_WEIGHT="",P_YYMMDD="",P_HHMM="",P_CHARGER_PK="",P_LOT_NO="",P_DOF_NO="";
        type = getArguments().getString("type");
        if(type=="online") {
            String para = getArguments().getString("para1");
            JDataTable dt = GetDataOnServer1(para);
            if(dt==null || dt.totalrows <=0) {return vContent ;}
                 P_PACKAGES_PK =  dt.records.get(0).get("pk").toString();
                 P_ITEM_BC = dt.records.get(0).get("item_bc").toString();
                 P_QTY =  dt.records.get(0).get("qty").toString();
                 P_ITEM_CODE =  dt.records.get(0).get("item_code").toString();
                 P_ITEM_NAME =  dt.records.get(0).get("item_name").toString();
                 P_GRADE =  dt.records.get(0).get("grade").toString();
                 P_PROD_CODE =  dt.records.get(0).get("prod_code").toString();
                 P_NET_WEIGHT = dt.records.get(0).get("pa_net_weight").toString();
                 P_YYMMDD =  dt.records.get(0).get("yymmdd").toString();
                 P_HHMM =  dt.records.get(0).get("hhmm").toString();
                 P_CHARGER_PK =  dt.records.get(0).get("charger_pk").toString();
                P_LOT_NO =  dt.records.get(0).get("lot_no").toString();
                P_DOF_NO =  dt.records.get(0).get("dof_no").toString();

        }
        else { //offline
            String[] arr_para = getArguments().getStringArray("para1");

            P_ITEM_BC = arr_para[0];
            P_QTY = arr_para[1];
            P_ITEM_CODE = arr_para[2];
            P_ITEM_NAME = arr_para[2];
            P_GRADE =  arr_para[3];
            P_PROD_CODE =  "";
            P_NET_WEIGHT = arr_para[4];
            P_YYMMDD = arr_para[5];
            P_HHMM =  arr_para[6];
            P_CHARGER_PK =  arr_para[7];
        }
                ((TextView) vContent.findViewById(R.id.txt_ItemName)).setText("POLYESTER \n TWISTED YARN");
                ((TextView) vContent.findViewById(R.id.txt_Item_Spec)).setText(P_ITEM_NAME);
                ((TextView) vContent.findViewById(R.id.txt_grade)).setText(P_GRADE);
                ((TextView) vContent.findViewById(R.id.txt_item_code)).setText(P_ITEM_CODE);
                ((TextView) vContent.findViewById(R.id.txt_netwt)).setText(P_NET_WEIGHT);
                ((TextView) vContent.findViewById(R.id.txt_numchese)).setText(P_QTY);
                ((TextView) vContent.findViewById(R.id.txt_date)).setText(P_YYMMDD);
                ((TextView) vContent.findViewById(R.id.txt_time)).setText(P_HHMM);
                ((TextView) vContent.findViewById(R.id.txt_lotno)).setText(P_LOT_NO);
                ((TextView) vContent.findViewById(R.id.txt_dofno)).setText(P_DOF_NO);
                ((TextView) vContent.findViewById(R.id.txt_BC)).setText(P_ITEM_BC);


   /*     int Measuredheight = 0;
        Point size = new Point();
        WindowManager w =  gwMActivity.getWindowManager();
*/

     //   ((TextView)vContent.findViewById(R.id.tvLocation_Value)).setText(_assetLocationName);

        try{
            Bitmap bitmapBarcode = encodeAsBitmapBarcode(P_ITEM_BC, BarcodeFormat.CODE_39, 1000, 150);
           Bitmap bitmapQaCode = encodeAsBitmapBarcode(P_ITEM_BC, BarcodeFormat.QR_CODE, 400, 400);

            ((ImageView)vContent.findViewById(R.id.ivQACode)).setImageBitmap(bitmapQaCode);
            ((ImageView)vContent.findViewById(R.id.ivBarcode)).setImageBitmap(bitmapBarcode);
        }catch (Exception ex){
            Log.e("Execption Image: ", ex.getMessage());
        }
        //CHOOSE PRINT
        bixolon = appPrefs.getBoolean("PrintBixolon", false);
        zebraIP = appPrefs.getBoolean("PrintZebraIP", false);
        zebraBT = appPrefs.getBoolean("PrintZebraBluetooth", false);
        ipPrintZebra  = appPrefs.getString("ipPrintZebra", "");
        //BIXOLON
        ipPrint  =  appPrefs.getString("ipPrint", "");
        String levelB =  appPrefs.getString("levelPrintB", "20");

        if(!CNumber.isStringInt(levelB)){
            tvStatus.setText("Please Insert Number For Level In Config Print " + mDeviceBixolon);
            tvStatus.setTextColor(Color.RED);
            flagPrinter = false;
        }else{
            levelPrintB = Integer.parseInt(levelB);
        }

        //Zebra
        bluetoothMac = appPrefs.getString("bluetoothPrint", "");
        levelPrintZ =  appPrefs.getString("levelPrintZ", "");

        tvStatus = (TextView)vContent.findViewById(R.id.tvStatus);
        if(bixolon){
            mBixolonLabelPrinter = new BixolonLabelPrinter(gwAc, mHandlerBixolon, null);
            mBixolonLabelPrinter.findNetworkPrinters(2000);
        }else {
            //if(zebra)

            //checkStatusZebraPrinter();

        }

        btn_Print = (Button) vContent.findViewById(R.id.btn_Print);
        btn_Print.setOnClickListener(this);
        btn_Close = (Button) vContent.findViewById(R.id.btn_Close);
        btn_Close.setOnClickListener(this);
    return vContent;
//        if (shouldAskPermission()) {
//            ActivityCompat.requestPermissions(gwMActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
//        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_Print:
                onPrint(v);
                break;

            case R.id.btn_Close:

                OnComeBackResult(v);
                break;
            default:
                break;
        }

    }

    public void OnComeBackResult(View v) {
      /*  Intent i = new Intent();
        i.putExtra("line", LINE_DETAIL);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);*/
        dismiss();
       // SewingResultActivity fr =(SewingResultActivity) getTargetFragment();
      //  fr.loadGridData();
    }
    @Override
    public void onStart()
    {
        super.onStart();

    }
    @Override
    public void onResume() {
        super.onResume();
        Point size = new Point();
        WindowManager w =  gwMActivity.getWindowManager();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
        w.getDefaultDisplay().getSize(size);
        Measuredheight = size.y;
        getDialog().getWindow().setLayout(size.x*80/100,  Measuredheight*80/100);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTION_TAKE_PHOTO_B && resultCode == Activity.RESULT_OK) {

            Intent intent = new Intent(gwMActivity, EditCapture.class);
         //   intent.putExtra("URLPhotoPath", mCurrentPhotoPath);
            startActivityForResult(intent, SIGNATURE_ACTIVITY);

            //region ACTION_TAKE_PHOTO_B
            //setPic();
            //galleryAddPic();
            //mCurrentPhotoPath = null;
            //mImageView = new ImageView(gwMActivity);
            //mImageView.setImageBitmap(imageBitmap);
            //endregion

        }
    }
    public List<String> GetDataOnServer( String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";

                l_para = "1,LG_MPOS_MAPP_GET_LABLE_INFO," + para;

            //ConnectThread networkThread = new ConnectThread(gwMActivity);
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                ////if (type.endsWith("SlipNo")) {
                for (int j = 0; j < result[i].length; j++)
                    labels.add(result[i][j].toString());
                ////}
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GetDataOnServer" + ": " + ex.getMessage());
        }
        return labels;
    }
    public JDataTable GetDataOnServer1(String para) {
        JDataTable dt = null;
        String proc="LG_MPOS_MAPP_GET_LABLE_INFO";

        try {
            JResultData jresultkq = HandlerWebService.GetData(proc, 1, para);
            if (jresultkq ==null)
            {
                gwMActivity.alertToastLong("Please check Network!!"  + ": " +para);
                return  null;
            }
            if (jresultkq.getTotals() > 0) {
                List<JDataTable> jdt = jresultkq.getListODataTable();
                dt = jdt.get(0);
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(type + ": " + ex.getMessage());
        }
        return  dt;
    }
    public void onPrint(View view){

      //  if(!flagPrinter) return;

        FrameLayout flIssueLabel = new FrameLayout(gwMActivity);
        flIssueLabel = (FrameLayout)vContent.findViewById(R.id.flIssueLabel);
        flIssueLabel.setDrawingCacheEnabled(true);

        flIssueLabel.buildDrawingCache();
       // Bitmap bitmap_temp = flIssueLabel.getDrawingCache();
      //  bitmap_temp=getRotatedBitmap(bitmap_temp);
        final Bitmap bitmap= flIssueLabel.getDrawingCache();;

        if(bitmap!= null){
            try{
                if(bixolon){

                    if(levelPrintB > 80) levelPrintB = 80;
                    if(levelPrintB < 20) levelPrintB = 20;

                    ProgressDialog progressDialog = null;
                    progressDialog = ProgressDialog.show(gwMActivity, "", "Printing Barcode, Please Wait...");

                    final ProgressDialog finalProgressDialog = progressDialog;
                    new Thread() {

                        public void run() {
                            mBixolonLabelPrinter.connect(ipPrint, 9100, 10000);
                            //AssetEntry_F01_View_Issue_Label.mBixolonLabelPrinter.drawBitmap(bitmap, 82, 2, 1060, levelPrintB);
                            mBixolonLabelPrinter.drawBitmap(bitmap, 1, 1, 1160, levelPrintB);

                            mBixolonLabelPrinter.print(1, 1);

                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            finalProgressDialog.dismiss();
                        }

                    }.start();

                }else {
                 // if(zebra)  Bitmap bitmap11 = getRotatedBitmap(bitmap);
                    printPhotoZebra(bitmap);
                }
            }catch (Exception ex){
                Log.e("Check status: ", ex.getMessage());
            }finally {

            }
        }
    }
    final private int rotation = 90;
    private Bitmap getRotatedBitmap(Bitmap bitmap) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) gwMActivity.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels;

        Matrix matrix = new Matrix();
        Bitmap bitmapResize = null;
        //bitmapResize = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()),                      //0.93 tab A    //0.45 tab S 7inch
        //(int)(bitmap.getHeight()), true);
        //bitmapResize = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*0.594),                      //0.93 tab A    //0.45 tab S 7inch
        //(int)(bitmap.getHeight()*0.610), true);

        if(screenWidth==1600 && screenHeight==2560){                                                // tab S 7inch
            bitmapResize = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*0.456),                      //0.93 tab A    //0.45 tab S 7inch
                    (int)(bitmap.getHeight()*0.47), true);                                                              //0.82 tab A    //0.47 tab S 7inch

        }
        else if(screenWidth==768 && screenHeight==1024){                                            // tab A
            bitmapResize = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*0.94),                      //0.93 tab A    //0.45 tab S 7inch
                    (int)(bitmap.getHeight()*0.82), true);                                                              //0.82 tab A    //0.47 tab S 7inch

        }
        else if(screenWidth==1200 && screenHeight==1920){                                            // tab A(2016)
            bitmapResize = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*0.594),                      //0.93 tab A    //0.45 tab S 7inch
                    (int)(bitmap.getHeight()*0.610), true);                                                          //0.82 tab A    //0.47 tab S 7inch

        }
        //===================Tablet==S2======1536x2048 ================================

        else if(screenWidth==1536 && screenHeight==2048){                                                // 1536x2048
            bitmapResize = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*0.465),
                    (int)(bitmap.getHeight()*0.49), true);

        }

        else
        {
            bitmapResize = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*0.94),                      //0.93 tab A    //0.45 tab S 7inch
                    (int)(bitmap.getHeight()*0.82), true);
        }

        matrix.postRotate(rotation, bitmapResize.getWidth(),
                bitmapResize.getHeight());

        return Bitmap.createBitmap(bitmapResize, 0, 0, bitmapResize.getWidth(),
                bitmapResize.getHeight(), matrix, true);
    }
    //endregion


    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }
    Bitmap encodeAsBitmapBarcode(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }
    private static final Handler mHandlerBixolon = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            flagPrinter = true;

            switch (msg.what) {

                case BixolonLabelPrinter.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BixolonLabelPrinter.STATE_CONNECTED:
                            break;

                        case BixolonLabelPrinter.STATE_CONNECTING:
                        //    tvStatus.setText("Connecting To Print");
                            tvStatus.setTextColor(Color.BLUE);
                            flagPrinter = false;
                            break;

                        case BixolonLabelPrinter.STATE_NONE:
                          //  tvStatus.setText("Error " + mDeviceBixolon);
                            tvStatus.setTextColor(Color.RED);
                            flagPrinter = false;
                            break;
                    }
                    break;

                case BixolonLabelPrinter.MESSAGE_READ:

                    break;

                case BixolonLabelPrinter.MESSAGE_DEVICE_NAME:
                    mDeviceBixolon = msg.getData().getString(BixolonLabelPrinter.DEVICE_NAME);

                    if(mDeviceBixolon.contains(".")) {//where ip
                      //  tvStatus.setText("Connected To Printer: " + mDeviceBixolon);
                        tvStatus.setTextColor(Color.BLACK);
                       // flagPrinter = true;
                    }else{
                     //   tvStatus.setText("Error " + mDeviceBixolon);
                        tvStatus.setTextColor(Color.RED);
                      //  flagPrinter = false;
                    }

                    break;

                case BixolonLabelPrinter.MESSAGE_USB_DEVICE_SET:
                    if (msg.obj == null) {
                        tvStatus.setText("No connectable to printer:" + mDeviceBixolon);
                        tvStatus.setTextColor(Color.RED);
                     //   flagPrinter = false;
                    } else {

                    }
                    break;

                case BixolonLabelPrinter.MESSAGE_NETWORK_DEVICE_SET:
                    if (msg.obj != null) {
                      //  tvStatus.setText("No connectable to printer. Please check your network");
                        tvStatus.setTextColor(Color.RED);
                  //      flagPrinter = false;
                    }else{
                        mBixolonLabelPrinter.connect(ipPrint, 9100, 10000);
                        flagPrinter = true;
                    }
                    break;

            }
        }
    };
    private void printPhotoZebra(final Bitmap bitmap) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    Looper.prepare();
                    helper.showLoadingDialog("Printing Barcode, Please Wait...");;
                    Connection connection = getZebraPrinterConn();
                    connection.open();
                    ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
                    printer.printImage(new ZebraImageAndroid(bitmap), 0, 15, 1175, 1750 , false);

                    connection.close();

                } catch (ConnectionException e) {
                    helper.showErrorDialogOnGuiThread(e.getMessage());
                } catch (ZebraPrinterLanguageUnknownException e) {
                    helper.showErrorDialogOnGuiThread(e.getMessage());
                } finally {
                    //bitmap.recycle();
                    helper.dismissLoadingDialog();

                    if ( Looper.myLooper()!=null) {
                        Looper.myLooper().quit();
                    }
                }
            }
        }).start();

    }
    private Connection getZebraPrinterConn() {
        int portNumber;
        try {
            portNumber = Integer.parseInt("9100");
        } catch (NumberFormatException e) {
            portNumber = 0;
        }
        return zebraIP ? new TcpConnection(ipPrintZebra, portNumber):new BluetoothConnection(this.bluetoothMac) ;
    }
    private Connection getZebraPrinterConnbt() {
        //Can Print with IP
        //Now, return bluetooth.
        return new BluetoothConnection(this.bluetoothMac);
    }


}
