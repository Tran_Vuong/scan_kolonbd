package gw.genumobile.com.views.inventory;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

import gw.genumobile.com.R;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.views.gwFragmentDialog;
import gw.genumobile.com.views.productresult.EditCapture;


public class StockOutReq_PopReq extends gwFragmentDialog implements View.OnClickListener {

    static TextView tvStatus;
    private static final int ACTION_TAKE_PHOTO_B = 1;
    private static final int SIGNATURE_ACTIVITY = 2;
    public int REQUEST_CODE = 1;
    public int RESULT_CODE = 1;

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    protected int fixedRowHeight = 70; //70|30
    protected int fixedHeaderHeight = 60; //80|40
    protected int[] scrollableColumnWidths = new int[]{25, 10,12, 23, 15, 0, 10, 10, 10};
    SQLiteDatabase db = null;
    Queue queue = new LinkedList();
    Button btn_view,btn_search,btn_edit;
    int indexPrevious=-1;
    String line_detail[] = new String[0];
    View vContent;
    Activity gwAc;
    HashMap hashMapGrade= new HashMap();
    String[][] ls_grade = new String[][]{
            {"A", "A"},
            {"B", "B"},
            {"C", "C"}};
    public Spinner spn_sewing;
    String grade_item="";
    TextView _txtTotalGridBot;
    EditText edt_ymd_from,edt_ymd_to,edt_WINo,edt_item_code;
    TableLayout scrollablePart;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gwAc=getActivity();
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vContent = inflater.inflate(R.layout.fragment_stock_out_req_pop_req, container, false);
        Toolbar toolbar = (Toolbar) vContent.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                   OnComeBackResult(null);
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);
        toolbar.setTitle("LIST");

        btn_search = (Button) vContent.findViewById(R.id.btn_search);
        btn_search.setOnClickListener(this);
        btn_edit= (Button) vContent.findViewById(R.id.btn_edit);
        btn_edit.setOnClickListener(this);
        spn_sewing = (Spinner) vContent.findViewById(R.id.spn_sewing);
        edt_ymd_from =(EditText) vContent.findViewById(R.id.edt_yymmdd_from);
        edt_ymd_from.setOnClickListener(this);
        edt_ymd_to =(EditText) vContent.findViewById(R.id.edt_yymmdd_to);
        edt_ymd_to.setOnClickListener(this);
        edt_WINo =(EditText) vContent.findViewById(R.id.edt_WINo);
        edt_item_code =(EditText) vContent.findViewById(R.id.edt_item_code);


        edt_ymd_from.setText(CDate.getDateFormatyyyyMMdd());
        edt_ymd_to.setText(CDate.getDateFormatyyyyMMdd());
        LoadGrade(ls_grade);
        OnShowGridHeader();
      //  onSeach(null);
        return vContent;
//        if (shouldAskPermission()) {
//            ActivityCompat.requestPermissions(gwMActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
//        }
    }
    public void onSeach(View v) {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            scrollablePart = (TableLayout) vContent.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child
            String l_para = "";
            String edt_item_code =String.valueOf (((TextView)vContent.findViewById(R.id.edt_item_code)).getText());
            String edt_WINo =String.valueOf (((TextView)vContent.findViewById(R.id.edt_WINo)).getText());
            String edt_grade_item =String.valueOf (grade_item);
            String edt_yymmdd_from =String.valueOf (((TextView)vContent.findViewById(R.id.edt_yymmdd_from)).getText()).replace("/","");
            String edt_yymmdd_to =String.valueOf (((TextView)vContent.findViewById(R.id.edt_yymmdd_to)).getText()).replace("/","");
                l_para = "1,LG_MPOS_GET_OUT_INV_REQ_POP," + edt_item_code+"|"+edt_WINo+"|"+edt_grade_item+"|"+edt_yymmdd_from+"|"+edt_yymmdd_to;


            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                row = new TableRow(gwMActivity);
                row.setLayoutParams(wrapWrapTableRowParams);
                row.setGravity(Gravity.CENTER);
                row.setBackgroundColor(Color.LTGRAY);
                row.addView(makeTableColWithText(result[i][0].toString(), scrollableColumnWidths[0], fixedRowHeight, -1));
                row.addView(makeTableColWithText(result[i][1].toString(), scrollableColumnWidths[1], fixedRowHeight, -1));
                row.addView(makeTableColWithText(result[i][2].toString(), scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(result[i][3].toString(), scrollableColumnWidths[3], fixedRowHeight, -1));
                row.addView(makeTableColWithText(result[i][4].toString(), scrollableColumnWidths[4], fixedRowHeight, -1));
                row.addView(makeTableColWithText(result[i][5].toString(), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                           // TextView tvSlipNo = (TextView) tr1.getChildAt(7); //SLIP_NO
                          //  String st_slipNO = tvSlipNo.getText().toString();
                          //  if (st_slipNO.equals("-")) {

                                TextView tv1 = (TextView) tr1.getChildAt(7); //PK
                                int xx = tr1.getChildCount();
                            line_detail = new String[tr1.getChildCount()];
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setBackgroundColor(getResources().getColor(R.color.grd_bgrow_selected));
                                    //tv11.setTextColor(Color.rgb(255, 99, 71));
                                    line_detail[i] = tv11.getText().toString();
                                }
                            if (indexPrevious >= 0
                                    && indexPrevious != scrollablePart.indexOfChild(v)) {
                                TableRow trP = (TableRow) scrollablePart.getChildAt(indexPrevious);
                                for (int k = 0; k < trP.getChildCount(); k++) {
                                    tv11 = (TextView) trP.getChildAt(k);
                                    tv11.setBackgroundColor(getResources().getColor(R.color.grd_bgrow));
                                }
                            }
                            //Update indexPrevious
                            indexPrevious = scrollablePart.indexOfChild(v);
                        }
                    });
                    scrollablePart.addView(row);
                }
            _txtTotalGridBot = (TextView) vContent.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " +  result.length + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
        }
    }
    private void LoadGrade(String[][] _data) {
        try {
            String l_para = "1,LG_MPOS_GET_LG_PROCESS,"+"1" ;

            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
           _data = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();
            //
            for (int i = 0; i < _data.length; i++) {
                lstGroupName.add(_data[i][1]);
                hashMapGrade.put(i, _data[i][0]);
            }

            if (_data != null && _data.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spn_sewing.setAdapter(dataAdapter);
                //  wh_name = myLstWH.getItemAtPosition(0).toString();
            } else {
                spn_sewing.setAdapter(null);
                grade_item = "0";
            }
        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
        //-----------

        // onchange spinner wh
        spn_sewing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                // wh_name = myLstWH.getItemAtPosition(i).toString();
                // Log.e("WH_name",wh_name);
                Object pkGroup = hashMapGrade.get(i);
                if (pkGroup != null) {
                    grade_item = String.valueOf(pkGroup);
                    if (!grade_item.equals("0")) {

                        //dosomehitng

                    }
                } else {
                    grade_item = "0";
                    Log.e("grade_item: ", grade_item);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
  private void onSelect(View v){
        if(line_detail.length>0) {
            Intent intent = new Intent();
            intent.putExtra("slip_no", line_detail[1]);
            intent.putExtra("pk", line_detail[5]);
            getTargetFragment().onActivityResult(getTargetRequestCode(), 1, intent);
            getDialog().dismiss();
        }
    }
    private  void LoadDate(View v){
        final int v_id=v.getId();
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                //updateLabel();
                String myFormat = "yyyy/MM/dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                if(v_id== R.id.edt_yymmdd_from)
                edt_ymd_from.setText(sdf.format(myCalendar.getTime()));
                else   edt_ymd_to.setText(sdf.format(myCalendar.getTime()));
            }
        };
        new DatePickerDialog(gwMActivity, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_search:
                onSeach(v);
                break;
            case R.id.btn_edit:
                onSelect(v);
                break;
            case R.id.edt_yymmdd_from:
                LoadDate(v);
                break;
            case R.id.edt_yymmdd_to:
                LoadDate(v);
                break;
            default:
                break;
        }

    }

    public void OnComeBackResult(View v) {
      /*  Intent i = new Intent();
        i.putExtra("line", LINE_DETAIL);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);*/
        dismiss();
       // SewingResultActivity fr =(SewingResultActivity) getTargetFragment();
      //  fr.loadGridData();
    }
    @Override
    public void onStart()
    {
        super.onStart();

    }
    @Override
    public void onResume() {
        super.onResume();
        Point size = new Point();
        WindowManager w =  gwMActivity.getWindowManager();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
        w.getDefaultDisplay().getSize(size);
        Measuredheight = size.y;
            getDialog().getWindow().setLayout(size.x,  Measuredheight*95/100);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTION_TAKE_PHOTO_B && resultCode == Activity.RESULT_OK) {

            Intent intent = new Intent(gwMActivity, EditCapture.class);
         //   intent.putExtra("URLPhotoPath", mCurrentPhotoPath);
            startActivityForResult(intent, SIGNATURE_ACTIVITY);

            //region ACTION_TAKE_PHOTO_B
            //setPic();
            //galleryAddPic();
            //mCurrentPhotoPath = null;
            //mImageView = new ImageView(gwMActivity);
            //mImageView.setImageBitmap(imageBitmap);
            //endregion

        }
    }
    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;


        // Accept Scan
        scrollablePart = (TableLayout) vContent.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("WI No", scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Req No", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item CD", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Name", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Req Qty", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("PK", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

}
