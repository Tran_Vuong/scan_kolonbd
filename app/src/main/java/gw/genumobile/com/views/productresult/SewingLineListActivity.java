package gw.genumobile.com.views.productresult;

import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.models.gwfunction;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.views.gwFragment;

public class SewingLineListActivity extends gwFragment implements View.OnClickListener {
    SQLiteDatabase db;
    TextView lbDate, lbTime, bsTextView;
    public Spinner myLstFactory, myLstLine;
    TableLayout scrollablePart;
    String factory_pk = "", line_pk = "";
    HashMap hashMapFac = new HashMap();
    HashMap hashMapLine = new HashMap();
    int fixedRowHeight = 65, fixedHeaderHeight = 50;
    int indexPrevious = -1;
    String line_detail[] = new String[0];
    Thread myThread = null;
   protected int[] scrollableColumnWidths = new int[]{16, 16, 16, 16, 16, 12, 7, 13, 10, 10,10};
   // protected int[] scrollableColumnWidths = new int[]{14, 7, 10, 8, 9, 10, 7, 13, 10, 10,10};
    String[][] dataFactory = new String[][]{
            {"11", "Factory 1"},
            {"12", "Factory 2"},
            {"13", "Factory 3"}};
    String[][] dataLine = new String[][]{
            {"11", "Sewing Line"},
            {"12", "Cutting Line"},
            {"13", "ASS Line"}};
    String[][] dataGrid = new String[][]{
            {"F1S01 Sewing 01", "800", "11:00", "340", "460", "350", "97%", "BIBI 17S/S001", "3,500", "1,200"},
            {"F1S01 Sewing 02", "800", "11:00", "300", "500", "300", "100%", "BIBI 17S/S001", "3,500", "1,200"}
    };
    String[][] dataHead = new String[][]{
            {"F1S01 Sewing 01", "800", "11:00", "340", "460", "350", "97%", "BIBI 17S/S001", "3,500", "1,200"}
    };
    protected int[] bsColors = new int[]{0xff00ddff, 0x300000FF, 0xCCFFFF, 0x99CC33, 0x00FF99};
    View rootView;
    Button btProResult;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setLanguageApplication();
        rootView = inflater.inflate(R.layout.activity_sewing_line_list, container, false);

        lbDate = (TextView) rootView.findViewById(R.id.lbDate);
        lbTime = (TextView) rootView.findViewById(R.id.lbTime);
        myLstFactory = (Spinner) rootView.findViewById(R.id.cbFactory);
        myLstLine = (Spinner) rootView.findViewById(R.id.cbLineGroup);
        btProResult= (Button) rootView.findViewById(R.id.btProResult);
        btProResult.setOnClickListener(this);
        LoadFactory(dataFactory);
        LoadLine();


        // LoadLineProduct();

        Runnable runnable = new CountDownRunner();
        myThread = new Thread(runnable);
        myThread.start();
        return rootView;
    }
    @Override
    public void onResume() {
        super.onResume();
        OnShowGridHeader();
        LoadLineProduct();
        line_detail=null;
    }

    @Override
    public void onStop() {
        super.onStop();
        myThread.interrupt();
    }

    public void OnPro_Sew_Result(View v) throws InterruptedException, ExecutionException {
        if(line_detail==null||line_detail.length<=0){
            Toast.makeText(v.getContext(), "Please Select one row!!!",
                    Toast.LENGTH_LONG).show();
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putStringArray("line", line_detail);
        int title = R.string.title_activity_prodresult;// getResources().getIdentifier("ic_menu_default","string",gwActivity.getPackageName());
        gwMActivity.ShowForm("gw.genumobile.com.views.productresult.ProResultByWI",title,bundle);

        // Intent i = new Intent(v.getContext(), SewingResultActivity.class);
        //  i.putExtra("line",line_detail);
        //   i.putExtra("user", user);
        // startActivity(i);

    }

    private void LoadFactory(String[][] _data) {
        try {
            List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < _data.length; i++) {
                lstGroupName.add(_data[i][1]);
                hashMapFac.put(i, _data[i][0]);
            }

            if (_data != null && _data.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstFactory.setAdapter(dataAdapter);
                //  wh_name = myLstWH.getItemAtPosition(0).toString();
            } else {
                myLstFactory.setAdapter(null);
                factory_pk = "0";
            }
        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
        //-----------

        // onchange spinner wh
        myLstFactory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                // wh_name = myLstWH.getItemAtPosition(i).toString();
                // Log.e("WH_name",wh_name);
                Object pkGroup = hashMapFac.get(i);
                if (pkGroup != null) {
                    factory_pk = String.valueOf(pkGroup);
                    Log.e("f_pk: ", factory_pk);
                    if (!factory_pk.equals("0")) {

                        //dosomehitng

                    }
                } else {
                    factory_pk = "0";
                    Log.e("wi_pk: ", factory_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void LoadLine() {
        String   l_para =String.format("1,%s,1,%s", gwfunction.prodmonitoring_selGrgLine.getValue(),bsUserID,"");
        //  String   l_para = "1,LG_MPOS_GET_FACT_LINE,2," + bsUserID;
        try {
            List<String> lstGroupName = new ArrayList<String>();
            gwService networkThread = new gwService(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
            List<JDataTable> jdt = dtLocGroup.getListODataTable();
            JDataTable dt=jdt.get(0);

            for (int i = 0; i < dt.totalrows; i++) {
                lstGroupName.add(dt.records.get(i).get("line_name").toString());
                hashMapLine.put(i, dt.records.get(i).get("pk").toString());
            }

            if (dt != null &&  dt.totalrows> 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstLine.setAdapter(dataAdapter);
                //  wh_name = myLstWH.getItemAtPosition(0).toString();
            } else {
                myLstLine.setAdapter(null);
                line_pk = "0";
            }
        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
        //-----------

        // onchange myLstLine
        myLstLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here

                Object pkGroup = hashMapLine.get(i);
                if (pkGroup != null) {
                    line_pk = String.valueOf(pkGroup);
                    Log.e("line_pk: ", line_pk);
                    if (!line_pk.equals("0")) {
                        Calendar c = Calendar.getInstance();
                        String month=c.get(Calendar.MONTH)+1+"";
                        if(month.length()<2){
                            month="0"+month;
                        }
                        String date=c.get(Calendar.DATE)+"";
                        if(date.length()<2){
                            date="0"+date;
                        }
                        String ymd = c.get(Calendar.YEAR) +"" + month  +  date;
                        String   l_para = "1,LG_MPOS_GET_LINE_PRODUCT,1," + line_pk+"|"+ymd;
                        try {

                            gwService networkThread = new gwService(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
                            JResultData dtLocGroup = networkThread.execute(l_para).get();
                            List<JDataTable> jdt = dtLocGroup.getListODataTable();
                            JDataTable dt=jdt.get(0);
                            db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                            db.execSQL("DELETE from line_result");
                            for (int r = 0; r < dt.totalrows; r++) {
                                String _line_pk         = dt.records.get(r).get("line_pk").toString();
                                String _line_name       = dt.records.get(r).get("line_name").toString();
                               String _work_plan_date ="";// = dt.records.get(r).get("work_plan_date").toString();
                                String _target          = dt.records.get(r).get("target").toString();
                                String _pro_time        = dt.records.get(r).get("pro_time").toString();
                                String  _prod           = dt.records.get(r).get("prod").toString();
                                String _bal             = dt.records.get(r).get("bal").toString();
                                String _r_pro           = dt.records.get(r).get("r_pro").toString();
                                String _r_real          = dt.records.get(r).get("r_real").toString();
                                String _info_wi     ="";//    = dt.records.get(r).get("info_wi").toString();
                                String _ord_qty      ="";//   = dt.records.get(r).get("ord_qty").toString();
                                String _wi_line_target_pk= dt.records.get(r).get("wi_pk").toString();
                                String _total_prod= "";//dt.records.get(r).get("total_prod").toString();

                                String countQuery = "SELECT PK FROM line_result where  line_pk='" + _line_pk + "' and del_if=0 and PO='"+_info_wi+"'";

                                Cursor cursor = db.rawQuery(countQuery, null);
                                if (cursor != null && cursor.moveToFirst()) {
                                    return;
                                }
                                db.execSQL("INSERT INTO line_result(LINE_PK,LINE_NAME,PRO_DATE,TARGET,PRO_TIME,PRO,BAL,R_PRO,ROTATION,BUYER,STYLE,PO,ORD_QTY,PRO_QTY,WI_LINE_TARGET_PK) "
                                        +"VALUES('"
                                        +_line_pk+ "','"
                                        +_line_name+ "','"
                                        +_work_plan_date+ "','"
                                        +_target+ "','"
                                        +_pro_time+ "','"
                                        +_prod+ "','"
                                        +_bal+ "','"
                                        +_r_pro+ "','"
                                        +_r_real+ "','"
                                        +" "+ "','"
                                        +" "+ "','"
                                        +_info_wi + "','"
                                        +_ord_qty + "','"
                                        +_total_prod  + "','"
                                        + _wi_line_target_pk + "');");

                            }
                            db.close();

                        } catch (Exception ex) {
                            //  alertToastLong(ex.getMessage().toString());
                            Log.e("LoadLineProduct Error: ", ex.getMessage());
                        }
                        finally {
//                            OnShowGridHeader();
                            LoadLineProduct();
                        }

                    }
                } else {
                    line_pk = "0";
                    Log.e("wi_pk: ", line_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    public  void LoadLineProduct(){


        scrollablePart = (TableLayout) rootView.findViewById(R.id.tbLineList);
        scrollablePart.removeAllViews();//remove all view child
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT,
                DrawerLayout.LayoutParams.WRAP_CONTENT);
        TableRow row = new TableRow(gwMActivity);
        db= gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        Cursor cur=db.rawQuery("select LINE_NAME,TARGET,PRO_TIME,PRO,BAL,R_PRO,ROTATION,WI_LINE_TARGET_PK,LINE_PK from line_result" , null);
        if(cur!=null)
        {
            if(cur.moveToFirst())
            {
                do
                {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(getResources().getColor(R.color.grd_border));
                    for (int j = 0; j < cur.getColumnNames().length; j++) {
                        if(j==0) {
                            row.addView(makeTableColWithText(cur.getString(j), scrollableColumnWidths[j] , fixedRowHeight, -1, -1));
                        }else if(j==2 || j==6){
                            row.addView(makeTableColWithText(cur.getString(j), scrollableColumnWidths[j], fixedRowHeight, 0, -1));
                        }
                        else if(j==cur.getColumnNames().length-1 || j==cur.getColumnNames().length-2){
                            row.addView(makeTableColWithText(cur.getString(j), 0, fixedRowHeight, 1, -1));
                        }else{
                            String val=cur.getString(j);
                            if(j==6)
                                val=val +"%";
                            row.addView(makeTableColWithText(val, scrollableColumnWidths[j], fixedRowHeight, 1, -1));
                        }
                    }
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tvTeamp;

                        @Override
                        public void onClick(View v) {

                            TableRow tr1 = (TableRow) v;
                            //  tvTeamp = (TextView) tr1.getChildAt(0); //SLIP_NO

                            line_detail = new String[tr1.getChildCount()];
                            for (int i = 0; i < tr1.getChildCount(); i++) {
                                tvTeamp = (TextView) tr1.getChildAt(i);
                                tvTeamp.setBackgroundColor(getResources().getColor(R.color.grd_bgrow_selected));
                                line_detail[i] = tvTeamp.getText().toString();

                            }

                            if (indexPrevious >= 0
                                    && indexPrevious != scrollablePart.indexOfChild(v)) {
                                TableRow trP = (TableRow) scrollablePart.getChildAt(indexPrevious);
                                for (int k = 0; k < trP.getChildCount(); k++) {
                                    tvTeamp = (TextView) trP.getChildAt(k);
                                    tvTeamp.setBackgroundColor(getResources().getColor(R.color.grd_bgrow));
                                }
                            }
                            //Update indexPrevious
                            indexPrevious = scrollablePart.indexOfChild(v);
                        }
                    });
                    scrollablePart.addView(row);
                }
                while(cur.moveToNext());
            }
        }
    }
    public void UpdateTime() {
        gwMActivity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    Calendar c = Calendar.getInstance();
                    String month=c.get(Calendar.MONTH)+1+"";
                    if(month.length()<2){
                        month="0"+month;
                    }
                    String date=c.get(Calendar.DATE)+"";
                    if(date.length()<2){
                        date="0"+date;
                    }
                    String ymd = c.get(Calendar.YEAR) + "-" + month  + "-" + date;
                    int hours = c.getTime().getHours();
                    int minutes = c.getTime().getMinutes();
                    int seconds = c.getTime().getSeconds();
                    lbDate.setText(ymd);
                    lbTime.setText(hours + ":" + minutes + ":" + seconds);
                } catch (Exception e) {
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btProResult:
                try {
                    OnPro_Sew_Result(view);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    class CountDownRunner implements Runnable {
        // @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    UpdateTime();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (Exception e) {
                }
            }
        }
    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.tbLineHeader);
        scrollablePart.removeAllViews();//remove all view child
//        scrollablePart.removeAllViews();
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(getResources().getColor(R.color.grd_border));       // row.setVerticalGravity(50);
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.line), scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.target), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.prod_time), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.prod), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.bal), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.r_plan), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.real_ration), scrollableColumnWidths[6], fixedHeaderHeight));
       // row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.buyer_style_po), scrollableColumnWidths[7], fixedHeaderHeight));
      //  row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.ord_qty), scrollableColumnWidths[8], fixedHeaderHeight));
   //     row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.prod_qty), scrollableColumnWidths[9], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("wi PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Line PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    public TextView makeTableColHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        TextView bsTextView;
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(getResources().getColor(R.color.grd_textHeader));
        bsTextView.setTextSize(16);
        bsTextView.setTypeface(null, Typeface.BOLD);
        bsTextView.setGravity(Gravity.CENTER);
        bsTextView.setBackgroundColor(getResources().getColor(R.color.grd_bgheader));
        bsTextView.setPadding(0, 2, 0, 2);

        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }

    public TextView makeTableColWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels, int textAlign, int color) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(getResources().getColor(R.color.grd_textRow));
        if (textAlign == -1) {
            bsTextView.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
            bsTextView.setPadding(1, 1, 1, 1);
        } else if (textAlign == 0) {
            bsTextView.setGravity(Gravity.CENTER|Gravity.CENTER_VERTICAL);
            bsTextView.setPadding(0, 5, 0, 5);
        } else {
            bsTextView.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
            bsTextView.setPadding(0, 5, 10, 5);
        }
        bsTextView.setTextSize(18);
        // bsTextView.setTextColor(color);
        if(color<0)
            color=getResources().getColor(R.color.grd_bgrow);
        bsTextView.setBackgroundColor(color);
        bsTextView.setPadding(6, 2, 2, 2);
        //  bsTextView.sty
        bsTextView.setLayoutParams(params);
        if(widthInPercentOfScreenWidth==0)
            bsTextView.setSingleLine(true);
        // bsTextView.setWidth(widthInPercentOfScreenWidth);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }

    protected void setLanguageApplication(){

        String strLang = "en";
        String languageApp = appPrefs.getString("language", "");
        if(languageApp!=""){
            strLang = languageApp;
        }


        Resources res = this.getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(strLang.toLowerCase());
        res.updateConfiguration(conf, dm);

        //this.setContentView(activity);
    }

}
