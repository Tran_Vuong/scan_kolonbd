package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.interfaces.frItemInquiry;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;

public class ReturnSupplier extends gwFragment implements View.OnClickListener {
    protected Boolean flagUpload=true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE=1;
    public static final String formID="17"; //Return Supplier
    Queue queue = new LinkedList();
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    String  data[] = new String[0];

    String sql = "", scan_date = "",scan_time="",unique_id="";
    String cus_name ="", cus_pk ="", wh_name ="", wh_pk ="";
    public Spinner myLstWH_In, myLstCus;
    EditText myBC;
    TextView _txtDate, _txtError, _txtTT, _txtSent, _txtRemain,_txtTotalGridBot,_txtTotalQtyBot,_txtTotalGridMid,_txtTime;
    int timeUpload=0;
    Button btnViewStt,btnList,btnInquiry,btnMakeSlip,btnDelAll;
    View rootView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_return_supplier, container, false);
   
       // this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        myBC        = (EditText) rootView.findViewById(R.id.editBC);


        _txtRemain   = (TextView) rootView.findViewById(R.id.txtRemain );
        _txtDate     = (TextView) rootView.findViewById(R.id.txtDate);
        _txtError  = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");
        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        _txtDate.setText(formattedDate);
        _txtTime=(TextView) rootView.findViewById(R.id.txtTime);

        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);

        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        if(Measuredwidth <=600) {
            _txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);
        }
        OnShowGridHeader();
        OnShowScanLog();
        OnShowScanIn();
        OnShowScanAccept();
        CountSendRecord();
        init_color();

        myBC.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        OnSaveBC();
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // this is for backspace
                    myBC.clearFocus();
                    Thread.interrupted();
                    //finish();
                    //System.exit(0);
                    //Log.e("IME_TEST", "BACK VIRTUAL");

                }
                return false;
            }
        });

        timeUpload=hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);
        return   rootView;
    }
//region --------Upload data to server ---------
    private Runnable updateDataToServer = new Runnable() {
        public void run()
        {
            if(flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload*1000);
        }
    };

    private  void init_color(){
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private void doStart() {
        db2=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        cursor2 = db2.rawQuery("select PK, ITEM_BC  from INV_TR where DEL_IF=0 and TR_TYPE='"+formID+"' and sent_yn = 'N' order by PK asc LIMIT 20",null);

        if (cursor2.moveToFirst()) {
            // Write your code here to invoke YES event
            flagUpload=false;
            //set value message
            _txtError=(TextView)rootView.findViewById(R.id.txtError);
            _txtError.setTextColor(Color.RED);
            _txtError.setText("");

            //data=new String [cursor2.getCount()];//step by step
            data=new String [1];//only one call
            int j=0;
            String para ="";
            boolean flag=false;
            do {
                flag=true;
                for(int i=0; i < cursor2.getColumnCount();i++)
                {
                    if(para.length() <= 0)
                    {
                        if(cursor2.getString(i)!= null)
                            para += cursor2.getString(i)+"|";
                        else
                            para += "|";
                    }
                    else
                    {
                        if(flag==true){
                            if(cursor2.getString(i)!= null) {
                                para += cursor2.getString(i);
                                flag=false;
                            }
                            else {
                                para += "|";
                                flag=false;
                            }
                        }
                        else{
                            if(cursor2.getString(i)!= null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }
                }
                para += "|" + deviceID;
                para += "|" + bsUserID;
                para += "*|*";

            } while (cursor2.moveToNext());
            //////////////////////////
            para += "|!"+ "LG_MPOS_UPL_SUPPLIER_RETURN";
            data[j++]=para;
            System.out.print("\n\n\n para upload: " + para);
            Log.e("para upload: ", para);

            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                task.execute(data);
                gwMActivity.alertToastShort("Send to Server");
            } else
            {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }

        }
        cursor2.close();
        db2.close();
    }
    //endregion

    //region ------- OnSaveBC ---------
    public void OnSaveBC() {
        if (myBC.getText().toString().equals("")) {
            _txtError.setTextColor(Color.RED);
            _txtError.setText("Pls Scan barcode!");
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        }
        else{
            String str_scanBC = myBC.getText().toString().toUpperCase();
            try
            {
                // check barcode exist in data
                if (hp.isExistBarcode(str_scanBC,formID))// exist data
                {
                    alertRingMedia();
                    _txtError.setText("Barcode exist in database!");
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                }
                else
                {
                    db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                    scan_date = CDate.getDateyyyyMMdd();
                    scan_time = CDate.getDateYMDHHmmss();

                    db.execSQL("INSERT INTO INV_TR(ITEM_BC,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                            + "VALUES('"
                            + str_scanBC + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "N" + "','"
                            + " " + "','"
                            + formID + "');");
                    db.close();

                }
            }
            catch (Exception ex)
            {
                _txtError.setText("Save Error!!!");
                Log.e("OnSaveBC", ex.getMessage());
            }
            finally {
                _txtError.setText("Add success!");
                myBC.getText().clear();
                myBC.requestFocus();
            }
        }
        OnShowScanLog();
    }
    //endregion

    public void onClickViewStt(View view) throws InterruptedException, ExecutionException {

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    ///////////////////////////////////////////////
    public void alertDialogYN(String title,String mess,final String _type){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onApprove")) {
                    Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    btnMSlip.setVisibility(View.GONE);
                    //_btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    //_btnApprove.setVisibility(View.GONE);
                    //ProcessApprove();
                }
                if (_type.equals("onDelAll")) {
                    ProcessDelete();
                }
                if (_type.equals("onMakeSlip")) {
                    //Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    //btnMSlip.setVisibility(View.GONE);
                    //_btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    //_btnApprove.setVisibility(View.GONE);
                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void onMakeSlip(View view){

        //Check het Barcode send to server
        if(checkRecordGridView(R.id.grdScanLog)){
            gwMActivity.alertToastLong( this.getResources().getString(R.string.tvAlertMakeSlip) );
            return;
        }
        //Check have value make slip
        if(hp.isMakeSlip(formID)==false) return;

        String title="Confirm Make Slip...";
        String mess="Are you sure you want to Make Slip ???";
        alertDialogYN(title, mess, "onMakeSlip");
    }

    public void ProcessMakeSlip()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            return;
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = " select  TR_ITEM_PK,TR_QTY, UOM, TR_LOT_NO, TR_WH_IN_PK, SUPPLIER_PK, PO_NO,EXECKEY  " +
                        " from "+
                        " (select TR_ITEM_PK, COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO,PO_NO, UOM, TR_WH_IN_PK,SUPPLIER_PK,EXECKEY " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "'  AND TR_TYPE = '"+formID+"' AND STATUS='000' and SLIP_NO='-' " +
                        " GROUP BY TR_ITEM_PK,TR_LOT_NO, PO_NO, UOM, TR_WH_IN_PK, SUPPLIER_PK,EXECKEY  )";

                cursor = db.rawQuery(sql, null);
                //cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_CODE,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, SCAN_DATE, TR_WAREHOUSE_PK,TR_LINE_PK, UNIT_PRICE   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='"+formID+"' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount()-1; k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey=cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if( execkey==null || execkey.equals("") ) {
                            int ex_item_pk=Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_wh_pk=cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK"));
                            String ex_supplier_pk=cursor.getString(cursor.getColumnIndex("SUPPLIER_PK"));
                            String ex_lot_no=cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            hp.updateExeckeyGoodsReturn(formID,ex_item_pk,ex_wh_pk,ex_supplier_pk,ex_lot_no,unique_id);

                            para += "|" + unique_id;
                        }else {
                            para += "|" + execkey;
                        }
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "|"+ deviceID;
                        para += "*|*";
                    }while (cursor.moveToNext());

                    para += "|!"+"LG_MPOS_PRO_SUPPLIER_MSLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip stock out: " + para);

                    // asyns server
                    if (CNetwork.isInternet(tmpIP, checkIP)) {
                        MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                        task.execute(data);
                    } else
                    {
                        gwMActivity.alertToastLong("Network is broken. Please check network again !");
                    }
                }
            }
            catch (Exception ex){
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }


        }
    }

    ////////////////////////////////////////////////
    public void onDelAll(View view){
        if(!hp.isCheckDelete(formID)) return;
        String title="Confirm Delete..";
        String mess="Are you sure you want to delete ???";
        alertDialogYN(title, mess, "onDelAll");
    }

    public void  ProcessDelete(){
        try{
            db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(queue.size()>0)
            {/*
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();
                data=new String [myLst.length];
                for (int i = 0; i < myLst.length; i++) {
                    db.execSQL("DELETE FROM INV_TR where TR_TYPE='"+formID+"' and pk="+myLst[i]);
                }
            */
            }else{
                //db.execSQL("DELETE FROM INV_TR where TR_TYPE='"+formID+"';");
                cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='"+formID+"' and SLIP_NO='-' and STATUS ='000' ", null);
                int count = cursor.getCount();
                String para ="";
                boolean flag=false;
                int j = 0;
                if (cursor.moveToFirst()) {
                    data = new String[1];
                    do {

                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + bsUserID;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_UPD_INV_TR_DEL";
                    data[j] = para;
                    Log.e("para update||delete: ", para);
                }

            }
        }
        catch (Exception ex){
            gwMActivity.alertToastLong("Delete All :" + ex.getMessage());
        }
        finally {
            cursor.close();
            db.close();

        }
        if (CNetwork.isInternet(tmpIP,checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        }
        else
        {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }
    }
    public  void onDeleteAllFinish(){
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(queue.size()>0)
            {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();

            }else{
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='"+formID+"';");
            }
        }catch (Exception ex){
            Log.e("Error Delete All :", ex.getMessage());
        }
        finally {
            db.close();
            OnShowScanLog();
            OnShowScanIn();
            OnShowScanAccept();
        }
    }

    //region ---------Build Grid Header--------
    public void OnShowGridHeader()
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLineName), scrollableColumnWidths[5], fixedHeaderHeight));

        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedHeaderHeight));

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTotal), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvCustomer), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }
    //endregion

    //region -------- Load data scan log------------
    public void OnShowScanLog()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+formID+"' and SENT_YN='N' order by PK desc LIMIT 20", null);// TLG_LABEL

            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //Count Grid Log
            count = hp.CountGridLog(formID);

            _txtRemain=(TextView) rootView.findViewById(R.id.txtRemain);
            _txtRemain.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
    //endregion

    //region ---------Load data scan in -----------
    public void OnShowScanIn()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, TR_WH_IN_NAME, LINE_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SENT_YN='Y' AND SCAN_DATE = '" + scan_date + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq= String.valueOf(count-i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[0], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[5], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND TR_TYPE = '"+formID+"' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtTotalGridMid=(TextView) rootView.findViewById(R.id.txtTotalMid);
            _txtTotalGridMid.setText("Total: " + count);
            _txtTotalGridMid.setTextColor(Color.BLUE);
            _txtTotalGridMid.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
//endregion

    //region -------- Load data scan accept ----------
    public void OnShowScanAccept()
    {
        Button btnMSlip=(Button)rootView.findViewById(R.id.btnMakeSlip);
        btnMSlip.setVisibility(View.VISIBLE);
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = " select  TR_ITEM_PK,  ITEM_CODE,TOTAL,TR_QTY, TR_LOT_NO, SLIP_NO, UNIT_PRICE, TR_WH_IN_NAME, SUPPLIER_NAME,TR_WH_IN_PK,EXECKEY  " +
                    " from "+
                    " (select TR_ITEM_PK, ITEM_CODE,COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO, SLIP_NO,UNIT_PRICE,TR_WH_IN_NAME,SUPPLIER_NAME,TR_WH_IN_PK,EXECKEY " +
                    " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND TR_TYPE = '"+formID+"' AND STATUS IN('OK', '000') " +
                    " GROUP BY TR_ITEM_PK,ITEM_CODE,TR_LOT_NO, SLIP_NO,UNIT_PRICE,TR_WH_IN_NAME,LINE_NAME,TR_WH_IN_PK,EXECKEY )"+
                    " ORDER BY SLIP_NO desc LIMIT 20 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[0], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TOTAL")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")), 0, fixedRowHeight,-1));

                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvLotNo = (TextView) tr1.getChildAt(4); //LOT_NO
                            String st_LotNO=tvLotNo.getText().toString();
                            TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP NO
                            String st_SlipNO=tvSlipNo.getText().toString();

                            TextView tvItemPK = (TextView) tr1.getChildAt(8); //TR_ITEM_PK
                            String item_pk=tvItemPK.getText().toString();
                            TextView tvWhPk = (TextView) tr1.getChildAt(9); //WH_PK
                            String whPK=tvWhPk.getText().toString();
                            Intent openNewActivity = new Intent(v.getContext(), gw.genumobile.com.views.PopDialogGrid.class);
                            //send data into Activity
                            openNewActivity.putExtra("TYPE", formID);
                            openNewActivity.putExtra("ITEM_PK", item_pk);
                            openNewActivity.putExtra("LOT_NO",st_LotNO);
                            openNewActivity.putExtra("WH_PK",whPK);
                            openNewActivity.putExtra("SLIP_NO",st_SlipNO);
                            openNewActivity.putExtra("USER",bsUserID);
                            //startActivity(openNewActivity);
                            startActivityForResult(openNewActivity, REQUEST_CODE);
                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND STATUS='000'";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty=0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty=_qty+Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot=(TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count+ " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 16.0);

            _txtTotalQtyBot=(TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty)+ " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 16.0);

        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanAccept: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
//endregion

    //region ---------Count Send Record---------------
    public void CountSendRecord()
    {
        flagUpload=true;
        // total scan log
        _txtSent=(TextView) rootView.findViewById(R.id.txtSent);
        _txtTotalGridMid=(TextView) rootView.findViewById(R.id.txtTotalMid);
        _txtTotalGridBot=(TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid=Integer.parseInt(_txtTotalGridMid.getText().toString().replaceAll("[\\D]",""));
        int countBot=Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]",""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Se: "+(countMid+countBot));

        _txtRemain=(TextView) rootView.findViewById(R.id.txtRemain);
        int countRe=Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]",""));

        _txtTT=(TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: " + (countMid + countBot + countRe));

    }
    public void onClickInquiry(View view) throws InterruptedException, ExecutionException {
        if(!checkRecordGridView(R.id.grdScanAccept)) return;

      /*  Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        startActivity(openNewActivity);
*/

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  frItemInquiry.newInstance(
                R.string.tv_inquiry,formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnViewStt:
                try {
                    onClickViewStt(view);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnList:
              //  onListGridMid(view);
                break;
            case R.id.btnInquiry:
                try {
                    onClickInquiry(view);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);
                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;

            default:
                break;
        }
    }
}
