package gw.genumobile.com.views;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TableRow;
import android.widget.TextView;

import gw.genumobile.com.R;
import gw.genumobile.com.db.MySQLiteOpenHelper;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link gwFragmentDialog#newInstance} factory method to
 * create an instance of this fragment.
 */
public class gwFragmentDialog extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    protected String deviceID="";
    protected String tmpIP="";
    protected Boolean checkIP=false;
    protected int[] bsColors = new int[]{0x30FF0000, 0x300000FF, 0xCCFFFF, 0x99CC33, 0x00FF99};
    protected int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};
    TextView bsTextView;
    private MediaPlayer mediaPlayer;
    protected int fixedRowHeight = 70; //70|30
    protected int fixedHeaderHeight = 80; //80|40
    protected int  Measuredheight = 0, Measuredwidth = 0;
    protected MySQLiteOpenHelper hp;
    public String bsUserPK="",bsUserID="",bsUser="";
    public String getBsUserPK() {
        return bsUserPK;
    }
    public void setBsUserPK(String bsUserPK) {
        this.bsUserPK = bsUserPK;
    }
    public String getBsUserID() {
        return bsUserID;
    }
    public void setBsUserID(String bsUserID) {
        this.bsUserID = bsUserID;
    }
    public String getBsUser() {
        return bsUser;
    }
    public void setBsUser(String bsUser) {
        this.bsUser = bsUser;
    }
    protected gwMainActivity gwMActivity;
    protected Activity gwActivity;
    public  int height=0;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    protected SharedPreferences appPrefs;

    public gwFragmentDialog() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment gwFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static gwFragmentDialog newInstance(String param1, String param2) {
        gwFragmentDialog fragment = new gwFragmentDialog();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        gwActivity = getActivity();
        gwMActivity= ((gwMainActivity)getActivity());
        appPrefs = gwActivity.getSharedPreferences("myConfig", gwActivity.MODE_PRIVATE);
        TelephonyManager telephonyManager = (TelephonyManager)gwActivity.getSystemService(Context.TELEPHONY_SERVICE);
        deviceID=telephonyManager.getDeviceId();
        //if(deviceID=="" || deviceID.equals("null") || deviceID==null) {
        //    deviceID="000000000000000";
        //}

        //Info User
        String userInfo=appPrefs.getString("userInfo", "");
        String[] strArray = userInfo.split("\\|");
        bsUserID=strArray[0];
        bsUserPK=strArray[1];
        //
        fixedRowHeight = Integer.parseInt(appPrefs.getString("rowHeight", "70"));
        fixedHeaderHeight = Integer.parseInt(appPrefs.getString("headerHeight", "80"));
        hp=new MySQLiteOpenHelper(gwMActivity);
        tmpIP= appPrefs.getString("server", "");
        checkIP=appPrefs.getBoolean("ckbInternet", Boolean.FALSE);
          Point size = new Point();
        WindowManager w = gwMActivity.getWindowManager();
        height =w.getDefaultDisplay().getHeight();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_gw, container, false);
    }
    @Override
    public void onResume()
    {
        super.onResume();
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                height-100);
        getDialog().getWindow().setGravity(Gravity.CENTER);
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public  void alertRingMedia(){
        mediaPlayer = MediaPlayer.create(gwMActivity, R.raw.msbeep01);
        mediaPlayer.start();
        //for(int j=0;j<30000;j++)
        //    mediaPlayer.stop();
    }
    public  void alertRingMedia2(){
        //mediaPlayer = MediaPlayer.create(this,   this.getResources().getIdentifier("msbeep02.mp3","assets", this.getPackageName()));// R.drawable.msbeep02);
        mediaPlayer = MediaPlayer.create(gwMActivity, R.raw.msbeep02);
        mediaPlayer.start();
        //for(int j=0;j<30000;j++)
        //    mediaPlayer.stop();
    }

    public TextView makeTableColHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.BLACK);
        bsTextView.setTextSize(18);
        bsTextView.setGravity(Gravity.CENTER);
        bsTextView.setBackgroundColor(Color.parseColor("#B0E2FF"));
        bsTextView.setPadding(0, 2, 0, 2);
        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }

    public TextView makeTableColWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels,int textAlign) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.BLACK);
        if (textAlign == -1) {
            bsTextView.setGravity(Gravity.LEFT);
            bsTextView.setPadding(10, 5, 0, 5);
        } else if (textAlign == 0) {
            bsTextView.setGravity(Gravity.CENTER);
            bsTextView.setPadding(0, 5, 0, 5);
        } else {
            bsTextView.setGravity(Gravity.RIGHT);
            bsTextView.setPadding(0, 5, 10, 5);
        }
        bsTextView.setTextSize(16);
        bsTextView.setBackgroundColor(-1);

        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;


    }
}
