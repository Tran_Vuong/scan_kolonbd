package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.models.gwfunction;
import gw.genumobile.com.views.PopDialogGrid;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.GridListViewMid;
import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;


public class StockTransfer extends gwFragment implements View.OnClickListener {
    protected Boolean flagUpload = true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;
    //int[] colors = new int[]{0x30FF0000, 0x300000FF, 0xCCFFFF, 0x99CC33, 0x00FF99};
    public static final String formID = "9";
    Queue queue = new LinkedList();
    Queue queueMid = new LinkedList();
    Queue queueMidBC = new LinkedList();
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    String data[] = new String[0];
    Button btnViewStt, btnSelectMid, btnList, btnInquiry, btnMakeSlip, btnDelAll, btnDelBC, btnListBot;
    String sql = "", scan_date = "", scan_time = "", unique_id = "";
    String out_wh_name = "", out_wh_pk = "", in_wh_name = "", in_wh_pk = "", type_name = "", type_pk = "";
    String out_loc_name = "", out_loc_pk = "", in_loc_name = "", in_loc_pk = "";
    public Spinner myLstWH_In, myLstWH_Out, myLstType, myLstLocOut, myLstLocIn;
    EditText myBC;
    TextView _txtDate, _txtError, _txtTT, _txtSent, _txtRemain, _txtTotalGridBot, _txtTotalQtyBot, _txtTotalGridMid, _txtTime;
    int timeUpload = 0;
    HashMap hashMapType = new HashMap();
    HashMap hashMapWHOut = new HashMap();
    HashMap hashMapWHIn = new HashMap();
    HashMap hashMapLocOut = new HashMap();
    HashMap hashMapLocIn = new HashMap();
    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_stock_transfer, container, false);
        //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnSelectMid = (Button) rootView.findViewById(R.id.btnSelectMid);
        btnSelectMid.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnDelBC = (Button) rootView.findViewById(R.id.btnDelBC);
        btnDelBC.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);

        myBC = (EditText) rootView.findViewById(R.id.editBC);
        myLstWH_Out = (Spinner) rootView.findViewById(R.id.lstWH_Out);
        myLstWH_In = (Spinner) rootView.findViewById(R.id.lstWH_In);
        myLstType = (Spinner) rootView.findViewById(R.id.lstType);
        myLstLocOut = (Spinner) rootView.findViewById(R.id.lstLocOut);
        myLstLocIn = (Spinner) rootView.findViewById(R.id.lstLocIn);

        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        _txtDate = (TextView) rootView.findViewById(R.id.txtDate);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");
        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        _txtDate.setText(formattedDate);
        _txtTime = (TextView) rootView.findViewById(R.id.txtTime);
        if (Measuredwidth <= 600) {
            _txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);
        }
        OnShowGridHeader();
        OnShowScanLog();
        OnShowScanIn();
        OnShowScanAccept();
        CountSendRecord();
        LoadTransferType();
        init_color();
        //LoadOutWH();
        //LoadInWH();


        myBC.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        OnSaveBC();
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // this is for backspace
                    myBC.clearFocus();
                    Thread.interrupted();
                    //Log.e("IME_TEST", "BACK VIRTUAL");
                }
                return false;
            }
        });


        timeUpload = hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);
        return rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run() {
            if (flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload * 1000);
        }
    };

    private void init_color() {
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private void doStart() {
        flagUpload = false;
        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        cursor2 = db2.rawQuery("select PK, ITEM_BC,TR_WH_OUT_PK,TR_WH_IN_PK,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK  from INV_TR where DEL_IF=0 and TR_TYPE='" + formID + "' and sent_yn = 'N' order by PK asc LIMIT 10", null);
        System.out.print("\n\n\n cursor2.count: " + String.valueOf(cursor2.getCount()));
        //data=new String [cursor2.getCount()];

        if (cursor2.moveToFirst()) {
            // Write your code here to invoke YES event

            //set value message
            _txtError = (TextView) rootView.findViewById(R.id.txtError);
            _txtError.setTextColor(Color.RED);
            _txtError.setText("");

            //data=new String [cursor2.getCount()];//step by step
            data = new String[1];//only one call
            int j = 0;
            String para = "";
            boolean flag = false;
            do {
                flag = true;
                for (int i = 0; i < cursor2.getColumnCount(); i++) {
                    if (para.length() <= 0) {
                        if (cursor2.getString(i) != null)
                            para += cursor2.getString(i) + "|";
                        else
                            para += "|";
                    } else {
                        if (flag == true) {
                            if (cursor2.getString(i) != null) {
                                para += cursor2.getString(i);
                                flag = false;
                            } else {
                                para += "|";
                                flag = false;
                            }
                        } else {
                            if (cursor2.getString(i) != null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }
                }
                para += "|" + deviceID;
                para += "|" + bsUserID;
                para += "*|*";

            } while (cursor2.moveToNext());
            //////////////////////////
            para += "|!" + gwfunction.StockTranfer_UPLOAD.getValue();
            data[j++] = para;
            System.out.print("\n\n\n para upload: " + para);
            Log.e("para upload: ", para);

            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                task.execute(data);
                gwMActivity.alertToastShort(getString(R.string.send_server));
            } else {
                flagUpload = true;
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }

        } else {
            flagUpload = true;
        }
        cursor2.close();
        db2.close();
    }


    public void OnSaveBC() {
        if (myBC.getText().toString().equals("")) {
            _txtError.setTextColor(Color.RED);
            _txtError.setText(getString(R.string.pls_scanBC));
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        }
        if (out_wh_pk.equals("0")) {
            gwMActivity.alertToastShort("Please check Out WH!");
            _txtError.setTextColor(Color.RED);
            _txtError.setText("Please check Out WH!");
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        }
        if (in_wh_pk.equals("0")) {
            gwMActivity.alertToastShort("Please check In WH!");
            _txtError.setTextColor(Color.RED);
            _txtError.setText("Please check In WH!");
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        } else {
            String str_scanBC = myBC.getText().toString().toUpperCase();

            try {
                boolean isExists = hp.isExistBarcode(str_scanBC, formID);// check barcode exist in data
                if (isExists)// exist data
                {
                    alertRingMedia();
                    _txtError.setText(getString(R.string.BC_exist));
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                } else {
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                    scan_date = CDate.getDateyyyyMMdd();
                    scan_time = CDate.getDateYMDHHmmss();

                    db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_OUT_PK,TR_WH_OUT_NAME,TR_WH_IN_PK,TR_WH_IN_NAME,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,BC_TYPE,TR_TYPE," +
                            "TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_OUT_NAME,TLG_IN_WHLOC_IN_PK,TLG_IN_WHLOC_IN_NAME) "
                            + "VALUES('"
                            + str_scanBC + "',"
                            + out_wh_pk + ",'"
                            + out_wh_name + "',"
                            + in_wh_pk + ",'"
                            + in_wh_name + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "N" + "','"
                            + " " + "','"
                            + type_pk + "','"
                            + formID + "','"
                            + out_loc_pk + "','"
                            + out_loc_name + "','"
                            + in_loc_pk + "','"
                            + in_loc_name + "');");

                    _txtError.setText("Add success!");
                }
            } catch (Exception ex) {
                _txtError.setText(getString(R.string.save_error));
                Log.e("OnSaveBC", ex.getMessage());
            } finally {
                db.close();
                myBC.getText().clear();
                myBC.requestFocus();
            }
        }
        OnShowScanLog();
    }

    public void onMakeSlip(View view) {
        //Check het Barcode send to server
        if (checkRecordGridView(R.id.grdScanLog)) {
            gwMActivity.alertToastLong(this.getResources().getString(R.string.tvAlertMakeSlip));
            return;
        }
        //Check have value make slip
        if (hp.isMakeSlip(formID) == false) return;
//        if(!checkRecordGridView(R.id.grdScanAccept)) return;

        String title = getString(R.string.tvConfirmMakeSlip);
        String mess = getString(R.string.mesConfirmMakeSlip);
        alertDialogYN(title, mess, "onMakeSlip");
    }

    public void ProcessMakeSlip() {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if (queue.size() > 0) {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data = new String[1];
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j = 0;
                String para = "";
                boolean flag = false;
                for (int i = 0; i < myLst.length; i++) {
                    flag = true;
                    cursor = db.rawQuery("select  TR_ITEM_PK,TR_QTY, UOM, TR_LOT_NO, TR_WH_OUT_PK, TR_WH_IN_PK,UNIT_PRICE,BC_TYPE,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK,EXECKEY "
                            + "   from INV_TR where del_if=0 and SLIP_NO IN ('-','') and TR_TYPE='" + formID + "' and pk=" + myLst[i], null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_wh_pk = cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK"));
                            String ex_in_wh_pk = cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK"));
                            String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            String ex_bc_type = cursor.getString(cursor.getColumnIndex("BC_TYPE"));
                            hp.updateExeckeyStTransfer(formID, ex_item_pk, ex_wh_pk, ex_in_wh_pk, ex_lot_no, ex_bc_type, unique_id);

                            para += "|" + unique_id;
                        } else {
                            para += "|" + execkey;
                        }
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;


                    }
                    para += "*|*";
                }
                para += "|!" + gwfunction.StockTranfer_MAKESLIP.getValue();
                data[j] = para;
                System.out.print("\n\n ******para make slip stock out: " + para);

            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
        } else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = " select  TR_ITEM_PK,TR_QTY, UOM, TR_LOT_NO, TR_WH_OUT_PK, TR_WH_IN_PK,UNIT_PRICE,BC_TYPE,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK,EXECKEY  " +
                        " from " +
                        " (select TR_ITEM_PK, COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO,UNIT_PRICE, UOM, TR_WH_OUT_PK,TR_WH_IN_PK,BC_TYPE,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK,EXECKEY " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "'  AND TR_TYPE = '" + formID + "' AND STATUS='000' and SLIP_NO IN ('-','') " +
                        " GROUP BY TR_ITEM_PK,TR_LOT_NO, UNIT_PRICE, UOM, TR_WH_OUT_PK,TR_WH_IN_PK,BC_TYPE,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK,EXECKEY  )";

                cursor = db.rawQuery(sql, null);
                //cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_CODE,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, SCAN_DATE, TR_WH_OUT_PK,TR_WH_IN_PK, UNIT_PRICE   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='"+formID+"' ", null);
                int count = cursor.getCount();
                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_wh_pk = cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK"));
                            String ex_in_wh_pk = cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK"));
                            String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            String ex_bc_type = cursor.getString(cursor.getColumnIndex("BC_TYPE"));
                            hp.updateExeckeyStTransfer(formID, ex_item_pk, ex_wh_pk, ex_in_wh_pk, ex_lot_no, ex_bc_type, unique_id);

                            para += "|" + unique_id;
                        } else {
                            para += "|" + execkey;
                        }
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!" +gwfunction.StockTranfer_MAKESLIP.getValue();
                    data[j] = para;
                    System.out.print("\n\n ******para make slip stock out: " + para);
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                task.execute(data);
            } else {
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }

        }
    }

    public void onClickViewStt(View view){

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ////////////////////////////////////////////////
    //region ---------Select Mid Status 005-------
    public void onClickSelectMid(View view){
        final Object[] myLst = queueMid.toArray();
        final Object[] myLstStatus = queueMidBC.toArray();
        if (myLst.length <= 0) return;
        String str = "Are you sure you want to select ";

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Select ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);

        // Setting Negative "YES" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                try {

                    queueMid = new LinkedList();
                    queueMidBC = new LinkedList();
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                    for (int i = 0; i < myLst.length; i++) {
                        String val = myLst[i].toString();
                        String valStatus = myLstStatus[i].toString();
                        String _pk = valStatus.split("\\|")[0].toString();
                        String _status = valStatus.split("\\|")[1].toString();
                        if (_status.equals("005")) {
                            sql = "UPDATE INV_TR set  REMARKS='Not FIFO',SLIP_NO =''   where  PK = " + val;
                            db.execSQL(sql);
                        }
                        /*
                        if(_status.equals("006")) {
                            //region ----check 006
                            sql="UPDATE INV_TR set  REMARKS='LotNO Wrong Format'   where  PK = " + _pk;
                            db.execSQL(sql);
                            //.endregion
                        }
                        if(_status.equals("008"))
                        {
                            sql="UPDATE INV_TR set STATUS = '000', REMARKS='Qty larger'  where PK = " + _pk;
                            db.execSQL(sql);

                            OnShowScanIn();
                            OnShowScanAccept();
                        }
                        */
                    }
                    ProcessStatus005(myLst);

                } catch (Exception ex) {
                    gwMActivity.alertToastLong("Select Mid :" + ex.getMessage());
                } finally {
                    db.close();
                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }

    public void ProcessStatus005(Object[] myLst) {
        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        data = new String[1];
        int j = 0;
        String para = "";
        for (int rw = 0; rw < myLst.length; rw++) {

            String val = myLst[rw].toString();
            Log.e("005", val);
            sql = "select PK, ITEM_BC,TR_WH_OUT_PK,TR_WH_IN_PK,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK  from INV_TR where DEL_IF=0  and PK = " + val;
            cursor2 = db2.rawQuery(sql, null);
            if (cursor2.moveToFirst()) {

                boolean flag = false;
                do {
                    // labels.add(cursor.getString(1));
                    flag = true;
                    for (int i = 0; i < cursor2.getColumnCount(); i++) {
                        if (para.length() <= 0) {
                            if (cursor2.getString(i) != null)
                                para += cursor2.getString(i) + "|";
                            else
                                para += "|";
                        } else {

                            if (flag == true) {
                                if (cursor2.getString(i) != null) {
                                    para += cursor2.getString(i);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor2.getString(i) != null)
                                    para += "|" + cursor2.getString(i);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "*|*";

                } while (cursor2.moveToNext());

            }
        }
        //////////////////////////
        para += "|!" + "LG_MPOS_UPLOAD_ST_TRANSFER_005";
        data[j++] = para;
        System.out.print("\n\n\n para upload: " + para);

        cursor2.close();
        db2.close();

        ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
        task.execute(data);
    }

    //endregion
    ////////////////////////////////////////////////
    public void onClickInquiry(View view){
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        startActivity(openNewActivity);
    }

    ///////////////////////////////////////////////
    public void onListGridMid(View view){
        if (!checkRecordGridView(R.id.grdScanIn)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  frGridListViewMid.newInstance(1,formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    ///////////////////////////////////////////////
    public void onListGridBot(View view){
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new GridListViewBot();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");

    }


    public void onDelAll(View view) {
        if (!hp.isCheckDelete(formID)) return;

        String title = getString(R.string.tvConfirmDelete);
        String mess = getString(R.string.mesConfirmDelete);
        alertDialogYN(title, mess, "onDelAll");
    }

    public void ProcessDelete() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();
                data = new String[myLst.length];
                for (int i = 0; i < myLst.length; i++) {
                    db.execSQL("DELETE FROM INV_TR where TR_TYPE='" + formID + "' and pk=" + myLst[i]);
                }

            } else {
                //db.execSQL("DELETE FROM INV_TR where TR_TYPE='"+formID+"';");
                cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "' and SLIP_NO IN ('-','') and STATUS ='000' ", null);
                int count = cursor.getCount();
                String para = "";
                boolean flag = false;
                int j = 0;
                if (cursor.moveToFirst()) {
                    data = new String[1];
                    do {

                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + bsUserID;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!"+gwfunction.Core_UPD_INV_TR_DEL.getValue() ;
                    data[j] = para;
                    Log.e("para update||delete: ", para);
                }
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("Delete All :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        } else {
            gwMActivity.alertToastLong(getString(R.string.network_broken));
        }
    }

    public void onDeleteAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();

            } else {
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='" + formID + "';");
            }
        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            OnShowScanLog();
            OnShowScanIn();
            OnShowScanAccept();
        }
    }

    public void LoadTransferType() {
        String l_para = "1,LG_MPOS_GET_TRANS_TYPE," + bsUserPK + "|SASY3000";

        try {
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String dataWHGroup[][] = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < dataWHGroup.length; i++) {
                lstGroupName.add(dataWHGroup[i][1].toString());
                hashMapType.put(i, dataWHGroup[i][0]);
            }

            if (dataWHGroup != null && dataWHGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstType.setAdapter(dataAdapter);
                type_name = myLstType.getItemAtPosition(0).toString();
            }
        } catch (Exception ex) {
            Log.e("Searh Error: ", ex.getMessage());
        }

        //Even Choose Group
        myLstType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {

                type_name = myLstType.getItemAtPosition(i).toString();
                Log.e("Type_name", type_name);
                Object pkGroup = hashMapType.get(i);
                if (pkGroup != null) {
                    type_pk = String.valueOf(pkGroup);
                    Log.e("Type_PK: ", type_pk);
                    if (!type_pk.equals("0")) {
                        LoadOutWH();
                    } else {
                        type_pk = "0";
                        Log.e("Type_PK: ", type_pk);
                        LoadOutWH();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    private void LoadOutWH() {
        try {
            String l_para = "1,LG_MPOS_M010_GET_WH_OUT_USER," + bsUserPK + "|wh_ord_4|" + type_pk;
            String dataWhOutGroup[][] = getDataConnectThread(gwMActivity, l_para);
            List<String> lstNameWhOut = new ArrayList<String>();
            for (int i = 0; i < dataWhOutGroup.length; i++) {
                lstNameWhOut.add(dataWhOutGroup[i][1].toString());
                hashMapWHOut.put(i, dataWhOutGroup[i][0]);
            }
            if (dataWhOutGroup != null && dataWhOutGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity, android.R.layout.simple_spinner_item, lstNameWhOut);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstWH_Out.setAdapter(dataAdapter);
                out_wh_name = myLstWH_Out.getItemAtPosition(0).toString();
            } else {
                myLstWH_Out.setAdapter(null);
                out_wh_pk = "";
                Log.e("out_wh_pk", out_wh_pk);
                LoadInWH();
            }

            // onchange spinner wh out
            myLstWH_Out.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                    // your code here
                    out_wh_name = myLstWH_Out.getItemAtPosition(i).toString();
                    Log.e("OUTWH_name", out_wh_name);
                    Object pkOutWh = hashMapWHOut.get(i);
                    if (pkOutWh != null) {
                        out_wh_pk = String.valueOf(pkOutWh);
                        Log.e("OutWH_PK: ", out_wh_pk);
                        if (!out_wh_pk.equals("")) {
                            if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
                                TextView tvLocOut = (TextView) rootView.findViewById(R.id.tvLocOut);
                                tvLocOut.setVisibility(View.VISIBLE);
                                myLstLocOut.setVisibility(View.VISIBLE);
                                LoadLocationOut();
                            } else {
                                TextView tvLocOut = (TextView) rootView.findViewById(R.id.tvLocOut);
                                tvLocOut.setVisibility(View.GONE);
                                myLstLocOut.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        out_wh_pk = "";
                        Log.e("OutWH_PK ", out_wh_pk);
                    }
                    LoadInWH();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });
        } catch (Exception ex) {

        }
    }

    private void LoadInWH() {
        try {
            String l_para = "1,LG_MPOS_M010_GET_WH_IN_USER," + bsUserPK + "|" + out_wh_pk + "|wh_ord_3|" + type_pk;
            String dataWhInGroup[][] = getDataConnectThread(gwMActivity, l_para);
            List<String> lstNameWhIn = new ArrayList<String>();
            for (int i = 0; i < dataWhInGroup.length; i++) {
                lstNameWhIn.add(dataWhInGroup[i][1].toString());
                hashMapWHIn.put(i, dataWhInGroup[i][0]);
            }
            if (dataWhInGroup != null && dataWhInGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstNameWhIn);
                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // attaching data adapter to spinner
                myLstWH_In.setAdapter(dataAdapter);
                in_wh_name = myLstWH_In.getItemAtPosition(0).toString();
            } else {
                myLstWH_In.setAdapter(null);
                in_wh_pk = "";
                Log.e("in_wh_pk", in_wh_pk);
            }

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.toString());
        }

        // onchange spinner wh in
        myLstWH_In.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                in_wh_name = myLstWH_In.getItemAtPosition(i).toString();

                Log.e("In WH_name", in_wh_name);
                Object pkInWh = hashMapWHIn.get(i);
                if (pkInWh != null) {
                    in_wh_pk = String.valueOf(pkInWh);
                    Log.e("In WH_PK ", in_wh_pk);
                    if (!in_wh_pk.equals("")) {
                        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
                            TextView tvLoc = (TextView) rootView.findViewById(R.id.tvLocIn);
                            tvLoc.setVisibility(View.VISIBLE);
                            myLstLocIn.setVisibility(View.VISIBLE);
                            LoadLocationIn();
                        } else {
                            TextView tvLocOut = (TextView) rootView.findViewById(R.id.tvLocIn);
                            tvLocOut.setVisibility(View.GONE);
                            myLstLocIn.setVisibility(View.GONE);
                        }
                    }
                } else {
                    in_wh_pk = "";
                    Log.e("In WH_PK ", in_wh_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    private void LoadLocationOut() {
        String l_para = "1,LG_MPOS_GET_LOC," + out_wh_pk;
        try {

            String dtLocOutGroup[][] = getDataConnectThread(gwMActivity, l_para);
            List<String> lstLocOutName = new ArrayList<String>();

            for (int i = 0; i < dtLocOutGroup.length; i++) {
                lstLocOutName.add(dtLocOutGroup[i][1].toString());
                hashMapLocOut.put(i, dtLocOutGroup[i][0]);
            }

            if (dtLocOutGroup != null && dtLocOutGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstLocOutName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstLocOut.setAdapter(dataAdapter);
                out_loc_name = myLstLocOut.getItemAtPosition(0).toString();
            } else {
                myLstLocOut.setAdapter(null);
                out_loc_pk = "";
                Log.e("Loc_PK: ", out_loc_pk);
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage().toString());
            Log.e("Loc Error: ", ex.getMessage());
        }
        //-----------
        // onchange spinner LOC
        myLstLocOut.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int i, long id) {
                // your code here
                out_loc_name = myLstLocOut.getItemAtPosition(i).toString();
                Log.e("Loc_name", out_loc_name);
                Object pkGroup = hashMapLocOut.get(i);
                if (pkGroup != null) {
                    out_loc_pk = String.valueOf(pkGroup);
                    Log.e("Loc_PK: ", out_loc_pk);
                    LoadLocationIn();

                } else {
                    out_loc_pk = "";
                    Log.e("Loc_PK: ", out_loc_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    private void LoadLocationIn() {
        String l_para = "1,LG_MPOS_GET_LOC_IN," + in_wh_pk + "|" + out_wh_pk + "|" + out_loc_pk;
        try {

            String dtLocInGroup[][] = getDataConnectThread(gwMActivity, l_para);
            List<String> lstLocInName = new ArrayList<String>();

            for (int i = 0; i < dtLocInGroup.length; i++) {
                lstLocInName.add(dtLocInGroup[i][1].toString());
                hashMapLocIn.put(i, dtLocInGroup[i][0]);
            }

            if (dtLocInGroup != null && dtLocInGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstLocInName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstLocIn.setAdapter(dataAdapter);
                in_loc_name = myLstLocIn.getItemAtPosition(0).toString();
            } else {
                myLstLocIn.setAdapter(null);
                in_loc_pk = "";
                Log.e("Loc_PK: ", in_loc_pk);
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage().toString());
            Log.e("Loc Error: ", ex.getMessage());
        }
        //-----------
        // onchange spinner LOC
        myLstLocIn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int i, long id) {
                // your code here
                in_loc_name = myLstLocIn.getItemAtPosition(i).toString();
                Log.e("Loc_name", in_loc_name);
                Object pkGroup = hashMapLocIn.get(i);
                if (pkGroup != null) {
                    in_loc_pk = String.valueOf(pkGroup);
                    Log.e("Loc_PK: ", in_loc_pk);

                } else {
                    in_loc_pk = "";
                    Log.e("Loc_PK: ", in_loc_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhOut), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhIn), scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedHeaderHeight));

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTotal), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhOut), scrollableColumnWidths[4], fixedHeaderHeight));
        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
            row.addView(makeTableColHeaderWithText("Loc Out", scrollableColumnWidths[4], fixedHeaderHeight));
        }
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhIn), scrollableColumnWidths[4], fixedHeaderHeight));
        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
            row.addView(makeTableColHeaderWithText("Loc In", scrollableColumnWidths[4], fixedHeaderHeight));
        }

        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));


        scrollablePart.addView(row);
    }


    // show data scan log
    public void OnShowScanLog() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' order by PK desc LIMIT 20", null);// TLG_LABEL

            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
            _txtRemain.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanIn() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select PK, ITEM_BC, ITEM_CODE,  STATUS,TR_LOT_NO, SLIP_NO, INCOME_DATE, CHARGER, TR_WH_OUT_NAME, TR_WH_IN_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SENT_YN='Y' AND SCAN_DATE = '" + scan_date + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {

                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[0], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, 0));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvStatus = (TextView) tr1.getChildAt(3); //STATUS
                            String str_Status = tvStatus.getText().toString();

                            if (str_Status.equals("005")) {
                                TextView tvBC = (TextView) tr1.getChildAt(1);
                                TextView tvPK = (TextView) tr1.getChildAt(10);
                                String value = tvBC.getText().toString() + "|" + str_Status;
                                String valuePK = tvPK.getText().toString();
                                if (hp.isExistBarcode(tvBC.getText().toString(), formID)) {
                                    gwMActivity.onPopAlert(tvBC.getText().toString() + "ton tai luoi thu 3 !!!");
                                    return;
                                }
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255, 99, 71));
                                }
                                if (queueMid.size() > 0) {

                                    if (queueMidBC.contains(value)) {
                                        Log.e("Contain Click", value);
                                        queueMid.remove(valuePK);
                                        queueMidBC.remove(value);

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK);
                                        }
                                        duplicate = true;
                                    }
                                }
                                if (!duplicate) {
                                    //Log.e("PK Click", valuePK);
                                    queueMid.add(valuePK);
                                    queueMidBC.add(value);
                                }
                            }
                            gwMActivity.alertToastShort(queueMid.size() + "");
                        }
                    });
                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND TR_TYPE = '" + formID + "' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
            _txtTotalGridMid.setText("Total: " + count);
            _txtTotalGridMid.setTextColor(Color.BLUE);
            _txtTotalGridMid.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan accept
    public void OnShowScanAccept() {
        Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMSlip.setVisibility(View.VISIBLE);
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = " select TR_ITEM_PK, ITEM_CODE,COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_IN_NAME,TR_WH_OUT_NAME,TR_WH_OUT_PK,BC_TYPE,EXECKEY,TLG_IN_WHLOC_OUT_NAME,TLG_IN_WHLOC_IN_NAME " +
                    " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND TR_TYPE = '" + formID + "' AND STATUS IN('OK', '000') " +
                    " GROUP BY TR_ITEM_PK,ITEM_CODE,TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_OUT_NAME,TR_WH_IN_NAME,TR_WH_OUT_PK,BC_TYPE,EXECKEY " +
                    " ORDER BY EXECKEY desc,TR_ITEM_PK  LIMIT 20 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[0], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TOTAL")), scrollableColumnWidths[2], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_OUT_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    } else {
                        row.addView(makeTableColWithText("", 0, fixedRowHeight, -1));
                    }
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    } else {
                        row.addView(makeTableColWithText("", 0, fixedRowHeight, -1));
                    }
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight, -1));

                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK")), 0, fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")), 0, fixedRowHeight, -1));

                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            onShowClickGridBot(v);
                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND STATUS='000'";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 17.0);

        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanAccept: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public  void onShowClickGridBot(View v){
        boolean duplicate = false;

        TableRow tr1 = (TableRow) v;
        TextView tvLotNo = (TextView) tr1.getChildAt(4); //LOT_NO
        String st_LotNO = tvLotNo.getText().toString();
        TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP NO
        String st_SlipNO = tvSlipNo.getText().toString();

        TextView tvItemPK = (TextView) tr1.getChildAt(10); //TR_ITEM_PK
        String item_pk = tvItemPK.getText().toString();
        TextView tvWhPk = (TextView) tr1.getChildAt(12); //WH_PK
        String whPK = tvWhPk.getText().toString();

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new PopDialogGrid();
        Bundle args = new Bundle();

        args.putString("TYPE", formID);
        args.putString("ITEM_PK", item_pk);
        args.putString("LOT_NO",st_LotNO);
        args.putString("WH_PK",whPK);
        args.putString("SLIP_NO",st_SlipNO);
        args.putString("USER",bsUserID);
        dialogFragment.setArguments(args);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    //////////////////////////
    // check data inv_tr
    public int Check_Exist_PK(String l_bc_item) {
        int countRow = 0;
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String countQuery = "SELECT PK FROM INV_TR where tr_type='" + formID + "' AND del_if=0 AND status='000' and ITEM_BC ='" + l_bc_item + "' ";
            // db = this.getReadableDatabase();
            cursor = db.rawQuery(countQuery, null);
            // Check_Exist_PK=cursor.getCount();
            if (cursor != null && cursor.moveToFirst()) {
                countRow = cursor.getCount();
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("Check_Exist_PK :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return countRow;
    }

    public void CountSendRecord() {
        flagUpload = true;
        // total scan log
        _txtSent = (TextView) rootView.findViewById(R.id.txtSent);
        _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid = Integer.parseInt(_txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        int countBot = Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Send: " + (countMid + countBot));

        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]", ""));

        _txtTT = (TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: " + (countMid + countBot + countRe));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiem tra coi trong requestCode =REQUEST_CODE_INPUT hay khong
        //V� ta c� th? m? Activity v?i nh?ng RequestCode khac nhau
        if(requestCode==REQUEST_CODE)
        {
            //Kiem tra ResultCode tra ve, cai nay ? ben InputDataActivity truyen ve
            OnShowScanAccept();
            switch(resultCode)
            {
                case RESULT_CODE:

                    break;
            }
        }
    }

    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onApprove")) {
                    Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    btnMSlip.setVisibility(View.GONE);
                    //_btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    //_btnApprove.setVisibility(View.GONE);
                    //ProcessApprove();
                }
                if (_type.equals("onDelAll")) {
                    ProcessDelete();
                }
                if (_type.equals("onMakeSlip")) {
                    //Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    //btnMSlip.setVisibility(View.GONE);
                    //_btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    //_btnApprove.setVisibility(View.GONE);
                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
                onClickInquiry(view);

                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnList:
                onListGridMid(view);

                break;
            case R.id.btnSelectMid:

                onClickSelectMid(view);
                break;
            case R.id.btnViewStt:
                onClickViewStt(view);

                break;
            case R.id.btnDelAll:

                onDelAll(view);

                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);

                break;
            default:
                break;
        }
    }

}
