package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.GridListViewMid;
import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;

public class GoodsPartial extends gwFragment implements View.OnClickListener{
    protected Boolean flagUpload=true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE=1;
    public static final String formID="11";//GOODS Partial;

    Queue queue = new LinkedList();
    Queue dlgqueue = new LinkedList();
    Queue queueMid = new LinkedList();
    String reqPK = "";// TLG_GD_REQ_M_PK
    String slipNO =  ""; // SLIP_NO
    String _reqQty =  ""; // REQ_QTY
    String _whPK =  ""; // OUT_WH_PK
    String _reqBal =  ""; // BALANCE

    SQLiteDatabase db=null,db2=null;
    Cursor cursor,cursor2;
    private Handler customHandler = new Handler();
    String  data[] = new String[0];

    public Editable str_scan;
    EditText bc1,tr_qty;
    TextView reqWH,msg_error,txtDate,recyclableTextView,txtTT, txtSentLog, txtRem,txtTotalGridMid,txtTotalGridBot,txtTotalQtyBot,_txtTime;
    EditText bc2,reqNo,reqQty,reqBal;
    Calendar c;
    SimpleDateFormat df;
    String[][] myLstAccept = new String[0][0];
    String sql = "", scan_date = "",scan_time="",unique_id="";
    String _drCurrentScan = null,lbl_SlipNo1_Tag="",parentBC="";
    String P_ITEM_BC,P_ITEM_CODE,P_ITEM_NAME,P_TR_LOT_NO,P_TR_QTY,P_SLIP_NO,P_TR_ITEM_PK,P_REQ_D_PK,P_WH_PK,P_TR_WH_IN_NAME;
    float total_qty=0;
    int timeUpload=0;
    Button btnGoodsPartial,btnViewStt,btnSelectMid,btnList,btnInquiry,btnMakeSlip,btnDelAll,btnListBot;
    View rootView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_goods_partial,container, false);
        //lock screen
        //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        btnGoodsPartial = (Button) rootView.findViewById(R.id.btnGoodsPartial);
        btnGoodsPartial.setOnClickListener(this);
        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnSelectMid = (Button) rootView.findViewById(R.id.btnSelectMid);
        btnSelectMid.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);

        txtDate     = (TextView)  rootView.findViewById(R.id.txtDate);
        bc1 =(EditText) rootView.findViewById(R.id.edit_Bc1);
        msg_error=(TextView) rootView.findViewById(R.id.txtError);
        msg_error.setText("");

        bc2 =(EditText) rootView.findViewById(R.id.edit_Bc2);
        reqNo =(EditText) rootView.findViewById(R.id.edit_reqNo);
        reqQty =(EditText) rootView.findViewById(R.id.edit_reqQty);
        reqBal =(EditText) rootView.findViewById(R.id.edit_reqBal);
        reqWH =(TextView) rootView.findViewById(R.id.txt_GD_WH_PK);

        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        txtDate.setText(formattedDate);
        _txtTime=(TextView)  rootView.findViewById(R.id.txtTime);
        if(Measuredwidth <=600) {
            txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);
        }
        OnShowGWHeader();
        OnShowScanLog();
        OnShowScanIn();
        OnShowScanAccept();
        init_color();

        bc1.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        msg_error = (TextView)  rootView.findViewById(R.id.txtError);
                        msg_error.setTextColor(Color.RED);
                        msg_error.setTextSize((float) 16.0);
                        if (bc1.getText().toString().equals("")) {
                            msg_error.setText("Pls Scan barcode!");
                            bc1.requestFocus();
                        } else {

                            //if (slipNO.equals(""))
                            //     OnGetSlipNo();
                            // else
                            //     OnSaveBC();


                            OnSaveData();
                        }
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // gwMActivity is for backspace
                    bc1.clearFocus();
                    //finish();
                    //System.exit(0);
                    //Log.e("IME_TEST", "BACK VIRTUAL");

                }
                return false;
            }

        });

        timeUpload=hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload*1000);
        
        return rootView;
    }
    private Runnable updateDataToServer = new Runnable() {
        public void run()
        {
            Log.e("flagUpload",flagUpload.toString());
            if(flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload*1000);
        }
    };

    private  void init_color(){
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout)  rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout)  rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout)  rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout)  rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout)  rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout)  rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

   

    public void OnGetSlipNo()
    {
        String str_req_no=bc1.getText().toString();
        try
        {
            if (str_req_no.equals(""))
            {
                msg_error.setText("Pls Scan Slip No!");
                return;
            }

            //scan slip no
            List<String> lables = GetDataOnServer("SlipNo", str_req_no);
            int i = 0;

            if (lables.size() > 0) {
                reqPK = lables.get(0); // TLG_GD_REQ_M_PK
                slipNO = lables.get(1); // SLIP_NO
                _reqQty = lables.get(2); // REQ_QTY
                _whPK = lables.get(3); // OUT_WH_PK
                _reqBal = lables.get(4); // BALANCE

                lbl_SlipNo1_Tag = slipNO;
                reqNo.setText(slipNO);
                reqQty.setText(_reqQty);
                reqBal.setText(_reqBal);
                reqWH.setText(_whPK);
                msg_error.setText("Req get success");
            }
            else {
                msg_error.setText("Slip No '" + str_req_no +"' not exist !!!" );
            }
        }
        catch (Exception ex) {
        }
        finally {
            bc1.setText("");
        }
    }
    public void OnSaveBC()
    {
        if (bc1.getText().toString().equals("")) {
            msg_error.setTextColor(Color.RED);
            msg_error.setText("Pls Scan barcode!");
            bc1.getText().clear();
            bc1.requestFocus();
            return;
        }
        else {

            String str_scan = bc1.getText().toString().toUpperCase();
            try {

                int cnt = Check_Exist_PK(str_scan);// check barcode exist in data
                if (cnt > 0)// exist data
                {
                    msg_error.setText("Barcode exist in database!");
                    bc1.getText().clear();
                    bc1.requestFocus();
                    return;
                }
                else{
                    bc2 = (EditText)  rootView.findViewById(R.id.edit_Bc2);
                    bc2.setText(str_scan);
                    if (OnSave()) {
                        msg_error.setText("Save success.");
                        bc1.getText().clear();
                        bc1.requestFocus();
                        OnShowScanLog();
                        OnShowScanIn();
                    }
                }
            }catch(Exception ex) {
                bc1.setText("");
                msg_error.setText(ex.getMessage());
                Log.e("OnSaveBC", ex.getMessage());
            }
            finally {
                bc1.setText("");
            }
        }
    }
    private void doStart()
    {
        try {

            /////
            db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor2 = db2.rawQuery("select PK, ITEM_BC,GD_SLIP_NO  from INV_TR where DEL_IF=0 and TR_TYPE='"+formID+"' and sent_yn = 'N' order by PK asc LIMIT 10", null);
           //System.out.print("\n\n\n cursor2.count: " + String.valueOf(cursor2.getCount()));
            //data=new String [cursor2.getCount()];
           // boolean read = Config.ReadFileConfig();
            if (cursor2.moveToFirst()) {
                flagUpload = false; //waiting response from server.
                // Write your code here  to invoke YES event
                System.out.print("\n\n**********doStart********\n\n\n");
                //Toast.makeText(gwMActivity, "Send to Server", Toast.LENGTH_SHORT).show();
                //set value message
                msg_error = (TextView)  rootView.findViewById(R.id.txtError);
                msg_error.setTextColor(Color.RED);
                msg_error.setText("");

                data = new String[cursor2.getCount()];
                int j = 0;
                String para = "";
                boolean flag = false;
                do {
                    flag = true;
                    for (int i = 0; i < cursor2.getColumnCount(); i++) {
                        if (para.length() <= 0) {
                            if (cursor2.getString(i) != null)
                                para += cursor2.getString(i) + "|";
                            else
                                para += "|";
                        } else {

                            if (flag == true) {
                                if (cursor2.getString(i) != null) {
                                    para += cursor2.getString(i);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor2.getString(i) != null)
                                    para += "|" + cursor2.getString(i);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "*|*";

                } while (cursor2.moveToNext());
                //////////////////////////
                para += "|!LG_MPOS_UPLOAD_GD_PARTIAL";
                data[j++] = para;
                System.out.print("\n\n\n para upload GD: " + para);
                Log.e("para upload GD: ", para);


                if (CNetwork.isInternet(tmpIP, checkIP)) {
                    ServerAsyncTask task = new ServerAsyncTask(gwMActivity);
                    task.execute(data);
                    // Write your code here to invoke YES event
                    gwMActivity.alertToastShort("Send to Server");
                } else {
                    flagUpload=true;
                    gwMActivity.alertToastLong("Network is broken. Please check network again !");
                }

            }
            cursor2.close();
            db2.close();
        }catch (Exception ex){
            Log.e("GD Error App:", ex.getMessage());
            gwMActivity.alertToastLong("Do start Error : " + ex.getMessage());
        }
    }

    //****************************************************************************
    public void OnSaveDataBK()
    {
        bc1 =(EditText) rootView.findViewById(R.id.edit_Bc1);
        bc2 =(EditText) rootView.findViewById(R.id.edit_Bc2);
        reqNo =(EditText) rootView.findViewById(R.id.edit_reqNo);
        reqQty =(EditText) rootView.findViewById(R.id.edit_reqQty);
        reqBal =(EditText) rootView.findViewById(R.id.edit_reqBal);
        reqWH =(TextView) rootView.findViewById(R.id.txt_GD_WH_PK);

        msg_error =(TextView) rootView.findViewById(R.id.txtError);
        msg_error.setTextColor(Color.RED);
        msg_error.setTextSize((float) 16.0);
        if(bc1.getText().equals(""))
        {
            msg_error.setText("Pls Scan barcode!");
            return;
            //bc1.requestFocus();
        }

        String str_scan=bc1.getText().toString().toUpperCase();
        int l_lenght=bc1.getText().toString().length();
        String str_req_no=bc1.getText().toString().substring(1, l_lenght);
        System.out.println("str_req_no " + str_req_no);
        try
        {
            if (str_scan.equals(""))
            {
                msg_error.setText("Pls Scan barcode!");
                return;
            }
            //scan slip no
            if (lbl_SlipNo1_Tag.equals("") && str_scan.indexOf("S") == 0)
            {
                List<String> lables = GetDataOnServer("SlipNo", str_req_no);
                String WHOUT_PK=reqWH.getText().toString();
                if (lables.size() > 0) {
                    String reqPK = lables.get(0); // TLG_GD_REQ_M_PK
                    String slipNO = lables.get(1); // SLIP_NO
                    String _reqQty = lables.get(2); // REQ_QTY
                    String _whPK = lables.get(3); // OUT_WH_PK
                    String _reqBal = lables.get(4); // BALANCE
                    if(WHOUT_PK.equals(_whPK) || WHOUT_PK.equals("0")) {
                        lbl_SlipNo1_Tag = slipNO;
                        reqNo.setText(slipNO);
                        reqQty.setText(_reqQty);
                        reqBal.setText(_reqBal);
                        reqWH.setText(_whPK);
                        msg_error.setText("Req get success");
                    }else{
                        bc2.getText().clear();
                        String aler="Delivery Req is different Customer ";
                        msg_error.setText(aler);
                        onPopAlert(aler);
                    }
                    bc1.getText().clear();
                    bc1.requestFocus();
                    return;
                }
                else
                {
                    msg_error.setText("Slip No is not found!");
                    bc1.setText("");
                    bc1.requestFocus();
                    lbl_SlipNo1_Tag=null;
                    reqNo.getText().clear();
                    reqQty.getText().clear();
                    reqBal.getText().clear();

                    return;
                }
            }

            //scan already slip no
            if (lbl_SlipNo1_Tag.equals(""))
            {
                msg_error.setText("Pls Scan Slip No!");
                bc1.getText().clear();
                bc1.requestFocus();
                return;
            }
            else
            {
                int cnt = Check_Exist_PK(str_scan);// check barcode exist in data
                if (cnt > 0)// exist data
                {
                    msg_error.setText("Barcode exist in database!");
                    bc1.getText().clear();
                    bc1.requestFocus();
                    return;
                }
                else{
                    bc2 = (EditText)  rootView.findViewById(R.id.edit_Bc2);
                    bc2.setText(str_scan);
                    if (OnSave()) {
                        msg_error.setText("Save success.");
                        bc1.getText().clear();
                        bc1.requestFocus();
                        OnShowScanLog();
                    }
                }
            }
            System.out.println("str_scan " +str_scan);
            System.out.println("lbl_SlipNo1_Tag " +lbl_SlipNo1_Tag);

        }
        catch (Exception ex)
        {
            //save data error write to file log

        }
    }

    public void OnSaveData()
    {

        if(bc1.getText().equals(""))
        {
            msg_error.setText("Pls Scan barcode!");
            return;
            //bc1.requestFocus();
        }

        String str_scan=bc1.getText().toString().toUpperCase();
        int l_length=bc1.getText().toString().length();
        String str_req_no=bc1.getText().toString().substring(1, l_length);
        try
        {
            if (str_scan.equals(""))
            {
                msg_error.setText("Pls Scan barcode!");
                return;
            }
            //scan slip no
            if (slipNO.equals("") && str_scan.indexOf("S") == 0)
            {
               // System.out.println("str_req_no " +str_req_no);
                List<String> lables = GetDataOnServer("SlipNo", str_req_no);
                if (lables.size() > 0)
                {
                    reqPK = lables.get(0); // TLG_GD_REQ_M_PK
                    slipNO = lables.get(1); // SLIP_NO
                    _reqQty = lables.get(2); // REQ_QTY
                    _whPK = lables.get(3); // OUT_WH_PK
                    _reqBal = lables.get(4); // BALANCE

                    lbl_SlipNo1_Tag = slipNO;
                    reqNo.setText(slipNO);
                    reqQty.setText(_reqQty);
                    reqBal.setText(_reqBal);
                    reqWH.setText(_whPK);
                    msg_error.setText("Req get success");

                    bc1.getText().clear();
                    bc1.requestFocus();
                    return;
                }
                else
                {
                    msg_error.setText("Slip No '" + str_req_no + "' not exist !!!");

                    lbl_SlipNo1_Tag="";
                    reqNo.getText().clear();
                    reqQty.getText().clear();
                    reqBal.getText().clear();

                    bc1.getText().clear();
                    bc1.requestFocus();
                    return;
                }
            }

            //scan already slip no
            if (lbl_SlipNo1_Tag.equals(""))
            {
                msg_error.setText("Slip No '" + str_scan + "' not exist !!!");
                bc1.getText().clear();
                bc1.requestFocus();
                return;
            }
            else
            {
                int cnt = Check_Exist_PK(str_scan);// check barcode exist in data
                if (cnt > 0)// exist data
                {
                    alertRingMedia();
                    msg_error.setText("Barcode exist in database!");
                    bc1.getText().clear();
                    bc1.requestFocus();
                    return;
                }
                else{
                    bc2 = (EditText)  rootView.findViewById(R.id.edit_Bc2);
                    bc2.setText(str_scan);
                    if(str_scan.length()>20) {
                        msg_error.setText("Barcode has length more 20 char !");
                        bc2.getText().clear();
                        bc1.getText().clear();
                        bc1.requestFocus();
                        return;
                    }
                    if (OnSave()) {
                        msg_error.setText("Save success.");
                        bc1.getText().clear();
                        bc1.requestFocus();
                        OnShowScanLog();
                    }
                }
            }

        }
        catch (Exception ex)
        {
            //save data error write to file log
            Log.e("Save error", ex.toString());
        }
    }

    //****************************************************************************
    public List<String> GetDataOnServer(String type, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";
            if (type.endsWith("SlipNo")) //WH
            {
                l_para = "1,LG_MPOS_GET_GD_REQ," + para;
            }
            if (type.endsWith("UploadChild"))
            {
                l_para = "2,LG_MPOS_UPD_PARTIAL," + para;
            }

            //ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String result[][] = getDataConnectThread(gwMActivity, l_para);

            for (int i = 0; i < result.length; i++) {
                if (type.endsWith("SlipNo")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                }
                if (type.endsWith("UploadChild")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                }
            }
        } catch (Exception ex) {
            Toast.makeText(gwMActivity, type + ": " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
        return labels;
    }
    //**************************************************************
    public  boolean OnSave()
    {
        boolean kq=false;
        db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        try
        {
            scan_date = CDate.getDateyyyyMMdd();
            scan_time = CDate.getDateYMDHHmmss();

            bc1 =(EditText) rootView.findViewById(R.id.edit_Bc1);
            EditText bc2 =(EditText) rootView.findViewById(R.id.edit_Bc2);
            reqNo =(EditText) rootView.findViewById(R.id.edit_reqNo);

            // get data input control to var then insert db
            P_SLIP_NO  = reqNo.getText().toString();
            P_ITEM_BC  = bc2.getText().toString();

            db.execSQL("INSERT INTO INV_TR(ITEM_BC,GD_SLIP_NO,SCAN_DATE,SCAN_TIME,SENT_YN,TR_TYPE) "
                    +"VALUES('"
                    +P_ITEM_BC+"','"
                    +P_SLIP_NO+"','"
                    +scan_date+"','"
                    +scan_time+"','"
                    +"N"+"','"
                    +formID
                    +"');");

            db.close();
            kq= true;
        }
        catch (Exception ex)
        {
            msg_error.setText("Save Error!!!");
            kq= false;
        }
        return kq;
    }
    ////////////////////////////////////////////////
    public void onClickViewStt(View view) {

        String str="";
        str=str+"002: \t\t\t Barcode nay khong ton tai\n";
        str=str+"------------------------------------------------------\n";
        str=str+"003: \t\t\t Barcode nay chua duoc nhap kho\n";
        str=str+"------------------------------------------------------\n";
        str=str+"004: \t\t\t Barcode nay khong co trong kho da chon, \n \t\t\t\tma co trong kho khac\n";
        str=str+"------------------------------------------------------\n";
        str=str+"005: \t\t\t Barcode nay da duoc nhap kho truoc ngay do\n";
        str=str+"------------------------------------------------------\n";
        str=str+"006: \t\t\t Barcode nay da xuat kho roi\n";
        str=str+"------------------------------------------------------\n";
        str=str+"007: \t\t\t Barcode nay da khong co trong phieu yeu cau\n";
        str=str+"------------------------------------------------------\n";
        str=str+"008: \t\t\t Barcode nay qty se vuot qua qty yeu cau\n";

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                //Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();


    }

    public void onClickSelectMid(View view) {

        String str="Are you sure you want to select ";

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Select ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);

        // Setting Negative "YES" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                try {
                    Object[] myLst = queueMid.toArray();
                    queueMid = new LinkedList();
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                    for (int i = 0; i < myLst.length; i++) {
                        String val = myLst[i].toString();
                        String _pk = val.split("\\|")[0].toString();
                        String _status = val.split("\\|")[1].toString();
                        if (_status.equals("005")) {
                            sql = "UPDATE INV_TR set  REMARKS='Not FIFO',SLIP_NO='-'  where PK = " + _pk;
                            db.execSQL(sql);

                            //ProcessStatus005_008(_pk);
                        }
                        if (_status.equals("008")) {
                            sql = "UPDATE INV_TR set STATUS = '000', REMARKS='Qty larger'  where PK = " + _pk;
                            db.execSQL(sql);

                            OnShowScanIn();
                            OnShowScanAccept();
                        }
                    }


                } catch (Exception ex) {
                    gwMActivity.alertToastLong("Select Mid :" + ex.getMessage());

                } finally {
                    db.close();
                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }

    //----------------------------------------------------------
    ///Load Header grid
    //----------------------------------------------------------
    public void OnShowGWHeader()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30,40};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout)  rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout)  rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Pk", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhName), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLineName), scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout)  rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Parent Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Child Item BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Remain Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Tr_Deli Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvRemarks), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        scrollablePart.addView(row);
    }



    ////////////////////////////////////////////////
    //DELETE ALL
    ///////////////////////////////////////////////
    public void onDelAll(View view){
        if(!checkRecordGridView(R.id.grdScanAccept)
                && !checkRecordGridView(R.id.grdScanIn)) return;

        String title="Confirm Delete..";
        String mess="Are you sure you want to delete all";
        alertDialogYN(title, mess, "DelAll");
    }

    public void alertDialogYN(String title,String mess,final String _type){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(_type.equals("DelAll")){
                    ProcessDeleteAll();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void onGoodsPartial(View view){
        if(!checkRecordGridView(R.id.grdScanAccept)) return;

        if(!parentBC.equals("")){
            dlgqueue = new LinkedList();//reset
            //popup
            final Dialog dlg_Partial = new Dialog(gwMActivity);
            dlg_Partial.setContentView(R.layout.dialog_parent_partial);
            dlg_Partial.setCancelable(false);//not close when touch
            dlg_Partial.setTitle(" List Child Item BC");
            ///////
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart;
            scrollablePart = (TableLayout) dlg_Partial.findViewById(R.id.grd_dlg_Header);
            row = new TableRow(gwMActivity);
            row.setLayoutParams(wrapWrapTableRowParams);
            row.setGravity(Gravity.CENTER);
            row.setBackgroundColor(bsColors[1]);
            row.setVerticalGravity(50);

            row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
            row.addView(makeTableColHeaderWithText("Qty", scrollableColumnWidths[4], fixedHeaderHeight));
            scrollablePart.addView(row);
            /////////////////////////////////////////////////////////////////
            try {
                String l_para = "1,LG_MPOS_GET_CHILDBC," + parentBC;
               // ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));

                String result[][] = getDataConnectThread(gwMActivity,l_para); //networkThread.execute(l_para).get();

                TableLayout scrollablePart1 = (TableLayout) dlg_Partial.findViewById(R.id.grd_dlg_Content);
                scrollablePart1.removeAllViews();//remove all view child

                for (int i = 0; i < result.length; i++) {
                    TableRow row1 = new TableRow(gwMActivity);
                    row1.setLayoutParams(wrapWrapTableRowParams);
                    row1.setGravity(Gravity.CENTER);
                    row1.setBackgroundColor(Color.LTGRAY);
                    dlgqueue.add(result[i][0].toString());
                    row1.addView(makeTableColWithText(result[i][0].toString(), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row1.addView(makeTableColWithText(result[i][1].toString(), scrollableColumnWidths[4], fixedRowHeight, 0));
                    row1.addView(makeTableColWithText(result[i][2].toString(), scrollableColumnWidths[4], fixedRowHeight, -1));
                    total_qty=total_qty +  Float.valueOf(result[i][2].toString());//Qty
                    row1.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            duplicate = false;
                            TableRow rw = (TableRow) v;
                            //////
                            TextView tv1 = (TextView) rw.getChildAt(0); //INV_TR_PK
                            TextView tvQty = (TextView) rw.getChildAt(2); //INV_TR_PK
                            int r = rw.getChildCount();
                            for (int i = 0; i < r; i++) {
                                tv11 = (TextView) rw.getChildAt(i);

                                tv11.setTextColor(Color.BLACK);
                            }

                            if (dlgqueue.size() > 0) {
                                if (dlgqueue.contains(tv1.getText().toString())) {
                                    dlgqueue.remove(tv1.getText().toString());
                                    total_qty = total_qty - Float.valueOf(tvQty.getText().toString());
                                    for (int i = 0; i < r; i++) {
                                        tv11 = (TextView) rw.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255, 99, 71));
                                    }
                                    duplicate = true;
                                }
                            }

                            if (!duplicate) {
                                dlgqueue.add(tv1.getText().toString());
                                total_qty = total_qty + Float.valueOf(tvQty.getText().toString());
                            }
                           // Toast.makeText(getApplicationContext(), "Size dlgqueue " + dlgqueue.size() + "", Toast.LENGTH_SHORT).show();
                        }
                    });
                    scrollablePart1.addView(row1);
                }
                //Toast.makeText(getApplicationContext(),"Size dlgqueue "+ dlgqueue.size() + "", Toast.LENGTH_SHORT).show();
            } catch (Exception ex) {
                Toast.makeText(gwMActivity, "Error get Child BC: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            }
            /////////////////////////////////////////////////////////////////
            Button btnSave = (Button) dlg_Partial.findViewById(R.id.btnSave);
            Button btnCancel = (Button) dlg_Partial.findViewById(R.id.btnCancel);

            // Attached listener for login GUI button
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Object[] lst = dlgqueue.toArray();
                        if(lst.length > 0) {
                            String pk_child="";
                            for(int i=0;i<lst.length;i++)
                            {
                                if(pk_child.length() <= 0)
                                {
                                    pk_child += lst[i];
                                }
                                else
                                {
                                    pk_child += ";" + lst[i];
                                }
                            }
                            //onPopAlert(pk_child);
                            List<String> val = GetDataOnServer("UploadChild",parentBC+"|"+pk_child+"|"+formID+"|"+deviceID+"|"+bsUserID+"|updateQty" );
                            if(val.get(0).equals("OK")) {
                                sql = "UPDATE INV_TR set TR_DELI_QTY=" + total_qty + ",ITEM_BC_CHILD='" + pk_child + "'  where ITEM_BC = " + parentBC;
                                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                                db.execSQL(sql);
                                db.close();
                            }
                        }
                    } catch (Exception ex) {
                        gwMActivity.alertToastLong(ex.getMessage());
                        Log.e("Save Qty error:", ex.getMessage());
                    } finally {
                        parentBC="";
                        total_qty = 0;

                        OnShowScanAccept();
                        dlg_Partial.dismiss();
                    }
                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    parentBC="";
                    total_qty = 0;
                    OnShowScanAccept();
                    dlg_Partial.dismiss();
                }
            });
            dlg_Partial.show();
        }else{
            onPopAlert("Please select Parent BC first!!!");
        }
    }

    public void onMakeSlip(View view){

        //Check het Barcode send to server
        if(checkRecordGridView(R.id.grdScanLog)){
            gwMActivity.alertToastLong( gwMActivity.getResources().getString(R.string.tvAlertMakeSlip) );
            return;
        }
        //Check have value make slip
        if(hp.isMakeSlip(formID)==false) return;
//       if(!checkRecordGridView(R.id.grdScanAccept)) return;


        boolean flag=true;
        if (!flag) {
            msg_error =(TextView) rootView.findViewById(R.id.txtError);
            msg_error.setText("Please, select Item.");
            msg_error.setTextColor(Color.RED);
            msg_error.setTextSize((float) 16.0);
            return;
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
            // Setting Dialog Title
            alertDialog.setTitle("Confirm Make Slip...");
            // Setting Dialog Message
            alertDialog.setMessage("Are you sure you want to Make Slip");
            // Setting Icon to Dialog
            alertDialog.setIcon(R.drawable.cfm_save);
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {
                    // Write your code here to invoke YES event
                    Button btnMSlip=(Button) rootView.findViewById(R.id.btnMakeSlip);
                    btnMSlip.setVisibility(View.GONE);
                    ProcessMakeSlip();
                }
            });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event
                    dialog.cancel();
                }
            });
            // Showing Alert Message
            alertDialog.show();
        }

    }

    public void ProcessMakeSlip()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();
        if(queue.size()>0)
        {
            /*
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_DELI_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WAREHOUSE_PK,TLG_GD_REQ_M_PK,REMARKS,ITEM_BC_CHILD   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='"+formID+"' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|"+unique_id;
                        para += "|"+ user;
                        para += "|"+ user_id;
                        para += "*|*";

                    }
                }
                para += "|!LG_MPOS_PRO_GD_PARTIAL_MSLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip goods delivery: " + para);
                //onPopAlert(para);
            }catch (Exception ex){
                Toast.makeText(gwMActivity, "onMakeSlip :" + ex.getMessage() , Toast.LENGTH_LONG).show();
                onPopAlert(ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
            */
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                //cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WAREHOUSE_PK,TLG_GD_REQ_M_PK,REMARKS   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='6' ",null);
                sql =       " SELECT TR_ITEM_PK, TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,TR_QTY, UOM,TR_LOT_NO " +
                        " FROM " +
                        "       (   " +
                        "           SELECT T2.ITEM_NAME, T2.ITEM_CODE, COUNT(*) TOTAL, SUM(T2.TR_DELI_QTY) TR_QTY, T2.UOM, T2.TR_ITEM_PK, T2.TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,TR_LOT_NO " +
                        "           FROM INV_TR T2 " +
                        "           WHERE DEL_IF = 0 AND T2.TR_TYPE = '"+formID+"'  AND T2.SCAN_DATE = '" + scan_date + "' AND T2.SENT_YN = 'Y' AND T2.STATUS='000' AND SLIP_NO='-' " +
                        "           GROUP BY T2.ITEM_NAME, T2.ITEM_CODE, T2.UOM, T2.TR_ITEM_PK, T2.TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,TR_LOT_NO" +
                        "       )";
                cursor = db.rawQuery(sql, null);
                // /int count=cursor.getCount();
                //data=new String [count];
                data=new String [1];
                int j=0;
                String para = "";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "|" + deviceID;
                        para +="*|*";

                    }while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_GD_MAKESLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip goods delivery: " + para);
                }
            }
            catch (Exception ex){
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
                Log.e("onMakeSlip :", ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP,checkIP)) {
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
                task.execute(data);
            } else
            {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        }
    }

    public void ProcessDeleteAll(){
        int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(queue.size()>0) {
                Object[] myLst = queue.toArray();
                data=new String [myLst.length];
                int j=0;
                for (int i = 0; i < myLst.length; i++) {
                    String para="";
                    cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='"+formID+"' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {
                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "0";
                            } else {
                                if (cursor.getString(k) != null)
                                    para += "|!" + cursor.getString(k);
                                else
                                    para += "|!";
                            }
                        }
                        para += "|!LG_MPOS_UPD_GD_V2";
                        data[j++] = para;

                    }
                }
            }
            else{
                cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='"+formID+"' and SLIP_NO='-' and STATUS ='000' AND SCAN_DATE > '" + date_previous + "'", null);
                int count = cursor.getCount();
                data = new String[count];
                int j = 0;
                if (cursor.moveToFirst()) {
                    do {
                        String para = "";
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "0";
                            } else {
                                if (cursor.getString(k) != null)
                                    para += "|!" + cursor.getString(k);
                                else
                                    para += "|!";
                            }
                        }
                        para += "|!LG_MPOS_UPD_GD_V2";
                        data[j++] = para;

                    } while (cursor.moveToNext());
                }
            }

        }catch (Exception ex){
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());

        }
        finally {
            cursor.close();
            db.close();
        }
        InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity);
        task.execute(data);
    }

    public void onDelAllFinish(){
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(queue.size()>0)
            {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();


            }else{
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='"+formID+"';");
            }
        }catch (Exception ex){
            Log.e("Error Delete All :", ex.getMessage());
        }
        finally {
            db.close();
            OnShowScanLog();
            OnShowScanIn();
            OnShowScanAccept();
            CountSendRecord();
            gwMActivity.alertToastShort( "Delete success..");

        }
    }

    ////////////////////////////////////////////////
    public void onListGrid(View view) {
        if(!checkRecordGridView(R.id.grdScanIn)) return;

        Intent openNewActivity = new Intent(view.getContext(), GridListViewMid.class);
        //send data into Activity
        openNewActivity.putExtra("type", "11");
        startActivity(openNewActivity);
    }
    ///////////////////////////////////////////////
    public void onListGridBot(View view){
        if(!checkRecordGridView(R.id.grdScanAccept)) return;

        Intent openNewActivity = new Intent(view.getContext(), GridListViewBot.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        //startActivity(openNewActivity);
        startActivityForResult(openNewActivity, REQUEST_CODE);
    }

    public void onClickInquiry(View view){
        if(!checkRecordGridView(R.id.grdScanAccept)) return;

        Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        startActivity(openNewActivity);
    }
    /**
     * Xử lý kết quả trả về ở đây
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiểm tra có đúng requestCode =REQUEST_CODE_INPUT hay không
        //Vì ta có thể mở Activity với những RequestCode khác nhau
        if(requestCode==REQUEST_CODE)
        {
            //Kiểm trả ResultCode trả về, cái này ở bên InputDataActivity truyền về
            //Toast.makeText(gwMActivity,"Result GoodsDelivery", Toast.LENGTH_LONG).show();
            OnShowScanAccept();
            switch(resultCode)
            {
                case RESULT_CODE:

                    break;
            }
        }
    }

    // show data scan log
    public void OnShowScanLog()
    {
        try
        {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30,40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout)  rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+formID+"' and SENT_YN='N' order by PK desc LIMIT 20",null);// TLG_LABEL

            int count = cursor.getCount();
            //Log.e("count log",String.valueOf(count));
            if (cursor != null && cursor.moveToFirst())
            {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count -i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));


                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            // total scan log
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+formID+"' and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            txtRem=(TextView)  rootView.findViewById(R.id.txtRemain);
            txtRem.setText("Re: " + count);

        } catch (Exception ex) {
            //Toast.makeText(gwMActivity, ex.getMessage(), Toast.LENGTH_LONG).show();
            //Log.e("Grid Scan log Error -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanIn()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30,40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout)  rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select (select COUNT(0)" +
                    "from INV_TR t1 " +
                    "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE = '" + scan_date + "' AND T1.STATUS NOT IN('000', ' ')" +
                    ") AS SEQ, T2.PK, T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.INCOME_DATE, T2.CHARGER, T2.TR_WH_OUT_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK " +
                    " FROM INV_TR t2 "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SCAN_DATE = '" + scan_date + "' AND T2.STATUS NOT IN('000', ' ')  " +
                    " GROUP BY T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.INCOME_DATE, T2.CHARGER, T2.TR_WH_OUT_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK" +
                    " ORDER BY pk desc LIMIT 10 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")), scrollableColumnWidths[5], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));

                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(4); //STATUS
                            String st_slipNO=tvSlipNo.getText().toString();

                            //if(st_slipNO.equals("005") || st_slipNO.equals("008")){
                            if(st_slipNO.equals("005") ){
                                TextView tv1 = (TextView) tr1.getChildAt(1); //INV_TR_PK
                                String value=tv1.getText().toString()+"|"+st_slipNO;

                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255,99,71));
                                }

                                if (queueMid.size() > 0) {

                                    if (queueMid.contains(value)) {
                                        queueMid.remove(value);

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK );
                                        }
                                        duplicate = true;
                                    }
                                }

                                if (!duplicate)
                                    queueMid.add(value);

                            }
                            //Toast.makeText(getApplicationContext(), queueMid.size() + "", Toast.LENGTH_SHORT).show();

                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND TR_TYPE = '"+formID+"' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            txtTotalGridMid=(TextView)  rootView.findViewById(R.id.txtTotalMid);
            txtTotalGridMid.setText("Total: " + count+ " ");
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);
        } catch (Exception ex) {
            //Toast.makeText(gwMActivity,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            //Log.e("OnShowScanIn Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan accept
    public void OnShowScanAccept()
    {
        Button btnMSlip=(Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMSlip.setVisibility(View.VISIBLE);
        try {
            int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

           // int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 23,40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout)  rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select (select COUNT(0)" +
                    "from INV_TR t1 " +
                    "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE > '" + date_previous + "' AND T1.SENT_YN = 'Y' AND T1.STATUS ='000' " +
                    ") AS SEQ, T2.ITEM_BC,T2.BC_TYPE, T2.ITEM_CODE, T2.TR_QTY,T2.TR_DELI_QTY, T2.TR_LOT_NO, T2.SLIP_NO, T2.TLG_POP_INV_TR_PK, T2.PK , T2.REMARKS " +
                    " FROM INV_TR t2 WHERE DEL_IF = 0 AND T2.TR_TYPE = '"+formID+"' AND T2.SCAN_DATE > '" + date_previous + "' AND T2.SENT_YN = 'Y' AND T2.STATUS='000' ORDER BY pk desc LIMIT 20 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight,0));
                    if(cursor.getString(cursor.getColumnIndex("BC_TYPE")).equals("P"))
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    else
                        row.addView(makeTableColWithText("", scrollableColumnWidths[3], fixedRowHeight,0));
                    if(cursor.getString(cursor.getColumnIndex("BC_TYPE")).equals("C"))
                        row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    else
                        row.addView(makeTableColWithText("", scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_DELI_QTY")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[5], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_POP_INV_TR_PK")), 0, fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, -1));

                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(7); //SLIP_NO
                            String st_slipNO=tvSlipNo.getText().toString();

                            if(st_slipNO.equals("-")){
                                ///////check click parent pk
                                TextView parent=(TextView) tr1.getChildAt(1);//Parent BC

                                //////
                                TextView tv1 = (TextView) tr1.getChildAt(10); //INV_TR_PK
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255,99,71));
                                }

                                if (queue.size() > 0) {
                                    if (queue.contains(tv1.getText().toString())) {
                                        queue.remove(tv1.getText().toString());

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK );
                                        }
                                        parentBC="";
                                        duplicate = true;
                                    }
                                }

                                if (!duplicate) {
                                    queue.add(tv1.getText().toString());
                                    parentBC=parent.getText().toString();
                                }
                            }
                            gwMActivity.alertToastShort(String.valueOf(queue.size()));

                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SCAN_DATE > '" + date_previous + "' AND SENT_YN = 'Y' AND STATUS='000'";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty=0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty=_qty+Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            txtTotalGridBot=(TextView)  rootView.findViewById(R.id.txtTotalBot);
            txtTotalGridBot.setText("Total: " + count+ " ");
            txtTotalGridBot.setTextColor(Color.BLUE);
            txtTotalGridBot.setTextSize((float) 18.0);

            txtTotalQtyBot=(TextView)  rootView.findViewById(R.id.txtTotalQtyBot);
            txtTotalQtyBot.setText("Qty: " + _qty+ " ");
            txtTotalQtyBot.setTextColor(Color.BLACK);
            txtTotalQtyBot.setTextSize((float) 18.0);

        } catch (Exception ex) {
            Toast.makeText(gwMActivity,"GridScanIn: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            //Log.e("OnShowScanAccept Error: -->", ex.getMessage());

        } finally {
            db.close();
            cursor.close();
        }
    }


    public void CountSendRecord()
    {
        flagUpload=true;
        // total scan log
        txtTotalGridMid=(TextView)  rootView.findViewById(R.id.txtTotalMid);
        txtTotalGridBot=(TextView)  rootView.findViewById(R.id.txtTotalBot);
        int countMid=Integer.parseInt(txtTotalGridMid.getText().toString().replaceAll("[\\D]",""));
        int countBot=Integer.parseInt(txtTotalGridBot.getText().toString().replaceAll("[\\D]",""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));
        txtSentLog=(TextView)  rootView.findViewById(R.id.txtSent);
        txtSentLog.setText("Send: "+(countMid+countBot));

        txtRem=(TextView)  rootView.findViewById(R.id.txtRemain);
        int countRe=Integer.valueOf(txtRem.getText().toString().replaceAll("[\\D]", ""));

        txtTT=(TextView)  rootView.findViewById(R.id.txtTotal);
        txtTT.setText("TT: " + (countMid + countBot + countRe));


    }

    //////////////////////////
    // check data inv_tr
    public int Check_Exist_PK(String l_bc_item) {
        int countRow = 0;
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            bc2 = (EditText)  rootView.findViewById(R.id.edit_Bc2);

            String countQuery = "SELECT PK FROM INV_TR where tr_type='"+formID+"' AND del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";

            // db = gwMActivity.getReadableDatabase();
            cursor = db.rawQuery(countQuery, null);
            // Check_Exist_PK=cursor.getCount();
            if (cursor != null && cursor.moveToFirst()) {
                countRow = cursor.getCount();
            }

        }
        catch (Exception ex){
            Toast.makeText(gwMActivity, "Check_Exist_PK :" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
        finally {
            cursor.close();
            db.close();
        }
        return countRow;

    }

    ////////////////////////////////////////////////
    //
    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str=str_alert;
        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(" Thông Báo ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                //Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
                onClickInquiry(view);

                break;
            case R.id.btnGoodsPartial:
                onGoodsPartial(view);

                break;
            case R.id.btnViewStt:
                onClickViewStt(view);

                break;
            case R.id.btnSelectMid:
                onClickSelectMid(view);

                break;
            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnList:
                onListGrid(view);

                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);

                break;
            default:
                break;
        }
    }
}
