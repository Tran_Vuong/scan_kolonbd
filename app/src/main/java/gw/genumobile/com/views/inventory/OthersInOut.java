package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.db.MySQLiteOpenHelper;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;

public class OthersInOut extends gwFragment implements View.OnClickListener{
    protected Boolean flagUpload=true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE=1;
    public static final int tes_role_pk=0000;
    public static final String formID="15";    // Other In Out
    Queue queue = new LinkedList();

    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    Button btnApprove,btnViewStt,btnList,btnInquiry,btnMakeSlip,btnDelAll,btnListBot;
    TextView recyclableTextView, _txtDate, _txtError, _txtTT, _txtSent, _txtRemain,_txtTotalGridBot,_txtTotalQtyBot,_txtTotalGridMid,_txtTime;
    EditText myBC, bc2, code, name;
    public Spinner myLstWH, myLstLoc ,myLstTrans,myLstSlip;

    String sql = "", tr_date = "",scan_date="",scan_time="",unique_id="";


    String wh_pk = "", wh_name = "", loc_pk = "", loc_name = "", trans_pk ="", trans_name = "" ,  slip_pk = "", slip_name  ="" ;

    String  data[] = new String[0];
    String dlg_pk="",dlg_barcode="",dlg_qty="";
    int timeUpload=0;


    Hashtable hashMapWH = new Hashtable();
    Hashtable hashMapLoc = new Hashtable();
    Hashtable hashMapTrans = new Hashtable();
    Hashtable hashMapSlip = new Hashtable();

    View rootView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_others__in__out,container, false);

        //lock screen
        //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        hp=new MySQLiteOpenHelper(gwMActivity);

        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);

        myBC        = (EditText) rootView.findViewById(R.id.editBC);
        myLstWH     = (Spinner) rootView.findViewById(R.id.lstWH);
        myLstLoc    = (Spinner) rootView.findViewById(R.id.lstLoc);
        myLstTrans  = (Spinner) rootView.findViewById(R.id.lstTranType);
        myLstSlip  = (Spinner) rootView.findViewById(R.id.lstSlipType);
        _txtRemain   = (TextView) rootView.findViewById(R.id.txtRemain );
        _txtDate     = (TextView) rootView.findViewById(R.id.txtDate);
        _txtError  = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");

        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        _txtDate.setText(formattedDate);
        _txtTime=(TextView) rootView.findViewById(R.id.txtTime);
        if(Measuredwidth <=600) {
            _txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);
        }
        OnShowGridHeader();
        OnShowScanLog();
        OnShowScanIn();
        OnShowScanAccept();
        CountSendRecord();
        LoadWH();
       // LoadLocation();
        LoadTransType();
        LoadSlipType();
        init_color();
        //OnPermissionApprove();
        myBC.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        OnSaveBC();
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // gwMActivity is for backspace
                    myBC.clearFocus();
                    Thread.interrupted();
                    //finish();
                    //System.exit(0);
                    //Log.e("IME_TEST", "BACK VIRTUAL");

                }
                return false;
            }
        });
        // onchange spinner wh
        myLstWH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                wh_pk = hashMapWH.get(myLstWH.getSelectedItemPosition()).toString();
                wh_name = myLstWH.getSelectedItem().toString();

                if(wh_pk.length() > 0 )
                    LoadLocation( wh_pk);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        myLstLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    loc_pk = hashMapLoc.get(myLstLoc.getSelectedItemPosition()).toString();
                    loc_name = myLstLoc.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // onchange spinner line
        myLstTrans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int i, long id) {
                trans_pk = hashMapTrans.get(myLstTrans.getSelectedItemPosition()).toString();
                trans_name = myLstTrans.getSelectedItem().toString();
              //  LoadSlipType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        // onchange spinner line
        myLstSlip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int i, long id) {
                slip_pk = hashMapSlip.get(myLstSlip.getSelectedItemPosition()).toString();
                slip_name = myLstSlip.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

      timeUpload=hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);
        
        return  rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run()
        {
            if(flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload*1000);
        }
    };

    private  void init_color(){
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private void doStart() {
        db2=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        cursor2 = db2.rawQuery("select PK, ITEM_BC,TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK,TRANS_PK  from INV_TR where DEL_IF=0 and TR_TYPE='"+formID+"' and sent_yn = 'N' order by PK asc LIMIT 10",null);
        //System.out.print("\n\n\n cursor2.count: "+ String.valueOf(cursor2.getCount()))TR_WH_IN_PK;

        if (cursor2.moveToFirst()) {
            // Write your code here to invoke YES event
            flagUpload=false;
            //set value message
            _txtError=(TextView)rootView.findViewById(R.id.txtError);
            _txtError.setTextColor(Color.RED);
            _txtError.setText("");

            data=new String [cursor2.getCount()];
            int j=0;
            String para ="";
            boolean flag=false;
            do {
                flag=true;
                for(int i=0; i < cursor2.getColumnCount();i++)
                {
                    if(para.length() <= 0)
                    {
                        if(cursor2.getString(i)!= null)
                            para += cursor2.getString(i)+"|";
                        else
                            para += "|";
                    }
                    else
                    {
                        if(flag==true){
                            if(cursor2.getString(i)!= null) {
                                para += cursor2.getString(i);
                                flag=false;
                            }
                            else {
                                para += "|";
                                flag=false;
                            }
                        }
                        else{
                            if(cursor2.getString(i)!= null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }
                }
                para += "|" + deviceID;
                para += "|" + bsUserID;
               // para += "*|*";
            } while (cursor2.moveToNext());
            //////////////////////////

           // List<Object[]> test = GetData("LG_MPOS_UPL_ST_OTHERS",para);
            para += "|!"+ "LG_MPOS_UPL_ST_OTHERS";
            data[j++]=para;
            //System.out.print("\n\n\n para upload: "+ para);
            Log.e("para upload: ", para);

            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                task.execute(data);
                gwMActivity.alertToastShort("Send to Server");
            } else
            {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }


        }
        cursor2.close();
        db2.close();
    }
    ////////////////////////////////////////////////
    public void OnSaveBC(){


        if (wh_pk.equals("")){
            _txtError.setTextColor(Color.RED);
            _txtError.setText("Pls select W/H!");
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        }
        if (myBC.getText().toString().equals("")) {
            _txtError.setTextColor(Color.RED);
            _txtError.setText("Pls Scan barcode!");
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        }
        else{

            String str_scanBC = myBC.getText().toString().toUpperCase();
            try
            {
                boolean isExists= hp.isExistBarcode(str_scanBC, formID);// check barcode exist in data
                if (isExists)// exist data
                {
                    alertRingMedia();
                    _txtError.setText("Barcode exist in database!");
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                }
                else
                {
                    scan_date = CDate.getDateyyyyMMdd();
                    scan_time = CDate.getDateYMDHHmmss();
                    db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                    db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK,TLG_IN_WHLOC_IN_NAME,TRANS_PK,TRANS_NAME,SLIP_PK,SLIP_NAME,GRADE,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                            + "VALUES('"
                            + str_scanBC + "',"
                            + wh_pk + ",'"
                            + wh_name + "','"
                            + loc_pk + "','"
                            + loc_name + "','"
                            + trans_pk + "','"
                            + trans_name + "',"
                            + slip_pk + ",'"
                            + slip_name + "','"
                            + "" + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "N" + "','"
                            + " " + "','"
                            + formID + "');");
                }
            }
            catch (Exception ex)
            {
                _txtError.setText("Save Error!!!");
                Log.e("Error OnSaveBC", ex.getMessage());
            }
            finally {
                db.close();
                myBC.getText().clear();
                myBC.requestFocus();
            }
        }
        OnShowScanLog();


    }

    ////////////////////////////////////////////////
    public void onDelAll(View view){
        String title="Confirm Delete..";
        String mess="Are you sure you want to delete ???";
        alertDialogYN(title, mess, "onDelAll");
    }

    public void  ProcessDelete(){
        try{
            db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(queue.size()>0)
            {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();
                data=new String [myLst.length];
                for (int i = 0; i < myLst.length; i++) {
                    db.execSQL("DELETE FROM INV_TR where TR_TYPE='"+formID+"' and pk="+myLst[i]);
                }

            }else{
                db.execSQL("DELETE FROM INV_TR where TR_TYPE='"+formID+"';");
            }
        }
        catch (Exception ex){
            gwMActivity.alertToastLong("Delete All :" + ex.getMessage());
        }
        finally {
            db.close();
            OnShowScanLog();
            OnShowScanIn();
            OnShowScanAccept();
        }
    }
    public void onClickViewStt(View view) {

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    ///////////////////////////////////////////////
    public void alertDialogYN(String title,String mess,final String _type){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onApprove")) {
                    Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    btnMSlip.setVisibility(View.GONE);
                    btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    btnApprove.setVisibility(View.GONE);
                    //ProcessApprove();
                }
                if (_type.equals("onDelAll")) {
                    ProcessDelete();
                }
                if (_type.equals("onMakeSlip")) {
                    //Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    //btnMSlip.setVisibility(View.GONE);
                    btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    btnApprove.setVisibility(View.GONE);
                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ////////////////////////////////////////////////
    public void onMakeSlip(View view){

        //Check het Barcode send to server
        if(checkRecordGridView(R.id.grdScanLog)){
            gwMActivity.alertToastLong( gwMActivity.getResources().getString(R.string.tvAlertMakeSlip) );
            return;
        }
        //Check have value make slip
        if(hp.isMakeSlip(formID)==false) return;

        String title="Confirm Make Slip...";
        String mess="Are you sure you want to Make Slip";
        alertDialogYN(title, mess, "onMakeSlip");
    }

    public void ProcessMakeSlip()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {/*
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TR_LINE_PK, SUPPLIER_PK, SCAN_DATE, TR_WAREHOUSE_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='"+formID+"' and pk="+myLst[i],null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {
                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|" + user;
                        para += "|" + user_id;
                    }
                    para += "*|*";
                }
                para += "|!LG_MPOS_PRO_ST_OTHERS_MSLIP";
                data[j++]=para;
                System.out.print("\n\n ******para make slip goods delivery: "+para);
            }catch (Exception ex){
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
            task.execute(data);
            /**/
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                //cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TR_LINE_PK, SUPPLIER_PK, SCAN_DATE, TR_WAREHOUSE_PK, UNIT_PRICE,PO_NO   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='"+formID+"' ", null);

                sql = " select  TR_ITEM_PK,TR_QTY, UOM, TR_LOT_NO, TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK,GRADE,TLG_PO_PO_D_PK,TR_LINE_PK,TRANS_PK,SLIP_PK  " +
                        " from "+
                        " (select TR_ITEM_PK, COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO, UOM, TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK,TRANS_PK,SLIP_PK,GRADE,TLG_PO_PO_D_PK,TR_LINE_PK  " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "'  AND TR_TYPE = '"+formID+"' AND STATUS='000' and SLIP_NO='-' " +
                        " GROUP BY TR_ITEM_PK,TR_LOT_NO, UOM, TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK,TRANS_PK,SLIP_PK,GRADE,TLG_PO_PO_D_PK,TR_LINE_PK   )";
                cursor = db.rawQuery(sql, null);
                data=new String [1];
                //int count=cursor.getCount();
                //data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "|"+ deviceID;
                        para += "*|*";
                    }while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_ST_OTHERS_MSLIP";
                    System.out.print("\n\n ******para make slip in stock: " + para);
                    data[j++] = para;

                    // asyns server
                    if (CNetwork.isInternet(tmpIP, checkIP)) {
                        MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                        task.execute(data);
                    } else
                    {
                        gwMActivity.alertToastLong("Network is broken. Please check network again !");
                    }
                }
            }
            catch (Exception ex){
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }


        }
    }


    public void onListGridBot(View view){
        if(!checkRecordGridView(R.id.grdScanAccept)) return;

        Intent openNewActivity = new Intent(view.getContext(), GridListViewBot.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);

        //startActivity(openNewActivity);
        startActivityForResult(openNewActivity, REQUEST_CODE);

    }
    ///////////////////////////////////////////////

    private void LoadWH() {
        hashMapWH = LoadDataToSpinner(GetData("LG_MPOS_M010_GET_WH_USER",bsUserPK + "|wh_ord"),myLstWH) ;
    }

    private void LoadLocation(String wh_pk) {

      //  LG_MPOS_GET_LOC

        hashMapLoc = LoadDataToSpinner(GetData("LG_MPOS_GET_LOC",wh_pk),myLstLoc) ;



    }

    ////////////////////////////////////////////////
    private void LoadTransType() {

        List<Object[]> transType  = new ArrayList<Object[]>();

        transType.add(new Object[]{"I130","OTHERS IN"});
        transType.add(new Object[]{"O130","OTHERS OUT"});

        hashMapTrans = LoadDataToSpinner(transType,myLstTrans) ;

    }

    ////////////////////////////////////////////////
    private void LoadSlipType() {

        hashMapSlip = LoadDataToSpinner(GetData("LG_MPOS_LOGISTIC_CODE","LGIN0303"),myLstSlip) ;

    }
    ////////////////////////////////////////////////
    public List<String> GetDataOnServer(String typeData, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";
            if (typeData.endsWith("WH")) //WH
            {
                l_para = "1,LG_MPOS_M010_GET_WH_USER," + para+"|wh_ord_2";
            }

            if(typeData.endsWith("LOC") ){
                l_para = "1,LG_MPOS_GET_LOC," + para;
            }

            if (typeData.endsWith("TRANS"))//LINE
            {
                l_para = "1,LG_MPOS_GET_TRANSTYPE," + para;
            }

            if (typeData.endsWith("WH_PK")) //WH PK
            {
                l_para = "1,LG_MPOS_M010_GET_WH_PK," + para;
            }

            if (typeData.endsWith("TRANS_PK"))//LINE PK
            {
                l_para = "1,LG_MPOS_GET_TRANSTYPE_CODE," + para;
            }
            if (typeData.endsWith("SLIPTYPE"))//
            {
                l_para = "1,LG_MPOS_GET_SLIPTYPE," + para;
            }
            if (typeData.endsWith("ROLE"))
            {
                l_para = "1,LG_MPOS_GET_ROLE," + para;
            }


            String result[][] = getDataConnectThread(gwMActivity,l_para);

            for (int i = 0; i < result.length; i++) {
                if (typeData.endsWith("BC")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                } else if (typeData.endsWith("WH_PK")){
                    labels.add(result[i][0].toString());
                } else if (typeData.endsWith("TRANS_PK")) {
                    labels.add(result[i][0].toString());
                }else if(typeData.endsWith("ROLE")){
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                }
                else
                    labels.add(result[i][1].toString());
            }
        } catch (Exception ex) {
            //gwMActivity.alertToastLong(typeData + ": " + ex.getMessage());
        }
        return labels;
    }

    public List<Object[]> GetData(String pro, String para) {
        List<Object[]> labels = new ArrayList<Object[]>();
        try {

            String result[][] = getDataConnectThread(gwMActivity,"1,"+pro+"," + para);

            for (int i = 0; i < result.length; i++) {

                    List<String> tlist = new ArrayList<String>();
                    for (int j = 0; j < result[i].length; j++)
                        tlist.add(result[i][j].toString());
                    labels.add(tlist.toArray());
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(pro + ": " + ex.getMessage());
        }
        return labels;
    }

    /// ARIC LAM
    public Hashtable LoadDataToSpinner(List<Object[]> list ,Spinner spinner){
        return  LoadDataToSpinner(list,0,1,spinner);
    }

    /// ARIC LAM
    public Hashtable LoadDataToSpinner(List<Object[]> list , int indexValueKey   ,int indexValueShow ,Spinner spinner){
        if(list == null || spinner == null){
            spinner.setAdapter(null);
            return  null;
        }


        try {
            Hashtable hashtable = new Hashtable();
            List<String> arrayName = new ArrayList<String>();
            for (int i = 0; i < list.size(); i++) {

                Object[] o = list.get(i) ;
                arrayName.add(o[indexValueShow].toString());
                hashtable.put(i,o[indexValueKey].toString() );
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, arrayName);

            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner.setAdapter(dataAdapter);

            return hashtable ;
        } catch (Exception ex) {
            gwMActivity.alertToastLong("ListObjectToHashTable" + ": " + ex.getMessage());
            spinner.setAdapter(null);
        }


        return  null;
    }

    // show data scan log
    public void OnShowScanLog()
    {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+formID+"' and SENT_YN='N' order by PK desc LIMIT 20",null);// TLG_LABEL

            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //Count Grid Log
            count = hp.CountGridLog(formID);//tr_type=14
            _txtRemain=(TextView) rootView.findViewById(R.id.txtRemain);
            _txtRemain.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanIn()
    {
        try {
            int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, TR_WH_IN_NAME,  TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SENT_YN='Y' AND SCAN_DATE > '" + date_previous + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq= String.valueOf(count-i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));
//                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TRANS_NAME")), scrollableColumnWidths[3], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > '" + date_previous + "' AND TR_TYPE = '"+formID+"' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtTotalGridMid=(TextView) rootView.findViewById(R.id.txtTotalMid);
            _txtTotalGridMid.setText("Total: " + count);
            _txtTotalGridMid.setTextColor(Color.BLUE);
            _txtTotalGridMid.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan accept
    public void OnShowScanAccept()
    {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            //sql = "select  PK, ITEM_BC, ITEM_CODE, TR_QTY, TR_LOT_NO, SLIP_NO, TLG_POP_INV_TR_PK,UNIT_PRICE,LOC_NAME,PO_NO,TR_WH_IN_NAME " +
            //        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND TR_TYPE = '"+formID+"' AND STATUS IN('OK', '000') ORDER BY PK desc LIMIT 20 ";
            sql = " select  TR_ITEM_PK,  ITEM_CODE,TOTAL,TR_QTY, TR_LOT_NO, SLIP_NO, UNIT_PRICE, TR_WH_IN_NAME, TR_WH_IN_PK,TRANS_PK,TRANS_NAME,TLG_IN_WHLOC_IN_NAME,GRADE,SLIP_PK,SLIP_NAME  " +
                    " from "+
                    " ( select TR_ITEM_PK,  ITEM_CODE, count(*) TOTAL,SUM(TR_QTY) as TR_QTY, TR_LOT_NO, SLIP_NO, UNIT_PRICE, TR_WH_IN_NAME, TR_WH_IN_PK,TLG_IN_WHLOC_IN_NAME,TRANS_PK,TRANS_NAME,GRADE,SLIP_PK,SLIP_NAME " +
                    " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND TR_TYPE = '"+formID+"' AND STATUS IN('OK', '000') " +
                    " GROUP BY TR_ITEM_PK,  ITEM_CODE, TR_LOT_NO, SLIP_NO, UNIT_PRICE, TR_WH_IN_NAME, TR_WH_IN_PK,TRANS_PK,TRANS_NAME,TLG_IN_WHLOC_IN_NAME,GRADE,SLIP_PK,SLIP_NAME)" +
                    "ORDER BY TR_ITEM_PK desc LIMIT 20 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TOTAL")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TRANS_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));


                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("GRADE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")), 0, fixedRowHeight,0));

                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            TableRow tr1 = (TableRow) v;
                            TextView tvLotNo = (TextView) tr1.getChildAt(4); //LOT_NO
                            String st_LotNO=tvLotNo.getText().toString();
                            TextView tvSlipNo = (TextView) tr1.getChildAt(8); //SLIP NO
                            String st_SlipNO=tvSlipNo.getText().toString();

                            TextView tvItemPK = (TextView) tr1.getChildAt(9); //TR_ITEM_PK
                            String item_pk=tvItemPK.getText().toString();
                            TextView tvWhPk = (TextView) tr1.getChildAt(10); //WH_PK
                            String whPK=tvWhPk.getText().toString();
                            Intent openNewActivity = new Intent(v.getContext(), gw.genumobile.com.views.PopDialogGrid.class);
                            //send data into Activity
                            openNewActivity.putExtra("TYPE", formID);
                            openNewActivity.putExtra("ITEM_PK", item_pk);
                            openNewActivity.putExtra("LOT_NO",st_LotNO);
                            openNewActivity.putExtra("WH_PK",whPK);
                            openNewActivity.putExtra("SLIP_NO", st_SlipNO);
                            openNewActivity.putExtra("USER",bsUserID);
                            //startActivity(openNewActivity);
                            startActivityForResult(openNewActivity, REQUEST_CODE);

                            /*
                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(6); //SLIP_NO
                            String st_slipNO=tvSlipNo.getText().toString();
                            if(st_slipNO.equals("-")) {
                                TextView tv1 = (TextView) tr1.getChildAt(5); //TR_WH_IN_NAME
                                if(wh_name_grid.equals("")){
                                    wh_name_grid=tv1.getText().toString();
                                }

                                if(tv1.getText().toString().equals(wh_name_grid)) {
                                    TextView tvBarcode = (TextView) tr1.getChildAt(1); //barcode
                                    dlg_barcode=tvBarcode.getText().toString();
                                    TextView tvQty = (TextView) tr1.getChildAt(3); //qty
                                    dlg_qty=tvQty.getText().toString();
                                    TextView tv2 = (TextView) tr1.getChildAt(9); //TLG_POP_INV_TR_PK
                                    dlg_pk=tv2.getText().toString();
                                    int xx = tr1.getChildCount();

                                    for (int i = 0; i < xx; i++) {
                                        tv11 = (TextView) tr1.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255,99,71));
                                    }
                                    String value =tv2.getText().toString();
                                    if (queue.size() > 0) {
                                        if (queue.contains(value)) {
                                            queue.remove(value);
                                            dlg_pk="";////reset dialog edit qty
                                            for (int i = 0; i < xx; i++) {
                                                tv11 = (TextView) tr1.getChildAt(i);
                                                tv11.setTextColor(Color.BLACK);
                                            }
                                            duplicate = true;
                                        }
                                    }

                                    if (!duplicate)
                                        queue.add(value);
                                }
                                else{
                                    gwMActivity.alertToastLong("WH is different");
                                }
                            }
                            gwMActivity.alertToastLong(queue.size() + "");
                            */
                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            float _qty=0;
            String[] _arrVal=hp.CountGridAccept("15", scan_date);
            count=Integer.valueOf(_arrVal[0]);
            _qty=Float.parseFloat(_arrVal[1]);
            _txtTotalGridBot=(TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 16.0);

            _txtTotalQtyBot=(TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty)+ " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 16.0);

        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanAccept: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void CountSendRecord()
    {
        flagUpload=true;
        // total scan log
        _txtSent=(TextView) rootView.findViewById(R.id.txtSent);
        _txtTotalGridMid=(TextView) rootView.findViewById(R.id.txtTotalMid);
        _txtTotalGridBot=(TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid=Integer.parseInt(_txtTotalGridMid.getText().toString().replaceAll("[\\D]",""));
        int countBot=Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Send: " + (countMid + countBot));

        _txtRemain=(TextView) rootView.findViewById(R.id.txtRemain);
        int countRe=Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]",""));

        _txtTT=(TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: " + (countMid + countBot + countRe));

    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhName), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Trans Type", scrollableColumnWidths[3], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvTotal), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhName), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("LOC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Trans Type", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Slip", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("GRADE", scrollableColumnWidths[3], fixedHeaderHeight));

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_WH_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
                //onClickInquiry(view);

                break;
            case R.id.btnApprove:
                //onApprove(view);

                break;
            case R.id.btnViewStt:
                onClickViewStt(view);

                break;

            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnList:
                //onListGridMid(view);

                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);

                break;
            default:
                break;
        }
    }
}
