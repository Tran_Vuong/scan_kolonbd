package gw.genumobile.com.views;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.db.MySQLiteOpenHelper;
import gw.genumobile.com.models.FormInfo;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.models.gwform_info;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.gwService;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link gwFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link gwFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class gwFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public   String formID="0";
    protected String deviceID="";
    protected String tmpIP="";
    protected Boolean checkIP=false;
    protected int[] bsColors = new int[]{0x300000FF, 0xFF0078A8, 0xCCFFFF, 0x99CC33, 0xFF556B2F, 0xFF0078a8};  //Color hexadecimal (ARGB) alpha,red green blue
    protected int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};
    TextView bsTextView;
    private MediaPlayer mediaPlayer;
    protected int fixedRowHeight = 70; //70|30
    protected int fixedHeaderHeight = 80; //80|40
    protected int  Measuredheight = 0, Measuredwidth = 0;
    protected MySQLiteOpenHelper hp;
    public String bsUserPK="",bsUserID="",bsUser="";
    public String getBsUserPK() {
        return bsUserPK;
    }
    public void setBsUserPK(String bsUserPK) {
        this.bsUserPK = bsUserPK;
    }
    public String getBsUserID() {
        return bsUserID;
    }
    public void setBsUserID(String bsUserID) {
        this.bsUserID = bsUserID;
    }
    public String getBsUser() {
        return bsUser;
    }
    public void setBsUser(String bsUser) {
        this.bsUser = bsUser;
    }
    protected gwMainActivity gwMActivity;
    protected Activity gwActivity;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    protected SharedPreferences appPrefs;
    private OnFragmentInteractionListener mListener;


    protected String dataStatus ;

    public void setDataStatus(String data){
        this.dataStatus = data;
    }



    public gwFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment gwFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static gwFragment newInstance(String param1, String param2) {
        gwFragment fragment = new gwFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        Log.i(":::create", ":::::::::::::create::::::::::::create createcreatecreate::::create: ");
        gwActivity = getActivity();
        gwMActivity= ((gwMainActivity)getActivity());
        appPrefs = gwActivity.getSharedPreferences("myConfig", gwActivity.MODE_PRIVATE);
        TelephonyManager telephonyManager = (TelephonyManager)gwActivity.getSystemService(Context.TELEPHONY_SERVICE);
        deviceID=telephonyManager.getDeviceId();
        //if(deviceID=="" || deviceID.equals("null") || deviceID==null) {
        //    deviceID="000000000000000";
        //}
        gwform_info finfo=null;
        try {
            finfo= gwform_info.valueOf(this.getTag());
        } catch (IllegalArgumentException iae) {
            finfo= gwform_info.valueOf("gw");
        }
        this.formID=String.valueOf(finfo.Id());
      //  gwMActivity.setRequestedOrientation(finfo.getValue().ori);

        //Info User
        String userInfo=appPrefs.getString("userInfo", "");
        String[] strArray = userInfo.split("\\|");
        bsUserID=strArray[0];
        bsUserPK=strArray[1];
        //
        fixedRowHeight = Integer.parseInt(appPrefs.getString("rowHeight", "70"));
        fixedHeaderHeight = Integer.parseInt(appPrefs.getString("headerHeight", "80"));
        hp=new MySQLiteOpenHelper(gwMActivity);
        tmpIP= appPrefs.getString("server", "");
        checkIP=appPrefs.getBoolean("ckbInternet", Boolean.FALSE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_gw, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public  void alertRingMedia(){
        mediaPlayer = MediaPlayer.create(gwMActivity, R.raw.msbeep01);
        mediaPlayer.start();
        //for(int j=0;j<30000;j++)
        //    mediaPlayer.stop();
    }
    public  void alertRingMedia2(){
        //mediaPlayer = MediaPlayer.create(this,   this.getResources().getIdentifier("msbeep02.mp3","assets", this.getPackageName()));// R.drawable.msbeep02);
        mediaPlayer = MediaPlayer.create(gwMActivity, R.raw.msbeep02);
        mediaPlayer.start();
        //for(int j=0;j<30000;j++)
        //    mediaPlayer.stop();
    }

    public TextView makeTableColHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(getResources().getColor(R.color.grd_textHeader));
        bsTextView.setTextSize(18);
        bsTextView.setGravity(Gravity.CENTER);
        bsTextView.setBackgroundColor(getResources().getColor(R.color.grd_border));
        bsTextView.setPadding(0, 2, 0, 2);
        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }

    public TextView makeTableColWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels,int textAlign) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.BLACK);
        if (textAlign == -1) {
            bsTextView.setGravity(Gravity.LEFT);
            bsTextView.setPadding(10, 5, 0, 5);
        } else if (textAlign == 0) {
            bsTextView.setGravity(Gravity.CENTER);
            bsTextView.setPadding(0, 5, 0, 5);
        } else {
            bsTextView.setGravity(Gravity.RIGHT);
            bsTextView.setPadding(0, 5, 10, 5);
        }
        bsTextView.setTextSize(16);
        bsTextView.setBackgroundColor(-1);

        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;


    }
    public String[][] getDataConnectThread(Activity arg_Activity, String l_para) throws ExecutionException, InterruptedException {

        ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));

        String result[][] = networkThread.execute(l_para).get();
        return result;
    }

    public List<JDataTable> getDataJson(Activity arg_Activity, String l_para) throws ExecutionException, InterruptedException {

        gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
        JResultData result = networkThread.execute(l_para).get();
        List<JDataTable> jdt = result.getListODataTable();
        return jdt;
    }

    public boolean checkRecordGridView(int GridViewName){
        TableLayout scrollablePart = (TableLayout) gwMActivity.findViewById(GridViewName);
        if(scrollablePart.getChildCount() > 0)return  true;
        else return false;
    }
}
