package gw.genumobile.com.views.gwcore;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import gw.genumobile.com.R;
import gw.genumobile.com.views.gwFragmentDialog;

public class DebugActivity extends gwFragmentDialog  {

    private EditText editText ;
    private  Button btn ;
    private  Button btnCommit;
    private  Button btnClose ;
    private ListView listView;
    private LinearLayout linearLayout;

    private  DBAdapter adapter;

    SQLiteDatabase db = null;
    Cursor cursor;

    private List<Object> data = new ArrayList<Object>();
    private List<Object> dataHeader = new ArrayList<Object>();
    private int[] dataSize;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View  vContent = inflater.inflate(R.layout.activity_debug, container, false);



        db = getContext().openOrCreateDatabase("gasp", getContext().MODE_PRIVATE, null);
        linearLayout = (LinearLayout) vContent.findViewById(R.id.dataHeader);

        editText = (EditText) vContent.findViewById(R.id.editText);
        btn = (Button) vContent.findViewById(R.id.btnSummit);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               doSearch();
            }
        });

        btnCommit = (Button) vContent.findViewById(R.id.btnCommit);

        btnCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(db != null){
                   // db.
                }
            }
        });
        btnClose = (Button) vContent.findViewById(R.id.btnClosePopup );

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        listView = (ListView) vContent.findViewById(R.id.dataList);
        adapter = new DBAdapter();
        listView.setAdapter(adapter);

        return vContent;
    }


    private void doSearch(){
        try {
            cursor = db.rawQuery(editText.getText().toString(), null);
            int j = 0;

            data.clear();
            dataHeader.clear();
            int count = cursor.getCount();
            boolean checkHeader = false;
            if (cursor.moveToFirst()) {

                List<String> row = new ArrayList<String>();
                do {
                    if (!checkHeader) {
                        dataSize = new int[cursor.getColumnCount()];
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            dataSize[k] = 50;
                        }
                    }

                    boolean flag = true;
                    for (int k = 0; k < cursor.getColumnCount(); k++) {
                        String str = cursor.getString(k);

                        if (str == null) {
                            str = "";
                            //  continue;
                        }

                        if (str.length() * 20 > dataSize[k]) {
                            dataSize[k] = str.length() * 20;
                        }

                        row.add(str);

                        if (!checkHeader) {
                            String strheader = cursor.getColumnName(k);

                            if (strheader.length() * 20 > dataSize[k]) {
                                dataSize[k] = strheader.length() * 20;
                            }
                            dataHeader.add(strheader);
                        }
                    }

                    checkHeader = true;

                    data.add(row);

                } while (cursor.moveToNext());
            }
            adapter.notifyDataSetChanged();

            iniHeader();
        }catch (Exception ex){
            Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG);
        }

    }

    public void iniHeader(){
        linearLayout.removeAllViews();

        for(int i = 0 ; i < dataSize.length; i++){
            int size = 20 ;
            try{
                size = dataSize[i];
            }catch (Exception ex){}

            LinearLayout CELL = new LinearLayout(getContext());
            LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(size,LinearLayout.LayoutParams.MATCH_PARENT);
            CELL.setLayoutParams(lp2);
            CELL.setOrientation(LinearLayout.HORIZONTAL);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CELL.setBackground(getContext().getDrawable(R.drawable.border_blue_gray_cell));
            }

            TextView textView = new TextView(getContext());
           // textView.setLayoutParams(lp);
            textView.setText(dataHeader.get(i).toString());
            textView.setTextColor(Color.WHITE);
            CELL.addView(textView);
            linearLayout.addView(CELL);
        }
    }

    public  class DBAdapter extends BaseAdapter{

        @Override
        public int getCount() {

            return data.size();
        }

        @Override
        public Object getItem(int i) {

            if(i > data.size())
                return null;
            return  data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {


            LinearLayout parent = new LinearLayout(getContext());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT,58);
            parent.setOrientation(LinearLayout.HORIZONTAL);
            parent.setLayoutParams(lp);

            List<String> item  = ( List<String>) getItem(i);

            for(int j = 0 ; j < item.size() ; j++){
                int size = 20 ;
                try{
                        size = dataSize[j];
                }catch (Exception ex){}
                LinearLayout CELL = new LinearLayout(getContext());
                LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(size,LinearLayout.LayoutParams.MATCH_PARENT);
                CELL.setLayoutParams(lp2);
                CELL.setOrientation(LinearLayout.HORIZONTAL);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    CELL.setBackground(getContext().getDrawable(R.drawable.border_primary_cell));
                }

                TextView textView = new TextView(getContext());
                textView.setLayoutParams(lp);
                textView.setText(item.get(j));
                CELL.addView(textView);
                parent.addView(CELL);
            }

            return parent;
        }
    }
}
