package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.models.gwform_info;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.AppConfig;
import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.interfaces.GridListView;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;

public class Mapping extends gwFragment implements  View.OnClickListener{
    protected Boolean flagUpload=true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;
    public static final String formID="10";
    private SharedPreferences mappingPrefs;

    Queue queue = new LinkedList();
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    String data[] = new String[0];
    String sql = "", scan_date = "", scan_time = "",unique_id="";
    EditText bc;
    EditText currentBC, _edItemCode, _edQty, _edLotNo;
    CheckBox _ckbLotNo, _ckbItemCode;
    TextView recyclableTextView, _txtError, _txtTotalGridBot, _txtTotalQtyBot, _txtTotalGridMid;
    int pkParent = 0;
    int indexDialog = 0;

    private Handler customHandler = new Handler();

    CharSequence seqColor[] = new CharSequence[] {"red", "green", "blue", "black","red", "green", "blue", "black","red", "green", "blue", "black","red", "green", "blue", "black","red", "green", "blue", "black","red", "green", "blue", "black"};
    Button btnViewStt,btnList,btnUploadMapping,btnDelAll,btnDelBC,btnMakeSlip,btnListBot;
    View rootView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_mapping,container, false);
        

        //lock screen
        //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mappingPrefs = gwActivity.getSharedPreferences("MappingConfig", gwActivity.MODE_PRIVATE);
        //

        InitActivity();
        OnShowGridHeader();
        OnShowParent();
        OnShowChild();
        init_color();
        bc.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        _txtError = (TextView) rootView.findViewById(R.id.txtError);
                        _txtError.setTextColor(Color.RED);
                        _txtError.setTextSize((float) 16.0);
                        if (bc.getText().toString().equals("")) {
                            _txtError.setText("Pls Scan barcode!");
                            bc.requestFocus();
                        } else {
                            OnSaveData();
                        }
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // gwMActivity is for backspace
                    bc.clearFocus();
                    Thread.interrupted();
                    //finish();
                    //System.exit(0);
                    //Log.e("IME_TEST", "BACK VIRTUAL");

                }
                return false;
            }
        });
        
        return rootView;
    }
    protected void InitActivity(){

        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);
        btnUploadMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnUploadMapping.setOnClickListener(this);

        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");
        currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
        _edItemCode = (EditText) rootView.findViewById(R.id.ed_ItemCode);
        _edQty = (EditText) rootView.findViewById(R.id.ed_Qty);
        _edLotNo = (EditText) rootView.findViewById(R.id.ed_LotNo);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setTextColor(Color.RED);
        _txtError.setTextSize((float) 16.0);
        _ckbLotNo = (CheckBox) rootView.findViewById(R.id.ckbLotNo);
        _ckbItemCode = (CheckBox) rootView.findViewById(R.id.ckbItemCode);
        if(!mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {
            _ckbItemCode.setChecked(false);
            _ckbLotNo.setChecked(false);
        }
        bc = (EditText) rootView.findViewById(R.id.ed_BC);
    }

    private  void init_color(){
        //region ------Set color tablet----
//        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
//        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
//        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
//        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdParent);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdChild);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    //****************************************************************************
    public void OnSaveData() {
        bc = (EditText) rootView.findViewById(R.id.ed_BC);
        currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
        _edItemCode = (EditText) rootView.findViewById(R.id.ed_ItemCode);
        _edQty = (EditText) rootView.findViewById(R.id.ed_Qty);
        _edLotNo = (EditText) rootView.findViewById(R.id.ed_LotNo);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setTextColor(Color.RED);
        _txtError.setTextSize((float) 16.0);
        _ckbLotNo = (CheckBox) rootView.findViewById(R.id.ckbLotNo);
        _ckbItemCode = (CheckBox) rootView.findViewById(R.id.ckbItemCode);
        if (bc.getText().equals("")) {
            _txtError.setText("Pls Scan barcode!");
            return;
            //bc1.requestFocus();
        }

        String str_scan = bc.getText().toString().toUpperCase();
        int l_lenght = str_scan.length();
        //String str_bc=str_scan.substring(1, l_lenght);
        //System.out.println("str_bc " + str_bc);
        Log.e("para str_bc ", str_scan);

        //region  Parent YN = Y
        if(mappingPrefs.getBoolean("parentYN", Boolean.FALSE))
        {

        }
        //endregion
        else{
            try {
                if (str_scan.equals("")) {
                    _txtError.setText("Pls Scan barcode!");
                    return;
                }

                List<String> lables = GetDataOnServer("Parent", str_scan);

                scan_date = CDate.getDateyyyyMMdd();
                scan_time = CDate.getDateYMDHHmmss();
                if (lables.size() > 0)
                {
                    String P_PACKAGES_PK = lables.get(0);
                    String P_ITEM_BC = lables.get(1);
                    String P_QTY = lables.get(2);
                    String P_ITEM_CODE = lables.get(3);
                    String P_ITEM_NAME = lables.get(4);
                    int Parent = Integer.valueOf(lables.get(5));
                    String P_TR_LOT_NO = lables.get(6);
                    int P_ITEM_PK = Integer.valueOf(lables.get(7));
                    if (Parent <= 0)
                    {
                        int cnt = Check_Exist_PK(str_scan);// check barcode exist in data
                        if (cnt > 0)// exist data
                        {
                            alertRingMedia();
                            _txtError.setText("Barcode exist in database!");
                            bc.getText().clear();
                            bc.requestFocus();
                            return;
                        } else {
                            String _bc_Type = "C";//child
                            if (_ckbItemCode.isChecked() && _ckbLotNo.isChecked()) {
                                String lotNoParent = _edLotNo.getText().toString();
                                String itemCodeParent = _edItemCode.getText().toString();
                                if (lotNoParent.equals(P_TR_LOT_NO) && itemCodeParent.equals(P_ITEM_CODE)) {
                                    sql = "INSERT INTO INV_TR(ITEM_BC,TR_QTY,PARENT_PK,CHILD_PK,ITEM_CODE,ITEM_NAME,TR_LOT_NO,SCAN_DATE,SCAN_TIME,SENT_YN,BC_TYPE,MAPPING_YN,SLIP_NO,TR_TYPE)"
                                            + "VALUES('"
                                            + P_ITEM_BC + "',"
                                            + P_QTY + ","
                                            + pkParent + ","
                                            + P_PACKAGES_PK + ",'"
                                            + P_ITEM_CODE + "','"
                                            + P_ITEM_NAME + "','"
                                            + P_TR_LOT_NO + "','"
                                            + scan_date + "','"
                                            + scan_time + "','"
                                            + "N" + "','"
                                            + _bc_Type + "','"
                                            + "N" + "','"
                                            + " " + "','"
                                            + formID + "')";

                                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                                    db.execSQL(sql);
                                    db.close();
                                } else {
                                    onPopAlert("Child Item or Lot No are different Parent Item or Lot No");
                                    _txtError.setText("Child Item or Lot No are different Parent Item or Lot No !!!");
                                    bc.getText().clear();
                                    bc.requestFocus();
                                    return;
                                }
                            } else {
                                sql = "INSERT INTO INV_TR(ITEM_BC,TR_QTY,PARENT_PK,CHILD_PK,ITEM_CODE,ITEM_NAME,TR_LOT_NO,SCAN_DATE,SCAN_TIME,SENT_YN,BC_TYPE,MAPPING_YN,TR_TYPE,TR_ITEM_PK,SLIP_NO)"
                                        + "VALUES('"
                                        + P_ITEM_BC + "',"
                                        + P_QTY + ","
                                        + pkParent + ","
                                        + P_PACKAGES_PK + ",'"
                                        + P_ITEM_CODE + "','"
                                        + P_ITEM_NAME + "','"
                                        + P_TR_LOT_NO + "','"
                                        + scan_date + "','"
                                        + scan_time + "','"
                                        + "N" + "','"
                                        + _bc_Type + "','"
                                        + "N" + "','"
                                        + formID + "',"
                                        + P_ITEM_PK + ",'"
                                        + " " + "')";

                                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                                db.execSQL(sql);
                                db.close();
                            }

                            _txtError.setText("Save success.");
                            bc.getText().clear();
                            bc.requestFocus();
                            OnShowChild();
                        }
                    } else {
                        _txtError.setText("This Barcode was mapping !!!");
                        onPopAlert("This Barcode was mapping !!!");
                        bc.getText().clear();
                        bc.requestFocus();
                        return;
                    }


                } else {
                    _txtError.setText("Barcode is not found!");
                    bc.setText("");
                    bc.requestFocus();
                    //currentBC.getText().clear();
                    //_edQty.getText().clear();
                    return;
                }
            } catch (Exception ex) {
                bc.setText("");
                bc.requestFocus();
                Log.e("Save error:", ex.toString());

            }
        }
    }


    //****************************************************************************
    public List<String> GetDataOnServer(String type, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";
            if (type.endsWith("Parent")) {
                l_para = "1,LG_MPOS_GET_PARENT_BC," + para;
            }
            if (type.endsWith("Child")) //WH
            {
                l_para = "1,LG_MPOS_GET_CHILD_BC," + para;
            }
            //ConnectThread networkThread = new ConnectThread(gwMActivity);
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                ////if (type.endsWith("SlipNo")) {
                for (int j = 0; j < result[i].length; j++)
                    labels.add(result[i][j].toString());
                ////}
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(type + ": " + ex.getMessage());
        }
        return labels;
    }

    public void onDelAll(View view) {
        if(!checkRecordGridView(R.id.grdParent)
                && !checkRecordGridView(R.id.grdChild))return;

        String title = "Confirm Delete...";
        String mess = "Are you sure you want to delete all";
        alertDialogYN(title, mess, "onDelAll");
    }

    public void ProcessDeleteAll() {
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));

        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String para = "";
            boolean flag = true;

            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //data=new String [myLst.length];
                data=new String [1];
                int j=0;
                for (int i = 0; i < myLst.length; i++) {

                    cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='"+formID+"' and pk="+myLst[i],null);
                    if (cursor.moveToFirst()) {
                        flag = true;
                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + bsUserID;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    }
                }
                para += "|!LG_MPOS_UPD_INV_TR_DEL";
                data[j++] = para;

            } else {
                cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='"+formID+"' and SLIP_NO='-'  AND SCAN_DATE > '" + date_previous + "'", null);
                int j = 0;

                if (cursor.moveToFirst()) {
                    data = new String[1];
                    do {

                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + bsUserID;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_UPD_INV_TR_DEL";
                    data[j] = para;
                }
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP,checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        } else
        {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }


    }

    public void onDelAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();


            } else {

                db.execSQL("DELETE FROM INV_TR where  SLIP_NO NOT IN('-','') and TR_TYPE ='"+formID+"';");
            }
        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
            _edQty = (EditText) rootView.findViewById(R.id.ed_Qty);
            _edItemCode = (EditText) rootView.findViewById(R.id.ed_ItemCode);
            _edLotNo = (EditText) rootView.findViewById(R.id.ed_LotNo);
            _txtError = (TextView) rootView.findViewById(R.id.txtError);
            currentBC.setText("");
            _edQty.setText("");
            _edItemCode.setText("");
            _edLotNo.setText("");
            _txtError.setText("");
            pkParent = 0;
            OnShowParent();
            OnShowChild();
            gwMActivity.alertToastShort("Delete success..");
        }
    }

    public void onMapping(View view)  {
        /*
        ArrayList mSelectedItems = new ArrayList();
        AlertDialog.Builder builder = new AlertDialog.Builder(gwMActivity);

        builder.setTitle("What do you Like ?");
        builder.setSingleChoiceItems(seqColor, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Toast.makeText(getApplicationContext(), "You Choose : " + seqColor[arg1], Toast.LENGTH_LONG).show();
                indexDialog = arg1;
            }
        });

        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                currentBC.setText(seqColor[indexDialog]);
                //Toast.makeText(getApplicationContext(), "You Have Cancel the Dialog box", Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
        */

        if(!checkRecordGridView(R.id.grdChild)) return;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Mapping...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to Mapping");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                //Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
                //btnMapping.setVisibility(View.GONE);
                ProcessMapping();
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void ProcessMapping() {
        unique_id = CDate.getDateyyyyMMddhhmmss();
        //region PARENT YN=Y
        if(mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {

        }
        //endregion
        else{
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("SELECT CHILD_PK,TR_ITEM_PK,PARENT_PK,UOM,TR_LOT_NO,TR_QTY,ITEM_BC,EXECKEY" +
                        "    FROM INV_TR " +
                        "    WHERE DEL_IF =0 AND BC_TYPE='C' AND TR_TYPE='" + formID + "' AND SENT_YN='N' AND MAPPING_YN='N' " +
                        "   ORDER BY PK ASC ", null);
                int count = cursor.getCount();
                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount()-1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_item_bc = cursor.getString(cursor.getColumnIndex("ITEM_BC"));
                            String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            hp.updateExeckeyMapping(formID, ex_item_pk, ex_item_bc,  ex_lot_no,  unique_id);

                            para += "|" + unique_id;
                        } else {
                            para += "|" + execkey;
                        }
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";///multi row

                    } while (cursor.moveToNext());
                    para += "|!" + "LG_MPOS_PRO_MAPP_MSLIP";
                    data[j++] = para;
                    // asyns server
                    if (CNetwork.isInternet(tmpIP,checkIP)) {
                        MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                        task.execute(data);
                    } else
                    {
                        gwMActivity.alertToastLong("Network is broken. Please check network again !");
                    }
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("Upload Mapping :" + ex.getMessage());
                Log.e(" Error Upload Mapping :", ex.getMessage().toString());
            } finally {
                cursor.close();
                db.close();
            }

        }

    }

    // show data scan in
    public void OnShowParent() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            // int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdParent);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,TR_LOT_NO,PARENT_PK " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and   BC_TYPE='P' AND TR_TYPE='"+gwform_info.Mapping.Id()+"' " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));

                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[3], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PARENT_PK")), 0, fixedRowHeight,0));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvParentPK = (TextView) tr1.getChildAt(6); //Parent PK
                            String st_Parent = tvParentPK.getText().toString();

                            // load thong tin parent item bc
                            TextView tvItemBC = (TextView) tr1.getChildAt(1); //Item BC
                            String stItemBC = tvItemBC.getText().toString();
                            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                            sql = "select PARENT_PK, ITEM_BC, ITEM_CODE,TR_QTY, TR_LOT_NO " +
                                    " FROM INV_TR  " +
                                    " WHERE DEL_IF = 0  and BC_TYPE='P' AND TR_TYPE='"+gwform_info.Mapping.Id()+"' " +
                                    " AND ITEM_BC = '" + stItemBC + "' ";

                            cursor = db.rawQuery(sql, null);
                            int count = cursor.getCount();
                            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

                            if (cursor != null && cursor.moveToFirst()) {
                                for (int i = 0; i < count; i++) {
                                    pkParent = Integer.valueOf(cursor.getString(cursor.getColumnIndex("PARENT_PK")));
                                    currentBC.setText(cursor.getString(cursor.getColumnIndex("ITEM_BC")));
                                    _edItemCode.setText((cursor.getString(cursor.getColumnIndex("ITEM_CODE"))));
                                    _edQty.setText(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                                    _edLotNo.setText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")));

                                }
                            }

                            if (!st_Parent.equals("") && queue.size() < 2) {
                                int xx = tr1.getChildCount();
                                if (queue.size() < 1) {
                                    for (int i = 0; i < xx; i++) {
                                        tv11 = (TextView) tr1.getChildAt(i);
                                        tv11.setTextColor(Color.rgb(255, 99, 71));
                                    }
                                }
                                if (queue.size() > 0) {
                                    if (queue.contains(st_Parent)) {
                                        queue.remove(st_Parent);

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK);
                                        }
                                        OnShowChild();
                                        duplicate = true;
                                    }
                                }

                                if (!duplicate && queue.size() < 1) {
                                    queue.add(st_Parent);
                                    OnShowChildParent(Integer.valueOf(st_Parent));
                                }
                            }
                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
            _txtTotalGridMid.setText("Total: " + count + " ");
            _txtTotalGridMid.setTextColor(Color.BLUE);
            _txtTotalGridMid.setTextSize((float) 17.0);

        } catch (Exception ex) {
            Log.e("onClickParent", ex.getMessage());
            gwMActivity.alertToastLong("Grid Parent: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowChild() {
        Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnMapping.setVisibility(View.VISIBLE);
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select PK,ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,MAPPING_YN,TR_LOT_NO,SLIP_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and BC_TYPE='C' AND TR_TYPE='"+gwform_info.Mapping.Id()+"' " +
                    " ORDER BY pk desc LIMIT 20 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[3], fixedRowHeight,1));
                    if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y"))
                        row.addView(makeTableColWithText("YES", scrollableColumnWidths[2], fixedRowHeight,0));
                    else
                        row.addView(makeTableColWithText("-", scrollableColumnWidths[2], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, -1));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;
                        @Override
                        public void onClick(View v) {
                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(7); //SLIP_NO
                            String st_slipNO=tvSlipNo.getText().toString();

                            if(st_slipNO.equals("-") || st_slipNO.equals(" ")){

                                TextView tv1 = (TextView) tr1.getChildAt(8); //PK
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255,99,71));
                                }

                                if (queue.size() > 0) {
                                    if (queue.contains(tv1.getText().toString())) {
                                        queue.remove(tv1.getText().toString());

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK );
                                        }
                                        duplicate = true;
                                    }
                                }
                                if (!duplicate)
                                    queue.add(tv1.getText().toString());
                            }
                            gwMActivity.alertToastShort(String.valueOf(queue.size()));

                        }
                    });
                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND BC_TYPE = 'C' AND TR_TYPE='"+gwform_info.Mapping.Id()+"'  ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowChildParent(int parent) {
        Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnMapping.setVisibility(View.VISIBLE);
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            // int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,MAPPING_YN,TR_LOT_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and BC_TYPE='C' AND TR_TYPE='"+ gwform_info.Mapping.Id()+"' and PARENT_PK=" + parent +
                    " ORDER BY pk desc  ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[3], fixedRowHeight,1));
                    if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y"))
                        row.addView(makeTableColWithText("YES", scrollableColumnWidths[2], fixedRowHeight,0));
                    else
                        row.addView(makeTableColWithText("-", scrollableColumnWidths[2], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, 0));

                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.MAGENTA);
            _txtTotalQtyBot.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }


    ///////////////////////////////////////////////
    public void onListGridBot(View view)  {
        if(!checkRecordGridView(R.id.grdChild)) return;

        Intent openNewActivity = new Intent(view.getContext(), GridListViewBot.class);
        //send data into Activity
        openNewActivity.putExtra("type", "10");
        //startActivity(openNewActivity);
        startActivityForResult(openNewActivity, REQUEST_CODE);
    }

    /**
     * Xử lý kết quả trả về ở đây
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiểm tra có đúng requestCode =REQUEST_CODE_INPUT hay không
        //Vì ta có thể mở Activity với những RequestCode khác nhau
        if (requestCode == REQUEST_CODE) {
            //Kiểm trả ResultCode trả về, cái này ở bên InputDataActivity truyền về
            OnShowChild();
            switch (resultCode) {
                case RESULT_CODE:

                    break;
            }
        }
    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Mapping", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[2], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    // end get data return array--use array set to grid view



    /////////////////////////
    // check data inv_tr
    public int Check_Exist_PK(String l_bc_item) {
        int countRow = 0;
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String countQuery = "SELECT PK FROM INV_TR where del_if=0 and TR_TYPE='"+formID+"' and ITEM_BC ='" + l_bc_item + "' ";
            // db = gwMActivity.getReadableDatabase();
            cursor = db.rawQuery(countQuery, null);
            // Check_Exist_PK=cursor.getCount();
            if (cursor != null && cursor.moveToFirst()) {
                countRow = cursor.getCount();
            }

        } catch (Exception ex) {
            gwMActivity.alertToastLong("Check_Exist_PK :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return countRow;

    }

    ////////////////////////////////////////////////
    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event

                if (_type.equals("onDelAll")) {
                    ProcessDeleteAll();
                }
                if (_type.equals("onMakeSlip")) {


                    //ProcessMakeSlip();

                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str = str_alert;

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            // onLoadFull();
            Intent i = new Intent(gwMActivity, AppConfig.class);
            i.putExtra("type",formID );
            i.putExtra("user", bsUserID);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onLoadFull() {
        Intent openNewActivity = new Intent(gwMActivity, GridListView.class);
        //send data into Activity
        openNewActivity.putExtra("type", "10");
        startActivity(openNewActivity);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
              

                break;
            case R.id.btnUploadMapping:
              onMapping(view);

                break;
            case R.id.btnViewStt:
               
                break;
            case R.id.btnSelectMid:
               

                break;
            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnList:
                

                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnMakeSlip:
                

                break;
            default:
                break;
        }
    }
}
