package gw.genumobile.com.views.inventory;

import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;

import gw.genumobile.com.R;
import gw.genumobile.com.models.Config;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.HandlerWebService;


public class Download extends Activity {
	//POINT location download and upload
	private String POINT="Default";
	SQLiteDatabase db;
	Cursor cursor;
	private TextView tvFromDateLable,tvToDateLable;
    private Button btnClose,btnDownload,btnFromDateLable,btnToDateLable;
    
    private TextView tvFromDateReq,tvToDateReq;
    private Button btnFromDateReq,btnToDateReq;
    private int year;
    private int month;
    private int day;
    private int year1;
    private int month1;
    private int day1;
    private String strDay,strMonth;
    static final int DATE_PICKER_FROM_LABEL_ID = 1111;
    static final int DATE_PICKER_TO_LABEL_ID = 1112; 
    static final int DATE_PICKER_FROM_REQ_ID = 1113;
    static final int DATE_PICKER_TO_REQ_ID = 1114;
    
    ProgressDialog progressBar;
	private int progressBarStatus = 0;
	private Handler progressBarHandler = new Handler();
	private int errors = 0;
	private int iNumber = 0;
	private int iNumber_req = 0;
	private int iNumber_wh = 0;
	private int iNumber_wh_loc = 0;
	private int iNumber_line = 0;
	private int iNumber_wh_user = 0;
	private long fileSize = 0;
	// /////////////////
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_download);
		//boolean read = ReadFileConfig();
		//OnCreateTableINV_TR();
		
		tvFromDateLable = (TextView) findViewById(R.id.tvFromDateLable);
		tvToDateLable=(TextView)findViewById(R.id.tvToDateLable);
		tvFromDateReq = (TextView) findViewById(R.id.tvFromDateReq);
		tvToDateReq=(TextView)findViewById(R.id.tvToDateReq);
		
		btnClose = (Button) findViewById(R.id.btnClose);
		btnDownload = (Button) findViewById(R.id.btnDownload);
		btnFromDateLable=(Button) findViewById(R.id.btnFromDateLable);
		btnToDateLable=(Button) findViewById(R.id.btnToDateLable);
		btnFromDateReq=(Button) findViewById(R.id.btnFromDateReq);
		btnToDateReq=(Button) findViewById(R.id.btnToDateReq);
		
		btnClose.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// do something when the button is clicked
				finish();
				System.exit(0);
				
			}
		});
		///////////////////////////
        // Get current date by calender
        final Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DATE, -30);
        year  = c1.get(Calendar.YEAR);
        month = c1.get(Calendar.MONTH);
        day   = c1.get(Calendar.DAY_OF_MONTH);
        
        final Calendar c2 = Calendar.getInstance();
        year1  = c2.get(Calendar.YEAR);
        month1 = c2.get(Calendar.MONTH);
        day1   = c2.get(Calendar.DAY_OF_MONTH);
        
        // Show current date
        tvFromDateLable.setText(new StringBuilder()
            // Month is 0 based, just add 1
        	.append(year).append("-").append(isPushNumber(month + 1)).append("-").append(isPushNumber(day)).append(" "));
        tvToDateLable.setText(new StringBuilder()
        	.append(year1).append("-").append(isPushNumber(month1 + 1)).append("-").append(isPushNumber(day1)).append(" "));
        // Show current date
        tvFromDateReq.setText(new StringBuilder()
        	.append(year).append("-").append(isPushNumber(month + 1)).append("-").append(isPushNumber(day)).append(" "));
        tvToDateReq.setText(new StringBuilder()
        	.append(year1).append("-").append(isPushNumber(month1 + 1)).append("-").append(isPushNumber(day1)).append(" "));
	}
	
	public void onChangeFromDateLable(View view) throws InterruptedException,ExecutionException {
   	 	showDialog(DATE_PICKER_FROM_LABEL_ID);
   }
	public void onChangeToDateLable(View view) throws InterruptedException,ExecutionException {
   	 	showDialog(DATE_PICKER_TO_LABEL_ID);
   }
   public void onChangeFromDateReq(View view) throws InterruptedException,ExecutionException {
   	 	showDialog(DATE_PICKER_FROM_REQ_ID);
   }
   public void onChangeToDateReq(View view) throws InterruptedException,ExecutionException {
   	 	showDialog(DATE_PICKER_TO_REQ_ID);
   }
   @Override
   protected Dialog onCreateDialog(int id) {
       switch (id) {
	       case DATE_PICKER_FROM_LABEL_ID:           
	           // open datepicker dialog. 
	           // set date picker for current date 
	           // add pickerListener listner to date picker
	           return new DatePickerDialog(this, pickerFromLable, year, month,day);
	       
		   case DATE_PICKER_TO_LABEL_ID:           
		       return new DatePickerDialog(this, pickerToLable, year1, month1,day1);
		   case DATE_PICKER_FROM_REQ_ID:           
		       return new DatePickerDialog(this, pickerFromReq, year, month,day);
		   case DATE_PICKER_TO_REQ_ID:           
		       return new DatePickerDialog(this, pickerToReq, year1, month1,day1);    
	   }
       return null;
   }

   private DatePickerDialog.OnDateSetListener pickerFromLable = new DatePickerDialog.OnDateSetListener() {
       // when dialog box is closed, below method will be called.
       @Override
       public void onDateSet(DatePicker view, int selectedYear,
               int selectedMonth, int selectedDay) {
            
           year  = selectedYear;
           month = selectedMonth;
           day   = selectedDay;
           // Show selected date 
           tvFromDateLable.setText(new StringBuilder()
           			.append(year).append("-").append(isPushNumber(month + 1)).append("-").append(isPushNumber(day)).append("")); 
      }
   };
   private DatePickerDialog.OnDateSetListener pickerToLable = new DatePickerDialog.OnDateSetListener() {
       // when dialog box is closed, below method will be called.
       @Override
       public void onDateSet(DatePicker view, int selectedYear,
               int selectedMonth, int selectedDay) {
            
           year  = selectedYear;
           month = selectedMonth;
           day   = selectedDay;
           // Show selected date 
           tvToDateLable.setText(new StringBuilder()
           		.append(year).append("-").append(isPushNumber(month + 1)).append("-").append(isPushNumber(day)).append("")); 
      }
   };
   private DatePickerDialog.OnDateSetListener pickerFromReq = new DatePickerDialog.OnDateSetListener() {
       // when dialog box is closed, below method will be called.
       @Override
       public void onDateSet(DatePicker view, int selectedYear,
               int selectedMonth, int selectedDay) {
            
           year  = selectedYear;
           month = selectedMonth;
           day   = selectedDay;
           // Show selected date 
           tvFromDateReq.setText(new StringBuilder()
           		.append(year).append("-").append(isPushNumber(month + 1)).append("-").append(isPushNumber(day)).append("")); 
      }
   };
   private DatePickerDialog.OnDateSetListener pickerToReq = new DatePickerDialog.OnDateSetListener() {
       // when dialog box is closed, below method will be called.
       @Override
       public void onDateSet(DatePicker view, int selectedYear,
               int selectedMonth, int selectedDay) {
            
           year  = selectedYear;
           month = selectedMonth;
           day   = selectedDay;
           // Show selected date 
           tvToDateReq.setText(new StringBuilder()
           		.append(year).append("-").append(isPushNumber(month + 1)).append("-").append(isPushNumber(day)).append("")); 
      }
   };
	
	// /////////////////////////////////////////
	// DOWNLOAD DATA
	// /////////////////////////////////////////
	public void OnDownload(View v) throws InterruptedException,ExecutionException {

		boolean read= Config.ReadFileConfig();
		POINT=Config.POINT;
		
		btnDownload = (Button) findViewById(R.id.btnDownload);
		btnDownload.setClickable(false);
		
		String tmp="";
		OnDeleteTableData();
		
		System.out.print("\n\n button download click \n\n");
		tvFromDateLable = (TextView) findViewById(R.id.tvFromDateLable);
		tmp=tvFromDateLable.getText().toString();		
		String date1=tmp.replace("-", "");
		
		tmp=tvToDateLable.getText().toString();
		String date2=tmp.replace("-", "");
		
		tmp=tvFromDateReq.getText().toString();		
		String date3=tmp.replace("-", "");
		
		tmp=tvToDateReq.getText().toString();
		String date4=tmp.replace("-", "");
	
		//String para1 = "20130101" + "|" + "20131201";
//		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
//		for(int vt=0;vt<5;vt++){
//			if(vt==1){
//				DownloadAsyncTask networkThread = new DownloadAsyncTask(this);
//				networkThread.execute("1,lg_mpos_m010_get_label," +date1+ "|" + date2);
//			}
//		if(vt==1){
//				DownloadAsyncTask networkThread = new DownloadAsyncTask(this);
//				networkThread.execute("1,lg_mpos_m010_get_gd_req," +date3+ "|" + date4+ "|"+POINT);
//			}
//			if(vt==2){
//				DownloadAsyncTask networkThread1 = new DownloadAsyncTask(this);
//				networkThread1.execute("1,lg_mpos_m010_get_wh,date1" + "|" + "date2");	
//			}
//			if(vt==3){
//				DownloadAsyncTask networkThread = new DownloadAsyncTask(this);
//				networkThread.execute("1,lg_mpos_m010_get_line,date1" + "|" + "date2");
//			}
//
//		}
		

		
		ConnectThread networkThread = new ConnectThread(this);
		final String req[][] = networkThread.execute("1,lg_mpos_m010_get_gd_req," +date3+ "|" + date4+ "|"+POINT).get();
		
		networkThread = new ConnectThread(this);
		final String label[][] = networkThread.execute("1,lg_mpos_m010_get_label,"+date1 + "|" + date2).get();
		
		networkThread = new ConnectThread(this);
		final String wh[][] = networkThread.execute("1,lg_mpos_m010_get_wh,"+date1 + "|" + date2).get();

		networkThread = new ConnectThread(this);
		final String wh_user[][] = networkThread.execute("1,lg_mpos_m010_get_wh_user,"+date1 + "|" + date2).get();
		
		networkThread = new ConnectThread(this);
		final String wh_loc[][] = networkThread.execute("1,lg_mpos_m010_get_whloc,"+date1 + "|" + date2).get();

		networkThread = new ConnectThread(this);
		final String line[][] = networkThread.execute("1,lg_mpos_m010_get_line,"+date1 + "|" + date2).get();
		
		System.out.print("\n\n --req.length: " + String.valueOf(label.length));
		
		System.out.print("\n\n data download to array ok. \n\n");
		
		
		// INSERT DATA INTO DB
		// prepare for a progress bar dialog
		final int max = label.length + req.length;
		progressBar = new ProgressDialog(v.getContext());
		progressBar.setCancelable(false);//not close when touch
		progressBar.setMessage("download successful.Inserting database into device...");
		progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressBar.setProgress(0);
		progressBar.setMax(label.length + req.length);
		progressBar.show();
		
		//reset progress bar status
		progressBarStatus = 0;
		
		// final int max = label.length+req.length;
		iNumber = 0;
		iNumber_req = 0;
		iNumber_wh = 0;
		iNumber_wh_loc = 0;
		iNumber_line = 0;
		iNumber_wh_user = 0;
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		
		new Thread(new Runnable() {
			public void run() {

				while (progressBarStatus < max) 
				{
					if (progressBarStatus < label.length) 
					{
						iNumber = InsertDataIntoDBTLG_LABEL(label, iNumber);
		
						if (progressBarStatus < wh.length) {
							// System.out.println("length wh :"+ wh.length);
							iNumber_wh = InsertDataWH(wh, iNumber_wh);
						}
						if (progressBarStatus < wh_loc.length) {
							iNumber_wh_loc = InsertDataWH_LOC(wh_loc,iNumber_wh_loc);
						}
						if (progressBarStatus < line.length) {
							// System.out.println("length wh :"+ wh.length);
							iNumber_line = InsertDataWH_Line(line, iNumber_line);		
						}
						/*
						if (progressBarStatus < wh_user.length) {
							// System.out.println("length wh :"+ wh.length);
							iNumber_wh_user =  (wh_user, iNumber_wh_user);
						}*/
						progressBarStatus++;
						// System.out.print("\n row num ++ is label "+progressBarStatus);
					}		
					else {
						try 
						{
							iNumber_req = InsertDataIntoDBTLG_GD_REQ(req,iNumber_req);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ExecutionException e) {
							// TODO Auto-generated catch block
							System.out.println("Error:InsertDataIntoDBTLG_GD_REQ->"+e);
							e.printStackTrace();
						}
						progressBarStatus++;
						// System.out.print("\n row num  is req "+progressBarStatus);
		
					}
					// Update the progress bar
					progressBarHandler.post(new Runnable() {
						public void run() {
							progressBar.setProgress(progressBarStatus);
						}
					});
				}
				// ok, file is downloaded,
				if (progressBarStatus >= max) {
					// sleep 2 seconds, so that you can see the 100%
					try {
						
						Thread.sleep(2000);
						onEnbleButton();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					// close the progress bar dialog
					progressBar.dismiss();
					db.close();
					
				}
			}
		}).start();
		
		
	}
	public void OnCreateTableINV_TR() {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		// CREATE TABLE IF NOT EXISTS INV_TR
		// db.execSQL("DROP TABLE IF EXISTS INV_TR ");//khi chep file qua may
		// khac khoi tao lai table.
		String CREATE_INV_TR = "CREATE TABLE IF NOT EXISTS INV_TR ( "
				+ "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "TLG_POP_LABEL_PK INTEGER, " + "TR_WAREHOUSE_PK INTEGER, "
				+ "TR_LOC_ID TEXT, " + "TR_QTY INTEGER, " + "TR_LOT_NO TEXT, "
				+ "TR_TYPE TEXT, " + "ITEM_BC TEXT, " + "TR_DATE TEXT, "
				+ "TR_ITEM_PK INTEGER, " + "TR_LINE_PK INTEGER, "
				+ "ITEM_CODE TEXT, " + "ITEM_NAME TEXT, " + "ITEM_TYPE TEXT, "
				+ "TLG_GD_REQ_D_PK INTEGER, " + "TR_WH_IN_NAME TEXT, "
				+ "LOC_NAME TEXT, " + "LINE_NAME TEXT, " + "SLIP_NO TEXT,DEL_IF INTEGER default 0 )";
		db.execSQL(CREATE_INV_TR);
		
		// CREATE TABLE TLG_LABEL
		db.execSQL("CREATE TABLE IF NOT EXISTS TLG_LABEL(PK NUMBER, TLG_IT_ITEM_PK NUMBER, ITEM_BC VARCHAR2,"
				+ " LABEL_UOM VARCHAR2, YYYYMMDD VARCHAR2, ITEM_CODE VARCHAR2, ITEM_NAME VARCHAR2, ITEM_TYPE VARCHAR2,"
				+ "LABEL_QTY NUMBER, LOT_NO VARCHAR2);");
		
		// CREATE TABLE TLG_GD_REQ
		db.execSQL("CREATE TABLE IF NOT EXISTS TLG_GD_REQ(TLG_GD_REQ_M_PK NUMBER, REQ_DATE VARCHAR2, SLIP_NO VARCHAR2"
				+ ", TLG_GD_REQ_D_PK NUMBER, REQ_ITEM_PK NUMBER, REQ_QTY NUMBER,REQ_UOM VARCHAR2, OUT_WH_PK NUMBER, REF_NO VARCHAR2, OVER_RATIO NUMBER);");
		
		// CREATE TABLE tlg_in_warehouse
		db.execSQL("CREATE TABLE IF NOT EXISTS tlg_in_warehouse(PK NUMBER,wh_id VARCHAR2,wh_name VARCHAR2);");
		
		// CREATE TABLE tlg_in_whloc
		db.execSQL("CREATE TABLE IF NOT EXISTS tlg_in_whloc(PK NUMBER, loc_id VARCHAR2, loc_name VARCHAR2);");
		
		// CREATE TABLE tlg_pb_line
		db.execSQL("CREATE TABLE IF NOT EXISTS tlg_pb_line(PK NUMBER,line_id VARCHAR2,line_name VARCHAR2);");

		// CREATE TABLE tlg_lg_user_wh
		db.execSQL("CREATE TABLE IF NOT EXISTS tlg_lg_user_wh(PK NUMBER,user_pk VARCHAR2,wh_name VARCHAR2);");
		
		db.close();
	}
	public void OnDeleteTableData(){
		
		CheckBox ck = (CheckBox)findViewById(R.id.ckbClearData);
		if(ck.isChecked() == true)
		{
			db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
			db.delete("TLG_LABEL", null, null);
			//db.delete("TLG_GD_REQ", null, null);
			//db.delete("tlg_in_warehouse", null, null);
			//db.delete("tlg_in_whloc", null, null);
			//db.delete("tlg_pb_line", null, null);
			
			db.close();
		}
		
	}
	public int InsertDataIntoDBTLG_LABEL(String a[][], int i) {

		
		//Log.e("InsertDataIntoDBTLG_LABEL", "InsertDataIntoDBTLG_LABEL"+ " -- i: " + String.valueOf(i));

		db.execSQL("CREATE TABLE IF NOT EXISTS TLG_LABEL(PK NUMBER, TLG_IT_ITEM_PK NUMBER, ITEM_BC VARCHAR2,"
				+ " LABEL_UOM VARCHAR2, YYYYMMDD VARCHAR2, ITEM_CODE VARCHAR2, ITEM_NAME VARCHAR2, ITEM_TYPE VARCHAR2,"
				+ "LABEL_QTY NUMBER, LOT_NO VARCHAR2);");

		if (isFullData(a, i) == true) {
			int PK = Integer.parseInt(a[i][0]);
			int TLG_IT_ITEM_PK = Integer.parseInt(a[i][1]);
			// String ITEM_TYPE = Integer.parseInt(a[i][7]);
			float LABEL_QTY = Float.parseFloat(a[i][8]);
			// Log.e("*************", "***************************");
			// Insert data into TLG_GD_REQ table
			db.execSQL("INSERT INTO TLG_LABEL VALUES(" + PK + ","
					+ TLG_IT_ITEM_PK + ",'" + a[i][2] + "','" + a[i][3] + "','"
					+ a[i][4] + "','" + a[i][5] + "','" + a[i][6] + "',"
					+ a[i][7] + "," + LABEL_QTY + ",'" + a[i][9] + "');");
		}
		else
			errors++;

		Log.e(" -- i+1 ", String.valueOf((i + 1)));
		return (i + 1);

	}

	public int InsertDataIntoDBTLG_GD_REQ(String a[][], int index) throws InterruptedException, ExecutionException
	{
		//Log.e("InsertDataIntoDBTLG_GD_REQ", "InsertDataIntoDBTLG_GD_REQ"+ " -- index: " + String.valueOf(index));
		// db.execSQL("DROP TABLE IF EXISTS TLG_GD_REQ ");
		String aa="a";
		if (isFullData(a, index) == true) {

			db.execSQL("CREATE TABLE IF NOT EXISTS TLG_GD_REQ(TLG_GD_REQ_M_PK NUMBER, REQ_DATE VARCHAR2, SLIP_NO VARCHAR2"
					+ ", TLG_GD_REQ_D_PK NUMBER, REQ_ITEM_PK NUMBER, REQ_QTY NUMBER,REQ_UOM VARCHAR2, OUT_WH_PK NUMBER, REF_NO VARCHAR2, OVER_RATIO NUMBER);");
			int TLG_GD_REQ_M_PK = Integer.parseInt(a[index][0]);
			int TLG_GD_REQ_D_PK = Integer.parseInt(a[index][3]);
			int REQ_ITEM_PK = Integer.parseInt(a[index][4]);
			float REQ_QTY = Float.parseFloat(a[index][5]);
			int OUT_WH_PK = Integer.parseInt(a[index][7]);
			int OVER_RATIO = Integer.parseInt(a[index][9]);
						
			//Check ton tai
        	db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
   		 	String SQL_SEL_GD_REQ1 = "SELECT  TLG_GD_REQ_M_PK AS PK,REQ_QTY AS REQ_QTY FROM TLG_GD_REQ WHERE REQ_QTY ='"+REQ_QTY+"' and TLG_GD_REQ_M_PK='"+TLG_GD_REQ_M_PK+"'";
   		    cursor = db.rawQuery(SQL_SEL_GD_REQ1, null);

   		    System.out.println(SQL_SEL_GD_REQ1);
            if (cursor.getCount() < 1)
            {
            	// Insert data into DB
    			db.execSQL("INSERT INTO TLG_GD_REQ VALUES(" + TLG_GD_REQ_M_PK+ ",'" 
									    					+ a[index][1] + "','" 
									    					+ a[index][2] + "',"
									    					+ TLG_GD_REQ_D_PK + "," 
									    					+ REQ_ITEM_PK + "," 
									    					+ REQ_QTY + ",'" 
									    					+ a[index][6] + "'," 
									    					+ OUT_WH_PK + ",'"
									    					+ a[index][8] + "'," 
									    					+ OVER_RATIO + ");");

    			String para=POINT+"|!" + TLG_GD_REQ_M_PK + "|!"+ REQ_QTY;
    			//Insert Log Table download
    			String kq= HandlerWebService.InsertTableArg("lg_mpos_upd_get_gd_req", para);

            }else{
            	db.execSQL("UPDATE  TLG_GD_REQ Set REQ_QTY='"+ REQ_QTY +"' where TLG_GD_REQ_M_PK="+TLG_GD_REQ_M_PK+"");
            }
			//--------------------------------------------//
			
		} else
			errors++;
		Log.e(" -- index+1 ", String.valueOf((index + 1)));
		return (index + 1);
	}

	// download data WH
	public int InsertDataWH(String a[][], int i) {
		//Log.e("InsertDataInto tlg_in_warehouse", "InsertDataInto tlg_in_warehouse"+ " -- index: " + String.valueOf(i));
		//db.execSQL("DROP TABLE IF EXISTS tlg_in_warehouse ");
		db.execSQL("CREATE TABLE IF NOT EXISTS tlg_in_warehouse(PK NUMBER,wh_id VARCHAR2,wh_name VARCHAR2);");

		if (isFullData(a, i) == true) {
			int PK = Integer.parseInt(a[i][0]);
			// Insert data into TLG_IN_WAREHOUSE table check exists
		//	db.execSQL("INSERT  INTO tlg_in_warehouse(pk,wh_id,wh_name) VALUES("
		//			+ PK + ",'" + a[i][1] + "','" + a[i][2] + "');");
						
			db.execSQL("INSERT  INTO tlg_in_warehouse(pk,wh_id,wh_name)"
					+ "SELECT " + PK + ",'" + a[i][1] + "','" + a[i][2] + "' "
					+ " WHERE NOT EXISTS (SELECT 1 FROM tlg_in_warehouse WHERE pk = '" + PK + "');");
			
			System.out.println("data insert to table wh_line :" + i);
		}
		else{
			errors++;
		}	
		Log.e(" -- i+1 ", String.valueOf((i + 1)));
		return (i + 1);

	}

	// download data WH Location
	public int InsertDataWH_LOC(String a[][], int i) {
		//Log.e("InsertDataInto TLG_IN_WHLOC", "InsertDataInto TLG_IN_WHLOC"+ " -- index: " + String.valueOf(i));
		
		db.execSQL("CREATE TABLE IF NOT EXISTS tlg_in_whloc(PK NUMBER, loc_id VARCHAR2, loc_name VARCHAR2);");
		
		if (isFullData(a, i) == true) {
			int PK = Integer.parseInt(a[i][0]);
			// Insert data into TLG_IN_WHLOC table
			//db.execSQL("INSERT INTO tlg_in_whloc VALUES(" + PK + ",'" + a[i][1] + "','" + a[i][2] + "');");
			
			db.execSQL("INSERT  INTO tlg_in_whloc(pk,loc_id,loc_name)"
					+"SELECT " + PK + ",'" + a[i][1] + "','" + a[i][2] + "' "
					+" WHERE NOT EXISTS (SELECT 1 FROM tlg_in_whloc WHERE pk = '"+PK+ "');");
			
			System.out.print("insert to table WHLOC finish row : " + i);
		}
		else
			errors++;

		Log.e(" -- i+1 ", String.valueOf((i + 1)));
		return (i + 1);
	}
	
	// download data Line
	public int InsertDataWH_Line(String a[][], int i) {

		//db.execSQL("DROP TABLE IF EXISTS tlg_pb_line ");
		db.execSQL("CREATE TABLE IF NOT EXISTS tlg_pb_line(PK NUMBER,line_id VARCHAR2,line_name VARCHAR2);");

		if (isFullData(a, i) == true) {
			int PK = Integer.parseInt(a[i][0]);
			// Insert data into TLG_PB_LINE table
			//db.execSQL("INSERT INTO tlg_pb_line(pk,line_id,line_name) VALUES("
			//		+ PK + ",'" + a[i][1] + "','" + a[i][2] + "');");
			
			db.execSQL("INSERT  INTO tlg_pb_line(pk,line_id,line_name)"
					+"SELECT " + PK + ",'" + a[i][1] + "','" + a[i][2] + "' "
					+" WHERE NOT EXISTS (SELECT 1 FROM tlg_pb_line WHERE pk = '"+PK+ "');");
			System.out.print("data insert to table wh_line " + i);
		}
		else
			errors++;

		Log.e(" -- i+1 ", String.valueOf((i + 1)));
		Log.e("InsertDataWH_Line", "InsertDataWH_Line"+ " -- index: " + String.valueOf(i));
		return (i + 1);
	}

	// download data WH User
	public int InsertDataWH_User(String a[][], int i) {
		//Log.e("InsertDataInto TLG_IN_WHLOC", "InsertDataInto TLG_IN_WHLOC"+ " -- index: " + String.valueOf(i));

		db.execSQL("CREATE TABLE IF NOT EXISTS tlg_lg_user_wh(PK NUMBER, user_pk VARCHAR2, wh_pk VARCHAR2);");

		if (isFullData(a, i) == true) {
			int PK = Integer.parseInt(a[i][0]);
			db.execSQL("INSERT  INTO tlg_lg_user_wh(pk,user_pk,wh_pk)"
					+"SELECT " + PK + ",'" + a[i][1] + "','" + a[i][2] + "' "
					+" WHERE NOT EXISTS (SELECT 1 FROM tlg_lg_user_wh WHERE pk = '"+PK+ "');");

			System.out.print("insert to table WH_User finish row : " + i);
		}
		else
			errors++;

		Log.e(" -- i+1 ", String.valueOf((i + 1)));
		return (i + 1);
	}

	public boolean isFullData(String a[][], int index) {
		for (int j = 0; j < a[0].length; j++)
			if (a[index][j].length() == 0)
				return false;
		return true;
	}
	public String isPushNumber(int x){
		String temp;
		if(x<10){
			temp="0"+String.valueOf(x);
		}
		else
			temp=String.valueOf(x);
		return temp;
	}
	
	public void onEnbleButton(){
		btnDownload = (Button) findViewById(R.id.btnDownload);
		btnDownload.setClickable(true);
	}
}
