package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.interfaces.GridListViewOnePK;
import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.views.PopDialogGrid;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.GridListViewMid;
import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.models.Config;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;

public class ExchangeProduct extends gwFragment implements View.OnClickListener {
    protected Boolean flagPopgrd3 = true;
    protected Boolean flagUpload = true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;
    private static final String formID = "20";
    Queue queue = new LinkedList();
    Queue queueMid = new LinkedList();

    SimpleDateFormat df;
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    TextView reqWH, msg_error, txtDate, txtTT, txtSentLog, txtRem, txtTotalGridMid, txtTotalGridBot, txtTotalQtyBot, _txtTime, txtCountRequest, txtQtyRequest;
    EditText edt_bc1, edt_bc2, edt_reqNo, edt_SOF, edt_SOT, edt_qty;
    String sql = "", scan_date = "", scan_time = "", unique_id = "", lbl_SlipNo1_Tag = "", req_pk = "", SOF_Pk = "", SOF_SlipNo = "", SOT_Pk = "", SOT_SlipNo = "", ex_qty = "", slipNO = "";
    String data[] = new String[0];
    String P_ITEM_BC, P_SLIP_NO, wh_pk = "";
    int timeUpload = 0;
    Button btnViewStt, btnSelectMid, btnList, btnInquiry, btnMakeSlip, btnDelAll, btnApprove, btnListBot;
    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_exchange_product, container, false);


        btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
        btnApprove.setOnClickListener(this);
        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnSelectMid = (Button) rootView.findViewById(R.id.btnSelectMid);
        btnSelectMid.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);

        txtDate = (TextView) rootView.findViewById(R.id.txtDate);
        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        txtDate.setText(formattedDate);
        _txtTime = (TextView) rootView.findViewById(R.id.txtTime);
        msg_error = (TextView) rootView.findViewById(R.id.txtError);
        msg_error.setTextColor(Color.RED);
        msg_error.setTextSize((float) 16.0);
        msg_error.setText("");
        edt_bc1 = (EditText) rootView.findViewById(R.id.edit_Bc1);
        edt_bc2 = (EditText) rootView.findViewById(R.id.edit_Bc2);
        edt_reqNo = (EditText) rootView.findViewById(R.id.edit_reqNo);
        edt_SOF = (EditText) rootView.findViewById(R.id.edit_SOF);
        edt_SOT = (EditText) rootView.findViewById(R.id.edit_SOT);
        edt_qty = (EditText) rootView.findViewById(R.id.edit_Qty);
        if (Measuredwidth <= 600) {
            txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);
        }

        OnShowGW_Header();
        OnShowScanLog();
        OnShowScanIn();
        OnShowScanAccept();
        CountSendFirst();
        init_color();
        edt_bc1.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keycode, KeyEvent keyEvent) {

                if (keycode == KeyEvent.KEYCODE_ENTER) {
                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                        if (edt_bc1.getText().toString().equals("")) {
                            msg_error.setText("Pls scan barcode!");
                        } else {
                            onSaveData();
                        }
                    }
                    return true;//important for event onKeyDown
                }
                if (keycode == KeyEvent.KEYCODE_BACK) {
                    edt_bc1.clearFocus();
                }
                return false;
            }
        });
        timeUpload = hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);

        return rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run() {
            if (flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload * 1000);
        }
    };

    private void init_color() {
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private void doStart() {
        try {
            flagUpload = false;//waiting data server response
            db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            //cursor2 = db2.rawQuery("select TLG_POP_LABEL_PK, TR_WH_IN_PK,TR_LOC_ID, TR_QTY , TR_LOT_NO, TR_TYPE, ITEM_BC, TR_DATE, TR_ITEM_PK, TR_LINE_PK, ITEM_CODE, ITEM_NAME,ITEM_TYPE,PK,TLG_GD_REQ_D_PK, SLIP_NO  from INV_TR where DEL_IF=0 and TR_TYPE='6' and sent_yn = 'N' order by PK asc LIMIT 1",null);
            cursor2 = db2.rawQuery("select PK, ITEM_BC,GD_SLIP_NO,TLG_SA_SALEORDER_D_PK,SUPPLIER_PK  from INV_TR where DEL_IF=0 and TR_TYPE='" + formID + "' and sent_yn = 'N' order by PK asc LIMIT 20", null);
            System.out.print("\n\n\n cursor2.count: " + String.valueOf(cursor2.getCount()));
            //data=new String [cursor2.getCount()];
            boolean read = Config.ReadFileConfig();

            if (cursor2.moveToFirst()) {

                //set value message
                msg_error = (TextView) rootView.findViewById(R.id.txtError);
                msg_error.setTextColor(Color.RED);
                msg_error.setText("");

                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                ///// TODO: 2016-01-13  view
                do {
                    flag = true;
                    for (int i = 0; i < cursor2.getColumnCount(); i++) {
                        if (para.length() <= 0) {
                            if (cursor2.getString(i) != null)
                                para += cursor2.getString(i) + "|";
                            else
                                para += "|";
                        } else {

                            if (flag == true) {
                                if (cursor2.getString(i) != null) {
                                    para += cursor2.getString(i);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor2.getString(i) != null)
                                    para += "|" + cursor2.getString(i);
                                else
                                    para += "|";
                            }
                        }

                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "*|*";

                } while (cursor2.moveToNext());
                //////////////////////////
                para += "|!LG_MPOS_UPL_EXCHANGE_PROD";
                data[j] = para;
                //System.out.print("\n\n\n para upload GD: " + para);
                Log.e("para upload Exchange: ", para);


                if (CNetwork.isInternet(tmpIP, checkIP)) {
                    ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                    task.execute(data);
                    // Write your code here to invoke YES event
                    gwMActivity.alertToastShort("Send to Server");
                } else {
                    flagUpload = true;
                    gwMActivity.alertToastLong("Network is broken. Please check network again !");
                }

            } else {
                flagUpload = true;//reset do start
            }
            cursor2.close();
            db2.close();

        } catch (Exception ex) {
            msg_error.setText(ex.getMessage());
        }

    }

    public void onSaveData() {

        if (edt_bc1.getText().equals("")) {
            msg_error.setText("Pls Scan Barcode!");
            return;
        }
        String str_scan = edt_bc1.getText().toString().toUpperCase();
        int length = edt_bc1.getText().length();
        String req_no = str_scan.substring(1, length);
        try {
            if (str_scan.equals("")) {
                msg_error.setText("Pls Scan Barcode!");
                return;
            }
            if (lbl_SlipNo1_Tag.equals("") && str_scan.indexOf("S") == 0) {
                List<String> labels = GetDataOnServer("Exchange", req_no);
                if (labels.size() > 0) {
                    req_pk = labels.get(0);
                    SOF_Pk = labels.get(1);
                    SOF_SlipNo = labels.get(2);
                    SOT_Pk = labels.get(3);
                    SOT_SlipNo = labels.get(4);
                    ex_qty = labels.get(5);
                    wh_pk = labels.get(6);//WH_PK

                    lbl_SlipNo1_Tag = req_no;
                    edt_reqNo.setText(req_no);
                    edt_SOF.setText(SOF_SlipNo);
                    edt_SOT.setText(SOT_SlipNo);
                    edt_qty.setText(ex_qty);

                    ////////////////
                    //////////////////
                    edt_bc1.getText().clear();
                    edt_bc1.clearFocus();
                    return;

                } else {
                    msg_error.setText("Slip No '" + req_no + "' not exist !!!");
                    lbl_SlipNo1_Tag = "";
                    edt_SOF.getText().clear();
                    edt_SOT.getText().clear();
                    edt_qty.getText().clear();
                    edt_bc1.getText().clear();
                    return;
                }
            }
            if (lbl_SlipNo1_Tag.equals("")) {
                msg_error.setText("Slip No '" + str_scan + "' not exist !!!");
                edt_bc1.getText().clear();
                edt_bc1.requestFocus();
                return;
            } else {
                boolean isExist = hp.isExistBarcode(str_scan, formID);// check barcode exist in data
                if (isExist)// exist data
                {
                    alertRingMedia();
                    msg_error.setText("Barcode exist in database!");
                    edt_bc1.getText().clear();
                    edt_bc1.requestFocus();
                    return;
                } else {
                    edt_bc2 = (EditText) rootView.findViewById(R.id.edit_Bc2);
                    edt_bc2.setText(str_scan);
                    if (str_scan.length() > 20) {
                        msg_error.setText("Barcode has length more 20 char !");
                        edt_bc2.getText().clear();
                        edt_bc1.getText().clear();
                        edt_bc1.requestFocus();
                        return;
                    }
                    if (OnSave()) {
                        msg_error.setText("Save success.");
                        edt_bc1.getText().clear();
                        edt_bc1.requestFocus();
                        OnShowScanLog();
                    }
                }
            }

        } catch (Exception ex) {

        }

    }

    //
    //region --------------ProcessMakeSlip-----------------
    public void onMakeSlip(View view) {
        //Check het Barcode send to server
        if (checkRecordGridView(R.id.grdScanLog)) {
            gwMActivity.alertToastLong(this.getResources().getString(R.string.tvAlertMakeSlip));
            return;
        }
        //Check have value make slip
        if (hp.isMakeSlip(formID) == false) return;

        String title = "Confirm Make Slip...";
        String mess = "Are you sure you want to Make Slip";
        alertDialogYN(title, mess, "onMakeSlip");
    }

    public void ProcessMakeSlip() {

        if (queue.size() > 0) {

            queue = new LinkedList();
            return;

        } else {
            unique_id = CDate.getDateyyyyMMddhhmmss();
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = " select  TR_ITEM_PK, TR_QTY, UOM, TR_LOT_NO, GD_SLIP_NO,TLG_SA_SALEORDER_D_PK,SUPPLIER_PK,TLG_GD_REQ_M_PK,TLG_GD_REQ_D_PK,TR_WH_IN_PK,TO_ITEM_PK,TO_UOM,TO_LOTNO,TO_UOM_QTY,PROD_D_PK,EXECKEY " +
                        " from " +
                        " ( select  TR_ITEM_PK, SUM(TR_QTY) as TR_QTY, TR_LOT_NO,UOM,GD_SLIP_NO,TLG_SA_SALEORDER_D_PK,SUPPLIER_PK,TLG_GD_REQ_M_PK,TLG_GD_REQ_D_PK,TR_WH_IN_PK,TO_ITEM_PK,TO_UOM,TO_LOTNO,TO_UOM_QTY,PROD_D_PK,EXECKEY " +
                        " FROM INV_TR  WHERE DEL_IF = 0  AND SENT_YN = 'Y' AND TR_TYPE ='" + formID + "' AND STATUS='000' and SLIP_NO IN ('-','') " +
                        " GROUP BY TR_ITEM_PK, UOM, TR_LOT_NO, GD_SLIP_NO, TLG_SA_SALEORDER_D_PK, SUPPLIER_PK, TLG_GD_REQ_M_PK,TLG_GD_REQ_D_PK,TR_WH_IN_PK,TO_ITEM_PK,TO_UOM,TO_LOTNO,TO_UOM_QTY,PROD_D_PK, EXECKEY )";
                cursor = db.rawQuery(sql, null);

                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }


                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_req_m_pk = cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK"));
                            String ex_from_order_d_pk = cursor.getString(cursor.getColumnIndex("TLG_SA_SALEORDER_D_PK"));
                            String ex_to_saleorder_d_pk = cursor.getString(cursor.getColumnIndex("SUPPLIER_PK"));
                            String ex_req_d_pk = cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_D_PK"));

                            hp.updateExeckeyExchangeProd(formID, ex_item_pk, ex_from_order_d_pk, ex_to_saleorder_d_pk, ex_req_d_pk, ex_req_m_pk, unique_id);

                            para += "|" + unique_id;
                        } else {
                            para += "|" + execkey;
                        }
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";


                    } while (cursor.moveToNext());
                }
                para += "|!LG_MPOS_PROD_EXCHANGE_MSLIP";
                //onPopAlert(para);
                System.out.print("\n\n ******para make slip income prod: " + para);
                data[j++] = para;
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
                Log.e("Make slip", ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                task.execute(data);
            } else {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        }
    }
    //endregion

    public void OnShowGW_Header()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLineName), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Pk", 0, fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);


        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTotal), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvUom), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvRequestNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Request PK", 0, fixedHeaderHeight));
        scrollablePart.addView(row);

    }

    //////////////////////////////////////////////////////////////////////////
    public List<String> GetDataOnServer(String type, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";
            if (type.endsWith("Exchange")) //WH
            {
                l_para = "1,LG_MPOS_GET_REQ_EXCHANGE_PR," + para;
            }

            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                if (type.endsWith("Exchange")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                }
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(type + ": " + ex.getMessage());
        }
        return labels;
    }

    ////////////////////////////////////////////////////////////////////////////
    public boolean OnSave() {
        boolean kq = false;
        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        try {

            scan_date = CDate.getDateyyyyMMdd();
            scan_time = CDate.getDateYMDHHmmss();

            // get data input control to var then insert db
            P_SLIP_NO = edt_reqNo.getText().toString();
            P_ITEM_BC = edt_bc2.getText().toString();

            db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,GD_SLIP_NO,TLG_SA_SALEORDER_D_PK,SUPPLIER_PK,TLG_GD_REQ_M_PK,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                    + "VALUES('"
                    + P_ITEM_BC + "',"
                    + wh_pk + ",'"
                    + P_SLIP_NO + "','"
                    + SOF_Pk + "','"
                    + SOT_Pk + "',"
                    + req_pk + ",'"
                    + scan_date + "','"
                    + scan_time + "','"
                    + "N" + "','"
                    + " " + "','"
                    + formID + "');");

            db.close();
            kq = true;
        } catch (Exception ex) {
            msg_error.setText("Save Error!!!");
            kq = false;
        }
        return kq;
    }

    // show data scan log
    public void OnShowScanLog() {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
            int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' order by PK desc LIMIT 20", null);// TLG_LABEL

            int count = cursor.getCount();
            //Log.e("count log",String.valueOf(count));
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));


                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            // total scan log
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            txtRem = (TextView) rootView.findViewById(R.id.txtRemain);
            txtRem.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
            //Log.e("Grid Scan log Error -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void onClickViewStt(View view){

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event

                if (_type.equals("DelAll")) {
                    ProcessDeleteAll();
                }
                if (_type.equals("onMakeSlip")) {

                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void onDelAll(View view) {
        if (!hp.isCheckDelete(formID)) return;

        String title = "Confirm Delete..";
        String mess = "Are you sure you want to delete all";
        alertDialogYN(title, mess, "DelAll");
    }

    public void ProcessDeleteAll() {
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
        String para = "";
        boolean flag = true;
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "' and SLIP_NO IN ('-','') and STATUS ='000' AND SCAN_DATE > '" + date_previous + "'", null);
            int count = cursor.getCount();

            int j = 0;
            if (cursor.moveToFirst()) {
                data = new String[1];

                do {
                    flag = true;
                    for (int k = 0; k < cursor.getColumnCount(); k++) {
                        if (para.length() <= 0) {
                            if (cursor.getString(k) != null)
                                para += cursor.getString(k);
                            else
                                para += "|";
                        } else {
                            if (flag == true) {
                                if (cursor.getString(k) != null) {
                                    para += cursor.getString(k);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor.getString(k) != null)
                                    para += "|" + cursor.getString(k);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "|" + formID;
                    para += "|delete";
                    para += "*|*";
                } while (cursor.moveToNext());

                para += "|!LG_MPOS_UPD_INV_TR_DEL";
                data[j] = para;
                //System.out.print("\n\n\n para update||delete: " + para);
                Log.e("para update||delete: ", para);
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        } else {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }

    }

    public void onDeleteAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();

            } else {
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='" + formID + "';");
            }
        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            OnShowScanLog();
            OnShowScanIn();
            OnShowScanAccept();
        }
    }

    // show data scan in
    public void OnShowScanIn() {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30,40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select (select COUNT(0)" +
                    "from INV_TR t1 " +
                    "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE = '" + scan_date + "' AND T1.STATUS NOT IN('000', ' ')" +
                    ") AS SEQ, T2.PK, T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.TR_LOT_NO, T2.INCOME_DATE, T2.CHARGER, T2.TR_WH_IN_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK " +
                    " FROM INV_TR t2 " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SCAN_DATE = '" + scan_date + "' AND T2.STATUS NOT IN('000', ' ')  " +
                    " GROUP BY T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.TR_LOT_NO, T2.INCOME_DATE, T2.CHARGER, T2.TR_WH_IN_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK" +
                    " ORDER BY pk desc LIMIT 10 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, 0));


                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND TR_TYPE = '" + formID + "' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
            txtTotalGridMid.setText("Total: " + count + " ");
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
            //Log.e("OnShowScanIn Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan accept
    public void OnShowScanAccept() {
        try {
            int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            sql = "   SELECT ITEM_NAME, ITEM_CODE, COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, UOM, TR_ITEM_PK, TLG_GD_REQ_M_PK,SLIP_NO, GD_SLIP_NO,TLG_SA_SALEORDER_D_PK,PO_NO,EXECKEY " +
                    "   FROM INV_TR " +
                    "    WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "'  AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND STATUS='000' " +
                    "    GROUP BY ITEM_NAME, ITEM_CODE, UOM, TR_ITEM_PK, TLG_GD_REQ_M_PK, SLIP_NO, GD_SLIP_NO, TLG_SA_SALEORDER_D_PK,PO_NO,EXECKEY" +
                    "    ORDER BY EXECKEY desc  LIMIT 30 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            float _qty = 0;
            queue.clear();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TOTAL")), scrollableColumnWidths[1], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UOM")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("GD_SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK")), 0, fixedRowHeight, 0));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            if (flagPopgrd3) {
//                                flagPopgrd3 = false;
                                onShowClickGridBot(v);
//                                duplicate = false;
//                                TableRow tr1 = (TableRow) v;
//
//                                TextView tvItemCode = (TextView) tr1.getChildAt(1); //Item Code
//                                String ITEM_CODE = tvItemCode.getText().toString();
//
//                                TextView tvSlipNo = (TextView) tr1.getChildAt(5); //Slip No
//                                String SLIPNO = tvSlipNo.getText().toString();
//
//                                TextView requestPk = (TextView) tr1.getChildAt(7); //TR_ITEM_PK
//                                String TR_ITEM_PK = requestPk.getText().toString();
//
//                                TextView tvPK = (TextView) tr1.getChildAt(8); //TLG_GD_REQ_M_PK
//                                String TLG_GD_REQ_M_PK = tvPK.getText().toString();


//                                Intent openNewActivity = new Intent(v.getContext(), gw.genumobile.com.GridListViewOnePK.class);
//                                //send data into Activity
//                                openNewActivity.putExtra("user", user);
//                                openNewActivity.putExtra("user_id", user_id);
//                                openNewActivity.putExtra("form_id", formID);
//
//                                openNewActivity.putExtra("ITEM_CODE", ITEM_CODE);
//                                openNewActivity.putExtra("SLIP_NO", SLIPNO);
//                                openNewActivity.putExtra("TR_ITEM_PK", TR_ITEM_PK);
//                                openNewActivity.putExtra("TLG_GD_REQ_M_PK", TLG_GD_REQ_M_PK);
//                                startActivity(openNewActivity);
//                                startActivityForResult(openNewActivity, REQUEST_CODE);
                                //end Giang
                            }

                        }
                    });


                    row.setOnLongClickListener(new View.OnLongClickListener() {
                                                   TextView tv11;
                                                   boolean duplicate = false;

                                                   @Override
                                                   public boolean onLongClick(View v) {
                                                       duplicate = false;
                                                       TableRow tr1 = (TableRow) v;
                                                       TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
                                                       String st_slipNO = tvSlipNo.getText().toString();

                                                       if (st_slipNO.equals("-")) {
                                                           TextView tvPK = (TextView) tr1.getChildAt(8); //TLG_GD_REQ_M_PK
                                                           String TLG_GD_REQ_M_PK = tvPK.getText().toString();

                                                           int xx = tr1.getChildCount();
                                                           for (int i = 0; i < xx; i++) {
                                                               tv11 = (TextView) tr1.getChildAt(i);
                                                               tv11.setTextColor(Color.rgb(255, 99, 71));
                                                           }

                                                           if (queue.size() > 0) {
                                                               if (queue.contains(TLG_GD_REQ_M_PK)) {
                                                                   queue.remove(TLG_GD_REQ_M_PK);

                                                                   for (int i = 0; i < xx; i++) {
                                                                       tv11 = (TextView) tr1.getChildAt(i);
                                                                       tv11.setTextColor(Color.BLACK);
                                                                   }

                                                                   duplicate = true;
                                                               }
                                                           }

                                                           if (!duplicate)
                                                               queue.add(TLG_GD_REQ_M_PK);
                                                       }
                                                       gwMActivity.alertToastLong(queue.size() + "");
                                                       return true;
                                                   }
                                               }
                    );
                    scrollablePart.addView(row);
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }

            txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            txtTotalGridBot.setText("Total: " + count + " ");
            txtTotalGridBot.setTextColor(Color.BLUE);
            txtTotalGridBot.setTextSize((float) 16.0);

            txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            txtTotalQtyBot.setTextColor(Color.BLACK);
            txtTotalQtyBot.setTextSize((float) 16.0);

            int countRequest = hp.CountRequest(formID);
            txtCountRequest = (TextView) rootView.findViewById(R.id.txtCountRequest);
            txtCountRequest.setText("Req: " + countRequest);
            txtCountRequest.setTextColor(Color.BLACK);
            txtCountRequest.setTextSize((float) 15.0);
            if (req_pk.equals("") || req_pk == "")
                req_pk = "0";
            Log.e("reqPK", req_pk);
            float qtyRequest = hp.QtyRequest(formID, Integer.valueOf(req_pk));
            txtQtyRequest = (TextView) rootView.findViewById(R.id.txtQtyRequest);
            txtQtyRequest.setText("RQty: " + String.format("%.02f", qtyRequest) + " ");
            txtQtyRequest.setTextColor(Color.BLACK);
            txtQtyRequest.setTextSize((float) 15.0);

        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanAccept: " + ex.getMessage());
            Log.e("Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public  void onShowClickGridBot(View v){
        boolean duplicate = false;

        TableRow tr1 = (TableRow) v;
        TextView tvItemCode = (TextView) tr1.getChildAt(1); //Item Code
        String ITEM_CODE = tvItemCode.getText().toString();

        TextView tvSlipNo = (TextView) tr1.getChildAt(5); //Slip No
        String SLIPNO = tvSlipNo.getText().toString();

        TextView requestPk = (TextView) tr1.getChildAt(7); //TR_ITEM_PK
        String TR_ITEM_PK = requestPk.getText().toString();

        TextView tvPK = (TextView) tr1.getChildAt(8); //TLG_GD_REQ_M_PK
        String TLG_GD_REQ_M_PK = tvPK.getText().toString();
//        TextView tvLotNo = (TextView) tr1.getChildAt(4); //LOT_NO
//        String st_LotNO = tvLotNo.getText().toString();
//        TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP NO
//        String st_SlipNO = tvSlipNo.getText().toString();
//
//        TextView tvItemPK = (TextView) tr1.getChildAt(10); //TR_ITEM_PK
//        String item_pk = tvItemPK.getText().toString();
//        TextView tvWhPk = (TextView) tr1.getChildAt(12); //WH_PK
//        String whPK = tvWhPk.getText().toString();

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new GridListViewOnePK();
        Bundle args = new Bundle();

        args.putString("form_id", formID);
        args.putString("ITEM_CODE", ITEM_CODE);
        args.putString("SLIP_NO",SLIPNO);
        args.putString("TR_ITEM_PK",TR_ITEM_PK);
        args.putString("TLG_GD_REQ_M_PK",TLG_GD_REQ_M_PK);
        dialogFragment.setArguments(args);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    public void CountSendRecord() {
        flagUpload = true;
        // total scan log
        txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid = Integer.parseInt(txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        int countBot = Integer.parseInt(txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));
        txtSentLog = (TextView) rootView.findViewById(R.id.txtSent);
        txtSentLog.setText("Send: " + (countMid + countBot));

        txtRem = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(txtRem.getText().toString().replaceAll("[\\D]", ""));

        txtTT = (TextView) rootView.findViewById(R.id.txtTotal);
        txtTT.setText("TT: " + (countMid + countBot + countRe));


    }

    public void CountSendFirst() {
        // total scan log
        txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        int countMid = Integer.parseInt(txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countBot = Integer.parseInt(txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));

        txtSentLog = (TextView) rootView.findViewById(R.id.txtSent);
        txtSentLog.setText("Send: " + (countMid + countBot));

        txtRem = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(txtRem.getText().toString().replaceAll("[\\D]", ""));

        txtTT = (TextView) rootView.findViewById(R.id.txtTotal);
        txtTT.setText("TT: " + (countMid + countBot + countRe));

    }

    ///////////////////////////////////////////////
    public void onListGrid(View view) {
        if (!checkRecordGridView(R.id.grdScanIn)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  frGridListViewMid.newInstance(1,formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    ///////////////////////////////////////////////
    public void onListGridBot(View view) {

        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new GridListViewBot();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    /////////////////////////////////////////////////////
    public void onClickInquiry(View view){

        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        startActivity(openNewActivity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiểm tra có đúng requestCode =REQUEST_CODE_INPUT hay không
        //Vì ta có thể mở Activity với những RequestCode khác nhau
        if(requestCode==REQUEST_CODE)
        {
            //Kiểm trả ResultCode trả về, cái này ở bên InputDataActivity truyền về

            OnShowScanAccept();
            switch(resultCode)
            {
                case RESULT_CODE:

                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnViewStt:
                onClickViewStt(view);

                break;
            case R.id.btnInquiry:
                onClickInquiry(view);

                break;
            case R.id.btnList:
                onListGrid(view);

                break;
            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);

                break;
            case R.id.btnApprove:

                break;

            case R.id.btnSelectMid:
                //onClickSelectMid(view);

                break;
            default:
                break;
        }
    }
}
