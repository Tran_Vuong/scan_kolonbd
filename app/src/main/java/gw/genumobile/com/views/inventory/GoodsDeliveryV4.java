package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import gw.genumobile.com.R;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.GridListViewOnePK;
import gw.genumobile.com.interfaces.PopupInquiry;
import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.views.gwFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GoodsDeliveryV4.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GoodsDeliveryV4#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GoodsDeliveryV4 extends gwFragment implements View.OnClickListener {

    public static final int REQUEST_CODE = 1;
    String sql = "", tr_date = "", scan_date = "", scan_time = "", str_reqNo = "", str_reqType = "", unique_id = "", user = "";//, wh_pk = "", wh_name = "";
    EditText edt_BC;
    TextView txt_reqNo, txt_reqBal, txt_reqQty, txt_scanned, txt_BCScan, txt_error, _txtTime;
    TextView _txtDate, _txtTT, _txtSent, _txtRemain, _txtTotalGridBot, _txtTotalQtyBot, _txtTotalGridMid;
    int timeUpload = 0;
    private Handler customHandler = new Handler();
    Button btnDelAll, btnMakeSlip, btnViewStt, btnList, btnInquiry, btnListBot;
    protected Boolean flagUpload = true;
    View rootView;
    List<JDataTable> jDatatable;
    Calendar c;
    SimpleDateFormat df;
    SQLiteDatabase db = null;
    public SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    String data[][] = new String[0][0];
    String data1[] = new String[0];
    double req_qty = 0.0, req_bal = 0.0, req_scanned = 0.0;

    public GoodsDeliveryV4() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_goods_delivery_v4, container, false);
        edt_BC = (EditText) rootView.findViewById(R.id.editBC);
        txt_error = (TextView) rootView.findViewById(R.id.txtError);
        txt_reqNo = (TextView) rootView.findViewById(R.id.txt_reqNo);
        txt_reqBal = (TextView) rootView.findViewById(R.id.txt_reqBal);
        txt_reqQty = (TextView) rootView.findViewById(R.id.txt_reqQty);
        txt_scanned = (TextView) rootView.findViewById(R.id.txtAccumScan);
        txt_BCScan = (TextView) rootView.findViewById(R.id.txtBCScan);
        _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);

        
        txt_error.setTextColor(Color.RED);
        txt_error.setTextSize((float) 16.0);
        txt_error.setText("");

        _txtDate = (TextView) rootView.findViewById(R.id.txtDate);

        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        _txtDate.setText(formattedDate);
        _txtTime = (TextView) rootView.findViewById(R.id.txtTime);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);

        edt_BC.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        txt_error.setText("");
                        if (edt_BC.getText().toString().equals("")) {
                            txt_error.setText("Pls scan barcode!");
                        } else {
                            OnSaveBC();
                        }
                    }
                    return true;
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // this is for backspace
                    edt_BC.clearFocus();
                    Thread.interrupted();
                }
                return false;
            }

        });

        OnShowGridHeader();
        init_color();
        OnShowGridDetail();
        OnShowScanIn();
        OnShowScanLog();

        timeUpload = hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);
        return rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run() {
            if (flagUpload)
                doStart();
                SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload * 1000);
        }
    };

    private void init_color() {
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
    }

    public void OnSaveBC() {
        if (edt_BC.getText().toString().equals("")) {
            txt_error.setTextColor(Color.RED);
            txt_error.setText("Pls Scan barcode!");
            edt_BC.getText().clear();
            edt_BC.requestFocus();
            return;
        } else {

            String str_scanBC = edt_BC.getText().toString().toUpperCase();

            try {

                if (str_reqNo.equals("") && str_scanBC.indexOf("S") == 0) {
                    String slip_no = "", out_name = "", in_name = "";
                    String req_no = str_scanBC.substring(1, str_scanBC.length());
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                    String check_reqNo = "SELECT PK,SLIP_NO,TR_WH_IN_NAME FROM INV_TR where  tr_type='" + formID + "' AND (status='000' or status=' ')  and REQ_NO ='" + req_no + "' and ITEM_BC is null";
                    Cursor cursor = db.rawQuery(check_reqNo, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            if (!cursor.getString(cursor.getColumnIndex("SLIP_NO")).equals("-") && !cursor.getString(cursor.getColumnIndex("SLIP_NO")).equals(" ")) {
                                slip_no = cursor.getString(cursor.getColumnIndex("SLIP_NO"));
                                in_name = cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME"));
                            }

                        } while (cursor.moveToNext());
                        str_reqNo = req_no;
                        txt_reqNo.setText(str_reqNo);
                        scan_date = CDate.getDateyyyyMMdd();
                        OnShowGridDetail();
                        OnShowScanIn();
                        OnShowScanLog();
                        gwMActivity.alertToastShort("ReqNo is exist!!!");
                        db.close();
                    } else {

                        String para = "1,LG_MPOS_GET_GOODS_DELI_REQ,1," + req_no;
                        gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                        JResultData jrd = networkThread.execute(para).get();
                        jDatatable = jrd.getListODataTable();
                        //jDatatable =  this.getDataJson(gwActivity, para);
                        JDataTable dt = jDatatable.get(0);
                        if (dt.totalrows > 0) {
                            /*for (int j = 0; j < dt.totalrows; j++) {
                                if (!dt.records.get(j).get("slip_no").toString().equals(""))
                                    slip_no = dt.records.get(j).get("slip_no").toString();
                            }
                            if (!slip_no.equals("")) {
                                gwMActivity.alertToastShort("Request was make slip!!!");
                                edt_BC.getText().clear();
                                edt_BC.requestFocus();
                                return;
                            }*/

                            for (int i = 0; i < dt.totalrows; i++) {
                                try {

                                    req_qty = req_qty + Double.parseDouble(dt.records.get(i).get("req_qty").toString());
                                    req_scanned = req_scanned + Double.parseDouble(dt.records.get(i).get("scanned_qty").toString());
                                    scan_date = CDate.getDateyyyyMMdd();
                                    scan_time = CDate.getDateYMDHHmmss();
                                    db.execSQL("INSERT INTO INV_TR(TLG_GD_REQ_M_PK, TLG_GD_REQ_D_PK, REQ_NO, TR_ITEM_PK, ITEM_CODE, ITEM_NAME," +
                                            " CUST_PK, CUST_NAME, REQ_QTY, SCANNED, REQ_BAL, UOM, SLIP_NO, TR_WH_OUT_PK, TR_WH_OUT_NAME,"
                                            + " SCAN_DATE, SCAN_TIME, SENT_YN, STATUS, TR_TYPE) "
                                            + " VALUES('"
                                            + dt.records.get(i).get("req_m_pk").toString() + "','"
                                            + dt.records.get(i).get("req_d_pk").toString() + "','"
                                            + dt.records.get(i).get("slip_no").toString() + "',"
                                            + dt.records.get(i).get("item_pk").toString() + ",'"
                                            + dt.records.get(i).get("item_code").toString() + "','"
                                            + dt.records.get(i).get("item_name").toString() + "','"
                                            + dt.records.get(i).get("cust_pk").toString() + "','"
                                            + dt.records.get(i).get("cust_name").toString() + "','"
                                            + dt.records.get(i).get("req_qty").toString() + "','"
                                            + dt.records.get(i).get("scanned_qty").toString() + "','"
                                            + dt.records.get(i).get("req_bal").toString() + "','"
                                            + dt.records.get(i).get("uom").toString() + "','','"
                                            + dt.records.get(i).get("wh_pk").toString() + "','"
                                            + dt.records.get(i).get("wh_name").toString() + "','"
                                            + scan_date + "','"
                                            + scan_time + "','"
                                            + "N" + "','"
                                            + " " + "','"
                                            + formID + "');");
                                } catch (Exception ex) {
                                    txt_error.setText("Save Error!!!" + ex.toString());
                                }
                            }
                            str_reqNo = dt.records.get(0).get("slip_no").toString();
                            str_reqType = dt.records.get(0).get("slip_type").toString();
                            txt_reqQty.setText(String.valueOf(req_qty));
                            txt_scanned.setText(String.valueOf(req_scanned));

                            db.close();
                            OnShowGridDetail();
                            txt_reqNo.setText(req_no);
                        } else {
                            txt_error.setText("Req No '" + req_no + "' not exist !!!");
                            str_reqNo = "";
                            str_reqType="";
                            return;
                        }
                    }
                    edt_BC.getText().clear();
                    edt_BC.requestFocus();
                    return;
                }
                if (str_reqNo.equals("") || str_reqNo == "") {
                    alertRingMedia2();
                    txt_error.setText("Please scan request first!");
                    edt_BC.getText().clear();
                    edt_BC.requestFocus();
                    return;
                }
                boolean isExists = hp.isExistBarcode(str_scanBC, formID);// check barcode exist in data
                if (str_scanBC.length() > 20) {
                    txt_error.setText("Barcode has length more 20 char !");
                    edt_BC.getText().clear();
                    edt_BC.requestFocus();
                    return;
                }
                if (isExists)// exist data
                {
                    alertRingMedia();
                    txt_error.setText("Barcode exist in database!");
                    edt_BC.getText().clear();
                    edt_BC.requestFocus();
                    return;
                } else {
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                    scan_date = CDate.getDateyyyyMMdd();
                    scan_time = CDate.getDateYMDHHmmss();

                    db.execSQL("INSERT INTO INV_TR(ITEM_BC,REQ_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                            + "VALUES('"
                            + str_scanBC + "','"
                            + str_reqNo + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "N" + "','"
                            + " " + "','"
                            + formID + "');");
                    db.close();

                }
            } catch (Exception ex) {
                txt_error.setText("Save Error!!!");
                Log.e("OnSaveBC", ex.getMessage());
            } finally {

                edt_BC.getText().clear();
                edt_BC.requestFocus();
            }
        }
        OnShowScanLog();
        CountSendRecord();
    }

    private void doStart() {
        if (str_reqNo.equals(""))
            return;
        flagUpload = false;
        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        cursor2 = db2.rawQuery("select PK, ITEM_BC  from INV_TR where DEL_IF=0 and TR_TYPE='" + formID + "' and sent_yn = 'N' and ITEM_BC is not null order by PK asc LIMIT 10", null);

        if (cursor2.moveToFirst()) {

            // Write your code here to invoke YES event
            System.out.print("\n\n**********doStart********\n\n\n");

            //set value message
            txt_error = (TextView) rootView.findViewById(R.id.txtError);
            txt_error.setTextColor(Color.RED);
            txt_error.setText("");

            //  data=new String [cursor2.getCount()];
            data1 = new String[1];
            int j = 0;
            String para = "";
            boolean flag = false;
            do {
                flag = true;
                for (int i = 0; i < cursor2.getColumnCount(); i++) {
                    if (para.length() <= 0) {
                        if (cursor2.getString(i) != null)
                            para += cursor2.getString(i) + "|";
                        else
                            para += "|";
                    } else {

                        if (flag == true) {
                            if (cursor2.getString(i) != null) {
                                para += cursor2.getString(i);
                                flag = false;
                            } else {
                                para += "|";
                                flag = false;
                            }
                        } else {
                            if (cursor2.getString(i) != null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }
                }
                para += "|" + str_reqNo;
                para += "|" + str_reqType;

                para += "|" + deviceID;
                para += "|" + bsUserID;
                para += "*|*";
            } while (cursor2.moveToNext());
            //////////////////////////
            para += "|!" + "LG_MPOS_UPLOAD_GD_V4";
            data1[j++] = para;
            System.out.print("\n\n\n para upload: " + para);
            Log.e("para upload: ", para);

            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity, this);
                task.execute(data1);
                gwMActivity.alertToastShort("Send to Server");
            } else {
                flagUpload = true;
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        } else {
            flagUpload = true;
        }
        cursor2.close();
        db2.close();
    }

    public void onMakeSlip(View view) {
        //Check het Barcode send to server
        if (checkRecordGridView(R.id.grdScanLog)) {
            gwMActivity.alertToastLong(this.getResources().getString(R.string.tvAlertMakeSlip));
            return;
        }
        //Check have value make slip
        if (hp.isMakeSlip(formID) == false) return;

        String title = "Confirm Make Slip...";
        String mess = "Are you sure you want to Make Slip";
        alertDialogYN(title, mess, "onMakeSlip");
    }

    public void ProcessMakeSlip() {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = " SELECT TR_ITEM_PK, TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,TR_QTY, UOM,TR_LOT_NO,PO_NO,TLG_IN_WHLOC_IN_PK,GRADE,TLG_GD_REQ_D_PK,REQ_NO,EXECKEY " +
                    " FROM " + "( SELECT TR_ITEM_PK,  SUM(TR_QTY) TR_QTY, UOM,  TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,TR_LOT_NO,PO_NO,TLG_IN_WHLOC_IN_PK,GRADE,TLG_GD_REQ_D_PK,REQ_NO,EXECKEY " +
                    " FROM INV_TR " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "'  AND SCAN_DATE = " + scan_date + " AND ITEM_BC IS NOT NULL AND REQ_NO='" + str_reqNo + "' AND SENT_YN = 'Y' AND STATUS='000' " +
                    " AND SLIP_NO IN('-','') " +
                    " GROUP BY   TR_ITEM_PK, UOM, TLG_GD_REQ_M_PK, TLG_SA_SALEORDER_D_PK, TR_LOT_NO,PO_NO,TLG_IN_WHLOC_IN_PK,GRADE,TLG_GD_REQ_D_PK,REQ_NO, EXECKEY" + " )";
//            sql = "  select TLG_GD_REQ_M_PK, TLG_GD_REQ_D_PK, TR_ITEM_PK,SUM(TR_QTY) as QTY, UOM, TR_LOT_NO , TLG_PO_PO_D_PK, SUPPLIER_PK, TR_WH_IN_PK, TLG_IN_WHLOC_IN_PK, UNIT_PRICE, PO_NO, REQ_NO, SO_NO,EXECKEY " +
//                    "  FROM INV_TR  WHERE DEL_IF = 0 AND STATUS='000' AND SCAN_DATE = '" + scan_date + "' AND ITEM_BC IS NOT NULL AND TR_TYPE = '" + formID + "' AND REQ_NO='" + str_reqNo + "' and SLIP_NO IN('-','',null) " +
//                    "  GROUP BY TLG_GD_REQ_M_PK,TLG_GD_REQ_D_PK, TR_ITEM_PK, UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, TR_WH_IN_PK, TLG_IN_WHLOC_IN_PK, UNIT_PRICE, PO_NO, REQ_NO,SO_NO, EXECKEY  ";
            cursor = db.rawQuery(sql, null);

            int count = cursor.getCount();
            data1 = new String[1];
            int j = 0;
            String para = "";
            boolean flag = false;
            if (cursor.moveToFirst()) {
                do {
                    flag = true;
                    for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                        if (para.length() <= 0) {
                            if (cursor.getString(k) != null)
                                para += cursor.getString(k) + "|";
                            else
                                para += "0|";
                        } else {
                            if (flag == true) {
                                if (cursor.getString(k) != null) {
                                    para += cursor.getString(k);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor.getString(k) != null)
                                    para += "|" + cursor.getString(k);
                                else
                                    para += "|";
                            }
                        }
                    }
                    String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                    if (execkey == null || execkey.equals("")) {
                        int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                        String ex_req_m_pk = cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK"));
                        String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                        String ex_sale_order_d_pk = cursor.getString(cursor.getColumnIndex("TLG_SA_SALEORDER_D_PK"));
                        hp.updateExeckeyGoodsDeli(formID, ex_item_pk, ex_req_m_pk, ex_sale_order_d_pk, ex_lot_no, unique_id);

                        para += "|" + unique_id;
                    } else {
                        para += "|" + execkey;
                    }
                    para += "|" + bsUserID;
                    para += "|" + bsUserPK;
                    para += "|" + deviceID;
                    para += "*|*";
                } while (cursor.moveToNext());

                para += "|!LG_MPOS_PRO_GD_MAKESLIP_V4";
                System.out.print("\n\n ******para make slip in stock: " + para);
                //data[j++] = para;
                data1[j] = para;
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        // asyns server
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity, this);
            task.execute(data1);
        } else {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }

    }

    public void OnShowScanLog() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' and REQ_NO = '" + str_reqNo + "' AND ITEM_BC IS NOT NULL order by PK desc LIMIT 20", null);// TLG_LABEL

            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and item_bc is not null and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
            _txtRemain.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowScanIn() {
        try {
            int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE, STATUS, TR_LOT_NO, SLIP_NO, INCOME_DATE, CHARGER, TR_WH_OUT_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SENT_YN='Y' AND SCAN_DATE > '" + date_previous + "' AND STATUS NOT IN('000', ' ') and REQ_NO = '" + str_reqNo + "' AND ITEM_BC IS NOT NULL  " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[0], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > '" + date_previous + "' AND TR_TYPE = '" + formID + "' AND SENT_YN = 'Y' AND ITEM_BC IS NOT NULL AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
            _txtTotalGridMid.setText("Total: " + count);
            _txtTotalGridMid.setTextColor(Color.BLUE);
            _txtTotalGridMid.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowGridDetail() {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        scrollablePart.removeAllViews();

        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

        sql = "   SELECT  ITEM_CODE,UOM,REQ_QTY,SCANNED,REQ_BAL,TR_LOT_NO,SLIP_NO,ITEM_NAME,TR_WH_OUT_NAME,REMARKS,TR_ITEM_PK,TR_WH_OUT_PK,TLG_GD_REQ_M_PK,TLG_GD_REQ_D_PK," +
                " (SELECT SUM(TR_QTY) FROM INV_TR B WHERE ITEM_BC IS NOT NULL AND REQ_NO = '" + str_reqNo + "' AND B.TR_ITEM_PK = A.TR_ITEM_PK AND STATUS = '000' AND tr_type='" + formID + "') CURRENT_SCAN," +
                " (SELECT COUNT(*) FROM INV_TR B WHERE ITEM_BC IS NOT NULL AND REQ_NO = '" + str_reqNo + "' AND B.TR_ITEM_PK = A.TR_ITEM_PK AND STATUS = '000' AND tr_type='" + formID + "') BC_NUM" +
                "   FROM INV_TR A" +
                "    WHERE tr_type='" + formID + "' AND (status='000' or status=' ')  and REQ_NO ='" + str_reqNo + "' and ITEM_BC is null";

        cursor = db.rawQuery(sql, null);
        int count = cursor.getCount();

        double req_qty = 0;
        float req_bal = 0, scanned = 0;

        if (cursor != null && cursor.moveToFirst()) {
            for (int i = 0; i < count; i++) {
                req_qty += Double.parseDouble(cursor.getString(cursor.getColumnIndex("REQ_QTY")));
                req_bal = req_bal + Float.parseFloat(cursor.getString(cursor.getColumnIndex("REQ_BAL")).toString());
                scanned = scanned + Float.parseFloat(cursor.getString(cursor.getColumnIndex("SCANNED")).toString());

                row = new TableRow(gwMActivity);
                row.setLayoutParams(wrapWrapTableRowParams);
                row.setGravity(Gravity.CENTER);
                row.setBackgroundColor(Color.LTGRAY);
                row.addView(makeTableColWithText(String.valueOf(i + 1), scrollableColumnWidths[1], fixedRowHeight, 0));
//                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UOM")), scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REQ_QTY")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCANNED")), scrollableColumnWidths[3], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REQ_BAL")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CURRENT_SCAN")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("BC_NUM")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")), scrollableColumnWidths[3], fixedRowHeight, -1));
//                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_OUT_NAME")), scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[3], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK")),scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_D_PK")),scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK")), 0, fixedRowHeight, -1));
//                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_OUT_PK")),0, fixedRowHeight, -1));
//                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")), 0, fixedRowHeight, -1));
//                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_PK")), 0, fixedRowHeight, -1));

                row.setOnClickListener(new View.OnClickListener() {
                    TextView tv11;
                    boolean duplicate = false;

                    @Override
                    public void onClick(View v) {
                        OnClickGridDetailBC(v);
                    }
                });

                row.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return false;
                    }
                });

                scrollablePart.addView(row);
                cursor.moveToNext();
            }
        }
        txt_reqQty.setText(String.valueOf(req_qty));
        txt_reqBal.setText(String.valueOf(req_bal));
        txt_scanned.setText(String.valueOf(scanned));
        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND STATUS='000' and REQ_NO ='" + str_reqNo + "' and ITEM_BC is NOT NULL";
        cursor = db.rawQuery(sql, null);
        count = cursor.getCount();
        float _qty=0;
        if (cursor != null && cursor.moveToFirst()) {
            for (int i = 0; i < count; i++) {
                _qty=_qty+ Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                cursor.moveToNext();
            }
        }
        _txtTotalGridBot=(TextView) rootView.findViewById(R.id.txtTotalBot);
        _txtTotalGridBot.setText("Total: " + count+ " ");
        _txtTotalGridBot.setTextColor(Color.BLUE);
        _txtTotalGridBot.setTextSize((float) 16.0);

        txt_BCScan.setText(String.valueOf(count));
    }

    public void OnClickGridDetailBC(View v) {
        boolean duplicate = false;
        TableRow tr1 = (TableRow) v;

        TextView tvSlipNo = (TextView) tr1.getChildAt(9); //SLIP NO
        String st_SlipNO = tvSlipNo.getText().toString();

        TextView tvItemPK = (TextView) tr1.getChildAt(15); //TR_ITEM_PK
        String item_pk = tvItemPK.getText().toString();
        TextView tvReq_M_Pk = (TextView) tr1.getChildAt(13);
        String req_M_PK = tvReq_M_Pk.getText().toString();
        TextView tvReq_D_Pk = (TextView) tr1.getChildAt(14);
        String req_D_PK = tvReq_D_Pk.getText().toString();

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = new GridListViewOnePK();
        Bundle args = new Bundle();

        args.putString("form_id", formID);
        args.putString("ITEM_CODE", "");
        args.putString("SLIP_NO", st_SlipNO);
        args.putString("TR_ITEM_PK", item_pk);
        args.putString("TLG_GD_REQ_M_PK", req_M_PK);
        args.putString("TLG_GD_REQ_D_PK", req_D_PK);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");

    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhOut), scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
//        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemName), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvUom), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvReqQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvScanned), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvBalance), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvCurrentScan), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvBCNum), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvMakeSlip), scrollableColumnWidths[4], fixedHeaderHeight));
//        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvRefNo), scrollableColumnWidths[3], fixedHeaderHeight));
//        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvPartListNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhOut), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvRemarks), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("TLG_GD_REQ_M_PK", scrollableColumnWidths[2], fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TLG_GD_REQ_D_PK", scrollableColumnWidths[2], fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TR_ITEM_PK", 0, fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TR_WH_OUT_PK", 0, fixedRowHeight));
        scrollablePart.addView(row);
    }

    public void CountSendRecord() {
        flagUpload = true;
        // total scan log
        _txtSent = (TextView) rootView.findViewById(R.id.txtSent);
        _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid = Integer.parseInt(_txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        int countBot = Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Send: " + (countMid + countBot));

        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]", ""));

        _txtTT = (TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: " + (countMid + countBot + countRe));

    }

    //region ------------------Delete-----------------------
    public void onDelAll(View view) {

        if (!hp.isCheckDelete(formID)) return;

        String title = "Confirm Delete..";
        String mess = "Are you sure you want to delete all";
        alertDialogYN(title, mess, "onDelAll");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiểm tra có đúng requestCode =REQUEST_CODE_INPUT hay không
        //Vì ta có thể mở Activity với những RequestCode khác nhau
        if (requestCode == REQUEST_CODE) {
            //Kiểm trả ResultCode trả về, cái này ở bên InputDataActivity truyền về
            //ClearControl();
            OnShowGridDetail();
//            switch (resultCode) {
//                case RESULT_CODE:
//
//                    break;
//            }
        }
    }
    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onApprove")) {
//                    Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
//                    btnMSlip.setVisibility(View.GONE);
//                    btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
//                    btnApprove.setVisibility(View.GONE);
//                    ProcessApprove();
                }
                if (_type.equals("onDelAll")) {
                    ProcessDelete();
                }
                if (_type.equals("onMakeSlip")) {
                    //Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    //btnMSlip.setVisibility(View.GONE);
//                    btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
//                    btnApprove.setVisibility(View.GONE);
                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void ProcessDelete() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);


            cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "' and STATUS  in ('000',' ') AND ITEM_BC IS NOT NULL", null); //SLIP_NO='-' and
            int count = cursor.getCount();
            String para = "";
            boolean flag = false;
            int j = 0;
            if (cursor.moveToFirst()) {
                data1 = new String[1];
                do {

                    flag = true;
                    for (int k = 0; k < cursor.getColumnCount(); k++) {
                        if (para.length() <= 0) {
                            if (cursor.getString(k) != null)
                                para += cursor.getString(k);
                            else
                                para += "|";
                        } else {
                            if (flag == true) {
                                if (cursor.getString(k) != null) {
                                    para += cursor.getString(k);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor.getString(k) != null)
                                    para += "|" + cursor.getString(k);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserPK;
                    para += "|" + formID;
                    para += "|delete";
                    para += "*|*";
                } while (cursor.moveToNext());

                para += "|!LG_MPOS_UPD_INV_TR_DEL";
                data1[j] = para;
                Log.e("para update||delete: ", para);
            }
            else{
                onDeleteAllFinish();
            }

        } catch (Exception ex) {
            gwMActivity.alertToastLong("Delete All :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();

        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity, this);
            task.execute(data1);
        } else {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }
    }


    public void onDeleteAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='" + formID + "';");


        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            OnShowGridDetail();
            OnShowScanLog();
            OnShowScanIn();
            txt_reqNo.setText("");
            txt_reqQty.setText("");
            txt_reqBal.setText("");
            txt_scanned.setText("");
            txt_BCScan.setText("");
            str_reqNo = "";
            str_reqType="";
        }
    }

    public void onListGridBot(View view) {
        if (!checkRecordGridView(R.id.grdScanAccept)) return;


        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = new GridListViewBot();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    public void onClickViewStt(View view) {



        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ////////////////////////////////////////////////
    public void onClickInquiry(View view) {
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        /*Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        startActivity(openNewActivity);*/
        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = new PopupInquiry();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    ///////////////////////////////////////////////
    public void onListGridMid(View view) {
        if (!checkRecordGridView(R.id.grdScanIn)) return;


        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = frGridListViewMid.newInstance(1, formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");

        //  gwMActivity.showDialog(formID);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnInquiry:
                onClickInquiry(v);
                break;

            case R.id.btnApprove:
                break;

            case R.id.btnViewStt:
                onClickViewStt(v);
                break;

            case R.id.btnListBot:
                onListGridBot(v);
                break;

            case R.id.btnDelBC:
                break;

            case R.id.btnList:
                onListGridMid(v);
                break;

            case R.id.btnDelAll:
                onDelAll(v);
                break;

            case R.id.btnMakeSlip:
                onMakeSlip(v);
                break;

            default:
                break;
        }
    }
}
