package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

import gw.genumobile.com.R;
import gw.genumobile.com.interfaces.GridListView;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.models.gwform_info;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.HandlerWebService;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.AppConfig;

public class MappingV2 extends gwFragment implements View.OnClickListener {
    protected Boolean flagUpload = true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;
    private SharedPreferences mappingPrefs;

    Handler handler = new Handler();

    Queue queue = new LinkedList();
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    String data[] = new String[0], process_type="01";
    String sql = "", scan_date = "", scan_time = "", unique_id = "", parent_item_pk = "", parent_item_name = "", str_valid_same = "",grade_item="";
    EditText bc;
    EditText currentBC, _edItemCode, _edQty, _edLotNo, edt_item_code,edt_item_name,edt_qty,edt_weight,edt_remark,edt_dofing_no,edt_issue_date,edt_gross_weight;
    CheckBox _ckbLotNo, _ckbItemCode;
    TextView recyclableTextView, _txtError, _txtTotalGridBot, _txtTotalQtyBot, _txtTotalGridMid,txtprocess_type;
    public Spinner spn_sewing;
    int pkParent = 0;
    int indexDialog = 0;
    HashMap hashMapItem = new HashMap();

    private Handler customHandler = new Handler();

    CharSequence seqColor[] = new CharSequence[]{"red", "green", "blue", "black", "red", "green", "blue", "black", "red", "green", "blue", "black", "red", "green", "blue", "black", "red", "green", "blue", "black", "red", "green", "blue", "black"};
    Button btnViewStt, btnList, btnUploadMapping, btnDelAll, btnDelBC, btnMakeSlip, btnListBot,btn_new,btn_save,btn_view,btn_bclist;
    View rootView;
    HashMap hashMapGrade= new HashMap();
    String[][] ls_grade = new String[][]{
            {"A", "A"},
            {"B", "B"},
            {"C", "C"}};
    String[] lableinfo = new  String[0];
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_mapping_v2, container, false);
        gwMActivity.getWindow().setSoftInputMode(1);
        //lock screen
        //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mappingPrefs = gwActivity.getSharedPreferences("MappingConfig", gwActivity.MODE_PRIVATE);
        //
        InitActivity();
        OnShowGridHeader();
        OnShowChild();
        init_color();
        edt_issue_date.setText(CDate.getDateFormatyyyyMMdd());
        LoadGrade(ls_grade);
        bc.setFocusable(true);
        bc.requestFocus();
        bc.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        _txtError = (TextView) rootView.findViewById(R.id.txtError);
                        _txtError.setTextColor(Color.RED);
                        _txtError.setTextSize((float) 16.0);
                        if (bc.getText().toString().equals("")) {
                            _txtError.setText("Pls Scan barcode!");
                            bc.requestFocus();
                        } else {
                            OnSaveData();
                        }
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // gwMActivity is for backspace
                    bc.clearFocus();
                    Thread.interrupted();
                    //finish();
                    //System.exit(0);
                    //Log.e("IME_TEST", "BACK VIRTUAL");

                }
                return false;
            }
        });
        bc.requestFocus();
        return rootView;
    }

    protected void InitActivity() {

        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnDelBC= (Button) rootView.findViewById(R.id.btnDelBC);
        btnDelBC.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);
        btnUploadMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnUploadMapping.setOnClickListener(this);
        btn_new=(Button) rootView.findViewById(R.id.btn_new);
        btn_new.setOnClickListener(this);
        btn_save=(Button) rootView.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);
        btn_view=(Button) rootView.findViewById(R.id.btn_view);
        btn_view.setOnClickListener(this);
        btn_bclist=(Button) rootView.findViewById(R.id.btn_bclist);
        btn_bclist.setOnClickListener(this);
        edt_item_code = (EditText) rootView.findViewById(R.id.edt_item_code);
        edt_item_name = (EditText) rootView.findViewById(R.id.edt_item_name);
        edt_qty = (EditText) rootView.findViewById(R.id.edt_qty);
        edt_weight = (EditText) rootView.findViewById(R.id.edt_weight);
        edt_gross_weight = (EditText) rootView.findViewById(R.id.edt_gross_weight);
        edt_remark = (EditText) rootView.findViewById(R.id.edt_remark);
        edt_issue_date = (EditText) rootView.findViewById(R.id.edt_issue_date);
        edt_issue_date.setOnClickListener(this);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");
        currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
        _edItemCode = (EditText) rootView.findViewById(R.id.ed_ItemCode);
        _edQty = (EditText) rootView.findViewById(R.id.ed_Qty);
        _edLotNo = (EditText) rootView.findViewById(R.id.edt_lot_no);
        edt_dofing_no = (EditText) rootView.findViewById(R.id.edt_dofing_no);
        txtprocess_type = (TextView) rootView.findViewById(R.id.txtprocess_type);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setTextColor(Color.RED);
        _txtError.setTextSize((float) 16.0);
        _ckbLotNo = (CheckBox) rootView.findViewById(R.id.ckbLotNo);
        _ckbItemCode = (CheckBox) rootView.findViewById(R.id.ckbItemCode);
        spn_sewing = (Spinner) rootView.findViewById(R.id.spn_sewing);
        if (!mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {
            _ckbItemCode.setChecked(false);
            _ckbLotNo.setChecked(false);
        }
        bc = (EditText) rootView.findViewById(R.id.ed_BC);
    }

    private void init_color() {
        //region ------Set color tablet----
//        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
//        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
//        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
//        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdParent);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdChild);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private void LoadGrade(String[][] _data) {
        try {
            String lg_code="LGPC0071";
            String l_para = "1,LG_MPOS_GET_LG_CODE," + lg_code;

            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            _data = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();
            //
            for (int i = 0; i < _data.length; i++) {
                lstGroupName.add(_data[i][1]);
                hashMapGrade.put(i, _data[i][0]);
            }

            if (_data != null && _data.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spn_sewing.setAdapter(dataAdapter);
                //  wh_name = myLstWH.getItemAtPosition(0).toString();
            } else {
                spn_sewing.setAdapter(null);
                grade_item = "0";
            }
        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
        //-----------

        // onchange spinner wh
        spn_sewing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                // wh_name = myLstWH.getItemAtPosition(i).toString();
                // Log.e("WH_name",wh_name);
                Object pkGroup = hashMapGrade.get(i);
                if (pkGroup != null) {
                    grade_item = String.valueOf(pkGroup);
                    if (!grade_item.equals("0")) {

                        //dosomehitng

                    }
                } else {
                    grade_item = "0";
                    Log.e("grade_item: ", grade_item);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    //****************************************************************************
    public void OnSaveData() {
        bc = (EditText) rootView.findViewById(R.id.ed_BC);
        currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
        _edItemCode = (EditText) rootView.findViewById(R.id.ed_ItemCode);
        _edQty = (EditText) rootView.findViewById(R.id.ed_Qty);
        _edLotNo = (EditText) rootView.findViewById(R.id.edt_lot_no);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setTextColor(Color.RED);
        _txtError.setTextSize((float) 16.0);
        _ckbLotNo = (CheckBox) rootView.findViewById(R.id.ckbLotNo);
        _ckbItemCode = (CheckBox) rootView.findViewById(R.id.ckbItemCode);
        if (bc.getText().equals("")) {
            _txtError.setText("Pls Scan barcode!");
            return;
            //bc1.requestFocus();
        }

        String str_scan = bc.getText().toString().toUpperCase();
        int l_lenght = str_scan.length();
        //String str_bc=str_scan.substring(1, l_lenght);
        //System.out.println("str_bc " + str_bc);
        Log.e("para str_bc ", str_scan);

        //region  Parent YN = Y
        if (mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {

        }
        //endregion
        else {
            try {
                if (str_scan.equals("")) {
                    _txtError.setText("Vui lòng scan barcode!");
                    return;
                }

                List<String> lables = GetDataOnServer("Parent", str_scan);

                scan_date = CDate.getDateyyyyMMdd();
                scan_time = CDate.getDateYMDHHmmss();
                if (lables.size() > 0) {
                    String P_PACKAGES_PK = lables.get(0);
                    String P_ITEM_BC = lables.get(1);
                    String P_QTY = lables.get(2);
                    String P_ITEM_CODE = lables.get(3);
                    String P_ITEM_NAME = lables.get(4);
                    int Parent = Integer.valueOf(lables.get(5));
                    String P_TR_LOT_NO = lables.get(6);
                     process_type = lables.get(9);

                    currentBC.setText(P_ITEM_BC);
                    _edQty.setText(P_QTY);
                    _edItemCode.setText(P_ITEM_CODE);

                     if(!str_valid_same.equals("") && !str_valid_same.equals(P_ITEM_CODE)){
                        gwMActivity.alertToastLong(" Item Code is incorrect with " + str_valid_same + "!!!");
                        _txtError.setText(" Item Code is incorrect with " + str_valid_same + "!!!");
                         bc.getText().clear();
                         bc.requestFocus();
                        return;
                    }
                    int P_ITEM_PK = Integer.valueOf(lables.get(7));
                    String wh_pk = lables.get(8);
                    if (Parent <= 0) {
                        int cnt = Check_Exist_PK(str_scan);// check barcode exist in data
                        if (cnt > 0)// exist data
                        {
                            alertRingMedia();
                            _txtError.setText("Barcode này đã scan!");
                            bc.getText().clear();
                            bc.requestFocus();
                            return;
                        } else {

                            /*if(wh_pk.equals("0")){
                                _txtError.setText("Barcode chưa được nhập kho !!!");
                                onPopAlert("Barcode chưa được nhập kho !!!");
                                bc.getText().clear();
                                bc.requestFocus();
                                return;
                            }*/
                            String _bc_Type = "C";//child
                            if (_ckbItemCode.isChecked() && _ckbLotNo.isChecked()) {
                                String lotNoParent = _edLotNo.getText().toString();
                                String itemCodeParent = _edItemCode.getText().toString();
                                if (lotNoParent.equals(P_TR_LOT_NO) && itemCodeParent.equals(P_ITEM_CODE)) {
                                    if(str_valid_same.equals("")){
                                        str_valid_same = P_ITEM_CODE;
                                    }
                                    sql = "INSERT INTO INV_TR(ITEM_BC,TR_QTY,PARENT_PK,TR_PARENT_ITEM_PK,CHILD_PK,ITEM_CODE,ITEM_NAME,TR_LOT_NO,SCAN_DATE,SCAN_TIME,SENT_YN,BC_TYPE,MAPPING_YN,SLIP_NO,PROCESS_TYPE,TR_TYPE)"
                                            + "VALUES('"
                                            + P_ITEM_BC + "',"
                                            + P_QTY + ","
                                            + pkParent + ",'"
                                            + parent_item_pk + "',"
                                            + P_PACKAGES_PK + ",'"
                                            + P_ITEM_CODE + "','"
                                            + P_ITEM_NAME + "','"
                                            + P_TR_LOT_NO + "','"
                                            + scan_date + "','"
                                            + scan_time + "','"
                                            + "N" + "','"
                                            + _bc_Type + "','"
                                            + "N" + "','"
                                            + "-" + "','"
                                            + process_type + "','"
                                            + formID + "')";

                                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                                    db.execSQL(sql);
                                    db.close();
                                } else {
                                    onPopAlert("Child Item or Lot No are different Parent Item or Lot No");
                                    _txtError.setText("Child Item or Lot No are different Parent Item or Lot No !!!");
                                    bc.getText().clear();
                                    bc.requestFocus();
                                    return;
                                }
                            } else {
                                if(str_valid_same.equals("")){
                                    str_valid_same = P_ITEM_CODE;
                                }
                                sql = "INSERT INTO INV_TR(ITEM_BC,TR_QTY,PARENT_PK,TR_PARENT_ITEM_PK,CHILD_PK,ITEM_CODE,ITEM_NAME,TR_LOT_NO,SCAN_DATE,SCAN_TIME,SENT_YN,BC_TYPE,MAPPING_YN,TR_TYPE,TR_ITEM_PK,PROCESS_TYPE,SLIP_NO)"
                                        + "VALUES('"
                                        + P_ITEM_BC + "',"
                                        + P_QTY + ","
                                        + pkParent + ",'"
                                        + parent_item_pk + "',"
                                        + P_PACKAGES_PK + ",'"
                                        + P_ITEM_CODE + "','"
                                        + P_ITEM_NAME + "','"
                                        + P_TR_LOT_NO + "','"
                                        + scan_date + "','"
                                        + scan_time + "','"
                                        + "N" + "','"
                                        + _bc_Type + "','"
                                        + "N" + "','"
                                        + formID + "',"
                                        + P_ITEM_PK + ",'"
                                        + process_type + "','"
                                        + "-" + "')";

                                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                                db.execSQL(sql);
                                db.close();
                            }

                            _txtError.setText("Save success.");
                            bc.getText().clear();
                            bc.requestFocus();
                            OnShowChild();
                        }
                    } else {
                        _txtError.setText("Barcode đã được làm mapping !!!");
                        onPopAlert("Barcode đã được làm mapping !!!");
                        bc.getText().clear();
                        bc.requestFocus();
                        return;
                    }


                } else {
                    _txtError.setText("Không tìm thấy barcode!");
                    bc.setText("");
                    bc.requestFocus();
                    //currentBC.getText().clear();
                    //_edQty.getText().clear();
                    return;
                }
            } catch (Exception ex) {
                bc.getText().clear();
                bc.requestFocus();
                Log.e("Save error:", ex.toString());

            }
        }
    }


    //****************************************************************************
    public List<String> GetDataOnServer(String type, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";
            if (type.endsWith("Parent")) {
                l_para = "1,LG_MPOS_GET_PARENT_BC," + para;
            }
            if (type.endsWith("Child")) //WH
            {
                l_para = "1,LG_MPOS_GET_CHILD_BC," + para;
            }
            //ConnectThread networkThread = new ConnectThread(gwMActivity);
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                ////if (type.endsWith("SlipNo")) {
                for (int j = 0; j < result[i].length; j++)
                    labels.add(result[i][j].toString());
                ////}
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(type + ": " + ex.getMessage());
        }
        return labels;
    }

    public void onDelAll(View view) {
        if (!checkRecordGridView(R.id.grdParent)
                && !checkRecordGridView(R.id.grdChild)) return;
        String title = "Confirm Delete...";
        String mess = "Are you sure you want to delete all";
        alertDialogYN(title, mess, "onDelAll");
    }
    public void onDelBC(View view) {
        try {

            String para = "";

            if (queue.size() > 0) {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                Object[] myLst = queue.toArray();
                for (int i = 0; i < myLst.length; i++) {
                    db.execSQL("DELETE FROM INV_TR where PK = " + myLst[i].toString() + "");
                }
            }
            queue=new LinkedList();
            OnShowChild();
        } catch (Exception ex) {
            gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
        } finally {
            db.close();
        }

    }
    public void ProcessDeleteAll() {
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));

        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String para = "";
            boolean flag = true;
            cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "'", null);
            int j = 0;

            if (cursor.moveToFirst()) {
                data = new String[1];
                do {

                    flag = true;
                    for (int k = 0; k < cursor.getColumnCount(); k++) {
                        if (para.length() <= 0) {
                            if (cursor.getString(k) != null)
                                para += cursor.getString(k);
                            else
                                para += "|";
                        } else {
                            if (flag == true) {
                                if (cursor.getString(k) != null) {
                                    para += cursor.getString(k);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor.getString(k) != null)
                                    para += "|" + cursor.getString(k);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "|" + formID;
                    para += "|delete";
                    para += "*|*";
                } while (cursor.moveToNext());

                para += "|!LG_MPOS_UPD_INV_TR_DEL";
                data[j] = para;
            }

        } catch (Exception ex) {
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
            str_valid_same="";
        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity, this);
            task.execute(data);
            str_valid_same="";
        } else {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }


    }

    public void onDelAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();


            } else {
                //db.execSQL("DELETE FROM INV_TR where STATUS NOT IN('000') and TR_TYPE ='"+formID+"';");
                db.execSQL("DELETE FROM INV_TR where TR_TYPE ='" + formID + "';");
            }
        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
            _edQty = (EditText) rootView.findViewById(R.id.ed_Qty);
            _edItemCode = (EditText) rootView.findViewById(R.id.ed_ItemCode);
            _txtError = (TextView) rootView.findViewById(R.id.txtError);
            currentBC.setText("");
            _edQty.setText("");
            _edItemCode.setText("");
            _txtError.setText("");
            pkParent = 0;
            OnShowChild();
            gwMActivity.alertToastShort("Delete success..");
            str_valid_same="";
        }
    }

    public void onSave(View view) {
        /*
        ArrayList mSelectedItems = new ArrayList();
        AlertDialog.Builder builder = new AlertDialog.Builder(gwMActivity);

        builder.setTitle("What do you Like ?");
        builder.setSingleChoiceItems(seqColor, -1, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Toast.makeText(getApplicationContext(), "You Choose : " + seqColor[arg1], Toast.LENGTH_LONG).show();
                indexDialog = arg1;
            }
        });

        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                currentBC.setText(seqColor[indexDialog]);
                //Toast.makeText(getApplicationContext(), "You Have Cancel the Dialog box", Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
        */

        if (!checkRecordGridView(R.id.grdChild)) return;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Mapping...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to Save Mapping");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                //Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
                //btnMapping.setVisibility(View.GONE);
                ProcessMapping();
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    private  void LoadDate(View v){
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                //updateLabel();
                String myFormat = "yyyy/MM/dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                edt_issue_date.setText(sdf.format(myCalendar.getTime()));
            }
        };
        new DatePickerDialog(gwMActivity, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    public void ProcessMapping() {

        final ProgressDialog dialog = new ProgressDialog(this.getContext());
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        dialog.setMessage("Uploading  data into server, please wait.");
                        dialog.setCancelable(false);//not close when touch
                        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        dialog.setProgress(0);
                        dialog.show();

                    }
                });

        unique_id = CDate.getDateyyyyMMddhhmmss();
        //region PARENT YN=Y
        if (mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {

        }
        //endregion
        else {
//            try {
//                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
//                cursor = db.rawQuery("SELECT CHILD_PK,TR_ITEM_PK,UOM,TR_LOT_NO,TR_QTY,ITEM_BC,EXECKEY" +    //,PARENT_PK,TR_PARENT_ITEM_PK
//                        "    FROM INV_TR " +
//                        "    WHERE DEL_IF =0 AND BC_TYPE='C' AND TR_TYPE='" + formID + "' AND SENT_YN='N' AND MAPPING_YN='N' " +
//                        "   ORDER BY PK ASC ", null);
//                int count = cursor.getCount();
//                data = new String[1];
//                int j = 0;
//                String para = "";
//                boolean flag = false;
//                if (cursor.moveToFirst()) {
//                    do {
//                        flag = true;
//                        for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
//                            if (para.length() <= 0) {
//                                if (cursor.getString(k) != null)
//                                    para += cursor.getString(k) + "|";
//                                else
//                                    para += "0|";
//                            } else {
//                                if (flag == true) {
//                                    if (cursor.getString(k) != null) {
//                                        para += cursor.getString(k);
//                                        flag = false;
//                                    } else {
//                                        para += "|";
//                                        flag = false;
//                                    }
//                                } else {
//                                    if (cursor.getString(k) != null)
//                                        para += "|" + cursor.getString(k);
//                                    else
//                                        para += "|";
//                                }
//                            }
//                        }
//                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
//                        if (execkey == null || execkey.equals("")) {
//                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
//                            String ex_item_bc = cursor.getString(cursor.getColumnIndex("ITEM_BC"));
//                            String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
//                            hp.updateExeckeyMapping(formID, ex_item_pk, ex_item_bc, ex_lot_no, unique_id);
//
//                            para += "|" + unique_id;
//                        } else {
//                            para += "|" + execkey;
//                        }
//                        para += "|" + grade_item;
//                        para += "|" + edt_weight.getText();
//                        para += "|" + edt_gross_weight.getText();
//                        para += "|" + edt_remark.getText();
//                        para += "|" + _edLotNo.getText();
//                        para += "|" + edt_dofing_no.getText();
//                        para += "|" +String.valueOf (edt_issue_date.getText()).replace("/","");;
//                        para += "|" + bsUserID;
//                        para += "|" + bsUserPK;
//                        para += "|" + deviceID;
//                        para += "*|*";///multi row
//
//                    } while (cursor.moveToNext());
//                    para += "|!" + "LG_MPOS_PRO_MAPP_MSLIP";
//                    data[j++] = para;
//
//                }
//            } catch (Exception ex) {
//                gwMActivity.alertToastLong("Upload Mapping :" + ex.getMessage());
//                Log.e(" Error Upload Mapping :", ex.getMessage().toString());
//            } finally {
//                cursor.close();
//                db.close();
//            }
//            // asyns server
//            if (CNetwork.isInternet(tmpIP, checkIP)) {
//                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity, this);
//                task.execute(data);
//                str_valid_same="";
//            } else {
//                gwMActivity.alertToastLong("Network is broken. Please check network again !");
//            }

            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("SELECT CHILD_PK,TR_ITEM_PK,UOM,TR_LOT_NO,TR_QTY,ITEM_BC,EXECKEY" +    //,PARENT_PK,TR_PARENT_ITEM_PK
                        "    FROM INV_TR " +
                        "    WHERE DEL_IF =0 AND BC_TYPE='C' AND TR_TYPE='" + formID + "' AND SENT_YN='N' AND MAPPING_YN='N' " +
                        "   ORDER BY PK ASC ", null);
                int count = cursor.getCount();
                data = new String[1];
                int j = 0;
                String para = "";
                String key_char ="";
                boolean flag = false;
                if (cursor.moveToFirst()) {

                    do {
                        para  +=  key_char + cursor.getString(cursor.getColumnIndex("CHILD_PK"));
                        key_char = ";";

                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_item_bc = cursor.getString(cursor.getColumnIndex("ITEM_BC"));
                            String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            hp.updateExeckeyMapping(formID, ex_item_pk, ex_item_bc, ex_lot_no, unique_id);

                            para += ":" + unique_id;
                        } else {
                            para += ":" + execkey;
                        }



                    } while (cursor.moveToNext());
                    para += "|" + grade_item;
                    para += "|" + edt_weight.getText();
                    para += "|" + edt_gross_weight.getText();
                    para += "|" + edt_remark.getText();
                    para += "|" + _edLotNo.getText();
                    para += "|" + edt_dofing_no.getText();
                    para += "|" +String.valueOf (edt_issue_date.getText()).replace("/","");;
                    para += "|" + bsUserID;
                    para += "|" + bsUserPK;
                    para += "|" + deviceID;

                    //para += "|!" + "LG_MPOS_PRO_MAPP_MSLIP";


                }

                if ( !para.isEmpty()){

                    //ConnectThread networkThread = new ConnectThread(gwMActivity);
                    ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                    final String result[][] = networkThread.execute("1,LG_MPOS_PRO_MAPP_MSLIP_UP," + para).get();

                    if(result.length == 1 && result[0][0].toString().endsWith("ERROR")){
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                gwMActivity.alertToastLong("Upload Mapping :" + result[0][1].toString());

                            }
                        });

                    }else{

                        String pk = "";
                        for( int i = 0; i < result.length;i++){
                            int item_pk = Integer.valueOf(result[i][0]);
                            String item_bc = result[i][1];
                            String lotNo = result[i][2];
                            String slipNo = result[i][3];
                            pk =  result[i][4];

                            SQLiteDatabase db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                            String sql = "UPDATE INV_TR set SLIP_NO= '" + slipNo + "',MAPPING_YN='Y'  " +
                                    " where TR_TYPE='"+ gwform_info.MappingV2.Id()+"' AND SLIP_NO IN('-',' ',null) and ITEM_BC='" + item_bc + "' and TR_ITEM_PK=" + item_pk  ;
                            db2.execSQL(sql);
                            db2.close();
                        }
                        String[] parareturn =new  String[0];
                        String result11= HandlerWebService.ExecuteProcedureOutPara("LG_MPOS_PRO_MAPP_LABEL", pk );
                        parareturn = result11.split("\\|!");

                        final String[] finalParareturn = parareturn;
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                OnShowChild(finalParareturn);

                            }
                        });

                    }

                }
            } catch (Exception ex) {

                Log.e(" Error Upload Mapping :", ex.getMessage().toString());

            } finally {
                cursor.close();
                db.close();


            }

        }
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        dialog.dismiss();
                    }
                });
            }
        }).start();

    }
    public  void  OnNew(View view){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm New...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to make New Mapping");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                edt_item_code.getText().clear();
                edt_item_name.getText().clear();
                edt_qty.getText().clear();
                edt_remark.getText().clear();
                edt_weight.getText().clear();
                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
                scrollablePart.removeAllViews();
                ProcessDeleteAll();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }
    public  void  OnMapping(View view){
        if (mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {

        }
        //endregion
        else {
            try {

                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("SELECT ITEM_CODE,ITEM_NAME,TR_ITEM_PK,UOM,TR_LOT_NO,TR_QTY,ITEM_BC,EXECKEY" +    //,PARENT_PK,TR_PARENT_ITEM_PK
                        "    FROM INV_TR " +
                        "    WHERE DEL_IF =0 AND BC_TYPE='C' AND TR_TYPE='" + formID + "' AND SENT_YN='N' AND MAPPING_YN='N' " +
                        "   ORDER BY PK ASC ", null);
                int count = cursor.getCount();
                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                String qty,item_code, item_name, p_lot_no;
                List<String> ls_lotno = new ArrayList<String>();

                // cursor.moveToFirst();
                float _qty=0;
                if (cursor != null && cursor.moveToFirst()) {
                    edt_item_code.setText(cursor.getString(0));
                    edt_item_name.setText(cursor.getString(1));
                    p_lot_no =cursor.getString(4);
                    ls_lotno.add(p_lot_no);
                    for (int i = 0; i < count; i++) {
                        _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                       if (!ls_lotno.contains(cursor.getString(cursor.getColumnIndex("TR_LOT_NO"))) && !cursor.getString(cursor.getColumnIndex("TR_LOT_NO")).equals(""))
                           ls_lotno.add(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")));
                        cursor.moveToNext();
                    }
                }
                edt_qty.setText(String.valueOf(_qty));

                if(process_type.equals("02")) {
                    edt_dofing_no.setEnabled(true);
                    txtprocess_type.setText("R/C");
                    StringBuilder sb = new StringBuilder();
                    for (String s : ls_lotno)
                    {
                        sb.append(s);
                        sb.append("-");
                    }
                    sb.setLength(sb.length() - 1);
                    if (sb.length() >0)
                        _edLotNo.setText( sb.toString());
                }
                else {
                    edt_dofing_no.setEnabled(false);
                    Collections.sort(ls_lotno,Collections.reverseOrder());

                    txtprocess_type.setText("YARN");
                    _edLotNo.setText(ls_lotno.get(0));
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("Upload Mapping :" + ex.getMessage());
                Log.e(" Error Upload Mapping :", ex.getMessage().toString());
            } finally {
                cursor.close();
                db.close();
            }
        }
    }
    // show data scan in
    public  void  OnViewBC(View view){
        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = new MappingV2_Pop();
        Bundle args = new Bundle();
        if(lableinfo.length >0) {

            args.putString("type", "online");
            args.putString("para1", lableinfo[0]);
            //  args.putStringArray("LINE_DETAIL", line_detail);

            dialogFragment.setArguments(args);
            dialogFragment.setTargetFragment(this, 1);
            dialogFragment.setCancelable(false);
            dialogFragment.show(fm.beginTransaction(), "dialog");
        }
    }
    public  void  OnListBC(View view){
        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = new MappingV2_Pop2();
        Bundle args = new Bundle();
     //   args.putStringArray("para1",lableinfo);
        //  args.putStringArray("LINE_DETAIL", line_detail);

        dialogFragment.setArguments(args);
        dialogFragment.setTargetFragment(this, 1);
        dialogFragment.setCancelable(false);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    public void OnShowChild(String[]para) {
        Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnMapping.setVisibility(View.VISIBLE);
        lableinfo=para;

        ((TextView)rootView.findViewById(R.id.txtPallet)).setText("BC: "+lableinfo[1]);

        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select PK,ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,MAPPING_YN,TR_LOT_NO,SLIP_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and BC_TYPE='C' AND TR_TYPE='" + formID + "' " +
                    " ORDER BY pk desc";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), 0, fixedRowHeight, 1));
                    if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y"))
                        row.addView(makeTableColWithText("YES", scrollableColumnWidths[2], fixedRowHeight, 0));
                    else
                        row.addView(makeTableColWithText("-", scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, -1));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(7); //SLIP_NO
                            String st_slipNO = tvSlipNo.getText().toString();

                            if (st_slipNO.equals("-")) {

                                TextView tv1 = (TextView) tr1.getChildAt(8); //PK
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255, 99, 71));
                                }

                                if (queue.size() > 0) {
                                    if (queue.contains(tv1.getText().toString())) {
                                        queue.remove(tv1.getText().toString());

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK);
                                        }
                                        duplicate = true;
                                    }
                                }
                                if (!duplicate)
                                    queue.add(tv1.getText().toString());
                            }
                            gwMActivity.alertToastShort(String.valueOf(queue.size()));

                        }
                    });
                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND BC_TYPE = 'C' AND TR_TYPE='" + formID + "'  ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowChild() {
        Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnMapping.setVisibility(View.VISIBLE);
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select PK,ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,MAPPING_YN,TR_LOT_NO,SLIP_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and BC_TYPE='C' AND TR_TYPE='" + formID + "' " +
                    " ORDER BY pk desc";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), 0, fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));

                    if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y"))
                        row.addView(makeTableColWithText("YES", scrollableColumnWidths[2], fixedRowHeight, 0));
                    else
                        row.addView(makeTableColWithText("-", scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, -1));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(7); //SLIP_NO
                            String st_slipNO = tvSlipNo.getText().toString();

                            if (st_slipNO.equals("-")) {

                                TextView tv1 = (TextView) tr1.getChildAt(8); //PK
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255, 99, 71));
                                }

                                if (queue.size() > 0) {
                                    if (queue.contains(tv1.getText().toString())) {
                                        queue.remove(tv1.getText().toString());

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK);
                                        }
                                        duplicate = true;
                                    }
                                }
                                if (!duplicate)
                                    queue.add(tv1.getText().toString());
                            }
                            gwMActivity.alertToastShort(String.valueOf(queue.size()));

                        }
                    });
                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND BC_TYPE = 'C' AND TR_TYPE='" + formID + "'  ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowChildParent(int parent) {
        Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
        btnMapping.setVisibility(View.VISIBLE);
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            // int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,MAPPING_YN,TR_LOT_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and BC_TYPE='C' AND TR_TYPE='" + formID + "' and PARENT_PK=" + parent +
                    " ORDER BY pk desc  ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), 0, fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, 0));

                    if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y"))
                        row.addView(makeTableColWithText("YES", scrollableColumnWidths[2], fixedRowHeight, 0));
                    else
                        row.addView(makeTableColWithText("-", scrollableColumnWidths[2], fixedRowHeight, 0));

                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.MAGENTA);
            _txtTotalQtyBot.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }


    ///////////////////////////////////////////////
    public void onListGridBot(View view) {
        if (!checkRecordGridView(R.id.grdChild)) return;

        Intent openNewActivity = new Intent(view.getContext(), GridListViewBot.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        //startActivity(openNewActivity);
        startActivityForResult(openNewActivity, REQUEST_CODE);
    }

    /**
     * Xử lý kết quả trả về ở đây
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiểm tra có đúng requestCode =REQUEST_CODE_INPUT hay không
        //Vì ta có thể mở Activity với những RequestCode khác nhau
        if (requestCode == REQUEST_CODE) {
            //Kiểm trả ResultCode trả về, cái này ở bên InputDataActivity truyền về
            OnShowChild();
            switch (resultCode) {
                case RESULT_CODE:

                    break;
            }
        }
    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[5]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Mapping", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[2], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    // end get data return array--use array set to grid view


    /////////////////////////
    // check data inv_tr
    public int Check_Exist_PK(String l_bc_item) {
        int countRow = 0;
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String countQuery = "SELECT PK FROM INV_TR where del_if=0 and TR_TYPE='" + formID + "' and ITEM_BC ='" + l_bc_item + "' ";
            // db = gwMActivity.getReadableDatabase();
            cursor = db.rawQuery(countQuery, null);
            // Check_Exist_PK=cursor.getCount();
            if (cursor != null && cursor.moveToFirst()) {
                countRow = cursor.getCount();
            }

        } catch (Exception ex) {
            gwMActivity.alertToastLong("Check_Exist_PK :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return countRow;

    }

    ////////////////////////////////////////////////
    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event

                if (_type.equals("onDelAll")) {
                    ProcessDeleteAll();
                }
                if (_type.equals("onMakeSlip")) {


                    //ProcessMakeSlip();

                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str = str_alert;

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            // onLoadFull();
            Intent i = new Intent(gwMActivity, AppConfig.class);
            i.putExtra("type", formID);
            i.putExtra("user", bsUserID);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onLoadFull() {
        Intent openNewActivity = new Intent(gwMActivity, GridListView.class);
        //send data into Activity
        openNewActivity.putExtra("type", "10");
        startActivity(openNewActivity);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:


                break;
            case R.id.btnUploadMapping:
                OnMapping(view);

                break;
            case R.id.btnViewStt:

                break;
            case R.id.btnSelectMid:


                break;
            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelBC:
                onDelBC(view);
                break;
            case R.id.btnList:


                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnMakeSlip:
                break;
            case R.id.btn_new:
                OnNew(view);
                break;
            case R.id.btn_save:
                onSave(view);
                break;
            case R.id.btn_view:
                OnViewBC(view);
                break;
            case R.id.btn_bclist:
                OnListBC(view);
                break;
            case R.id.edt_issue_date:
                LoadDate(view);
                break;

            default:
                break;
        }
    }
}
