package gw.genumobile.com.views.productresult;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import gw.genumobile.com.R;
import gw.genumobile.com.db.CSVFile;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.models.gwfunction;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.views.gwFragment;

public class ProdMonitoring extends gwFragment {
    protected int[] bsColors = new int[]{0xff00ddff, 0x300000FF, 0xCCFFFF, 0x99CC33, 0x00FF99};
    protected int[] scrollableColumnWidths = new int[]{10, 10, 10, 10, 10};
    SQLiteDatabase db;

    TextView bsTextView;
    protected int fixedRowHeight = 65; //70|30
    protected int fixedHeaderHeight = 65; //80|40
    private BarChart mChart;
    public Spinner  myLstLine;
    HashMap hashMapGrpLine = new HashMap();
    private String  grpline_pk="";
    public JDataTable dtmonitor =null;
    View rootView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_prod_monitoring, container, false);

     //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        TextView infoDate = (TextView) rootView.findViewById(R.id.infoDate);
        myLstLine = (Spinner) rootView.findViewById(R.id.cbLineGroup);
        Thread myThread = null;

        Runnable runnable = new CountDownRunner();
        myThread= new Thread(runnable);
        myThread.start();
        LoadLine();
       /////


    return  rootView;
    }
    private void LoadLine() {
        String   l_para =String.format("1,%s,1,%s|$s", gwfunction.prodmonitoring_selGrgLine.getValue(),"",bsUserID);
        try {

            List<String> lstGroupName = new ArrayList<String>();
            gwService networkThread = new gwService(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
            List<JDataTable> jdt = dtLocGroup.getListODataTable();
            JDataTable dt=jdt.get(0);

            for (int i = 0; i < dt.totalrows; i++) {
                lstGroupName.add(dt.records.get(i).get("line_name").toString());
                hashMapGrpLine.put(i, dt.records.get(i).get("pk").toString());
            }

            if (dt != null &&  dt.totalrows> 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstLine.setAdapter(dataAdapter);
                //  wh_name = myLstWH.getItemAtPosition(0).toString();
            } else {
                myLstLine.setAdapter(null);
                grpline_pk = "0";
            }
        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
        //-----------

        // onchange myLstLine
        myLstLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here

                Object pkGroup = hashMapGrpLine.get(i);
                if (pkGroup != null) {
                    grpline_pk = String.valueOf(pkGroup);
                    Log.e("line_pk: ", grpline_pk);
                    if (!grpline_pk.equals("0")) {
                        LoadGridMonitor(grpline_pk);
                      }
                } else {
                    grpline_pk = "0";
                    Log.e("wi_pk: ", grpline_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
    public void LoadGridMonitor(String grp_pk)
    {
        String   l_para =String.format("1,%s,1,%s|%s|%s", gwfunction.prodmonitoring_q.getValue(),"Q",grp_pk,"20170223");
        try {

            List<String> lstGroupName = new ArrayList<String>();
            gwService networkThread = new gwService(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
            List<JDataTable> jdt = dtLocGroup.getListODataTable();
            dtmonitor=jdt.get(0);
            OnShowGridHeader(dtmonitor);
            BindingGrid(dtmonitor);
            DrawChart();

        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
    }
   private void OnShowGridHeader(JDataTable dt)// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT,
                DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdMonitor);
        scrollablePart.removeAllViews();
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(Color.parseColor("#00ddff"));
       // row.setVerticalGravity(50);
        row.addView(makeTableColHeaderWithText("LINE", scrollableColumnWidths[1], fixedHeaderHeight));
        for (int i = 0; i < dt.totalrows; i++) {
            String line = dt.records.get(i).get("line_name").toString();
            row.addView(makeTableColHeaderWithText(line,  scrollableColumnWidths[1], fixedHeaderHeight));
        }
    /*    row.addView(makeTableColHeaderWithText(mLine[0],  scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(mLine[1], scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(mLine[2],  scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(mLine[3],  scrollableColumnWidths[1], fixedHeaderHeight));*/
        scrollablePart.addView(row);

    }
    private void BindingGrid(JDataTable dt)
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT,
                DrawerLayout.LayoutParams.WRAP_CONTENT);
        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdMonitor);

        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(Color.parseColor("#00ddff"));
        row.addView(makeTableColWithText("PLAN", scrollableColumnWidths[1], fixedRowHeight,0, Color.rgb(104, 241, 175)));
        for (int i = 0; i < dt.totalrows; i++) {
            String plan_qty =  dt.records.get(i).get("plan_qty") == null ? "" :  dt.records.get(i).get("plan_qty").toString();
            row.addView(makeTableColWithText(plan_qty,
                    scrollableColumnWidths[4], fixedRowHeight,0,-1));
        }
        scrollablePart.addView(row);

        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(Color.parseColor("#00ddff"));
        row.addView(makeTableColWithText("PROD", scrollableColumnWidths[1], fixedRowHeight,0, Color.rgb(164, 228, 251)));
        for (int i = 0; i < dt.totalrows; i++) {
            String plan_qty = dt.records.get(i).get("pro_qty") == null ? "" :  dt.records.get(i).get("pro_qty").toString();
            row.addView(makeTableColWithText(plan_qty,
                    scrollableColumnWidths[4], fixedRowHeight,0,-1));
        }
        scrollablePart.addView(row);

        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(Color.parseColor("#00ddff"));
        row.addView(makeTableColWithText("DEF", scrollableColumnWidths[1], fixedRowHeight,0, Color.rgb(255, 102, 0)));
        for (int i = 0; i < dt.totalrows; i++) {
            String plan_qty =  dt.records.get(i).get("def_qty") == null ? "" :  dt.records.get(i).get("def_qty").toString();
            row.addView(makeTableColWithText(plan_qty,
                    scrollableColumnWidths[4], fixedRowHeight,0,-1));
        }
        scrollablePart.addView(row);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(Color.parseColor("#00ddff"));
        row.addView(makeTableColWithText("RATE", scrollableColumnWidths[1], fixedRowHeight,0, Color.rgb(242, 247, 158)));
        for (int i = 0; i < dt.totalrows; i++) {

            String plan_qty = dt.records.get(i).get("rate") == null ? "" :  dt.records.get(i).get("rate").toString();
            row.addView(makeTableColWithText(plan_qty,
                    scrollableColumnWidths[4], fixedRowHeight,0,-1));
        }
        scrollablePart.addView(row);

    }

    public void DrawChart()
    {
        mChart = (BarChart) rootView.findViewById(R.id.chartM);
        // mChart.setOnChartValueSelectedListener(gwMActivity);
        mChart.getDescription().setEnabled(false);
        MyMarkerView mv = new MyMarkerView(gwMActivity, R.layout.custom_marker_view);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);
        mChart.setDrawBorders(true);
        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
        //  l.setTypeface(mTfLight);
        l.setYOffset(0f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);
        final int start = 0;
        final int end = dtmonitor.totalrows;
        XAxis xAxis = mChart.getXAxis();
        // xAxis.setTypeface(mTfLight);
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(true);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                if(value<start ||value>=end)
                    return  String.valueOf((int) value);
                Log.i("FDFDS VAL",     String.valueOf((int) value) );
                Log.i("FDFDS",       dtmonitor.records.get((int) value).get("line_name").toString()   );
                return dtmonitor.records.get((int) value).get("line_name").toString();
            }
        });
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(true);
        YAxis leftAxis = mChart.getAxisLeft();
        //  leftAxis.setTypeface(mTfLight);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(true);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        mChart.getAxisRight().setEnabled(false);

        ///
        float groupSpace = 0.08f;
        float barSpace = 0.03f; // x4 DataSet
        float barWidth = 0.2f; // x4 DataSet
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"



        int  groupCount =end-start;


        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
        ArrayList<BarEntry> yVals2 = new ArrayList<BarEntry>();
        ArrayList<BarEntry> yVals3 = new ArrayList<BarEntry>();
        ArrayList<BarEntry> yVals4 = new ArrayList<BarEntry>();
        for( int i=0;i<dtmonitor.totalrows;i++)
        {
            String plan_qty =  dtmonitor.records.get(i).get("plan_qty") == null||  dtmonitor.records.get(i).get("plan_qty").equals("") ? "0" :  dtmonitor.records.get(i).get("plan_qty").toString();
            String pro_qty =  dtmonitor.records.get(i).get("pro_qty") == null||  dtmonitor.records.get(i).get("pro_qty").equals("") ? "0" :  dtmonitor.records.get(i).get("pro_qty").toString();
            String def_qty =  dtmonitor.records.get(i).get("def_qty") == null||  dtmonitor.records.get(i).get("def_qty").equals("") ? "0" :  dtmonitor.records.get(i).get("def_qty").toString();
            String rate =  dtmonitor.records.get(i).get("rate") == null||  dtmonitor.records.get(i).get("rate").equals("") ? "0" :  dtmonitor.records.get(i).get("rate").toString();

            yVals1.add(new BarEntry(i, Float.valueOf(plan_qty)));
            yVals2.add(new BarEntry(i, Float.valueOf(pro_qty)));
            yVals3.add(new BarEntry(i, Float.valueOf(def_qty)));
            yVals4.add(new BarEntry(i,(Float.valueOf(rate))));
        }
        BarDataSet set1, set2, set3, set4;
        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0) {

            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set2 = (BarDataSet) mChart.getData().getDataSetByIndex(1);
            set3 = (BarDataSet) mChart.getData().getDataSetByIndex(2);
            set4 = (BarDataSet) mChart.getData().getDataSetByIndex(3);
            set1.setValues(yVals1);
            set2.setValues(yVals2);
            set3.setValues(yVals3);
            set4.setValues(yVals4);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();

        } else {
            // create 4 DataSets
            set1 = new BarDataSet(yVals1, "PLAN");
            set1.setColor(Color.rgb(104, 241, 175));
            set2 = new BarDataSet(yVals2, "PROD");
            set2.setColor(Color.rgb(164, 228, 251));
            set3 = new BarDataSet(yVals3, "DEF");
            set3.setColor(Color.rgb(255, 102, 0));
            set4 = new BarDataSet(yVals4, "RATE");
            set4.setColor(Color.rgb(242, 247, 158));

            BarData data = new BarData(set1, set2, set3, set4);
            //     data.setValueFormatter(new LargeValueFormatter());
            //   data.setValueTypeface(mTfLight);

            mChart.setData(data);
            mChart.getXAxis().setAxisMinimum(data.getXMin());
            //  mChart.getXAxis().setAxisMaximum(data.getXMax());
        }

        mChart.getBarData().setBarWidth(barWidth);

        // restrict the x-axis range
        mChart.getXAxis().setAxisMinimum(start);

        // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
        mChart.getXAxis().setAxisMaximum(start + mChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        mChart.groupBars(start, groupSpace, barSpace);
        mChart.invalidate();
    }

    public TextView makeTableColHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        TextView bsTextView ;
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.parseColor("#0029ff"));
        bsTextView.setTextSize(20);
        bsTextView.setTypeface(null, Typeface.BOLD);
        bsTextView.setGravity(Gravity.CENTER);
        bsTextView.setBackgroundColor(Color.parseColor("#d6f1ff"));
        bsTextView.setPadding(0, 2, 0, 2);
        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }
    public TextView makeTableColWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels, int textAlign, int color) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.BLACK);
        if (textAlign == -1) {
            bsTextView.setGravity(Gravity.LEFT);
            bsTextView.setPadding(1, 1, 1, 1);
        } else if (textAlign == 0) {
            bsTextView.setGravity(Gravity.CENTER);
            bsTextView.setPadding(0, 5, 0, 5);
        } else {
            bsTextView.setGravity(Gravity.RIGHT);
            bsTextView.setPadding(0, 5, 10, 5);
        }
        bsTextView.setPadding(6, 2, 2, 2);
        bsTextView.setTextSize(20);
        bsTextView.setSingleLine(false);
       // bsTextView.setTextColor(color);
        bsTextView.setBackgroundColor(color);
      //  bsTextView.sty
        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }
    public void doWork() {
        gwMActivity.runOnUiThread(new Runnable() {
            public void run() {
                try{
                    TextView txtCurrentTime= (TextView)rootView.findViewById(R.id.infoDate);
                    Calendar c = Calendar.getInstance();
                    String month=c.get(Calendar.MONTH)+1+"";
                    if(month.length()<2){
                        month="0"+month;
                    }
                    String date=c.get(Calendar.DATE)+"";
                    if(date.length()<2){
                        date="0"+date;
                    }
                    String ymd = c.get(Calendar.YEAR) + "-" + month  + "-" + date;
                    int hours = c.getTime().getHours();
                    int minutes = c.getTime().getMinutes();
                    int seconds = c.getTime().getSeconds();

                    txtCurrentTime.setText( ymd + " "+hours + ":" + minutes + ":" + seconds);
                }catch (Exception e) {}
            }
        });
    }

    class CountDownRunner implements Runnable {
        // @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()){
                try {
                   doWork();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }catch(Exception e){
                }
            }
        }
    }
}
