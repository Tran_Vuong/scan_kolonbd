package gw.genumobile.com.views.inventory;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import gw.genumobile.com.interfaces.GridListView;
import gw.genumobile.com.R;
import gw.genumobile.com.models.Config;
import gw.genumobile.com.services.UploadAsyncTask;

public class Incoming extends Activity {
	private int[] colors = new int[] { 0x30FF0000, 0x300000FF,0xCCFFFF,0x99CC33,0x00FF99 };
	private String USER="Android";
	public Editable str_scan;
	int p_label_pk, wh_pk, wh_loc_pk, line_pk;
	int P_TLG_IT_ITEM_PK;

	String _drCurrentScan = null, lbl_SlipNo1_Tag = "", wh_name = "",
			wh_loc_name = "", line_name = "", p_item_type = "";

	String P_ITEM_BC, P_ITEM_CODE, P_ITEM_NAME, P_TR_LOT_NO, P_TR_QTY,
			P_SLIP_NO;
	int cnt = 0;
	SQLiteDatabase db = null;
	SQLiteDatabase mydb = null;
	Cursor cursor,cursor1;
	SimpleCursorAdapter adapter;
	//--upload--
	SQLiteDatabase db2 = null;
	Cursor cursor2;
	// --grid gridlayout
	GridView gridView;
	ListView lv1;
	LinearLayout Linear;
	EditText bc1, bc2, tr_qty,code,name,lotNo;
	Button btnDelete;
	Button btnClose;
	Button btnUploadInc;
	TextView txt_error2, txtTotalLabel;
	String CREATE_TABLE_DUAL;
	String l_Scan_Type = "1";// incoming

	public Spinner lst_WH, lst_LOC, lst_Line;
	ArrayAdapter<String> dataAdapter;
	
	String data[]=new String [0];

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_incoming);
		//OnCreateTableINV_TR();

		btnDelete = (Button) findViewById(R.id.btnDelete);
		btnClose = (Button) findViewById(R.id.btnClose_Ingo);
		btnUploadInc = (Button) findViewById(R.id.btnUploadInc);		
		bc1 = (EditText) findViewById(R.id.edit_Bc1);
		bc2 = (EditText) findViewById(R.id.edit_Bc2);
		code = (EditText) findViewById(R.id.edit_Code2);
		name = (EditText) findViewById(R.id.edit_Name);
		lotNo = (EditText) findViewById(R.id.edit_LotNo);
		tr_qty = (EditText) findViewById(R.id.edit_Qty);

		btnClose.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// do something when the button is clicked
				finish();
				System.exit(0);
				
			}
		});
		///////////////////////////
		LoadWH();
		// LoadLOC();
		LoadLine();
		OnShowGW_Header(); 
		OnShowGW();

		// addKeyListener();		
		bc1.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					if (event.getAction() == KeyEvent.ACTION_DOWN) {
						txt_error2 = (TextView) findViewById(R.id.txt_error2);
						txt_error2.setTextColor(Color.RED);
						txt_error2.setTextSize((float) 18.0);
						if ("".equals(bc1.getText().toString())) {
							txt_error2.setText("Pls Scan barcode!");
							bc1.requestFocus();
						} else {
							OnSaveData();														
						}
					}
					return true;//important for event onKeyDown
				}
				if (keyCode == KeyEvent.KEYCODE_BACK) {
	                // this is for backspace
					bc1.clearFocus();
					Thread.interrupted();
					finish();
					System.exit(0);
	                //Log.e("IME_TEST", "BACK VIRTUAL");
	            }

				return false;
			}
		});
		// onchange spinner wh
		lst_WH.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int i, long id) {
				// your code here
				wh_name = lst_WH.getItemAtPosition(i).toString();
				List<String> lables = GetDataWH_PK();
				wh_pk = Integer.parseInt(lables.get(i));
				//System.out.println("value select wh:"+lables.get(i));
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});
		// onchange spinner line
		lst_Line.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int i, long id) {
				// your code here
				line_name = lst_Line.getItemAtPosition(i).toString();
				List<String> lables = GetDataLine_PK();
				line_pk = Integer.parseInt(lables.get(i));
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});
	}
	
	// select data
//	public void OnCreateTableINV_TR() {
//		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
//		// CREATE TABLE IF NOT EXISTS INV_TR 
//		String CREATE_INV_TR = "CREATE TABLE IF NOT EXISTS INV_TR ( "
//				+ "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
//				+ "TLG_POP_LABEL_PK INTEGER, " + "TR_WAREHOUSE_PK INTEGER, "
//				+ "TR_LOC_ID TEXT, " + "TR_QTY INTEGER, " + "TR_LOT_NO TEXT, "
//				+ "TR_TYPE TEXT, " + "ITEM_BC TEXT, " + "TR_DATE TEXT, "
//				+ "TR_ITEM_PK INTEGER, " + "TR_LINE_PK INTEGER, "
//				+ "ITEM_CODE TEXT, " + "ITEM_NAME TEXT, " + "ITEM_TYPE TEXT, "
//				+ "TLG_GD_REQ_D_PK INTEGER, " + "TR_WH_IN_NAME TEXT, "
//				+ "LOC_NAME TEXT, " + "LINE_NAME TEXT, " + "SLIP_NO TEXT, DEL_IF INTEGER default 0 )";
//		db.execSQL(CREATE_INV_TR);
//		db.close();
//	}
	//////////////////////////////
	//-------upload------------///
	//////////////////////////////
	public void  onClickUploadDataInc(View view) throws InterruptedException, ExecutionException{

		 db2=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
	     cursor2 = db2.rawQuery("select TLG_POP_LABEL_PK, TR_WAREHOUSE_PK,TR_LOC_ID, TR_QTY , TR_LOT_NO, TR_TYPE, ITEM_BC, TR_DATE, TR_ITEM_PK, TR_LINE_PK, ITEM_CODE, ITEM_NAME,ITEM_TYPE,PK,TLG_GD_REQ_D_PK, SLIP_NO  from INV_TR where DEL_IF=0",null);
	   	 System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor2.getCount()));
	     //data=new String [cursor2.getCount()];
	   	 boolean read= Config.ReadFileConfig();
	   	 USER=Config.USER;
	     if (cursor2.moveToFirst()) {
	    	 
	    	// INSERT DATA INTO DB
	    		// Show dialog Yes No
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);		 
		        // Setting Dialog Title
		        alertDialog.setTitle("Confirm Upload...");
		        // Setting Dialog Message
		        alertDialog.setMessage("Are you sure you want to upload data to server?"); 
		        // Setting Icon to Dialog
		        alertDialog.setIcon(R.drawable.cfm_up); 
		        // Setting Positive "Yes" Button
		        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog,int which) { 
		            // Write your code here to invoke YES event
		            	//set value message
		        		txt_error2 = (TextView) findViewById(R.id.txt_error2);
		        		txt_error2.setTextColor(Color.RED);
		        		txt_error2.setText("");
		        		
		        		data=new String [cursor2.getCount()];
		           	  	int j=0;
		        		do {
		                       // labels.add(cursor.getString(1));
		                  	  String para ="";
		          	          for(int i=0; i < cursor2.getColumnCount();i++)
		          	 			{
		          		        		if(para.length() <= 0)
		          		        		{
		          		        			if(cursor2.getString(i)!= null)	  	        			
		          		        				para += cursor2.getString(i);
		          		        			else
		          		        				para += "|!";	  	        			
		          		        		}
		          		        		else
		          		        		{
		          		        			if(cursor2.getString(i)!= null)
		          		        				para += "|!" + cursor2.getString(i);
		          		        			else
		          		        				para += "|!";	  	        			
		          		        		}
		          	 				
		          	 			}
		          		        para += "|!"+USER;

		          		        System.out.print("\n\n ******para: "+para);
		                  	  data[j++]=para;
		                  	  
		                    } while (cursor2.moveToNext());
		        		 //////////////////////////
		        		cursor2.close();   
		               db2.close();
		               doStart();
		            }
		        });
		        // Setting Negative "NO" Button
		        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		            // Write your code here to invoke NO event
		            	//Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
		            	 //////////////////////////
		        	      cursor2.close();   
		        	      db2.close();
		            	dialog.cancel();	
		            	
		            }
		        }); 
		        // Showing Alert Message
		        alertDialog.show();
	    	 	
	     }else{
	    	cursor2.close();   
   	      	db2.close();
	    	txt_error2 = (TextView) findViewById(R.id.txt_error2);
			txt_error2.setTextColor(Color.RED);
			txt_error2.setText("Data is empty!");
	     }
	     //Toast.makeText(this, "Upload!",Toast.LENGTH_LONG).show();
	  	 //finish();	     
	}
	private void doStart()
	{
		  System.out.print("\n\n**********doStart********\n\n\n");
		 
		  UploadAsyncTask task = new UploadAsyncTask(this);
      	  task.execute(data);
	}
	 
	public void clearData(){
		 
		 db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		 db.delete("INV_TR", null, null);
		 db.close();
	}
	public void clearAllData(){
		CheckBox ck = (CheckBox)findViewById(R.id.ckbClearData);
		if(ck.isChecked() == true)
		{
			db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
			//db.delete("INV_TR", null, null);
			db.execSQL("DELETE from INV_TR where DEL_IF != '0'");
			db.close();
		}
		else
			Log.e("ck.isChecked() == else: ","ck.isChecked() == else");
		
	}
	///////////////////////////////
	public void OnDeleteBC(String p_pk){
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);

		db.execSQL("DELETE from INV_TR where ITEM_BC = '"+ p_pk+"'");
		System.out.println("delete ok");			
		
		db.close();
	}
	
	// end get data return array--use array set to grid view
	@SuppressWarnings("deprecation")
	public void OnShowGW()// show data gridview
	{
		TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20,20};
        int[] scrollableColumnWidths = new int[]{20, 20, 20, 30, 30};
        int fixedRowHeight = 40;
        int fixedHeaderHeight = 60;

        TableRow row = new TableRow(this);
        TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
        scrollablePart.removeAllViews();//remove all view child
        ///
        db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
        cursor1 = db.rawQuery("SELECT PK FROM INV_TR where TR_TYPE='1' order by PK desc ",null);// TLG_LABEL
        cursor = db.rawQuery("SELECT ITEM_BC, TR_QTY, TR_LOT_NO,TR_WH_IN_NAME,LINE_NAME,null WH_LOC,ITEM_CODE,ITEM_NAME FROM INV_TR where TR_TYPE='1' order by PK desc  LIMIT 20",null);// TLG_LABEL
        int countList = cursor1.getCount();
        int count = cursor.getCount();
		int l_total = cursor.getCount() - 1;
		txtTotalLabel = (TextView) findViewById(R.id.txtTotalLabel);
		txtTotalLabel.setText("Total:" + countList + " Label(s)");
		txtTotalLabel.setTextColor(Color.BLUE);
		txtTotalLabel.setTextSize((float) 18.0);
		// count la so dong,con so 5 la column,-->so phan tu cua mang =count*5
		String[] vals = new String[count * 7];
		if (cursor != null && cursor.moveToFirst()) {
			for (int i = 0; i < count; i++) {
	    		row = new TableRow(this);
		        row.setLayoutParams(wrapWrapTableRowParams);
		        row.setGravity(Gravity.CENTER);
		        row.setBackgroundColor(Color.LTGRAY);

		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[1], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[3], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[4], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[4], fixedRowHeight));
		        row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight));
		        
		        row.setOnClickListener(new OnClickListener() {

		            @Override
		            public void onClick(View v) {
		                TableRow tr1=(TableRow)v;
		                TextView tv1= (TextView)tr1.getChildAt(0);
		                int xx=tr1.getChildCount();
		                for (int i = 0; i < xx; i++) {
		                	TextView tv11= (TextView)tr1.getChildAt(i);
		                	tv11.setTextColor(-256);
		                }
		                //Toast.makeText(getApplicationContext(),tv1.getText().toString(),Toast.LENGTH_SHORT).show();
		                bc2 = (EditText) findViewById(R.id.edit_Bc2);
		                bc2.setText(tv1.getText().toString());
		            }
		        });
		        
		        scrollablePart.addView(row);
		        
				cursor.moveToNext();
			}
		}
		cursor1.close();
		cursor.close();
		db.close();        
	}
	
	// show data with control	
	// show data to tablelayout the same with gridview
	@SuppressWarnings("deprecation")
	public String[] getData() {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		cursor = db.rawQuery("SELECT ITEM_BC, TR_QTY, TR_LOT_NO,TR_WH_IN_NAME,LINE_NAME,null WH_LOC,ITEM_CODE,ITEM_NAME FROM INV_TR where TR_TYPE='1' order by PK desc",null);// TLG_LABEL
		int count = cursor.getCount();

		txtTotalLabel = (TextView) findViewById(R.id.txtTotalLabel);
		txtTotalLabel.setText("Total :" + count + " (Labels.)");
		txtTotalLabel.setTextColor(Color.BLUE);
		txtTotalLabel.setTextSize((float) 13.0);
		// count la so dong,con so 5 la column,-->so phan tu cua mang =count*5
		String[] vals = new String[count * 8 + 8];
		vals[0]="ITEM BC";
		vals[1]="QTY";
		vals[2]="LOT.NO";
		vals[3]="WH.NAME";
		vals[4]="LINE.NAME";
		vals[5]="WH.LOC";
		vals[6]="ITEM CODE";
		vals[7]="ITEM NAME";
		if (cursor != null && cursor.moveToFirst()) {
			int j = 8;
			for (int i = 0; i < count; i++) {
				vals[j++] = cursor.getString(cursor.getColumnIndex("ITEM_BC"))+ " ";
				vals[j++] = cursor.getString(cursor.getColumnIndex("TR_QTY"))+ " ";
				vals[j++] = cursor.getString(cursor.getColumnIndex("TR_LOT_NO")) + " ";
				vals[j++] = cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME"))+ " ";// wh
				vals[j++] = cursor.getString(cursor.getColumnIndex("LINE_NAME")) + " ";// line
				vals[j++] = cursor.getString(cursor.getColumnIndex("WH_LOC"))+ " ";// loc
				vals[j++] = cursor.getString(cursor.getColumnIndex("ITEM_CODE")) + " ";
				vals[j++] = cursor.getString(cursor.getColumnIndex("ITEM_NAME")) + " ";

				// System.out.println("row : " + vals[j-1]);
				cursor.moveToNext();
			}
			cursor.close();
			return vals;
		}
		db.close();
		return vals;
	}
	//////////////////////////////////
	private void LoadWH() {
		lst_WH = (Spinner) findViewById(R.id.lst_WH);
		List<String> lables = GetDataWH();
		// System.out.println("\n ****string list 1:"+lables.get(1));
		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, lables);

		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		lst_WH.setAdapter(dataAdapter);
	}
	////
	public List<String> GetDataWH() {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		List<String> labels = new ArrayList<String>();
		// Select All Query tlg_in_warehouse
		String selectQuery = "SELECT  pk,wh_id||'-'||wh_name,wh_name FROM tlg_in_warehouse where pk >0 order by  wh_id";// tlg_in_warehouse
		// db = this.getReadableDatabase();
		cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor != null && cursor.moveToFirst()) {
			do {
				labels.add(cursor.getString(1));
			} while (cursor.moveToNext());
		}
		// closing connection
		cursor.close();
		db.close();
		return labels;
	}
	// public void GetDataWH(){
	public List<String> GetDataWH_PK() {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);

		List<String> labels = new ArrayList<String>();

		// Select All Query
		String selectQuery = "SELECT  pk,wh_id||'-'||wh_name,wh_name FROM tlg_in_warehouse where pk >0 order by  wh_id ";// tlg_in_warehouse

		// db = this.getReadableDatabase();
		cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor != null && cursor.moveToFirst()) {
			do {
				labels.add(cursor.getString(0));
				// System.out.println("\n \n******data spinner :"+cursor.getString(1));
			} while (cursor.moveToNext());
		}

		// closing connection
		cursor.close();
		db.close();

		return labels;
	}
	//////////////////////////////////////////////////////////////
	//----LOAD LINE----------
	//////////////////////////////////////////////////////////////
	private void LoadLine() {

		lst_Line = (Spinner) findViewById(R.id.lst_line);
		List<String> lables = GetDataLine();

		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, lables);

		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// attaching data adapter to spinner
		lst_Line.setAdapter(dataAdapter);
	}
	public List<String> GetDataLine() {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);

		List<String> labels = new ArrayList<String>();
		// Select All Query
		String selectQuery = "SELECT  pk,pk||'-'||line_id, line_name FROM tlg_pb_line where pk>0 order by line_id ";

		// db = this.getReadableDatabase();
		cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor != null && cursor.moveToFirst()) {
			do {
				labels.add(cursor.getString(1));
			} while (cursor.moveToNext());
		}
		// closing connection
		cursor.close();
		db.close();
		return labels;
	}
	///////////////
	public List<String> GetDataLine_PK() {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);

		List<String> labels = new ArrayList<String>();
		// Select All Query
		String selectQuery = "SELECT  pk,line_id||'-'||line_name, line_name FROM tlg_pb_line ";
		// db = this.getReadableDatabase();
		cursor = db.rawQuery(selectQuery, null);
		// looping through all rows and adding to list
		if (cursor != null && cursor.moveToFirst()) {
			do {
				labels.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		// closing connection
		cursor.close();
		db.close();

		return labels;
	}
	/////////////////////////////////////
	////util method
    private TextView recyclableTextView;
    
	public void OnShowGW_Header()// show data gridview
	{
		TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20,20};
        int[] scrollableColumnWidths = new int[]{20, 20, 20, 30, 30};
        int fixedRowHeight = 40;
        int fixedHeaderHeight = 40;

        TableRow row = new TableRow(this);
        TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part1);
       
        
        row = new TableRow(this);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(colors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText("ITEM_BC", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("TR_QTY", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("TR_LOT_NO", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("TR_WH_IN_NAME", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("LINE_NAME", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("ITEM_CODE", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("ITEM_NAME", scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);
	}
	
	public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
		params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(this);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.BLACK);
        recyclableTextView.setTextSize(20);
        recyclableTextView.setBackgroundColor(-1);
        recyclableTextView.setPadding(0, 5, 0, 5);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }
	public TextView makeTableRowHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
    	int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
		params.setMargins(1, 1, 1, 1);
		recyclableTextView =new  TextView(this);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(-256);
        recyclableTextView.setTextSize(20);
        recyclableTextView.setBackgroundColor(colors[1]);
        recyclableTextView.setPadding(0, 5, 0, 5);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }
    
	
	////////////////////////////////////////////////
	//
	///////////////////////////////////////////////
	public void onListGrid(View view) throws InterruptedException,ExecutionException {
		Intent openNewActivity = new Intent(view.getContext(), GridListView.class);
		//send data into Activity
		openNewActivity.putExtra("type", "1");
		startActivity(openNewActivity);
	}
	////////////////////////////////////////////////
	//
	///////////////////////////////////////////////
	public void onClickSearch(View view) throws InterruptedException,ExecutionException {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
        cursor = db.rawQuery("SELECT  ITEM_CODE,count(ITEM_CODE) as COUNT_ITEM,SUM(TR_QTY) AS QTY FROM INV_TR where TR_TYPE='1' GROUP BY ITEM_CODE ",null);// TLG_LABEL
		int count = cursor.getCount();
		String str="";
		if (cursor != null && cursor.moveToFirst()) {
			str="ITEM \t\t\t COUNT  \t\t\t QTY\n----------------------------------------------------------------\n";
			for (int i = 0; i < count; i++) {
		       str=str+cursor.getString(cursor.getColumnIndex("ITEM_CODE"))+"\t\t\t\t "+cursor.getString(cursor.getColumnIndex("COUNT_ITEM"))+"\t\t\t\t\t "+cursor.getString(cursor.getColumnIndex("QTY"))+"\n";
		       str=str+"----------------------------------------------------------------\n";
				cursor.moveToNext();
			}
		}else{
			str="Not found data.";
		}
		cursor.close();
		db.close(); 
		//Dialog
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);		 
        // Setting Dialog Title
        alertDialog.setTitle("List Item ...");
        // Setting Dialog Message
        alertDialog.setMessage(str); 
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram); 

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            // Write your code here to invoke NO event
            	//Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
            	dialog.cancel();
            }
        }); 
        // Showing Alert Message
        alertDialog.show();
												
		
	}
		
	//////////////////////////////////	
	////////////////////////////////////////////////
	//
	///////////////////////////////////////////////
	public void DeleteData(View view) throws InterruptedException,ExecutionException {
		
		txt_error2 = (TextView) findViewById(R.id.txt_error2);
		txt_error2.setTextColor(Color.RED);
		txt_error2.setTextSize((float) 18.0);
		P_ITEM_BC = bc2.getText().toString();
		if (P_ITEM_BC.length() < 1) {
			txt_error2.setText("Please select B/C!:");
		} else {
	
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);		 
	        // Setting Dialog Title
	        alertDialog.setTitle("Confirm Delete...");
	        // Setting Dialog Message
	        alertDialog.setMessage("Are you sure you want to delete item B/C: \n"+P_ITEM_BC+"?"); 
	        // Setting Icon to Dialog
	        alertDialog.setIcon(R.drawable.cfm_del); 
	        // Setting Positive "Yes" Button
	        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int which) { 
	            // Write your code here to invoke YES event
	            	OnDeleteBC(P_ITEM_BC);
	    			bc2.getText().clear();
	    			code.getText().clear();
	    			name.getText().clear();
	    			lotNo.getText().clear();
	    			tr_qty.getText().clear();
	    			txt_error2.setText("Delete data sucess."+ P_ITEM_BC.toString());

	    			OnShowGW();
	            	Toast.makeText(getApplicationContext(), "Delete success..", Toast.LENGTH_SHORT).show();
	            }
	        });
	        // Setting Negative "NO" Button
	        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            // Write your code here to invoke NO event
	            	//Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
	            	dialog.cancel();
	            }
	        }); 
	        // Showing Alert Message
	        alertDialog.show();
												
		}
	}
		
	//////////////////////////////////		
	public void OnSaveData() {
		bc1 = (EditText) findViewById(R.id.edit_Bc1);
		bc2 = (EditText) findViewById(R.id.edit_Bc2);
		EditText code = (EditText) findViewById(R.id.edit_Code2);
		EditText name = (EditText) findViewById(R.id.edit_Name);
		EditText LotNo = (EditText) findViewById(R.id.edit_LotNo);

		txt_error2 = (TextView) findViewById(R.id.txt_error2);
		txt_error2.setTextColor(Color.RED);
		//txt_error2.setTextSize((float) 13.0);
		if (bc1.getText().toString().equals("")) {
			txt_error2.setText("Pls Scan barcode!");
			return;
			// bc1.requestFocus();
		}
		// setTextSize(float size)
		String str_scan = bc1.getText().toString().toUpperCase();
		//int l_lenght = bc1.getText().toString().length();

		//String strscan_1_to_end = bc1.getText().toString().substring(1, l_lenght);
		//String strscan_2_to_end = bc1.getText().toString().substring(2, l_lenght);

		try {
			if (str_scan.equals("")) {
				txt_error2.setText("Pls Scan barcode!");
				return;
			}
			int cnt = Check_Exist_PK(str_scan);// check barcode exist in data
												// base
			System.out.println("str_scan " + str_scan);
			boolean l_rs = OnLoadBC(str_scan);
			db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);
			final CheckBox chkDuplicate = (CheckBox) findViewById(R.id.chkDuplicate);
			if (cnt > 0)// exist data
			{
				if (chkDuplicate.isChecked()) {
					if (OnSave())// save data Duplicate barcode
					{
						txt_error2.setText("add new data sucess(insert Duplicate)!!!");
						bc1.getText().clear();
						bc1.requestFocus();
						OnShowGW();
					}
				} else {
					bc1.setText("");
					bc1.requestFocus();
				}
				// return;
			} else {
				if (l_rs)// scan barcode,has data then
				{
					// int cnt=Check_Exist_PK(str_scan);//check barcode exist in
					if (OnSave())// scanned save data existed from downoad
					{
						txt_error2.setText("insert label success.");
						bc1.getText().clear();
						bc1.requestFocus();
						OnShowGW();
					}

				} else {
					bc2.setText(bc1.getText());// scan barcode not exist then
												// save barcode here.(can not
												// yet download or register
												// barcode)
					code.setText("");
					name.setText("");
					LotNo.setText("");
					tr_qty.setText("");
					// txt_error2.setText("add new data sucess!!!");
					if (OnSave())// save data new,then update lot no,tr_qty
					{
						txt_error2.setText("add new data sucess!!!");
						bc1.getText().clear();
						bc1.requestFocus();
						// txt_error2.setText("Barcoce "+str_scan+" not exist.");
						OnShowGW();
					}
				}
			}
			db.close();

		} catch (Exception ex) {
			// save data error write to file log
		}
		//OnShowGW();
	}
	// end save data in control
	//
	public boolean OnSave() {
		//db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
		try {
			bc1 = (EditText) findViewById(R.id.edit_Bc1);
			EditText bc2 = (EditText) findViewById(R.id.edit_Bc2);
			EditText code = (EditText) findViewById(R.id.edit_Code2);
			EditText name = (EditText) findViewById(R.id.edit_Name);
			EditText LotNo = (EditText) findViewById(R.id.edit_LotNo);
			tr_qty = (EditText) findViewById(R.id.edit_Qty);

			// bc2.setText(bc1.getText());

			// get data input control to var then insert db

			P_ITEM_BC = bc2.getText().toString();
			P_ITEM_CODE = code.getText().toString();
			P_ITEM_NAME = name.getText().toString();
			P_TR_LOT_NO = LotNo.getText().toString();
			P_TR_QTY = tr_qty.getText().toString();

			String P_TR_DATE;
			Date cal = Calendar.getInstance().getTime();

			// c.get(Calendar.)
			P_TR_DATE = "";// nho lay sysdate

			int p_wh_pk = wh_pk;
			int p_wh_loc_pk = wh_loc_pk;
			int p_lie_pk = line_pk;

			// (TLG_POP_LABEL_PK, TR_WAREHOUSE_PK, TR_LOC_ID, TR_QTY, TR_LOT_NO,
			// TR_TYPE, ITEM_BC, TR_DATE, TR_ITEM_PK, TR_LINE_PK, ITEM_CODE,
			// ITEM_NAME,ITEM_TYPE)
			db.execSQL("INSERT INTO INV_TR(TLG_POP_LABEL_PK, TR_WAREHOUSE_PK,TR_LOC_ID, TR_LINE_PK, TR_QTY, TR_LOT_NO, ITEM_BC, TR_DATE, TR_ITEM_PK, ITEM_CODE, ITEM_NAME,TR_TYPE,TR_WH_IN_NAME,LINE_NAME,ITEM_TYPE) "
					+ "VALUES('"
					+ p_label_pk
					+ "','"
					+ p_wh_pk
					+ "','"
					+ p_wh_loc_pk
					+ "','"
					+ p_lie_pk
					+ "','"
					+ P_TR_QTY
					+ "','"
					+ P_TR_LOT_NO
					+ "','"
					+ P_ITEM_BC
					+ "','"
					+ P_TR_DATE
					+ "','"
					+ P_TLG_IT_ITEM_PK
					+ "','"
					+ P_ITEM_CODE
					+ "','"
					+ P_ITEM_NAME
					+ "','"
					+ l_Scan_Type
					+ "','"
					+ wh_name
					+ "','" + line_name + "','" + p_item_type + "');");
			 //bc1.setText("");
			 //bc1.requestFocus();
			// txt_error2.setText("Save Error!!!");
			// db.close();
			return true;
		} catch (Exception ex) {
			txt_error2.setText("Save Error!!!");
		}
		return true;

	}
	//////
	public boolean OnLoadBC(String l_bc_item) {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);

		boolean l_RS = false;
		String SQL_SEL_LABEL_REQ = "SELECT  PK AS LABEL_PK, TLG_IT_ITEM_PK, ITEM_BC, LABEL_QTY, LABEL_UOM, YYYYMMDD, ITEM_CODE, ITEM_NAME, ITEM_TYPE, LOT_NO "
				+ " FROM TLG_LABEL "
				+ " WHERE     (ITEM_BC = '"
				+ l_bc_item
				+ "') ";// ='"+l_bc_item+"'
		cursor = db.rawQuery(SQL_SEL_LABEL_REQ, null);
		int i = 0;

		EditText bc2 = (EditText) findViewById(R.id.edit_Bc2);
		// EditText reqNo =(EditText)findViewById(R.id.edit_reqNo);
		EditText code = (EditText) findViewById(R.id.edit_Code2);
		EditText name = (EditText) findViewById(R.id.edit_Name);
		EditText LotNo = (EditText) findViewById(R.id.edit_LotNo);
		tr_qty = (EditText) findViewById(R.id.edit_Qty);

		if (cursor != null && cursor.moveToFirst()) {
			bc2.setText(cursor.getString(cursor.getColumnIndex("ITEM_BC")));
			code.setText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")));
			name.setText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")));
			tr_qty.setText(cursor.getString(cursor.getColumnIndex("LABEL_QTY")));
			LotNo.setText(cursor.getString(cursor.getColumnIndex("LOT_NO")));
			p_label_pk = Integer.parseInt(cursor.getString(cursor
					.getColumnIndex("LABEL_PK")));
			P_TLG_IT_ITEM_PK = Integer.parseInt(cursor.getString(cursor
					.getColumnIndex("TLG_IT_ITEM_PK")));
			p_item_type = cursor.getString(cursor.getColumnIndex("ITEM_TYPE"));
			cursor.moveToNext();
			cursor.close();
			l_RS = true;
		}

		db.close();
		return l_RS;
	}
	////////////////////////////////////////
	// save data in control
	public void SaveData(View view) throws InterruptedException,ExecutionException {
		
		EditText bc2 = (EditText) findViewById(R.id.edit_Bc2);
		txt_error2 = (TextView) findViewById(R.id.txt_error2);
		if (bc2.length() < 1) {
			txt_error2.setText("Please, scan one barcode to update.");
			txt_error2.setTextColor(Color.RED);
			txt_error2.setTextSize((float) 18.0);
			return;
		} else {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);		 
	        // Setting Dialog Title
	        alertDialog.setTitle("Confirm update...");
	        // Setting Dialog Message
	        alertDialog.setMessage("Are you sure you want to update item B/C: \n"+P_ITEM_BC+"?"); 
	        // Setting Icon to Dialog
	        alertDialog.setIcon(R.drawable.cfm_save); 
	        // Setting Positive "Yes" Button
	        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int which) { 
	            // Write your code here to invoke YES event
	            	OnUpdateData();
	    			OnShowGW();
	            	Toast.makeText(getApplicationContext(), "Update success..", Toast.LENGTH_SHORT).show();
	            }
	        });
	        // Setting Negative "NO" Button
	        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            // Write your code here to invoke NO event
	            	dialog.cancel();
	            }
	        }); 
	        // Showing Alert Message
	        alertDialog.show();
		}
	}
	// update data in control
	public void OnUpdateData() {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);

		bc1 = (EditText) findViewById(R.id.edit_Bc1);
		String str_scan_bc = bc1.getText().toString();
		// int P_PK= Check_Exist_PK(str_scan_bc);
		EditText bc2 = (EditText) findViewById(R.id.edit_Bc2);
		EditText code = (EditText) findViewById(R.id.edit_Code2);
		EditText name = (EditText) findViewById(R.id.edit_Name);
		EditText LotNo = (EditText) findViewById(R.id.edit_LotNo);

		tr_qty = (EditText) findViewById(R.id.edit_Qty);

		// get data input control to var then update db
		P_ITEM_BC = bc2.getText().toString();
		P_ITEM_CODE = code.getText().toString();
		P_ITEM_NAME = name.getText().toString();
		P_TR_LOT_NO = LotNo.getText().toString();
		P_TR_QTY = tr_qty.getText().toString();
		db.execSQL("UPDATE INV_TR set TR_QTY='" + P_TR_QTY + "',TR_LOT_NO='"
				+ P_TR_LOT_NO
				+ "'  where pk = (select max(pk) from INV_TR where ITEM_BC ='"
				+ P_ITEM_BC + "') and TR_TYPE='" + l_Scan_Type + "' ");
		// System.out.println("update ok");
		txt_error2.setText("Update data sucess.");
		db.close();
		// Log.e("save","update ok");
	}
	//////////////////////////
	// check data inv_tr
	public int Check_Exist_PK(String l_bc_item) {
		db = openOrCreateDatabase("gasp", MODE_PRIVATE, null);

		bc1 = (EditText) findViewById(R.id.edit_Bc1);
		String countQuery = "SELECT PK FROM INV_TR where tr_type='1' and ITEM_BC ='"
				+ l_bc_item + "' ";// ='"+l_bc_item+"'
		int Check_Exist_PK = 0;
		// db = this.getReadableDatabase();
		cursor = db.rawQuery(countQuery, null);
		// Check_Exist_PK=cursor.getCount();
		if (cursor != null && cursor.moveToFirst()) {
			Check_Exist_PK = Integer.parseInt(cursor.getString(cursor
					.getColumnIndex("PK")));
			cursor.moveToNext();
			cursor.close();
			return Check_Exist_PK;
		}
		db.close();
		return Check_Exist_PK;

		// return count*/
	}
	
	////////////////////////////////
	//
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.incoming, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
