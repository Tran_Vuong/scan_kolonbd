package gw.genumobile.com.views.mold;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CNumber;

//RI20160406-001

/**
 *
 */
public class Mold_Status extends BaseGwActive  implements View.OnClickListener {

    //region Variable
    String [][] data1 = new String[][]{
            {"Mold_1","Mold_Name_1","1","Machine_1","Location_1","Shot_1","Actual_Shot_1","Shot_bal_1","QC_Result_1"},
            {"Mold_2","Mold_Name_2","2","Machine_2","Location_2","Shot_2","Actual_Shot_2","Shot_bal_2","QC_Result_2"},
            {"Mold_3","Mold_Name_3","3","Machine_3","Location_3","Shot_3","Actual_Shot_3","Shot_bal_3","QC_Result_3"},
            {"Mold_4","Mold_Name_4","4","Machine_4","Location_4","Shot_4","Actual_Shot_4","Shot_bal_4","QC_Result_4"},
            {"Mold_5","Mold_Name_5","1","Machine_5","Location_5","Shot_5","Actual_Shot_5","Shot_bal_5","QC_Result_5"},
            {"Mold_6","Mold_Name_6","2","Machine_6","Location_6","Shot_6","Actual_Shot_6","Shot_bal_6","QC_Result_6"},
            {"Mold_7","Mold_Name_7","3","Machine_7","Location_7","Shot_7","Actual_Shot_7","Shot_bal_7","QC_Result_7"},
            {"Mold_8","Mold_Name_8","3","Machine_8","Location_8","Shot_8","Actual_Shot_8","Shot_bal_8","QC_Result_8"},
            {"Mold_9","Mold_Name_9","3","Machine_9","Location_9","Shot_9","Actual_Shot_9","Shot_bal_9","QC_Result_9"},
            {"Mold_10","Mold_Name_10","3","Machine_10","Location_10","Shot_10","Actual_Shot_10","Shot_bal_10","QC_Result_10"},
            {"Mold_11","Mold_Name_11","1","Machine_11","Location_11","Shot_11","Actual_Shot_11","Shot_bal_11","QC_Result_11"},
            {"Mold_12","Mold_Name_12","1","Machine_12","Location_12","Shot_12","Actual_Shot_12","Shot_bal_12","QC_Result_12"},
            {"Mold_13","Mold_Name_13","2","Machine_13","Location_13","Shot_13","Actual_Shot_13","Shot_bal_13","QC_Result_13"},
            {"Mold_14","Mold_Name_14","3","Machine_14","Location_14","Shot_14","Actual_Shot_14","Shot_bal_14","QC_Result_14"},
            {"Mold_15","Mold_Name_15","4","Machine_15","Location_15","Shot_15","Actual_Shot_15","Shot_bal_15","QC_Result_15"},
            {"Mold_16","Mold_Name_16","1","Machine_16","Location_16","Shot_16","Actual_Shot_16","Shot_bal_16","QC_Result_16"},
            {"Mold_17","Mold_Name_17","2","Machine_17","Location_17","Shot_17","Actual_Shot_17","Shot_bal_17","QC_Result_17"},
            {"Mold_18","Mold_Name_18","1","Machine_18","Location_18","Shot_18","Actual_Shot_18","Shot_bal_18","QC_Result_18"},
            {"Mold_19","Mold_Name_19","4","Machine_19","Location_19","Shot_19","Actual_Shot_19","Shot_bal_19","QC_Result_19"},
            {"Mold_20","Mold_Name_20","3","Machine_20","Location_20","Shot_20","Actual_Shot_20","Shot_bal_20","QC_Result_20"},
            {"Mold_21","Mold_Name_21","3","Machine_21","Location_21","Shot_21","Actual_Shot_21","Shot_bal_21","QC_Result_21"},
            {"Mold_22","Mold_Name_22","1","Machine_22","Location_22","Shot_22","Actual_Shot_22","Shot_bal_22","QC_Result_22"},
            {"Mold_23","Mold_Name_23","4","Machine_23","Location_23","Shot_23","Actual_Shot_23","Shot_bal_23","QC_Result_23"},
            {"Mold_24","Mold_Name_24","3","Machine_24","Location_24","Shot_24","Actual_Shot_24","Shot_bal_24","QC_Result_24"},
            {"Mold_25","Mold_Name_25","2","Machine_25","Location_25","Shot_25","Actual_Shot_25","Shot_bal_25","QC_Result_25"},
            {"Mold_26","Mold_Name_26","1","Machine_26","Location_26","Shot_26","Actual_Shot_26","Shot_bal_26","QC_Result_26"},
            {"Mold_27","Mold_Name_27","1","Machine_27","Location_27","Shot_27","Actual_Shot_27","Shot_bal_27","QC_Result_27"},
            {"Mold_28","Mold_Name_28","1","Machine_28","Location_28","Shot_28","Actual_Shot_28","Shot_bal_28","QC_Result_28"},
            {"Mold_29","Mold_Name_29","3","Machine_29","Location_29","Shot_29","Actual_Shot_29","Shot_bal_29","QC_Result_29"},
            {"Mold_30","Mold_Name_30","3","Machine_30","Location_30","Shot_30","Actual_Shot_30","Shot_bal_30","QC_Result_30"}


    };

    String [][] data = new String[][]{
            {"1670","CMP Gamma Holder 5","M/C 1","2","Group_01","CMP Gamma Holder ","4000000","3205924","794076","0.8015"},
            {"1668","CMP Gamma Holder 4","M/C 2","2","Group_01","CMP Gamma Holder ","4000000","2192116","1807884","0.548"},
            {"1457","6Atss Holder 22","M/C 3","2","Group_02","6Atss Holder ","4000000","3200105","799895","0.8"},
            {"1459","6Atss Holder 26","01-Defect","4","Group_02","6Atss Holder ","4000000","3884411","115589","0.9711"},
            {"1412","6Atss Holder 52","M/C 4","2","Group_02","6Atss Holder ","4000000","3984532","15468","0.9961"},
            {"1816","CMP TAU CUP  5","L1","1","Group_03","CMP TAU CUP","2000000","","2000000","0"},
            {"1817","CMP TAU CUP 6","M/C 5","2","Group_03","CMP TAU CUP","2000000","617822","1382178","0.3089"},
            {"1829","CMP TAU CUP 7","M/C 6","2","Group_03","CMP TAU CUP","2000000","1613133","386867","0.8066"},
            {"1001","LSF PRO. CAP 3","A1","3","Group_04","LSF PRO. CAP ","4000000","952579","3047421","0.2381"},
            {"1662","MAP HOUSING 7","M/C 7","2","Group_05","MAP HOUSING","2000000","1579254","420746","0.7896"},
            {"1663","MAP HOUSING 8","02-Broken Part","4","Group_05","MAP HOUSING","2000000","1578006","421994","0.789"},
            {"1664","MAP HOUSING 9","L2","1","Group_05","MAP HOUSING","2000000","","2000000","0"},
            {"1775","HV16 BOBBIN","M/C 8","2","Group_06","HV16","4000000","1461800","2538200","0.3655"},
            {"1776","HV16 CONTACT GUIDE","A2","3","Group_06","HV16","4000000","461800","3538200","0.1155"},
            {"1210","CKP SLEEVE 2","M/C 9","2","Group_07","CKP SLEEVE","4000000","3060505","939495","0.7651"},
            {"1620","6ATSS CUP(OUT-D) 46,48","M/C 10","2","Group_08","6 ATSS CUP","2000000","1965662","34338","0.9828"},
            {"1414","6Atss Holder Sub. Assy (OUT-A) 23","02-Broken Part","4","Group_08","6 ATSS CUP","4000000","3999214","786","0.9998"},
            {"1619","6Atss CUP 43","M/C 11","2","Group_08","6 ATSS CUP","2000000","1620265","379735","0.8101"},
            {"1640","2 PIN CONECTOR I","M/C 12","2","Group_09","CONECTOR","4000000","3130800","869200","0.7827"},
            {"1641","2 PIN CONECTOR J","C1","3","Group_09","CONECTOR","4000000","130800","3869200","0.0327"},
            {"1464","CMP Gamma Holder 6","M/C 13","2","Group_10","CMP Gamma Holder","4000000","3217178","782822","0.8043"},
            {"1818","CMP Gamma Holder 7","M/C 14","2","Group_10","CMP Gamma Holder","4000000","937178","3062822","0.2343"},
            {"1819","CMP Gamma Holder 8","M/C 15","2","Group_10","CMP Gamma Holder","4000000","3273152","726848","0.8183"},
            {"1002","MAP PREMOLD 8","01-Defect","4","Group_11","MAP PREMOLD","3000000","946255","2053745","0.3154"},
            {"1003","MAP PREMOLD 9","M/C 17","2","Group_11","MAP PREMOLD","3000000","2122667","877333","0.7076"},
            {"1004","MAP PREMOLD 10","M/C 16","2","Group_11","MAP PREMOLD","3000000","1923935","1076065","0.6413"},
            {"1760","CMP U CUP 24 mm","02-Broken Part","4","Group_12","CMP U CUP","2000000","1553001","446999","0.7765"},
            {"1777","HV16 CAP","L3","1","Group_13","HV16","4000000","","4000000","0"},
            {"1477","NEW CMP (GAMMA) HOLDER - 3","M/C 19","2","Group_14","NEW CMP (GAMMA) HOLDER","4000000","2192116","1807884","0.548"},
            {"1668","NEW CMP (GAMMA) HOLDER - 4","C2","3","Group_14","NEW CMP (GAMMA) HOLDER","4000000","205924","3794076","0.0515"},
            {"1670","NEW CMP (GAMMA) HOLDER - 5","M/C 20","2","Group_14","NEW CMP (GAMMA) HOLDER","4000000","2192116","1807884","0.548"},
            {"1665","NEW CMP ALPHA 2 HOLDER","M/C 21","2","Group_14","NEW CMP (GAMMA) HOLDER","4000000","3200105","799895","0.8"},
            {"1361","MAP COVER 1","03-Stopping","4","Group_15","MAP COVER","4000000","3884411","115589","0.9711"},
            {"1610","MAP COVER 2","M/C 22","2","Group_15","MAP COVER","4000000","3984532","15468","0.9961"},
            {"1408","CKP (GAMMA) BOBBIN - 1","M/C 23","2","Group_16","CKP (GAMMA) BOBBIN","4000000","1617822","2382178","0.4045"},
            {"1478","CKP (GAMMA) BOBBIN - 2","B1","3","Group_16","CKP (GAMMA) BOBBIN","4000000","617822","3382178","0.1545"},
            {"1479","CKP (GAMMA) BOBBIN - 3","M/C 24","2","Group_16","CKP (GAMMA) BOBBIN","4000000","1613133","2386867","0.4033"},
            {"1617","CKP (GAMMA) BOBBIN - 4","B2","3","Group_16","CKP (GAMMA) BOBBIN","4000000","952579","3047421","0.2381"},
            {"1656","CKP (GAMMA) BOBBIN - 5","M/C 25","2","Group_16","CKP (GAMMA) BOBBIN","4000000","1579254","2420746","0.3948"},
            {"1657","CKP (GAMMA) BOBBIN - 6","03-Stopping","4","Group_16","CKP (GAMMA) BOBBIN","4000000","1578006","2421994","0.3945"},
            {"1658","CKP (GAMMA) BOBBIN - 7","M/C 26","2","Group_16","CKP (GAMMA) BOBBIN","4000000","1994708","2005292","0.4987"},
            {"1507","6ATSS HOLDER (INPUT 21) - 1st","L4","1","Group_17","6ATSS HOLDER","4000000","","4000000","0"},
            {"1508","6ATSS HOLDER (INPUT 22) - 3rd","M/C 27","2","Group_17","6ATSS HOLDER","4000000","461800","3538200","0.1155"}

    };
    private TextView tvDate, hd_All, tbStatus_A, hd_Line_Using, tbStatus_LU,
            hd_Problem, tbStatus_P, hd_Warehouse_Location, tbStatus_WL, hd_LineStorage, tbStatus_LS;

    private Button btnSearch;

    private int     WIDTH_IMAGE = 80, HEIGHT_IMAGE = 35, WIDTH_SEQ = 5
                ,   WIDTH_Mold_ID = 7, WIDTH_Mold_Name = 27,  WIDTH_STATUS = 8
                ,   WIDTH_Line_Using = 20, WIDTH_Problem = 15, WIDTH_Line_Storage = 20
                ,   WIDTH_Warehouse_Location = 18, WIDTH_Shot = 11,
                    WIDTH_Actual_Shot = 11, WIDTH_ShotBal = 11, WIDTH_QCResult = 10;

    private int COUNT_LINE_USING  = 0, COUNT_PROBLEM = 0, COUNT_WL = 0, COUNT_LineSto = 0;
    //endregion


    //region LIFE CIRCLE
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mold_status);
        //Read Config

        //lock screen
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        InitItem();
    }

    //endregionad


    //region INIT ITEM

    private void InitItem(){
        //Set listener
        hd_All = (TextView) findViewById(R.id.hd_All);
        hd_All.setOnClickListener(this);

        tbStatus_A = (TextView) findViewById(R.id.tbStatus_A);
        tbStatus_A.setOnClickListener(this);

        hd_Line_Using = (TextView) findViewById(R.id.hd_Line_Using);
        hd_Line_Using.setOnClickListener(this);

        tbStatus_LU = (TextView) findViewById(R.id.tbStatus_LU);
        tbStatus_LU.setOnClickListener(this);

        hd_Problem = (TextView) findViewById(R.id.hd_Problem);
        hd_Problem.setOnClickListener(this);

        tbStatus_P = (TextView) findViewById(R.id.tbStatus_P);
        tbStatus_P.setOnClickListener(this);

        hd_Warehouse_Location = (TextView) findViewById(R.id.hd_Warehouse_Location);
        hd_Warehouse_Location.setOnClickListener(this);

        tbStatus_WL = (TextView) findViewById(R.id.tbStatus_WL);
        tbStatus_WL.setOnClickListener(this);

        hd_LineStorage = (TextView) findViewById(R.id.hd_LineStorage);
        hd_LineStorage.setOnClickListener(this);

        tbStatus_LS = (TextView) findViewById(R.id.tbStatus_LS);
        tbStatus_LS.setOnClickListener(this);

        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);

        //Set Date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd    HH:mm");
        String currentDateandTime = sdf.format(new Date());
        tvDate = (TextView)(findViewById(R.id.tvDate));
        tvDate.setText(currentDateandTime);

        //Search All
        COUNT_LINE_USING  = 0; COUNT_PROBLEM = 0; COUNT_WL = 0; COUNT_LineSto = 0;
        ShowDataToGridView(data, "ALL");

        tbStatus_LU.setText(String.valueOf(COUNT_LINE_USING));
        tbStatus_P.setText(String.valueOf(COUNT_PROBLEM));
        tbStatus_LS.setText(String.valueOf(COUNT_LineSto));
        tbStatus_WL.setText(String.valueOf(COUNT_WL));

        tbStatus_A.setText(String.valueOf(COUNT_LINE_USING + COUNT_PROBLEM + COUNT_WL + COUNT_LineSto));

    }
    //endregion


    //region FUNCTIONS

        public String NullOrString(String str, String charSpecific){
            return (charSpecific).equals(str)?"":str;
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.btnSearch:
                    String strSearch = ((EditText)(findViewById(R.id.etInfo))).getText().toString();
                    SearchData(data, strSearch);

                    break;

                case R.id.hd_All:
                case R.id.tbStatus_A:
                    ShowDataToGridView(data, "ALL");
                    break;

                case R.id.hd_Line_Using:
                case R.id.tbStatus_LU:

                    ShowDataToGridView(data, "3");
                    break;

                case R.id.hd_Problem:
                case R.id.tbStatus_P:
                    ShowDataToGridView(data, "4");
                    break;

                case R.id.hd_Warehouse_Location:
                case R.id.tbStatus_WL:
                    ShowDataToGridView(data, "1");
                    break;

                case R.id.hd_LineStorage:
                case R.id.tbStatus_LS:
                    ShowDataToGridView(data, "2");
                    break;


                default:
                    break;
            }

        }

        //region GRID
        public void GetDataByStatus(int status){

        }

        public String[][] GetDataBySearchInfo(int status){
            String[][] data = null;

            return data;
        }



    public void ShowDataToGridView(final String[][] data, final String status){

            final int count = data.length;
            final int screenWidth = getResources().getDisplayMetrics().widthPixels;

            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                    TableLayout scrollablePart = (TableLayout) findViewById(R.id.grdScanAccept);
                    scrollablePart.removeAllViews();
                    int index = 0;

                    for (int i = 0; i < count; i++) {


                        //CHECK CONDITION
                        if(data[i][3].equals(status) || status.equals("ALL")){
                            index++;

                            final TableRow row = new TableRow(getApplication());
                            row.setLayoutParams(wrapWrapTableRowParams);
                            row.setGravity(Gravity.CENTER);
                            row.setBackgroundColor(Color.LTGRAY);

                            //SEQ
                            row.addView(makeTableColWithText(String.valueOf(index), scrollableColumnWidths[0], fixedRowHeight, 0));

                            //Mold ID
                            row.addView(makeTableColWithText(data[i][0], 7, fixedRowHeight, 1));

                            //Mold Name
                            row.addView(makeTableColWithText(data[i][1], 27, fixedRowHeight, -1));

                            //Status
                            //row.addView(makeTableColWithText(data[i][2], 8, fixedRowHeight, -1));
                            if(data[i][3].equals("1")){
                                ImageView view = new ImageView(getApplicationContext());
                                view.setImageResource(R.drawable.status_1_j1);

                                row.addView(view);
                                view.getLayoutParams().width = WIDTH_IMAGE;
                                view.getLayoutParams().height = HEIGHT_IMAGE;
                                view.setPadding(2,2,2,2);

                                view.setBackgroundColor(0xffffffff);

                                TableRow.LayoutParams params = new TableRow.LayoutParams(WIDTH_IMAGE, HEIGHT_IMAGE);
                                params.setMargins(1, 1, 1, 1);
                                view.setLayoutParams(params);

                                COUNT_WL++;

                                //Line Using
                                row.addView(makeTableColWithText("", WIDTH_Line_Using, fixedRowHeight, -1));
                                //Problem
                                row.addView(makeTableColWithText("", WIDTH_Problem, fixedRowHeight, -1));
                                //Line Storage
                                row.addView(makeTableColWithText("", WIDTH_Line_Storage, fixedRowHeight, -1));
                                //Warehouse Location
                                row.addView(makeTableColWithText(data[i][2], WIDTH_Warehouse_Location, fixedRowHeight, -1));
                            }else if(data[i][3].equals("2")){
                                ImageView view = new ImageView(getApplicationContext());
                                view.setImageResource(R.drawable.status_2_j1);

                                row.addView(view);
                                view.getLayoutParams().width = WIDTH_IMAGE;
                                view.getLayoutParams().height = HEIGHT_IMAGE;
                                view.setPadding(2,2,2,2);

                                view.setBackgroundColor(0xffffffff);

                                TableRow.LayoutParams params = new TableRow.LayoutParams(WIDTH_IMAGE, HEIGHT_IMAGE);
                                params.setMargins(1, 1, 1, 1);
                                view.setLayoutParams(params);

                                COUNT_LineSto++;

                                //Line Using
                                row.addView(makeTableColWithText("", WIDTH_Line_Using, fixedRowHeight, -1));
                                //Problem
                                row.addView(makeTableColWithText("", WIDTH_Problem, fixedRowHeight, -1));
                                //Line Storage
                                row.addView(makeTableColWithText(data[i][2], WIDTH_Line_Storage, fixedRowHeight, -1));
                                //Warehouse Location
                                row.addView(makeTableColWithText("", WIDTH_Warehouse_Location, fixedRowHeight, -1));
                            }else if(data[i][3].equals("3")){
                                ImageView view = new ImageView(getApplicationContext());
                                view.setImageResource(R.drawable.status_3_j1);

                                row.addView(view);
                                view.getLayoutParams().width = WIDTH_IMAGE;
                                view.getLayoutParams().height = HEIGHT_IMAGE;
                                view.setPadding(2,2,2,2);

                                view.setBackgroundColor(0xffffffff);

                                TableRow.LayoutParams params = new TableRow.LayoutParams(WIDTH_IMAGE, HEIGHT_IMAGE);
                                params.setMargins(1, 1, 1, 1);
                                view.setLayoutParams(params);

                                COUNT_LINE_USING++;

                                //Line Using
                                row.addView(makeTableColWithText(data[i][2], WIDTH_Line_Using, fixedRowHeight, -1));
                                //Problem
                                row.addView(makeTableColWithText("", WIDTH_Problem, fixedRowHeight, -1));
                                //Line Storage
                                row.addView(makeTableColWithText("", WIDTH_Line_Storage, fixedRowHeight, -1));
                                //Warehouse Location
                                row.addView(makeTableColWithText("", WIDTH_Warehouse_Location, fixedRowHeight, -1));
                            }else if(data[i][3].equals("4")){
                                ImageView view = new ImageView(getApplicationContext());
                                view.setImageResource(R.drawable.status_4_j1);

                                row.addView(view);
                                view.getLayoutParams().width = WIDTH_IMAGE;
                                view.getLayoutParams().height = HEIGHT_IMAGE;
                                view.setPadding(2,2,2,2);

                                view.setBackgroundColor(0xffffffff);

                                TableRow.LayoutParams params = new TableRow.LayoutParams(WIDTH_IMAGE, HEIGHT_IMAGE);
                                params.setMargins(1, 1, 1, 1);
                                view.setLayoutParams(params);

                                COUNT_PROBLEM++;

                                //Line Using
                                row.addView(makeTableColWithText("", WIDTH_Line_Using, fixedRowHeight, -1));
                                //Problem
                                row.addView(makeTableColWithText(data[i][2], WIDTH_Problem, fixedRowHeight, -1));
                                //Line Storage
                                row.addView(makeTableColWithText("", WIDTH_Line_Storage, fixedRowHeight, -1));
                                //Warehouse Location
                                row.addView(makeTableColWithText("", WIDTH_Warehouse_Location, fixedRowHeight, -1));
                            }


                            //Shot
                            String _shot = CNumber.customFormat("###,###.###", data[i][6]);
                            row.addView(makeTableColWithText(_shot, WIDTH_Shot, fixedRowHeight, 1));

                            //Actual Shot
                            String _Actual_Shot = CNumber.customFormat("###,###.###", data[i][7]);
                            row.addView(makeTableColWithText(_Actual_Shot, WIDTH_Actual_Shot, fixedRowHeight, 1));

                            //Shot bal
                            String _Shot_bal = CNumber.customFormat("###,###.###", data[i][8]);
                            row.addView(makeTableColWithText(_Shot_bal, WIDTH_ShotBal, fixedRowHeight, 1));
                            //QC Result
                            Float percent = (Float.valueOf(data[i][9])*100); //.valueOf(data[i][9])*100;
                            DecimalFormat df = new DecimalFormat();
                            df.setMaximumFractionDigits(2);

                            row.addView(makeTableColWithText(df.format(percent) + "%", WIDTH_QCResult, fixedRowHeight, 1));

                            scrollablePart.addView(row);
                        }
                    }




                }
            });
        }

    int orangeColor = Color.rgb(255,99,71);
    String Item_Code = "";
    int indexPrevious = -1;
    public void SearchData(final String[][] data, final String strSearch){

        final int count = data.length;
        final int screenWidth = getResources().getDisplayMetrics().widthPixels;
/*
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {*/
                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                final TableLayout scrollablePart = (TableLayout) findViewById(R.id.grdScanAccept);
                scrollablePart.removeAllViews();
                int index = 0;
                for (int i = 0; i < count; i++) {


                    //CHECK CONDITION
                    if(data[i][0].toLowerCase().indexOf(strSearch.toLowerCase()) != -1
                            ||data[i][1].toLowerCase().indexOf(strSearch.toLowerCase()) != -1
                            ||data[i][2].toLowerCase().indexOf(strSearch.toLowerCase()) != -1
                            || strSearch.length() == 0){
                        index++;

                        final TableRow row = new TableRow(getApplication());
                        row.setLayoutParams(wrapWrapTableRowParams);
                        row.setGravity(Gravity.CENTER);
                        row.setBackgroundColor(Color.LTGRAY);

                        //SEQ
                        row.addView(makeTableColWithText(String.valueOf(index), WIDTH_SEQ, fixedRowHeight, 0));
                        //Mold ID
                        row.addView(makeTableColWithText(data[i][0], WIDTH_Mold_ID, fixedRowHeight, 1));
                        //Mold Name
                        row.addView(makeTableColWithText(data[i][1], WIDTH_Mold_Name, fixedRowHeight, -1));
                        //Status
                        //row.addView(makeTableColWithText(data[i][2], 8, fixedRowHeight, -1));
                        if(data[i][3].equals("1")){
                            ImageView view = new ImageView(getApplicationContext());
                            view.setImageResource(R.drawable.status_1_j1);

                            row.addView(view);
                            view.getLayoutParams().width = WIDTH_IMAGE;
                            view.getLayoutParams().height = HEIGHT_IMAGE;
                            view.setPadding(2,2,2,2);

                            view.setBackgroundColor(0xffffffff);

                            TableRow.LayoutParams params = new TableRow.LayoutParams(WIDTH_IMAGE, HEIGHT_IMAGE);
                            params.setMargins(1, 1, 1, 1);
                            view.setLayoutParams(params);

                            COUNT_WL++;

                            //Line Using
                            row.addView(makeTableColWithText("", WIDTH_Line_Using, fixedRowHeight, -1));
                            //Problem
                            row.addView(makeTableColWithText("", WIDTH_Problem, fixedRowHeight, -1));
                            //Line Storage
                            row.addView(makeTableColWithText("", WIDTH_Line_Storage, fixedRowHeight, -1));
                            //Warehouse Location
                            row.addView(makeTableColWithText(data[i][2], WIDTH_Warehouse_Location, fixedRowHeight, -1));
                        }else if(data[i][3].equals("2")){
                            ImageView view = new ImageView(getApplicationContext());
                            view.setImageResource(R.drawable.status_2_j1);

                            row.addView(view);
                            view.getLayoutParams().width = WIDTH_IMAGE;
                            view.getLayoutParams().height = HEIGHT_IMAGE;
                            view.setPadding(2,2,2,2);

                            view.setBackgroundColor(0xffffffff);

                            TableRow.LayoutParams params = new TableRow.LayoutParams(WIDTH_IMAGE, HEIGHT_IMAGE);
                            params.setMargins(1, 1, 1, 1);
                            view.setLayoutParams(params);

                            COUNT_LineSto++;

                            //Line Using
                            row.addView(makeTableColWithText("", WIDTH_Line_Using, fixedRowHeight, -1));
                            //Problem
                            row.addView(makeTableColWithText("", WIDTH_Problem, fixedRowHeight, -1));
                            //Line Storage
                            row.addView(makeTableColWithText(data[i][2], WIDTH_Line_Storage, fixedRowHeight, -1));
                            //Warehouse Location
                            row.addView(makeTableColWithText("", WIDTH_Warehouse_Location, fixedRowHeight, -1));
                        }else if(data[i][3].equals("3")){
                            ImageView view = new ImageView(getApplicationContext());
                            view.setImageResource(R.drawable.status_3_j1);

                            row.addView(view);
                            view.getLayoutParams().width = WIDTH_IMAGE;
                            view.getLayoutParams().height = HEIGHT_IMAGE;
                            view.setPadding(2,2,2,2);

                            view.setBackgroundColor(0xffffffff);

                            TableRow.LayoutParams params = new TableRow.LayoutParams(WIDTH_IMAGE, HEIGHT_IMAGE);
                            params.setMargins(1, 1, 1, 1);
                            view.setLayoutParams(params);

                            COUNT_LINE_USING++;

                            //Line Using
                            row.addView(makeTableColWithText(data[i][2], WIDTH_Line_Using, fixedRowHeight, -1));
                            //Problem
                            row.addView(makeTableColWithText("", WIDTH_Problem, fixedRowHeight, -1));
                            //Line Storage
                            row.addView(makeTableColWithText("", WIDTH_Line_Storage, fixedRowHeight, -1));
                            //Warehouse Location
                            row.addView(makeTableColWithText("", WIDTH_Warehouse_Location, fixedRowHeight, -1));
                        }else if(data[i][3].equals("4")){
                            ImageView view = new ImageView(getApplicationContext());
                            view.setImageResource(R.drawable.status_4_j1);

                            row.addView(view);
                            view.getLayoutParams().width = WIDTH_IMAGE;
                            view.getLayoutParams().height = HEIGHT_IMAGE;
                            view.setPadding(2,2,2,2);

                            view.setBackgroundColor(0xffffffff);

                            TableRow.LayoutParams params = new TableRow.LayoutParams(WIDTH_IMAGE, HEIGHT_IMAGE);
                            params.setMargins(1, 1, 1, 1);
                            view.setLayoutParams(params);

                            COUNT_PROBLEM++;

                            //Line Using
                            row.addView(makeTableColWithText("", WIDTH_Line_Using, fixedRowHeight, -1));
                            //Problem
                            row.addView(makeTableColWithText(data[i][2], WIDTH_Problem, fixedRowHeight, -1));
                            //Line Storage
                            row.addView(makeTableColWithText("", WIDTH_Line_Storage, fixedRowHeight, -1));
                            //Warehouse Location
                            row.addView(makeTableColWithText("", WIDTH_Warehouse_Location, fixedRowHeight, -1));
                        }


                        //Shot
                        String _shot = CNumber.customFormat("###,###.###", data[i][6]);
                        row.addView(makeTableColWithText(_shot, WIDTH_Shot, fixedRowHeight, 1));

                        //Actual Shot
                        String _Actual_Shot = CNumber.customFormat("###,###.###", data[i][7]);
                        row.addView(makeTableColWithText(_Actual_Shot, WIDTH_Actual_Shot, fixedRowHeight, 1));

                        //Shot bal
                        String _Shot_bal = CNumber.customFormat("###,###.###", data[i][8]);
                        row.addView(makeTableColWithText(_Shot_bal, WIDTH_ShotBal, fixedRowHeight, 1));

                        //QC Result
                        Float percent = (Float.valueOf(data[i][9])*100); //.valueOf(data[i][9])*100;
                        DecimalFormat df = new DecimalFormat();
                        df.setMaximumFractionDigits(2);

                        row.addView(makeTableColWithText(df.format(percent) + "%", WIDTH_QCResult, fixedRowHeight, 1));


                        /*
                        //SET LISTENER
                        row.setOnClickListener(new View.OnClickListener() {
                            TextView tvTeamp;

                            @Override
                            public void onClick(View v) {
                                alertToastShort("<<<<   onClick ---------->>>> ");

                                TableRow tr1 = (TableRow) v;
                                tvTeamp = (TextView) tr1.getChildAt(0); //SLIP_NO

                                for (int i = 0; i < tr1.getChildCount(); i++) {
                                    if(i!=3){
                                        tvTeamp = (TextView) tr1.getChildAt(i);
                                        tvTeamp.setTextColor(orangeColor);
                                        if (i == 1) {
                                            Item_Code = tvTeamp.getText().toString();
                                        }
                                    }else{
                                        alertToastShort("Image ---------->>>> ");
                                    }

                                }

                                if (indexPrevious >= 0
                                        && indexPrevious != scrollablePart.indexOfChild(v)) {
                                    TableRow trP = (TableRow) scrollablePart.getChildAt(indexPrevious);
                                    for (int k = 0; k < trP.getChildCount(); k++) {
                                        tvTeamp = (TextView) trP.getChildAt(k);
                                        tvTeamp.setTextColor(Color.BLACK);
                                    }
                                }
                                //Update indexPrevious
                                indexPrevious = scrollablePart.indexOfChild(v);
                            }
                        });
*/
                        scrollablePart.addView(row);
                    }
                }
            }
    /*    });
    }
*/
        public void CallDataBase(String query){

        }
        //endregion

        //endregion


        //region REVIEW LATER


    //endregion


}
