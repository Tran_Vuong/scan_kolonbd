package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

import gw.genumobile.com.R;
import gw.genumobile.com.interfaces.GridListView;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.services.HandlerWebService;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.AppConfig;

public class DevideBC_V2 extends gwFragment implements View.OnClickListener {
    protected Boolean flagUpload = true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;
    private SharedPreferences mappingPrefs;

    Queue queue = new LinkedList();
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    String data[] = new String[0], process_type="01";
    String sql = "", scan_date = "", scan_time = "", unique_id = "",pallet_pk="";
    EditText bc,            currentBC ,  _edItemCode,    _edQty,           _edLotNo,    edt_item_code,            edt_item_name,edt_remark,edt_dofing_no,    edt_qty,            edt_weight,edt_grade,    edt_qty1,            edt_weight1;
    TextView recyclableTextView, _txtError, _txtTotalGridBot, _txtTotalQtyBot, _txtTotalGridMid,txtprocess_type;
    int pkParent = 0;
    float totalqty=0,totalweight=0;
     Button btnViewStt, btnList, btnDelAll, btnDelBC, btnMakeSlip, btnListBot,btn_new,btn_save,btn_view,btn_bclist,btn_view1;
    View rootView;
    TableLayout grdChild;
    String[] lableinfo = new  String[0];
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_devide, container, false);
        gwMActivity.getWindow().setSoftInputMode(1);
        //lock screen
        //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mappingPrefs = gwActivity.getSharedPreferences("MappingConfig", gwActivity.MODE_PRIVATE);
        //
        InitActivity();
        OnShowGridHeader();
        OnShowChild();
        init_color();
        bc.setFocusable(true);
        bc.requestFocus();
        bc.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        _txtError = (TextView) rootView.findViewById(R.id.txtError);
                        _txtError.setTextColor(Color.RED);
                        _txtError.setTextSize((float) 16.0);
                        if (bc.getText().toString().equals("")) {
                            _txtError.setText("Pls Scan barcode!");
                            bc.requestFocus();
                        } else {
                            OnSaveData();
                        }
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // gwMActivity is for backspace
                    bc.clearFocus();
                    Thread.interrupted();
                    //finish();
                    //System.exit(0);
                    //Log.e("IME_TEST", "BACK VIRTUAL");

                }
                return false;
            }
        });
        bc.requestFocus();
        return rootView;
    }

    protected void InitActivity() {
      //  HandlerWebService.SetConfig (appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""),"");
        bc = (EditText) rootView.findViewById(R.id.ed_BC);
        currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
        _edItemCode = (EditText) rootView.findViewById(R.id.ed_ItemCode);
        _edQty = (EditText) rootView.findViewById(R.id.ed_Qty);
        _edLotNo = (EditText) rootView.findViewById(R.id.edt_lot_no);
        grdChild= (TableLayout) rootView.findViewById(R.id.grdChild);
        edt_item_code = (EditText) rootView.findViewById(R.id.edt_item_code);
        edt_item_name = (EditText) rootView.findViewById(R.id.edt_item_name);
        edt_qty      = (EditText) rootView.findViewById(R.id.edt_qty);
        edt_qty1 = (EditText) rootView.findViewById(R.id.edt_qty1);
        edt_weight = (EditText) rootView.findViewById(R.id.edt_weight);
        edt_weight1 = (EditText) rootView.findViewById(R.id.edt_weight1);
        edt_remark = (EditText) rootView.findViewById(R.id.edt_remark);
        edt_grade = (EditText) rootView.findViewById(R.id.edt_grade);

        edt_dofing_no = (EditText) rootView.findViewById(R.id.edt_dofing_no);
        txtprocess_type = (TextView) rootView.findViewById(R.id.txtprocess_type);

        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setTextColor(Color.RED);
        _txtError.setTextSize((float) 16.0);

        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);
        btn_new=(Button) rootView.findViewById(R.id.btn_new);
        btn_new.setOnClickListener(this);
        btn_save=(Button) rootView.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);
        btn_view=(Button) rootView.findViewById(R.id.btn_view);
        btn_view.setOnClickListener(this);
        btn_view1=(Button) rootView.findViewById(R.id.btn_view1);
        btn_view1.setOnClickListener(this);
        btn_bclist=(Button) rootView.findViewById(R.id.btn_bclist);
        btn_bclist.setOnClickListener(this);
        edt_weight1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if(edt_weight1.getText().toString().trim().length() > 0 ) {
                    try {
                        float val = totalweight - Float.parseFloat(edt_weight1.getText().toString());
                        edt_weight.setText(String.valueOf(val));
                    } catch (NumberFormatException e) {
                        edt_weight.setText(String.valueOf(totalweight));
                    }
                }
            }
        });
        edt_qty1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edt_qty1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    TextView tv;
                    if(hasFocus){
                        for (int i = 0; i < grdChild.getChildCount(); i++) {
                            TableRow row = (TableRow) grdChild.getChildAt(i);
                            int xx = row.getChildCount();
                            for (int r = 0; r < xx; r++) {
                                tv = (TextView) row.getChildAt(r);
                                tv.setTextColor(Color.rgb(0, 0, 0));
                            }
                        }

                    }
                    else {
                        float in_val = Float.parseFloat(edt_qty1.getText().toString());
                        if (in_val >= totalqty) {
                            gwMActivity.alertToastLong("Can't Input number more than " + totalqty);
                            return;
                        }
                        float val = totalqty - in_val;
                        edt_qty.setText(String.valueOf(val));

                        for (int i = 0; i < in_val; i++) {
                            TableRow row = (TableRow) grdChild.getChildAt(i);
                            int xx = row.getChildCount();
                            for (int r = 0; r < xx; r++) {
                                tv = (TextView) row.getChildAt(r);
                                tv.setTextColor(Color.rgb(255, 99, 71));
                            }
                        }
                    }
                }
                catch (NumberFormatException e)
                {
                    edt_qty.setText(String.valueOf(totalqty));
                }
            }
        });
    }

    private void init_color() {
        //region ------Set color tablet----
//        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
//        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
//        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
//        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdParent);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdChild);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

       //****************************************************************************
    public void OnSaveData() {
        _txtError.setText("");
        if (bc.getText().equals("")) {
            _txtError.setText("Pls Scan barcode!");
            return;
            //bc1.requestFocus();
        }

        String str_scan = bc.getText().toString().toUpperCase();
        int l_lenght = str_scan.length();
        //String str_bc=str_scan.substring(1, l_lenght);
        //System.out.println("str_bc " + str_bc);
        Log.e("para str_bc ", str_scan);

        //region  Parent YN = Y
        if (mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {

        }
        //endregion
        else {
            try {
                if (str_scan.equals("")) {
                    _txtError.setText("Vui lòng scan barcode!");
                    return;
                }

                    JDataTable dt = GetDataOnServer1("Parent", str_scan+"|"+deviceID);
                    scan_date = CDate.getDateyyyyMMdd();
                    scan_time = CDate.getDateYMDHHmmss();
                    if(dt==null || dt.totalrows <=0) {
                        alertRingMedia2();
                        _txtError.setText("Please scan Pallet first!");
                        bc.getText().clear();
                        bc.requestFocus();
                        return;
                    }
                    else{
                        Init_Form();
                        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        db.execSQL("DELETE FROM INV_TR where  tr_type='" + formID + "'");

                        process_type = dt.records.get(0).get("process_type").toString();
                        pallet_pk = dt.records.get(0).get("pk").toString();
                        currentBC.setText(dt.records.get(0).get("item_bc").toString());
                        _edQty.setText(dt.records.get(0).get("bc_number").toString());
                        _edItemCode.setText(dt.records.get(0).get("item_code").toString());
                        _edLotNo.setText(dt.records.get(0).get("lot_no").toString());
                        edt_item_code.setText(dt.records.get(0).get("item_code").toString());
                        edt_item_name.setText(dt.records.get(0).get("item_name").toString());
                        edt_qty.setText(dt.records.get(0).get("bc_number").toString());
                        edt_weight.setText(dt.records.get(0).get("qty").toString());
                        edt_grade.setText(dt.records.get(0).get("grade").toString());
                        totalqty=+ Float.parseFloat(dt.records.get(0).get("bc_number").toString());
                        totalweight =Float.parseFloat(dt.records.get(0).get("qty").toString());
                        String item_pk= dt.records.get(0).get("item_pk").toString();
                        if(process_type.equals("02")) {
                            edt_dofing_no.setEnabled(true);
                            txtprocess_type.setText("R/C");
                        }
                        else if(process_type.equals("01")) {
                            edt_dofing_no.setEnabled(false);
                            txtprocess_type.setText("YARN");
                        }

                        bc.getText().clear();
                        bc.requestFocus();
                        dt = GetDataOnServer1("Child",  item_pk+ "|" + pallet_pk  + "|" + deviceID + "|" + bsUserID);
                        String _bc_Type = "C";//child
                        for (int i = 0; i < dt.totalrows; i++) {
                            try {

                       sql = "INSERT INTO INV_TR(ITEM_BC,TR_QTY,PARENT_PK,TR_PARENT_ITEM_PK,CHILD_PK,ITEM_CODE,ITEM_NAME,TR_LOT_NO,SCAN_DATE,SCAN_TIME,SENT_YN,BC_TYPE,MAPPING_YN,TR_TYPE,TR_ITEM_PK,PROCESS_TYPE,SLIP_NO)"
                                + "VALUES('"
                               + dt.records.get(i).get("item_bc").toString() + "','"
                               + dt.records.get(i).get("qty").toString() + "','"
                               + pallet_pk + "','"
                               + dt.records.get(i).get("item_pk").toString() + "','"
                               + dt.records.get(i).get("pk").toString() + "','"
                               + dt.records.get(i).get("item_code").toString() + "','"
                               + dt.records.get(i).get("item_name").toString() + "','"
                               + dt.records.get(i).get("lot_no").toString() + "','"
                               + scan_date + "','"
                               + scan_time + "','"
                               + "N" + "','"
                               + _bc_Type + "','"
                               + "N" + "','"
                               + formID + "','"
                               + dt.records.get(i).get("item_pk").toString() + "','"
                               + process_type + "','"
                               + "-" + "')";
                        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        db.execSQL(sql);
                            } catch (Exception ex) {
                                Log.e("Save error:", ex.toString());
                            }
                        }
                        db.close();
                        _txtError.setText("Save success.");
                        bc.getText().clear();
                        bc.requestFocus();
                        OnShowChild();
                    }

            } catch (Exception ex) {
                Log.e("Save error:", ex.toString());

            }
        }
    }

    //****************************************************************************
    public JDataTable GetDataOnServer1(String type, String para) {
        JDataTable dt = null;
        String proc="";
        //HandlerWebService.SetConfig (appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""),"");
        if (type.endsWith("Parent")) {
                proc="LG_MPOS_DEVIDE_GET_PARENT_BC_2";
        }
        else
        {
            proc="LG_MPOS_DEVIDE_GET_CHILD_BC_2";
        }
        try {
            // JResultData jresultkq = HandlerWebService.GetData(proc, 1, para);

            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String l_para = "1,"+proc+",1," + para ;
            JResultData jresultkq =     networkThread.execute(l_para).get();
            if(jresultkq ==null)
            {   gwMActivity.alertToastLong("Pls check internet!!" + "- " + para);
                return  null;
            }
            if (jresultkq.getTotals() > 0) {
                List<JDataTable> jdt = jresultkq.getListODataTable();
                dt = jdt.get(0);
            }
        } catch (Exception ex) {
        gwMActivity.alertToastLong(type + ": " + ex.getMessage());
          }
        return  dt;
    }

    public void onDelAll(View view) {
        if (!checkRecordGridView(R.id.grdParent)
                && !checkRecordGridView(R.id.grdChild)) return;

        String title = "Confirm Delete...";
        String mess = "Are you sure you want to delete all";
        alertDialogYN(title, mess, "onDelAll");
    }

    public void ProcessDeleteAll() {
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));

        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String para = "";
            boolean flag = true;

            cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "'", null);
            int j = 0;

            if (cursor.moveToFirst()) {
                data = new String[1];
                do {

                    flag = true;
                    for (int k = 0; k < cursor.getColumnCount(); k++) {
                        if (para.length() <= 0) {
                            if (cursor.getString(k) != null)
                                para += cursor.getString(k);
                            else
                                para += "|";
                        } else {
                            if (flag == true) {
                                if (cursor.getString(k) != null) {
                                    para += cursor.getString(k);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor.getString(k) != null)
                                    para += "|" + cursor.getString(k);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "|" + formID;
                    para += "|delete";
                    para += "*|*";
                } while (cursor.moveToNext());

                para += "|!LG_MPOS_UPD_INV_TR_DEL";
                data[j] = para;
            }

        } catch (Exception ex) {
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity, this);
            task.execute(data);
        } else {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }


    }

    public void onDelAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();


            } else {
                //db.execSQL("DELETE FROM INV_TR where STATUS NOT IN('000') and TR_TYPE ='"+formID+"';");
                db.execSQL("DELETE FROM INV_TR where TR_TYPE ='" + formID + "';");
            }
        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            currentBC = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
            _edQty = (EditText) rootView.findViewById(R.id.ed_Qty);
            _edItemCode = (EditText) rootView.findViewById(R.id.ed_ItemCode);
            _txtError = (TextView) rootView.findViewById(R.id.txtError);
            pkParent = 0;
            OnShowChild();
            Init_Form();
            gwMActivity.alertToastShort("Delete success..");
        }
    }
public  void Init_Form()
{
    pallet_pk="";
    currentBC.setText("");
    _edQty.setText("");
    _edItemCode.setText("");
    _txtError.setText("");
    _edLotNo.setText("");
     edt_item_code.setText("");
    edt_item_name.setText("");
    edt_qty.setText("");
    edt_weight.setText("");
    edt_qty1.setText("");
    edt_weight1.setText("0");
    edt_dofing_no.setText("");
    txtprocess_type.setText("");
    ((TextView)rootView.findViewById(R.id.txtPallet)).setText("");
    ((TextView)rootView.findViewById(R.id.txtPallet1)).setText("");
    lableinfo = new  String[0];
}
    public void onSave(View view) {

        if (!checkRecordGridView(R.id.grdChild)) return;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Mapping...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to Save Mapping");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                //Button btnMapping = (Button) rootView.findViewById(R.id.btnUploadMapping);
                //btnMapping.setVisibility(View.GONE);
                ProcessMapping();
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    private  void LoadDate(View v){
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                //updateLabel();
                String myFormat = "yyyy/MM/dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            }
        };
        new DatePickerDialog(gwMActivity, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    public void ProcessMapping() {
        unique_id = CDate.getDateyyyyMMddhhmmss();
        if(edt_qty1.getText().toString().equals("") ||edt_weight.getText().toString().equals("") )
        {
            gwMActivity.alertToastLong("Please Input QTY and WEIGHT!!");
            return;
        }
        float val=  Float.parseFloat(edt_weight.getText().toString());
        float val1=  Float.parseFloat(edt_weight1.getText().toString());
        if(totalweight !=val+val1)
        {
            gwMActivity.alertToastLong("Please check weight! Not eq!!");
                return;
        }
        //region PARENT YN=Y
        if (mappingPrefs.getBoolean("parentYN", Boolean.FALSE)) {

        }
        //endregion

        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("SELECT PARENT_PK,TR_PARENT_ITEM_PK,EXECKEY" +    //,PARENT_PK,TR_PARENT_ITEM_PK
                        "    FROM INV_TR " +
                        "    WHERE DEL_IF =0 AND BC_TYPE='C' AND TR_TYPE='" + formID + "' AND SENT_YN='N' AND MAPPING_YN='N' " +
                        " group by PARENT_PK,EXECKEY  ORDER BY PK ASC ", null);
                int count = cursor.getCount();
                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int parent_pk_ = Integer.valueOf(cursor.getString(cursor.getColumnIndex("PARENT_PK")));
                            hp.updateExeckeyDevide(formID, parent_pk_,  unique_id);

                            para += "|" + unique_id;
                        } else {
                            para += "|" + execkey;
                        }
                        para += "|" + edt_grade.getText();
                        para += "|" + edt_qty.getText();
                        para += "|" + edt_weight.getText();
                        para += "|" + edt_qty1.getText();
                        para += "|" + edt_weight1.getText();
                        para += "|" + edt_remark.getText();
                        para += "|" + _edLotNo.getText();
                        para += "|" + edt_dofing_no.getText();

                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";///multi row

                    } while (cursor.moveToNext());
                    para += "|!" + "LG_MPOS_PRO_DEVIDE_MSLIP_V2";
                    data[j++] = para;

                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("Upload Mapping :" + ex.getMessage());
                Log.e(" Error Upload Mapping :", ex.getMessage().toString());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity, this);
                task.execute(data);
            } else {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        }

    }
    public  void  OnNew(View view){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm New...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to make New Mapping");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                edt_item_code.getText().clear();
                edt_item_name.getText().clear();
                edt_qty.getText().clear();
                edt_remark.getText().clear();
                edt_weight.getText().clear();
                TableRow row = new TableRow(gwMActivity);
                TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
                scrollablePart.removeAllViews();
                ProcessDeleteAll();
                Init_Form();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }
    // show data scan in
    public  void  OnViewBC(View view,String seq){
        FragmentManager fm = gwMActivity.getSupportFragmentManager();

        Bundle args = new Bundle();
        if(lableinfo.length >0) {

            args.putString("type", "online");
            if(seq.equals("0"))
            args.putString("para1", lableinfo[5]);
            else  args.putString("para1", lableinfo[6]);
            //  args.putStringArray("LINE_DETAIL", line_detail);
            if(process_type.equals("01")) {
                DialogFragment dialogFragment = new MappingV2_Pop();
                dialogFragment.setArguments(args);
                dialogFragment.setTargetFragment(this, 1);
                dialogFragment.setCancelable(false);
                dialogFragment.show(fm.beginTransaction(), "dialog");
            }
            else if(process_type.equals("02")) {
                DialogFragment dialogFragment = new MappingV2_Pop3();
                dialogFragment.setArguments(args);
                dialogFragment.setTargetFragment(this, 1);
                dialogFragment.setCancelable(false);
                dialogFragment.show(fm.beginTransaction(), "dialog");
            }
        }
    }
    public  void  OnListBC(View view){
        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = new MappingV2_Pop2();
        Bundle args = new Bundle();
     //   args.putStringArray("para1",lableinfo);
        //  args.putStringArray("LINE_DETAIL", line_detail);

        dialogFragment.setArguments(args);
        dialogFragment.setTargetFragment(this, 1);
        dialogFragment.setCancelable(false);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    public void OnShowChild(String[]para) {
        lableinfo=para;
        ((TextView)rootView.findViewById(R.id.txtPallet)).setText("BC: "+lableinfo[3]);
        ((TextView)rootView.findViewById(R.id.txtPallet1)).setText("BC: "+lableinfo[4]);
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select PK,ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,MAPPING_YN,TR_LOT_NO,SLIP_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and BC_TYPE='C' AND TR_TYPE='" + formID + "' " +
                    " ORDER BY pk desc";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), 0, fixedRowHeight, 1));
                    if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y"))
                        row.addView(makeTableColWithText("YES", scrollableColumnWidths[2], fixedRowHeight, 0));
                    else
                        row.addView(makeTableColWithText("-", scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, -1));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(7); //SLIP_NO
                            String st_slipNO = tvSlipNo.getText().toString();

                            if (st_slipNO.equals("-")) {

                                TextView tv1 = (TextView) tr1.getChildAt(8); //PK
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255, 99, 71));
                                }

                                if (queue.size() > 0) {
                                    if (queue.contains(tv1.getText().toString())) {
                                        queue.remove(tv1.getText().toString());

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK);
                                        }
                                        duplicate = true;
                                    }
                                }
                                if (!duplicate)
                                    queue.add(tv1.getText().toString());
                            }
                            gwMActivity.alertToastShort(String.valueOf(queue.size()));

                        }
                    });
                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND BC_TYPE = 'C' AND TR_TYPE='" + formID + "'  ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowChild() {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdChild);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select PK,ITEM_BC, ITEM_CODE,TR_QTY,ITEM_NAME,MAPPING_YN,TR_LOT_NO,SLIP_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0  and BC_TYPE='C' AND TR_TYPE='" + formID + "' " +
                    " ORDER BY TR_LOT_NO asc";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                  //  String seq = String.valueOf(count - i);
                    String seq = String.valueOf(i+1);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), 0, fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));

                    if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y"))
                        row.addView(makeTableColWithText("YES", scrollableColumnWidths[2], fixedRowHeight, 0));
                    else
                    row.addView(makeTableColWithText("-", scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND BC_TYPE = 'C' AND TR_TYPE='" + formID + "'  ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 17.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");

            //edt_qty1.setText(String.valueOf(_qty));
           // edt_qty.setText(String.valueOf(totalqty -_qty) );
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
    ///////////////////////////////////////////////
    public void onListGridBot(View view) {
        if (!checkRecordGridView(R.id.grdChild)) return;

        Intent openNewActivity = new Intent(view.getContext(), GridListViewBot.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        //startActivity(openNewActivity);
        startActivityForResult(openNewActivity, REQUEST_CODE);
    }

    /**
     * Xử lý kết quả trả về ở đây
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiểm tra có đúng requestCode =REQUEST_CODE_INPUT hay không
        //Vì ta có thể mở Activity với những RequestCode khác nhau
        if (requestCode == REQUEST_CODE) {
            //Kiểm trả ResultCode trả về, cái này ở bên InputDataActivity truyền về
            OnShowChild();
            switch (resultCode) {
                case RESULT_CODE:

                    break;
            }
        }
    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[5]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Item Name", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Mapping", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[2], fixedHeaderHeight));
        scrollablePart.addView(row);
    }

    // end get data return array--use array set to grid view


    /////////////////////////
    // check data inv_tr
    public int Check_Exist_PK(String l_bc_item) {
        int countRow = 0;
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String countQuery = "SELECT PK FROM INV_TR where del_if=0 and TR_TYPE='" + formID + "' and ITEM_BC ='" + l_bc_item + "' ";
            // db = gwMActivity.getReadableDatabase();
            cursor = db.rawQuery(countQuery, null);
            // Check_Exist_PK=cursor.getCount();
            if (cursor != null && cursor.moveToFirst()) {
                countRow = cursor.getCount();
            }

        } catch (Exception ex) {
            gwMActivity.alertToastLong("Check_Exist_PK :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return countRow;

    }

    ////////////////////////////////////////////////
    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event

                if (_type.equals("onDelAll")) {
                    ProcessDeleteAll();
                }
                if (_type.equals("onMakeSlip")) {


                    //ProcessMakeSlip();

                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str = str_alert;

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            // onLoadFull();
            Intent i = new Intent(gwMActivity, AppConfig.class);
            i.putExtra("type", formID);
            i.putExtra("user", bsUserID);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onLoadFull() {
        Intent openNewActivity = new Intent(gwMActivity, GridListView.class);
        //send data into Activity
        openNewActivity.putExtra("type", "10");
        startActivity(openNewActivity);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:

                break;
            case R.id.btnViewStt:

                break;
            case R.id.btnSelectMid:


                break;
            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnList:


                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnMakeSlip:
                break;
            case R.id.btn_new:
                OnNew(view);
                break;
            case R.id.btn_save:
                onSave(view);
                break;
            case R.id.btn_view:
                OnViewBC(view,"0");
                break;
            case R.id.btn_view1:
                OnViewBC(view,"1");
                break;
            case R.id.btn_bclist:
                OnListBC(view);
                break;
            case R.id.edt_issue_date:
                LoadDate(view);
                break;

            default:
                break;
        }
    }
}
