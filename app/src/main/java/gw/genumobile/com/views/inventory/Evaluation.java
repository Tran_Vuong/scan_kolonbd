package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import gw.genumobile.com.fragments.DatePickerFragment;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;
import gw.genumobile.com.views.gwMainActivity;

public class Evaluation extends gwFragment implements View.OnClickListener {
    protected Boolean flagUpload=true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE=1;
    private static final String formID="19";
    Queue queue = new LinkedList();
    Queue queueMid = new LinkedList();

    SimpleDateFormat df;
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    Button _btnApprove,btOption1,btOption2,btOption3,btOption4,btnDelAll,btnMakeSlip,btOption5;
    TextView _txtDate, _txtError, _txtTT, _txtSent, _txtRemain,_txtTotalGridBot,_txtTotalQtyBot,_txtTotalGridMid,_txtTime;
    EditText myBC, bc2, code, name, lotNo;
    CheckBox ckbShowAll;
    public Spinner myLstWH,myLstLoc;

    String sql = "", tr_date = "",scan_date="",scan_time="",unique_id="", strDate = "";;
    String wh_pk = "",wh_name = "",wh_name_grid = "", loc_pk = "",line_pk = "",line_name="", slipType_pk = "",slipType_name="",approveYN="N";
    String  data[] = new String[0];
    int timeUpload=0;
    HashMap hashMapWH = new HashMap();
    HashMap hashMapLoc = new HashMap();
    HashMap hashMapSlipT = new HashMap();
    View rootView;

    CheckBox _doStart;

   
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_evaluation, container, false);
        //lock screen
      // this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        myBC        = (EditText) rootView.findViewById(R.id.editBC);
        myLstWH     = (Spinner) rootView.findViewById(R.id.lstWH);
        myLstLoc     = (Spinner) rootView.findViewById(R.id.lstLoc);
        ckbShowAll=(CheckBox) rootView.findViewById(R.id.ckbAll);

        _txtRemain   = (TextView) rootView.findViewById(R.id.txtRemain );
        _txtDate     = (TextView) rootView.findViewById(R.id.txtDate);
        _txtError  = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");


        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        _txtDate.setText(formattedDate);
        _txtTime=(TextView) rootView.findViewById(R.id.txtTime);

        btOption1 = (Button) rootView.findViewById(R.id.btOption1);
        btOption1.setOnClickListener(this);
        btOption2 = (Button) rootView.findViewById(R.id.btOption2);
        btOption2.setOnClickListener(this);
        btOption3 = (Button) rootView.findViewById(R.id.btOption3);
        btOption3.setOnClickListener(this);
        btOption4 = (Button) rootView.findViewById(R.id.btOption4);
        btOption4.setOnClickListener(this);
        btOption5 = (Button) rootView.findViewById(R.id.btOption5);
        btOption5.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);

        _doStart = (CheckBox)  rootView.findViewById(R.id.doStart);

        if(Measuredwidth <=600) {
            _txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);


          /*  // Gets linearlayout
            LinearLayout layout = (LinearLayout)rootView.findViewById(R.id.layoutContent);
            // Gets the layout params that will allow you to resize the layout
            LinearLayout.LayoutParams par = (LinearLayout.LayoutParams) layout.getLayoutParams();
            // Changes the height and width to the specified *pixels*
            par.height = 200;
            //par.width = Measuredwidth;
            layout.setLayoutParams(params);
            */
        }

        OnShowGridHeader();
        LoadWH();
        OnShowScanLog();
        OnShowScanAccept("002");
        CountSendRecord();
        init_color();
        myBC.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        OnSaveBC();
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // this is for backspace
                    myBC.clearFocus();
                    Thread.interrupted();
                   // finish();
                    //System.exit(0);
                    //Log.e("IME_TEST", "BACK VIRTUAL");

                }
                return false;
            }
        });

        timeUpload=hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload*1000);
        return rootView;
    }
    private Runnable updateDataToServer = new Runnable() {
        public void run()
        {
            if(flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload*1000);
        }
    };

    private  void init_color(){
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
//        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
//        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
//        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
//        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private void doStart() {

        if(!_doStart.isChecked()){
            return;
        }

        try {
            flagUpload = false;
            db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor2 = db2.rawQuery("select PK, ITEM_BC,TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK  from INV_TR where DEL_IF=0 and TR_TYPE='" + formID + "' and sent_yn = 'N' order by PK asc LIMIT 10", null);
            System.out.print("\n\n\n cursor2.count: " + String.valueOf(cursor2.getCount()));
            //data=new String [cursor2.getCount()];

            if (cursor2.moveToFirst()) {
                // Write your code here to invoke YES event

                //set value message
                _txtError = (TextView) rootView.findViewById(R.id.txtError);
                _txtError.setTextColor(Color.RED);
                _txtError.setText("");

                //data=new String [cursor2.getCount()];
                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                do {
                    flag = true;
                    for (int i = 0; i < cursor2.getColumnCount(); i++) {
                        if (para.length() <= 0) {
                            if (cursor2.getString(i) != null)
                                para += cursor2.getString(i) + "|";
                            else
                                para += "|";
                        } else {

                            if (flag == true) {
                                if (cursor2.getString(i) != null) {
                                    para += cursor2.getString(i);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor2.getString(i) != null)
                                    para += "|" + cursor2.getString(i);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "*|*";

                } while (cursor2.moveToNext());
                //////////////////////////
                para += "|!" + "LG_MPOS_UPL_EVALUATION";
                data[j++] = para;
                System.out.print("\n\n\n para upload stock out: " + para);
                Log.e("para upload stock out: ", para);

                if (CNetwork.isInternet(tmpIP, checkIP)) {
                    ServerAsyncTask task = new ServerAsyncTask(gwMActivity, this);
                    task.execute(data);
                    gwMActivity.alertToastShort("Send to Server");
                } else {
                    flagUpload = true;
                    gwMActivity.alertToastLong("Network is broken. Please check network again !");
                }
            } else {
                flagUpload = true;
            }
            cursor2.close();
            db2.close();
        }catch (Exception ex){
            System.out.print("\n\n\nException: 234 " + ex.getMessage());
        }
    }


    //region ------------------Delete-----------------------
    public  void onDeleteAllFinish(){
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(queue.size()>0)
            {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();

            }else{
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('002') or (STATUS='002' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='"+formID+"';");
            }
        }catch (Exception ex){
            Log.e("Error Delete All :", ex.getMessage());
        }
        finally {
            db.close();
            OnShowScanLog();
            OnShowScanAccept("002");
        }
    }
    //endregion

    public  void ProcessDeleteAll() {
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
        String para = "";
        boolean flag = true;
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "' and SLIP_NO='-' and STATUS ='002' AND SCAN_DATE > '" + date_previous + "'", null);
            int count = cursor.getCount();

            int j = 0;
            if (cursor.moveToFirst()) {
                data = new String[1];

                do {
                    flag = true;
                    for (int k = 0; k < cursor.getColumnCount(); k++) {
                        if (para.length() <= 0) {
                            if (cursor.getString(k) != null)
                                para += cursor.getString(k);
                            else
                                para += "|";
                        } else {
                            if (flag == true) {
                                if (cursor.getString(k) != null) {
                                    para += cursor.getString(k);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor.getString(k) != null)
                                    para += "|" + cursor.getString(k);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "|" + formID;
                    para += "|delete";
                    para += "*|*";
                } while (cursor.moveToNext());

                para += "|!LG_MPOS_UPD_INV_TR_DEL";
                data[j] = para;
                //System.out.print("\n\n\n para update||delete: " + para);
                Log.e("para update||delete: ", para);
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        } else {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }
    }

    private DatePickerFragment pickerDate;

        //region Make Slip 002
    //region MakeSlip
    public void onMakeSlip(View view){

        //Check het Barcode send to server
        if(checkRecordGridView(R.id.grdScanLog)){
            gwMActivity.alertToastLong( this.getResources().getString(R.string.tvAlertMakeSlip) );
            return;
        }
        //Check have value make slip
        if(hp.isMakeSlipStatus(formID,"002")==false) return;

        pickerDate = new DatePickerFragment() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {

                Calendar c = Calendar.getInstance();
                c.set(year, month, day);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                String formattedDate = sdf.format(c.getTime());

                strDate = formattedDate;
                //List lstDateTime = new ArrayList();
                //lstDateTime.add(0, strDate);

                Log.e("strDate",strDate);

            }

        };
        pickerDate.show(gwMActivity.getFragmentManager(), "Date");
        ///////////////////////////////////////////////////
        String title="Confirm Make Slip...";
        String mess="Are you sure you want to Make Slip ???";
        alertDialogYN(title,mess,"onMakeSlip");
    }

    public void ProcessMakeSlip() {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {

            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();


        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = " select  TR_ITEM_PK, TR_WH_IN_PK, TR_LINE_PK, TR_QTY, UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK,TLG_PO_PO_D_PK, SUPPLIER_PK,PO_NO,UNIT_PRICE " +
                        " from "+
                        " ( select  TR_ITEM_PK, SUM(TR_QTY) as TR_QTY, TR_LOT_NO, UOM, TR_WH_IN_PK, TR_LINE_PK, TLG_SA_SALEORDER_D_PK,TLG_PO_PO_D_PK, SUPPLIER_PK,PO_NO,UNIT_PRICE " +
                        " FROM INV_TR  WHERE DEL_IF = 0  AND SENT_YN = 'Y' AND TR_TYPE ='"+formID+"' AND STATUS='002' and SLIP_NO='-' "+
                        " GROUP BY TR_ITEM_PK,TR_LOT_NO, UOM, TR_WH_IN_PK, TR_LINE_PK, TLG_SA_SALEORDER_D_PK,TLG_PO_PO_D_PK, SUPPLIER_PK,PO_NO,UNIT_PRICE )";
                cursor = db.rawQuery(sql,null);

                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        /*
                        //get list child pk
                        String parent_pk=cursor.getString(cursor.getColumnIndex("PARENT_PK"));
                        //onPopAlert(parent_pk);
                        String lschild="";
                        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        cursor2 = db2.rawQuery("select CHILD_PK   from INV_TR where del_if=0 and CHILD_PK !=0 and PARENT_PK="+parent_pk,null);
                        int count1=cursor2.getCount();
                        if (cursor2.moveToFirst()) {
                            lschild = "";
                            do {

                                for (int col = 0; col < cursor2.getColumnCount(); col++) {
                                    if (lschild.length() <= 0) {
                                        if (cursor2.getString(col) != null)
                                            lschild += cursor2.getString(col);
                                        else
                                            lschild += "0";
                                    } else {
                                        if (cursor2.getString(col) != null)
                                            lschild += ";" + cursor2.getString(col);
                                        else
                                            lschild += ";";
                                    }
                                }
                            }while (cursor2.moveToNext());

                        }
                        db2.close();
                        cursor2.close();

                        ////////////////////
                        para += "|" +lschild;
                        */
                        para += "|" + strDate;
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";
                    }while (cursor.moveToNext());
                }
                para += "|!LG_MPOS_PRO_EVALUATION_MSLIP";
                System.out.print("\n\n ******para make slip income prod: " + para);
                data[j++] = para;
            }
            catch (Exception ex){
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP,checkIP)) {
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                task.execute(data);
            } else
            {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        }
    }

    //endregion
    public void alertDialogYN(String title,String mess,final String _type){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if(_type.equals("onApprove")){
                    //ProcessApprove();
                }
                if(_type.equals("onDelAll")){
                    ProcessDeleteAll();
                }
                if(_type.equals("onMakeSlip")){
                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    public void OnSaveBC(){
        if (myBC.getText().toString().equals("")) {
            _txtError.setTextColor(Color.RED);
            _txtError.setText("Pls Scan barcode!");
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        }
        else{
            String str_scanBC = myBC.getText().toString().toUpperCase();
            try
            {
                if(str_scanBC.length()>20) {
                    _txtError.setText("Barcode has length more 20 char !");
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                }

                boolean isExists= isExistBarcode(str_scanBC, formID);// check barcode exist in data
                if (isExists)// exist data
                {
                    alertRingMedia();
                    _txtError.setText("Barcode exist in database!");
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                }
                else
                {
                    db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                    scan_date=CDate.getDateyyyyMMdd();
                    scan_time = CDate.getDateYMDHHmmss();

                    db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                            + "VALUES('"
                            + str_scanBC + "','"
                            + wh_pk + "','"
                            + wh_name + "','"
                            + loc_pk + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "N" + "','"
                            + " " + "','"
                            + formID + "');");

                    _txtError.setText("Add success!");
                }
            }
            catch (Exception ex)
            {
                _txtError.setText("Save Error!!!");
                Log.e("OnSaveBC", ex.getMessage());
            }
            finally {
                db.close();
                myBC.getText().clear();
                myBC.requestFocus();
            }
        }
        OnShowScanLog();
        CountSendRecord();
    }

    public void LoadWH(){
        String l_para = "1,LG_MPOS_M010_GET_WH_USER," + bsUserPK+"|wh_ord";

        try{
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String dataWHGroup[][] = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < dataWHGroup.length; i++) {
                lstGroupName.add(dataWHGroup[i][1].toString());
                hashMapWH.put(i, dataWHGroup[i][0]);
            }

            if(dataWHGroup!= null && dataWHGroup.length > 0){
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstWH.setAdapter(dataAdapter);
                wh_name = myLstWH.getItemAtPosition(0).toString();
            }
        }catch(Exception ex){
            Log.e("Searh Error: ", ex.getMessage());
        }

        //Even Choose Group
        myLstWH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {

                wh_name = myLstWH.getItemAtPosition(i).toString();
                Log.e("WH_name",wh_name);
                Object pkGroup = hashMapWH.get(i);
                if(pkGroup!=null){
                    wh_pk = String.valueOf(pkGroup);
                    Log.e("WH_PK: ",wh_pk);
                    LoadLocation();
                    // if(!wh_pk.equals("0")){
                    //     GetLineByLineGroup(LINE_GROUP_PK);
                    //     TextView tv = (TextView)selectedItemView;
                    //    tv.setTextColor(Color.BLACK);
                    // }
                }else {
                    wh_pk="";
                    Log.e("WH_PK: ",wh_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }
    ///////////////////////////////////////////////
    private void LoadLocation() {
        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
            String l_para = "1,LG_MPOS_GET_LOC," + wh_pk;
            try {
                ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                String dtLocGroup[][] = networkThread.execute(l_para).get();
                List<String> lstLocName = new ArrayList<String>();

                for (int i = 0; i < dtLocGroup.length; i++) {
                    lstLocName.add(dtLocGroup[i][1].toString());
                    hashMapLoc.put(i, dtLocGroup[i][0]);
                }

                if (dtLocGroup != null && dtLocGroup.length > 0) {
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstLocName);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    myLstLoc.setAdapter(dataAdapter);
                  //  loc_name = myLstLoc.getItemAtPosition(0).toString();
                } else {
                    myLstLoc.setAdapter(null);
                    loc_pk = "0";
                    Log.e("Loc_PK: ", loc_pk);
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong(ex.getMessage().toString());
                Log.e("Loc Error: ", ex.getMessage());
            }
            //-----------
            // onchange spinner LOC
            myLstLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView,
                                           View selectedItemView, int i, long id) {
                    // your code here
                  //  loc_name = myLstLoc.getItemAtPosition(i).toString();
                  //  Log.e("Loc_name", loc_name);
                    Object pkGroup = hashMapLoc.get(i);
                    if (pkGroup != null) {
                        loc_pk = String.valueOf(pkGroup);
                        Log.e("Loc_PK: ", loc_pk);

                    } else {
                        loc_pk = "0";
                        Log.e("Loc_PK: ", loc_pk);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }
            });
        } else {
            TextView tvLoc = (TextView) rootView.findViewById(R.id.tvLoc);
            tvLoc.setVisibility(View.INVISIBLE);
            myLstLoc.setVisibility(View.INVISIBLE);
        }
    }

    //region show data scan log
    public void OnShowScanLog()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' order by PK desc " /*LIMIT 20" */, null);// TLG_LABEL

            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+formID+"' and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtRemain=(TextView) rootView.findViewById(R.id.txtRemain);
            _txtRemain.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
    //endregion


    //region show data scan accept
    public void OnShowScanAccept(String status)
    {

        try {
            int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if( !ckbShowAll.isChecked()) {
                sql = "  SELECT PK, TR_ITEM_PK, ITEM_BC, ITEM_CODE,TR_QTY ,ITEM_BC_CHILD,EVAL_QTY,EVAL_NUM_CHILD, TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_IN_NAME,SCAN_DATE,STATUS  " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > '" + date_previous + "' AND SENT_YN = 'Y' AND TR_TYPE = '" + formID + "' and STATUS='" + status + "' " +

                        " ORDER BY PK desc ";
            }else{
                sql = "  SELECT PK, TR_ITEM_PK, ITEM_BC, ITEM_CODE,TR_QTY ,ITEM_BC_CHILD , TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_IN_NAME,SCAN_DATE,STATUS  " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > '" + date_previous + "' AND SENT_YN = 'Y' AND TR_TYPE = '" + formID + "' and STATUS='" + status + "' "+
                        " ORDER BY PK desc  ";
            }

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));   //0
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    //row.addView(makeTableColWithText("1", scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC_CHILD")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("EVAL_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("EVAL_NUM_CHILD")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight,0)); // 7
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PO_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")), 0, fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, 0));



                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            TableRow tr1 = (TableRow) v;


                            String status = ((TextView)tr1.getChildAt(7)).getText().toString() ;

                            if(status.equals("000")) {



                                String pk = ((TextView)tr1.getChildAt(13)).getText().toString() ;
                                String qty = ((TextView)tr1.getChildAt(3)).getText().toString() ;
                                String num = ((TextView)tr1.getChildAt(4)).getText().toString() ;
                                String eval_qty = ((TextView)tr1.getChildAt(5)).getText().toString() ;
                                String eval_num = ((TextView)tr1.getChildAt(6)).getText().toString() ;
                                String item_bc =((TextView)tr1.getChildAt(1)).getText().toString() ;

                                String wh_pk = ((TextView)tr1.getChildAt(12)).getText().toString() ;

                                FragmentManager fm = gwMActivity.getSupportFragmentManager();
                                DialogFragment dialogFragment = new Evaluation_pop();
                                Bundle args = new Bundle();


                                args.putString("pk", pk);
                                args.putString("qty", qty);
                                args.putString("num", num);
                                args.putString("eval_qty", eval_qty);
                                args.putString("eval_num", eval_num);


                                args.putString("wh_pk", wh_pk);

                                args.putString("item_bc", item_bc);


                                dialogFragment.setArguments(args);
                                dialogFragment.setTargetFragment(getTargetFragment(), 1);
                                dialogFragment.setCancelable(false);
                                dialogFragment.show(fm.beginTransaction(), "dialog");

                            }



                        }
                    });


                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SCAN_DATE > '" + date_previous + "' AND SENT_YN = 'Y' ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtTotalGridBot=(TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count+ " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 16.0);



        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanAccept: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
    //endregion
    public void CountSendRecord()
    {
        flagUpload=true;
        // total scan log
        _txtSent=(TextView) rootView.findViewById(R.id.txtSent);
        //_txtTotalGridMid=(TextView) rootView.findViewById(R.id.txtTotalMid);
        _txtTotalGridBot=(TextView) rootView.findViewById(R.id.txtTotalBot);
        //int countMid=Integer.parseInt(_txtTotalGridMid.getText().toString().replaceAll("[\\D]",""));
        int countMid=0;
        int countBot=Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]",""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Send: "+(countMid+countBot));

        _txtRemain=(TextView) rootView.findViewById(R.id.txtRemain);
        int countRe=Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]",""));

        _txtTT=(TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: "+(countMid+countBot+countRe));

    }

    //region---------- show gridview Header ------------
    public void OnShowGridHeader()
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);


        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        //row.addView(makeTableColHeaderWithText("Total", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvNumChesse), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvNumChesse), scrollableColumnWidths[2], fixedHeaderHeight));

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[4], fixedHeaderHeight));

        //row.addView(makeTableColHeaderWithText("PO NO", scrollableColumnWidths[2], fixedHeaderHeight));
        // row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    public  boolean isExistBarcode(String l_bc_item,String type) {
        boolean flag = false;
        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        try{
            //String countQuery = "SELECT PK FROM INV_TR where tr_type='"+type+"'  AND del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";
            String countQuery = "SELECT PK FROM INV_TR where  tr_type='"+type+"' AND  del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";
            Cursor cursor =  db.rawQuery(countQuery, null);
            if (cursor != null && cursor.moveToFirst()) {
                flag = true;
            }
            cursor.close();
        }
        catch (Exception ex){
            //Toast.makeText(this, "Check_Exist_PK :" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
        finally {
            db.close();
        }
        return flag;
    }

    public void isLockWH(){
        _txtError.setText("WH is not locked!!!");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btOption1:
                OnShowScanAccept("001");
                break;
            case R.id.btOption2:
                OnShowScanAccept("002");
                break;
            case R.id.btOption3:
                OnShowScanAccept("003");
                break;
            case R.id.btOption5:
                OnShowScanAccept("004");
                break;
            case R.id.btOption4:
                OnShowScanAccept("000");
                break;
            case R.id.btnDelAll:

                if(!hp.isCheckDelete(formID)) return;

                String title="Confirm Delete..";
                String mess="Are you sure you want to delete all";
                alertDialogYN(title,mess,"onDelAll");
                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);
                break;
            default:
                break;
        }
    }
}
