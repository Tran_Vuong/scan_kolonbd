package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import android.os.Handler;

import gw.genumobile.com.interfaces.GridListViewOnePK;
import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.views.PopDialogGrid;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.interfaces.GridListView;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.GridListViewMid;
import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.interfaces.ListViewMultipleSelectionActivity;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.services.ApproveAsyncTask;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;


public class ProductInc extends gwFragment implements View.OnClickListener {
    protected Boolean flagUpload = true;

    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;
    public static final int tes_role_pk = 2442;//Role Approve "POP Tablet"
    Queue queue = new LinkedList();
    Calendar c;
    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    Button _btnApprove, btnViewStt, btnListBot, btnInquiry, btnMakeSlip, btnDelAll, btnList;
    TextView _txtDate, _txtError, _txtTT, _txtSent, _txtRemain, txtTotalGridMid, txtTotalGridBot, txtTotalQtyBot, _txtTime;
    EditText myBC, bc2, code, name, lotNo, edQty, edBarcod;
    List<String> arrLoc;
    String wh_name = "", loc_name = "", line_name = "", loc_pk = "", unique_id = "";
    String sql = "", tr_date = "", scan_date = "", scan_time = "", approveYN = "N";

    String data[] = new String[0];

    String wh_pk = "", line_pk = "";
    String dlg_pk = "", dlg_barcode = "", dlg_qty = "";
    int timeUpload = 0;
    int countEnter = 0;
    public Spinner myLstWH, myLstLine, myLstLoc;
    HashMap hashMapWH = new HashMap();
    HashMap hashMapLoc = new HashMap();
    HashMap hashMapLine = new HashMap();

    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_incoming_v2, container, false);
        //gwMActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);

        _txtTT = (TextView) rootView.findViewById(R.id.txtTT);
        _txtSent = (TextView) rootView.findViewById(R.id.txtSent);
        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        myBC = (EditText) rootView.findViewById(R.id.editBC);
        myLstWH = (Spinner) rootView.findViewById(R.id.lstWH);
        myLstLoc = (Spinner) rootView.findViewById(R.id.lstLoc);
        myLstLine = (Spinner) rootView.findViewById(R.id.lstLine);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");
        _txtDate = (TextView) rootView.findViewById(R.id.txtDate);


        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        _txtDate.setText(formattedDate);
        _txtTime = (TextView) rootView.findViewById(R.id.txtTime);
        if (Measuredwidth <= 600) {
            _txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);
        }
        OnShowGW_Header();
        OnShowScanLog();
        OnShowScanIn();
        OnShowScanAccept();
        CountSendRecord();
        LoadWH();
        LoadLine();
        OnPermissionApprove();
        init_color();


        myBC.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    //countEnter++;
                    //if(countEnter==2){
                    //    OnSaveBC();
                    //    countEnter = 0;
                    //}
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (wh_pk.equals("0")) {
                            myBC.getText().clear();
                            myBC.requestFocus();

                            _txtError.setTextColor(Color.RED);
                            _txtError.setText("Warehouse was wrong!!");
                            gwMActivity.alertToastShort("Warehouse was wrong. Plz check Wifi !!!");
                        } else {
                            OnSaveBC();
                        }
                    }
                    return true;//important for event onKeyDown
                }

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // this is for backspace
                    myBC.clearFocus();
                    Thread.interrupted();
                    //Log.e("IME_TEST", "BACK VIRTUAL");
                }


                return false;
            }
        });

        timeUpload = hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);

        return rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run() {
            if (flagUpload)
                doStart();
            //SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload * 1000);
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        customHandler.removeCallbacks(updateDataToServer);

    }

    @Override
    public void onResume() {
        super.onResume();
        customHandler.post(updateDataToServer);
//        customHandler.postDelayed(updateDataToServer, timeUpload*1000);
    }

    private void init_color() {
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    public void OnSaveBC() {
        if (wh_pk.equals("")) {
            _txtError.setTextColor(Color.RED);
            _txtError.setText(getString(R.string.pls_select_wh));
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        }

        if (myBC.getText().toString().equals("")) {
            _txtError.setTextColor(Color.RED);
            _txtError.setText(getString(R.string.pls_scanBC));
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        } else {

            String str_scanBC = myBC.getText().toString().toUpperCase();
            try {
                if (str_scanBC.length() > 20) {
                    _txtError.setText(getString(R.string.BC_more20));
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                }

                boolean isExist = hp.isExistBarcode(str_scanBC, formID);// check barcode exist in data
                if (isExist)// exist data
                {
                    /////alertRingTone();
                   // alertRingMedia();
                    _txtError.setText(getString(R.string.BC_exist));
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                } else {
                    scan_date = CDate.getDateyyyyMMdd();
                    scan_time = CDate.getDateYMDHHmmss();
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                    db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK, TLG_IN_WHLOC_IN_NAME,TR_LINE_PK,LINE_NAME,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                            + "VALUES('"
                            + str_scanBC + "','"
                            + wh_pk + "','"
                            + wh_name + "','"
                            + loc_pk + "','"
                            + loc_name + "','"
                            + line_pk + "','"
                            + line_name + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "N" + "','"
                            + " " + "','"
                            + "4" + "');");
                }
            } catch (Exception ex) {
                _txtError.setText(getString(R.string.save_error));
                Log.e("Error OnSaveBC", ex.getMessage());
            } finally {
                db.close();
                myBC.getText().clear();
                myBC.requestFocus();
            }
        }
        OnShowScanLog();
    }


    private void doStart() {

        try {
            flagUpload = false;//wating data from server

            db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            //cursor2 = db2.rawQuery("select TLG_POP_LABEL_PK, TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK, TR_QTY , TR_LOT_NO, TR_TYPE, ITEM_BC, TR_DATE, TR_ITEM_PK, TR_LINE_PK, ITEM_CODE, ITEM_NAME,ITEM_TYPE,PK,TLG_GD_REQ_D_PK, SLIP_NO  from INV_TR where DEL_IF=0 and TR_TYPE='6' and sent_yn = 'N' order by PK asc LIMIT 1",null);
            cursor2 = db2.rawQuery("select PK, ITEM_BC,TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK,TR_LINE_PK  from INV_TR where DEL_IF=0 and TR_TYPE='" + formID + "' and sent_yn = 'N' and STATUS=' ' order by PK asc LIMIT 20", null);
            //System.out.print("\n\n\n cursor2.count: "+ String.valueOf(cursor2.getCount()));
            //data=new String [cursor2.getCount()];
            //boolean read=Config.ReadFileConfig();

            if (cursor2.moveToFirst()) {

                //set value message
                _txtError = (TextView) rootView.findViewById(R.id.txtError);
                _txtError.setTextColor(Color.RED);
                _txtError.setText("");

                //  data=new String [cursor2.getCount()];
                data = new String[1];
                //End: Giang 12/1/2016
                int j = 0;
                String para = "";
                boolean flag = false;
                do {
                    flag = true;
                    for (int i = 0; i < cursor2.getColumnCount(); i++) {
                        if (para.length() <= 0) {
                            if (cursor2.getString(i) != null)
                                para += cursor2.getString(i) + "|";
                            else
                                para += "|";
                        } else {

                            if (flag == true) {
                                if (cursor2.getString(i) != null) {
                                    para += cursor2.getString(i);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor2.getString(i) != null)
                                    para += "|" + cursor2.getString(i);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "*|*";

                } while (cursor2.moveToNext());
                //////////////////////////
                para += "|!LG_MPOS_UPLOAD_INC_V2";
                data[j++] = para;

                //System.out.print("\n\n\n para upload: " + para);
                Log.e("para upload: ", para);

                cursor2.close();
                db2.close();

                if (CNetwork.isInternet(tmpIP, checkIP)) {
                    ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                    task.execute(data);

                    System.out.print("\n\n**********doStart********\n\n\n");
                    gwMActivity.alertToastShort("Send to Server");
                } else {
                    flagUpload = true;
                    gwMActivity.alertToastLong(getString(R.string.network_broken));
                }
            } else {
                flagUpload = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void OnPermissionApprove() {
        String para = bsUserPK + '|' + String.valueOf(tes_role_pk);
        List<String> arr_role = GetDataOnServer("ROLE", para);
        if (arr_role.size() > 0) {
            approveYN = arr_role.get(0);
            Log.e("approveYN", approveYN);
        }
        _btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
        if (approveYN.equals("Y")) {
            _btnApprove.setVisibility(View.VISIBLE);
        } else
            _btnApprove.setVisibility(View.GONE);

    }

    public void onApprove(View view){
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        String title = getString(R.string.tvConfirmApprove);
        String mess = getString(R.string.mesConfirmApprove);
        alertDialogYN(title, mess, "onApprove");
    }

    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onApprove")) {
                    Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    btnMSlip.setVisibility(View.GONE);
                    _btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    _btnApprove.setVisibility(View.GONE);
                    ProcessApprove();
                }
                if (_type.equals("DelAll")) {
                    ProcessDeleteAll();
                }
                if (_type.equals("onMakeSlip")) {
                    _btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    _btnApprove.setVisibility(View.GONE);
                    ProcessMakeSlip();
                    //ProcessMakeSlipV2();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void onClickBcMapping(View view) {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
            // Setting Dialog Title
            alertDialog.setTitle("Confirm Get Item Mapping...");
            // Setting Dialog Message
            alertDialog.setMessage("Are you sure...");
            // Setting Icon to Dialog
            alertDialog.setIcon(R.drawable.cfm_save);
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                    sql = "SELECT ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TR_LINE_PK,LINE_NAME,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE " +
                            "   FROM INV_TR WHERE TR_TYPE='10' AND BC_TYPE = 'P' AND SENT_YN='N'  " +
                            "   AND PARENT_PK IN (SELECT PARENT_PK " +
                            "       FROM INV_TR " +
                            "                       WHERE BC_TYPE='C' AND SENT_YN='N' AND (CHILD_PK IS NOT NULL OR CHILD_PK <> '') " +
                            "   GROUP BY PARENT_PK)";
                    cursor = db.rawQuery(sql, null);
                    int count = cursor.getCount();
                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            //for (int i = 0; i < count; i++) {
                            db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TR_LINE_PK,LINE_NAME,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                                    + "VALUES('"
                                    + cursor.getString(cursor.getColumnIndex("ITEM_BC")) + "',"
                                    + wh_pk + ",'"
                                    + wh_name + "',"
                                    + line_pk + ",'"
                                    + line_name + "','"
                                    + scan_date + "','"
                                    + scan_time + "','"
                                    + "N" + "','"
                                    + " " + "','"
                                    + "4" + "');");
                            //}


                            sql = "UPDATE INV_TR set "
                                    + "SENT_YN='Y'"

                                    + "  where TR_TYPE='10' and ITEM_BC='" + cursor.getString(cursor.getColumnIndex("ITEM_BC")) + "'";
                            db.execSQL(sql);
                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                    db.close();
                    OnShowScanLog();
                }
            });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    // Write your code here to invoke NO event
                    dialog.cancel();
                }
            });
            // Showing Alert Message
            alertDialog.show();
        } catch (SQLException ex) {
            Log.e("onClickBcMapping", ex.toString());
        }
    }

    /*public void onProcessGetBCMapping() throws SQLException {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "UPDATE INV_TR set "
                    + "TR_WH_IN_PK = " + wh_pk
                    + ", WH_NAME = '" + wh_name
                    + "', TR_LINE_PK = " + line_pk
                    + ",LINE_NAME='" + line_name
                    + "',TR_TYPE='4'"
                    + "  where BC_TYPE = 'P' and Parent_pk=(SELECT MAX(PARENT_PK) FROM INV_TR WHERE CHILD_PK IS NOT NULL AND BC_TYPE = 'C' AND SENT_YN='Y')";
            db.execSQL(sql);
            OnShowScanLog();
        } catch (Exception ex) {
            Log.e("Error Get BC Mapping", ex.toString());
        } finally {
            db.close();
        }
    }*/

    public void onClickViewStt(View view){

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    //region --------------ProcessMakeSlip-----------------
    public void onMakeSlip(View view) {

        //Check het Barcode send to server
        if (checkRecordGridView(R.id.grdScanLog)) {
            gwMActivity.alertToastLong(this.getResources().getString(R.string.tvAlertMakeSlip));
            return;
        }
        //Check have value make slip
        if (hp.isMakeSlip(formID) == false) return;

        String title = getString(R.string.tvConfirmMakeSlip);
        String mess = getString(R.string.mesConfirmMakeSlip);
        alertDialogYN(title, mess, "onMakeSlip");
    }

    public void ProcessMakeSlip() {

        if (queue.size() > 0) {

            queue = new LinkedList();
            return;

        } else {
            unique_id = CDate.getDateyyyyMMddhhmmss();
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = " select  TR_ITEM_PK, TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK, TR_LINE_PK, TR_QTY, UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK,PO_NO,WI_PLAN_PK,GRADE,EXECKEY " +
                        " from " +
                        " ( select  TR_ITEM_PK, SUM(TR_QTY) as TR_QTY, TR_LOT_NO,UOM, TR_WH_IN_PK, TLG_IN_WHLOC_IN_PK,TR_LINE_PK, TLG_SA_SALEORDER_D_PK,PO_NO,WI_PLAN_PK,GRADE,EXECKEY " +
                        " FROM INV_TR  WHERE DEL_IF = 0  AND SENT_YN = 'Y' AND TR_TYPE ='" + formID + "' AND STATUS='000' and SLIP_NO IN ('-','') " +
                        " GROUP BY TR_ITEM_PK,TR_LOT_NO, UOM, TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK, TR_LINE_PK, TLG_SA_SALEORDER_D_PK,PO_NO,WI_PLAN_PK,GRADE,EXECKEY )";
                cursor = db.rawQuery(sql, null);
                //cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK,  TLG_POP_LABEL_PK, SCAN_DATE, TR_WH_IN_PK, TR_LINE_PK,REMARKS,PARENT_PK   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE IN ('4') ", null);
                int count = cursor.getCount();
                data = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }


                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_wh_pk = cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK"));
                            String ex_line_pk = cursor.getString(cursor.getColumnIndex("TR_LINE_PK"));
                            String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            String ex_sale_order_d_pk = cursor.getString(cursor.getColumnIndex("TLG_SA_SALEORDER_D_PK"));
                            hp.updateExeckeyProd(formID, ex_item_pk, ex_wh_pk, ex_line_pk, ex_lot_no, ex_sale_order_d_pk, unique_id);

                            para += "|" + unique_id;
                        } else {
                            para += "|" + execkey;
                        }
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";


                    } while (cursor.moveToNext());
                }

                para += "|!LG_MPOS_PRO_INC_MAKESLIP";
                //onPopAlert(para);
                System.out.print("\n\n ******para make slip income prod: " + para);
                //data[j++] = para;
                data[j] = para;


            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                task.execute(data);
            } else {
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }
        }
    }
    //endregion


    //region --------------ProcessApprove-----------------
    public void ProcessApprove() {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if (queue.size() > 0) {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data = new String[myLst.length];
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j = 0;
                String para = "";
                boolean flag = false;
                for (int i = 0; i < myLst.length; i++) {
                    flag = true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_POP_LABEL_PK, SCAN_DATE, TR_WH_IN_PK, TR_LINE_PK,REMARKS,PARENT_PK   from INV_TR where del_if=0 and SLIP_NO IN ('-','') and TR_TYPE IN('4') and pk=" + myLst[i], null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {

                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //get list child pk
                        String parent_pk = cursor.getString(cursor.getColumnIndex("PARENT_PK"));
                        //onPopAlert(parent_pk);
                        String lschild = "";
                        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        cursor2 = db2.rawQuery("select CHILD_PK   from INV_TR where del_if=0 and CHILD_PK !=0 and PARENT_PK=" + parent_pk, null);
                        int count1 = cursor2.getCount();
                        if (cursor2.moveToFirst()) {
                            lschild = "";
                            do {

                                for (int col = 0; col < cursor2.getColumnCount(); col++) {
                                    if (lschild.length() <= 0) {
                                        if (cursor2.getString(col) != null)
                                            lschild += cursor2.getString(col);
                                        else
                                            lschild += "0";
                                    } else {
                                        if (cursor2.getString(col) != null)
                                            lschild += ";" + cursor2.getString(col);
                                        else
                                            lschild += ";";
                                    }
                                }
                            } while (cursor2.moveToNext());

                        }
                        db2.close();
                        cursor2.close();

                        ///////////////////////////////
                        para += "|" + lschild;
                        para += "|" + unique_id;
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;

                    }
                    para += "*|*";
                }
                para += "|!" + "lg_mpos_pro_inc_makeslip_map";
                System.out.print("\n\n ******para make slip goods delivery: " + para);
                Log.e("Make slip para", para);
                data[j++] = para;
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server

            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ApproveAsyncTask task = new ApproveAsyncTask(gwMActivity,this);
                task.execute(data);
            } else {
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }
        } else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK,  TLG_POP_LABEL_PK, SCAN_DATE, TR_WH_IN_PK, TR_LINE_PK,REMARKS,PARENT_PK   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO IN ('-','')  and TR_TYPE IN('4') ", null);
                int count = cursor.getCount();
                data = new String[count];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        //get list child pk
                        String parent_pk = cursor.getString(cursor.getColumnIndex("PARENT_PK"));
                        //onPopAlert(parent_pk);
                        String lschild = "";
                        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        cursor2 = db2.rawQuery("select CHILD_PK   from INV_TR where del_if=0 and CHILD_PK !=0 and PARENT_PK=" + parent_pk, null);
                        int count1 = cursor2.getCount();
                        if (cursor2.moveToFirst()) {
                            lschild = "";
                            do {

                                for (int col = 0; col < cursor2.getColumnCount(); col++) {
                                    if (lschild.length() <= 0) {
                                        if (cursor2.getString(col) != null)
                                            lschild += cursor2.getString(col);
                                        else
                                            lschild += "0";
                                    } else {
                                        if (cursor2.getString(col) != null)
                                            lschild += ";" + cursor2.getString(col);
                                        else
                                            lschild += ";";
                                    }
                                }
                            } while (cursor2.moveToNext());

                        }
                        db2.close();
                        cursor2.close();

                        ////////////////////
                        para += "|" + lschild;
                        para += "|" + unique_id;
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        //para += "|!lg_mpos_pro_inc_makeslip";
                        para += "*|*";
                    } while (cursor.moveToNext());
                }
                para += "|!lg_mpos_pro_inc_makeslip_map";
                data[j++] = para;
                //onPopAlert(para);
                System.out.print("\n\n ******para make slip income prod: " + para);
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ApproveAsyncTask task = new ApproveAsyncTask(gwMActivity,this);
                task.execute(data);
            } else {
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }

        }
    }
    //endregion

    public void onClickInquiry(View view) {
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        startActivity(openNewActivity);
    }

    public void onDelBC(View view) {
        try {

            String para = "";

            if (queue.size() > 0) {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                Object[] myLst = queue.toArray();
                for (int i = 0; i < myLst.length; i++) {
                    db.execSQL("DELETE FROM INV_TR where TLG_POP_INV_TR_PK = " + myLst[i].toString() + "");
                }
            }

            OnShowScanLog();
            OnShowScanIn();
            OnShowScanAccept();
        } catch (Exception ex) {
            gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
        } finally {
            db.close();
        }

    }

    public void onDelAll(View view) {
        if (!hp.isCheckDelete(formID)) return;

        String title = getString(R.string.tvConfirmDelete);
        String mess = getString(R.string.mesConfirmDelete);
        alertDialogYN(title, mess, "DelAll");
    }

    public void ProcessDeleteAll() {
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
        String para = "";
        boolean flag = true;
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "' and SLIP_NO IN ('-','') AND SCAN_DATE > '" + date_previous + "'", null);
            int count = cursor.getCount();

            int j = 0;
            if (cursor.moveToFirst()) {
                data = new String[1];

                do {
                    flag = true;
                    for (int k = 0; k < cursor.getColumnCount(); k++) {
                        if (para.length() <= 0) {
                            if (cursor.getString(k) != null)
                                para += cursor.getString(k);
                            else
                                para += "|";
                        } else {
                            if (flag == true) {
                                if (cursor.getString(k) != null) {
                                    para += cursor.getString(k);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor.getString(k) != null)
                                    para += "|" + cursor.getString(k);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "|" + formID;
                    para += "|delete";
                    para += "*|*";
                } while (cursor.moveToNext());

                para += "|!LG_MPOS_UPD_INV_TR_DEL";
                data[j] = para;
                //System.out.print("\n\n\n para update||delete: " + para);
                Log.e("para update||delete: ", para);
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        } else {
            gwMActivity.alertToastLong(getString(R.string.network_broken));
        }

    }

    public void onDeleteAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();

            } else {
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='" + formID + "';");
            }
        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            OnShowScanLog();
            OnShowScanIn();
            OnShowScanAccept();
        }
    }

    ////////////////////////////////////////////////
    public void onEditQty(View view) {
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        Intent openNewActivity = new Intent(view.getContext(), ListViewMultipleSelectionActivity.class);
        //send data into Activity
        startActivity(openNewActivity);
/*
        if(dlg_pk!="") {
            final Dialog dlg_EditQty = new Dialog(this);
            dlg_EditQty.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            dlg_EditQty.setContentView(R.layout.dialog_edit_qty);
            dlg_EditQty.setCancelable(false);//not close when touch
            dlg_EditQty.setTitle(" Edit Qty");
            dlg_EditQty.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_edit);

            edBarcod = (EditText) dlg_EditQty.findViewById(R.id.dlg_txt_Barcode);
            edQty = (EditText) dlg_EditQty.findViewById(R.id.dlg_txt_Qty);

            edBarcod.setText(dlg_barcode);
            edQty.setText(dlg_qty);
            Button btnSave = (Button) dlg_EditQty.findViewById(R.id.dlg_btnSave);
            Button btnCancel = (Button) dlg_EditQty.findViewById(R.id.dlg_btnCancel);

            // Attached listener for login GUI button
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlg_qty = edQty.getText().toString();
                    if(!dlg_qty.equals("")){
                        processUpdateQty(dlg_pk,dlg_qty,dlg_barcode);
                        dlg_EditQty.dismiss();
                    }
                    else{
                        onPopAlert("Qty is empty!!!");
                    }

                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlg_EditQty.dismiss();
                }
            });
            dlg_EditQty.show();
        }else{
            alertToast("Please select row first !!!");
        }
        */
    }

    //    public void processUpdateQty(String pk,String qty,String bc){
//        String para=pk+"|"+qty+"|"+bc+"|!LG_MPOS_UPD_QTY";
//        data = new String[1];
//        data[0]=para;
//        InsertUpdAsyncTask task = new InsertUpdAsyncTask(this);
//        task.execute(data);
//    }
//    public void processUpdateQtyFinish(){
//        dlg_pk = "";
//        //OnShowScanAccept();
//        gwMActivity.alertToastShort("Save finish!!");
//    }
    ///////////////////////////////////////////////
    public void onListGridMid(View view) {
        if (!checkRecordGridView(R.id.grdScanIn)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  frGridListViewMid.newInstance(1,formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    ///////////////////////////////////////////////
    public void onListGridBot(View view) {
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new GridListViewBot();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }


    //    private String GetPK(Spinner mySp)
//    {
//        String strPK = "";
//        String strTemp = "";
//
//        try{
//            strTemp = mySp.getItemAtPosition(0).toString();
//            List<String> lables = GetDataOnServer("WH_PK", strTemp);
//            if (lables.size() > 0)
//                strPK = lables.get(0);
//
//        }catch (Exception ex)
//        {
//            gwMActivity.alertToastLong("GetPK: " + ex.getMessage());
//        }
//        return strPK;
//    }
    // show data scan log
    public void OnShowScanLog() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='4' and SENT_YN='N' and DEL_IF=0 order by PK desc LIMIT 20", null);// TLG_LABEL
            //cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR WHERE TR_TYPE IN('4') AND SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            _txtRemain.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanIn() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, TR_WH_IN_NAME, LINE_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0 AND TR_TYPE IN( '4') AND SENT_YN='Y' AND SCAN_DATE = '" + scan_date + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
                //total Grid Scan in
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND TR_TYPE IN( '4') AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
                cursor = db.rawQuery(sql, null);
                count = cursor.getCount();

                txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
                txtTotalGridMid.setText("Total: " + count);
                txtTotalGridMid.setTextColor(Color.BLUE);
                txtTotalGridMid.setTextSize((float) 18.0);

            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan accept
    public void OnShowScanAccept() {
        try {
            int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            sql = " select  TR_ITEM_PK,  ITEM_CODE,TOTAL,TR_QTY, TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_IN_NAME,TR_LINE_PK,LINE_NAME,TLG_SA_SALEORDER_D_PK,PO_NO,EXECKEY " +
                    "from " +
                    "( select  TR_ITEM_PK,  ITEM_CODE,COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_IN_NAME,TR_LINE_PK,LINE_NAME,EXECKEY,TLG_SA_SALEORDER_D_PK,PO_NO " +
                    " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y'  AND TR_TYPE ='" + formID + "' AND STATUS IN('OK', '000')" +
                    " GROUP BY TR_ITEM_PK,ITEM_CODE,TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_IN_NAME,TR_LINE_PK,LINE_NAME,EXECKEY,TLG_SA_SALEORDER_D_PK,PO_NO )" +
                    " ORDER BY SLIP_NO desc  LIMIT 30 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TOTAL")), scrollableColumnWidths[2], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")), 0, fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_SA_SALEORDER_D_PK")), scrollableColumnWidths[4], fixedRowHeight, 1));

                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {
                            onShowClickGridBot(v);
                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE='4' AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND STATUS='000'";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            txtTotalGridBot.setText("Total: " + count + " ");
            txtTotalGridBot.setTextColor(Color.BLUE);
            txtTotalGridBot.setTextSize((float) 16.0);

            txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            //txtTotalQtyBot.setText("Qty: " +_qty+ " ");
            txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            txtTotalQtyBot.setTextColor(Color.BLACK);
            txtTotalQtyBot.setTextSize((float) 16.0);

        } catch (Exception ex) {
            gwMActivity.alertToastLong("Error Grid Accept: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public  void onShowClickGridBot(View v){
        boolean duplicate = false;

        TableRow tr1 = (TableRow) v;
        TextView tvLotNo = (TextView) tr1.getChildAt(4); //LOT_NO
        String st_LotNO = tvLotNo.getText().toString();
        TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP NO
        String st_SlipNO = tvSlipNo.getText().toString();
        TextView tvWhPk = (TextView) tr1.getChildAt(9);
        String whPK = tvWhPk.getText().toString();
        TextView tvItemPK = (TextView) tr1.getChildAt(8); //TR_ITEM_PK
        dlg_pk = tvItemPK.getText().toString();

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new PopDialogGrid();
        Bundle args = new Bundle();

        args.putString("TYPE", formID);
        args.putString("ITEM_PK", dlg_pk);
        args.putString("LOT_NO",st_LotNO);
        args.putString("WH_PK",whPK);
        args.putString("SLIP_NO",st_SlipNO);
        args.putString("USER",bsUserID);
        dialogFragment.setArguments(args);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    public void OnShowGW_Header()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLineName), scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTotal), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("LOT NO", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLineName), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_WH_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Sale Oder D", scrollableColumnWidths[4], fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    // end get data return array--use array set to grid view

    private void LoadWH() {

        String l_para = "1,LG_MPOS_M010_GET_WH_USER," + bsUserPK + "|wh_pro";
        try {
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String dataWHGroup[][] = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < dataWHGroup.length; i++) {
                lstGroupName.add(dataWHGroup[i][1].toString());
                hashMapWH.put(i, dataWHGroup[i][0]);
            }

            if (dataWHGroup != null && dataWHGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstWH.setAdapter(dataAdapter);
                wh_name = myLstWH.getItemAtPosition(0).toString();
            } else {
                myLstWH.setAdapter(null);
                wh_pk = "0";
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
        //-----------

        // onchange spinner wh
        myLstWH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                wh_name = myLstWH.getItemAtPosition(i).toString();
                Log.e("WH_name", wh_name);
                Object pkGroup = hashMapWH.get(i);
                if (pkGroup != null) {
                    wh_pk = String.valueOf(pkGroup);
                    Log.e("WH_PK: ", wh_pk);
                    if (!wh_pk.equals("0")) {
                        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
                            TextView tvLoc = (TextView) rootView.findViewById(R.id.tvLoc);
                            tvLoc.setVisibility(View.VISIBLE);
                            myLstLoc.setVisibility(View.VISIBLE);
                        }
                        LoadLocation();
                    }
                } else {
                    wh_pk = "0";
                    Log.e("WH_PK: ", wh_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }

    ///////////////////////////////////////////////
    private void LoadLocation() {
        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
            String l_para = "1,LG_MPOS_GET_LOC," + wh_pk;
            try {
                ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                String dtLocGroup[][] = networkThread.execute(l_para).get();
                List<String> lstLocName = new ArrayList<String>();

                for (int i = 0; i < dtLocGroup.length; i++) {
                    lstLocName.add(dtLocGroup[i][1].toString());
                    hashMapLoc.put(i, dtLocGroup[i][0]);
                }

                if (dtLocGroup != null && dtLocGroup.length > 0) {
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstLocName);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    myLstLoc.setAdapter(dataAdapter);
                    loc_name = myLstLoc.getItemAtPosition(0).toString();
                } else {
                    myLstLoc.setAdapter(null);
                    loc_pk = "0";
                    Log.e("Loc_PK: ", loc_pk);
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong(ex.getMessage().toString());
                Log.e("Loc Error: ", ex.getMessage());
            }
            //-----------
            // onchange spinner LOC
            myLstLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView,
                                           View selectedItemView, int i, long id) {
                    // your code here
                    loc_name = myLstLoc.getItemAtPosition(i).toString();
                    Log.e("Loc_name", loc_name);
                    Object pkGroup = hashMapLoc.get(i);
                    if (pkGroup != null) {
                        loc_pk = String.valueOf(pkGroup);
                        Log.e("Loc_PK: ", loc_pk);

                    } else {
                        loc_pk = "0";
                        Log.e("Loc_PK: ", loc_pk);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }
            });
        } else {
            TextView tvLoc = (TextView) rootView.findViewById(R.id.tvLoc);
            tvLoc.setVisibility(View.INVISIBLE);
            myLstLoc.setVisibility(View.INVISIBLE);
        }
    }


    private void LoadLine() {
        String l_para = "1,LG_MPOS_M010_GET_LINE_USER," + bsUserPK;
        try {
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String dataLineGroup[][] = networkThread.execute(l_para).get();

            List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < dataLineGroup.length; i++) {
                lstGroupName.add(dataLineGroup[i][1].toString());
                hashMapLine.put(i, dataLineGroup[i][0]);
            }

            if (dataLineGroup != null && dataLineGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity, android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstLine.setAdapter(dataAdapter);
                line_name = myLstLine.getItemAtPosition(0).toString();
            } else {
                line_pk = "0";
            }
        } catch (Exception ex) {
            Log.e("Searh Error: ", ex.getMessage());
        }

        //Even Choose Group
        myLstLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {

                line_name = myLstLine.getItemAtPosition(i).toString();
                Log.e("WH_name", line_name);
                Object pkGroup = hashMapLine.get(i);
                if (pkGroup != null) {
                    line_pk = String.valueOf(pkGroup);
                    Log.e("Line_PK: ", line_pk);
                    // if(!wh_pk.equals("0")){
                    //     GetLineByLineGroup(LINE_GROUP_PK);
                    //     TextView tv = (TextView)selectedItemView;
                    //    tv.setTextColor(Color.BLACK);
                    // }
                } else {
                    line_pk = "0";
                    Log.e("line_pk: ", line_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    public List<String> GetDataOnServer(String type, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";
            if (type.endsWith("WH")) //WH
            {
                l_para = "1,LG_MPOS_M010_GET_WH_USER," + para + "|wh_ord";
            }

            if (type.endsWith("LINE"))//LINE
            {
                l_para = "1,LG_MPOS_M010_GET_LINE_USER," + para;
            }

            if (type.endsWith("WH_PK")) //WH PK
            {
                l_para = "1,LG_MPOS_M010_GET_WH_PK," + para;
            }

            if (type.endsWith("LINE_PK"))//LINE PK
            {
                l_para = "1,LG_MPOS_M010_GET_LINE_PK," + para;
            }
            if (type.endsWith("LOC"))//LOCATION
            {
                l_para = "1,LG_MPOS_GET_LOC," + para;
            }
            if (type.endsWith("LOC_PK"))//LINE PK
            {
                l_para = "1,LG_MPOS_GET_LOC_PK," + para;
            }
            if (type.endsWith("BC")) //Load BC
            {
                l_para = "1,LG_MPOS_M010_GET_BC," + para;
            }
            if (type.endsWith("ROLE")) {
                l_para = "1,LG_MPOS_GET_ROLE," + para;
            }

            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                if (type.endsWith("BC")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                } else if (type.endsWith("WH_PK")) {
                    labels.add(result[i][0].toString());
                } else if (type.endsWith("LINE_PK")) {
                    labels.add(result[i][0].toString());
                } else if (type.endsWith("ROLE")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                } else if (type.endsWith("LOC_PK")) {
                    labels.add(result[i][0].toString());
                } else
                    labels.add(result[i][1].toString());
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(type + ": " + ex.getMessage());
            Log.e("Error get Data:" + type + ":", ex.getMessage());
        }
        return labels;
    }


    public void OnSaveData() {
        //txt_error2.setTextSize((float) 13.0);
        if (myBC.getText().toString().equals("")) {
            _txtError.setTextColor(Color.RED);
            _txtError.setText("Pls Scan barcode!");
            myBC.requestFocus();
            return;
            // bc1.requestFocus();
        }


        String str_scan = myBC.getText().toString().toUpperCase();

        String item_bc = "", item_code = "", item_name = "", tr_qty = "", lot_no = "", scan_time = "";
        int label_pk = 0, item_pk;

        try {
            List<String> lables = GetDataOnServer("BC", str_scan);
            int i = 0;

            if (lables.size() > 0) {
                item_bc = lables.get(0); // Item BC
                item_code = lables.get(1); // Item Code
                item_name = lables.get(2); // Item Name
                tr_qty = lables.get(3); // Qty
                lot_no = lables.get(4); // Lot No
                label_pk = Integer.parseInt(lables.get(5)); // Label PK
                item_pk = Integer.parseInt(lables.get(6)); // Item PK

                tr_date = CDate.getDateyyyyMMdd();
                scan_time = CDate.getDateYMDHHmmss();

                if (!lables.get(11).toString().equals(" ")) // wh name
                    wh_name = lables.get(11);

                if (!lables.get(12).toString().equals("0")) // wh pk
                    wh_pk = lables.get(12);

                if (!lables.get(13).toString().equals(" ")) // line name
                    line_name = lables.get(13);

                if (!lables.get(14).toString().equals("0")) // line pk
                    line_pk = lables.get(14);

                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                db.execSQL("INSERT INTO INV_TR(TLG_POP_LABEL_PK, TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK, TR_LINE_PK, TR_QTY, TR_LOT_NO, ITEM_BC, TR_DATE, TR_ITEM_PK, ITEM_CODE, ITEM_NAME,TR_TYPE,TR_WH_IN_NAME,LINE_NAME,SCAN_TIME,SENT_YN, STATUS, SLIP_NO, INCOME_DATE, CHARGER) "
                        + "VALUES('"
                        + label_pk + "','"
                        + wh_pk + "','"
                        + "" + "','" // wh_loc
                        + line_pk + "','"
                        + tr_qty + "','"
                        + lot_no + "','"
                        + item_bc + "','"
                        + tr_date + "','"
                        + item_pk + "','"
                        + item_code + "','"
                        + item_name + "','"
                        + "4" + "','"// Trans type 4: Incoming V2
                        + wh_name + "','"
                        + line_name + "','"
                        + scan_time + "','"
                        + "N" + "','"
                        + lables.get(7) + "','" // status
                        + lables.get(8) + "','"  // slip no
                        + lables.get(9) + "','"  // income date
                        + lables.get(10) + "'" +  // charger
                        ");");
                db.close();
                //bc1.getText().clear();
                myBC.requestFocus();

                _txtError.setTextColor(Color.BLACK);
                _txtError.setText("insert success: " + str_scan);

            } else {
                _txtError.setText(str_scan + " do not exist");
            }
        } catch (Exception ex) {
            //myTxtError.setText("Save Item BC Error !!!");
            myBC.setText("");
            _txtError.setText(ex.getMessage());
            Log.e("OnSave", ex.getMessage());
        } finally {
            myBC.setText("");
        }
        OnShowScanLog();
        OnShowScanIn();
    }

    public void CountSendRecord() {
        flagUpload = true;
        // total scan log
        _txtSent = (TextView) rootView.findViewById(R.id.txtSent);
        txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid = Integer.parseInt(txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        int countBot = Integer.parseInt(txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Send: " + (countMid + countBot));

        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]", ""));

        _txtTT = (TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: " + (countMid + countBot + countRe));

    }

    ////////////////////////////////////////////////

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiem tra coi trong requestCode =REQUEST_CODE_INPUT hay khong
        //V� ta c� th? m? Activity v?i nh?ng RequestCode khac nhau
        if(requestCode==REQUEST_CODE)
        {
            //Kiem tra ResultCode tra ve, cai nay ? ben InputDataActivity truyen ve
            OnShowScanAccept();
            switch(resultCode)
            {
                case RESULT_CODE:

                    break;
            }
        }
    }

    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str = str_alert;

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            onLoadFull();
            return true;
        }
        if (id == R.id.action_printer) {
            Intent i = new Intent(gwMActivity, gw.genumobile.com.views.label.Label_S130.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onLoadFull() {
        Intent openNewActivity = new Intent(gwMActivity, GridListView.class);
        //send data into Activity
        openNewActivity.putExtra("type", "4");
        startActivity(openNewActivity);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
                onClickInquiry(view);

                break;
            case R.id.btnApprove:
                onApprove(view);

                break;
            case R.id.btnViewStt:
                onClickViewStt(view);

                break;
            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelBC:
                onDelBC(view);
                break;
            case R.id.btnList:
                onListGridMid(view);

                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);

                break;
            case R.id.btnBcMapping:
                onClickBcMapping(view);

                break;
            case R.id.btnEditQty:
                onEditQty(view);

                break;
            default:
                break;
        }
    }
}
