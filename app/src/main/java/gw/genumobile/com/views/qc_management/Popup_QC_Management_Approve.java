package gw.genumobile.com.views.qc_management;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import gw.genumobile.com.R;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.views.gwcore.BaseGwActive;

/** Created by HNDGiang on 08/01/2016. **/

/*
*   Load Customer Info
*   Update: 16/06/2016
*   Author: HNDGiang
*/
public class Popup_QC_Management_Approve extends BaseGwActive {

    //region Avarible

    protected SharedPreferences appPrefs;

    public static final int REQUEST_CODE = 1;
    private int APPROVE_PK = -1;
    private String APPROVE_NAME = "";
    int indexPrevious = -1;
    int orangeColor = Color.rgb(255,99,71);
    //endregion

    //region Application Circle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_qc_management_employee);

        SetActivity(this);
        AddEvenKeyboardHiddenWhenClickOutSide(findViewById(R.id.POPUP_QC_CUSTOMER));

        appPrefs = this.getSharedPreferences("myConfig", this.MODE_PRIVATE);

        //region Set Screen Size
        int Measuredheight = 0, Measuredwidth = 0;
        Point size = new Point();
        WindowManager w = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
            w.getDefaultDisplay().getSize(size);
            Measuredheight = size.y;
            Measuredwidth = size.x;
            final LinearLayout root = (LinearLayout) findViewById(R.id.POPUP_QC_CUSTOMER);
            FrameLayout.LayoutParams rootParams = (FrameLayout.LayoutParams)root.getLayoutParams();
            rootParams.height = Measuredheight*60/100;
            rootParams.width =Measuredwidth*80/100;
            System.out.println("H: " + rootParams.height + "   W: " + rootParams.width);

        }
        //endregion

        CreatGridHeader();
        ((TextView)findViewById(R.id.tvTitle)).setText("APPROVE");
        ((TextView)findViewById(R.id.tvInfo)).setText("Approve");
        //endregion
    }

    public void GetDataByConditions(String para){

        if(para.length() == 0) para="-";
        String l_para = "1,drivtldrlgtl0001_s_11," + para;

        try{
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String resultSearch[][] = networkThread.execute(l_para).get();

            TableLayout scrollablePart = (TableLayout) findViewById(R.id.grdData);
            scrollablePart.removeAllViews();

            if(resultSearch!= null && resultSearch.length > 0){
                ShowResultSearch(resultSearch, scrollablePart);
            }
        }catch(Exception ex){
            Log.e("Searh Error: ", ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    //endregion

    private void CreatGridHeader(){
        //Init Avarible
        TableRow.LayoutParams trParam = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        TableRow tRow = new TableRow(this);
        TableLayout tLayout;
        int height = 30;
        //Design Grid
        tLayout = (TableLayout)findViewById(R.id.grdHeader);
        tRow = new TableRow(this);
        tRow.setLayoutParams(trParam);
        tRow.setGravity(Gravity.CENTER);
        tRow.setBackgroundColor(bsColors[1]);
        tRow.setVerticalGravity(50);

        //Add Column
        tRow.addView(makeTableColHeaderWithText("PK", 0, fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Seq", scrollableColumnWidths[0], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Employee ID", scrollableColumnWidths[2], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Employee Name", scrollableColumnWidths[4], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Organization", scrollableColumnWidths[4], fixedRowHeight));
        tLayout.addView(tRow);
    }

    public void onChoose(View view){
        Intent intent = getIntent();
        Bundle bundle = new Bundle();

        bundle.putInt("APPROVE_PK", APPROVE_PK);
        bundle.putString("APPROVE_NAME", APPROVE_NAME);
        intent.putExtra("RESULT_APPROVE", bundle);

        setResult(RESULT_OK, intent);
        finish();
    }

    public void ShowResultSearch(String result[][], final TableLayout scrollablePart){
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = null;
        //remove all view child
        int count = result.length;
        for(int i = 0;i < result.length; i++){
            row = new TableRow(this);
            row.setLayoutParams(wrapWrapTableRowParams);
            row.setGravity(Gravity.CENTER);
            row.setBackgroundColor(Color.LTGRAY);
            row.addView(makeTableColWithText(result[i][0], 0, fixedRowHeight, -1));
            row.addView(makeTableColWithText(String.valueOf(i+1), scrollableColumnWidths[0], fixedRowHeight, 0));
            row.addView(makeTableColWithText(result[i][1], scrollableColumnWidths[2], fixedRowHeight, -1));
            row.addView(makeTableColWithText(result[i][2], scrollableColumnWidths[4], fixedRowHeight, -1));
            row.addView(makeTableColWithText(result[i][3], scrollableColumnWidths[4], fixedRowHeight, -1));
            row.setOnClickListener(new View.OnClickListener() {
                TextView tvTeamp;
                TextView tvApprove;

                @Override
                public void onClick(View v) {

                    TableRow tr1 = (TableRow) v;
                    tvTeamp = (TextView) tr1.getChildAt(0);
                    tvApprove = (TextView) tr1.getChildAt(3);

                    APPROVE_PK = Integer.valueOf(tvTeamp.getText().toString());
                    APPROVE_NAME = tvApprove.getText().toString();

                    for (int i = 0; i < tr1.getChildCount(); i++) {
                        tvTeamp = (TextView) tr1.getChildAt(i);
                        tvTeamp.setTextColor(orangeColor);
                    }

                    if (indexPrevious >= 0
                            && indexPrevious != scrollablePart.indexOfChild(v)) {
                        TableRow trP = (TableRow) scrollablePart.getChildAt(indexPrevious);
                        for (int k = 0; k < trP.getChildCount(); k++) {
                            tvTeamp = (TextView) trP.getChildAt(k);
                            tvTeamp.setTextColor(Color.BLACK);
                        }
                    }

                    //Update indexPrevious
                    indexPrevious = scrollablePart.indexOfChild(v);
                }
            });
            scrollablePart.addView(row);
        }
    }

    public void onClose(View view){
        finish();
    }

    public void onSearch(View view){
        String para = ((EditText)findViewById(R.id.etInfo)).getText().toString();
        GetDataByConditions(para);
        indexPrevious = -1;

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    @Override
    public void onBackPressed() {
        //Do stuff
        finish();
        System.exit(0);
    }
    //endregion

}
