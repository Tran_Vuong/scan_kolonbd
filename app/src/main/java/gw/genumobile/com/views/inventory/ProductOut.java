package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.interfaces.frItemInquiry;
import gw.genumobile.com.views.PopDialogGrid;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.GridListViewMid;
import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;

public class ProductOut extends gwFragment implements View.OnClickListener {
    protected Boolean flagUpload = true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;
    private static final String formID = "18";
    Queue queue = new LinkedList();
    Queue queueMid = new LinkedList();
    //Queue queueMidBC = new LinkedList();
    SimpleDateFormat df;

    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    Button _btnApprove, btnViewStt, btnSelectMid, btnInquiry, btnMakeSlip, btnDelAll, btnList,btnListBot;
    TextView _txtDate, _txtError, _txtTT, _txtSent, _txtRemain, _txtTotalGridBot, _txtTotalQtyBot, _txtTotalGridMid, _txtTime;
    EditText myBC, bc2, code, name, lotNo;
    public Spinner myLstWH, myLstLine, myLstSlipType;

    String sql = "", tr_date = "", scan_date = "", scan_time = "", unique_id = "";
    String wh_pk = "", wh_name = "", wh_name_grid = "", line_pk = "", line_name = "", slipType_pk = "", slipType_name = "", approveYN = "N";
    String data[] = new String[0];
    int timeUpload = 0;
    HashMap hashMapWH = new HashMap();
    HashMap hashMapLine = new HashMap();
    HashMap hashMapSlipT = new HashMap();
    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_product_out, container, false);
        //lock screen
        //  this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);

        myBC = (EditText) rootView.findViewById(R.id.editBC);
        myLstWH = (Spinner) rootView.findViewById(R.id.lstWH);
        myLstLine = (Spinner) rootView.findViewById(R.id.lstLine);
        myLstSlipType = (Spinner) rootView.findViewById(R.id.lstSlipType);

        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        _txtDate = (TextView) rootView.findViewById(R.id.txtDate);
        _txtError = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");


        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        _txtDate.setText(formattedDate);
        _txtTime = (TextView) rootView.findViewById(R.id.txtTime);
        if (Measuredwidth <= 600) {
            _txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);
        }

        OnShowGridHeader();
        OnShowScanLog();
        OnShowScanIn();
        OnShowScanAccept();
        ShowWHGroup();
        LoadLine();
        LoadSlipType();
        init_color();

        myBC.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        OnSaveBC();
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // this is for backspace
                    myBC.clearFocus();
                    Thread.interrupted();
                    //Log.e("IME_TEST", "BACK VIRTUAL");
                }
                return false;
            }
        });

        timeUpload = hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);
        return rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run() {
            if (flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload * 1000);
        }
    };

    private void init_color() {
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private void doStart() {
        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        cursor2 = db2.rawQuery("select PK, ITEM_BC,TR_WH_OUT_PK  from INV_TR where DEL_IF=0 and TR_TYPE='" + formID + "' and sent_yn = 'N' order by PK asc LIMIT 10", null);
        System.out.print("\n\n\n cursor2.count: " + String.valueOf(cursor2.getCount()));
        //data=new String [cursor2.getCount()];

        if (cursor2.moveToFirst()) {
            // Write your code here to invoke YES event
            flagUpload = false;
            //set value message
            _txtError = (TextView) rootView.findViewById(R.id.txtError);
            _txtError.setTextColor(Color.RED);
            _txtError.setText("");

            //data=new String [cursor2.getCount()];
            data = new String[1];
            int j = 0;
            String para = "";
            boolean flag = false;
            do {
                flag = true;
                for (int i = 0; i < cursor2.getColumnCount(); i++) {
                    if (para.length() <= 0) {
                        if (cursor2.getString(i) != null)
                            para += cursor2.getString(i) + "|";
                        else
                            para += "|";
                    } else {

                        if (flag == true) {
                            if (cursor2.getString(i) != null) {
                                para += cursor2.getString(i);
                                flag = false;
                            } else {
                                para += "|";
                                flag = false;
                            }
                        } else {
                            if (cursor2.getString(i) != null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }
                }
                para += "|" + deviceID;
                para += "|" + bsUserID;
                para += "*|*";

            } while (cursor2.moveToNext());
            //////////////////////////
            para += "|!" + "LG_MPOS_UPL_PROD_OUT";
            data[j++] = para;
            System.out.print("\n\n\n para upload stock out: " + para);
            Log.e("para upload stock out: ", para);

            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                task.execute(data);
                gwMActivity.alertToastShort(getString(R.string.send_server));
            } else {
                flagUpload = true;
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }
        }
        cursor2.close();
        db2.close();
    }

    ////////////////////////////////////////////////
    public void OnSaveBC() {
        if (myBC.getText().toString().equals("")) {
            _txtError.setTextColor(Color.RED);
            _txtError.setText(getString(R.string.pls_scanBC));
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        } else {
            String str_scanBC = myBC.getText().toString().toUpperCase();
            try {
                if (str_scanBC.length() > 20) {
                    _txtError.setText(getString(R.string.BC_more20));
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                }

                boolean isExists = hp.isExistBarcode(str_scanBC, formID);// check barcode exist in data
                if (isExists)// exist data
                {
                    alertRingMedia();
                    _txtError.setText(getString(R.string.BC_exist));
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                } else {
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                    scan_date = CDate.getDateyyyyMMdd();
                    scan_time = CDate.getDateYMDHHmmss();

                    db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_OUT_PK,TR_WH_OUT_NAME,TR_LINE_PK,LINE_NAME,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,BC_TYPE,TR_TYPE) "
                            + "VALUES('"
                            + str_scanBC + "',"
                            + wh_pk + ",'"
                            + wh_name + "','"
                            + line_pk + "','"
                            + line_name + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "N" + "','"
                            + " " + "','"
                            + slipType_pk + "','"
                            + formID + "');");

                    _txtError.setText("Add success!");
                }
            } catch (Exception ex) {
                _txtError.setText(getString(R.string.save_error));
                Log.e("OnSaveBC", ex.getMessage());
            } finally {
                db.close();
                myBC.getText().clear();
                myBC.requestFocus();
            }
        }
        OnShowScanLog();
        CountSendRecord();
    }

    //region MakeSlip
    public void onMakeSlip(View view) {

        //Check het Barcode send to server
        if (checkRecordGridView(R.id.grdScanLog)) {
            gwMActivity.alertToastLong(this.getResources().getString(R.string.tvAlertMakeSlip));
            return;
        }
        //Check have value make slip
        if (hp.isMakeSlip(formID) == false) return;

        String title = getString(R.string.confirm_makeslip);
        String mess = getString(R.string.sure_makeslip);
        alertDialogYN(title, mess, "onMakeSlip");
    }

    public void ProcessMakeSlip() {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if (queue.size() > 0) {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
        } else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = " select  TR_ITEM_PK, TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, BC_TYPE,  TR_WH_OUT_PK, UNIT_PRICE,TR_LINE_PK" +
                        " from " +
                        " ( select TR_ITEM_PK,SUM(TR_QTY) as TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, BC_TYPE,  TR_WH_OUT_PK, UNIT_PRICE,TR_LINE_PK " +
                        " FROM INV_TR  WHERE DEL_IF = 0  AND SENT_YN = 'Y' AND TR_TYPE ='" + formID + "' AND STATUS='000' and SLIP_NO IN ('-','') " +
                        " GROUP BY TR_ITEM_PK,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, BC_TYPE,  TR_WH_OUT_PK, UNIT_PRICE,TR_LINE_PK )";
                cursor = db.rawQuery(sql, null);

                int count = cursor.getCount();
                data = new String[count];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";
                    } while (cursor.moveToNext());
                    para += "|!" + "LG_MPOS_PRO_PROD_OUT_MSLIP";

                    data[j++] = para;
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                task.execute(data);
            } else {
                gwMActivity.alertToastLong(getString(R.string.network_broken));
            }
        }
    }
    //endregion

    //region ------------------Delete-----------------------
    public void onDelAll(View view) {

        if (!hp.isCheckDelete(formID)) return;

        String title = getString(R.string.tvConfirmDelete);
        String mess = getString(R.string.mesConfirmDelete);
        alertDialogYN(title, mess, "onDelAll");
    }

    public void ProcessDelete() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                return;

            } else {

                cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "' and SLIP_NO IN ('-','') and STATUS ='000' ", null);
                int count = cursor.getCount();
                String para = "";
                boolean flag = false;
                int j = 0;
                if (cursor.moveToFirst()) {
                    data = new String[1];
                    do {

                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + bsUserID;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_UPD_INV_TR_DEL";
                    data[j] = para;
                    Log.e("para update||delete: ", para);
                }
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("Delete All :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();

        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        } else {
            gwMActivity.alertToastLong(getString(R.string.network_broken));
        }
    }

    public void onDeleteAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();

            } else {
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='" + formID + "';");
            }
        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            OnShowScanLog();
            OnShowScanIn();
            OnShowScanAccept();
        }
    }
    //endregion

    public void onClickViewStt(View view){


        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onApprove")) {
                    //ProcessApprove();
                }
                if (_type.equals("onDelAll")) {
                    ProcessDelete();
                }
                if (_type.equals("onMakeSlip")) {
                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void onClickInquiry(View view){
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        startActivity(openNewActivity);
    }

    ///////////////////////////////////////////////
    public void onListGridMid(View view) {
        if (!checkRecordGridView(R.id.grdScanIn)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  frGridListViewMid.newInstance(1,formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    ///////////////////////////////////////////////
    public void onListGridBot(View view) {
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new GridListViewBot();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    public void ShowWHGroup() {
        String l_para = "1,LG_MPOS_M010_GET_WH_USER," + bsUserPK + "|wh_ord_3";

        try {
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String dataWHGroup[][] = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < dataWHGroup.length; i++) {
                lstGroupName.add(dataWHGroup[i][1].toString());
                hashMapWH.put(i, dataWHGroup[i][0]);
            }

            if (dataWHGroup != null && dataWHGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstWH.setAdapter(dataAdapter);
                wh_name = myLstWH.getItemAtPosition(0).toString();
            }
        } catch (Exception ex) {
            Log.e("Searh Error: ", ex.getMessage());
        }

        //Even Choose Group
        myLstWH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {

                wh_name = myLstWH.getItemAtPosition(i).toString();
                Log.e("WH_name", wh_name);
                Object pkGroup = hashMapWH.get(i);
                if (pkGroup != null) {
                    wh_pk = String.valueOf(pkGroup);
                    Log.e("WH_PK: ", wh_pk);
                    // if(!wh_pk.equals("0")){
                    //     GetLineByLineGroup(LINE_GROUP_PK);
                    //     TextView tv = (TextView)selectedItemView;
                    //    tv.setTextColor(Color.BLACK);
                    // }
                } else {
                    wh_pk = "0";
                    Log.e("WH_PK: ", wh_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    private void LoadLine() {
        String l_para = "1,LG_MPOS_M010_GET_LINE_USER," + bsUserPK;
        try {
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String dataLineGroup[][] = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < dataLineGroup.length; i++) {
                lstGroupName.add(dataLineGroup[i][1].toString());
                hashMapLine.put(i, dataLineGroup[i][0]);
            }
            if (dataLineGroup != null && dataLineGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstLine.setAdapter(dataAdapter);
                line_name = myLstLine.getItemAtPosition(0).toString();
            } else {
                line_pk = "0";
            }
        } catch (Exception ex) {
            Log.e("Searh Error: ", ex.getMessage());
        }

        //Even Choose Group
        myLstLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {

                line_name = myLstLine.getItemAtPosition(i).toString();
                Log.e("WH_name", line_name);
                Object pkGroup = hashMapLine.get(i);
                if (pkGroup != null) {
                    line_pk = String.valueOf(pkGroup);
                    Log.e("Line_PK: ", line_pk);
                    // if(!wh_pk.equals("0")){
                    //     GetLineByLineGroup(LINE_GROUP_PK);
                    //     TextView tv = (TextView)selectedItemView;
                    //    tv.setTextColor(Color.BLACK);
                    // }
                } else {
                    line_pk = "0";
                    Log.e("line_pk: ", line_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

    }

    private void LoadSlipType() {
        String l_para = "1,LG_MPOS_GET_SLIP_TYPE_USER," + bsUserPK;
        try {
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String dataSlipTypeGroup[][] = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < dataSlipTypeGroup.length; i++) {
                lstGroupName.add(dataSlipTypeGroup[i][1].toString());
                hashMapSlipT.put(i, dataSlipTypeGroup[i][0]);
            }
            if (dataSlipTypeGroup != null && dataSlipTypeGroup.length > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstSlipType.setAdapter(dataAdapter);
                slipType_name = myLstSlipType.getItemAtPosition(0).toString();
            }
        } catch (Exception ex) {
            Log.e("Searh Error: ", ex.getMessage());
        }
        //Even Choose Group
        myLstSlipType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {

                slipType_name = myLstSlipType.getItemAtPosition(i).toString();
                Log.e("WH_name", slipType_name);
                Object pkGroup = hashMapSlipT.get(i);
                if (pkGroup != null) {
                    slipType_pk = String.valueOf(pkGroup);
                    Log.e("slipType_pk: ", slipType_pk);
                    // if(!wh_pk.equals("0")){
                    //     GetLineByLineGroup(LINE_GROUP_PK);
                    //     TextView tv = (TextView)selectedItemView;
                    //    tv.setTextColor(Color.BLACK);
                    // }
                } else {
                    slipType_pk = "0";
                    Log.e("line_pk: ", slipType_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    //region---------- show gridview Header ------------
    public void OnShowGridHeader() {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLineName), scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        //row.addView(makeTableColHeaderWithText("Item BC", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTotal), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[4], fixedHeaderHeight));

        //row.addView(makeTableColHeaderWithText("PO NO", scrollableColumnWidths[2], fixedHeaderHeight));
        // row.addView(makeTableColHeaderWithText("Supplier", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("WH_PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    //endregion

    //region show data scan log
    public void OnShowScanLog() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' order by PK desc LIMIT 20", null);// TLG_LABEL

            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
            _txtRemain.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    //endregion
    // ////////////////////////////////////////////
    //region show data scan in
    public void OnShowScanIn() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select PK, ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, TR_WH_OUT_NAME, LINE_NAME, TLG_POP_INV_TR_PK,TR_LOT_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SENT_YN='Y' AND SCAN_DATE = '" + scan_date + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, 0));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvStatus = (TextView) tr1.getChildAt(3); //STATUS
                            String str_Status = tvStatus.getText().toString();

                            //if(st_slipNO.equals("005") || st_slipNO.equals("008")){
                            if (str_Status.equals("005") || str_Status.equals("006")) {
                                TextView tvBC = (TextView) tr1.getChildAt(1); //INV_TR_PK
                                TextView tvPK = (TextView) tr1.getChildAt(10); //INV_TR_PK
                                String value = tvPK.getText().toString() + "|" + str_Status;
                                String valueBC = tvBC.getText().toString();
                                Log.e("Value Click", value);
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255, 99, 71));
                                }

                                if (queueMid.size() > 0) {

                                    if (queueMid.contains(value)) {
                                        Log.e("Contain Click", value);
                                        queueMid.remove(value);
                                        //queueMidBC.remove(valueBC);

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK);
                                        }
                                        duplicate = true;
                                    }
                                }

                                if (!duplicate) {
                                    queueMid.add(value);
                                    //queueMidBC.add(valueBC);
                                }
                            }
                            gwMActivity.alertToastShort(queueMid.size() + "");
                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND TR_TYPE = '" + formID + "' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
            _txtTotalGridMid.setText("Total: " + count);
            _txtTotalGridMid.setTextColor(Color.BLUE);
            _txtTotalGridMid.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    //endregion
    // ////////////////////////////////////////////
    //region show data scan accept
    public void OnShowScanAccept() {
        try {
            int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            sql = " select  TR_ITEM_PK,  ITEM_CODE,TOTAL,TR_QTY, TR_LOT_NO, SLIP_NO,TR_WH_OUT_PK,TR_WH_OUT_NAME,SCAN_DATE " +
                    " from " +
                    "( SELECT  TR_ITEM_PK,  ITEM_CODE,COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO, SLIP_NO,TR_WH_OUT_PK,TR_WH_OUT_NAME,SCAN_DATE  " +
                    " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > '" + date_previous + "' AND SENT_YN = 'Y' AND TR_TYPE = '" + formID + "' AND STATUS IN('OK', '000') " +
                    " GROUP BY TR_ITEM_PK,ITEM_CODE,TR_LOT_NO, SLIP_NO,TR_WH_OUT_PK,TR_WH_OUT_NAME,SCAN_DATE )" +
                    " ORDER BY SCAN_DATE desc,TR_ITEM_PK,SLIP_NO,TR_LOT_NO  LIMIT 30 ";

            cursor = db.rawQuery(sql, null);
            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TOTAL")), scrollableColumnWidths[2], fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight, 1));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UNIT_PRICE")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PO_NO")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK")), 0, fixedRowHeight, 0));

                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            onShowClickGridBot(v);
                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SCAN_DATE > '" + date_previous + "' AND SENT_YN = 'Y' AND STATUS='000'";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count + " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 16.0);

            _txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty) + " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 16.0);

        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanAccept: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public  void onShowClickGridBot(View v){

        TableRow tr1 = (TableRow) v;
        TextView tvLotNo = (TextView) tr1.getChildAt(4); //LOT_NO
        String st_LotNO = tvLotNo.getText().toString();
        TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP NO
        String st_SlipNO = tvSlipNo.getText().toString();

        TextView tvItemPK = (TextView) tr1.getChildAt(7); //TR_ITEM_PK
        String item_pk = tvItemPK.getText().toString();
        TextView tvWhPk = (TextView) tr1.getChildAt(8); //WH_PK
        String whPK = tvWhPk.getText().toString();

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new PopDialogGrid();
        Bundle args = new Bundle();

        args.putString("TYPE", formID);
        args.putString("ITEM_PK", item_pk);
        args.putString("LOT_NO",st_LotNO);
        args.putString("WH_PK",whPK);
        args.putString("SLIP_NO",st_SlipNO);
        args.putString("USER",bsUserID);
        dialogFragment.setArguments(args);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    //endregion
    // //////////////////////
    public void CountSendRecord() {
        flagUpload = true;
        // total scan log
        _txtSent = (TextView) rootView.findViewById(R.id.txtSent);
        _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid = Integer.parseInt(_txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        int countBot = Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Send: " + (countMid + countBot));

        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]", ""));

        _txtTT = (TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: " + (countMid + countBot + countRe));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
                onClickInquiry(view);

                break;
            case R.id.btnApprove:

                break;
            case R.id.btnViewStt:
                onClickViewStt(view);

                break;
            case R.id.btnListBot:
                onListGridBot(view);
                break;

            case R.id.btnList:
                onListGridMid(view);

                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);

                break;
            default:
                break;
        }
    }
}
