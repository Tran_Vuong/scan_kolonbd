package gw.genumobile.com.views.inventory;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.GridListViewMid;
import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.R;
import gw.genumobile.com.models.Config;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.BaseGwActive;


public class GoodsDeliNoReq extends gwFragment implements View.OnClickListener{
    private SharedPreferences appPrefs;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE=1;
    int[] colors = new int[]{0x30FF0000, 0x300000FF, 0xCCFFFF, 0x99CC33, 0x00FF99};
    Queue queue = new LinkedList();
    SQLiteDatabase db=null,db2=null;
    Cursor cursor,cursor2;
    private Handler customHandler = new Handler();
    String  data[] = new String[0];
    EditText bc;
    TextView msg_error,txtDate,recyclableTextView,txtTT, txtSentLog, txtRem,txtTotalGridMid,txtTotalGridBot,txtTotalQtyBot,_txtTime;
    String sql = "", scan_date = "",scan_time="",unique_id="";
    public Spinner myLstBus,myLstWH;

    String bus_pk = "",bus_name = "",wh_name_grid = "", line_pk = "",line_name="",wh_name="",wh_pk="";
    int timeUpload=0;
    int fixedRowHeight = 70; //70|30
    int fixedHeaderHeight = 80; //80|40
    Button btnViewStt,btnList,btnInquiry,btnMakeSlip,btnDelAll,btnListBot;
    View rootView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_goods_deli_no_req,container, false);


        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);

        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);

        myLstWH      = (Spinner) rootView.findViewById(R.id.lstWH);
        myLstBus   = (Spinner) rootView.findViewById(R.id.lstBus);

        txtDate     = (TextView) rootView.findViewById(R.id.txtDate);
        msg_error=(TextView)rootView.findViewById(R.id.txtError);
        msg_error.setText("");
        bc =(EditText)rootView.findViewById(R.id.edit_Bc);
        bc.findFocus();
        scan_date =CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        txtDate.setText(formattedDate);

        OnShowGridHeader();
        OnShowScanLog();
        OnShowScanIn();
        OnShowScanAccept();
        CountSendRecord();
        LoadWH();
        LoadBUS();
        init_color();

        // onchange spinner wh
        myLstWH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                wh_name = myLstWH.getItemAtPosition(i).toString();

                List<String> lables = GetDataOnServer("WH_PK", wh_name);
                if (lables.size() > 0) {
                    wh_pk = lables.get(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        // onchange spinner bus
        myLstBus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                bus_name = myLstBus.getItemAtPosition(i).toString();

                List<String> lables = GetDataOnServer("BUS_PK", bus_name);
                if (lables.size() > 0) {
                    bus_pk = lables.get(0);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        bc.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        msg_error = (TextView) rootView.findViewById(R.id.txtError);
                        msg_error.setTextColor(Color.RED);
                        msg_error.setTextSize((float) 16.0);
                        if (bc.getText().toString().equals("")) {
                            msg_error.setText("Pls Scan barcode!");
                            bc.requestFocus();
                        } else {
                            OnSaveBC();
                        }
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // gwMActivity is for backspace
                    bc.clearFocus();
                    //finish();
                    //System.exit(0);
                    //Log.e("IME_TEST", "BACK VIRTUAL");

                }
                return false;
            }

        });

        _txtTime=(TextView) rootView.findViewById(R.id.txtTime);
        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        sql="Select * FROM TR_CONFIG  where del_if=0";
        cursor = db.rawQuery(sql,null);
        //data=new String [cursor2.getCount()];
        if (cursor.moveToFirst()) {
            timeUpload =Integer.parseInt(cursor.getString(cursor.getColumnIndex("TIME_UPLOAD")));

        }else{
            timeUpload=3;
        }
        _txtTime.setText("Time: " + timeUpload + "s");
        cursor.close();
        db.close();
        customHandler.postDelayed(updateDataToServer, timeUpload*10000);
        return  rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run()
        {
            doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload*1000);
        }
    };

    private  void init_color(){
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private void doStart()
    {
        db2=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        //cursor2 = db2.rawQuery("select TLG_POP_LABEL_PK, TR_WAREHOUSE_PK,TR_LOC_ID, TR_QTY , TR_LOT_NO, TR_TYPE, ITEM_BC, TR_DATE, TR_ITEM_PK, TR_LINE_PK, ITEM_CODE, ITEM_NAME,ITEM_TYPE,PK,TLG_GD_REQ_D_PK, SLIP_NO  from INV_TR where DEL_IF=0 and TR_TYPE='6' and sent_yn = 'N' order by PK asc LIMIT 1",null);
        cursor2 = db2.rawQuery("select PK, ITEM_BC,TR_WAREHOUSE_PK  from INV_TR where DEL_IF=0 and TR_TYPE='8' and sent_yn = 'N' order by PK asc LIMIT 10",null);
        System.out.print("\n\n\n cursor2.count: "+ String.valueOf(cursor2.getCount()));
        //data=new String [cursor2.getCount()];
        boolean read= Config.ReadFileConfig();

        if (cursor2.moveToFirst()) {
            // Write your code here to invoke YES event
            System.out.print("\n\n**********doStart********\n\n\n");
            gwMActivity.alertToastShort("Send to Server");
            //set value message
            msg_error=(TextView)rootView.findViewById(R.id.txtError);
            msg_error.setTextColor(Color.RED);
            msg_error.setText("");

            data=new String [cursor2.getCount()];
            int j=0;
            String para ="";
            boolean flag=false;
            do {
                // labels.add(cursor.getString(1));
                flag=true;
                for(int i=0; i < cursor2.getColumnCount();i++)
                {
                    if(para.length() <= 0)
                    {
                        if(cursor2.getString(i)!= null)
                            para += cursor2.getString(i)+"|";
                        else
                            para += "|";
                    }
                    else
                    {

                        if(flag==true){
                            if(cursor2.getString(i)!= null) {
                                para += cursor2.getString(i);
                                flag=false;
                            }
                            else {
                                para += "|";
                                flag=false;
                            }
                        }
                        else{
                            if(cursor2.getString(i)!= null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }

                }
                //para += "|!"+Config.USER;
                para += "*|*";//phan biet nhieu record

            } while (cursor2.moveToNext());
            //////////////////////////
            para += "|!"+ "LG_MPOS_UPLOAD_GD_NO_REQ";
            data[j++]=para;
            System.out.print("\n\n\n para upload: "+ para);
            Log.e("para upload: ",  para);
            cursor2.close();
            db2.close();

            ServerAsyncTask task = new ServerAsyncTask(gwMActivity);
            task.execute(data);
        }

    }

    

    public void OnSaveBC(){
        if (bc.getText().toString().equals("")) {
            msg_error.setTextColor(Color.RED);
            msg_error.setText("Pls Scan barcode!");
            bc.getText().clear();
            bc.requestFocus();
            return;
        }
        else{

            String str_scanBC = bc.getText().toString().toUpperCase();

            try
            {
                int cnt = Check_Exist_PK(str_scanBC);// check barcode exist in data
                if (cnt > 0)// exist data
                {
                    msg_error.setText("Barcode exist in database!");
                    bc.getText().clear();
                    bc.requestFocus();
                    return;
                }
                else
                {
                    db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                    scan_date = CDate.getDateyyyyMMdd();
                    scan_time = CDate.getDateIncline();

                    db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_OUT_PK,TR_WH_OUT_NAME,CUST_PK,SUPPLIER_NAME,SCAN_DATE,SCAN_TIME,SENT_YN,TR_TYPE) "
                            + "VALUES('"
                            + str_scanBC + "',"
                            + wh_pk + ",'"
                            + wh_name + "',"
                            + bus_pk + ",'"
                            + bus_name + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "N" + "','"
                            + "8" + "');");
                }
            }
            catch (Exception ex)
            {
                msg_error.setText("Save Error!!!");
                Log.e("OnSaveBC", ex.getMessage());
            }
            finally {
                db.close();
                bc.getText().clear();
                bc.requestFocus();
            }
        }
        OnShowScanLog();
        //CountSendRecord();
    }

    //////////////////////////
    // check data inv_tr
    public int Check_Exist_PK(String l_bc_item) {
        int countRow = 0;
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            String countQuery = "SELECT PK FROM INV_TR where tr_type='8' AND del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";

            // db = gwMActivity.getReadableDatabase();
            cursor = db.rawQuery(countQuery, null);
            // Check_Exist_PK=cursor.getCount();
            if (cursor != null && cursor.moveToFirst()) {
                countRow = cursor.getCount();
            }
        }
        catch (Exception ex){
            gwMActivity.alertToastShort("Check_Exist_PK :" + ex.getMessage());
        }
        finally {
            cursor.close();
            db.close();
        }
        return countRow;

    }

    // show data scan log
    public void OnShowScanLog()
    {
        try
        {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
            int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30,40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='8' and SENT_YN='N' order by PK desc LIMIT 20",null);// TLG_LABEL

            int count = cursor.getCount();
            //Log.e("count log",String.valueOf(count));
            if (cursor != null && cursor.moveToFirst())
            {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("PK")), scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight));


                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            // total scan log
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='8' and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            txtRem=(TextView) rootView.findViewById(R.id.txtRemain);
            txtRem.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastShort(ex.getMessage());
            //Log.e("Grid Scan log Error -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void onMakeSlip(View view) {


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Make Slip...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to Make Slip");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                // Write your code here to invoke YES event
                Button btnMSlip=(Button)rootView.findViewById(R.id.btnMakeSlip);
                btnMSlip.setVisibility(View.GONE);
                ProcessMakeSlip();
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }
    public void ProcessMakeSlip()
    {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if(queue.size()>0)
        {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data=new String [myLst.length];
            try{
                db=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j=0;
                String para ="";
                boolean flag=false;
                for (int i = 0; i < myLst.length; i++) {
                    flag=true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WH_OUT_PK,REMARKS   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='8' and pk="+myLst[i],null);
                    if (cursor.moveToFirst()) {
                        for(int k=0; k < cursor.getColumnCount();k++)
                        {
                            if(para.length() <= 0)
                            {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|"+unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                    }
                    para += "*|*";
                }
                para += "|!"+"lg_mpos_pro_gd_noreq_makeslip";
                data[j++]=para;
                System.out.print("\n\n ******para make slip goods delivery: "+para);

            }catch (Exception ex){
                gwMActivity.alertToastShort("onMakeSlip :" + ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
        else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WH_OUT_PK,REMARKS   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='8' ", null);
                int count=cursor.getCount();
                data=new String [count];
                int j=0;
                String para ="";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + unique_id;
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "*|*";
                    }while (cursor.moveToNext());

                    para += "|!"+"lg_mpos_pro_gd_noreq_makeslip";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip goods delivery: " + para);
                }
            }
            catch (Exception ex){
                gwMActivity.alertToastShort("onMakeSlip :" + ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }
            // asyns server
            MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity);
            task.execute(data);
        }
    }

    private void LoadWH() {

        List<String> lables = GetDataOnServer("WH", bsUserPK);

        if (lables.size() <= 0) {
            myLstWH.setAdapter(null);
        } else {
            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                    android.R.layout.simple_spinner_item, lables);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            myLstWH.setAdapter(dataAdapter);

            wh_name = myLstWH.getItemAtPosition(0).toString();
            lables = GetDataOnServer("WH_PK", wh_name);
            if (lables.size() > 0)
                wh_pk = lables.get(0);
        }
    }

    private void LoadBUS() {
        List<String> lables = GetDataOnServer("BUS", bsUserPK);
        if (lables.size() <= 0) {
            myLstBus.setAdapter(null);
        } else {
            //System.out.println("\n ****string list 1:" + lables.get(1));
            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                    android.R.layout.simple_spinner_item, lables);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // attaching data adapter to spinner
            myLstBus.setAdapter(dataAdapter);

            bus_name = myLstBus.getItemAtPosition(0).toString();
            lables = GetDataOnServer("BUS_PK", bus_name);
            if (lables.size() > 0)
                bus_pk = lables.get(0);
        }
    }

    public List<String> GetDataOnServer(String type, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";
            if (type.endsWith("BUS")) //WH
            {
                l_para = "1,LG_MPOS_GD_GET_BUS," + para;
            }

            if (type.endsWith("BUS_PK")) //WH PK
            {
                l_para = "1,LG_MPOS_GD_GET_BUS_PK," + para;
            }
            if (type.endsWith("WH")) //WH
            {
                l_para = "1,LG_MPOS_M010_GET_WH_USER," + para+"|wh_ord";
            }
            if (type.endsWith("WH_PK")) //WH PK
            {
                l_para = "1,LG_MPOS_M010_GET_WH_PK," + para;
            }

            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                if (type.endsWith("BUS_PK")){
                    labels.add(result[i][0].toString());
                }
                else if (type.endsWith("WH_PK")) {
                    labels.add(result[i][0].toString());
                }
                else
                    labels.add(result[i][1].toString());
            }
        } catch (Exception ex) {
            gwMActivity.alertToastShort(type + ": " + ex.getMessage());
        }
        return labels;
    }

    //----------------------------------------------------------
    ///Load Header grid
    //----------------------------------------------------------
    public void OnShowGridHeader()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30,40};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(colors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(colors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("Pk", 0, fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhName), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("BUS", scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(colors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText("INV_TR_PK", 0, fixedHeaderHeight));
        row.addView(makeTableRowHeaderWithText(gwMActivity.getResources().getString(R.string.tvRemarks), scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);
    }


    public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(gwMActivity);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.BLACK);
        recyclableTextView.setTextSize(16);
        recyclableTextView.setBackgroundColor(-1);
        recyclableTextView.setPadding(0, 5, 0, 5);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }

    public TextView makeTableRowHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        recyclableTextView = new TextView(gwMActivity);
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.BLACK);
        recyclableTextView.setTextSize(18);
        recyclableTextView.setGravity(Gravity.CENTER);
        recyclableTextView.setBackgroundColor(Color.parseColor("#B0E2FF"));
        recyclableTextView.setPadding(0, 2, 0, 2);
        recyclableTextView.setLayoutParams(params);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }
    //----------------------------------------------------------
    ///End Load Header grid
    //----------------------------------------------------------

    // show data scan in
    public void OnShowScanIn()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30,40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select (select COUNT(0)" +
                    "from INV_TR t1 " +
                    "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE = '" + scan_date + "' AND T1.STATUS NOT IN('000', ' ')" +
                    ") AS SEQ, T2.PK, T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.INCOME_DATE, T2.CHARGER, T2.TR_WH_OUT_NAME, T2.SUPPLIER_NAME, T2.TLG_POP_INV_TR_PK " +
                    " FROM INV_TR t2 "+
                    " WHERE DEL_IF = 0 AND TR_TYPE = '8' AND SCAN_DATE = '" + scan_date + "' AND T2.STATUS NOT IN('000', ' ')  " +
                    " GROUP BY T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.INCOME_DATE, T2.CHARGER, T2.TR_WH_OUT_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK" +
                    " ORDER BY pk desc LIMIT 10 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("CHARGER"))     , scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME"))     , scrollableColumnWidths[4], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SUPPLIER_NAME"))     , scrollableColumnWidths[5], fixedRowHeight));


                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND TR_TYPE = '8' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            txtTotalGridMid=(TextView) rootView.findViewById(R.id.txtTotalMid);
            txtTotalGridMid.setText("Total: " + count+ " ");
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);
        } catch (Exception ex) {
            gwMActivity.alertToastShort("GridScanIn: " + ex.getMessage());
            //Log.e("OnShowScanIn Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan accept
    public void OnShowScanAccept()
    {
        Button btnMSlip=(Button)rootView.findViewById(R.id.btnMakeSlip);
        btnMSlip.setVisibility(View.VISIBLE);
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 23,40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select (select COUNT(0)" +
                    "from INV_TR t1 " +
                    "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE = '" + scan_date + "' AND T1.SENT_YN = 'Y' AND T1.STATUS ='000' " +
                    ") AS SEQ, T2.ITEM_BC, T2.ITEM_CODE, T2.TR_QTY, T2.TR_LOT_NO, T2.SLIP_NO, T2.TLG_POP_INV_TR_PK, T2.PK , T2.REMARKS " +
                    " FROM INV_TR t2 WHERE DEL_IF = 0 AND T2.TR_TYPE = '8' AND T2.SCAN_DATE = '" + scan_date + "' AND T2.SENT_YN = 'Y' AND T2.STATUS='000' ORDER BY pk desc LIMIT 20 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[5], fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("TLG_POP_INV_TR_PK")), 0, fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight));
                    row.addView(makeTableRowWithText(cursor.getString(cursor.getColumnIndex("REMARKS")), scrollableColumnWidths[5], fixedRowHeight));


                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(5); //SLIP_NO
                            String st_slipNO=tvSlipNo.getText().toString();

                            if(st_slipNO.equals("-")){

                                TextView tv1 = (TextView) tr1.getChildAt(7); //INV_TR_PK
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255,99,71));
                                }

                                if (queue.size() > 0) {
                                    if (queue.contains(tv1.getText().toString())) {
                                        queue.remove(tv1.getText().toString());

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK );
                                        }

                                        duplicate = true;
                                    }
                                }

                                if (!duplicate)
                                    queue.add(tv1.getText().toString());
                            }
                            gwMActivity.alertToastShort(queue.size() + "");
                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '8' AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND STATUS='000'";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty=0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty=_qty+Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            txtTotalGridBot=(TextView) rootView.findViewById(R.id.txtTotalBot);
            txtTotalGridBot.setText("Total: " + count+ " ");
            txtTotalGridBot.setTextColor(Color.BLUE);
            txtTotalGridBot.setTextSize((float) 18.0);

            txtTotalQtyBot=(TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            txtTotalQtyBot.setText("Qty: " + _qty+ " ");
            txtTotalQtyBot.setTextColor(Color.BLACK);
            txtTotalQtyBot.setTextSize((float) 18.0);

        } catch (Exception ex) {
            gwMActivity.alertToastShort("GridScanIn: " + ex.getMessage());
            //Log.e("OnShowScanAccept Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void CountSendRecord()
    {
        // total scan log
        txtTotalGridMid=(TextView) rootView.findViewById(R.id.txtTotalMid);
        txtTotalGridBot=(TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid=Integer.parseInt(txtTotalGridMid.getText().toString().replaceAll("[\\D]",""));
        int countBot=Integer.parseInt(txtTotalGridBot.getText().toString().replaceAll("[\\D]",""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));
        txtSentLog=(TextView) rootView.findViewById(R.id.txtSent);
        txtSentLog.setText("Send: "+(countMid+countBot));

        txtRem=(TextView) rootView.findViewById(R.id.txtRemain);
        int countRe=Integer.valueOf(txtRem.getText().toString().replaceAll("[\\D]",""));

        txtTT=(TextView) rootView.findViewById(R.id.txtTotal);
        txtTT.setText("TT: "+(countMid+countBot+countRe));


    }

    ////////////////////////////////////////////////
    //
    ///////////////////////////////////////////////
    public void onClickViewStt(View view){

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();


    }
    ////////////////////////////////////////////////
    public void onClickInquiry(View view){
        Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", "8");
        startActivity(openNewActivity);
    }

    public void onDelAll(View view){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Delete...");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to delete all");
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_del);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                // Write your code here to invoke YES event
                try{
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                    db.execSQL("DELETE FROM INV_TR where TR_TYPE='8';");

                    OnShowScanLog();
                    OnShowScanIn();
                    OnShowScanAccept();

                }catch (Exception ex){
                    gwMActivity.alertToastShort("Delete All :" + ex.getMessage());
                }
                finally {
                    db.close();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ////////////////////////////////////////////////
    //
    ///////////////////////////////////////////////
    public void onListGrid(View view) {
        Intent openNewActivity = new Intent(view.getContext(), GridListViewMid.class);
        //send data into Activity
        openNewActivity.putExtra("type", "8");
        startActivity(openNewActivity);
    }
    ///////////////////////////////////////////////
    public void onListGridBot(View view){
        Intent openNewActivity = new Intent(view.getContext(), GridListViewBot.class);
        //send data into Activity
        openNewActivity.putExtra("type", "8");
        //startActivity(openNewActivity);
        startActivityForResult(openNewActivity, REQUEST_CODE);
    }
    /**
     * Xử lý kết quả trả về ở đây
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //Kiểm tra có đúng requestCode =REQUEST_CODE_INPUT hay không
        //Vì ta có thể mở Activity với những RequestCode khác nhau
        if(requestCode==REQUEST_CODE)
        {
            //Kiểm trả ResultCode trả về, cái này ở bên InputDataActivity truyền về
            OnShowScanAccept();
            switch(resultCode)
            {
                case RESULT_CODE:

                    break;
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
                onClickInquiry(view);

                break;

            case R.id.btnViewStt:
                onClickViewStt(view);

                break;

            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnList:
                onListGrid(view);

                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);

                break;
            default:
                break;
        }
    }
}
