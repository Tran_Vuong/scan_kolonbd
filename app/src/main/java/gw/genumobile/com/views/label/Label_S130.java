package gw.genumobile.com.views.label;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bixolon.labelprinter.BixolonLabelPrinter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.EnumMap;
import java.util.Map;

import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CNumber;

public class Label_S130 extends BaseGwActive {
    //region Variable
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    static BixolonLabelPrinter mBixolonLabelPrinter;
    static private String mDeviceBixolon = null;
    static private boolean flagPrinter = false;
    static private String ipPrint = "", bluetoothMac = "", levelPrintZ = "";
    int levelPrintB = 0;
    boolean bixolon, zebra;
    static TextView tvStatus;

    //ZEBRA
    //private Connection mZebraLabelPrinter;
    //private UIHelper helper = new UIHelper(this);
    final String[] strStatus = {""};
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_label__s130);

        bixolon = appPrefs.getBoolean("PrintBixolon", false);
        zebra = appPrefs.getBoolean("PrintZebra", false);

        //BIXOLON
        ipPrint  =  appPrefs.getString("ipPrint", "");
        String levelB =  appPrefs.getString("levelPrintB", "20");

        if(!CNumber.isStringInt(levelB)){
            tvStatus.setText("Please Insert Number For Level In Config Print " + mDeviceBixolon);
            tvStatus.setTextColor(Color.RED);
            flagPrinter = false;
            return;
        }else{
            levelPrintB = Integer.parseInt(levelB);
        }

        //Zebra
        bluetoothMac = appPrefs.getString("bluetoothPrint", "");
        levelPrintZ =  appPrefs.getString("levelPrintZ", "");

        tvStatus = (TextView)findViewById(R.id.tvStatus);

        //region Set Screen Size
        int Measuredheight = 0;
        Point size = new Point();
        WindowManager w = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
            w.getDefaultDisplay().getSize(size);
            Measuredheight = size.y;

            final LinearLayout root = (LinearLayout) findViewById(R.id.View_Issue_Label);
            FrameLayout.LayoutParams rootParams = (FrameLayout.LayoutParams)root.getLayoutParams();
            rootParams.height = Measuredheight*65/100;
            rootParams.width = size.x;
            System.out.println("H: " + rootParams.height + "   W: " + rootParams.width);

        }
    }

    public void onPrint(View view){

        if(!flagPrinter) return;

        FrameLayout flIssueLabel = new FrameLayout(this);
        flIssueLabel = (FrameLayout)findViewById(R.id.flIssueLabel);
        flIssueLabel.setDrawingCacheEnabled(true);

        flIssueLabel.buildDrawingCache();

        final Bitmap bitmap = flIssueLabel.getDrawingCache();

        if(bitmap!= null){

            if(bixolon){

                if(levelPrintB > 80) levelPrintB = 80;
                if(levelPrintB < 20) levelPrintB = 20;

                ProgressDialog progressDialog = null;
                progressDialog = ProgressDialog.show(this, "", "Printing Barcode, Please Wait...");

                final ProgressDialog finalProgressDialog = progressDialog;
                new Thread() {

                    public void run() {
                        Label_S130.mBixolonLabelPrinter.drawBitmap(bitmap, 52, 1, 725, levelPrintB);
                        Label_S130.mBixolonLabelPrinter.print(1, 1);

                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        finalProgressDialog.dismiss();
                    }

                }.start();

            }else if(zebra){
                //Bitmap bitmap1 = getRotatedBitmap(bitmap);
                //printPhotoZebra(bitmap1);
            }
        }
    }

    //region BIXOLON
    Bitmap encodeAsBitmapBarcode(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

}
