package gw.genumobile.com.views.productresult;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.models.gwfunction;
import gw.genumobile.com.services.HandlerWebService;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.qc_management.Popup_QC_Management_ReqNo;

public class SewingResultActivity extends gwFragment implements View.OnClickListener {

    int target = 0, prod = 0, balance = 0, plan_prod = 0;
    String prod_time = "";
    String line_detail[] = new String[0];
    int qty_perH = 0;
    Cursor cursor, cursor1;
    Spinner myLstWI, myLstHour;
    List ListWI = new ArrayList();
    List ListHour = new ArrayList();
    String hour_pk = "", WI_pk = "", qty_g00 = "", qty_g30 = "", qty_d00 = "", qty_d30 = "", lst_WI = "", WorkShift = "", TimeSheetSeq = "";
    String item_pk = "", line_group_pk = "", line_pk = "", process_pk = "", work_process_pk = "", po_no,wh_pk;
    String ymd = "";
    TextView txt_prod_time, txt_current_time, txt_date, txt_qty, txt_line;
    EditText edt_target, edt_prod, edt_balance, edt_plan_prod, edt_qty_PerHour, edt_qty_def, edt_item_name, edt_employee;
    EditText edt_buyer, edt_buyer_code, edt_code, edt_itemcode, edt_color, edt_size, edt_total_good, edt_total_defect;
    Button btn_time1, btn_time2, btn_good, btn_line_list, btn_defect,btn_10,btn_1,btn_tru1,btn_tru10;
    HashMap hashMapHour = new HashMap();
    TextView bsTextView;
    ImageView img_wi;
    Bitmap imageBitmap;
    String sql = "", sql_line_result = "";
    int indexPrevious = -1;
    int fixedRowHeight = 65;
    double total_good = 0, total_defect = 0;
    boolean flag00 = false, flag30 = false, flag_insert = true;
    SQLiteDatabase db;
    ArrayAdapter<String> dataAdapter_hour = null;
    HashMap hashMapWI = new HashMap();
    HashMap hashMapWorkShift = new HashMap();
    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_sewing_result, container, false);
        //  this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        line_detail = this.getArguments().getStringArray("line");
        // line_detail = getIntent().getStringArrayExtra("line");
        target = line_detail[1].equals("") ? 0 : Integer.parseInt(line_detail[1]);
        prod = line_detail[3].equals("") ? 0 : Integer.parseInt(line_detail[3]);
        balance = line_detail[4].equals("") ? 0 : Integer.parseInt(line_detail[4]);
        plan_prod = line_detail[5].equals("") ? 0 : Integer.parseInt(line_detail[5]);
        lst_WI = line_detail[10];
        prod_time = line_detail[2];
        qty_perH = target / 8 / 2;

        edt_target = (EditText) rootView.findViewById(R.id.edt_target);
        edt_prod = (EditText) rootView.findViewById(R.id.edt_prod);
        edt_balance = (EditText) rootView.findViewById(R.id.edt_balance);
        edt_plan_prod = (EditText) rootView.findViewById(R.id.edt_plan_prod);
        edt_qty_PerHour = (EditText) rootView.findViewById(R.id.edt_qty_per_hour);

        myLstWI = (Spinner) rootView.findViewById(R.id.spn_WI);
        myLstHour = (Spinner) rootView.findViewById(R.id.spn_hour);
        edt_buyer = (EditText) rootView.findViewById(R.id.edt_buyer);
        edt_buyer_code = (EditText) rootView.findViewById(R.id.edt_buyer_code);
        edt_code = (EditText) rootView.findViewById(R.id.edt_code);
        edt_itemcode = (EditText) rootView.findViewById(R.id.edt_item_code);
        edt_color = (EditText) rootView.findViewById(R.id.edt_color);
        edt_size = (EditText) rootView.findViewById(R.id.edt_size);
        txt_prod_time = (TextView) rootView.findViewById(R.id.txt_prod_time);
        txt_current_time = (TextView) rootView.findViewById(R.id.txt_current_time);
        txt_date = (TextView) rootView.findViewById(R.id.txt_date);
        txt_qty = (TextView) rootView.findViewById(R.id.txt_qty);
        txt_line = (TextView) rootView.findViewById(R.id.txt_line);
        edt_total_good = (EditText) rootView.findViewById(R.id.edt_total_good);
        edt_total_defect = (EditText) rootView.findViewById(R.id.edt_total_defect);
        edt_qty_def = (EditText) rootView.findViewById(R.id.edt_qty_def);
        edt_item_name = (EditText) rootView.findViewById(R.id.edt_item_name);
        edt_employee = (EditText) rootView.findViewById(R.id.edt_employee);
        img_wi = (ImageView) rootView.findViewById(R.id.img_wi);

        edt_employee.setText(gwMActivity.getBsUserName());
        btn_time1 = (Button) rootView.findViewById(R.id.btn_time1);
        btn_time2 = (Button) rootView.findViewById(R.id.btn_time2);
        btn_good = (Button) rootView.findViewById(R.id.btn_good);
        btn_defect = (Button) rootView.findViewById(R.id.btn_defect);

        btn_10 = (Button) rootView.findViewById(R.id.btn_10);
        btn_1 = (Button) rootView.findViewById(R.id.btn_1);
        btn_tru1 = (Button) rootView.findViewById(R.id.btn_tru1);
        btn_tru10 = (Button) rootView.findViewById(R.id.btn_tru10);


        btn_1.setOnClickListener(this);
        btn_10.setOnClickListener(this);
        btn_tru1.setOnClickListener(this);
        btn_tru10.setOnClickListener(this);

        btn_line_list = (Button) rootView.findViewById(R.id.btn_line_list);
        edt_target.setText(String.valueOf(target));
        edt_prod.setText(String.valueOf(prod));
        edt_balance.setText(String.valueOf(balance));
        edt_plan_prod.setText(String.valueOf(plan_prod));
        edt_qty_PerHour.setText(String.valueOf(qty_perH));
        txt_prod_time.setText(prod_time);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String currentTime = sdf.format(new Date());
        txt_current_time.setText(currentTime);
        txt_qty.setText(line_detail[9] + " / " + line_detail[8]);
        txt_line.setText(line_detail[0]);
        edt_qty_def.setText("0");


        load_WI();
//        load_Hour();
        btn_line_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    OnComeBackLine(view);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });
        btn_time1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_time2.setBackgroundColor(Color.parseColor("#dddddd"));
                btn_time1.setBackgroundColor(Color.parseColor("#3366CC"));
                flag00 = true;
                flag30 = false;
                TimeSheetSeq = "0";

                if (qty_g00.equals("")) edt_qty_PerHour.setText("0");
                else edt_qty_PerHour.setText(qty_g00);
                if (qty_d00.equals("")) edt_qty_def.setText("0");
                else edt_qty_def.setText(qty_d00);

                if (edt_qty_PerHour.getText().toString().equals("0")) flag_insert = true;
                else flag_insert = false;

            }
        });

        btn_time2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_time1.setBackgroundColor(Color.parseColor("#dddddd"));
                btn_time2.setBackgroundColor(Color.parseColor("#3366CC"));
                flag00 = false;
                flag30 = true;
                TimeSheetSeq = "1";

                if (qty_g30.equals(""))
                    edt_qty_PerHour.setText("0");
                else
                    edt_qty_PerHour.setText(qty_g30);
                if (qty_d30.equals(""))
                    edt_qty_def.setText("0");
                else
                    edt_qty_def.setText(qty_d30);

                if (edt_qty_PerHour.getText().toString().equals("0"))
                    flag_insert = true;
                else
                    flag_insert = false;
            }
        });

        btn_good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean flag_save = false;
                String hour_update = "";
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                if(wh_pk==null||wh_pk=="") {
                    gwMActivity.alertToastLong("This line not yet define WH!!");
                    return;
                }
                if (!flag_insert) {
                    if ((flag30 == false && flag00 == false) || (flag30 == true && flag00 == true)) {
                    }
                    else {
                        try {
                            String l_para =String.format("2,%s,%s|%s|%s|%s|%s|%s|%s|%s|%s", gwfunction.SewingResultActivity_U,
                                    WI_pk,WorkShift,hashMapHour.get(dataAdapter_hour.getPosition(myLstHour.getSelectedItem().toString())),TimeSheetSeq,edt_qty_PerHour.getText(),bsUserID,ymd,po_no,wh_pk);

                           // String l_para = "2,LG_MPOS_UPDATE_LINE_RESULT_H," + WI_pk + "|" + WorkShift + "|" + hashMapHour.get(dataAdapter_hour.getPosition(myLstHour.getSelectedItem().toString())) +
                                //    "|" + TimeSheetSeq + "|" + edt_qty_PerHour.getText() + "|" + bsUserID + "|" + ymd;
                            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                            JResultData dtLocGroup = networkThread.execute(l_para).get();
                        } catch (Exception ex) {
                            Log.e("Update Goods: ", ex.toString());
                        }
                    }
                }
                else {
                    if ((flag30 == false && flag00 == false) || (flag30 == true && flag00 == true)) {
                    } else {
                        try {
                            String l_para =String.format("2,%s,%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s",gwfunction.SewingResultActivity_I,
                                    WI_pk, myLstWI.getSelectedItem().toString(),WorkShift,hashMapHour.get(dataAdapter_hour.getPosition(myLstHour.getSelectedItem().toString())),TimeSheetSeq,edt_qty_PerHour.getText(),item_pk,line_group_pk
                                    ,line_pk,process_pk,work_process_pk,bsUserID,ymd,po_no,wh_pk);
                                    /*String l_para = "2,LG_MPOS_INSERT_LINE_RESULT_H," + WI_pk + "|" + myLstWI.getSelectedItem().toString() + "|" + WorkShift + "|" + hashMapHour.get(dataAdapter_hour.getPosition(myLstHour.getSelectedItem().toString())) +
                                    "|" + TimeSheetSeq + "|" + edt_qty_PerHour.getText() + "|" + item_pk + "|" + line_group_pk + "|" + line_pk + "|" + process_pk + "|" + work_process_pk + "|" + bsUserID + "|" + ymd;*/
                            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                            JResultData dtLocGroup = networkThread.execute(l_para).get();
                        } catch (Exception ex) {
                            Log.e("Update Goods: ", ex.toString());
                        }
                    }
                }
                loadGridData();

                btn_time1.setBackgroundColor(Color.parseColor("#dddddd"));
                btn_time2.setBackgroundColor(Color.parseColor("#dddddd"));
                flag00 = false;
                flag30 = false;
                db.close();
            }
        });

        btn_defect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    OnPro_Sew_Def(view);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });
//        loadGridData();
        Calendar c = Calendar.getInstance();
        String month = c.get(Calendar.MONTH) + 1 + "";
        if (month.length() < 2) {
            month = "0" + month;
        }
        String date = c.get(Calendar.DATE) + "";
        if (date.length() < 2) {
            date = "0" + date;
        }
        ymd = c.get(Calendar.YEAR) + "" + month + date;
        return rootView;
    }

    private void load_WI() {
        List<String> lst = new ArrayList<String>();
        String l_para = "1,LG_MPOS_GET_WI,1," + lst_WI.replace("|", ";");
        try {
            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
            List<JDataTable> jdt = dtLocGroup.getListODataTable();
            JDataTable dt = jdt.get(0);
            for (int i = 0; i < dt.totalrows; i++) {
                hashMapWI.put(i, dt.records.get(i).get("pk").toString());
                hashMapWorkShift.put(i, dt.records.get(i).get("work_shift").toString());
                lst.add(dt.records.get(i).get("wi_no").toString());
            }

            if (dt != null && dt.totalrows > 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity, android.R.layout.simple_spinner_item, lst);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                myLstWI.setAdapter(dataAdapter);
            } else {
                myLstWI.setAdapter(null);
                WI_pk = "0";
            }

        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WI Error: ", ex.getMessage());
        }

        myLstWI.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                WI_pk = myLstWI.getItemAtPosition(i).toString();
                Object wi_pk = hashMapWI.get(i);
                WI_pk = String.valueOf(wi_pk);
                Object wShift = hashMapWorkShift.get(i);
                WorkShift = String.valueOf(wShift);
                if (!WI_pk.equals("0")) {
                    load_WI_detail(WI_pk);
                    load_Hour();
                    loadGridData();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void load_WI_detail(String WI) {
//        LG_MPOS_GET_WI
            String l_para = "1,LG_MPOS_GET_WI_DETAIL,1," + WI_pk;
        try {
            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
            List<JDataTable> jdt = dtLocGroup.getListODataTable();
            JDataTable dt = jdt.get(0);
            if (dt.totalrows > 0) {
                edt_buyer.setText(dt.records.get(0).get("partner_name").toString());
                edt_buyer_code.setText(dt.records.get(0).get("partner_id").toString());
                edt_code.setText(dt.records.get(0).get("po_no").toString());
                edt_itemcode.setText(dt.records.get(0).get("item_code").toString());
                edt_color.setText(dt.records.get(0).get("color_nm").toString());
                edt_size.setText(dt.records.get(0).get("size_nm").toString());
                edt_item_name.setText(dt.records.get(0).get("item_name").toString());

                line_group_pk = dt.records.get(0).get("tlg_pb_line_group_pk").toString();
                line_pk = dt.records.get(0).get("tlg_pb_line_pk").toString();
                process_pk = dt.records.get(0).get("tlg_pb_process_pk").toString();
                work_process_pk = dt.records.get(0).get("tlg_pb_work_process_pk").toString();
                po_no = dt.records.get(0).get("po_no").toString();
                wh_pk = dt.records.get(0).get("wh_pk").toString();

                imageBitmap 			= null;
                String result 			= dt.records.get(0).get("data").toString();
                byte[] decodedString 	= android.util.Base64.decode(result, android.util.Base64.DEFAULT);
                imageBitmap 			= BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                img_wi.setImageBitmap(imageBitmap);
//                SystemClock.sleep(500);
//                loadPhoto(mImageView);
            }
        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WI Detail Error: ", ex.getMessage());
        }

    }

    private void load_Hour() {
        //LG_MPOS_GET_HOUR
        List<String> lst = new ArrayList<String>();
        String   l_para =String.format("1,%s,1,%s|%s", gwfunction.SewingResultActivitysewingresult_gethour.getValue(),WorkShift,line_pk);

        try {
            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
            List<JDataTable> jdt = dtLocGroup.getListODataTable();
            JDataTable dt = jdt.get(0);
            for (int i = 0; i < dt.totalrows; i++) {
                hashMapHour.put(i, dt.records.get(i).get("code").toString());
                lst.add(dt.records.get(i).get("cha_value1").toString().substring(0, 5) + " - " + dt.records.get(i).get("cha_value2").toString().substring(0, 5));
            }

            if (dt != null && dt.totalrows > 0) {
                dataAdapter_hour = new ArrayAdapter<String>(gwMActivity, android.R.layout.simple_spinner_item, lst);
                dataAdapter_hour.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                myLstHour.setAdapter(dataAdapter_hour);
            } else {
                myLstHour.setAdapter(null);
                hour_pk = "0";
            }

        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("Hour Error: ", ex.getMessage());
        }

        myLstHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Object pkGroup = hashMapHour.get(i);
                if (pkGroup != null) {
                    hour_pk = String.valueOf(pkGroup);
                    String hour_name = myLstHour.getItemAtPosition(i).toString();
                    if (!hour_pk.equals("0")) {
                        //dosomehitng
                        String hour_split[] = hour_name.split(" - ");
                        String hour1 = hour_split[0];
                        String hour2 = hour_split[1];
                        hour1 = hour1.substring(0, 2);
                        btn_time1.setText(String.valueOf(Integer.parseInt(hour1) + 1) + ":00");
                        btn_time2.setText(hour2);
                        edt_qty_PerHour.setText("0");
                        TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grd_data);
                        if (scrollablePart.getChildCount() > 0) {
                            for (int j = 0; j < scrollablePart.getChildCount(); j++) {
                                TableRow row = (TableRow) scrollablePart.getChildAt(j);
                                TextView prod_hour = (TextView) row.getChildAt(0);
                                TextView tv_child;
                                if (hour_name.equals(prod_hour.getText().toString())) {
                                    flag_insert = true;
                                    TextView g00 = (TextView) row.getChildAt(1); //PROD HOUR
                                    qty_g00 = g00.getText().toString();
                                    TextView g30 = (TextView) row.getChildAt(2); //PROD HOUR
                                    qty_g30 = g30.getText().toString();
                                    TextView d00 = (TextView) row.getChildAt(4); //PROD HOUR
                                    qty_d00 = d00.getText().toString();
                                    TextView d30 = (TextView) row.getChildAt(5); //PROD HOUR
                                    qty_d30 = d30.getText().toString();
                                    int xx = row.getChildCount();
                                    for (int m = 0; m < xx; m++) {
                                        tv_child = (TextView) row.getChildAt(m);
                                        if (m > 0 && tv_child.getText().length() > 0) {
                                            flag_insert = false;
                                        }
                                        tv_child.setBackgroundColor(Color.parseColor("#f2f99d"));
                                    }
                                    if (indexPrevious >= 0
                                            && indexPrevious != j) {
                                        TableRow trP = (TableRow) scrollablePart.getChildAt(indexPrevious);
                                        for (int k = 0; k < trP.getChildCount(); k++) {
                                            tv_child = (TextView) trP.getChildAt(k);
                                            tv_child.setBackgroundColor(Color.WHITE);
                                        }
                                    }
                                    //Update indexPrevious
                                    indexPrevious = j;
                                    return;
                                }
                            }

                        }

                    }
                } else {
                    hour_pk = "0";
                    Log.e("wi_pk: ", hour_pk);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void OnComeBackLine(View v) throws InterruptedException, ExecutionException {
        // Intent i = new Intent(v.getContext(), SewingLineListActivity.class);
        // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //  startActivity(i);
        gwMActivity.PopBackStack();
    }

    public void OnPro_Sew_Def(View v) throws InterruptedException, ExecutionException {
      /*  Intent i = new Intent(v.getContext(), PopSewingDefectActivity.class);
        i.putExtra("SLIPWI", WI_pk.toString());
        i.putExtra("PRODTIME", myLstHour.getSelectedItem().toString());
        i.putExtra("PROD_DATE", txt_date.getText());
        i.putExtra("TIME1", btn_time1.getText().toString());
        i.putExtra("TIME2", btn_time2.getText().toString());
        i.putExtra("WI_PK", WI_pk.toString());
        i.putExtra("HOURLY", hashMapHour.get(dataAdapter_hour.getPosition(myLstHour.getSelectedItem().toString())).toString());
        i.putExtra("LINE", txt_line.getText());
        i.putExtra("LINE_DETAIL", line_detail);
         i.putExtra("YMD",ymd);
        i.putExtra("WS",WorkShift);
        i.putExtra("LINE_PK",line_pk);
        i.putExtra("WORK_PROCESS",work_process_pk);
        i.putExtra("IT_ITEM_PK",item_pk);

        if (flag00 == true)
            i.putExtra("HOURLY_TYPE", "00");
        else i.putExtra("HOURLY_TYPE", "30");
        startActivity(i);*/


        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = new PopSewingDefectActivity();
        Bundle args = new Bundle();
        args.putString("SLIPWI", WI_pk.toString());
        args.putString("PRODTIME", myLstHour.getSelectedItem().toString());
        args.putString("PROD_DATE", txt_date.getText().toString());
        args.putString("TIME1", btn_time1.getText().toString());
        args.putString("TIME2", btn_time2.getText().toString());
        args.putString("WI_PK", WI_pk.toString());
        args.putString("HOURLY", hashMapHour.get(dataAdapter_hour.getPosition(myLstHour.getSelectedItem().toString())).toString());
        args.putString("LINE", txt_line.getText().toString());
        args.putStringArray("LINE_DETAIL", line_detail);
        args.putString("YMD", ymd);
        args.putString("WS", WorkShift);
        args.putString("LINE_PK", line_pk);
        args.putString("WORK_PROCESS", work_process_pk);
        args.putString("IT_ITEM_PK", item_pk);
        args.putString("po_no", po_no);
        args.putString("wh_pk", wh_pk);
        if (flag00 == true)
            args.putString("HOURLY_TYPE", "00");
        else args.putString("HOURLY_TYPE", "30");
        dialogFragment.setArguments(args);
        dialogFragment.setTargetFragment(this, 1);
        dialogFragment.setCancelable(false);
        dialogFragment.show(fm.beginTransaction(), "dialog");

    }

    public TextView makeTableColWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels, int textAlign, int color) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(4, 0, 0, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.BLACK);
        if (textAlign == -1) {
            bsTextView.setGravity(Gravity.LEFT);
            bsTextView.setPadding(10, 5, 0, 5);
        } else if (textAlign == 0) {
            bsTextView.setGravity(Gravity.CENTER);
            bsTextView.setPadding(0, 5, 0, 5);
        } else {
            bsTextView.setGravity(Gravity.RIGHT);
            bsTextView.setPadding(0, 5, 10, 5);
        }
        bsTextView.setTextSize(22);
        // bsTextView.setTextColor(color);
        bsTextView.setBackgroundColor(color);
        //  bsTextView.sty
        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }

    //Load grid data
    public void loadGridData() {
        String   l_para =String.format("1,%s,1,%s|%s|%s|%s", gwfunction.SewingResultActivity_getlineresult_h.getValue(),WI_pk,WorkShift,line_pk,ymd);
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
            TableRow row = new TableRow(gwMActivity);
            final TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grd_data);
            scrollablePart.removeAllViews();//remove all view child

            total_defect = 0;
            total_good = 0;

            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
            List<JDataTable> jdt = dtLocGroup.getListODataTable();
            JDataTable dt = jdt.get(0);
            for (int i = 0; i < dt.totalrows; i++) {
                double good_sum = 0;
                double def_sum = 0;
                double ratio = 0;
                row = new TableRow(gwMActivity);
                row.setLayoutParams(wrapWrapTableRowParams);
                row.setGravity(Gravity.CENTER);
                row.setBackgroundColor(Color.parseColor("#00FFFF"));
                int color = Color.WHITE;
                if (i >= 8) color = Color.parseColor("#eae3e3");
                row.addView(makeTableColWithText(dt.records.get(i).get("cha_value1").toString().substring(0, 5) + " - " + dt.records.get(i).get("cha_value2").toString().substring(0, 5), ((TextView) rootView.findViewById(R.id.hd_Prod_hour)).getWidth(), fixedRowHeight, -1, color));

                String g_00 = dt.records.get(i).get("g_00").toString().equals("0") ? "" : dt.records.get(i).get("g_00").toString();
                String g_30 = dt.records.get(i).get("g_30").toString().equals("0") ? "" : dt.records.get(i).get("g_30").toString();
                row.addView(makeTableColWithText(g_00, ((TextView) rootView.findViewById(R.id.hd_good00)).getWidth(), fixedRowHeight, 1, color));
                row.addView(makeTableColWithText(g_30, ((TextView) rootView.findViewById(R.id.hd_good30)).getWidth(), fixedRowHeight, 1, color));
                good_sum = Double.parseDouble(dt.records.get(i).get("g_00").toString()) + Integer.parseInt(dt.records.get(i).get("g_30").toString());
                total_good += good_sum;
                if (good_sum != 0)
                    row.addView(makeTableColWithText(String.valueOf(Math.round(good_sum)), ((TextView) rootView.findViewById(R.id.hd_goodSum)).getWidth(), fixedRowHeight, 1, color));
                else
                    row.addView(makeTableColWithText("", ((TextView) rootView.findViewById(R.id.hd_goodSum)).getWidth(), fixedRowHeight, 1, color));

                String d_00 = dt.records.get(i).get("d_00").toString().equals("0") ? "" : dt.records.get(i).get("d_00").toString();
                String d_30 = dt.records.get(i).get("d_30").toString().equals("0") ? "" : dt.records.get(i).get("d_30").toString();
                row.addView(makeTableColWithText(d_00, ((TextView) rootView.findViewById(R.id.hd_defect00)).getWidth(), fixedRowHeight, 1, color));
                row.addView(makeTableColWithText(d_30, ((TextView) rootView.findViewById(R.id.hd_defect30)).getWidth(), fixedRowHeight, 1, color));
                def_sum = Double.parseDouble(dt.records.get(i).get("d_00").toString()) + Integer.parseInt(dt.records.get(i).get("d_30").toString());
                total_defect += def_sum;
                if (def_sum != 0)
                    row.addView(makeTableColWithText(String.valueOf(Math.round(def_sum)), ((TextView) rootView.findViewById(R.id.hd_defectSum)).getWidth(), fixedRowHeight, 1, color));
                else
                    row.addView(makeTableColWithText("", ((TextView) rootView.findViewById(R.id.hd_defectSum)).getWidth(), fixedRowHeight, 1, color));

                ratio = (good_sum / (target / 8)) * 100;
                if (ratio != 0)
                    row.addView(makeTableColWithText(String.valueOf(Math.round(ratio)), ((TextView) rootView.findViewById(R.id.hd_Ratio)).getWidth(), fixedRowHeight, 1, color));
                else
                    row.addView(makeTableColWithText("", ((TextView) rootView.findViewById(R.id.hd_Ratio)).getWidth(), fixedRowHeight, 1, color));

                //file pic
//                String pic = cursor.getString(cursor.getColumnIndex("PIC"));
//                String[] arr = null;
//                if (!(pic == null || pic.equals("")))
//                    arr = pic.split(";");
//                if (!(arr == null || arr.length == 0)) {
//                    row.addView(makeTableColWithText("P " + arr.length, ((TextView) rootView.findViewById(R.id.hd_File)).getWidth(), fixedRowHeight, -1, color));
//                } else
//                    row.addView(makeTableColWithText("", ((TextView) rootView.findViewById(R.id.hd_File)).getWidth(), fixedRowHeight, -1, color));
                row.addView(makeTableColWithText("", ((TextView) rootView.findViewById(R.id.hd_File)).getWidth(), fixedRowHeight, -1, color));

                row.setOnClickListener(new View.OnClickListener() {
                    TextView tv11;

                    @Override
                    public void onClick(View v) {
                        flag_insert = true;
                        TableRow tr1 = (TableRow) v;
                        TextView prod_hour = (TextView) tr1.getChildAt(0); //PROD HOUR
                        String hour = prod_hour.getText().toString();
                        TextView g00 = (TextView) tr1.getChildAt(1); //PROD HOUR
                        qty_g00 = g00.getText().toString();
                        TextView g30 = (TextView) tr1.getChildAt(2); //PROD HOUR
                        qty_g30 = g30.getText().toString();
                        TextView d00 = (TextView) tr1.getChildAt(4); //PROD HOUR
                        qty_d00 = d00.getText().toString();
                        TextView d30 = (TextView) tr1.getChildAt(5); //PROD HOUR
                        qty_d30 = d30.getText().toString();
                        myLstHour.setSelection(dataAdapter_hour.getPosition(hour));
                        btn_time1.setBackgroundColor(Color.parseColor("#dddddd"));
                        btn_time2.setBackgroundColor(Color.parseColor("#dddddd"));
                        edt_qty_PerHour.setText("0");
                        flag30 = flag00 = false;

                        int xx = tr1.getChildCount();
                        for (int i = 0; i < xx; i++) {
                            tv11 = (TextView) tr1.getChildAt(i);
//                            if (i > 0 && tv11.getText().length() > 0) {
//                                flag_insert = false;
//                            }
                            tv11.setBackgroundColor(Color.parseColor("#f2f99d"));
                        }
                        if (indexPrevious >= 0
                                && indexPrevious != scrollablePart.indexOfChild(v)) {
                            TableRow trP = (TableRow) scrollablePart.getChildAt(indexPrevious);
                            for (int k = 0; k < trP.getChildCount(); k++) {
                                tv11 = (TextView) trP.getChildAt(k);
                                int color = Color.WHITE;
                                if (indexPrevious >= 8) color = Color.parseColor("#eae3e3");
                                tv11.setBackgroundColor(color);
                            }
                        }
                        //Update indexPrevious
                        indexPrevious = scrollablePart.indexOfChild(v);


                    }
                });
                scrollablePart.addView(row);
            }


        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("Hour Error: ", ex.getMessage());
        }


        edt_total_good.setText(String.valueOf(Math.round(total_good)));
        edt_total_defect.setText(String.valueOf(Math.round(total_defect)));


    }

    //OnPlusTen
    public void OnPlusTen(View v) throws InterruptedException, ExecutionException {
        int qty = Integer.parseInt(edt_qty_PerHour.getText().toString());
        qty = qty + 10;
        edt_qty_PerHour.setText(String.valueOf(qty));

    }

    //OnPlusOne
    public void OnPlusOne(View v) throws InterruptedException, ExecutionException {
        int qty = Integer.parseInt(edt_qty_PerHour.getText().toString());
        qty = qty + 1;
        edt_qty_PerHour.setText(String.valueOf(qty));
    }

    //OnMinusTen
    public void OnMinusTen(View v) throws InterruptedException, ExecutionException {
        int qty = Integer.parseInt(edt_qty_PerHour.getText().toString());
        qty = qty - 10;
        edt_qty_PerHour.setText(String.valueOf(qty));
    }

    //OnMinusOne
    public void OnMinusOne(View v) throws InterruptedException, ExecutionException {
        int qty = Integer.parseInt(edt_qty_PerHour.getText().toString());
        qty = qty - 1;
        edt_qty_PerHour.setText(String.valueOf(qty));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_10:
                try {
                    OnPlusTen(view);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_1:
                try {
                    OnPlusOne(view);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_tru1:
                try {
                    OnMinusOne(view);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_tru10:
                try {
                    OnMinusTen(view);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;

        }

    }
}
