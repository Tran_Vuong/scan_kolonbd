package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.views.gwFragment;

public class StockTransferReq extends gwFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match

    protected Boolean flagUpload = true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;

    Queue queue = new LinkedList();
    Queue queueMid = new LinkedList();
    Queue queueMidBC = new LinkedList();

    Calendar c;
    SimpleDateFormat df;
    String arr[] = new String[0];
    SQLiteDatabase db = null;
    public SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    String sql = "", tr_date = "", scan_date = "", scan_time = "", unique_id = "", pk_user = "", user = "", user_id = "", str_reqNo = "", wh_in_m_pk = "", wh_out_m_pk = "";
    EditText edt_BC;
    TextView tv_reqNo, tv_reqBal, tv_reqQty, msg_error, _txtTime, txtWhIn, txtWhOut;
    TextView _txtDate, _txtError, _txtTT, _txtSent, _txtRemain, _txtTotalGridBot, _txtTotalQtyBot, _txtTotalGridMid;
    //    String data[][] = new String[0][0];
    String data1[] = new String[0];
    int timeUpload = 0;
    Button btnDelAll, btnMakeSlip, btnViewStt, btnList, btnInquiry, btnListBot;
    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for gwMActivity fragment
        rootView = inflater.inflate(R.layout.fragment_stock_transfer_req, container, false);
        edt_BC = (EditText) rootView.findViewById(R.id.editBC);
        tv_reqNo = (TextView) rootView.findViewById(R.id.txt_reqNo);
        tv_reqBal = (TextView) rootView.findViewById(R.id.txt_reqBal);
        tv_reqQty = (TextView) rootView.findViewById(R.id.txt_reqQty);
        txtWhIn = (TextView) rootView.findViewById(R.id.txtWHIn);
        txtWhOut = (TextView) rootView.findViewById(R.id.txtWHOut);
        _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);

        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);

        msg_error = (TextView) rootView.findViewById(R.id.txtError);
        msg_error.setTextColor(Color.RED);
        msg_error.setTextSize((float) 16.0);
        msg_error.setText("");

        _txtDate = (TextView) rootView.findViewById(R.id.txtDate);
        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateFormat("dd-MM-yy");
        _txtDate.setText(formattedDate);
        _txtTime = (TextView) rootView.findViewById(R.id.txtTime);

        edt_BC.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        msg_error.setText("");
                        if (edt_BC.getText().toString().equals("")) {
                            msg_error.setText("Pls scan barcode!");
                        } else {
                            onSaveData();
                        }
                    }
                    return true;
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // gwMActivity is for backspace
                    edt_BC.clearFocus();
                    Thread.interrupted();

                }
                return false;
            }

        });

        OnShowGridHeader();
        init_color();
        OnShowGridDetail();
        OnShowScanIn();
        OnShowScanLog();

        timeUpload = hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);


        return rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run() {
            if (flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload * 1000);
        }
    };

    private void init_color() {
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));

    }

    private void doStart() {
        try {
            if(str_reqNo.equals(""))
                return;
            flagUpload = false;//waiting data server response

            db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            cursor2 = db2.rawQuery("select PK, ITEM_BC,REQ_NO  from INV_TR where DEL_IF=0 and TR_TYPE='" + formID + "' and sent_yn = 'N' and ITEM_BC is not null order by PK asc LIMIT 20", null);
            System.out.print("\n\n\n cursor2.count: " + String.valueOf(cursor2.getCount()));


            if (cursor2.moveToFirst()) {

                //set value message
                msg_error = (TextView) rootView.findViewById(R.id.txtError);
                msg_error.setTextColor(Color.RED);
                msg_error.setText("");

                data1 = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                ///// TODO: 2016-01-13  view
                do {
                    flag = true;
                    for (int i = 0; i < cursor2.getColumnCount(); i++) {
                        if (para.length() <= 0) {
                            if (cursor2.getString(i) != null)
                                para += cursor2.getString(i) + "|";
                            else
                                para += "|";
                        } else {

                            if (flag == true) {
                                if (cursor2.getString(i) != null) {
                                    para += cursor2.getString(i);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor2.getString(i) != null)
                                    para += "|" + cursor2.getString(i);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + bsUserID;
                    para += "*|*";

                } while (cursor2.moveToNext());
                //////////////////////////
                para += "|!LG_MPOS_UPLOAD_ST_TRANS_REQ";
                data1[j] = para;
                //System.out.print("\n\n\n para upload GD: " + para);
//                Log.e("para upload STOCK TRANS INV: ", para);


                if (CNetwork.isInternet(tmpIP, checkIP)) {
                    ServerAsyncTask task = new ServerAsyncTask(gwMActivity, this);
                    task.execute(data1);
                    // Write your code here to invoke YES event
                    gwMActivity.alertToastShort("Send to Server");
                } else {
                    flagUpload = true;
                    gwMActivity.alertToastLong("Network is broken. Please check network again !");
                }

            } else {
                flagUpload = true;
            }
            cursor2.close();
            db2.close();

        } catch (Exception ex) {
            msg_error.setText(ex.getMessage());
            Log.e("Bug Crash App:", ex.getMessage());
        }

    }

    public void onSaveData() {

        if (edt_BC.getText().equals("")) {
            msg_error.setText("Pls Scan Barcode!");
            return;
        }
        String str_scan = edt_BC.getText().toString().toUpperCase();
        int length = edt_BC.getText().length();
        String req_no = str_scan.substring(1, length);

        try {
            if (str_scan.equals("")) {
                msg_error.setText("Pls Scan Barcode!");
                return;
            }


            if (str_reqNo.equals("") &&  str_scan.indexOf("S") == 0) {
                String slip_no = "", wh_out_name = "", wh_in_name = "";
                String para = "1,LG_MPOS_SEL_STOCK_TRANS_REQ,1," + req_no;
                gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                JResultData dtLocGroup = networkThread.execute(para).get();
                List<JDataTable> jdt = dtLocGroup.getListODataTable();
                JDataTable dt = jdt.get(0);

                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                String check_reqNo = "SELECT PK,SLIP_NO,CUST_PK,CUST_NAME,SUPPLIER_PK,SUPPLIER_NAME FROM INV_TR where  tr_type='" + formID + "' AND (status='000' or status=' ')  and REQ_NO ='" + req_no + "' and ITEM_BC is null";
                Cursor cursor = db.rawQuery(check_reqNo, null);
                if (cursor != null && cursor.moveToFirst()) {
                    if(dt.totalrows >0){
                        wh_out_m_pk = dt.records.get(0).get("out_wh_pk_m").toString();
                        wh_out_name = dt.records.get(0).get("out_wh_name_m").toString();
                        wh_in_m_pk = dt.records.get(0).get("in_wh_pk_m").toString();
                        wh_in_name = dt.records.get(0).get("in_wh_name_m").toString();
                    }

                    str_reqNo = req_no;
                    tv_reqNo.setText(str_reqNo);
                    txtWhOut.setText(wh_out_name);
                    txtWhIn.setText(wh_in_name);
                    scan_date = CDate.getDateyyyyMMdd();
                    OnShowGridDetail();
                    OnShowScanIn();
                    gwMActivity.alertToastLong("ReqNo is exist!!!");
                    db.close();
                } else {
                    if (dt.totalrows > 0) {
                        for (int i = 0; i < dt.totalrows; i++) {
                            try {
                                scan_date = CDate.getDateyyyyMMdd();
                                scan_time = CDate.getDateYMDHHmmss();
                                db.execSQL("INSERT INTO INV_TR(TLG_GD_REQ_M_PK,TLG_GD_REQ_D_PK,REQ_NO,TR_ITEM_PK,ITEM_CODE,ITEM_NAME,REQ_QTY,SCANNED,REQ_BAL,TR_LOT_NO,TR_WH_OUT_NAME,TR_WH_OUT_PK,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_OUT_NAME,"
                                        + "TR_WH_IN_NAME,TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK,TLG_IN_WHLOC_IN_NAME,SLIP_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                                        + "VALUES('"
                                        + dt.records.get(i).get("req_m_pk").toString() + "','"
                                        + dt.records.get(i).get("req_d_pk").toString() + "','"
                                        + dt.records.get(i).get("slip_no").toString() + "',"
                                        + dt.records.get(i).get("item_pk").toString() + ",'"
                                        + dt.records.get(i).get("item_code").toString() + "','"
                                        + dt.records.get(i).get("item_name").toString() + "','"
                                        + dt.records.get(i).get("req_qty").toString() + "','"
                                        + dt.records.get(i).get("scanned").toString() + "','"
                                        + dt.records.get(i).get("req_bal").toString() + "','"
                                        + dt.records.get(i).get("lot_no").toString() + "','"
                                        + dt.records.get(i).get("out_wh_name_d").toString() + "','"
                                        + dt.records.get(i).get("out_wh_pk_d").toString() + "','"
                                        + dt.records.get(i).get("out_loc_pk").toString() + "','"
                                        + dt.records.get(i).get("out_loc_name").toString() + "','"
                                        + dt.records.get(i).get("in_wh_name_d").toString() + "','"
                                        + dt.records.get(i).get("in_wh_pk_d").toString() + "','"
                                        + dt.records.get(i).get("in_loc_pk").toString() + "','"
                                        + dt.records.get(i).get("in_loc_name").toString() + "','-','"
                                        + scan_date + "','"
                                        + scan_time + "','"
                                        + "N" + "','"
                                        + " " + "','"
                                        + formID + "');");
                            } catch (Exception ex) {
                                msg_error.setText("Save Error!!!" + ex.toString());
                            }
                        }

                        db.close();
                        str_reqNo = dt.records.get(0).get("slip_no").toString();
                        wh_out_m_pk = dt.records.get(0).get("out_wh_pk_m").toString();
                        txtWhOut.setText(dt.records.get(0).get("out_wh_name_m").toString());
                        wh_in_m_pk = dt.records.get(0).get("in_wh_pk_m").toString();
                        txtWhIn.setText(dt.records.get(0).get("in_wh_name_m").toString());
                        OnShowGridDetail();
                        OnShowScanLog();
                        OnShowScanIn();
                        tv_reqNo.setText(req_no);
                    } else {
                        msg_error.setText("Req No '" + req_no + "' not exist !!!");
                        str_reqNo = "";
                        return;
                    }
                }

                edt_BC.getText().clear();
                edt_BC.requestFocus();
                return;
            }

            if (str_reqNo.equals("")) {
                msg_error.setText("Please scan Request first !!!");
                edt_BC.getText().clear();
                edt_BC.requestFocus();
                return;
            } else {
                boolean isExist = hp.isExistBarcode(str_scan, formID);// check barcode exist in data
                if (isExist)// exist data
                {
                    alertRingMedia();
                    msg_error.setText("Barcode exist in database!");
                    edt_BC.getText().clear();
                    edt_BC.requestFocus();
                    return;
                } else {

                    if (str_scan.length() > 20) {
                        msg_error.setText("Barcode has length more 20 char !");
                        edt_BC.getText().clear();
                        edt_BC.requestFocus();
                        return;
                    }
                    if (OnSave()) {
                        msg_error.setText("Save success.");
                        edt_BC.getText().clear();
                        edt_BC.requestFocus();
                        OnShowScanLog();
                    }
                }
            }

        } catch (Exception ex) {
            msg_error.setText("Save Error!!!");
            Log.e("OnSaveBC", ex.getMessage());
        } finally {

            edt_BC.getText().clear();
            edt_BC.requestFocus();
        }
    }

    public boolean OnSave() {
        boolean kq = false;
        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        try {

            scan_date = CDate.getDateyyyyMMdd();
            scan_time = CDate.getDateYMDHHmmss();

            // get data input control to var then insert db
            String item_bc = edt_BC.getText().toString().toUpperCase();

            db.execSQL("INSERT INTO INV_TR(ITEM_BC,REQ_NO,TR_WH_OUT_PK,TR_WH_IN_PK,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                    + "VALUES('"
                    + item_bc + "','"
                    + str_reqNo + "','"
                    + wh_out_m_pk + "','"
                    + wh_in_m_pk + "','"
                    + scan_date + "','"
                    + scan_time + "','"
                    + "N" + "','"
                    + " " + "','"
                    + formID + "');");

            db.close();
            kq = true;
        } catch (Exception ex) {
            msg_error.setText("Save Error!!!");
            kq = false;
        }
        return kq;
    }

    public void onMakeSlip(View view) {
        //Check het Barcode send to server
        if (checkRecordGridView(R.id.grdScanLog)) {
            gwMActivity.alertToastLong(gwMActivity.getResources().getString(R.string.tvAlertMakeSlip));
            return;
        }

        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        String title = "Confirm Make Slip...";
        String mess = "Are you sure you want to Make Slip ???";
        alertDialogYN(title, mess, "onMakeSlip");
    }

    public void ProcessMakeSlip() {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if (queue.size() > 0) {
        } else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = " select  TLG_GD_REQ_D_PK,TR_ITEM_PK,TR_QTY, UOM, TR_LOT_NO, TR_WH_OUT_PK, TR_WH_IN_PK,UNIT_PRICE,BC_TYPE,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK,REQ_NO, TLG_SA_SALEORDER_D_PK,GRADE,EXECKEY " +
                        " from " +
                        " (select TLG_GD_REQ_D_PK,TR_ITEM_PK, COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO,UNIT_PRICE, UOM, TR_WH_OUT_PK,TR_WH_IN_PK,BC_TYPE,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK,REQ_NO,TLG_SA_SALEORDER_D_PK,GRADE,EXECKEY " +
                        " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND ITEM_BC IS NOT NULL AND TR_TYPE = '" + formID + "' AND STATUS='000' and SLIP_NO IN ('-','') " +
                        " GROUP BY TLG_GD_REQ_D_PK,TR_ITEM_PK,TR_LOT_NO, UNIT_PRICE, UOM, TR_WH_OUT_PK,TR_WH_IN_PK,BC_TYPE,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK,REQ_NO,TLG_SA_SALEORDER_D_PK,GRADE,EXECKEY  )";

                cursor = db.rawQuery(sql, null);

                int count = cursor.getCount();
                data1 = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_wh_pk = cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK"));
                            String ex_in_wh_pk = cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK"));
                            String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            String ex_bc_type = cursor.getString(cursor.getColumnIndex("BC_TYPE"));
                            hp.updateExeckeyStTransfer(formID, ex_item_pk, ex_wh_pk, ex_in_wh_pk, ex_lot_no, ex_bc_type, unique_id);

                            para += "|" + unique_id;
                        } else {
                            para += "|" + execkey;
                        }
                        para += "|" + wh_out_m_pk;
                        para += "|" + wh_in_m_pk;
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!" + "LG_MPOS_PRO_ST_TRANS_REQ_MSLIP";
                    data1[j] = para;
                    System.out.print("\n\n ******para make slip stock out: " + para);
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
               // appendLog(data1[0]);
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity, this);
                task.execute(data1);
            } else {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }

        }
    }
    public void appendLog(String text)
    {
        File logFile = new File("sdcard/log.file");
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
            /* BufferedWriter for performance, true to set append to file flag */
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void CountSendRecord() {
        flagUpload = true;
        // total scan log
        _txtSent = (TextView) rootView.findViewById(R.id.txtSent);
        _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid = Integer.parseInt(_txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        int countBot = Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Send: " + (countMid + countBot));

        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]", ""));

        _txtTT = (TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: " + (countMid + countBot + countRe));

    }

    public void Alert_OutOfQty(boolean alert) {
        if (!alert) {
            gwMActivity.alertToastLong("Out Of Qty!!!");
        }
    }

    public void OnShowScanLog() {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' AND ITEM_BC IS NOT NULL AND REQ_NO = '" + str_reqNo + "' order by PK desc LIMIT 20", null);// TLG_LABEL

            int count = cursor.getCount();
            //Log.e("count log",String.valueOf(count));
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));


                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            // total scan log
//            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
//            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+formID+"' and SENT_YN='N' ; ";
//            cursor = db.rawQuery(sql, null);
//            count = cursor.getCount();

//            txtRem=(TextView) rootView.findViewById(R.id.txtRemain);
//            txtRem.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
            //Log.e("Grid Scan log Error -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowScanIn() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select PK, ITEM_BC, ITEM_CODE,  STATUS,TR_LOT_NO, SLIP_NO, INCOME_DATE, CHARGER, TR_WH_OUT_PK, TR_WH_IN_PK, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND ITEM_BC is not null AND REQ_NO='" + str_reqNo + "' AND SENT_YN='Y' AND SCAN_DATE = '" + scan_date + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[0], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK")), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")), scrollableColumnWidths[5], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight, 0));
                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvStatus = (TextView) tr1.getChildAt(3); //STATUS
                            String str_Status = tvStatus.getText().toString();

                            if (str_Status.equals("004") || str_Status.equals("005")) {
                                TextView tvBC = (TextView) tr1.getChildAt(1);
                                TextView tvPK = (TextView) tr1.getChildAt(10);
                                String value = tvBC.getText().toString() + "|" + str_Status;
                                String valuePK = tvPK.getText().toString();
                                if (hp.isExistBarcode(tvBC.getText().toString(), formID)) {
                                    gwMActivity.onPopAlert(tvBC.getText().toString() + "ton tai luoi thu 3 !!!");
                                    return;
                                }
                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255, 99, 71));
                                }
                                if (queueMid.size() > 0) {

                                    if (queueMidBC.contains(value)) {
                                        Log.e("Contain Click", value);
                                        queueMid.remove(valuePK);
                                        queueMidBC.remove(value);

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK);
                                        }
                                        duplicate = true;
                                    }
                                }
                                if (!duplicate) {
                                    //Log.e("PK Click", valuePK);
                                    queueMid.add(valuePK);
                                    queueMidBC.add(value);
                                }
                            }
                            gwMActivity.alertToastShort(queueMid.size() + "");
                        }
                    });
                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND ITEM_BC is not null AND SCAN_DATE = '" + scan_date + "' AND TR_TYPE = '" + formID + "' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
            _txtTotalGridMid.setText("Total: " + count);
            _txtTotalGridMid.setTextColor(Color.BLUE);
            _txtTotalGridMid.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowGridDetail() {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        scrollablePart.removeAllViews();

        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

        sql = "   SELECT  TR_ITEM_PK,ITEM_CODE,REQ_QTY,SCANNED,REQ_BAL,TR_LOT_NO,SLIP_NO,TR_WH_OUT_PK,TR_WH_OUT_NAME,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_OUT_NAME," +
                " TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK,TLG_IN_WHLOC_IN_NAME, " +
                " (SELECT SUM(TR_QTY) FROM INV_TR B WHERE ITEM_BC IS NOT NULL AND REQ_NO = '" + str_reqNo + "' AND B.TR_ITEM_PK = A.TR_ITEM_PK AND STATUS = '000' AND tr_type='" + formID + "') CURRENT_SCAN," +
                " (SELECT COUNT(*) FROM INV_TR B WHERE ITEM_BC IS NOT NULL AND REQ_NO = '" + str_reqNo + "' AND B.TR_ITEM_PK = A.TR_ITEM_PK AND STATUS = '000' AND tr_type='" + formID + "') BC_NUM" +
                "   FROM INV_TR A" +
                "    WHERE tr_type='" + formID + "' AND (status='000' or status=' ')  and REQ_NO ='" + str_reqNo + "' and ITEM_BC is null";

        cursor = db.rawQuery(sql, null);
        int count = cursor.getCount();

        double req_qty = 0;
        float req_bal = 0;

        if (cursor != null && cursor.moveToFirst()) {
            for (int i = 0; i < count; i++) {
                req_bal = req_bal + Float.parseFloat(cursor.getString(cursor.getColumnIndex("REQ_BAL")).toString());

                row = new TableRow(gwMActivity);
                row.setLayoutParams(wrapWrapTableRowParams);
                row.setGravity(Gravity.CENTER);
                row.setBackgroundColor(Color.LTGRAY);
                row.addView(makeTableColWithText(String.valueOf(i + 1), scrollableColumnWidths[0], fixedRowHeight, 0));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[4], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REQ_QTY")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCANNED")), scrollableColumnWidths[3], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REQ_BAL")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CURRENT_SCAN")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("BC_NUM")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")), scrollableColumnWidths[3], fixedRowHeight, -1));
                if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_OUT_NAME")), scrollableColumnWidths[2], fixedRowHeight, -1));
                }
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[3], fixedRowHeight, -1));
                if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_NAME")), scrollableColumnWidths[2], fixedRowHeight, -1));
                }
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK")), 0, fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_OUT_PK")), 0, fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")), 0, fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_PK")), 0, fixedRowHeight, -1));

                row.setOnClickListener(new View.OnClickListener() {
                    TextView tv11;
                    boolean duplicate = false;

                    @Override
                    public void onClick(View v) {

//                        duplicate = false;
//                        TableRow tr1 = (TableRow) v;
//                        TextView tvLotNo = (TextView) tr1.getChildAt(7); //LOT_NO
//                        String st_LotNO = tvLotNo.getText().toString();
//                        TextView tvSlipNo = (TextView) tr1.getChildAt(8); //SLIP NO
//                        String st_SlipNO = tvSlipNo.getText().toString();
//
//                        TextView tvItemPK = (TextView) tr1.getChildAt(13); //TR_ITEM_PK
//                        String item_pk = tvItemPK.getText().toString();
//                        TextView tvWh_out_Pk = (TextView) tr1.getChildAt(14); //WH_OUT_PK
//                        String wh_out_PK = tvWh_out_Pk.getText().toString();
//                        TextView tvWhLocOutPk = (TextView) tr1.getChildAt(15); //WH_LOC_OUT_PK
//                        String whLocOutPK = tvWhLocOutPk.getText().toString();
//                        TextView tvWh_in_Pk = (TextView) tr1.getChildAt(16); //WH_IN_PK
//                        String wh_in_PK = tvWh_in_Pk.getText().toString();
//                        TextView tvWhLocInPk = (TextView) tr1.getChildAt(17); //WH_LOC_IN_PK
//                        String whLocInPK = tvWhLocInPk.getText().toString();
//                        Intent openNewActivity = new Intent(v.getContext(), gw.genumobile.com.views.PopDialogGrid.class);
//                        //send data into Activity
//                        openNewActivity.putExtra("TYPE", formID);
//                        openNewActivity.putExtra("ITEM_PK", item_pk);
//                        openNewActivity.putExtra("LOT_NO", st_LotNO);
//                        openNewActivity.putExtra("WH_OUT_PK", wh_out_PK);
//                        openNewActivity.putExtra("WH_IN_PK", wh_in_PK);
//                        openNewActivity.putExtra("SLIP_NO", st_SlipNO);
//                        openNewActivity.putExtra("USER", user);
//                        openNewActivity.putExtra("WH_LOC_OUT_PK", whLocOutPK);
//                        openNewActivity.putExtra("WH_LOC_IN_PK", whLocInPK);
//                        startActivityForResult(openNewActivity, REQUEST_CODE);
                    }
                });


                row.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return false;
                    }
                });

                req_qty += Double.parseDouble(cursor.getString(cursor.getColumnIndex("REQ_QTY")));

                scrollablePart.addView(row);
                cursor.moveToNext();
            }
        }
        tv_reqQty.setText(String.valueOf(req_qty));
        tv_reqBal.setText(String.valueOf(req_bal));
    }

    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhOut), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhIn), scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvReqQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvScanned), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvBalance), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvCurrentScan), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvBCNum), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvMakeSlip), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhOut), scrollableColumnWidths[3], fixedHeaderHeight));
        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
            row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhLocOut), scrollableColumnWidths[2], fixedHeaderHeight));
        }
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhIn), scrollableColumnWidths[3], fixedHeaderHeight));
        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
            row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhLocIn), scrollableColumnWidths[2], fixedHeaderHeight));
        }
        row.addView(makeTableColHeaderWithText("TR_ITEM_PK", 0, fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TR_WH_OUT_PK", 0, fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TLG_IN_WHLOC_OUT_PK", 0, fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TR_WH_IN_PK", 0, fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TLG_IN_WHLOC_IN_PK", 0, fixedRowHeight));
        scrollablePart.addView(row);
    }

    public void onDelAll(View view) {
        if (!hp.isCheckDelete(formID)) return;

        String title = "Confirm Delete..";
        String mess = "Are you sure you want to delete ???";
        alertDialogYN(title, mess, "onDelAll");
    }

    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onApprove")) {
//                    Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
//                    btnMSlip.setVisibility(View.GONE);
                }
                if (_type.equals("onDelAll")) {
                    ProcessDelete();
                }
                if (_type.equals("onMakeSlip")) {
                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void ProcessDelete() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();
                data1 = new String[myLst.length];
                for (int i = 0; i < myLst.length; i++) {
                    db.execSQL("DELETE FROM INV_TR where TR_TYPE='" + formID + "' and pk=" + myLst[i]);
                }

            } else {
                //db.execSQL("DELETE FROM INV_TR where TR_TYPE='"+formID+"';");
                cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "' and SLIP_NO in('-','') and STATUS ='000' ", null);
                int count = cursor.getCount();
                String para = "";
                boolean flag = false;
                int j = 0;
                if (cursor.moveToFirst()) {
                    data1 = new String[1];
                    do {

                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + user;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_UPD_INV_TR_DEL";
                    data1[j] = para;
                    Log.e("para update||delete: ", para);
                }
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("Delete All :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity, this);
            task.execute(data1);
        } else {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }
    }

    public void onDeleteAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();

            } else {
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='" + formID + "';");
            }
        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            OnShowScanLog();
            OnShowScanIn();
            OnShowGridDetail();
            CountSendRecord();
            tv_reqQty.setText("");
            tv_reqNo.setText("");
            tv_reqBal.setText("");
            txtWhIn.setText("");
            txtWhOut.setText("");
            str_reqNo = "";

        }
    }

    public void onClickViewStt(View view) {



        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ///////////////////////////////////////////////
    public void onListGridMid(View view) {
        if (!checkRecordGridView(R.id.grdScanIn)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = frGridListViewMid.newInstance(1, formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    ///////////////////////////////////////////////
    public void onListGridBot(View view) {
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment = new GridListViewBot();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");

    }

    ////////////////////////////////////////////////
    //region ---------Select Mid Status 005-------
    public void onClickSelectMid(View view) {
        final Object[] myLst = queueMid.toArray();
        final Object[] myLstStatus = queueMidBC.toArray();
        if (myLst.length <= 0) return;
        String str = "Are you sure you want to select ";

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Select ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);

        // Setting Negative "YES" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                try {

                    queueMid = new LinkedList();
                    queueMidBC = new LinkedList();
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                    for (int i = 0; i < myLst.length; i++) {
                        String val = myLst[i].toString();
                        String valStatus = myLstStatus[i].toString();
                        String _pk = valStatus.split("\\|")[0].toString();
                        String _status = valStatus.split("\\|")[1].toString();
                        if (_status.equals("005")) {
                            sql = "UPDATE INV_TR set  REMARKS='Not FIFO',SLIP_NO='-'   where  PK = " + val;
                            db.execSQL(sql);
                        }
                        /*
                        if(_status.equals("004")) {
                            //region ----check 006
                            sql="UPDATE INV_TR set  REMARKS='LotNO Wrong Format'   where  PK = " + _pk;
                            db.execSQL(sql);
                            //.endregion
                        }
                        if(_status.equals("008"))
                        {
                            sql="UPDATE INV_TR set STATUS = '000', REMARKS='Qty larger'  where PK = " + _pk;
                            db.execSQL(sql);

                            OnShowScanIn();
                            OnShowScanAccept();
                        }
                        */
                    }
                    ProcessStatus005(myLst);

                } catch (Exception ex) {
                    gwMActivity.alertToastLong("Select Mid :" + ex.getMessage());
                } finally {
                    db.close();
                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }

    public void ProcessStatus005(Object[] myLst) {
        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        data1 = new String[1];
        int j = 0;
        String para = "";
        for (int rw = 0; rw < myLst.length; rw++) {

            String val = myLst[rw].toString();
            Log.e("005", val);
            sql = "select PK, ITEM_BC,REQ_NO,STATUS from INV_TR where DEL_IF=0  and PK = " + val;
            cursor2 = db2.rawQuery(sql, null);
            if (cursor2.moveToFirst()) {

                boolean flag = false;
                do {
                    // labels.add(cursor.getString(1));
                    flag = true;
                    for (int i = 0; i < cursor2.getColumnCount(); i++) {
                        if (para.length() <= 0) {
                            if (cursor2.getString(i) != null)
                                para += cursor2.getString(i) + "|";
                            else
                                para += "|";
                        } else {

                            if (flag == true) {
                                if (cursor2.getString(i) != null) {
                                    para += cursor2.getString(i);
                                    flag = false;
                                } else {
                                    para += "|";
                                    flag = false;
                                }
                            } else {
                                if (cursor2.getString(i) != null)
                                    para += "|" + cursor2.getString(i);
                                else
                                    para += "|";
                            }
                        }
                    }
                    para += "|" + deviceID;
                    para += "|" + user;
                    para += "*|*";

                } while (cursor2.moveToNext());

            }
        }
        //////////////////////////
        para += "|!" + "LG_MPOS_UPL_ST_TRANS_INV";
        data1[j++] = para;
        System.out.print("\n\n\n para upload: " + para);

        cursor2.close();
        db2.close();

        ServerAsyncTask task = new ServerAsyncTask(gwMActivity, this);
        task.execute(data1);
    }
    //endregion

    public void onClickInquiry(View view) {
        if (!checkRecordGridView(R.id.grdScanAccept)) return;

        Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        startActivity(openNewActivity);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
                onClickInquiry(view);
                break;

            case R.id.btnApprove:
                break;

            case R.id.btnViewStt:
                onClickViewStt(view);
                break;

            case R.id.btnListBot:
                onListGridBot(view);
                break;

            case R.id.btnDelBC:
                break;

            case R.id.btnList:
                onListGridMid(view);
                break;

            case R.id.btnDelAll:
                onDelAll(view);
                break;

            case R.id.btnMakeSlip:
                onMakeSlip(view);
                break;

            default:
                break;
        }
    }
}
