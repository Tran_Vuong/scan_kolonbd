package gw.genumobile.com.views.productresult;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.adapters.adapter_defect;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.models.gwfunction;
import gw.genumobile.com.services.HandlerWebService;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.utils.ItemDefect;
import gw.genumobile.com.views.gwFragmentDialog;
import gw.genumobile.com.views.gwcore.BaseGwActive;

public class PopSewingDefectActivity extends gwFragmentDialog implements View.OnClickListener {
    //region Variable
    private static final int ACTION_TAKE_PHOTO_B = 1;
    private static final int SIGNATURE_ACTIVITY = 2;
    public int REQUEST_CODE = 1;
    public int RESULT_CODE = 1;
    protected int[] bsColors = new int[]{0x30FF0000, 0x300000FF, 0xCCFFFF, 0x99CC33, 0x00FF99};
    protected int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40, 50};
    TextView bsTextView;
    protected int fixedRowHeight = 70; //70|30
    protected int fixedHeaderHeight = 80; //80|40
    SQLiteDatabase db = null;
    Cursor cursor, cursor2;
    //endregion

    Button btnDefect01, btnDefect02, btnDefect03, btnDefect04, btnDefect05, btnDefect06, btnDefect07, btnDefect08, btnDefect09, btnDefect10;
    Button btnDefect11, btnDefect12, btnDefect13, btnDefect14, btnDefect15, btnDefect16, btnDefect17, btnDefect18, btnDefect19, btnDefect20;
    Button btnDefect, btnClose, btnCapture, btnShowCapture, btnFromTime, btnToTime;
    GridView grd_defect;
    adapter_defect adapter;

    EditText edQty;
    TextView txtDate, edt_WINo, txtHour, lbSewingline;
    List<ItemDefect> lst_defect = new ArrayList<ItemDefect>();

    private String defectVal = "00", DEF_NAME = "", URL_PIC = "";
    private String mCurrentPhotoPath="";

    private Bitmap imageBitmap;
    private ImageView mImageView;
    String slipIW = "", prodTime = "", timeFrom = "", timeTo = "", prodDate = "", WI_PK = "", HOURLY = "", HOURLY_TYPE = "", LINE="", YMD = "", WorkShift = "";
    String item_pk = "", work_process_pk = "", line_pk = "",time_sheet_seq ="", DEFECT_D_PK="",po_no="",    wh_pk ="";
    String[] LINE_DETAIL;
    int previous_position = 0;
    View vContent;
    Activity gwAc;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gwAc=getActivity();
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vContent = inflater.inflate(R.layout.activity_pop_sewing_defect, container, false);
        Toolbar toolbar = (Toolbar) vContent.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                    OnComeBackResult(null);
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);
        toolbar.setTitle("DEFECT");
        //this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        mImageView = new ImageView(gwMActivity);
        //this.getCurrentFocus().clearFocus();
        OnShowHeader();
        createControl();
        createButton();
//        resetButtonDefect();

        slipIW = getArguments().getString("SLIPWI");
        prodTime = getArguments().getString("PRODTIME");
        prodDate =getArguments().getString("PROD_DATE");
        timeFrom =getArguments().getString("TIME1");
        timeTo = getArguments().getString("TIME2");
        WI_PK = getArguments().getString("WI_PK");
        HOURLY = getArguments().getString("HOURLY");

        HOURLY_TYPE = getArguments().getString("HOURLY_TYPE");
        LINE = getArguments().getString("LINE");
        LINE_DETAIL =getArguments().getStringArray("LINE_DETAIL");
        YMD = getArguments().getString("YMD");
        WorkShift =getArguments().getString("WS");
        item_pk = getArguments().getString("IT_ITEM_PK");
        line_pk =getArguments().getString("LINE_PK");
        work_process_pk = getArguments().getString("WORK_PROCESS");
        po_no =getArguments().getString("po_no");
        wh_pk = getArguments().getString("wh_pk");

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        if(HOURLY_TYPE.equals("00")) time_sheet_seq = "0";
        else time_sheet_seq = "1";


        grd_defect = (GridView)vContent.findViewById(R.id.grd_lst_defect);
        load_defect();
        if (lst_defect.size() > 0) {
            adapter = new adapter_defect(gwMActivity.getApplicationContext(), R.layout.item_defect, lst_defect);
            grd_defect.setAdapter(adapter);
        }
        grd_defect.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(previous_position==position){
                    TextView tv = (TextView) view.findViewById(R.id.txt_defect_name);
                    tv.setBackgroundColor(Color.GRAY);
                }else{
                    GridView grd = (GridView) view.getParent();
                    TextView tv = (TextView) view.findViewById(R.id.txt_defect_name);
                    tv.setBackgroundColor(Color.GRAY);
                    View v = grd.getChildAt(previous_position);
                    TextView tv1 = (TextView) v.findViewById(R.id.txt_defect_name);
                    tv1.setBackgroundColor(Color.LTGRAY);
                    previous_position = position;
                }
                defectVal = lst_defect.get(position).getPk();
            }
        });

        //set Text
        edt_WINo.setText(slipIW);
        btnFromTime.setText(timeFrom);
        btnToTime.setText(timeTo);
        lbSewingline.setText(LINE);
        txtDate.setText(prodDate);
        txtHour.setText(prodTime);
        if (HOURLY_TYPE.equals("00")) {
            btnFromTime.setSelected(true);
            btnToTime.setSelected(false);
            btnFromTime.setBackgroundColor(Color.parseColor("#3366CC"));
            btnToTime.setBackgroundColor(Color.parseColor("#dddddd"));
        } else {
            btnFromTime.setSelected(false);
            btnToTime.setSelected(true);
            btnFromTime.setBackgroundColor(Color.parseColor("#dddddd"));
            btnToTime.setBackgroundColor(Color.parseColor("#3366CC"));
        }
        //Show Content
        OnShowContet();
    return vContent;
//        if (shouldAskPermission()) {
//            ActivityCompat.requestPermissions(gwMActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
//        }
    }

    private void load_defect() {
        String l_para = "1,LG_MPOS_SEL_DEFECT_LST,1," + WI_PK;
        try {
            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
            List<JDataTable> jdt = dtLocGroup.getListODataTable();
            JDataTable dt = jdt.get(0);
            for (int i = 0; i < dt.totalrows; i++) {
                ItemDefect item = new ItemDefect();
                item.setPk(dt.records.get(i).get("pk").toString());
                item.setDefect_id(dt.records.get(i).get("defect_id").toString());
                item.setDefect_name(dt.records.get(i).get("defect_name").toString());
                lst_defect.add(item);
            }

        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WI Error: ", ex.getMessage());
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnDefect:
                if (defectVal.equals("00")) {
                    Toast.makeText(gwMActivity, "Please select Defect Type first!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edQty.getText().toString().equals("") || edQty.getText().toString() == null) {
                    Toast.makeText(gwMActivity, "Please input Qty!!", Toast.LENGTH_SHORT).show();
                    return;
                }

                SaveDefect();
                break;

            case R.id.btnCapture:
                if (defectVal.equals("00")) {
                    Toast.makeText(gwMActivity, "Please select Defect Type first!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                dispatchTakePictureIntent();
                break;

            case R.id.btnShowCapture:

                if (imageBitmap == null || imageBitmap.getByteCount() == 0) return;
                loadPhoto(mImageView);
                break;

            case R.id.btnClose:

                OnComeBackResult(v);
                break;
            default:
                break;
        }

    }

    public void OnComeBackResult(View v) {
      /*  Intent i = new Intent();
        i.putExtra("line", LINE_DETAIL);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);*/
        dismiss();
        SewingResultActivity fr =(SewingResultActivity) getTargetFragment();
        fr.loadGridData();
    }

    private void createControl() {
        txtDate = (TextView)vContent.findViewById(R.id.txtDate);
        edQty = (EditText)vContent.findViewById(R.id.edt_Qty);
        edt_WINo = (TextView)vContent.findViewById(R.id.edt_WINo);
        txtHour = (TextView)vContent.findViewById(R.id.txtHour);
        lbSewingline = (TextView)vContent.findViewById(R.id.lbSewingline);
        btnFromTime = (Button)vContent.findViewById(R.id.btnFromTime);
        btnToTime = (Button)vContent.findViewById(R.id.btnToTime);
    }

    private void createButton() {
        //////
        btnDefect = (Button)vContent.findViewById(R.id.btnDefect);
        btnDefect.setOnClickListener(this);
        btnClose = (Button)vContent.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
        btnCapture = (Button)vContent.findViewById(R.id.btnCapture);
        btnCapture.setOnClickListener(this);
        btnShowCapture = (Button)vContent.findViewById(R.id.btnShowCapture);
        btnShowCapture.setOnClickListener(this);
        btnFromTime = (Button)vContent.findViewById(R.id.btnFromTime);
        btnToTime = (Button)vContent.findViewById(R.id.btnToTime);
    }

    private void loadPhoto(ImageView imageView) {

        ImageView tempImageView = imageView;


        AlertDialog.Builder imageDialog = new AlertDialog.Builder(gwMActivity);
        LayoutInflater inflater = (LayoutInflater) gwAc.getSystemService(gwAc.LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.custom_fullimage_dialog,
                (ViewGroup)vContent.findViewById(R.id.layout_root));
        ImageView image = (ImageView) layout.findViewById(R.id.imageView1);
        image.setImageDrawable(tempImageView.getDrawable());
        imageDialog.setView(layout);


        imageDialog.create();
        imageDialog.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File f = null;

        try {
            f = setUpPhotoFile();
            mCurrentPhotoPath = f.getAbsolutePath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        } catch (IOException e) {
            e.printStackTrace();
            f = null;
            mCurrentPhotoPath = null;
        }
        startActivityForResult(takePictureIntent, ACTION_TAKE_PHOTO_B);
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private File createImageFile() throws IOException {
        try {
            // Create an image file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = "file:" + image.getAbsolutePath();
            return image;
        } catch (Exception ex) {
            Log.e("File Erorr", ex.toString());
            return null;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTION_TAKE_PHOTO_B && resultCode == Activity.RESULT_OK) {

            Intent intent = new Intent(gwMActivity, EditCapture.class);
            intent.putExtra("URLPhotoPath", mCurrentPhotoPath);
            startActivityForResult(intent, SIGNATURE_ACTIVITY);

            //region ACTION_TAKE_PHOTO_B
            //setPic();
            //galleryAddPic();
            //mCurrentPhotoPath = null;
            //mImageView = new ImageView(gwMActivity);
            //mImageView.setImageBitmap(imageBitmap);
            //endregion

        }
        if (requestCode == SIGNATURE_ACTIVITY && resultCode == Activity.RESULT_OK) {

            System.out.println("----dataType: " + data.getType());
            Bundle bundle = data.getExtras();
            String status = bundle.getString("status");
            mCurrentPhotoPath = bundle.getString("URL_PIC");
            Log.e("----Status------: ", status);
            //mCurrentPhotoPath=status;
            //region ACTION_TAKE_PHOTO_B
            setPic();
            galleryAddPic();
            mCurrentPhotoPath = null;
            mImageView = new ImageView(gwMActivity);
            mImageView.setImageBitmap(imageBitmap);
            //endregion

        }
    }

    private void galleryAddPic() {

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        gwMActivity .sendBroadcast(mediaScanIntent);
    }

    private void setPic() {

    /* There isn't enough memory to open up more than a couple camera photos */
    /* So pre-scale the target bitmap into which the file is decoded */

    /* Get the size of the ImageView */
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

    /* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

    /* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

    /* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

    /* Decode the JPEG file into a Bitmap */
        imageBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
        if (URL_PIC != "")
            URL_PIC = URL_PIC + ";" + mCurrentPhotoPath;
        else
            URL_PIC = mCurrentPhotoPath;
    /* Associate the Bitmap to the ImageView */
        mImageView.setImageBitmap(imageBitmap);
    }

    public void OnShowHeader()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan

        scrollablePart = (TableLayout)vContent.findViewById(R.id.grdHeader);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText("DEFECT CODE", scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("QTY", scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(" # of FILE", scrollableColumnWidths[3], fixedHeaderHeight));
        scrollablePart.addView(row);


    }

    public void OnShowContet()// show data gridview
    {
//        try {
            //black #01101a, red #ee1010
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout)vContent.findViewById(R.id.grdContet);
            scrollablePart.removeAllViews();//remove all view child

            String l_para = "1,LG_MPOS_SEL_DEFECT_RESULT,1," + WI_PK + "|" + YMD + "|" + WorkShift + "|" + HOURLY + "|" + time_sheet_seq;
            try {
                gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                JResultData dtLocGroup = networkThread.execute(l_para).get();
                List<JDataTable> jdt = dtLocGroup.getListODataTable();
                JDataTable dt = jdt.get(0);
                float _qty = 0;
                for (int i = 0; i < dt.totalrows; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText("  " + dt.records.get(i).get("defect_name").toString(), scrollableColumnWidths[4], fixedRowHeight, -1, "#01101a"));
                    row.addView(makeTableColWithText(dt.records.get(i).get("work_qty").toString(), scrollableColumnWidths[1], fixedRowHeight, 1, "#ee1010"));

                    int pic = Integer.parseInt(dt.records.get(i).get("count_pic").toString());
                    String[] arr = null;

                    if (pic!=0) {

                        int screenWidth = getResources().getDisplayMetrics().widthPixels;
                        TableRow.LayoutParams params = new TableRow.LayoutParams(scrollableColumnWidths[3] * screenWidth / 100, fixedRowHeight);
                        params.setMargins(1, 1, 1, 1);
                        GridView grd_pic = new GridView(gwMActivity.getApplicationContext());
                        grd_pic.setBackgroundColor(-1);
                        grd_pic.setLayoutParams(params);
                        grd_pic.setColumnWidth(5);
                        grd_pic.setNumColumns(pic);
                        grd_pic.setPadding(0, 5, 0, 5);
                        String[] arr_pic = new String[pic];

                        TextView tv_pic = new TextView(gwMActivity.getApplicationContext());
                        tv_pic.setText("P");
                        for (int r = 0; r < pic; r++) {
                                arr_pic[r] = "P";
                        }
                        ArrayAdapter<String> da = new ArrayAdapter<String>
                                (gwMActivity, R.layout.item_grid, arr_pic);
                        grd_pic.setAdapter(da);
                        grd_pic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                final int position = i;
                                GridView gridView = (GridView) view.getParent();
                                TableRow tr = (TableRow) gridView.getParent();
                                TextView tv = (TextView) tr.getChildAt(3) ;
                                final String defect_pk = tv.getText().toString();
                                Thread thread 	= new Thread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        try{
                                            imageBitmap 			= null;
                                            String result 			= HandlerWebService.GetImagePosition("TLG_NG_DAILY_INPUT_D", defect_pk,position);
                                            byte[] decodedString 	= android.util.Base64.decode(result, android.util.Base64.DEFAULT);
                                            imageBitmap 			= BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                            mImageView.setImageBitmap(imageBitmap);

                                        }
                                        catch (Exception e)
                                        {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                thread.start();
                                SystemClock.sleep(1000);
                                loadPhoto(mImageView);
                            }
                        });
                        row.addView(grd_pic);

                    } else {
                        row.addView(makeTableColWithText("", scrollableColumnWidths[3], fixedRowHeight, -1, "#01101a"));
                    }
                    row.addView(makeTableColWithText(dt.records.get(i).get("pk").toString(), 0, fixedRowHeight, 1, "#ee1010"));
                    scrollablePart.addView(row);
                    _qty = _qty + Float.parseFloat(dt.records.get(i).get("work_qty").toString());
                }

                row = new TableRow(gwMActivity);
                row.setLayoutParams(wrapWrapTableRowParams);
                row.setGravity(Gravity.CENTER);
                row.setBackgroundColor(Color.LTGRAY);
                row.addView(makeTableColWithText("", scrollableColumnWidths[4], fixedRowHeight, -1, "#01101a"));
                row.addView(makeTableColWithText("", scrollableColumnWidths[1], fixedRowHeight, 1, "#ee1010"));
                row.addView(makeTableColWithText(" ", scrollableColumnWidths[3], fixedRowHeight, -1, "#ee1010"));

                scrollablePart.addView(row);
                //////TOTAL
                row = new TableRow(gwMActivity);
                row.setLayoutParams(wrapWrapTableRowParams);
                row.setGravity(Gravity.CENTER);
                row.setBackgroundColor(Color.LTGRAY);
                row.addView(makeTableColWithText("   TOTAL", scrollableColumnWidths[4], fixedRowHeight, -1, "#01101a"));
                row.addView(makeTableColWithText(String.valueOf(_qty), scrollableColumnWidths[1], fixedRowHeight, 1, "#ee1010"));
                row.addView(makeTableColWithText(" ", scrollableColumnWidths[3], fixedRowHeight, -1, "#ee1010"));

                scrollablePart.addView(row);

            } catch (Exception ex) {
                //  alertToastLong(ex.getMessage().toString());
                Log.e("WI Error: ", ex.getMessage());
            }

    }

    //region -----Init control Table-------
    public TextView makeTableColHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.BLACK);
        bsTextView.setTextSize(18);
        bsTextView.setGravity(Gravity.CENTER);
        bsTextView.setBackgroundColor(Color.parseColor("#B0E2FF"));
        bsTextView.setPadding(0, 2, 0, 2);
        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }

    public TextView makeTableColWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels, int textAlign, String _color) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.parseColor(_color));
        if (textAlign == -1) {
            bsTextView.setGravity(Gravity.LEFT);
            bsTextView.setPadding(10, fixedHeightInPixels / 5, 5, 5);
        } else if (textAlign == 0) {
            bsTextView.setGravity(Gravity.CENTER);
            bsTextView.setPadding(0, fixedHeightInPixels / 5, 0, 5);
        } else {
            bsTextView.setGravity(Gravity.RIGHT);
            bsTextView.setPadding(0, fixedHeightInPixels / 5, 10, 5);
        }
        bsTextView.setTextSize(16);
        bsTextView.setBackgroundColor(-1);

        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }

    public void SaveDefect() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Comfirm Defect");
        // Setting Dialog Message
        alertDialog.setMessage("Do you want save Defect");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (OnSave()) {
                    OnShowContet();
                    edQty.setText("");
                    URL_PIC = "";
                    GridView grd = (GridView)vContent.findViewById(R.id.grd_lst_defect);
                    View v = grd.getChildAt(previous_position);
                    TextView tv1 = (TextView) v.findViewById(R.id.txt_defect_name);
                    tv1.setBackgroundColor(Color.LTGRAY);
                } else
                    Toast.makeText(gwMActivity.getApplication(), "Save Defect Error", Toast.LENGTH_SHORT).show();

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public boolean OnSave() {
//        LG_MPOS_UPD_DEFECT_RESULT
        boolean kq = false;
        DEFECT_D_PK = "";

        try {
            String _qty = edQty.getText().toString();
            String   l_para =String.format("1,%s,1,%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s", gwfunction.PopSewingDefectActivity_S.getValue(),
                    WI_PK , YMD, WorkShift , HOURLY , time_sheet_seq,line_pk , work_process_pk, item_pk , _qty , defectVal , bsUserID,wh_pk,po_no);

            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
           if(dtLocGroup.getResults().equals("F"))
           {
            gwMActivity.alertToastShort(dtLocGroup.getErrors().get(1).values().toString().split("\n")[0].split(":")[1].toString());
                return false;
           }
            List<JDataTable> jdt = dtLocGroup.getListODataTable();

            JDataTable dt = jdt.get(0);
            DEFECT_D_PK = dt.records.get(0).get("pk").toString();
            if(!URL_PIC.equals("")){
                String [] arr_url = URL_PIC.split(";");
                for(int i=0; i<arr_url.length; i++) {
                    String PhotoPath = arr_url[i];
                    Bitmap bmp = BitmapFactory.decodeFile(PhotoPath);
                    UpLoadImage(bmp);
                }
            }
            kq = true;
        } catch (Exception ex) {
            Log.e("Defect error: ", ex.toString());
            kq = false;
        }

        return kq;
    }

    public void UpLoadImage(Bitmap imageBmp){

        final Bitmap bmp = imageBmp;
        if(//!flagUpdateImage ||
                bmp == null
                || bmp.getByteCount() == 0) return;

        final ProgressDialog progress = new ProgressDialog(gwMActivity);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                progress.setMessage("Uploading  image into server, please wait...");
                progress.setCancelable(false);
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setProgress(0);
                progress.show();

            }

            @Override
            protected Void doInBackground(Void... params) {

                //region UPLOAD BIG IMAGE
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.WEBP, 50, out);
                int bytes = bmp.getByteCount();

                byte[] byteArrayImage = new byte[out.size()];
                byteArrayImage = out.toByteArray();

                String kq = HandlerWebService.UploadImageArg(
                        "","TLG_NG_DAILY_INPUT_D", DEFECT_D_PK, DEFECT_D_PK     // _TLG_INSPECTION_ENTRY_M_PK, _TLG_INSPECTION_ENTRY_M_PK
                        , "defect.jpg", String.valueOf(bytes), ""
                        , byteArrayImage
                        , bsUserID);

                Log.i("UpLoadImage(): ", kq);
                return null;
            }

            @Override
            protected void onPostExecute(Void args) {
                super.onPreExecute();
                progress.dismiss();
                this.cancel(true);
//                flagUpdateImage = false;

            }
        }.execute();
    }
}
