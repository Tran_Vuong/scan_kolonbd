package gw.genumobile.com.views.qc_management;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.lang.Exception;import java.lang.Override;import java.lang.String;import java.lang.System;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import gw.genumobile.com.R;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.views.gwFragmentDialog;
import gw.genumobile.com.views.gwcore.BaseGwActive;

/** Created by HNDGiang on 08/01/2016. **/

/*
*   Load Detail With: PK, ITEM CODE
*   Update: 09/01/2016
*   Author: HNDGiang
*/
public class Popup_QC_Management_ReqNo extends gwFragmentDialog implements View.OnClickListener {

    //region Avarible
    static final int DATE_PICKER_FROM_REQ_ID = 1113;
    static final int DATE_PICKER_TO_REQ_ID = 1114;



    public static final int REQUEST_CODE = 1;
    String pkSlipNo = "", custID = "", nameSlipNo = "";

    int indexPrevious = -1;

    int orangeColor = Color.rgb(255, 99, 71);
    String _req_No = "";
    Calendar c1,c2;
    //private static DatePickerFragment pickerFrom, pickerTo;
    SimpleDateFormat sdf_DataBase = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat sdf_View = new SimpleDateFormat("dd-MM-yyyy");
    String fromDate = "", toDate = "";
    String fromDate_db = "", toDate_db = "";
    EditText etFromDateEtxt, etToDateEtxt;

    Button btn_FromDate, btn_ToDate,btnSearchReqNo,btnSelectReqNo;

    TextView tvTeamp = null, tvReqNo = null;

    private int year;
    private int month;
    private int day;
    private int year1;
    private int month1;
    private int day1;
    //endregion

    //region Application Circle
    //region Application Circle
    View vContent;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //type = getArguments().getString("type");
        //gwAc=getActivity();
        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_TITLE, getTheme());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vContent = inflater.inflate(R.layout.popup_qc_management_reqno, container, false);

        Toolbar toolbar = (Toolbar) vContent.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                    Intent i = new Intent();
                           // .putExtra("month", "trang");
                   // getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);*/
                    gwMActivity.setResult(Activity.RESULT_CANCELED, i);
                    dismiss();
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);
        toolbar.setTitle("REQ NO ");

        gwMActivity.AddEvenKeyboardHiddenWhenClickOutSide(vContent.findViewById(R.id.POPUP_QC_REQNO));


        //region Set Screen Size
        int Measuredheight = 0, Measuredwidth = 0;
        Point size = new Point();
       /* WindowManager w = getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            w.getDefaultDisplay().getSize(size);
            Measuredheight = size.y;
            Measuredwidth = size.x;
            final LinearLayout root = (LinearLayout) vContent.findViewById(R.id.POPUP_QC_REQNO);
            FrameLayout.LayoutParams rootParams = (FrameLayout.LayoutParams) root.getLayoutParams();
            rootParams.height = Measuredheight * 60 / 100;
            rootParams.width = Measuredwidth * 90 / 100;
            System.out.println("H: " + rootParams.height + "   W: " + rootParams.width);

        }*/
        //endregion
        //   _req_No = getIntent().getStringExtra("Req_No");
        CreatGridHeader();
        //  ShowAll();
        //endregion


        etFromDateEtxt = (EditText) vContent.findViewById(R.id.etFromDate);
        etToDateEtxt = (EditText) vContent.findViewById(R.id.etToDate);

        // Show current date
        // Get current date by calender
        c1 = Calendar.getInstance();
        c1.add(Calendar.DATE, -1);
        year  = c1.get(Calendar.YEAR);
        month = c1.get(Calendar.MONTH);
        day   = c1.get(Calendar.DAY_OF_MONTH);
        fromDate_db=String.valueOf(year)+String.valueOf(isPushNumber(month+1))+String.valueOf(isPushNumber(day));

        c2 = Calendar.getInstance();
        year1  = c2.get(Calendar.YEAR);
        month1 = c2.get(Calendar.MONTH);
        day1   = c2.get(Calendar.DAY_OF_MONTH);
        toDate_db=String.valueOf(year1)+String.valueOf(isPushNumber(month1+1))+String.valueOf(isPushNumber(day1));

        etFromDateEtxt.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(isPushNumber(day)).append("-").append(isPushNumber(month + 1)).append("-").append(year).append(" "));
        etToDateEtxt.setText(new StringBuilder()
                .append(isPushNumber(day1)).append("-").append(isPushNumber(month1 + 1)).append("-").append(year1).append(" "));
        //////////
        btn_FromDate = (Button) vContent.findViewById(R.id.btn_FromDate);
        btn_FromDate.setOnClickListener(this);

        btn_ToDate = (Button) vContent.findViewById(R.id.btn_ToDate);
        btn_ToDate.setOnClickListener(this);
        btnSearchReqNo = (Button) vContent.findViewById(R.id.btnSearchReqNo);
        btnSearchReqNo.setOnClickListener(this);
        btnSelectReqNo = (Button) vContent.findViewById(R.id.btnSelectReqNo);
        btnSelectReqNo.setOnClickListener(this);

        return vContent;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    //endregion

    private DatePickerDialog.OnDateSetListener pickerFrom = new DatePickerDialog.OnDateSetListener() {
        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;
            // Show selected date
            fromDate_db =String.valueOf(year)+String.valueOf(isPushNumber(month+1))+String.valueOf(isPushNumber(day));
            etFromDateEtxt.setText(new StringBuilder()
                    //.append(year).append("-").append(isPushNumber(month + 1)).append("-").append(isPushNumber(day)).append(""));
                    .append(isPushNumber(day)).append("-").append(isPushNumber(month + 1)).append("-").append(year).append(" "));
        }
    };
    private DatePickerDialog.OnDateSetListener pickerTo = new DatePickerDialog.OnDateSetListener() {
        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;
            toDate_db =String.valueOf(year)+String.valueOf(isPushNumber(month+1))+String.valueOf(isPushNumber(day));
            // Show selected date
            etToDateEtxt.setText(new StringBuilder()
                    //.append(year).append("-").append(isPushNumber(month + 1)).append("-").append(isPushNumber(day)).append(""));
                    .append(isPushNumber(day)).append("-").append(isPushNumber(month + 1)).append("-").append(year).append(" "));
        }
    };

    private void CreatGridHeader() {
        //Init varible
        TableRow.LayoutParams trParam = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        TableRow tRow = new TableRow(gwMActivity);
        TableLayout tLayout;

        //Design Grid
        tLayout = (TableLayout) vContent.findViewById(R.id.grdHeader);
        tRow = new TableRow(gwMActivity);
        tRow.setLayoutParams(trParam);
        tRow.setGravity(Gravity.CENTER);
        tRow.setBackgroundColor(bsColors[1]);
        tRow.setVerticalGravity(50);

        //Add Column
        ///// TODO: 2016-02-04 design grid
        tRow.addView(makeTableColHeaderWithText("SEQ", scrollableColumnWidths[0], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Request Date", scrollableColumnWidths[2], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Request No", scrollableColumnWidths[2], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Item Code", scrollableColumnWidths[2], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Item Name", scrollableColumnWidths[2], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Lot No", scrollableColumnWidths[1], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("Line", scrollableColumnWidths[2], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("QC Type", scrollableColumnWidths[2], fixedRowHeight));
        //tRow.addView(makeTableColHeaderWithText("QC Type Pk", scrollableColumnWidths[2], fixedRowHeight));
        tLayout.addView(tRow);
    }


    public void ShowAll() {

        String para = _req_No;
        String l_para = "1,drivtldrlgtl0001_s_05," + para;

        try {
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String resultSearch[][] = networkThread.execute(l_para).get();

            TableLayout scrollablePart = (TableLayout) vContent.findViewById(R.id.grdQCSlipNo);
            scrollablePart.removeAllViews();

            if (resultSearch != null && resultSearch.length > 0) {
                ShowResultSearch(resultSearch, scrollablePart);
            }
        } catch (Exception ex) {
            Log.e("Searh Error: ", ex.getMessage());
        }
    }

    public void onSearch(View view) {

        String _item = ((EditText) vContent.findViewById(R.id.etItem)).getText().toString();
        String _line = ((EditText)vContent.findViewById(R.id.etLine)).getText().toString();

        String para = fromDate_db + "|" +  toDate_db+ "|" + _item + "|" + _line;

        String l_para = "";
        l_para = "1,drivtldrlgtl0001_s_07," + para;

        try {
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String resultSearch[][] = networkThread.execute(l_para).get();

            TableLayout scrollablePart = (TableLayout) vContent.findViewById(R.id.grdQCReqNo);
            scrollablePart.removeAllViews();

            if (resultSearch.length > 0) {
                ShowResultSearch(resultSearch, scrollablePart);
            }
        } catch (Exception ex) {
            Log.e("Searh Error: ", ex.getMessage());
        }finally {
            indexPrevious = -1;
        }
    }


    public void onSelect(View view) {

        if(tvReqNo == null || tvReqNo.getText().toString().length() == 0){
            gwMActivity.alertToastShort("Please Choose A Request No");
            return;
        }else{
            Intent intent = gwMActivity.getIntent();
            Bundle bundle = new Bundle();

            bundle.putString("REQUEST_NO", tvReqNo.getText().toString());
            intent.putExtra("DATA_REQUEST_NO", bundle);

            if(tvReqNo.getText().toString().length() > 0){
                gwMActivity.setResult(Activity.RESULT_OK, intent);
            }else{
                gwMActivity.setResult(Activity.RESULT_CANCELED, intent);
            }
          dismiss();

        }
    }


    public void ShowResultSearch(String result[][], final TableLayout scrollablePart) {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        //remove all view child
        int count = result.length;
        for (int i = 0; i < result.length; i++) {

            row = new TableRow(gwMActivity);
            row.setLayoutParams(wrapWrapTableRowParams);
            row.setGravity(Gravity.CENTER);
            row.setBackgroundColor(Color.LTGRAY);

            //QC SEQ
            row.addView(makeTableColWithText(String.valueOf(i + 1), scrollableColumnWidths[0], fixedRowHeight, 0));
            //QC Request Date
            row.addView(makeTableColWithText(result[i][0], scrollableColumnWidths[2], fixedRowHeight, -1));
            //QC Request No
            row.addView(makeTableColWithText(result[i][1], scrollableColumnWidths[2], fixedRowHeight, -1));
            //QC Item Code
            row.addView(makeTableColWithText(result[i][2], scrollableColumnWidths[2], fixedRowHeight, -1));
            //QC Item Name
            row.addView(makeTableColWithText(result[i][3], scrollableColumnWidths[2], fixedRowHeight, -1));
            //QC Lot No
            row.addView(makeTableColWithText(result[i][4], scrollableColumnWidths[1], fixedRowHeight, -1));
            //QC Line
            row.addView(makeTableColWithText(result[i][5], scrollableColumnWidths[2], fixedRowHeight, -1));
            //QC Type
            row.addView(makeTableColWithText(result[i][6], scrollableColumnWidths[2], fixedRowHeight, -1));
            //QC TYPE PK
            row.addView(makeTableColWithText(result[i][7],0, fixedRowHeight, 1));


            row.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    TableRow tr1 = (TableRow) v;
                    tvTeamp = (TextView) tr1.getChildAt(0); //PK SLIP_NO
                    tvReqNo = (TextView) tr1.getChildAt(2);

                    pkSlipNo = tvTeamp.getText().toString();
                    nameSlipNo = tvReqNo.getText().toString();
                    custID = tvReqNo.getText().toString();

                    for (int i = 0; i < tr1.getChildCount(); i++) {
                        tvTeamp = (TextView) tr1.getChildAt(i);
                        tvTeamp.setTextColor(orangeColor);
                    }

                    if (indexPrevious >= 0
                            && indexPrevious != scrollablePart.indexOfChild(v)) {
                        TableRow trP = (TableRow) scrollablePart.getChildAt(indexPrevious);
                        for (int k = 0; k < trP.getChildCount(); k++) {
                            tvTeamp = (TextView) trP.getChildAt(k);
                            tvTeamp.setTextColor(Color.BLACK);
                        }
                    }
                    //Update indexPrevious
                    indexPrevious = scrollablePart.indexOfChild(v);
                }
            });
            scrollablePart.addView(row);
        }
    }
/*

    public void onClose(View view) {
        finish();
        overridePendingTransition(R.anim.push_out_left, R.anim.push_out_left);
    }
*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_FromDate:
                //alertToastShort("From Date");
                // pickerFrom = new DatePickerFragment();
                /*
                pickerFrom = new DatePickerFragment() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Calendar c = Calendar.getInstance();
                        c.set(year, month, day);

                        fromDate = sdf_DataBase.format(c.getTime());
                        fromDate_db = sdf_DataBase.format(c.getTime());
                        ((EditText) vContent.findViewById(R.id.etFromDate)).setText(sdf_View.format(c.getTime()));
                    }
                };
                pickerFrom.show(getFragmentManager(), "From Date");
                */
                DatePickerDialog dg=   new DatePickerDialog(gwMActivity, pickerFrom, year, month,day);
                dg.show();
               // showDialog(DATE_PICKER_FROM_REQ_ID);
                break;

            case R.id.btn_ToDate:
                DatePickerDialog dg1=   new DatePickerDialog(gwMActivity, pickerTo, year1, month1,day1);
                dg1.show();
                /*
                pickerTo = new DatePickerFragment() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Calendar c = Calendar.getInstance();
                        c.set(year, month, day);

                        toDate = sdf_View.format(c.getTime());
                        toDate_db = sdf_DataBase.format(c.getTime());
                        ((EditText) vContent.findViewById(R.id.etToDate)).setText(sdf_View.format(c.getTime()));
                    }
                };
                pickerTo.show(getFragmentManager(), "To Date");
                */
                break;

            case R.id.btnSearchReqNo:
              onSearch(v);
                break;
            case R.id.btnSelectReqNo:
                onSelect(v);
                break;
        }

        //endregion
    }


    //region CLASS DATE PICKER FRAGMENT
    @SuppressLint("ValidFragment")
    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {


        public String strDate;

        public void setStrDate(String strDate) {
            this.strDate = strDate;
        }

        public String getStrDate() {
            return strDate;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            int h = c.get(Calendar.HOUR_OF_DAY);
            int m = c.get(Calendar.MINUTE);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public String a;

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar c = Calendar.getInstance();
            c.set(year, month, day);

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String formattedDate = sdf.format(c.getTime());
            setStrDate(formattedDate);
        }


    }



    public String isPushNumber(int x){
        String temp;
        if(x<10){
            temp="0"+String.valueOf(x);
        }
        else
            temp=String.valueOf(x);
        return temp;
    }
}