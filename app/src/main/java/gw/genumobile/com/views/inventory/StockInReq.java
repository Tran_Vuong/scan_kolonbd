package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import gw.genumobile.com.R;
import gw.genumobile.com.interfaces.GridListViewBot;
import gw.genumobile.com.interfaces.GridListViewOnePK;
import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.interfaces.PopupInquiry;
import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.views.gwFragment;


public class StockInReq extends gwFragment implements View.OnClickListener {

    protected Boolean flagUpload = true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;

    Queue queue = new LinkedList();
    Queue queueMid = new LinkedList();
    Queue queueMidBC = new LinkedList();

    Calendar c;
    SimpleDateFormat df;
    String arr[] = new String[0];
    SQLiteDatabase db = null;
    public SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    String sql = "", tr_date = "", scan_date = "", scan_time = "", unique_id = "", pk_user = "", user = "", user_id = "", str_reqNo = "", loc_pk = "", loc_name = "", wh_pk = "", wh_name = "";
    EditText edt_BC;
    TextView tv_reqNo, tv_reqBal, tv_reqQty, msg_error, _txtTime, txtLocIn, txtBcScan, txt_accum;
    TextView _txtDate, _txtTT, _txtSent, _txtRemain, _txtTotalGridBot, _txtTotalQtyBot, _txtTotalGridMid;
    String data[][] = new String[0][0];
    String data1[] = new String[0];
    List<JDataTable> jDatatable;
    int timeUpload = 0;
    Button btnDelAll,btnMakeSlip,btnViewStt,btnList,btnInquiry,btnListBot;
    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_stock_in_req, container, false);

        edt_BC = (EditText) rootView.findViewById(R.id.editBC);
        tv_reqNo = (TextView) rootView.findViewById(R.id.txt_reqNo);
        tv_reqBal = (TextView) rootView.findViewById(R.id.txt_reqBal);
        tv_reqQty = (TextView) rootView.findViewById(R.id.txt_reqQty);
        txtLocIn = (TextView) rootView.findViewById(R.id.txtLocIn);
        txtBcScan = (TextView) rootView.findViewById(R.id.txtBcScan);
        txt_accum = (TextView) rootView.findViewById(R.id.txtAccum);
        _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);

        msg_error = (TextView) rootView.findViewById(R.id.txtError);
        msg_error.setTextColor(Color.RED);
        msg_error.setTextSize((float) 16.0);
        msg_error.setText("");

        _txtDate = (TextView) rootView.findViewById(R.id.txtDate);

        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        _txtDate.setText(formattedDate);
        _txtTime = (TextView) rootView.findViewById(R.id.txtTime);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip) ;
        btnMakeSlip.setOnClickListener(this);
        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt) ;
        btnViewStt.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList) ;
        btnList.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry) ;
        btnInquiry.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot) ;
        btnListBot.setOnClickListener(this);

        edt_BC.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        msg_error.setText("");
                        if (edt_BC.getText().toString().equals("")) {
                            msg_error.setText("Pls scan barcode!");
                        } else {
                            OnSaveBC();
                        }
                    }
                    return true;
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // this is for backspace
                    edt_BC.clearFocus();
                    Thread.interrupted();
                }
                return false;
            }

        });

        OnShowGridHeader();
        init_color();
        OnShowGridDetail();
        OnShowScanIn();
        OnShowScanLog();

        timeUpload = hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);
        return rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run() {
            if (flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload * 1000);
        }
    };

    private void init_color() {
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
        if (Measuredwidth <= 600) {
            _txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);

            // Gets linearlayout
            LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.layoutLeft);
            // Gets the layout params that will allow you to resize the layout
            ViewGroup.LayoutParams paramsLayoutLeft = layout.getLayoutParams();
            // Changes the height and width to the specified *pixels*
            //paramsLayoutLeft.height = 430;
            paramsLayoutLeft.width = Measuredwidth / 2;
            layout.setLayoutParams(paramsLayoutLeft);

            TextView tv3 = (TextView) rootView.findViewById(R.id.textView3);
            TextView tv5 = (TextView) rootView.findViewById(R.id.textView5);
            TextView tv7 = (TextView) rootView.findViewById(R.id.textView7);
            TextView tv8 = (TextView) rootView.findViewById(R.id.textView8);
            TextView tv9 = (TextView) rootView.findViewById(R.id.textView9);
            ViewGroup.LayoutParams paramsWeight = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            tv3.setLayoutParams(paramsWeight);
            tv5.setLayoutParams(paramsWeight);
            tv7.setLayoutParams(paramsWeight);
            tv8.setLayoutParams(paramsWeight);
            tv9.setLayoutParams(paramsWeight);
            ViewGroup.LayoutParams paramsMatch = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            tv_reqNo.setLayoutParams(paramsMatch);
            tv_reqBal.setLayoutParams(paramsMatch);
            tv_reqQty.setLayoutParams(paramsMatch);

            txtBcScan.setLayoutParams(paramsMatch);
            params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            txtLocIn.setLayoutParams(params);

        }
    }

    private void doStart() {
        if (str_reqNo.equals(""))
            return;
        flagUpload = false;
        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

        cursor2 = db2.rawQuery("select PK, ITEM_BC,TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK,REQ_NO  from INV_TR where DEL_IF=0 and TR_TYPE='" + formID + "' and sent_yn = 'N' and ITEM_BC is not null order by PK asc LIMIT 10", null);
        //System.out.print("\n\n\n cursor2.count: "+ String.valueOf(cursor2.getCount()));

        if (cursor2.moveToFirst()) {

            // Write your code here to invoke YES event
            System.out.print("\n\n**********doStart********\n\n\n");

            //set value message
            msg_error = (TextView) rootView.findViewById(R.id.txtError);
            msg_error.setTextColor(Color.RED);
            msg_error.setText("");

            //  data=new String [cursor2.getCount()];
            data1 = new String[1];
            int j = 0;
            String para = "";
            boolean flag = false;
            do {
                flag = true;
                for (int i = 0; i < cursor2.getColumnCount(); i++) {
                    if (para.length() <= 0) {
                        if (cursor2.getString(i) != null)
                            para += cursor2.getString(i) + "|";
                        else
                            para += "|";
                    } else {

                        if (flag == true) {
                            if (cursor2.getString(i) != null) {
                                para += cursor2.getString(i);
                                flag = false;
                            } else {
                                para += "|";
                                flag = false;
                            }
                        } else {
                            if (cursor2.getString(i) != null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }
                }
                para += "|" + deviceID;
                para += "|" + bsUserID;
                para += "*|*";
            } while (cursor2.moveToNext());
            //////////////////////////
            para += "|!" + "LG_MPOS_UPLOAD_INC_REQ";
            data1[j++] = para;
            System.out.print("\n\n\n para upload: " + para);
            Log.e("para upload: ", para);

            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity, this);
                task.execute(data1);
                gwMActivity.alertToastShort("Send to Server");
            } else {
                flagUpload = true;
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        } else {
            flagUpload = true;
        }
        cursor2.close();
        db2.close();
    }

    public void OnSaveBC() {
        if (edt_BC.getText().toString().equals("")) {
            msg_error.setTextColor(Color.RED);
            msg_error.setText("Pls Scan barcode!");
            edt_BC.getText().clear();
            edt_BC.requestFocus();
            return;
        } else {

            String str_scanBC = edt_BC.getText().toString().toUpperCase();

            try {
                if (str_scanBC.length() > 20) {
                    msg_error.setText("Barcode has length more 20 char !");
                    edt_BC.getText().clear();
                    edt_BC.requestFocus();
                    return;
                }
                if (str_reqNo.equals("") && str_scanBC.indexOf("S") == 0) {
                    String slip_no = "", out_name = "", in_name = "";
                    String req_no = str_scanBC.substring(1, str_scanBC.length());
                    db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                    String check_reqNo = "SELECT PK,SLIP_NO,TR_WH_IN_NAME FROM INV_TR where  tr_type='" + formID + "' AND (status='000' or status=' ')  and REQ_NO ='" + req_no + "' and ITEM_BC is null";
                    Cursor cursor = db.rawQuery(check_reqNo, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            if (!cursor.getString(cursor.getColumnIndex("SLIP_NO")).equals("-") && !cursor.getString(cursor.getColumnIndex("SLIP_NO")).equals(" ")) {
                                slip_no = cursor.getString(cursor.getColumnIndex("SLIP_NO"));
                                in_name = cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME"));
                            }

                        } while (cursor.moveToNext());
//                        if(!slip_no.equals("")){
//                            gwMActivity.alertToastShort("Request is maked slip!!!");
//                            edt_BC.getText().clear();
//                            edt_BC.requestFocus();
//                            return;
//                        }
                        str_reqNo = req_no;
                        tv_reqNo.setText(str_reqNo);
                        scan_date = CDate.getDateyyyyMMdd();
                        OnShowGridDetail();
                        OnShowScanIn();
                        OnShowScanLog();
                        gwMActivity.alertToastShort("ReqNo is exist!!!");
                        db.close();
                    } else {

                        String para = "1,LG_MPOS_SEL_ST_IN_REQ,1," + req_no;
                        gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                        JResultData jrd = networkThread.execute(para).get();
                        jDatatable = jrd.getListODataTable();
                        //jDatatable =  this.getDataJson(gwActivity, para);
                        JDataTable dt = jDatatable.get(0);
                        if (dt.totalrows > 0) {
                            for (int j = 0; j < dt.totalrows; j++) {
                                if (!dt.records.get(j).get("slip_no").toString().equals(""))
                                    slip_no = dt.records.get(j).get("slip_no").toString();
                            }
                            if (!slip_no.equals("")) {
                                gwMActivity.alertToastShort("Request was make slip!!!");
                                edt_BC.getText().clear();
                                edt_BC.requestFocus();
                                return;
                            }

                            for (int i = 0; i < dt.totalrows; i++) {
                                try {

                                    scan_date = CDate.getDateyyyyMMdd();
                                    scan_time = CDate.getDateYMDHHmmss();
                                    db.execSQL("INSERT INTO INV_TR(TLG_GD_REQ_M_PK,TLG_GD_REQ_D_PK,REQ_NO,TR_ITEM_PK,ITEM_CODE,ITEM_NAME," +
                                            " REQ_QTY,SCANNED,REQ_BAL,UOM, TR_WH_IN_NAME,TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK,TLG_IN_WHLOC_IN_NAME,SLIP_NO,SO_NO,"
                                            + " SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                                            + " VALUES('"
                                            + dt.records.get(i).get("req_m_pk").toString() + "','"
                                            + dt.records.get(i).get("req_d_pk").toString() + "','"
                                            + dt.records.get(i).get("req_no").toString() + "','"
                                            + dt.records.get(i).get("item_pk").toString() + "','"
                                            + dt.records.get(i).get("item_code").toString() + "','"
                                            + dt.records.get(i).get("item_name").toString() + "','"
                                            + dt.records.get(i).get("req_qty").toString() + "','"
                                            + dt.records.get(i).get("scanned").toString() + "','"
                                            + dt.records.get(i).get("req_bal").toString() + "','"
                                            + dt.records.get(i).get("req_uom").toString() + "','"
                                            + dt.records.get(i).get("wh_name").toString() + "','"
                                            + dt.records.get(i).get("wh_pk").toString() + "','"
                                            + loc_pk + "','"
                                            + loc_name + "','"
                                            + dt.records.get(i).get("slip_no").toString() + "','"
                                            + dt.records.get(i).get("so_no").toString() + "','"
                                            + scan_date + "','"
                                            + scan_time + "','"
                                            + "N" + "','"
                                            + " " + "','"
                                            + formID + "');");
                                } catch (Exception ex) {
                                    msg_error.setText("Save Error!!!" + ex.toString());
                                }
                            }
                            wh_pk = dt.records.get(0).get("wh_pk").toString();
                            wh_name = dt.records.get(0).get("wh_name").toString();
                            str_reqNo = dt.records.get(0).get("req_no").toString();

                            db.close();

                            //txtWhOut.setText(data[0][10]);
                            //txtWhIn.setText(data[0][14]);
                            OnShowGridDetail();
                            //OnShowScanLog();
                            //OnShowScanIn();
                            tv_reqNo.setText(req_no);
                        } else {
                            msg_error.setText("Req No '" + req_no + "' not exist !!!");
                            str_reqNo = "";
                            return;
                        }
                    }

                    edt_BC.getText().clear();
                    edt_BC.requestFocus();
                    return;
                }
                if(str_reqNo.equals("")||str_reqNo==""){
                    alertRingMedia2();
                    msg_error.setText("Please scan request first!");
                    edt_BC.getText().clear();
                    edt_BC.requestFocus();
                    return;
                }
                boolean isExists = hp.isExistBarcode(str_scanBC, formID);// check barcode exist in data
                if (isExists)// exist data
                {
                    alertRingMedia();
                    msg_error.setText("Barcode exist in database!");
                    edt_BC.getText().clear();
                    edt_BC.requestFocus();
                    return;
                } else {
                    if (str_scanBC.indexOf("L") == 0) {
                        int str_loc = Integer.valueOf(str_scanBC.substring(1, str_scanBC.length()));
                        String l_para = "1,LG_MPOS_GET_LOC_NAME,1," + str_loc;
                        try {

                            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
                            JResultData dtLocGroup = networkThread.execute(l_para).get();
                            List<JDataTable> jdt = dtLocGroup.getListODataTable();
                            JDataTable dt = jdt.get(0);
                            if (dt.totalrows > 0) {
                                loc_pk = dt.records.get(0).get("pk").toString();
                                loc_name = dt.records.get(0).get("loc_name").toString();

                            } else {
                                loc_name = "";
                                loc_pk = "";
                                alertRingMedia2();
                                gwMActivity.onPopAlert("Location không hợp lệ !!!");
                            }
                            txtLocIn.setText(loc_name);
                        } catch (Exception ex) {
                            gwMActivity.onPopAlert(ex.toString());
                        }

                    } else {
                        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        scan_date = CDate.getDateyyyyMMdd();
                        scan_time = CDate.getDateYMDHHmmss();

                        db.execSQL("INSERT INTO INV_TR(ITEM_BC,TLG_IN_WHLOC_IN_PK, TLG_IN_WHLOC_IN_NAME,REQ_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                                + "VALUES('"
                                + str_scanBC + "','"

                                + loc_pk + "','"
                                + loc_name + "','"
                                + str_reqNo + "','"
                                + scan_date + "','"
                                + scan_time + "','"
                                + "N" + "','"
                                + " " + "','"
                                + formID + "');");
                        db.close();
                    }
                }
            } catch (Exception ex) {
                msg_error.setText("Save Error!!!");
                Log.e("OnSaveBC", ex.getMessage());
            } finally {

                edt_BC.getText().clear();
                edt_BC.requestFocus();
            }
        }
        OnShowScanLog();
        CountSendRecord();
    }

    public void onMakeSlip(View view) {
        //Check het Barcode send to server
        if (checkRecordGridView(R.id.grdScanLog)) {
            gwMActivity.alertToastLong(this.getResources().getString(R.string.tvAlertMakeSlip));
            return;
        }
        //Check have value make slip
        if (hp.isMakeSlip(formID) == false) return;

        String title = "Confirm Make Slip...";
        String mess = "Are you sure you want to Make Slip";
        alertDialogYN(title, mess, "onMakeSlip");
    }

    public void ProcessMakeSlip() {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if (queue.size() > 0) {

        } else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                sql = "  select TLG_GD_REQ_M_PK, TLG_GD_REQ_D_PK, TR_ITEM_PK,SUM(TR_QTY) as QTY, UOM, TR_LOT_NO , TLG_PO_PO_D_PK, SUPPLIER_PK, TR_WH_IN_PK, TLG_IN_WHLOC_IN_PK, UNIT_PRICE, PO_NO, REQ_NO, SO_NO,EXECKEY " +
                                "  FROM INV_TR  WHERE DEL_IF = 0 AND STATUS='000' AND SCAN_DATE = '" + scan_date + "' AND ITEM_BC IS NOT NULL AND TR_TYPE = '" + formID + "' AND REQ_NO='" + str_reqNo +"' and SLIP_NO IN('-','',null) " +
                                "  GROUP BY TLG_GD_REQ_M_PK,TLG_GD_REQ_D_PK, TR_ITEM_PK, UOM, TR_LOT_NO, TLG_PO_PO_D_PK, SUPPLIER_PK, TR_WH_IN_PK, TLG_IN_WHLOC_IN_PK, UNIT_PRICE, PO_NO, REQ_NO,SO_NO, EXECKEY  ";
                cursor = db.rawQuery(sql, null);

                int count = cursor.getCount();
                data1 = new String[1];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount() - 1; k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey = cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if (execkey == null || execkey.equals("")) {
                            int ex_item_pk = Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            String ex_wh_pk = cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK"));
                            String ex_supp_pk = cursor.getString(cursor.getColumnIndex("SUPPLIER_PK"));
                            String ex_lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            String ex_po_d_pk = cursor.getString(cursor.getColumnIndex("TLG_PO_PO_D_PK"));
                            hp.updateExeckeyIncome(formID, ex_item_pk, ex_wh_pk, ex_supp_pk, ex_lot_no, ex_po_d_pk, unique_id);

                            para += "|" + unique_id;
                        } else {
                            para += "|" + execkey;
                        }
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "|" + deviceID;
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_ST_INC_REQ_MSLIP";
                    System.out.print("\n\n ******para make slip in stock: " + para);
                    //data[j++] = para;
                    data1[j] = para;
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity, this);
                task.execute(data1);
            } else {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        }
    }

    public void OnShowScanLog() {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and SENT_YN='N' and REQ_NO = '" + str_reqNo + "' AND ITEM_BC IS NOT NULL order by PK desc LIMIT 20", null);// TLG_LABEL

            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='" + formID + "' and item_bc is not null and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
            _txtRemain.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowScanIn() {
        try {
            int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE, STATUS, TR_LOT_NO, SLIP_NO, INCOME_DATE, CHARGER, TR_WH_IN_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '" + formID + "' AND SENT_YN='Y' AND SCAN_DATE > '" + date_previous + "' AND STATUS NOT IN('000', ' ') and REQ_NO = '" + str_reqNo + "' AND ITEM_BC IS NOT NULL  " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq = String.valueOf(count - i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[0], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > '" + date_previous + "' AND TR_TYPE = '" + formID + "' AND SENT_YN = 'Y' AND ITEM_BC IS NOT NULL AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
            _txtTotalGridMid.setText("Total: " + count);
            _txtTotalGridMid.setTextColor(Color.BLUE);
            _txtTotalGridMid.setTextSize((float) 17.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void OnShowGridDetail() {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        scrollablePart.removeAllViews();

        db = gwMActivity.openOrCreateDatabase("gasp", gwMActivity.MODE_PRIVATE, null);
        sql="SELECT  REQ_NO,TR_ITEM_PK,ITEM_CODE,REQ_QTY,SCANNED,REQ_BAL,UOM,SLIP_NO,SO_NO,TLG_GD_REQ_M_PK,TLG_GD_REQ_D_PK,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_OUT_NAME,"
                + "TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK,TLG_IN_WHLOC_IN_NAME,"
                + "(select SUM(T1.TR_QTY) from INV_TR T1 WHERE T1.TLG_GD_REQ_D_PK= A.TLG_GD_REQ_D_PK and T1.tr_type='" + formID +"'"+ " and T1.STATUS='000' and T1.ITEM_BC is NOT NULL) as CURRENT_QTY, "
                + "(select COUNT(T2.PK) from INV_TR T2 WHERE T2.TLG_GD_REQ_D_PK= A.TLG_GD_REQ_D_PK and T2.tr_type='" + formID +"'"+ " and T2.STATUS='000' and T2.ITEM_BC is NOT NULL) as BC_NUM "
            +" FROM ( SELECT  REQ_NO,TR_ITEM_PK,ITEM_CODE,REQ_QTY,SCANNED,REQ_BAL,UOM,SLIP_NO,SO_NO,TLG_GD_REQ_M_PK,TLG_GD_REQ_D_PK,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_OUT_NAME,"
                + "TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK,TLG_IN_WHLOC_IN_NAME" +
                "   FROM INV_TR " +
                "    WHERE tr_type='" + formID + "' AND (status='000' or status=' ')  and REQ_NO ='" + str_reqNo + "' and ITEM_BC is null )A ";

        cursor = db.rawQuery(sql, null);
        int count = cursor.getCount();

        double req_qty = 0;
        float req_bal = 0;
        float req_accum = 0;

        if (cursor != null && cursor.moveToFirst()) {
            for (int i = 0; i < count; i++) {
                req_bal = req_bal + Float.parseFloat(cursor.getString(cursor.getColumnIndex("REQ_BAL")).toString());
                req_accum = req_accum + Float.parseFloat(cursor.getString(cursor.getColumnIndex("SCANNED")).toString());

                row = new TableRow(gwMActivity);
                row.setLayoutParams(wrapWrapTableRowParams);
                row.setGravity(Gravity.CENTER);
                row.setBackgroundColor(Color.LTGRAY);
                row.addView(makeTableColWithText(String.valueOf(i + 1), scrollableColumnWidths[0], fixedRowHeight, 0));

                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UOM")), scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REQ_QTY")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCANNED")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("REQ_BAL")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CURRENT_QTY")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("BC_NUM")), scrollableColumnWidths[2], fixedRowHeight, 1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[3], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[3], fixedRowHeight, -1));
//                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_NAME")), scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SO_NO")), scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK")),scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_D_PK")),scrollableColumnWidths[2], fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")), 0, fixedRowHeight, -1));
                row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_PK")), 0, fixedRowHeight, -1));

                row.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        OnClickGridDetailBC(v);

                    }
                });


                row.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return false;
                    }
                });

                req_qty += Double.parseDouble(cursor.getString(cursor.getColumnIndex("REQ_QTY")));
                tv_reqQty.setText(String.valueOf(req_qty));
                scrollablePart.addView(row);
                cursor.moveToNext();
            }
        }
        tv_reqBal.setText(String.valueOf(req_bal));
        txt_accum.setText(String.valueOf(req_accum));

        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND STATUS='000' and REQ_NO ='" + str_reqNo + "' and ITEM_BC is NOT NULL";
        cursor = db.rawQuery(sql, null);
        count = cursor.getCount();
        float _qty=0;
        if (cursor != null && cursor.moveToFirst()) {
            for (int i = 0; i < count; i++) {
                _qty=_qty+ Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                cursor.moveToNext();
            }
        }
        _txtTotalGridBot=(TextView) rootView.findViewById(R.id.txtTotalBot);
        _txtTotalGridBot.setText("Total: " + count+ " ");
        _txtTotalGridBot.setTextColor(Color.BLUE);
        _txtTotalGridBot.setTextSize((float) 16.0);

        txtBcScan.setText(String.valueOf(count));

        cursor.close();
        db.close();
    }

    public void OnClickGridDetailBC(View v){
        boolean duplicate = false;
        TableRow tr1 = (TableRow) v;

        TextView tvSlipNo = (TextView) tr1.getChildAt(8); //SLIP NO
        String st_SlipNO = tvSlipNo.getText().toString();

        TextView tvItemPK = (TextView) tr1.getChildAt(11); //TR_ITEM_PK
        String item_pk = tvItemPK.getText().toString();
        TextView tvReq_M_Pk = (TextView) tr1.getChildAt(12);
        String req_M_PK = tvReq_M_Pk.getText().toString();
        TextView tvReq_D_Pk = (TextView) tr1.getChildAt(13);
        String req_D_PK = tvReq_D_Pk.getText().toString();

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new GridListViewOnePK();
        Bundle args = new Bundle();

        args.putString("form_id", formID);
        args.putString("ITEM_CODE", "");
        args.putString("SLIP_NO", st_SlipNO);
        args.putString("TR_ITEM_PK", item_pk);
        args.putString("TLG_GD_REQ_M_PK", req_M_PK);
        args.putString("TLG_GD_REQ_D_PK", req_D_PK);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");

    }
    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhIn), scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvUom), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("ReqQty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Scanned", scrollableColumnWidths[2], fixedHeaderHeight));
//        row.addView(makeTableColHeaderWithText("Manual", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Bal", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Current Qty", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("BC Num", scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Make Slip", scrollableColumnWidths[3], fixedHeaderHeight));
//        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhOut), scrollableColumnWidths[3], fixedHeaderHeight));
        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
            //row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhLocOut), scrollableColumnWidths[2], fixedHeaderHeight));
        }
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhIn), scrollableColumnWidths[3], fixedHeaderHeight));
        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
            //row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhLocIn), scrollableColumnWidths[2], fixedHeaderHeight));
        }
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSoNo), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("TR_ITEM_PK", 0, fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TLG_GD_REQ_M_PK", scrollableColumnWidths[2], fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TLG_GD_REQ_D_PK", scrollableColumnWidths[2], fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TR_WH_IN_PK", 0, fixedRowHeight));
        row.addView(makeTableColHeaderWithText("TLG_IN_WHLOC_IN_PK", 0, fixedRowHeight));
        scrollablePart.addView(row);
    }

    public void CountSendRecord() {
        flagUpload = true;
        // total scan log
        _txtSent = (TextView) rootView.findViewById(R.id.txtSent);
        _txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        _txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid = Integer.parseInt(_txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        int countBot = Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Send: " + (countMid + countBot));

        _txtRemain = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]", ""));

        _txtTT = (TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: " + (countMid + countBot + countRe));

    }

    //region ------------------Delete-----------------------
    public void onDelAll(View view) {

        if (!hp.isCheckDelete(formID)) return;

        String title = "Confirm Delete..";
        String mess = "Are you sure you want to delete all";
        alertDialogYN(title, mess, "onDelAll");
    }

    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onApprove")) {
//                    Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
//                    btnMSlip.setVisibility(View.GONE);
//                    btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
//                    btnApprove.setVisibility(View.GONE);
//                    ProcessApprove();
                }
                if (_type.equals("onDelAll")) {
                    ProcessDelete();
                }
                if (_type.equals("onMakeSlip")) {
                    //Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    //btnMSlip.setVisibility(View.GONE);
//                    btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
//                    btnApprove.setVisibility(View.GONE);
                    ProcessMakeSlip();
                }
            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void ProcessDelete() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();
                data1 = new String[myLst.length];
                for (int i = 0; i < myLst.length; i++) {
                    db.execSQL("DELETE FROM INV_TR where TR_TYPE='" + formID + "' and pk=" + myLst[i]);
                }

            } else {

                cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='" + formID + "' and STATUS  in ('000',' ') AND ITEM_BC IS NOT NULL", null); //SLIP_NO='-' and
                int count = cursor.getCount();
                String para = "";
                boolean flag = false;
                int j = 0;
                if (cursor.moveToFirst()) {
                    data1 = new String[1];
                    do {

                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + bsUserPK;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_UPD_INV_TR_DEL";
                    data1[j] = para;
                    Log.e("para update||delete: ", para);
                }
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("Delete All :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();

        }
        if (CNetwork.isInternet(tmpIP, checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity, this);
            task.execute(data1);
        } else {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }
    }

    public void onDeleteAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();

            } else {
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='" + formID + "';");
            }


        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            OnShowGridDetail();
            OnShowScanLog();
            OnShowScanIn();
            tv_reqNo.setText("");
            tv_reqQty.setText("");
            tv_reqBal.setText("");
            txt_accum.setText("");
            txtBcScan.setText("");

        }
    }

    public void onListGridBot(View view){
        if(!checkRecordGridView(R.id.grdScanAccept)) return;

       /* Intent openNewActivity = new Intent(view.getContext(), GridListViewBot.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        openNewActivity.putExtra("user",bsUser);
        openNewActivity.putExtra("user_id",bsUserPK);
        openNewActivity.putExtra("user_pk", bsUserPK);
        //startActivity(openNewActivity);
        startActivityForResult(openNewActivity, REQUEST_CODE);*/

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new GridListViewBot();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    public void onClickViewStt(View view){

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    ////////////////////////////////////////////////
    public void onClickInquiry(View view){
        if(!checkRecordGridView(R.id.grdScanAccept)) return;

        /*Intent openNewActivity = new Intent(view.getContext(), ItemInquiry.class);
        //send data into Activity
        openNewActivity.putExtra("type", formID);
        startActivity(openNewActivity);*/
        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new PopupInquiry();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }
    ///////////////////////////////////////////////
    public void onListGridMid(View view) {
        if (!checkRecordGridView(R.id.grdScanIn)) return;


        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  frGridListViewMid.newInstance(1,formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");

        //  gwMActivity.showDialog(formID);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
                onClickInquiry(view);

                break;

            case R.id.btnApprove:

                break;
            case R.id.btnViewStt:
                onClickViewStt(view);

                break;

            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnList:
                onListGridMid(view);

                break;
            case R.id.btnDelAll:
                onDelAll(view);
                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);
                break;
            default:
                break;
        }
    }
}
