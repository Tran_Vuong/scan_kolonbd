package gw.genumobile.com.views.inventory;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.utils.UIHelper;
import gw.genumobile.com.views.gwFragmentDialog;

import static android.R.layout.simple_list_item_checked;
import static gw.genumobile.com.R.id.btn_save;


public class Evaluation_pop extends gwFragmentDialog implements View.OnClickListener {

    Handler handler = new Handler();
    Activity gwAc;
    private UIHelper helper;

    String item_bc ;
    String wh_pk ;
    String pk ;
    String eval_qty ;
    String eval_num ;


    View vContent;

    EditText ettQty;
    EditText ettNewQty;
    EditText ettNum;
    EditText ettNewNum;
    Button btnSave ;
    Button btnClose;

    ListView listView;
    Pallet[] pallets = null;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gwAc=getActivity();
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
        helper= new UIHelper(gwMActivity);


//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
//        int width = displayMetrics.widthPixels;
        //getDialog().getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,width);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        vContent = inflater.inflate(R.layout.activity_evaluation_pop, container, false);
        pk =  getArguments().getString("pk");

        ettQty = (EditText)vContent.findViewById(R.id.qty) ;
        ettNum = (EditText)vContent.findViewById(R.id.num);
        ettNewQty = (EditText)vContent.findViewById(R.id.newQty) ;
        ettNewNum = (EditText)vContent.findViewById(R.id.newNum );

        listView = (ListView) vContent.findViewById(R.id.listView);

        item_bc =  getArguments().getString("item_bc");
        wh_pk =  getArguments().getString("wh_pk");
        ((EditText)vContent.findViewById(R.id.item_bc)).setText(item_bc);
        ettQty.setText( getArguments().getString("qty"));
        ettNum.setText( getArguments().getString("num"));

         eval_qty = getArguments().getString("eval_qty");
        if(eval_qty.length() == 0 ){
            ettNewQty.setText( getArguments().getString("qty"));
        }else{
            ettNewQty.setText( eval_qty);
        }


         eval_num = getArguments().getString("eval_num");
        if(eval_num.length() == 0){
            ettNewNum.setText( getArguments().getString("num"));
        }else{
            ettNewNum.setText( eval_num);
        }



        btnSave = (Button)vContent.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);
        btnClose = (Button) vContent.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(this);

        ettNewNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(pallets == null) return;


                String num =  ettNewNum.getText().toString() ;
                int result = 0;
                try{
                    result=  Integer.parseInt(num);
                }catch(Exception e) {}

                for(int i=0; i< pallets.length; i++ )  {
                    if(i < result){
                        pallets[i].setActive(true);
                    }else {
                        pallets[i].setActive(false);
                    }
                    listView.setItemChecked(i,pallets[i].isActive());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("TAG", "onItemClick: " +position);
                CheckedTextView v = (CheckedTextView) view;
                boolean currentCheck = v.isChecked();
                Pallet user = (Pallet)listView.getItemAtPosition(position);
                user.setActive(!currentCheck);
            }
        });




        loadDetail(item_bc);
        return  vContent;
    }

    private void loadDetail(String item_bc)
    {



        String l_para = "1,LG_MPOS_BC_CHILD," + item_bc;
        try {
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String data[][] = networkThread.execute(l_para).get();

            pallets = new Pallet[data.length];


            for(int i = 0 ; i < data.length ;i++){

                    if(eval_qty.length() == 0  ||  eval_num.length() == 0 ){
                        Pallet p = new Pallet(data[i][1].toString(), data[i][0].toString(), true);
                        pallets[i] = p;
                    }else {

                        if( data[i][2].toString().equals("1")){
                            Pallet p = new Pallet(data[i][1].toString(), data[i][0].toString(),  true);
                            pallets[i] = p;
                        }else{
                            Pallet p = new Pallet(data[i][1].toString(), data[i][0].toString(),  false);
                            pallets[i] = p;
                        }

                    }

            }

            ArrayAdapter<Pallet> arrayAdapter
                    = new ArrayAdapter<Pallet>(getContext(), simple_list_item_checked , pallets);


            listView.setAdapter(arrayAdapter);


            for(int i=0;i< pallets.length; i++ )  {
                listView.setItemChecked(i,pallets[i].isActive());
            }


        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_close:
                dismiss();
                break;
            case R.id.btn_save:
                checkData2Update();
                break;
        }

    }



    private void checkData2Update(){
       try {
            int num = Integer.parseInt( ettNum.getText().toString());
           int newnum = Integer.parseInt( ettNewNum.getText().toString());

           int qty = Integer.parseInt( ettQty.getText().toString());
           int newqty = Integer.parseInt( ettNewQty.getText().toString());

           if(newnum > num || newnum <= 0){
               Toast.makeText(getContext(),"Please input new Num chesse!!",Toast.LENGTH_LONG).show();
               return;
           }

           if(newqty > qty || newqty <= 0){
               Toast.makeText(getContext(),"Please input new Qty!!",Toast.LENGTH_LONG).show();
               return;
           }

           String listPK = "";

           for(int i=0; i< pallets.length; i++ )  {
             if( pallets[i].isActive()){
                 listPK = listPK + pallets[i].getPk() + ";" ;
             }
           }


           String l_para = "1,LG_MPOS_ITEMBC_ADJ," + item_bc + "|" +  wh_pk  + "|" + newnum + "|" + newqty + "|" + listPK + "|" + bsUser ;
           try {
               ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
               String data[][] = networkThread.execute(l_para).get();


               if(data[0].length > 0){


                   SQLiteDatabase db = gwMActivity.openOrCreateDatabase("gasp", gwMActivity.MODE_PRIVATE, null);
                   String sql = "UPDATE INV_TR set EVAL_QTY='" + newqty + "', EVAL_NUM_CHILD = " + newnum
                                   + "  where PK = " + pk;
                   db.execSQL(sql);

                   db.close();

                   dismiss();
               }else{
                   Toast.makeText(getContext(),data[0].toString(),Toast.LENGTH_LONG).show();
               }

           }catch (Exception ex){
               Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
           }




       }catch (Exception e){
           Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_LONG).show();
       }
    }

    public class Pallet implements Serializable {

        private String item_bc;
        private String pk;

        private boolean active;

        public Pallet(String item_bc, String pk, boolean active)  {
            this.item_bc= item_bc;
            this.pk = pk;
            this.active= active;
        }



        public String getItemBC() {
            return item_bc;
        }


        public String getPk() {
            return pk;
        }



        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        @Override
        public String toString() {
            return this.item_bc ;
        }

    }

}
