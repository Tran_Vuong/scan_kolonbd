package gw.genumobile.com.views.gwcore;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.db.MySQLiteOpenHelper;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.gwService;

public class BaseGwActive extends AppCompatActivity{

    protected SharedPreferences appPrefs;

    protected String deviceID="";
    protected String tmpIP="";
    protected Boolean checkIP=false;
    protected int[] bsColors = new int[]{0x30FF0000, 0x300000FF, 0xCCFFFF, 0x99CC33, 0x00FF99};
    protected int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};
    TextView bsTextView;
    protected int fixedRowHeight = 70; //70|30
    protected int fixedHeaderHeight = 80; //80|40
    protected int  Measuredheight = 0, Measuredwidth = 0;
    protected MySQLiteOpenHelper hp;
    private MediaPlayer mediaPlayer;

    public String bsUserPK="",bsUserID="",bsUser="";
    public String getBsUserPK() {
        return bsUserPK;
    }
    public void setBsUserPK(String bsUserPK) {
        this.bsUserPK = bsUserPK;
    }
    public String getBsUserID() {
        return bsUserID;
    }
    public void setBsUserID(String bsUserID) {
        this.bsUserID = bsUserID;
    }
    public String getBsUser() {
        return bsUser;
    }
    public void setBsUser(String bsUser) {
        this.bsUser = bsUser;
    }



    public String[][] getDataConnectThread(BaseGwActive arg_Activity, String l_para) throws ExecutionException, InterruptedException {

        ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));

        String result[][] = networkThread.execute(l_para).get();
        return result;
    }
    public List<JDataTable> CallDataService(BaseGwActive arg_Activity, String l_para) throws ExecutionException, InterruptedException {
            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
            List<JDataTable> jdt = dtLocGroup.getListODataTable();
          return  jdt;
    }

    private Handler customHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
        }
        appPrefs = getSharedPreferences("myConfig", MODE_PRIVATE);
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        deviceID=telephonyManager.getDeviceId();
        //if(deviceID=="" || deviceID.equals("null") || deviceID==null) {
        //    deviceID="000000000000000";
        //}

        //Info User
        String userInfo=appPrefs.getString("userInfo", "");
        String[] strArray = userInfo.split("\\|");
        bsUserID=strArray[0];
        bsUserPK=strArray[1];
        //
        fixedRowHeight = Integer.parseInt(appPrefs.getString("rowHeight", "70"));
        fixedHeaderHeight = Integer.parseInt(appPrefs.getString("headerHeight", "80"));

        tmpIP= appPrefs.getString("server", "");
        checkIP=appPrefs.getBoolean("ckbInternet", Boolean.FALSE);

        Point size = new Point();
        WindowManager w = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
            w.getDefaultDisplay().getSize(size);
            Measuredheight = size.y;
            Measuredwidth = size.x;
            System.out.println("H: " + Measuredheight + "   W: " + Measuredwidth);
            Log.e("H:",String.valueOf(Measuredheight));
            Log.e("W:",String.valueOf(Measuredwidth));

        }

        hp=new MySQLiteOpenHelper(this);
        //customHandler.postDelayed(updateDataToServer, 5*1000);
        setLanguageApplication();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    private Runnable updateDataToServer = new Runnable() {
        public void run()
        {
            doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, 10*1000);
        }
    };

    private void doStart()
    {
       // Toast.makeText(this,"Upload Log",Toast.LENGTH_LONG).show();
    }

    public  void alertToastLong(String alert){
        Toast toast=Toast.makeText(getApplicationContext(), alert, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    public  void alertToastShort(String alert){
        Toast toast=Toast.makeText(getApplicationContext(), alert, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    public boolean checkRecordGridView(int GridViewName){
        TableLayout scrollablePart = (TableLayout) findViewById(GridViewName);
        if(scrollablePart.getChildCount() > 0)return  true;
        else return false;
    }

    //region -----Init control Table-------
    public TextView makeTableColHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(this);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.BLACK);
        bsTextView.setTextSize(18);
        bsTextView.setGravity(Gravity.CENTER);
        bsTextView.setBackgroundColor(Color.parseColor("#B0E2FF"));
        bsTextView.setPadding(0, 2, 0, 2);
        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }

    public TextView makeTableColWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels,int textAlign) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);
        bsTextView = new TextView(this);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.BLACK);
        if(textAlign==-1) {
            bsTextView.setGravity(Gravity.LEFT);
            bsTextView.setPadding(10, 5, 0, 5);
        }
        else if(textAlign==0) {
            bsTextView.setGravity(Gravity.CENTER);
            bsTextView.setPadding(0, 5, 0, 5);
        }
        else {
            bsTextView.setGravity(Gravity.RIGHT);
            bsTextView.setPadding(0, 5, 10, 5);
        }
        bsTextView.setTextSize(16);
        bsTextView.setBackgroundColor(-1);

        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }

    //endregion

    public  void alertRingTone(){

        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if(alert == null){
            // alert is null, using backup
            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            // I can't see this ever being null (as always have a default notification)
            // but just incase
            if(alert == null) {
                // alert backup is null, using 2nd backup
                alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        RingtoneManager.getRingtone(getApplicationContext(), alert).play();
        //Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        //r.play();

        //AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //MediaPlayer thePlayer = MediaPlayer.create(getApplicationContext(), RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        //try {
        //    thePlayer.setVolume( (float)( (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION)) / 7.0),(float)( (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION)) / 7.0));
        //} catch (Exception e) {
        //    e.printStackTrace();
        //}
        //thePlayer.start();
    }


    public  void alertRingMedia(){
        mediaPlayer = MediaPlayer.create(this, R.raw.msbeep01);
        mediaPlayer.start();
        //for(int j=0;j<30000;j++)
        //    mediaPlayer.stop();
    }
    public  void alertRingMedia2(){
        //mediaPlayer = MediaPlayer.create(this,   this.getResources().getIdentifier("msbeep02.mp3","assets", this.getPackageName()));// R.drawable.msbeep02);
        mediaPlayer = MediaPlayer.create(this, R.raw.msbeep02);
        mediaPlayer.start();
        //for(int j=0;j<30000;j++)
        //    mediaPlayer.stop();
    }

    public void onPopAlert(String str_alert) {

        String str=str_alert;

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }

    Activity activity = new Activity();

    public void SetActivity(Activity activity){
        this.activity = activity;
    }
    public void AddEvenKeyboardHiddenWhenClickOutSide(View view) {

        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    HideKeyboard(activity);
                    return false;
                }

            });
        }

        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                AddEvenKeyboardHiddenWhenClickOutSide(innerView);
            }
        }
    }

    public static void HideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    //region TIME DATE PICKER
    //region CLASS DATE - TIME PICKER FRAGMENT
    @SuppressLint("ValidFragment")
    public  class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {


        private String strDate;

        private void setStrDate(String strDate) {
            this.strDate = strDate;
        }

        private String getStrDate() {
            return strDate;
        }

        private boolean GetTime = false;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            if(GetTime == false){
                final Calendar c    = Calendar.getInstance();
                int year            = c.get(Calendar.YEAR);
                int month           = c.get(Calendar.MONTH);
                int day             = c.get(Calendar.DAY_OF_MONTH);

                // Create a new instance of DatePickerDialog and return it
                return new DatePickerDialog(getActivity(), this, year, month, day);
            }

            return null;
        }

        public String a;

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {

        }
    }
    //endregion

    protected void setLanguageApplication(){

        String strLang = "en";
        String languageApp = appPrefs.getString("language", "");
        if(languageApp!=""){
            strLang = languageApp;
        }

        Resources res = this.getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(strLang.toLowerCase());
        res.updateConfiguration(conf, dm);

        //this.setContentView(activity);
    }

}
