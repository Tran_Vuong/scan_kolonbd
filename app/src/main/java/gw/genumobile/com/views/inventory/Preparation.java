package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.interfaces.GridListViewOnePK;
import gw.genumobile.com.interfaces.PopupInquiry;
import gw.genumobile.com.interfaces.frGridListViewMid;
import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.interfaces.GridListView;
import gw.genumobile.com.interfaces.GridListViewMid;
import gw.genumobile.com.interfaces.GridListViewPreparation;
import gw.genumobile.com.interfaces.ItemInquiry;
import gw.genumobile.com.R;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.models.Config;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.InsertUpdAsyncTask;
import gw.genumobile.com.services.MakeSlipAsyncTask;
import gw.genumobile.com.services.ServerAsyncTask;

public class Preparation extends gwFragment implements View.OnClickListener{
    protected Boolean flagUpload=true, flagPopgrd3=true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE = 1;
    public static final int tes_role_pk = 2442;//Role Approve "POP Tablet"

    public boolean autoInYN = false;
    public Editable str_scan;
    Queue queue = new LinkedList();
    Queue queueMid = new LinkedList();
    SQLiteDatabase db = null, db2 = null;
    Cursor cursor, cursor2;
    String data[] = new String[0];

    EditText bc1, tr_qty;
    TextView reqWH, msg_error, txtDate, recyclableTextView, txtTT, txtSentLog, txtRem, txtTotalGridMid, txtTotalGridBot, txtTotalQtyBot, _txtTime, txtCountRequest;
    EditText bc2, reqNo, reqQty, reqBal;
    Calendar c;
    SimpleDateFormat df;
    String[][] myLstAccept = new String[0][0];
    String sql = "", scan_date = "", scan_time = "", unique_id = "";
    String approveYN = "N", lbl_SlipNo1_Tag = "";
    String P_ITEM_BC, P_ITEM_CODE, P_ITEM_NAME, P_TR_LOT_NO, P_TR_QTY, P_SLIP_NO, P_TR_ITEM_PK, P_REQ_D_PK, P_WH_PK, P_TR_WH_IN_NAME;
    int timeUpload = 0, countRequest = 0;
    String reqPK = "";// TLG_GD_REQ_M_PK
    String slipNO = ""; // SLIP_NO
    String _reqQty = ""; // REQ_QTY
    String _whPK = ""; // OUT_WH_PK
    String _reqBal = ""; // BALANCE


    HashMap<String, String> hMapRequestPK = new HashMap<String, String>();
    Button btnApprove,btnViewStt,btnSelectMid,btnList,btnInquiry,btnMakeSlip,btnDelAll,btnListBot,btnNewRequest;
    private Handler customHandler = new Handler();
    private long fileSize = 0;
    private ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();
    private GridListView gridView = new GridListView();

    View rootView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_preparation,container, false);

        autoInYN = appPrefs.getBoolean("autoIncomeYN", Boolean.FALSE);

        btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
        btnApprove.setOnClickListener(this);
        btnViewStt = (Button) rootView.findViewById(R.id.btnViewStt);
        btnViewStt.setOnClickListener(this);
        btnSelectMid = (Button) rootView.findViewById(R.id.btnSelectMid);
        btnSelectMid.setOnClickListener(this);
        btnList = (Button) rootView.findViewById(R.id.btnList);
        btnList.setOnClickListener(this);
        btnInquiry = (Button) rootView.findViewById(R.id.btnInquiry);
        btnInquiry.setOnClickListener(this);
        btnMakeSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMakeSlip.setOnClickListener(this);
        btnDelAll = (Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnListBot = (Button) rootView.findViewById(R.id.btnListBot);
        btnListBot.setOnClickListener(this);
        btnNewRequest = (Button) rootView.findViewById(R.id.btnNewRequest);
        btnNewRequest.setOnClickListener(this);

        txtDate = (TextView) rootView.findViewById(R.id.txtDate);
        msg_error = (TextView) rootView.findViewById(R.id.txtError);
        msg_error.setText("");

        bc1 = (EditText) rootView.findViewById(R.id.edit_Bc1);
        bc2 = (EditText) rootView.findViewById(R.id.edit_Bc2);
        reqNo = (EditText) rootView.findViewById(R.id.edit_reqNo);
        reqQty = (EditText) rootView.findViewById(R.id.edit_reqQty);
        reqBal = (EditText) rootView.findViewById(R.id.edit_reqBal);
        reqWH = (TextView) rootView.findViewById(R.id.txt_GD_WH_PK);

        msg_error.setTextColor(Color.RED);
        msg_error.setTextSize((float) 16.0);

        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        txtDate.setText(formattedDate);
        _txtTime=(TextView) rootView.findViewById(R.id.txtTime);
        if(Measuredwidth <=600) {
            txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);
        }
        OnShowGW_Header();
        OnShowScanLog();
        OnShowScanIn();
        OnShowScanAccept();
        CountSendFirst();
        //OnPermissionApprove();
        init_color();

        bc1.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        msg_error = (TextView) rootView.findViewById(R.id.txtError);
                        msg_error.setTextColor(Color.RED);
                        msg_error.setTextSize((float) 16.0);
                        if (bc1.getText().toString().equals("")) {
                            msg_error.setText("Pls Scan barcode!");
                            bc1.requestFocus();
                        } else {
                            OnSaveData();
                        }
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // gwMActivity is for backspace
                    bc1.clearFocus();
                    //finish();
                    //System.exit(0);
                    //Log.e("IME_TEST", "BACK VIRTUAL");

                }
                return false;
            }

        });

        timeUpload=hp.GetTimeAsyncData();
        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload * 1000);

        return rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run() {
            if(flagUpload)
                doStart();
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload * 1000);
        }
    };

    private  void init_color(){
        //region ------Set color tablet----
        TableLayout tbHeaderLog = (TableLayout) rootView.findViewById(R.id.grdData1);
        tbHeaderLog.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbLog = (TableLayout) rootView.findViewById(R.id.grdScanLog);
        tbLog.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //-------------
        TableLayout tbIn = (TableLayout) rootView.findViewById(R.id.grdScanIn);
        tbIn.setBackgroundColor(Color.parseColor("#c4c4c4"));
        TableLayout tbHeaderIn = (TableLayout) rootView.findViewById(R.id.grdData2);
        tbHeaderIn.setBackgroundColor(Color.parseColor("#00FFFF"));
        //-------------
        TableLayout tbHeaderAccept = (TableLayout) rootView.findViewById(R.id.grdData3);
        tbHeaderAccept.setBackgroundColor(Color.parseColor("#00FFFF"));
        TableLayout tbAccept = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
        tbAccept.setBackgroundColor(Color.parseColor("#c4c4c4"));
        //endregion
    }

    private void doStart() {

        //   CountRequestToDay();
        flagUpload=false;

        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

        cursor2 = db2.rawQuery("select PK, ITEM_BC,GD_SLIP_NO  from INV_TR where DEL_IF=0 and TR_TYPE='"+formID+"' and sent_yn = 'N' order by PK asc LIMIT 20", null);
        System.out.print("\n\n\n cursor2.count: " + String.valueOf(cursor2.getCount()));
        //boolean read=Config.ReadFileConfig();

        if (cursor2.moveToFirst()) {
            // Write your code here to invoke YES event


            //set value message
            msg_error = (TextView) rootView.findViewById(R.id.txtError);
            msg_error.setTextColor(Color.RED);
            msg_error.setText("");

            //Start: 12-1-2016: Giang
            //data=new String [cursor2.getCount()];
            data = new String[1];
            //End 12-1-2016: Giang
            int j = 0;
            String para = "";
            boolean flag = false;
            do {
                flag = true;
                for (int i = 0; i < cursor2.getColumnCount(); i++) {
                    if (para.length() <= 0) {
                        if (cursor2.getString(i) != null)
                            para += cursor2.getString(i) + "|";
                        else
                            para += "|";
                    } else {

                        if (flag == true) {
                            if (cursor2.getString(i) != null) {
                                para += cursor2.getString(i);
                                flag = false;
                            } else {
                                para += "|";
                                flag = false;
                            }
                        } else {
                            if (cursor2.getString(i) != null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }

                }
                para += "|" + deviceID;
                para += "|" + bsUserID;
                para += "*|*";

            } while (cursor2.moveToNext());
            //////////////////////////
            para += "|!LG_MPOS_UPL_PREPARA";
            data[j++] = para;
            //System.out.print("\n\n\n para upload Preparation: " + para);
            Log.e("para upl Preparation: ", para);

            // asyns server
            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                task.execute(data);
                gwMActivity.alertToastShort("Send to Server");
            } else
            {
                flagUpload=true;
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        }
        else{
            flagUpload=true;
        }
        cursor2.close();
        db2.close();
    }



    public void OnPermissionApprove() {
        String para = bsUserPK + '|' + String.valueOf(tes_role_pk);
        List<String> arr_role = GetDataOnServer("ROLE", para);
        if (arr_role.size() > 0) {
            approveYN = arr_role.get(0);
            Log.e("approveYN", approveYN);
        }
        btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
        if (approveYN.equals("Y")) {
            btnApprove.setVisibility(View.VISIBLE);
        } else
            btnApprove.setVisibility(View.GONE);

        Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
        btnMSlip.setVisibility(View.VISIBLE);
    }
    //****************************************************************************
    public void OnSaveBC() {
        if (bc1.getText().toString().equals("")) {
            msg_error.setTextColor(Color.RED);
            msg_error.setText("Pls Scan barcode!");
            bc1.getText().clear();
            bc1.requestFocus();
            return;
        } else {

            String str_scan = bc1.getText().toString().toUpperCase();
            try {

                if(str_scan.length()>20) {
                    msg_error.setText("Barcode has length more 20 char !");
                    bc1.getText().clear();
                    bc1.requestFocus();
                    return;
                }

                boolean isExists= hp.isExistBarcode(str_scan, formID);// check barcode exist in data
                if (isExists)// exist data
                {
                    alertRingMedia();
                    msg_error.setText("Barcode exist in database!");
                    bc1.getText().clear();
                    bc1.requestFocus();
                    return;
                } else {
                    bc2 = (EditText) rootView.findViewById(R.id.edit_Bc2);
                    bc2.setText(str_scan);
                    if (OnSave()) {
                        msg_error.setText("Save success.");
                        bc1.getText().clear();
                        bc1.requestFocus();
                        OnShowScanLog();
                        OnShowScanIn();
                    }
                }
            } catch (Exception ex) {
                bc1.setText("");
                msg_error.setText(ex.getMessage());
                Log.e("OnSaveBC", ex.getMessage());
            } finally {
                bc1.setText("");
            }
        }
    }

    public void OnSaveData() {
        if (bc1.getText().equals("")) {
            msg_error.setText("Pls Scan barcode!");
            return;
            //bc1.requestFocus();
        }

        String str_scan = bc1.getText().toString().toUpperCase();
        int l_length = bc1.getText().toString().length();
        String str_req_no = bc1.getText().toString().substring(1, l_length);
        try {
            if (str_scan.equals("")) {
                msg_error.setText("Pls Scan barcode!");
                return;
            }
            //scan slip no
            if (lbl_SlipNo1_Tag.equals("") && str_scan.indexOf("S") == 0) {
                System.out.println("str_req_no " + str_req_no);
                List<String> lables = GetDataOnServer("SlipNo", str_req_no);
                if (lables.size() > 0) {
                    reqPK = lables.get(0); // TLG_GD_REQ_M_PK
                    slipNO = lables.get(1); // SLIP_NO
                    _reqQty = lables.get(2); // REQ_QTY
                    _whPK = lables.get(3); // OUT_WH_PK
                    _reqBal = lables.get(4); // BALANCE

                    lbl_SlipNo1_Tag = slipNO;
                    reqNo.setText(slipNO);
                    reqQty.setText(_reqQty);
                    reqBal.setText(_reqBal);
                    reqWH.setText(_whPK);
                    msg_error.setText("Req get success");

                    bc1.getText().clear();
                    bc1.requestFocus();

                    //hMapRequestPK.put(reqPK,reqPK);
                    OnShowScanAccept();
                    return;
                } else {
                    msg_error.setText("Slip No '" + str_req_no + "' not exist !!!");

                    lbl_SlipNo1_Tag = "";
                    reqNo.getText().clear();
                    reqQty.getText().clear();
                    reqBal.getText().clear();

                    bc1.getText().clear();
                    bc1.requestFocus();
                    return;
                }
            }

            //String WHOUT_PK=reqWH.getText().toString();

            //scan already slip no
            if (lbl_SlipNo1_Tag.equals("")) {
                msg_error.setText("Slip No '" + str_scan + "' not exist !!!");
                bc1.getText().clear();
                bc1.requestFocus();
                return;
            } else {
                boolean isExist=hp.isExistBarcode(str_scan,formID);// check barcode exist in data
                if (isExist)// exist data
                {
                    alertRingMedia();
                    msg_error.setText("Barcode exist in database!");
                    bc1.getText().clear();
                    bc1.requestFocus();
                    return;
                } else {
                    bc2 = (EditText) rootView.findViewById(R.id.edit_Bc2);
                    bc2.setText(str_scan);
                    if(str_scan.length()>20) {
                        msg_error.setText("Barcode has length more 20 char !");
                        bc1.getText().clear();
                        bc1.requestFocus();
                        return;
                    }
                    if (OnSave()) {
                        msg_error.setText("Save success.");
                        bc1.getText().clear();
                        bc1.requestFocus();
                        OnShowScanLog();
                    }
                }
            }

        } catch (Exception ex) {
            //save data error write to file log
            gwMActivity.alertToastShort("Save Error: "+ex.toString());
            bc1.getText().clear();
            bc1.requestFocus();
        }
    }

    //****************************************************************************
    public List<String> GetDataOnServer(String type, String para) {
        List<String> labels = new ArrayList<String>();
        try {
            String l_para = "";
            if (type.endsWith("SlipNo")) //WH
            {
                l_para = "1,LG_MPOS_GET_GD_PRE_REQ," + para;
            }
            if (type.endsWith("ROLE")) {
                l_para = "1,LG_MPOS_GET_ROLE," + para;
            }

            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String result[][] = networkThread.execute(l_para).get();

            for (int i = 0; i < result.length; i++) {
                if (type.endsWith("SlipNo")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                }
                if (type.endsWith("ROLE")) {
                    for (int j = 0; j < result[i].length; j++)
                        labels.add(result[i][j].toString());
                }
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(type + ": " + ex.getMessage());
        }
        return labels;
    }

    //**************************************************************
    public boolean OnSave() {
        boolean kq = false;
        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        try {

            scan_date = CDate.getDateyyyyMMdd();
            scan_time = CDate.getDateYMDHHmmss();

            bc1 = (EditText) rootView.findViewById(R.id.edit_Bc1);
            bc2 = (EditText) rootView.findViewById(R.id.edit_Bc2);
            reqNo = (EditText) rootView.findViewById(R.id.edit_reqNo);

            // get data input control to var then insert db
            P_SLIP_NO = reqNo.getText().toString();
            P_ITEM_BC = bc2.getText().toString();

            db.execSQL("INSERT INTO INV_TR(ITEM_BC,GD_SLIP_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                    + "VALUES('"
                    + P_ITEM_BC + "','"
                    + P_SLIP_NO + "','"
                    + scan_date + "','"
                    + scan_time + "','"
                    + "N" + "','"
                    + " " + "','"
                    + formID + "');");

            db.close();
            kq = true;
        } catch (Exception ex) {
            msg_error.setText("Save Error!!!");
            kq = false;
        }
        return kq;
    }

    ////////////////////////////////////////////////
    public void onApprove(View view) {
        String title = "Confirm Approve...";
        String mess = "Are you sure you want to Approve";
        alertDialogYN(title, mess, "onApprove");
    }

    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onApprove")) {
                    Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    btnMSlip.setVisibility(View.GONE);
                    btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    btnApprove.setVisibility(View.GONE);
                    ProcessApprove();
                }
                if (_type.equals("onDelAll")) {
                    ProcessDeleteAll();
                }
                if (_type.equals("onMakeSlip")) {

                    //Button btnMSlip = (Button) rootView.findViewById(R.id.btnMakeSlip);
                    //btnMSlip.setVisibility(View.GONE);
                    btnApprove = (Button) rootView.findViewById(R.id.btnApprove);
                    btnApprove.setVisibility(View.GONE);
                    ProcessMakeSlip();

                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    ///////////////////////////////////////////////
    public void ProcessApprove() {
        unique_id = CDate.getDateyyyyMMddhhmmss();
        if (queue.size() > 0) {
            Object[] myLst = queue.toArray();
            //reset null when user click make slip again
            queue = new LinkedList();
            data = new String[myLst.length];
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                int j = 0;
                String para = "";
                boolean flag = false;
                for (int i = 0; i < myLst.length; i++) {
                    flag = true;
                    cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WH_OUT_PK,TLG_GD_REQ_M_PK,REMARKS   from INV_TR where del_if=0 and SLIP_NO='-' and TR_TYPE='"+formID+"' and pk=" + myLst[i], null);
                    //System.out.print("\n\n\n cursor.count: "+ String.valueOf(cursor.getCount()));
                    if (cursor.moveToFirst()) {

                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {

                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }

                        }
                        para += "|" + unique_id;
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "*|*";
                    }
                }
                para += "|!lg_mpos_pro_gd_makeslip";
                data[j++] = para;
                //System.out.print("\n\n ******para make slip goods delivery: "+para);

            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            //ApproveAsyncTask task = new ApproveAsyncTask(gwMActivity,this);
            //task.execute(data);
        } else {
            try {
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WH_OUT_PK,TLG_GD_REQ_M_PK,REMARKS   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='"+formID+"' ", null);
                int count = cursor.getCount();
                data = new String[count];
                int j = 0;
                String para = "";
                boolean flag = false;
                if (cursor.moveToFirst()) {
                    do {
                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k) + "|";
                                else
                                    para += "0|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }

                        }
                        para += "|" + unique_id;
                        para += "|" + bsUserID;
                        para += "|" + bsUserPK;
                        para += "*|*";

                    } while (cursor.moveToNext());

                    para += "|!lg_mpos_pro_gd_makeslip";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip goods delivery: " + para);
                }
            } catch (Exception ex) {
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
            } finally {
                cursor.close();
                db.close();
            }
            // asyns server
            // ApproveAsyncTask task = new ApproveAsyncTask(gwMActivity,this);
            // task.execute(data);
        }

    }

    public void onClickViewStt(View view) {


        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(dataStatus);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();


    }

    public void onClickSelectMid(View view)  {
        Object[] myLst = queueMid.toArray();
        if(myLst.length>0) {
            String str = "Are you sure you want to select ";
            //Dialog
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
            // Setting Dialog Title
            alertDialog.setTitle("Confirm Select ...");
            // Setting Dialog Message
            alertDialog.setMessage(str);
            // Setting Icon to Dialog
            alertDialog.setIcon(R.drawable.cfm_save);

            // Setting Negative "YES" Button
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke YES event
                    try {
                        Object[] myLst = queueMid.toArray();
                        queueMid = new LinkedList();
                        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        for (int i = 0; i < myLst.length; i++) {
                            String val = myLst[i].toString();
                            String _pk = val.split("\\|")[0].toString();
                            String _status = val.split("\\|")[1].toString();
                            if (_status.equals("005")) {
                                sql = "UPDATE INV_TR set  REMARKS='Not FIFO',SLIP_NO='-'  where PK = " + _pk;
                                db.execSQL(sql);

                                ProcessStatus005_008(_pk);
                            }
                            if (_status.equals("008")) {
                                sql = "UPDATE INV_TR set STATUS = '000', REMARKS='Qty larger'  where PK = " + _pk;
                                db.execSQL(sql);

                                OnShowScanIn();
                                OnShowScanAccept();
                            }
                        }


                    } catch (Exception ex) {
                        gwMActivity.alertToastLong("Select Mid :" + ex.getMessage());
                    } finally {
                        db.close();
                    }

                }
            });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event
                    dialog.cancel();
                }
            });
            // Showing Alert Message
            alertDialog.show();
        }else{
            gwMActivity.alertToastShort("Select data first!");
            return;
        }
    }

    public void onClickNewRequest(View view){

        String str = "Are you sure scan new request ?";

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm  ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);

        // Setting Negative "YES" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                slipNO = "";
                bc1.setText("");
                bc2.setText("");
                reqNo.setText("");
                reqQty.setText("");
                reqBal.setText("");

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }


    public void ProcessStatus005_008(String _pk) {
        db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        sql = "select PK, ITEM_BC,GD_SLIP_NO  from INV_TR where DEL_IF=0 and TR_TYPE='"+formID+"' and PK = " + _pk;
        cursor2 = db2.rawQuery(sql, null);
        System.out.print("\n\n\n cursor2.count: " + String.valueOf(cursor2.getCount()));
        //data=new String [cursor2.getCount()];
        boolean read = Config.ReadFileConfig();

        if (cursor2.moveToFirst()) {

            data = new String[1];
            int j = 0;
            String para = "";
            boolean flag = false;
            do {
                // labels.add(cursor.getString(1));
                flag = true;
                for (int i = 0; i < cursor2.getColumnCount(); i++) {
                    if (para.length() <= 0) {
                        if (cursor2.getString(i) != null)
                            para += cursor2.getString(i) + "|";
                        else
                            para += "|";
                    } else {

                        if (flag == true) {
                            if (cursor2.getString(i) != null) {
                                para += cursor2.getString(i);
                                flag = false;
                            } else {
                                para += "|";
                                flag = false;
                            }
                        } else {
                            if (cursor2.getString(i) != null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }
                }
                para += "|" + deviceID;
                para += "|" + bsUserID;
                para += "*|*";

            } while (cursor2.moveToNext());
            //////////////////////////
            para += "|!" + "LG_MPOS_UPL_PREPARA_005";
            data[j++] = para;
            System.out.print("\n\n\n para upload: " + para);

            cursor2.close();
            db2.close();
            if (CNetwork.isInternet(tmpIP,checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                task.execute(data);
            } else
            {
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }

        }

    }

    public void onClickInquiry(View view){

        if(!checkRecordGridView(R.id.grdScanAccept))return;
        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new PopupInquiry();
        Bundle args = new Bundle();

        args.putString("type", formID);

        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    public void onMakeSlip(View view) {

        //Check het Barcode send to server
        if(checkRecordGridView(R.id.grdScanLog)){
            gwMActivity.alertToastLong( gwMActivity.getResources().getString(R.string.tvAlertMakeSlip) );
            return;
        }
        //Check have value make slip
        if(hp.isMakeSlip(formID)==false) return;
//        if(!checkRecordGridView(R.id.grdScanAccept))return;

        boolean flag = true;
        if (!flag) {
            msg_error = (TextView) rootView.findViewById(R.id.txtError);
            msg_error.setText("Please, select Item.");
            msg_error.setTextColor(Color.RED);
            msg_error.setTextSize((float) 16.0);
            return;
        } else {
            String title = "Confirm Make Slip...";
            String mess = "Are you sure you want to Make Slip";
            alertDialogYN(title, mess, "onMakeSlip");
        }
    }

    public void ProcessMakeSlip() {
        unique_id = CDate.getDateyyyyMMddhhmmss();

        if (queue.size() > 0) {
            return;
        } else {
            try {
                int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
                db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                //cursor = db.rawQuery("select PK, TR_ITEM_PK,ITEM_BC,TR_QTY ,UOM, TR_LOT_NO, TLG_SA_SALEORDER_D_PK, TLG_GD_REQ_D_PK, TLG_POP_LABEL_PK, SCAN_DATE,GD_SLIP_NO,CUST_PK, TR_WAREHOUSE_PK,TLG_GD_REQ_M_PK,REMARKS   from INV_TR where del_if=0 and STATUS='000' and SLIP_NO='-'  and TR_TYPE='6' ",null);
                sql =       " SELECT TR_ITEM_PK, TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,TR_QTY, UOM,TR_LOT_NO,TR_WH_OUT_PK,TLG_GD_REQ_D_PK,EXECKEY " +
                        " FROM " +
                        "       (   " +
                        "           SELECT TR_ITEM_PK,  SUM(TR_QTY) TR_QTY, UOM,  TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,TR_LOT_NO,TR_WH_OUT_PK,TLG_GD_REQ_D_PK,EXECKEY " +
                        "           FROM INV_TR " +
                        "           WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"'  AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND STATUS='000' AND SLIP_NO IN('-','') " +
                        "           GROUP BY   TR_ITEM_PK, UOM, TLG_GD_REQ_M_PK, TLG_SA_SALEORDER_D_PK, TR_LOT_NO,TR_WH_OUT_PK,TLG_GD_REQ_D_PK, EXECKEY" +
                        "       )";
                cursor = db.rawQuery(sql, null);
                // /int count=cursor.getCount();
                //data=new String [count];
                data=new String [1];
                int j=0;
                String para = "";
                boolean flag=false;
                if (cursor.moveToFirst()) {
                    do {
                        flag=true;
                        for (int k = 0; k < cursor.getColumnCount()-1; k++) {
                            if (para.length() <= 0) {
                                if(cursor.getString(k)!= null)
                                    para += cursor.getString(k)+"|";
                                else
                                    para += "0|";
                            }
                            else
                            {
                                if(flag==true){
                                    if(cursor.getString(k)!= null) {
                                        para += cursor.getString(k);
                                        flag=false;
                                    }
                                    else {
                                        para += "|";
                                        flag=false;
                                    }
                                }
                                else{
                                    if(cursor.getString(k)!= null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        String execkey=cursor.getString(cursor.getColumnIndex("EXECKEY"));
                        if( execkey==null || execkey.equals("") ) {
                            int ex_item_pk=Integer.valueOf(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")));
                            int ex_req_m_pk=Integer.valueOf(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK")));
                            String ex_lot_no=cursor.getString(cursor.getColumnIndex("TR_LOT_NO"));
                            int ex_sale_order_d_pk=Integer.valueOf(cursor.getString(cursor.getColumnIndex("TLG_SA_SALEORDER_D_PK")));
                            hp.updateExeckeyGoodsDeli(formID,ex_item_pk,ex_req_m_pk,ex_sale_order_d_pk,ex_lot_no,unique_id);

                            para += "|" + unique_id;
                        }else {
                            para += "|" + execkey;
                        }
                        para += "|"+ bsUserID;
                        para += "|"+ bsUserPK;
                        para += "|" + deviceID;
                        para +="*|*";

                    }while (cursor.moveToNext());

                    para += "|!LG_MPOS_PRO_PREPA_MSLIP";
                    data[j++] = para;
                    System.out.print("\n\n ******para make slip goods delivery: " + para);
                    // asyns server
                    if (CNetwork.isInternet(tmpIP,checkIP)) {
                        MakeSlipAsyncTask task = new MakeSlipAsyncTask(gwMActivity,this);
                        task.execute(data);
                    } else
                    {
                        gwMActivity.alertToastLong("Network is broken. Please check network again !");
                    }
                }
            }
            catch (Exception ex){
                gwMActivity.alertToastLong("onMakeSlip :" + ex.getMessage());
                Log.e("onMakeSlip :", ex.getMessage());
            }
            finally {
                cursor.close();
                db.close();
            }


        }
    }

    ////////////////////////////////////////////////
    //DELETE ALL
    ///////////////////////////////////////////////
    public void onDelAll(View view) {

        if(!hp.isCheckDelete(formID)) return;

        String title = "Confirm Delete...";
        String mess = "Are you sure you want to delete all";
        alertDialogYN(title, mess, "onDelAll");
    }

    public void ProcessDeleteAll() {
        int date_previous = Integer.parseInt(CDate.getDatePrevious(2));
        int count = 0;

        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String para = "";
            boolean flag = true;

            if (queue.size() > 0) {
                return;
            } else {
                cursor = db.rawQuery("select ITEM_BC from INV_TR where del_if=0 and TR_TYPE='"+formID+"' and SLIP_NO='-' and STATUS ='000' AND SCAN_DATE > '" + date_previous + "'", null);
                int j = 0;

                if (cursor.moveToFirst()) {
                    data = new String[1];
                    do {

                        flag = true;
                        for (int k = 0; k < cursor.getColumnCount(); k++) {
                            if (para.length() <= 0) {
                                if (cursor.getString(k) != null)
                                    para += cursor.getString(k);
                                else
                                    para += "|";
                            } else {
                                if (flag == true) {
                                    if (cursor.getString(k) != null) {
                                        para += cursor.getString(k);
                                        flag = false;
                                    } else {
                                        para += "|";
                                        flag = false;
                                    }
                                } else {
                                    if (cursor.getString(k) != null)
                                        para += "|" + cursor.getString(k);
                                    else
                                        para += "|";
                                }
                            }
                        }
                        para += "|" + deviceID;
                        para += "|" + bsUserID;
                        para += "|" + formID;
                        para += "|delete";
                        para += "*|*";
                    } while (cursor.moveToNext());

                    para += "|!LG_MPOS_UPD_INV_TR_DEL";
                    data[j] = para;
                }
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("onDelAll :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        if (CNetwork.isInternet(tmpIP,checkIP)) {
            InsertUpdAsyncTask task = new InsertUpdAsyncTask(gwMActivity,this);
            task.execute(data);
        } else
        {
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }


    }

    public void onDelAllFinish() {
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if (queue.size() > 0) {
                Object[] myLst = queue.toArray();
                //reset null when user click make slip again
                queue = new LinkedList();


            } else {
                //db.execSQL("DELETE FROM INV_TR where STATUS NOT IN('000') and TR_TYPE ='"+formID+"';");
                db.execSQL("DELETE FROM INV_TR where (STATUS NOT IN('000') or (STATUS='000' and SLIP_NO NOT IN('-',''))) and TR_TYPE ='"+formID+"';");
            }
        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {
            db.close();
            OnShowScanLog();
            OnShowScanIn();
            OnShowScanAccept();
            CountSendRecord();
            gwMActivity.alertToastShort("Delete success..");
        }
    }

    //End Delete

    //End Delete
    ///////////////////////////////////////////////
    public void onListGrid(View view)  {
        if(!checkRecordGridView(R.id.grdScanIn))return;

        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  frGridListViewMid.newInstance(1,formID);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    ///////////////////////////////////////////////
    public void onListGridBot(View view) {

        if(!checkRecordGridView(R.id.grdScanAccept))return;


        FragmentManager fm = gwMActivity.getSupportFragmentManager();
        DialogFragment dialogFragment =  new GridListViewPreparation();
        Bundle args = new Bundle();

        args.putString("type", formID);
        args.putString("reqPK", reqPK);
        args.putSerializable("hMapRequestPK", hMapRequestPK);


        dialogFragment.setArguments(args);
        dialogFragment.setCancelable(false);
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(fm.beginTransaction(), "dialog");
    }

    /**
     * Xử lý kết quả trả về ở đây
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Kiểm tra có đúng requestCode =REQUEST_CODE_INPUT hay không
        //Vì ta có thể mở Activity với những RequestCode khác nhau
        if (requestCode == REQUEST_CODE) {
            //Kiểm trả ResultCode trả về, cái này ở bên InputDataActivity truyền về
            //OnPermissionApprove();
            OnShowScanAccept();
            flagPopgrd3=true;
            switch (resultCode) {
                case RESULT_CODE:

                    break;
            }
        }
    }
    //----------------------------------------------------------
    ///Load Header grid

    public void OnShowGW_Header()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;
        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Status Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData2);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Pk", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvIncomeDate), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvNhanVien), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvWhName), scrollableColumnWidths[5], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvLineName), scrollableColumnWidths[5], fixedHeaderHeight));
        scrollablePart.addView(row);

        // Accept Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvTotal), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvUom), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(gwMActivity.getResources().getString(R.string.tvRequestNo), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Request PK", 0, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("PK", 0, fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    ///End Load Header grid
    //----------------------------------------------------------

    // show data scan log
    public void OnShowScanLog() {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+formID+"' and SENT_YN='N' order by PK desc LIMIT 20", null);// TLG_LABEL

            int count = cursor.getCount();
            //Log.e("count log",String.valueOf(count));
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight,-1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            // total scan log
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+formID+"' and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            txtRem = (TextView) rootView.findViewById(R.id.txtRemain);
            txtRem.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
            //Log.e("Grid Scan log Error -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanIn() {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select (select COUNT(0)" +
                    "from INV_TR t1 " +
                    "where del_if = 0 and t1.pk <= t2.pk AND SCAN_DATE = '" + scan_date + "' AND T1.STATUS NOT IN('000', ' ')" +
                    ") AS SEQ, T2.PK, T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.TR_LOT_NO, T2.INCOME_DATE, T2.CHARGER, T2.TR_WH_OUT_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK " +
                    " FROM INV_TR t2 " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SCAN_DATE = '" + scan_date + "' AND T2.STATUS NOT IN('000', ' ')  " +
                    " GROUP BY T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.SLIP_NO, T2.TR_LOT_NO, T2.INCOME_DATE, T2.CHARGER, T2.TR_WH_OUT_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK" +
                    " ORDER BY pk desc LIMIT 10 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("PK")), 0, fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SEQ")), scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")), scrollableColumnWidths[5], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight,1));

                    row.setOnClickListener(new View.OnClickListener() {
                        TextView tv11;
                        boolean duplicate = false;

                        @Override
                        public void onClick(View v) {

                            duplicate = false;
                            TableRow tr1 = (TableRow) v;
                            TextView tvSlipNo = (TextView) tr1.getChildAt(4); //STATUS
                            String st_slipNO = tvSlipNo.getText().toString();

                            //if(st_slipNO.equals("005") || st_slipNO.equals("008")){
                            if (st_slipNO.equals("005")) {
                                TextView tv1 = (TextView) tr1.getChildAt(0); //INV_TR_PK
                                String value = tv1.getText().toString() + "|" + st_slipNO;

                                int xx = tr1.getChildCount();
                                for (int i = 0; i < xx; i++) {
                                    tv11 = (TextView) tr1.getChildAt(i);
                                    tv11.setTextColor(Color.rgb(255, 99, 71));
                                }

                                if (queueMid.size() > 0) {

                                    if (queueMid.contains(value)) {
                                        queueMid.remove(value);

                                        for (int i = 0; i < xx; i++) {
                                            tv11 = (TextView) tr1.getChildAt(i);
                                            tv11.setTextColor(Color.BLACK);
                                        }
                                        duplicate = true;
                                    }
                                }

                                if (!duplicate)
                                    queueMid.add(value);

                            }
                            gwMActivity.alertToastShort(queueMid.size() + "");

                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND TR_TYPE = '"+formID+"' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
            txtTotalGridMid.setText("Total: " + count + " ");
            txtTotalGridMid.setTextColor(Color.BLUE);
            txtTotalGridMid.setTextSize((float) 18.0);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
            //Log.e("OnShowScanIn Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan accept
    public void OnShowScanAccept() {
        try {
            int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            if(reqPK==""){
                sql =       "   SELECT ITEM_NAME, ITEM_CODE, COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, UOM, TR_ITEM_PK, TLG_GD_REQ_M_PK,SLIP_NO, GD_SLIP_NO,TLG_SA_SALEORDER_D_PK,TR_WH_OUT_PK,EXECKEY " +
                        "   FROM INV_TR " +
                        "    WHERE DEL_IF = 0 AND TR_TYPE = '"+ formID +"'  AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND STATUS='000' " +
                        "    GROUP BY ITEM_NAME, ITEM_CODE, UOM, TR_ITEM_PK, TLG_GD_REQ_M_PK, SLIP_NO, GD_SLIP_NO, TLG_SA_SALEORDER_D_PK,TR_WH_OUT_PK,EXECKEY" +
                        "    ORDER BY EXECKEY desc  LIMIT 30 ";

            }else{

                sql =       "   SELECT ITEM_NAME, ITEM_CODE, COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, UOM, TR_ITEM_PK, TLG_GD_REQ_M_PK,SLIP_NO, GD_SLIP_NO,TLG_SA_SALEORDER_D_PK,TR_WH_OUT_PK,EXECKEY " +
                        "   FROM INV_TR " +
                        "    WHERE DEL_IF = 0 AND TR_TYPE = '"+ formID +"'  AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' " +
                        "    AND STATUS='000'  AND TLG_GD_REQ_M_PK = " + reqPK + "" +
                        "    GROUP BY ITEM_NAME, ITEM_CODE, UOM, TR_ITEM_PK, TLG_GD_REQ_M_PK, SLIP_NO, GD_SLIP_NO, TLG_SA_SALEORDER_D_PK,TR_WH_OUT_PK,EXECKEY" +
                        "    ORDER BY EXECKEY desc  LIMIT 30 ";
            }



            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    //row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TOTAL")), scrollableColumnWidths[1], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("UOM")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("GD_SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")), 0, fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK")), 0, fixedRowHeight, 0));

                    row.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            onShowClickGridBC(v);
                        }
                    });

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }


            if(reqPK == ""){
                sql =   " select pk,TR_QTY " +
                        " FROM INV_TR  " +
                        " WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SCAN_DATE > " + date_previous   +
                        " AND SENT_YN = 'Y' AND STATUS='000'";
            }else{
                sql =   " select pk,TR_QTY " +
                        " FROM INV_TR  " +
                        " WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SCAN_DATE > " + date_previous  +
                        " AND SENT_YN = 'Y' AND STATUS='000' AND TLG_GD_REQ_M_PK = '" + reqPK + "'";
            }

            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty = 0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty = _qty + Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
            txtTotalGridBot.setText("Total: " + count + " ");
            txtTotalGridBot.setTextColor(Color.BLUE);
            txtTotalGridBot.setTextSize((float) 16.0);

            txtTotalQtyBot = (TextView) rootView.findViewById(R.id.txtTotalQtyBot);
            txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty)+ " ");
            txtTotalQtyBot.setTextColor(Color.BLACK);
            txtTotalQtyBot.setTextSize((float) 16.0);

            sql =   " select DISTINCT TLG_GD_REQ_M_PK, GD_SLIP_NO " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' " +
                    " AND SCAN_DATE > " + date_previous + " AND SENT_YN = 'Y' AND STATUS='000' ";

            cursor = db.rawQuery(sql, null);
            if (cursor != null && cursor.moveToFirst()) {
                countRequest = cursor.getCount();

                hMapRequestPK.clear();
                hMapRequestPK.put("00", "All");
                ///// TODO: 2016-01-14  add to hashmap
                for (int i = 0; i < countRequest; i++) {
                    hMapRequestPK.put(cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK")), cursor.getString(cursor.getColumnIndex("GD_SLIP_NO")));
                    cursor.moveToNext();
                }
            }


            txtCountRequest = (TextView) rootView.findViewById(R.id.txtCountRequest);
            txtCountRequest.setText("Request: " + countRequest);
            txtCountRequest.setTextColor(Color.BLACK);
            txtCountRequest.setTextSize((float) 18.0);


            ///// TODO: 2016-01-13 Update lst_sp

            //Giang: End 12/01/2016
        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
            //Log.e("OnShowScanAccept Error: -->", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }
    public void onShowClickGridBC(View v){
        TextView tv11;
        boolean duplicate = false;
        if(flagPopgrd3) {
            flagPopgrd3=false;
            duplicate = false;
            TableRow tr1 = (TableRow) v;

            TextView tvItemCode = (TextView) tr1.getChildAt(1); //Item Code
            String ITEM_CODE = tvItemCode.getText().toString();

            TextView tvSlipNo = (TextView) tr1.getChildAt(5); //Slip No
            String SLIPNO = tvSlipNo.getText().toString();

            TextView requestPk = (TextView) tr1.getChildAt(7); //TR_ITEM_PK
            String TR_ITEM_PK = requestPk.getText().toString();

            TextView tvPK = (TextView) tr1.getChildAt(8); //TLG_GD_REQ_M_PK
            String TLG_GD_REQ_M_PK = tvPK.getText().toString();


            FragmentManager fm = gwMActivity.getSupportFragmentManager();
            DialogFragment dialogFragment =  new GridListViewOnePK();
            Bundle args = new Bundle();

            args.putString("form_id", formID);
            args.putString("ITEM_CODE", ITEM_CODE);
            args.putString("SLIP_NO", SLIPNO);
            args.putString("TR_ITEM_PK", TR_ITEM_PK);
            args.putString("TLG_GD_REQ_M_PK", TLG_GD_REQ_M_PK);
            args.putString("TLG_GD_REQ_D_PK", "");

            dialogFragment.setArguments(args);
            dialogFragment.setCancelable(false);
            dialogFragment.setTargetFragment(this, REQUEST_CODE);
            dialogFragment.show(fm.beginTransaction(), "dialog");
        }
    }
    public void CountSendRecord() {
        flagUpload=true;
        // total scan log
        txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countMid = Integer.parseInt(txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        int countBot = Integer.parseInt(txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));
        txtSentLog = (TextView) rootView.findViewById(R.id.txtSent);
        txtSentLog.setText("Send: " + (countMid + countBot));

        txtRem = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(txtRem.getText().toString().replaceAll("[\\D]", ""));

        txtTT = (TextView) rootView.findViewById(R.id.txtTotal);
        txtTT.setText("TT: " + (countMid + countBot + countRe));


    }

    public void CountSendFirst() {
        // total scan log
        txtTotalGridMid = (TextView) rootView.findViewById(R.id.txtTotalMid);
        int countMid = Integer.parseInt(txtTotalGridMid.getText().toString().replaceAll("[\\D]", ""));
        txtTotalGridBot = (TextView) rootView.findViewById(R.id.txtTotalBot);
        int countBot = Integer.parseInt(txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));

        txtSentLog = (TextView) rootView.findViewById(R.id.txtSent);
        txtSentLog.setText("Send: " + (countMid + countBot));

        txtRem = (TextView) rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(txtRem.getText().toString().replaceAll("[\\D]", ""));

        txtTT = (TextView) rootView.findViewById(R.id.txtTotal);
        txtTT.setText("TT: " + (countMid + countBot + countRe));

    }

    //////////////////////////
    // check data inv_tr
    public int Check_Exist_PK(String l_bc_item) {
        int countRow = 0;
        try {
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            bc2 = (EditText) rootView.findViewById(R.id.edit_Bc2);

            String countQuery = "SELECT PK FROM INV_TR where tr_type='12' AND (status='000' or status=' ') and del_if=0 and ITEM_BC ='" + l_bc_item + "' ";

            // db = gwMActivity.getReadableDatabase();
            cursor = db.rawQuery(countQuery, null);
            // Check_Exist_PK=cursor.getCount();
            if (cursor != null && cursor.moveToFirst()) {
                countRow = cursor.getCount();
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong("Check_Exist_PK :" + ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return countRow;

    }

    ////////////////////////////////////////////////
    //
    ///////////////////////////////////////////////
    public void onPopAlert(String str_alert) {

        String str = str_alert;

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("Thong Bao ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInquiry:
                onClickInquiry(view);

                break;
            case R.id.btnApprove:
                onApprove(view);

                break;
            case R.id.btnViewStt:
                onClickViewStt(view);

                break;
            case R.id.btnSelectMid:
                onClickSelectMid(view);

                break;
            case R.id.btnListBot:
                onListGridBot(view);

                break;
            case R.id.btnDelBC:

                break;
            case R.id.btnList:
                onListGrid(view);

                break;
            case R.id.btnDelAll:
                onDelAll(view);

                break;
            case R.id.btnMakeSlip:
                onMakeSlip(view);

                break;
            case R.id.btnNewRequest:
                onClickNewRequest(view);

                break;

            default:
                break;
        }
    }
}
