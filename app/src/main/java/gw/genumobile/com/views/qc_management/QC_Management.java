package gw.genumobile.com.views.qc_management;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.services.HandlerWebService;
import gw.genumobile.com.services.QCManagementAsyncTask;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;
import gw.genumobile.com.utils.CNumber;
import gw.genumobile.com.views.gwFragment;


/**
 * Something, It's an ox
 */
public class QC_Management extends gwFragment implements View.OnClickListener {

    //region Variable
    public int REQUEST_CODE = 1;
    public int RESULT_CODE  = 1;

    SQLiteDatabase db = null;

    Button _btnQC, _btnDelete;
    TextView etSlipNo;
    EditText myBC;

    String  scan_date = "", scan_time = "", pk_user = "", user = "", user_id = "";

    private Bitmap imageBitmap;

    private ProgressDialog progress  = null;
    private int QC_Type_PK = 99;

    private String _NOT_GOOD = "NG";
    private String _OK= "OK";
    private String _TLG_INSPECTION_ENTRY_M_PK = "";
    private String _Table_INSPECTION = "TLG_INSPECTION_ENTRY_M";
    private String _STATUS_EXIST = "000";
    private String _STATUS_NOT_EXIST = "001";
    private String APPROVED_INCOME = "OK";

    private int ACTION_TAKE_PHOTO_B = 1;
    private int ACTION_POPUP_SLIP_NO = 2;
    private int ACTION_POPUP_CHOOSE_REQ_NO = 3;
    private int ACTION_POPUP_DETAIL_REQ_NO = 4;
    private int ACTION_POPUP_COPPY_SLIP_NO = 5;
    private int ACTION_POPUP_SLIP_NO_INCOME = 6;
    String  dataMaster[] = new String[0];
    String  dataDetail[] = new String[0];

    //Handle One Thread To WebService.
    boolean flagUpdateImage = false;
    boolean flagGetData = false;
    boolean flagInserDetail = false;
    boolean flagloadSlipNo = true;
    boolean flagloadReqNo = true;
    boolean flagloadSlipNoIncome = true;
    boolean flagApproveIncome = false;
    String REQ_NO = "";
    String ITEM_PK ="";
    private ImageView mImageView;
    private String mCurrentPhotoPath;
    QCManagementAsyncTask QC_AsyncTask = null;
    QCManagementAsyncTask Detail_AsyncTask = null;
    String _pkSlipNo = "", _nameSlipNo = "",_pkSlipNoIncome = "", _nameSlipNoIncome = "";



    //Next Button on EditText
    int idButton = 1;
    private String _TAG_CAV = "Cav";
    private String TAG_NUMBER = "_NUMBER";
    private String TAG_TEXT = "_TEXT";
    private String _TAG_REMARK= "Remark";
    private String _TAG_RESULT= "Result";
    private final int MAX_CAVITY_COLOUMN = 10;

    //Time No
    String Tag_Time_No = "TIME_NO_";
    //Control Cancel or Approve SlipNo
    //Cancel: can update detail.
    //Approve: cann't update detail.

    boolean FLAG_INSERT_DETAIL = false;
    boolean FLAG_HEADER_CAVITY = true;
    boolean FLAG_POPUP_DETAIL = true;
    //Checkbox
    TableRow.LayoutParams lpCheckBox = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
    CheckBox checkBox = null;

    //width header
    int HD_SEQ,HD_INSPECTIONTYPE, HD_INSPECTIONITEM, HD_INSPECTIONCONTENT, HD_SAMPLING, HD_JUDGEMENT, HD_REMARK, HD_SA, HD_RESULT;

    boolean flagTab_A = false;
    int CAVITY_WIDTH = 200;
    int rowDetail = 0;
    //infor QC
    String SHIFT_A, SHIFT_B, SHIFT_C;
    int JUDGMENT = 0;
    int indexCavityInDataBase;

    //DETAIL REQ NO
    private String  INSPECTOR_NAME="",
            CHECKED_NAME = "",
            CUSTOMER_NAME = "",
            APPROVE_NAME = "",
            LINE_GROUP_NAME = "",
            LINE_NAME = "",
            LOT_NO = "",
            REMARK = "",
            LOT_QTY="",
            INCOME_DT="",
            LOT_NO_SUPPLIER="";

    private int INSPECTOR_PK = 0,
            CHECKED_PK = 0,
            CUSTOMER_PK = 0,
            APPROVE_PK = 0,
            LINE_GROUP_PK = 0,
            LINE_PK = 0,
            STATUS = 0,
            INSPECTION_TYPE = 0, //--1:Final  ---2:Process  --3:Incoming  --4: Patrol
            TIME_NO = 0;

    private static int FINAL_INSPECTION = 1;
    private static int PROCESS_INSPECTION = 2;
    private static int INCOME_INSPECTION = 3;
    private static int PATROL_INSPECTION = 4;

    List<String> CAVITY_LIST_PK = new ArrayList<String>();

    List<String> CAVITY_LIST_NAME = new ArrayList<String>();

    private TextView tvDate_Time;
    private TextView tvDate;
    private TextView tvTime,tvReqNo,tvSlipNo,tvSlipNoIncome,tvDetail;
    private  Button btnQC,btnCapture,btnShowCapture,btnDelAll,btnCancel,btnApprove;
    private static DatePickerFragment pickerDate;
    String strDate = "";
    String strTime = "";
    LinkedHashMap lstInspectionTime = new LinkedHashMap<String, ArrayList<String>>();

    //endregion

    View rootView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_qc_management, container, false);

       // this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        gwMActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

       // SetActivity(this);
        gwMActivity.AddEvenKeyboardHiddenWhenClickOutSide(rootView.findViewById(R.id.layout_QC_MANAGEMENT));

        mImageView = new ImageView(gwMActivity);
        user = gwMActivity.getIntent().getStringExtra("user");
        user_id = gwMActivity.getIntent().getStringExtra("user_id");
        pk_user = gwMActivity.getIntent().getStringExtra("user_pk");

        myBC = (EditText) rootView.findViewById(R.id.etReqNo);
        etSlipNo = (TextView) rootView.findViewById(R.id.etSlipNo);


        _btnQC = (Button) rootView.findViewById(R.id.btnQC);
        _btnDelete = (Button) rootView.findViewById(R.id.btnDelAll);
        tvDate_Time = (TextView)rootView.findViewById(R.id.tvDate_Time);
        tvDate = (TextView)rootView.findViewById(R.id.tvDate_);
        tvTime = (TextView)rootView.findViewById(R.id.tvTime);

        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();

        tvReqNo =(TextView)rootView.findViewById(R.id.tvReqNo);
        tvReqNo.setOnClickListener(this);
        tvSlipNo =(TextView)rootView.findViewById(R.id.tvSlipNo);
        tvSlipNo.setOnClickListener(this);
        tvSlipNoIncome=(TextView)rootView.findViewById(R.id.tvSlipNoIncome);
        tvSlipNoIncome.setOnClickListener(this);
        tvDetail=(TextView)rootView.findViewById(R.id.tvDetail);
        tvDetail.setOnClickListener(this);
        btnQC=(Button) rootView.findViewById(R.id.btnQC);
        btnQC.setOnClickListener(this);
        btnCapture=(Button) rootView.findViewById(R.id.btnCapture);
        btnCapture.setOnClickListener(this);
        btnShowCapture=(Button) rootView.findViewById(R.id.btnShowCapture);
        btnShowCapture.setOnClickListener(this);
        btnDelAll=(Button) rootView.findViewById(R.id.btnDelAll);
        btnDelAll.setOnClickListener(this);
        btnCancel=(Button) rootView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
        btnApprove=(Button) rootView.findViewById(R.id.btnApprove);
        btnApprove.setOnClickListener(this);


        myBC.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        OnScanBC();
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // this is for backspace
                    myBC.clearFocus();
                    Thread.interrupted();
                  //  finish();
                    System.exit(0);
                }
                return false;
            }
        });

        //Set layout checkBox
        lpCheckBox.setMargins(1, 1, 1, 1);

        //Set Table
        setScrollViewAndHorizontalScrollViewTag();


        int Measuredheight = 0, Measuredwidth = 0;
        Point size = new Point();
        WindowManager w = gwMActivity.getWindowManager();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            w.getDefaultDisplay().getSize(size);
            Measuredheight = size.y;
            Measuredwidth = size.x;
            //Tab A -1024 x 768

            //Tab S -2560 x 1600

            if (Measuredwidth <= 1024) {
                flagTab_A = true;
            } else {
                flagTab_A = false;
            }

        }

        InitItem(true, true);

        ((EditText)rootView.findViewById(R.id.etReqNo)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String txt = ((EditText)rootView.findViewById(R.id.etReqNo)).getText().toString();
                if(txt.contains("\n")){
                    OnScanBC();
                }
            }
        });
   return rootView;
    }

    @Override
    public void onDestroy() {
        REQ_NO = "";
        super.onDestroy();
    }

    /**
     * Send Image To Server Side
     */
    public void UpLoadImage(){

        if(!flagUpdateImage
                || imageBitmap == null
                || imageBitmap.getByteCount() == 0) return;

        final ProgressDialog progress = new ProgressDialog(gwMActivity);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                progress.setMessage("Uploading  image into server, please wait...");
                progress.setCancelable(false);
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setProgress(0);
                progress.show();

            }

            @Override
            protected Void doInBackground(Void... params) {

                //region UPLOAD BIG IMAGE
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.WEBP, 50, out);
                int bytes = imageBitmap.getByteCount();

                byte[] byteArrayImage = new byte[out.size()];
                byteArrayImage = out.toByteArray();

                String kq = HandlerWebService.UploadImageArg(
                        "", _Table_INSPECTION, _TLG_INSPECTION_ENTRY_M_PK, _TLG_INSPECTION_ENTRY_M_PK
                        , "test.jpg", String.valueOf(bytes), ""
                        , byteArrayImage
                        , user);

                Log.i("UpLoadImage(): ", kq);
                return null;
            }

            @Override
            protected void onPostExecute(Void args) {
                super.onPreExecute();
                progress.dismiss();
                this.cancel(true);
                _TLG_INSPECTION_ENTRY_M_PK = "";
                flagUpdateImage = false;

            }
        }.execute();
    }

    public void coppyDetailInpection_ByReqNoSlipNo(String ReqNo, final String pkSlipNo){

        final String para = ReqNo + "*" + pkSlipNo +"*" +"1";
        try{
            progress = new ProgressDialog(gwMActivity);
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    progress.setMessage("Geting  Data From Server, Please Wait...");
                    progress.setCancelable(false);//not close when touch
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setProgress(0);
                    progress.show();

                    flagGetData = false;
                }

                @Override
                protected Void doInBackground(Void... params) {
                    flagGetData = false;
                    String resultMaster[][] = HandlerWebService.GetDataTableArg("drivtldrlgtl0001_s_13", pkSlipNo);
                    String resultDetail[][] = HandlerWebService.GetDataTableArg("drivtldrlgtl0001_s_06", para);
                    Log.e("QCByReqNoSlipNo", "drivtldrlgtl0001_s_13  " + pkSlipNo);
                    Log.e("QCByReqNoSlipNo", "drivtldrlgtl0001_s_06  " + para);
                    if(resultDetail != null && resultDetail.length > 0){

                        if(resultMaster != null && resultMaster.length>0){
                            INSPECTOR_PK	= Integer.valueOf(resultMaster[0][1]);
                            CHECKED_PK 		= Integer.valueOf(resultMaster[0][2]);
                            APPROVE_PK 		= Integer.valueOf(resultMaster[0][3]);
                            LINE_GROUP_PK 	= Integer.valueOf(resultMaster[0][4]);
                            LINE_PK 		= Integer.valueOf(resultMaster[0][5]);
                            LOT_NO 			= resultMaster[0][6];
                            /*Business Change

                            SHIFT_A 		= resultMaster[0][7];
                            SHIFT_B 		= resultMaster[0][8];
                            SHIFT_C 		= resultMaster[0][9];*/
                            SHIFT_A 		= "F";
                            SHIFT_B 		= "F";
                            SHIFT_C 		= "F";
                            JUDGMENT 		= 0;
                            INSPECTOR_NAME 	= resultMaster[0][11];
                            CHECKED_NAME	= resultMaster[0][12];
                            APPROVE_NAME 	= resultMaster[0][13];
                            LINE_GROUP_NAME	= resultMaster[0][14];
                            LINE_NAME 		= resultMaster[0][15];
                            STATUS 			= 0;
                        }

                        SET_PERMISSION_BY_STATUS(STATUS);
                        ShowDataBySlipNoCoppy(resultDetail);
                        FLAG_HEADER_CAVITY=true;
                        ShowHeader(ITEM_PK);
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void args) {
                    super.onPreExecute();
                    progress.dismiss();
                    this.cancel(true);
                    flagGetData = true;
                }
            }.execute();

        }catch (Exception e){
            System.out.println("Exeption: " + e.getMessage());
        }
    }

    private void CheckShoworHiddenBtnGetData(){
        //Show button get Data
        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button btnGetData = null;
                if (INSPECTION_TYPE == PATROL_INSPECTION
                        && ((EditText) rootView.findViewById(R.id.etSlipNo)).getText().toString().length() > 0
                        ) {
                    btnGetData = (Button) rootView.findViewById(R.id.btn_GetData);
                    btnGetData.setVisibility(View.VISIBLE);
                } else {
                    btnGetData = (Button) rootView.findViewById(R.id.btn_GetData);
                    btnGetData.setVisibility(View.GONE);
                }

                ((TextView) rootView.findViewById(R.id.tvDate_Time)).setText("");
                lstInspectionTime.clear();
                hashMapSlipNoByInsTime.clear();
            }
        });
    }

    public void onGetDataByInsTime(View view){
        //find id checkbox
        boolean flag  = false;
        LinearLayout layoutTimeNo = (LinearLayout) rootView.findViewById(R.id.layoutTimeNo);
        for (int j = 0; j < layoutTimeNo.getChildCount(); j++) {

            View v = layoutTimeNo.getChildAt(j);
            if(v instanceof CheckBox){
                CheckBox cbTeam = new CheckBox(gwMActivity.getApplication());
                cbTeam 			= (CheckBox)layoutTimeNo.getChildAt(j);
                if(cbTeam.isChecked()){
                    flag = true;
                    String insTimePk =  cbTeam.getTag().toString().split(Tag_Time_No)[1];
                    getDetailInpection_ByReqNoSlipNo(REQ_NO, _TLG_INSPECTION_ENTRY_M_PK, Integer.valueOf(insTimePk));
                    return;
                }
            }
        }

        if(!flag){
            gwMActivity.alertToastShort("Please Choose Inspec Time To Get Data!");
        }
    }

    public void getDetailInpection_ByReqNoSlipNo(String ReqNo, final String pkSlipNo, final int pkInspectTime){
        final String para = ReqNo + "*" + pkSlipNo +"*"+pkInspectTime;
        try{
            progress = new ProgressDialog(gwMActivity);
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    progress.setMessage("Geting  Data From Server, Please Wait...");
                    progress.setCancelable(false);//not close when touch
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setProgress(0);
                    progress.show();

                    flagGetData = false;
                }

                @Override
                protected Void doInBackground(Void... params) {
                    flagGetData = false;
                    String resultMaster[][] = HandlerWebService.GetDataTableArg("drivtldrlgtl0001_s_13", pkSlipNo);
                    String resultDetail[][] = HandlerWebService.GetDataTableArg("drivtldrlgtl0001_s_06", para);
                    Log.e("getByReqNoSlipNo  ","drivtldrlgtl0001_s_13"+para);
                    Log.e("getDetail_ByReqNoSlipNo","drivtldrlgtl0001_s_06:"+para);
                    if(resultDetail != null && resultDetail.length > 0){

                        if(resultMaster != null && resultMaster.length>0){
                            INSPECTOR_PK 	= Integer.valueOf(resultMaster[0][1]);
                            CHECKED_PK 		= Integer.valueOf(resultMaster[0][2]);
                            APPROVE_PK 		= Integer.valueOf(resultMaster[0][3]);
                            LINE_GROUP_PK 	= Integer.valueOf(resultMaster[0][4]);
                            LINE_PK 		= Integer.valueOf(resultMaster[0][5]);
                            LOT_NO 			= resultMaster[0][6];
                            SHIFT_A 		= resultMaster[0][7];
                            SHIFT_B 		= resultMaster[0][8];
                            SHIFT_C 		= resultMaster[0][9];
                            JUDGMENT 		= Integer.valueOf(resultMaster[0][10]);
                            INSPECTOR_NAME 	= resultMaster[0][11];
                            CHECKED_NAME	= resultMaster[0][12];
                            APPROVE_NAME 	= resultMaster[0][13];
                            LINE_GROUP_NAME	= resultMaster[0][14];
                            LINE_NAME 		= resultMaster[0][15];
                            STATUS 			= Integer.valueOf(resultMaster[0][16]);
                            REMARK 			= resultMaster[0][17];
                            INCOME_DT 			= NullOrString(resultMaster[0][18], "-");

                            LOT_QTY			= resultMaster[0][19];
                            _nameSlipNoIncome = resultMaster[0][20];
                            LOT_NO_SUPPLIER = NullOrString(resultMaster[0][21], "-");

                            Log.e("nameSlipNoIncome",_nameSlipNoIncome);
                        }

                        SET_PERMISSION_BY_STATUS(STATUS);

                        ShowDataBySlipNo(resultDetail, JUDGMENT, pkInspectTime);
                        FLAG_HEADER_CAVITY=false;
                        ShowHeader(ITEM_PK);
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void args) {
                    super.onPreExecute();
                    progress.dismiss();
                    this.cancel(true);
                    flagGetData = true;
                }
            }.execute();

        }catch (Exception e){
            System.out.println("Exeption: " + e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        flagloadSlipNo 	= true;
        flagloadReqNo 	= true;
        flagloadSlipNoIncome=true;
        FLAG_POPUP_DETAIL=true;

        if (requestCode == ACTION_TAKE_PHOTO_B
                && resultCode == gwActivity.RESULT_OK) {

            //region ACTION_TAKE_PHOTO_B
            //Test QC Button
            setPic();
            galleryAddPic();
            mCurrentPhotoPath 	= null;
            flagUpdateImage 	= true;
            mImageView 			= new ImageView(gwMActivity);
            mImageView.setImageBitmap(imageBitmap);
        //endregion

        } else if (requestCode == ACTION_POPUP_SLIP_NO
                && resultCode == gwActivity.RESULT_OK) {

            //region ACTION_POPUP_SLIP_NO
            Bundle bundle 	= data.getBundleExtra("ResultSlipNo");
            _pkSlipNo 		= bundle.getString("PK_SLIP_NO");
            _nameSlipNo 	= bundle.getString("NAME_SLIP_NO");

            ((EditText)rootView.findViewById(R.id.etSlipNo)).setText(_nameSlipNo);
            _TLG_INSPECTION_ENTRY_M_PK = _pkSlipNo;

            //Clear Data Table C&D
            TableLayout tbD = (TableLayout) rootView.findViewById(R.id.tbD);
            tbD.removeAllViews();

            TableLayout tbC = (TableLayout) rootView.findViewById(R.id.tbC);
            tbC.removeAllViews();

            //Get Data By SlipNo
            getDetailInpection_ByReqNoSlipNo(REQ_NO, _pkSlipNo, 1);


            Thread thread 	= new Thread(new Runnable()
            {
                @Override
                public void run()
                {
                    try{
                        imageBitmap 			= null;
                        String result 			= HandlerWebService.GetImage("TLG_INSPECTION_ENTRY_M", _pkSlipNo);

                        byte[] decodedString 	= android.util.Base64.decode(result, android.util.Base64.DEFAULT);
                        imageBitmap 			= BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        mImageView.setImageBitmap(imageBitmap);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            });

            thread.start();

            CheckShoworHiddenBtnGetData();
            //endregion

        }else if (requestCode == ACTION_POPUP_SLIP_NO
                && resultCode == ACTION_POPUP_COPPY_SLIP_NO) {

            //region ACTION_POPUP_COPPY_SLIP_NO
            Bundle bundle 	= data.getBundleExtra("ResultSlipNo");
            _pkSlipNo 		= bundle.getString("PK_SLIP_NO");
            _nameSlipNo 	= bundle.getString("NAME_SLIP_NO");
            imageBitmap 	= null;

            ((EditText)rootView.findViewById(R.id.etSlipNo)).setText("");
            ((EditText)rootView.findViewById(R.id.etJudgment)).setText("");
            coppyDetailInpection_ByReqNoSlipNo(REQ_NO, _pkSlipNo);

            //endregion
        }else  if (requestCode == ACTION_POPUP_CHOOSE_REQ_NO
                && resultCode ==  gwActivity.RESULT_OK) {

            //region ACTION_POPUP_CHOOSE_REQ_NO
            Bundle bundle 	= data.getBundleExtra("DATA_REQUEST_NO");
            REQ_NO 			= bundle.getString("REQUEST_NO");
            flagGetData 	= true;
            getDataByReqNo(REQ_NO, true, true);
            CheckShoworHiddenBtnGetData();
            //endregion

        }else if (requestCode == ACTION_POPUP_SLIP_NO_INCOME && resultCode ==  gwActivity.RESULT_OK ) {

            //region ACTION_POPUP_SLIP_NO_INCOME
            Bundle bundle 	= data.getBundleExtra("ResultSlipNo");
            _pkSlipNoIncome 		= bundle.getString("PK_SLIP_NO_INCOME");
            _nameSlipNoIncome 	= bundle.getString("NAME_SLIP_NO");

            INCOME_DT 	= bundle.getString("DATE");
            LOT_QTY 	= bundle.getString("QTY_INCOME");
            LOT_NO 	= bundle.getString("LOT_NO");
            LOT_NO_SUPPLIER 	= bundle.getString("LOT_NO_SUPPLIER");

            //imageBitmap 	= null;
            /*
            bundle.putString("DATE", date);
            bundle.putString("QTY_INCOME", qty);
            bundle.putString("LOT_NO", lotNo);
            bundle.putString("LOT_NO_SUPPLIER", lotNoSupplier);
             */

            ((EditText)rootView.findViewById(R.id.etSlipNoIncome)).setText(_nameSlipNoIncome);
            //((EditText)rootView.findViewById(R.id.etJudgment)).setText("");
            //coppyDetailInpection_ByReqNoSlipNo(REQ_NO, _pkSlipNo);

            //endregion
        }
        else if(requestCode == ACTION_POPUP_DETAIL_REQ_NO
                && resultCode ==  gwActivity.RESULT_OK){
             FLAG_POPUP_DETAIL=true;
            //region ACTION_POPUP_DETAIL_REQ_NO
            Bundle bundle = data.getBundleExtra("RESULT_DETAIL");
            if(bundle!=null && STATUS != 3){
                flagGetData = true;
                INSPECTOR_PK    = bundle.getInt("INSPECTOR_PK");
                CHECKED_PK      = bundle.getInt("CHECKED_PK");
                APPROVE_PK      = bundle.getInt("APPROVE_PK");
                LINE_GROUP_PK   = bundle.getInt("LINE_GROUP_PK");
                LINE_PK         = bundle.getInt("LINE_PK");

                LOT_NO          = bundle.getString("LOT_NO");

                SHIFT_A         = bundle.getString("SHIFT_A");
                SHIFT_B         = bundle.getString("SHIFT_B");
                SHIFT_C         = bundle.getString("SHIFT_C");
                LOT_QTY         = bundle.getString("LOT_QTY");
                INCOME_DT       = bundle.getString("INCOME_DT");
                if(bundle.getStringArrayList("CAVITY_LIST_PK") !=null
                        && bundle.getStringArrayList("CAVITY_LIST_PK").size() > 0){
                    CAVITY_LIST_PK = bundle.getStringArrayList("CAVITY_LIST_PK");
                    CAVITY_LIST_NAME = bundle.getStringArrayList("CAVITY_LIST_NAME");
                    FLAG_HEADER_CAVITY=false;
                    getDataByReqNo(REQ_NO, true, false);
                }else{
                    getDataByReqNo(REQ_NO, false, false);
                }

                Log.e("START GET INFOR DETAIL:", "------------------------------------------------------------");
                Log.i("INSPECTOR_PK", String.valueOf(INSPECTOR_PK));
                Log.i("CHECKED_PK", String.valueOf(CHECKED_PK));
                Log.i("APPROVE_PK", String.valueOf(APPROVE_PK));
                Log.i("LINE_GROUP_PK", String.valueOf(LINE_GROUP_PK));
                Log.i("LINE_PK", String.valueOf(LINE_PK));
                Log.i("LOT_NO", String.valueOf(LOT_NO));

                Log.i("SHIFT_A", String.valueOf(SHIFT_A));
                Log.i("SHIFT_B", String.valueOf(SHIFT_B));
                Log.i("SHIFT_C", String.valueOf(SHIFT_C));

                for(int i = 0; i< CAVITY_LIST_PK.size(); i++){
                    Log.i("CAVITY_LIST_PK " + i, " : " + String.valueOf(CAVITY_LIST_PK.get(i)));
                }
                Log.i("END- GET INFOR DETAIL:", "------------------------------------------------------------");

            }
            //endregion
        }
    }

    private void loadPhoto(ImageView imageView) {

        ImageView tempImageView = imageView;


        AlertDialog.Builder imageDialog = new AlertDialog.Builder(gwMActivity);
        LayoutInflater inflater 		= (LayoutInflater) gwMActivity.getSystemService( gwActivity.LAYOUT_INFLATER_SERVICE);

        View layout 					= inflater.inflate(R.layout.custom_fullimage_dialog,
                (ViewGroup) rootView.findViewById(R.id.layout_root));
        ImageView image 				= (ImageView) layout.findViewById(R.id.imageView1);
        image.setImageDrawable(tempImageView.getDrawable());
        imageDialog.setView(layout);
        imageDialog.setPositiveButton(getResources().getString(R.string.btn_Upload), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });


        imageDialog.create();
        imageDialog.show();
    }

    //endregion

    //region INIT ITEM

    /**
     *
     * @param tf
     * @param text
     * @param widthInPercentOfScreenWidth
     * @param fixedHeightInPixels
     * @param textAlign
     * @param CharOrNumber 1 Test; 2 Number; 3 Date
     * @return
     */
    public EditText makeTableColWithEditText(Boolean tf, String text, int widthInPercentOfScreenWidth,
                                             int fixedHeightInPixels, int textAlign, int CharOrNumber, boolean setId, String tag) {

        final int[] _idEditText = new int[1];
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);

        if(widthInPercentOfScreenWidth >0){//If It was hidden, It will not set margin.
            if(flagTab_A){
                params.setMargins(1, 1, 1, 1);
            }else{
                params.setMargins(3, 1, 1, 1);
            }

        }

        final EditText bsTextView = new EditText(gwMActivity);

        bsTextView.setText(text);
        bsTextView.setTextColor(Color.BLACK);

        bsTextView.setPadding(1, 0, 0, 1);
        bsTextView.setTextSize(16);
        bsTextView.setBackgroundColor(-1);

        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth);
        bsTextView.setHeight(fixedHeightInPixels);

        bsTextView.setMaxLines(1);
        bsTextView.setEnabled(tf);

        //Set text align
        if (textAlign == -1) {
            bsTextView.setGravity(Gravity.LEFT);
            bsTextView.setPadding(10, 5, 0, 5);
        } else if (textAlign == 0) {
            bsTextView.setGravity(Gravity.CENTER);
            bsTextView.setPadding(0, 5, 0, 5);
        } else {
            bsTextView.setGravity(Gravity.RIGHT);
            bsTextView.setPadding(0, 5, 10, 5);
        }

        //Set input keyboard
        if(CharOrNumber == 3){
            bsTextView.setFocusable(false);
            final Calendar myCalendar = Calendar.getInstance();
            final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {

                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    //updateLabel();
                    String myFormat = "yy/MM/dd"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                    bsTextView.setText(sdf.format(myCalendar.getTime()));
                }
            };

            bsTextView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    new DatePickerDialog(gwMActivity, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
        }else if (CharOrNumber 	== 1) {
            bsTextView.setInputType(InputType.TYPE_CLASS_TEXT);
        } else if (CharOrNumber == 2) {
            bsTextView.setInputType(InputType.TYPE_CLASS_PHONE);
        }

        if(setId) {
            setId(bsTextView);
            _idEditText[0] = bsTextView.getId();

            //Validate Input

            bsTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    int index 				= bsTextView.getId();
                    boolean flagNotGood 	= false;
                    boolean flagSetResult 	= false;
                    double _value 			= 0;
                    int _end 				= idButton;

                    //Show Result by MIN_RESULT, MAX_RESULT
                    if(bsTextView.getText().length() >= 0){// Tag is Result or Remark

                        //case number
                        if( bsTextView.getTag().toString().contains(TAG_NUMBER)
                                && CNumber.checkNumberFloat(bsTextView.getText().toString())
                                || bsTextView.getText().length() == 0){ //=0 when clear data into cell

                            bsTextView.setBackgroundColor(Color.WHITE);

                            for(int _start = index; _start < _end; _start++){

                                EditText t 		= 	null;
                                String strTag	=	"";
                                Object o 		= 	rootView.findViewById(_start);

                                if(o instanceof EditText){
                                    strTag = ((EditText)rootView.findViewById(_start)).getTag().toString();
                                }

                                //Check EditText when View is Checkbox
                                if(o instanceof EditText
                                        && strTag.toLowerCase().contains(_TAG_RESULT.toLowerCase())){

                                    t = (EditText)rootView.findViewById(_start);

                                    String strMin 	=  ((EditText)rootView.findViewById(t.getId()+2)).getText().toString();
                                    String strMax 	=  ((EditText)rootView.findViewById(t.getId()+3)).getText().toString();

                                    if(strMin.length()>0 && strMax.length()>0
                                            && !strMin.equals("-1") && !strMax.equals("-1")){
                                        double min 	=  Double.valueOf(((EditText)rootView.findViewById(t.getId()+2)).getText().toString());
                                        double max 	=  Double.valueOf(((EditText)rootView.findViewById(t.getId()+3)).getText().toString());

                                        int _end2 	= t.getId();
                                        int _start_22 = _end2 - CAVITY_LIST_PK.size();
                                        boolean checkDataRow = false;

                                        for(int _start2 = _start_22; _start2 < _end2; _start2++){

                                            Object teamp = rootView.findViewById(_start2);
                                            if(teamp instanceof EditText){
                                                EditText t2 =  (EditText)rootView.findViewById(_start2);
                                                String strTagT2 = t2.getTag().toString();

                                                if(t2!= null && t2.getText().toString().length() > 0){
                                                    checkDataRow = true;
                                                    flagSetResult = true;

                                                    if(CNumber.checkNumberFloat(t2.getText().toString())){
                                                        _value = Double.valueOf(t2.getText().toString());

                                                        if (_value >= min && _value <= max) {

                                                        } else {
                                                            flagNotGood = true;
                                                        }
                                                    }else{
                                                        gwMActivity.alertToastShort("Please Input Number");
                                                        t2.setBackgroundColor(Color.parseColor("#ff5151"));
                                                        flagNotGood = true;
                                                    }
                                                }
                                            }

                                        }

                                        if(flagSetResult && flagNotGood){
                                            t.setText(_NOT_GOOD);
                                        }else if(flagSetResult && !flagNotGood){
                                            t.setText(_OK);
                                            CheckBox cb = (CheckBox) rootView.findViewById(t.getId()+1);
                                            cb.setChecked(false);
                                        }

                                        if(!checkDataRow){//Not data in row
                                            t.setText("");
                                            CheckBox cb = (CheckBox) rootView.findViewById(t.getId()+1);
                                            cb.setChecked(false);
                                        }
                                    }

                                    break;
                                }

                            }//end for
                        }else if(!bsTextView.getTag().toString().toLowerCase().contains(_TAG_REMARK.toLowerCase())
                                && !bsTextView.getTag().toString().toLowerCase().contains(TAG_TEXT.toLowerCase())){
                            if(bsTextView.getText().toString().length() > 0){
                                gwMActivity.alertToastShort("Please Input Number");
                                bsTextView.setBackgroundColor(Color.parseColor("#ff5151"));

                                //Clear Result at the row
                                for (int _start = index; _start < _end; _start++) {

                                    EditText t = new EditText( gwMActivity.getApplication());
                                    t = (EditText) rootView.findViewById(_start);
                                    String strTag = t.getTag().toString();
                                    if (strTag.toLowerCase().contains(_TAG_RESULT.toLowerCase())) {
                                        t.setText("");
                                        break;
                                    }
                                }
                            }
                        }else if(bsTextView.getTag().toString().contains(TAG_TEXT)){
                            String text = bsTextView.getText().toString();

                            for(int _start = index; _start < _end; _start++){

                                EditText t = null;
                                String strTag="";
                                Object o = rootView.findViewById(_start);

                                if(o instanceof EditText){
                                    strTag = ((EditText)rootView.findViewById(_start)).getTag().toString();
                                }

                                //Check EditText when View is Checkbox
                                if(o instanceof EditText
                                        && strTag.toLowerCase().contains(_TAG_RESULT.toLowerCase())){

                                    t = (EditText)rootView.findViewById(_start);
                                    int _end2 = t.getId();
                                    // 21/6
                                    //    int _start_22 = _end2 - _QTY_COLUMN_CAV;
                                    int _start_22 = _end2 - CAVITY_LIST_PK.size();
                                    boolean checkDataRow = false;

                                    for(int _start2 = _start_22; _start2 < _end2; _start2++){

                                        Object teamp = rootView.findViewById(_start2);
                                        if(teamp instanceof EditText){
                                            EditText t2 =  (EditText)rootView.findViewById(_start2);
                                            String strTagT2 = t2.getTag().toString();

                                            if(t2!= null && t2.getText().toString().length() > 0){
                                                checkDataRow = true;
                                                flagSetResult = true;
                                                String _valueText = t2.getText().toString();
                                                if (_valueText.length()>0 && !_valueText.trim().toLowerCase().equals("ok")) {
                                                    flagNotGood = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if(flagSetResult && flagNotGood){
                                        t.setText(_NOT_GOOD);
                                    }else if(flagSetResult && !flagNotGood){
                                        t.setText(_OK);
                                        CheckBox cb = (CheckBox) rootView.findViewById(t.getId()+1);
                                        cb.setChecked(false);
                                    }

                                    if(!checkDataRow){//Not data in row
                                        t.setText("");
                                        CheckBox cb = (CheckBox) rootView.findViewById(t.getId()+1);
                                        cb.setChecked(false);
                                    }

                                }

                            }
                        }
                    }
                }
            });

        }else if(tag.length()>0){
            setId(bsTextView);
        }

        bsTextView.setTag(tag);
        return bsTextView;
    }

    //endregion

    //region ACTION
    public void onClickViewStt(View view) throws InterruptedException, ExecutionException {
        String str = "";
        str = _STATUS_EXIST + ": \t\t\t The SEQ NO Is Exist\n";
        str += "________________________________________________________\n\n";
        str += _STATUS_NOT_EXIST + ": \t\t\t The SEQ NO Does Not Exist\n";

        //Dialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle("List Status ...");
        // Setting Dialog Message
        alertDialog.setMessage(str);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_diagram);

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void onCancel(View view) {
        progress = new ProgressDialog(gwMActivity);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                progress.setMessage("Processing, Please Wait...");
                progress.setCancelable(false);//not close when touch
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setProgress(0);
                progress.show();

                flagGetData = false;
            }

            @Override
            protected Void doInBackground(Void... params) {
                String para         = _pkSlipNo+"*4";
                String result[][]   = HandlerWebService.GetDataTableArg("drivtldrlgtl0001_u_14", para);
                Log.i("QC_Mana_onCancel: ", "drivtldrlgtl0001_u_14" + para);

                if(result!= null && result.length > 0){
                    STATUS = 4;
                    ProcessUIByStatus(STATUS);

                }else{
                    gwMActivity.alertToastShort("Approve Don't Complete");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void args) {
                super.onPreExecute();
                progress.dismiss();
                this.cancel(true);
                flagGetData = true;
            }
        }.execute();


    }

    public void onApprove(View view) {
        progress = new ProgressDialog(gwMActivity);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                progress.setMessage("Processing, Please Wait...");
                progress.setCancelable(false);//not close when touch
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setProgress(0);
                progress.show();

                flagGetData = false;
            }

            @Override
            protected Void doInBackground(Void... params) {
                String para = _pkSlipNo+"*3";
                String result[][] = HandlerWebService.GetDataTableArg("drivtldrlgtl0001_u_14", para);
                Log.i("QC_Mana_onApprove: ", "drivtldrlgtl0001_u_14  " + para);
                if(result!= null && result.length > 0){
                    STATUS = 3;
                    ProcessUIByStatus(STATUS);

                }else{
                    gwMActivity.alertToastShort("Approve Don't Complete");
                    Log.i("QC_Mana_onApprove: ", " Not complete");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void args) {
                super.onPreExecute();
                progress.dismiss();
                this.cancel(true);
                flagGetData = true;
            }
        }.execute();
    }

    private void ProcessUIByStatus(final int status){


        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(status == 3){
                    gwMActivity.alertToastShort("Approve Complete");
                    rootView.findViewById(R.id.btnApprove).setVisibility(View.GONE);
                    rootView.findViewById(R.id.btnQC).setVisibility(View.GONE);
                    rootView.findViewById(R.id.btnCancel).setVisibility(View.VISIBLE);
                }else if(status == 4){
                    gwMActivity.alertToastShort("Cancel Complete");
                    rootView.findViewById(R.id.btnCancel).setVisibility(View.GONE);
                    rootView.findViewById(R.id.btnApprove).setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.btnQC).setVisibility(View.VISIBLE);
                }

            }
        });
    }


    public void onDelAll(View view) {

        if (!checkRecordGridView(R.id.tbD)
                &&((EditText)rootView.findViewById(R.id.etReqNo)).getText().toString().length() == 0){
            return;
        }

        String title    = "Confirm Delete..";
        String mess     = "Are you sure you want to delete all";
        alertDialogYN(title, mess, "onDelAll");
    }

    public void onShowCapture(View view){
        if(imageBitmap == null || imageBitmap.getByteCount() == 0) return;
        loadPhoto(mImageView);
    }

    public void onCapture(View view){
        //  dispatchTakePictureIntent();
        dispatchTakePictureIntent_();

    }

    public void InsertOrUpdateMaster(){
        flagInserDetail = false;
        //Check Grid
        TableLayout table   = (TableLayout) rootView.findViewById(R.id.tbD);
        String slipNo       = "";
        slipNo              = ((EditText)rootView.findViewById(R.id.etSlipNo)).getText().toString();

        if(table.getChildCount() == 0) return;

        //STEP 1: INSERT MASTER;
        dataMaster=new String [1];
        String paraMaster = "";
        String remark = ((EditText)rootView.findViewById(R.id.etRemark)).getText().toString();
        paraMaster	+=	user		+ "|" +	REQ_NO			+  "|" +	INSPECTOR_PK 	+	"|" +	CHECKED_PK 		+	"|" +
                        APPROVE_PK 	+ "|" + LINE_GROUP_PK 	+  "|" +	LINE_PK 		+ 	"|"	+	SHIFT_A   		+ 	"|" +
                        SHIFT_B 	+ "|" + SHIFT_C   		+  "|" + 	LOT_NO 			+ 	"|" +	JUDGMENT		+ 	"|" +
                        slipNo		+ "|" + TIME_NO 		+  "|" +    remark          +   "|" +   ITEM_PK         +   "|" +
                        INCOME_DT   + "|" + LOT_QTY         +  "|" +    _pkSlipNoIncome +   "|" +   LOT_NO_SUPPLIER +   "*|*";

        paraMaster 	+= 	"|!"+ "drivtldrlgtl0001_u_03";

        Log.e("InsertOrUpdateMaster: ", paraMaster);

        int j = 0;

        dataMaster[j++] = paraMaster;
        QC_AsyncTask = new QCManagementAsyncTask(gwMActivity);
        if (CNetwork.isInternet(tmpIP, checkIP)) {

            QC_AsyncTask.execute(dataMaster);
            if(QC_AsyncTask.getPk_Master().length() > 0 && QC_AsyncTask.getSlipNo().length() > 0){
                //insert detail
                ((EditText)rootView.findViewById(R.id.etSlipNo)).setText(QC_AsyncTask.getSlipNo().toString());

                flagInserDetail = true;
            }
        } else{
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }

        if(JUDGMENT == 1){
            ((EditText)rootView.findViewById(R.id.etJudgment)).setText("OK");
        }else{
            ((EditText)rootView.findViewById(R.id.etJudgment)).setText("NG");
        }

        Log.e("TLG_INS.._ENTRY_M_PK: ", _TLG_INSPECTION_ENTRY_M_PK);
    }

    public void onDate(View view){

        if (getIDCheckedCheckBox(TIME_NO) == 0){
            gwMActivity.alertToastShort("Pleate Choose Inspection Time!");
            return;
        }
        String txt = ((EditText)rootView.findViewById(R.id.etReqNo)).getText().toString();

        pickerDate = new DatePickerFragment() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {

                Calendar c = Calendar.getInstance();
                c.set(year, month, day);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                String formattedDate = sdf.format(c.getTime());

                strDate = formattedDate;
                List lstDateTime = new ArrayList();
                List lstCurrent =  (List)lstInspectionTime.get(getIDCheckedCheckBox(TIME_NO));

                if(lstCurrent != null && lstCurrent.get(1) != null){
                    strTime = (String) lstCurrent.get(1);
                }

                lstDateTime.add(0, strDate);
                lstDateTime.add(1, strTime);

                lstInspectionTime.put(getIDCheckedCheckBox(TIME_NO),lstDateTime);
                tvDate_Time.setText(strDate + " " + strTime);

            }
        };
        pickerDate.show(getFragmentManager(), "From Date");

    }

    public void onTime(View view){

        if (getIDCheckedCheckBox(TIME_NO) == 0){
            gwMActivity.alertToastShort("Pleate Choose Inspection Time!");
            return;
        }
        String time = tvTime.getText().toString();

        if (time != null && !time.equals("")) {
            StringTokenizer st  = new StringTokenizer(time, ":");
            String timeHour     = st.nextToken();
            String timeMinute   = st.nextToken();

            timePickDialog = new TimePickerDialog(view.getContext(),
                    new TimePickHandler(), Integer.parseInt(timeHour),
                    Integer.parseInt(timeMinute), true);
        } else {
            Calendar calendar   = Calendar.getInstance();
            Date date           = calendar.getTime();
            int hour            = date.getHours();
            int minute          = date.getMinutes();
            timePickDialog      = new TimePickerDialog(view.getContext(),
                    new TimePickHandler(), hour, minute, true);
        }

        timePickDialog.show();

    }

    public void onQCSlipNo(View view) throws InterruptedException {
        //Check datatype input by Numberric
        String warrning = checkValidateInputInGrid();
        if(warrning.length() > 0){
            gwMActivity.alertToastShort(warrning);
            return;
        }

        String title="Confirm QC...";
        String mess="Are you sure you want to QC";
        alertDialogYN(title, mess, "onMakeQC");

    }

    public  void ProcessQCSlipNo(){
        //Check SlipNo: INSERT
        String slipNo = ((EditText)rootView.findViewById(R.id.etSlipNo)).getText().toString().trim();
        if(slipNo.length() == 0){
            InsertOrUpdateMaster();

            while(QC_AsyncTask != null && QC_AsyncTask.getStatus() != AsyncTask.Status.FINISHED){
                //int ms = 1000;
                //Thread.sleep(ms);
                if(QC_AsyncTask.getPk_Master().length() > 0){
                    _TLG_INSPECTION_ENTRY_M_PK = QC_AsyncTask.getPk_Master();
                    slipNo = QC_AsyncTask.getSlipNo();
                    break;
                }
                //System.out.println("Sleep: " + ms);
            }
            //Insert Data With Slipno do not containt Time No (=0)
            if(INSPECTION_TYPE == PATROL_INSPECTION){
                UpdateInsertDetailByInsTime();

            }else{
                InsertDetail_Image(0);
                while(Detail_AsyncTask != null && Detail_AsyncTask.getStatus() != AsyncTask.Status.FINISHED){

                    if(Detail_AsyncTask.getCountRow()==rowDetail && APPROVED_INCOME.equals("OK")){
                        if(INSPECTION_TYPE==INCOME_INSPECTION)
                            onApproveIncome();
                        break;
                    }
                    if(Detail_AsyncTask.getCountRow()==rowDetail && !APPROVED_INCOME.equals("OK")){
                        break;
                    }
                    //System.out.println("Sleep: " + ms);
                }
            }

            if(slipNo != null && slipNo.length() > 0 ){
                //Default get Inspect Time PK =1
                getDetailInpection_ByReqNoSlipNo(REQ_NO, _TLG_INSPECTION_ENTRY_M_PK, 1);
                ((EditText)rootView.findViewById(R.id.etSlipNo)).setText(slipNo);
            }

        }else{//UPDATE

            //UPDATE MASTER
            //// TODO: 7/16/2016  UPDATE MASTER
            UpdateMasterBySlipNo(slipNo);

            //DETAIL
            if(INSPECTION_TYPE == PATROL_INSPECTION){
                UpdateInsertDetailByInsTime();
            }else{
                InsertDetail_Image(0);

            }

            InsertOrUpdateMaster();
        }
    }
    public void onSlipNo(View view)  {
        if(!flagloadSlipNo || REQ_NO.length() ==0) return; //load 1 popup
        else{
            flagloadSlipNo  = false;

            REQ_NO          = ((EditText)rootView.findViewById(R.id.etReqNo)).getText().toString();

            if(REQ_NO.length() == 0) return;

/*            Intent openNewActivity = new Intent(view.getContext(), Popup_QC_Management_SlipNo.class);
            openNewActivity.putExtra("Req_No", REQ_NO);
            startActivityForResult(openNewActivity, ACTION_POPUP_SLIP_NO);
            gwMActivity.overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);*/

            FragmentManager fm = gwMActivity.getSupportFragmentManager();
            DialogFragment dialogFragment = new Popup_QC_Management_SlipNo();
            Bundle args = new Bundle();

            args.putString("Req_No", REQ_NO);
            dialogFragment.setArguments(args);
            dialogFragment.setTargetFragment(this, REQUEST_CODE);
           // gwMActivity.overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            dialogFragment.show(fm.beginTransaction(), "dialog");
        }
    }

    public void onSlipNoIncome(View view)  {
        if(!flagloadSlipNoIncome || REQ_NO.length() ==0) return; //load 1 popup
        else{
            flagloadSlipNoIncome  = false;

            REQ_NO          = ((EditText)rootView.findViewById(R.id.etReqNo)).getText().toString();

            if(REQ_NO.length() == 0) return;
            if(ITEM_PK.equals("0") || ITEM_PK.equals(" ")) return;

            FragmentManager fm = gwMActivity.getSupportFragmentManager();
            DialogFragment dialogFragment = new Popup_QC_Management_SlipNo_Income();
            Bundle args = new Bundle();
            args.putString("ITEM_PK", ITEM_PK);
            dialogFragment.setArguments(args);
            dialogFragment.setTargetFragment(this, ACTION_POPUP_SLIP_NO_INCOME);
            gwMActivity.overridePendingTransition(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom);
            dialogFragment.show(fm.beginTransaction(), "dialog");
        }
    }


    public void onSeqNo(View view)  {
        if(!flagloadReqNo) return; //load 1 popup
        else{
            flagloadReqNo = false;

            FragmentManager fm = gwMActivity.getSupportFragmentManager();
            DialogFragment dialogFragment = new Popup_QC_Management_ReqNo();
             dialogFragment.setTargetFragment(this, ACTION_POPUP_CHOOSE_REQ_NO);
             gwMActivity.overridePendingTransition(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom);
            dialogFragment.show(fm.beginTransaction(), "dialog");
        }
    }

    public void onShowDetail(View view){
        if(REQ_NO.length() == 0) return;

        if(FLAG_POPUP_DETAIL) {
            FLAG_POPUP_DETAIL=false; //Flag load double POPUP

            String slipNo = ((EditText) rootView.findViewById(R.id.etSlipNo)).getText().toString().trim();

            FLAG_INSERT_DETAIL = slipNo.length() > 0 ? false : true;


            FragmentManager fm = gwMActivity.getSupportFragmentManager();
            DialogFragment dialogFragment = new Popup_QC_Management_Detail();
            Bundle bundleDetail = new Bundle();


            bundleDetail.putString("ITEM_PK", ITEM_PK);
            bundleDetail.putInt("STATUS", STATUS);
            bundleDetail.putInt("CUSTOMER_PK", CUSTOMER_PK);
            bundleDetail.putString("CUSTOMER_NAME", CUSTOMER_NAME);

            bundleDetail.putInt("INSPECTOR_PK", INSPECTOR_PK);
            bundleDetail.putString("INSPECTOR_NAME", INSPECTOR_NAME);

            bundleDetail.putInt("CHECKED_PK", CHECKED_PK);
            bundleDetail.putString("CHECKED_NAME", CHECKED_NAME);

            bundleDetail.putInt("APPROVE_PK", APPROVE_PK);
            bundleDetail.putString("APPROVE_NAME", APPROVE_NAME);

            bundleDetail.putInt("LINE_GROUP_PK", LINE_GROUP_PK);
            bundleDetail.putString("LINE_GROUP_NAME", LINE_GROUP_NAME);

            bundleDetail.putInt("LINE_PK", LINE_PK);
            bundleDetail.putString("LINE_NAME", LINE_NAME);

            bundleDetail.putString("LOT_NO", LOT_NO);

            bundleDetail.putString("SHIFT_A", SHIFT_A);
            bundleDetail.putString("SHIFT_B", SHIFT_B);
            bundleDetail.putString("SHIFT_C", SHIFT_C);
            bundleDetail.putString("INCOME_DT", INCOME_DT);
            bundleDetail.putString("LOT_QTY", LOT_QTY);
            bundleDetail.putString("LOT_NO_SUPPLIER",LOT_NO_SUPPLIER);
            dialogFragment.setArguments(bundleDetail);
            dialogFragment.setTargetFragment(this, ACTION_POPUP_DETAIL_REQ_NO);
            gwMActivity.overridePendingTransition(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom);
            dialogFragment.show(fm.beginTransaction(), "dialog");
        }
    }
    //endregion

    //region FUNCTIONS
    private boolean FLAG_QC, FLAG_CANCEL, FLAG_APPROVE;

    private void SET_PERMISSION_BY_STATUS(int status){
        if(status == 1){            // = SAVE
            FLAG_CANCEL 	= false;
            FLAG_APPROVE 	= true;
            FLAG_QC = true;
        }else if(status == 3){      // = APPROVE
            FLAG_CANCEL 	= true;
            FLAG_APPROVE 	= false;
            FLAG_QC = false;
        }else if(status == 4){      // = CANCEL
            FLAG_CANCEL 	= false;
            FLAG_APPROVE 	= true;
            FLAG_QC 		= true;
        }

        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (FLAG_CANCEL) {
                    rootView.findViewById(R.id.btnCancel).setVisibility(View.VISIBLE);
                } else {
                    rootView.findViewById(R.id.btnCancel).setVisibility(View.GONE);
                }

                if (FLAG_APPROVE) {
                    rootView.findViewById(R.id.btnApprove).setVisibility(View.VISIBLE);
                } else {
                    rootView.findViewById(R.id.btnApprove).setVisibility(View.GONE);
                }

                if (FLAG_QC) {
                    rootView.findViewById(R.id.btnQC).setVisibility(View.VISIBLE);
                } else {
                    rootView.findViewById(R.id.btnQC).setVisibility(View.GONE);
                }
            }
        });

    }

    public String NullOrString(String str, String charSpecific){
        return (charSpecific).equals(str)?"":str;
    }

    public void InitItem(boolean flagHiddenInspTime, boolean ClearDataGrid){
        imageBitmap = null;
        ((TextView)rootView.findViewById(R.id.etReqNo)).setText("");
        ((TextView)rootView.findViewById(R.id.etItem)).setText("");
        ((TextView)rootView.findViewById(R.id.etDate)).setText("");
        ((TextView)rootView.findViewById(R.id.etJudgment)).setText("");
        ((TextView)rootView.findViewById(R.id.etRemark)).setText("");
        //((TextView)rootView.findViewById(R.id.etSlipNoIncome)).setText("");
        if(ClearDataGrid){
            ((TextView)rootView.findViewById(R.id.etSlipNo)).setText("");
            TableLayout tbD = (TableLayout) rootView.findViewById(R.id.tbD);
            tbD.removeAllViews();

            TableLayout tbC = (TableLayout) rootView.findViewById(R.id.tbC);
            tbC.removeAllViews();
        }

        flagloadSlipNo 	= true;

        FLAG_APPROVE 	= false;
        FLAG_CANCEL 	=false;
        FLAG_QC 		= true;
        REMARK = "";
        //flagloadSlipNoIncome=true;
        //_pkSlipNoIncome="";
        SET_PERMISSION_BY_STATUS(0);
        //hidden date and time
        if(flagHiddenInspTime){
            HiddenUITimeNo();
        }
    }

    public void ProcessDelete() {
        try {
            db = gwMActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            db.execSQL("DELETE FROM INV_TR WHERE  TR_TYPE = '" + QC_Type_PK + "';");
            InitItem(true, true);
        } catch (Exception ex) {
            gwMActivity.alertToastLong("Delete All :" + ex.getMessage());
        } finally {
            db.close();
        }
    }

    public void alertDialogYN(String title, String mess, final String _type) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event
                if (_type.equals("onDelAll")) {
                    ProcessDelete();
                }
                if(_type.equals("onMakeQC")){
                    ProcessQCSlipNo();
                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void OnScanBC() {

        //Step 1: Insert Req No

        if (myBC.getText().toString().equals("")) {
            gwMActivity.alertToastShort("Pls Scan barcode!");
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        }else {
            REQ_NO = String.valueOf(myBC.getText().toString().toUpperCase().trim().split("\n"));

            try {
                flagloadSlipNo = true;
                db = gwMActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                scan_time = CDate.getDateYMDHHmmss();

                String sql = 	"INSERT INTO INV_TR(" +
                        " 						QC_REQ_NO, " +
                        " 						SCAN_TIME, "+
                        " 						SCAN_DATE, " +
                        " 						SENT_YN, " +
                        " 						STATUS, " +
                        " 						TR_TYPE, " +
                        " 						MAPPING_YN) "
                        + 						"VALUES('"
                        + 						REQ_NO + "','"
                        + 						scan_time + "','"
                        + 						scan_date + "', '"
                        + 						"N',"
                        + 						"' ','"
                        + 						QC_Type_PK + "',"
                        + 						"'N'"
                        + 					");";

                db.execSQL(sql);

                gwMActivity.alertToastShort("Get Data Success!");
                flagGetData = true;
                InitDataLipNo();
            } catch (Exception ex) {
                gwMActivity.alertToastShort("Get Data Error!!!");
                Log.e("OnSaveBC", ex.getMessage());
                flagGetData = false;
            } finally {
                db.close();
                myBC.getText().clear();
                myBC.requestFocus();
            }
        }

        //Create Process Get Data From DB.
        getDataByReqNo(REQ_NO, true, true);
    }

    public TextView makeTableColHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels, int sizeText) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(1, 1, 1, 1);

        final TextView bsTextView = new TextView(gwMActivity);
        bsTextView.setText(text);
        bsTextView.setTextColor(Color.YELLOW);
        bsTextView.setTextSize(sizeText);
        bsTextView.setGravity(Gravity.CENTER);
        bsTextView.setBackgroundColor(bsColors[1]);
        bsTextView.setPadding(0, 2, 0, 2);
        bsTextView.setLayoutParams(params);
        bsTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        bsTextView.setHeight(fixedHeightInPixels);
        return bsTextView;
    }

    public void setId(TextView bsTextView){
        bsTextView.setId(idButton);
        idButton++;

        bsTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = true;
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {

                    if(v.getTag() != null && v.getTag().toString().length() > 0){
                        int i = v.getId();
                        EditText t2 	= (EditText)rootView.findViewById(i+1);

                        String strTag 	= t2.getTag().toString();

                        if(strTag.contains(_TAG_CAV)){
                            t2 			= (EditText)rootView.findViewById(i+1);
                        }else if(strTag.contains(_TAG_RESULT)){
                            t2 			= (EditText)rootView.findViewById(i+5);//pass to 4 cell: Result, MIN, MAX, SA.
                        }

                        if(t2!=null)
                            t2.requestFocus();
                        else
                            handled = false;
                    }
                }
                return handled;
            }
        });
    }

    public String checkValidateInputInGrid(){
        if(SHIFT_A==null)
        {
            String ms="Please, Load Data!";
            return ms;
        }
        //check SHIFT
        if(SHIFT_A.equals("F")  && SHIFT_B.equals("F") && SHIFT_C.equals("F")){
            return "Please Choose A Shift For The Slip No!!!";
        }else if(SHIFT_A.equals("T") && SHIFT_B.equals("T")){
            return "Please Choose A Shift For The Slip No!!!";
        }else if(SHIFT_A.equals("T") && SHIFT_C.equals("T")){
            return "Please Choose A Shift For The Slip No!!!";
        }else if(SHIFT_B.equals("T") && SHIFT_C.equals("T")){
            return "Please Choose A Shift For The Slip No!!!";
        }else if (SHIFT_A.equals("T") && SHIFT_B.equals("T") && SHIFT_C.equals("T")){
            return "Please Choose A Shift For The Slip No!!!";
        }
        //check PATROL_INSPECTION
        if(INSPECTION_TYPE == PATROL_INSPECTION){
            boolean flChoose = false;
            LinearLayout layoutTimeNo = (LinearLayout) rootView.findViewById(R.id.layoutTimeNo);
            for (int j = 0; j < layoutTimeNo.getChildCount(); j++) {

                View v = layoutTimeNo.getChildAt(j);
                if(v instanceof CheckBox){
                    CheckBox cbTeam = new CheckBox(gwMActivity.getApplication());
                    cbTeam 			= (CheckBox)layoutTimeNo.getChildAt(j);
                    if(cbTeam.isChecked()){
                        flChoose = true;
                    }
                }
            }

            if(!flChoose){
                return "Please Choose Inspect Time!";
            }

            //Check datetime inspection
            if(lstInspectionTime.size() != TIME_NO){
                return "Please Choose Date & Time for Inspection Time";
            }
            Iterator<Map.Entry<String,ArrayList<String>>> itr = lstInspectionTime.entrySet().iterator();
            int indexList  = 0;
            while (itr.hasNext()) {
                Map.Entry<String,ArrayList<String>> entry = itr.next();
                indexList++;
                List<String> list = entry.getValue();
                if(list.get(0) == null || list.get(0).toString().length() == 0){
                    return "Please Choose Date for Inspection Time " + indexList;
                }else if(list.get(1) == null || list.get(1).toString().length() == 0){
                    return "Please Choose Time for Inspection Time " + indexList;
                }

            }
        }
        //INCOME_INSPECTION
        //---------
        String message = "";
        TableLayout tbD = (TableLayout)rootView.findViewById(R.id.tbD);
        for(int i = 0; i<tbD.getChildCount(); i++){
            TableRow trP = (TableRow) tbD.getChildAt(i);
            View view ;
            for (int k = 0; k < trP.getChildCount(); k++) {
                view = trP.getChildAt(k);
                if(view instanceof EditText){
                    EditText tvTeamp = (EditText)trP.getChildAt(k);

                    if(tvTeamp.getTag().toString().toLowerCase().contains(TAG_NUMBER.toLowerCase())){
                        String strNumber = tvTeamp.getText().toString();
                        if(strNumber.trim().length()>0
                                && !CNumber.checkNumberFloat(strNumber)){
                            tvTeamp.requestFocus();
                            return  "Please Input Number At Red Cells!";
                        }
                    }

                    if(tvTeamp.getTag().toString().toLowerCase().contains(_TAG_RESULT.toLowerCase())){
                        String strResult = tvTeamp.getText().toString();
                        if(strResult.trim().length()== 0){
                            return  "Please Input Data At Row " + (i+1) +"!";
                        }
                    }
                }
            }
        }
        return  message;
    }

    private void HiddenUIIncomeSlipNo(){
        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(INSPECTION_TYPE ==INCOME_INSPECTION) {

                    rootView.findViewById(R.id.tvSlipNoIncome).setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.etSlipNoIncome).setVisibility(View.VISIBLE);
                }
                else{
                    _pkSlipNoIncome 	="0";
                    _nameSlipNoIncome 	= "";
                    ((EditText)rootView.findViewById(R.id.etSlipNoIncome)).setText(_nameSlipNoIncome);
                    rootView.findViewById(R.id.tvSlipNoIncome).setVisibility(View.GONE);
                    rootView.findViewById(R.id.etSlipNoIncome).setVisibility(View.GONE);
                }
            }
        });
    }

    private void HiddenUITimeNo(){
        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                lstInspectionTime.clear();
                hashMapSlipNoByInsTime.clear();

                rootView.findViewById(R.id.layoutDateTime).setVisibility(View.GONE);
                LinearLayout layoutTimeNo = (LinearLayout) rootView.findViewById(R.id.layoutTimeNo);
                layoutTimeNo.removeAllViews();
            }
        });
    }

    private void ShowUITimeNo(final int timeNo){
        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                HiddenUITimeNo();
                rootView.findViewById(R.id.layoutDateTime).setVisibility(View.VISIBLE);
                final LinearLayout layoutTimeNo = (LinearLayout) rootView.findViewById(R.id.layoutTimeNo);
                final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
                DrawerLayout.LayoutParams layoutParamsTextView = new DrawerLayout.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT, (int) 1.0f);

                float x = (float) 1.4, y = (float) 1.4;
                for (int i = 0; i < timeNo; i++) {

                    final CheckBox cb = new CheckBox(gwMActivity.getApplication());
                    cb.setText("" + (i + 1));
                    cb.setTextColor(Color.BLUE);
                    cb.setTextSize(16);
                    cb.setScaleX(x);
                    cb.setScaleX(y);
                    cb.setTag(Tag_Time_No+(i+1));

                    if (i == 0) {
                        layoutParams.setMargins(10, 0, 0, 0);
                        cb.setChecked(true);
                    } else {
                        layoutParams.setMargins(30, 0, 0, 0);
                    }

                    cb.setLayoutParams(layoutParams);
                    layoutTimeNo.addView(cb);
                    //add even
                    cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                            String perious = null;
                            for (int p = 0; p < layoutTimeNo.getChildCount(); p++) {
                                View v = layoutTimeNo.getChildAt(p);
                                if(v instanceof  CheckBox){
                                    CheckBox cbTeamp =  (CheckBox)layoutTimeNo.getChildAt(p);
                                    if(cbTeamp.isChecked()){
                                        perious = cbTeamp.getTag().toString().split(Tag_Time_No)[1];
                                        // add lst datetime
                                        // add data
                                        InsertDetail_Image(Integer.valueOf(Integer.valueOf(perious)));
                                        break;
                                    }
                                }
                            }

                            //check checked

                            if(cb.isChecked()){
                                //set uncheck for checkbox othes
                                for (int j = 0; j < layoutTimeNo.getChildCount(); j++) {
                                    View v  = layoutTimeNo.getChildAt(j);
                                    if(v instanceof CheckBox){
                                        CheckBox cbTeam = new CheckBox(gwMActivity.getApplication());
                                        cbTeam 			= (CheckBox)layoutTimeNo.getChildAt(j);
                                        cbTeam.setChecked(false);
                                    }
                                }
                                cb.setChecked(true);

                                String[] strPk = cb.getTag().toString().split(Tag_Time_No);

                                //check date_time's checkbox id
                                boolean flagDateTime = true;
                                if(lstInspectionTime.get(Integer.valueOf(strPk[1])) == null){
                                    ((TextView)rootView.findViewById(R.id.tvDate_Time)).setText("");
                                    strTime = "";
                                    strDate = "";
                                    flagDateTime = false;
                                    gwMActivity.alertToastShort("Please Choose Date & Time");
                                }else{
                                    int pkInspectionTime = Integer.valueOf(strPk[1]);
                                    List lstDateTime = (List) lstInspectionTime.get(pkInspectionTime);
                                    String date = lstDateTime.get(0)!=null?lstDateTime.get(0).toString():"";
                                    String time = lstDateTime.get(1)!=null?lstDateTime.get(1).toString():"";

                                    if(date.length()==0 ){
                                        gwMActivity.alertToastShort("Please Choose Date!");
                                        flagDateTime = false;
                                    }else if(time.length()==0 ){
                                        gwMActivity.alertToastShort("Please Choose Time!");
                                        flagDateTime = false;
                                    }
                                    ((TextView)rootView.findViewById(R.id.tvDate_Time)).setText(date + " " + time);
                                    //get data in grid

                                }

                                Log.i("pk previous: ", perious);
                                if(!flagDateTime){
                                    //    return;
                                }

                                InsertDetail_Image(Integer.valueOf(strPk[1]));
                                ///// TODO: 7/12/2016 clear table D
                                TableLayout tableD 	= (TableLayout) rootView.findViewById(R.id.tbD);
                                for (int j = 0; j < tableD.getChildCount(); j++) {//index 2
                                    TableRow tr1 = (TableRow) tableD.getChildAt(j);
                                    for (int l = 2; l < tr1.getChildCount()-2; l++) {
                                        Object object = tr1.getChildAt(l);
                                        if(object instanceof EditText){
                                            EditText editText = (EditText)object;
                                            editText.setText("");
                                        }else if(object instanceof CheckBox){
                                            CheckBox checkbox = (CheckBox)object;
                                            checkbox.setChecked(false);
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    private int getIDCheckedCheckBox(final int timeNo){
        final LinearLayout layoutTimeNo = (LinearLayout) rootView.findViewById(R.id.layoutTimeNo);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);

        for (int i = 0; i < timeNo; i++) {
            View v  = layoutTimeNo.getChildAt(i);
            if(v instanceof CheckBox){
                CheckBox cbTeam = new CheckBox(gwMActivity.getApplication());
                cbTeam 			= (CheckBox)layoutTimeNo.getChildAt(i);
                if(cbTeam.isChecked()){
                    String[] pk = cbTeam.getTag().toString().split(Tag_Time_No);
                    return Integer.valueOf(pk[1]);
                }
            }
        }
        return 0;
    }

    //region ___GRID & ITEM
    public void SetDataToEditText(final String[][] data){
        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) rootView.findViewById(R.id.etReqNo)).setText(data[0][2]);
                //((TextView) rootView.findViewById(R.id.etItem)).setText(data[0][12] + "*" + data[0][13]);
                ((TextView) rootView.findViewById(R.id.etItem)).setText(data[0][12]);
                ((TextView) rootView.findViewById(R.id.etDate)).setText(CDate.getFormatYYYYMMDD(data[0][1]));
            }

        });
    }

    public void SetGridTeamplateInspection(final String[][] data){
        final TableRow row 		= new TableRow(gwMActivity);
        final int count 		= data.length;
        final TableLayout tbC 	= (TableLayout) rootView.findViewById(R.id.tbC);
        final TableLayout tbD 	= (TableLayout) rootView.findViewById(R.id.tbD);

        //get width header
        HD_SEQ 					= ((TextView)rootView.findViewById(R.id.hd_Seq)).getWidth();
        HD_INSPECTIONTYPE 		= ((TextView)rootView.findViewById(R.id.hd_InspectionType)).getWidth();
        HD_INSPECTIONITEM 		= ((TextView)rootView.findViewById(R.id.hd_InspectionItem)).getWidth();
        HD_INSPECTIONCONTENT 	= ((TextView)rootView.findViewById(R.id.hd_InspectionContent)).getWidth();
        HD_SAMPLING 			= ((TextView)rootView.findViewById(R.id.hd_Sampling)).getWidth();
        HD_JUDGEMENT 			= ((TextView)rootView.findViewById(R.id.hd_Judgement)).getWidth();
        HD_REMARK 				= ((TextView)rootView.findViewById(R.id.hd_Remark)).getWidth();

        HD_SA 					= ((TextView)rootView.findViewById(R.id.hd_SA)).getWidth();
        HD_RESULT 				= ((TextView)rootView.findViewById(R.id.hd_Result)).getWidth();
        CAVITY_WIDTH  = HD_REMARK;
        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

/*                tbC.removeAllViews();
                tbD.removeAllViews();*/

                boolean fgShowJudgement = false;

                String judgement 		= "";

                //Reset Id
                idButton = 1;

                for (int i = 0; i < count; i++) {
                    final TableRow rowTable_D = new TableRow(gwMActivity.getApplication());
                    rowTable_D.setLayoutParams(wrapWrapTableRowParams);
                    rowTable_D.setGravity(Gravity.CENTER);
                    rowTable_D.setBackgroundColor(Color.LTGRAY);

                    final TableRow rowTable_C = new TableRow(gwMActivity.getApplication());
                    rowTable_C.setLayoutParams(wrapWrapTableRowParams);
                    rowTable_C.setGravity(Gravity.CENTER);
                    rowTable_C.setBackgroundColor(Color.LTGRAY);
                    rowTable_C.setMinimumHeight(37);
                    //PK
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][0], 0, fixedRowHeight, 0, 1, false,""));
                    //SEQ
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][2], HD_SEQ, fixedRowHeight, 0, 1, false,""));
                    //Inspection Type
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][5], HD_INSPECTIONTYPE, fixedRowHeight, -1, 1, false,""));
                    //Inspection ITEM
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][7], HD_INSPECTIONITEM, fixedRowHeight, -1, 1, false,""));
                    //Inspection Content
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][9], HD_INSPECTIONCONTENT, fixedRowHeight, -1, 1, false,""));
                    //Sampling Inspection Standard
                    rowTable_D.addView(makeTableColWithEditText(false, data[i][11], HD_SAMPLING, fixedRowHeight, 0, 1, false,""));
                    //Judgement/Gage
                    judgement =NullOrString(data[i][13], "-");
                    if(judgement.length() > 0){
                        fgShowJudgement = true;
                    }
                    //Judgement/Gage
                    rowTable_D.addView(makeTableColWithEditText(false, judgement, HD_JUDGEMENT, fixedRowHeight, 0, 1, false,""));
                    //Remark
                    rowTable_D.addView(makeTableColWithEditText(true,"", HD_REMARK, fixedRowHeight, 0, 1, true,_TAG_REMARK+i));
                    int CharOrNumber;
                    String text_yn 		= data[i][17].toString();
                    String tag			=_TAG_CAV;
                    if(text_yn.endsWith("Y")){
                        CharOrNumber 	= 1;
                        tag+=TAG_TEXT;
                    }else{
                        CharOrNumber 	= 2;
                        tag+=TAG_NUMBER;
                    }
                    for(int k = 0; k<CAVITY_LIST_PK.size() && k < MAX_CAVITY_COLOUMN; k++){
                        rowTable_D.addView(makeTableColWithEditText(true, "", CAVITY_WIDTH, fixedRowHeight, CharOrNumber, CharOrNumber, true,tag));
                    }
                    //Result
                    rowTable_D.addView(makeTableColWithEditText(false, "", HD_RESULT, fixedRowHeight, 0, 1, false, _TAG_RESULT+i));
                    //SA
                    checkBox = new CheckBox(gwMActivity.getBaseContext());
                    checkBox.setWidth(HD_SA);
                    checkBox.setHeight(35);
                    checkBox.setBackgroundColor(-1);
                    checkBox.setLayoutParams(lpCheckBox);
                    checkBox.setGravity(Gravity.CENTER);
                    checkBox.setMaxLines(1);
                    checkBox.setId(idButton);
                    idButton++;
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                    {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                        {

                            int id 			= buttonView.getId();
                            EditText et 	= (EditText)rootView.findViewById(id-1);
                            String result 	= et.getText().toString().trim();
                            if(result.length() == 0 || result.trim().equals(_OK)){
                                if(isChecked){
                                    CheckBox cb = (CheckBox)rootView.findViewById(id);
                                    cb.setChecked(false);
                                }
                            }

                        }
                    });

                    //Set permission Checkbox
                    if(data[i][16].equals("1")){
                        checkBox.setEnabled(true);
                    }else{
                        checkBox.setEnabled(false);
                    }
                    rowTable_D.addView(checkBox);

                    //MIN, MAX
                    rowTable_D.addView(makeTableColWithEditText(true, data[i][14], 0, fixedRowHeight, 0, 2, false,"Min"));
                    rowTable_D.addView(makeTableColWithEditText(true, data[i][15], 0, fixedRowHeight, 0, 2, false,"Max"));

                    tbD.addView(rowTable_D);
                    tbC.addView(rowTable_C);

                }

                if(fgShowJudgement == false){
                    if(!tbD.isColumnCollapsed(1))
                        tbD.setColumnCollapsed(1, !tbD.isColumnCollapsed(1));//hide

                    TextView tvJudgement = (TextView)rootView.findViewById(R.id.hd_Judgement);
                    tvJudgement.setVisibility(View.GONE);//hide header
                    Log.e("QC Hidden Col Judgement", "");
                }else{
                    if(!tbD.isColumnCollapsed(1))
                        tbD.setColumnCollapsed(1, tbD.isColumnCollapsed(1));
                    else{
                        tbD.setColumnCollapsed(1, !tbD.isColumnCollapsed(1));
                    }

                    TextView tvJudgement = (TextView)rootView.findViewById(R.id.hd_Judgement);
                    tvJudgement.setVisibility(View.VISIBLE); //show header
                    Log.e("QCShow Column Judgement", "");
                }
            }

        });

        //If data in grid have Height less than height scrollViewD - > Not catch even scroll
        //                                                 ELSE - > Catch even scroll

    }

    //Step 1: Get data cavity by Itemcode of Req No
    private String[][] GetCavityByItem(final String itemCode){

        String[][] data = {null};

        try {
         //region Tangw tam close load o column Cavity
            //String result[][] 		= HandlerWebService.GetDataTableArg("drivtldrlgtl0001_s_12", itemCode);
            //Log.i("QC_GetCavityByItem:"," drivtldrlgtl0001_s_12: " +itemCode);
            //data = result;
        //endregion
            String result[][]=null;

            if(data != null && data.length >0){
                CAVITY_LIST_PK		= new ArrayList<String>();
                CAVITY_LIST_NAME 	= new ArrayList<String>();

                for (int i = 0; i < data.length; i++) {
                    CAVITY_LIST_PK.add(result[i][0]);
                    CAVITY_LIST_NAME.add("CAV# "+result[i][3]);
                }
            }else{
                //for (int i = 0; i < 10; i++) {
                    CAVITY_LIST_PK		= new ArrayList<String>();
                    CAVITY_LIST_NAME 	= new ArrayList<String>();
                //    CAVITY_LIST_PK.add(String.valueOf(i));
                 //   CAVITY_LIST_NAME.add("CAV# ");
                //}
            }
        } catch (Exception ex) {
            Log.e("Searh Error: ", ex.getMessage());
            //for (int i = 0; i < 10; i++) {
                CAVITY_LIST_PK		= new ArrayList<String>();
                CAVITY_LIST_NAME 	= new ArrayList<String>();
            //   CAVITY_LIST_PK.add(String.valueOf(i));
            //    CAVITY_LIST_NAME.add("CAV# ");
            //}
        }
        return data;
    }

    //Step 2: General Grid by Cavity
    private void ShowHeader(final String itemCode){

        if(FLAG_HEADER_CAVITY){
            GetCavityByItem(itemCode);
        }

        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //01: Put all cavity header into lstCavity
                List<TextView> lstCavity = new ArrayList<TextView>();
                List<TextView> lstNumber = new ArrayList<TextView>();

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_1)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_1)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_1)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_1)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_2)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_2)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_2)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_2)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_3)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_3)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_3)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_3)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_4)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_4)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_4)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_4)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_5)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_5)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_5)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_5)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_6)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_6)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_6)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_6)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_7)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_7)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_7)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_7)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_8)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_8)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_8)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_8)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_9)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_9)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_9)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_9)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_10)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_10)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_10)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_10)).setVisibility(View.VISIBLE);

                //count cavity of database
                for(int i = 0;i<CAVITY_LIST_PK.size() && i<MAX_CAVITY_COLOUMN; i++){
                    lstCavity.get(i).setText(CAVITY_LIST_NAME.get(i));
                }

                //hidden cavity column of teamplate
                for(int i = MAX_CAVITY_COLOUMN - 1; i >=CAVITY_LIST_PK.size(); i--){
                    TextView tv = new TextView(gwMActivity.getBaseContext());
                    TextView tv2 = new TextView(gwMActivity.getBaseContext());

                    tv = lstCavity.get(i);
                    tv2 = lstNumber.get(i);
                    tv.setVisibility(View.GONE);
                    tv2.setVisibility(View.GONE);
                }
            }
        });
    }

    private void ShowHeaderNoCavity(){
        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //01: Put all cavity header into
                List<TextView> lstCavity = new ArrayList<TextView>();
                List<TextView> lstNumber = new ArrayList<TextView>();

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_1)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_1)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_1)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_1)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_2)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_2)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_2)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_2)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_3)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_3)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_3)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_3)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_4)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_4)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_4)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_4)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_5)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_5)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_5)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_5)).setVisibility(View.VISIBLE);
                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_6)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_6)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_6)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_6)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_7)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_7)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_7)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_7)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_8)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_8)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_8)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_8)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_9)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_9)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_9)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_9)).setVisibility(View.VISIBLE);

                lstCavity.add(((TextView)rootView.findViewById(R.id.hd_Cav_10)));
                lstNumber.add(((TextView)rootView.findViewById(R.id.hd_Number_10)));
                ((TextView)rootView.findViewById(R.id.hd_Cav_10)).setVisibility(View.VISIBLE);
                ((TextView)rootView.findViewById(R.id.hd_Number_10)).setVisibility(View.VISIBLE);

                CAVITY_LIST_PK		= new ArrayList<String>();
                CAVITY_LIST_NAME 	= new ArrayList<String>();

                for (int rw = 0; rw < 5; rw++) {
                    CAVITY_LIST_PK.add(String.valueOf(rw+1));
                    CAVITY_LIST_NAME.add("S "+String.valueOf(rw+1));
                }
                //count cavity of database
                for(int i = 0;i<CAVITY_LIST_PK.size(); i++){
                    lstCavity.get(i).setText(CAVITY_LIST_NAME.get(i));
                }
                //hidden cavity column of teamplate
                for(int i = MAX_CAVITY_COLOUMN - 1; i >=CAVITY_LIST_PK.size(); i--){
                    TextView tv = new TextView(gwMActivity.getBaseContext());
                    TextView tv2 = new TextView(gwMActivity.getBaseContext());

                    tv = lstCavity.get(i);
                    tv2 = lstNumber.get(i);
                    tv.setVisibility(View.GONE);
                    tv2.setVisibility(View.GONE);
                }

            }
        });
    }
    public void InitDataLipNo(){
        CUSTOMER_NAME       = "";

        INSPECTOR_PK        = 0;
        INSPECTOR_NAME      = "";

        CHECKED_PK          = 0;
        CHECKED_NAME        = "";

        APPROVE_PK          = 0;
        APPROVE_NAME        = "";

        LINE_GROUP_PK       = 0;
        LINE_PK             = 0;

        SHIFT_A             = "";
        SHIFT_B             = "";
        SHIFT_C             = "";

        LOT_NO              = "";
        LOT_QTY             = "";
        LOT_NO_SUPPLIER     = "";


        INSPECTION_TYPE = 0;

        REMARK = "";

        lstInspectionTime.clear();
        hashMapSlipNoByInsTime.clear();
    }

    public void getDataByReqNo(final String _req_No, final boolean flagLoadCavityByItem, final boolean flagGetInfoMaster){
        if(flagGetData){
            if(flagLoadCavityByItem){
                InitItem(false, true);//InitItem(boolean flagHiddenInspTime, boolean ClearDataGrid)
            }
            else{
                InitItem(false, false);
            }
            progress = new ProgressDialog(gwMActivity);

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    progress.setMessage("Geting  Data From Server, Please Wait...");
                    progress.setCancelable(false);//not close when touch
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setProgress(0);
                    progress.show();

                    flagGetData = false;
                }

                @Override
                protected Void doInBackground(Void... params) {
                    flagGetData = false;
                    String _DataEditText[][] = HandlerWebService.GetDataTableArg("drivtldrlgtl0001_s_01", _req_No);
                    Log.e("QC_getDataByReqNo: ", "drivtldrlgtl0001_s_01" + _req_No);

                    if(_DataEditText != null && _DataEditText.length > 0) {
                        ITEM_PK = _DataEditText[0][29];
                        Log.i("ITEM_PK:",  _DataEditText[0][29]);

                        if(flagGetInfoMaster){

                            CUSTOMER_NAME   = _DataEditText[0][8];

                            INSPECTOR_PK	= Integer.valueOf(_DataEditText[0][3]);
                            INSPECTOR_NAME  = _DataEditText[0][4];

                            CHECKED_PK      = Integer.valueOf(_DataEditText[0][5]);
                            CHECKED_NAME    = _DataEditText[0][6];

                            APPROVE_PK 		= Integer.valueOf(_DataEditText[0][9]);
                            APPROVE_NAME	= _DataEditText[0][10];

                            /*SHIFT_A 		= _DataEditText[0][23];
                            SHIFT_B 		= _DataEditText[0][24];
                            SHIFT_C 		= _DataEditText[0][25];*/

                            SHIFT_A 		= "F";
                            SHIFT_B 		= "F";
                            SHIFT_C 		= "F";

                            LOT_NO 			= _DataEditText[0][21];
                            STATUS 			= 0;
                            INSPECTION_TYPE = Integer.valueOf(_DataEditText[0][30]);
                            TIME_NO 		= Integer.valueOf(_DataEditText[0][31]);
                            if(INSPECTION_TYPE ==PATROL_INSPECTION){
                                ShowUITimeNo(TIME_NO);
                            }else{
                                HiddenUITimeNo();

                                HiddenUIIncomeSlipNo();

                            }
                        }


                        SetDataToEditText(_DataEditText);
                        String _DataGridInspect[][] = HandlerWebService.GetDataTableArg("drivtldrlgtl0001_s_02", _req_No);
                        Log.e("getDataByReqNo: ", "drivtldrlgtl0001_s_02" +_req_No );
                        //Load Teamplate
                        if(_DataGridInspect != null && _DataGridInspect.length > 0 && ITEM_PK.length() >0)
                        {
                            if(flagLoadCavityByItem){
                                if(INSPECTION_TYPE==2 || INSPECTION_TYPE==4 ) {
                                    // tangw

                                    ShowHeader(ITEM_PK);
                                }else {
                                    ShowHeaderNoCavity();
                                    FLAG_HEADER_CAVITY=true;
                                }
                                SetGridTeamplateInspection(_DataGridInspect);
                            }
                        }else {
                            Log.e("GENERAL  GRID ERROR:   ", "Not Get _DataGridInspect or ITEM_PK of REQ_NO " + REQ_NO.toString());
                        }

                        UpdateStatusReqNo_InSQLITE(_STATUS_EXIST, _req_No);

                    }else {
                        UpdateStatusReqNo_InSQLITE(_STATUS_NOT_EXIST, _req_No);

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void args) {
                    super.onPreExecute();
                    progress.dismiss();
                    this.cancel(true);
                    flagGetData = true;
                }
            }.execute();
        }
    }

    public int CountCavityIn2D(String [][] data, int start, int end){
        int count =0;
        CAVITY_LIST_NAME = new ArrayList<String>();
        CAVITY_LIST_PK = new ArrayList<String>();

        HashSet hsCavity = new HashSet();
        for(int i = 0; i<data.length; i++){
            for (int k = start; k <= end; k+=3) {//pass to name, value
                if(!data[i][k].equals("-") && !hsCavity.contains(data[i][k])){
                    hsCavity.add(data[i][k]);
                    CAVITY_LIST_PK.add(data[i][k]);
                    CAVITY_LIST_NAME.add(data[i][k+1]);
                }

            }
        }
        return CAVITY_LIST_PK.size();
    }

    public void ShowDataBySlipNo(final String[][] data, final int judgment, final int pkInspectTime){

        //GET CAVITY OF SLIPNO
        CountCavityIn2D(data, 18, 47);

        final TableRow row = new TableRow(gwMActivity);
        final int count = data.length;

        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(REMARK.length() > 0 && !REMARK.equals("-")){
                    ((EditText)rootView.findViewById(R.id.etRemark)).setText(REMARK);
                }else{
                    ((EditText)rootView.findViewById(R.id.etRemark)).setText("");
                }

                //check show button getdata
                if(INSPECTION_TYPE == PATROL_INSPECTION
                        && ((EditText)rootView.findViewById(R.id.etSlipNo)).getText().length()>0){
                    ((TextView)rootView.findViewById(R.id.btn_GetData)).setVisibility(View.VISIBLE);
                }else{
                    ((TextView)rootView.findViewById(R.id.btn_GetData)).setVisibility(View.GONE);
                }

                //set PKCheckBox = pkInspectTime
                LinearLayout layoutTimeNo = (LinearLayout) rootView.findViewById(R.id.layoutTimeNo);
                for (int j = 0; j < layoutTimeNo.getChildCount(); j++) {

                    View v = layoutTimeNo.getChildAt(j);
                    if(v instanceof CheckBox){
                        CheckBox cbTeam = new CheckBox(gwMActivity.getApplication());
                        cbTeam 			= (CheckBox)layoutTimeNo.getChildAt(j);
                        String[] pk = cbTeam.getTag().toString().split(Tag_Time_No);
                        if(pk[1].equals(String.valueOf(pkInspectTime))){
                            cbTeam.setChecked(true);
                        }else{
                            cbTeam.setChecked(false);
                        }
                    }
                }

                if(judgment ==1){
                    ((EditText)rootView.findViewById(R.id.etJudgment)).setText("OK");
                }else if(judgment == 0){
                    ((EditText)rootView.findViewById(R.id.etJudgment)).setText("NG");
                }else {
                    ((EditText)rootView.findViewById(R.id.etJudgment)).setText("");
                }

                Log.e("SlipNoIncome",_nameSlipNoIncome);
                ((EditText)rootView.findViewById(R.id.etSlipNoIncome)).setText(_nameSlipNoIncome);

                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableLayout tbD = (TableLayout) rootView.findViewById(R.id.tbD);
                TableLayout tbC = (TableLayout) rootView.findViewById(R.id.tbC);

                tbD.removeAllViews();//remove all view child
                tbC.removeAllViews();

                boolean fgShowJudgement = false;
                String judgement = "";

                //Reset Id
                idButton = 1;
                int widthSampling = ((TextView)rootView.findViewById(R.id.hd_Sampling)).getWidth();
                int widthContent = ((TextView)rootView.findViewById(R.id.hd_InspectionContent)).getWidth();
                int widthItem = ((TextView)rootView.findViewById(R.id.hd_InspectionItem)).getWidth();
                int widthSA = ((TextView)rootView.findViewById(R.id.hd_SA)).getWidth();
                for (int i = 0; i < count; i++) {

                    final TableRow rowTable_D = new TableRow(gwMActivity.getApplication());
                    rowTable_D.setLayoutParams(wrapWrapTableRowParams);
                    rowTable_D.setGravity(Gravity.CENTER);
                    rowTable_D.setBackgroundColor(Color.LTGRAY);

                    final TableRow rowTable_C = new TableRow(gwMActivity.getApplication());
                    rowTable_C.setLayoutParams(wrapWrapTableRowParams);
                    rowTable_C.setGravity(Gravity.CENTER);
                    rowTable_C.setBackgroundColor(Color.LTGRAY);
                    rowTable_C.setMinimumHeight(37);

                    //PK
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][0], 0, fixedRowHeight, 0, 1, false,""));

                    //SEQ
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][2], HD_SEQ, fixedRowHeight, 0, 1, false,""));

                    //Inspection Type
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][5], HD_INSPECTIONTYPE, fixedRowHeight, -1, 1, false,""));


                    //Inspection Item
                 //   rowTable_C.addView(makeTableColWithEditText(false, data[i][7], 170, fixedRowHeight, -1, 1, false,""));
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][7], HD_INSPECTIONITEM, fixedRowHeight, -1, 1, false,""));

                    //Inspetion Content

                    //rowTable_C.addView(makeTableColWithEditText(false, data[i][9], 170, HD_INSPECTIONTYPE, -1, 1, false,""));
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][9], HD_INSPECTIONCONTENT, fixedRowHeight, -1, 1, false,""));

                    //Sampling Inspection Standard

                    //rowTable_D.addView(makeTableColWithEditText(false, data[i][11], 100, widthSampling*2, 0, 1, false,""));
                    rowTable_D.addView(makeTableColWithEditText(false, data[i][11], HD_SAMPLING, fixedRowHeight, 0, 1, false,""));

                    //Judgement/Gage
                    judgement =NullOrString(data[i][13], "-");

                    if(judgement.length() > 0){
                        fgShowJudgement = true;
                    }

                    rowTable_D.addView(makeTableColWithEditText(false, judgement, HD_JUDGEMENT, fixedRowHeight, -1, 1, false,""));

                    //Remark
                    rowTable_D.addView(makeTableColWithEditText(true,NullOrString(data[i][17], "-"), HD_REMARK, fixedRowHeight, -1, 1, true,_TAG_REMARK+i));

                    //Cav#
                    String text_yn = data[i][54].toString();
                    int CharOrNumber;
                    String tag=_TAG_CAV;
                    if(text_yn.endsWith("Y")){
                        CharOrNumber = 1;
                        tag+=TAG_TEXT;
                    }else{
                        CharOrNumber = 2;
                        tag+=TAG_NUMBER;
                    }

                    indexCavityInDataBase = 20;
                    for (int l = 0; l < CAVITY_LIST_PK.size(); l++) {
                        rowTable_D.addView(makeTableColWithEditText(true, NullOrString(data[i][indexCavityInDataBase], "-"), HD_REMARK, fixedRowHeight, CharOrNumber, CharOrNumber, true,tag));
                        indexCavityInDataBase+=3;
                    }

                    //Result
                    rowTable_D.addView(makeTableColWithEditText(false, NullOrString(data[i][48], "-"), HD_RESULT, fixedRowHeight, -1, 1, false,_TAG_RESULT+i));

                    //PK INPECTION
                    rowTable_D.addView(makeTableColWithEditText(false, NullOrString(data[i][15], "-"), 0, 0, 0, 1, false,""));
                    //alertToastShort("PK INSPECTION:" +data[i][15]);
                    //SA
                    checkBox = new CheckBox(gwMActivity.getApplication());
                    checkBox.setWidth(widthSA);
                    checkBox.setHeight(35);

                    checkBox.setBackgroundColor(-1);

                    checkBox.setLayoutParams(lpCheckBox);

                    checkBox.setGravity(Gravity.CENTER);

                    checkBox.setMaxLines(1);

                    //set value
                    if(data[i][49].equals("1")){
                        checkBox.setChecked(true);
                    }
                    else{
                        checkBox.setChecked(false);
                    }

                    //set permission
                    if(data[i][53].equals("1")){
                        checkBox.setEnabled(true);
                    }
                    else{
                        checkBox.setEnabled(false);
                    }

                    checkBox.setId(idButton);
                    idButton++;

                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                    {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                        {
                            int id = buttonView.getId();
                            EditText et = (EditText)rootView.findViewById(id-1);
                            String result = et.getText().toString().trim();
                            if(result.length() == 0 || result.trim().equals(_OK)){
                                if(isChecked){
                                    CheckBox cb = (CheckBox)rootView.findViewById(id);
                                    cb.setChecked(false);
                                }
                            }
                        }
                    });

                    rowTable_D.addView(checkBox);

                    rowTable_D.addView(makeTableColWithEditText(true, data[i][50], 0, fixedRowHeight, 0, 2, false,"Min"));
                    rowTable_D.addView(makeTableColWithEditText(true, data[i][51], 0, fixedRowHeight, 0, 2, false,"Max"));

                    tbC.addView(rowTable_C);
                    tbD.addView(rowTable_D);
                }

                if(fgShowJudgement == false){
                    if(!tbD.isColumnCollapsed(1))
                        tbD.setColumnCollapsed(1, !tbD.isColumnCollapsed(1));

                    TextView tvJudgement = (TextView)rootView.findViewById(R.id.hd_Judgement);
                    tvJudgement.setVisibility(View.GONE);
                    Log.e("Hidden Column Judgement", "");
                }else{
                    tbD.setColumnCollapsed(1, tbD.isColumnCollapsed(1));

                    TextView tvJudgement = (TextView)rootView.findViewById(R.id.hd_Judgement);
                    tvJudgement.setVisibility(View.VISIBLE);
                    Log.e("Show Column Judgement", "");
                }

                //CHECK SHOW INSPECTION TIME
                if(INSPECTION_TYPE == PATROL_INSPECTION && !data[0][56].equals("-")){
                    TextView tvDate_Time = (TextView)rootView.findViewById(R.id.tvDate_Time);
                    tvDate_Time.setText(data[0][56]);

                    //input inspection time to hash map
                    String date="", time="";
                    List lstDateTime = new ArrayList();
                    String[] strAray = data[0][56].split(" ");
                    lstDateTime= Arrays.asList(strAray);  ;
                    lstInspectionTime.put(pkInspectTime, lstDateTime);
                }else{
                    TextView tvDate_Time = (TextView)rootView.findViewById(R.id.tvDate_Time);
                    tvDate_Time.setText("");
                }


            }
        });
    }

    public void ShowDataBySlipNoCoppy(final String[][] data){

        //GET CAVITY OF SLIPNO
        CountCavityIn2D(data, 18, 47);

        final TableRow row = new TableRow(gwMActivity);
        final int count = data.length;

        gwMActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

                TableLayout tbD = (TableLayout) rootView.findViewById(R.id.tbD);
                TableLayout tbC = (TableLayout) rootView.findViewById(R.id.tbC);

                tbD.removeAllViews();
                tbC.removeAllViews();

                boolean fgShowJudgement = false;
                String judgement = "";

                //Reset Id
                idButton = 1;

                for (int i = 0; i < count; i++) {

                    final TableRow rowTable_D = new TableRow(gwMActivity.getApplication());
                    rowTable_D.setLayoutParams(wrapWrapTableRowParams);
                    rowTable_D.setGravity(Gravity.CENTER);
                    rowTable_D.setBackgroundColor(Color.LTGRAY);

                    final TableRow rowTable_C = new TableRow(gwMActivity.getApplication());
                    rowTable_C.setLayoutParams(wrapWrapTableRowParams);
                    rowTable_C.setGravity(Gravity.CENTER);
                    rowTable_C.setBackgroundColor(Color.LTGRAY);
                    rowTable_C.setMinimumHeight(37);

                    //PK
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][0], 0, fixedRowHeight, 0, 1, false,""));

                    //SEQ
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][2], HD_SEQ, fixedRowHeight, 0, 1, false,""));

                    //Inspection Type
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][5], HD_INSPECTIONTYPE, fixedRowHeight, -1, 1, false,""));

                    //Inspection Item
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][7], HD_INSPECTIONITEM, fixedRowHeight, -1, 1, false,""));

                    //Inspetion Content
                    rowTable_C.addView(makeTableColWithEditText(false, data[i][9], HD_INSPECTIONCONTENT, fixedRowHeight, -1, 1, false,""));

                    //Sampling Inspection Standard
                    rowTable_D.addView(makeTableColWithEditText(false, data[i][11], HD_SAMPLING, fixedRowHeight, 0, 1, false,""));

                    //Judgement/Gage
                    judgement =NullOrString(data[i][13], "-");

                    if(judgement.length() > 0){
                        fgShowJudgement = true;
                    }

                    rowTable_D.addView(makeTableColWithEditText(false, judgement, HD_JUDGEMENT, fixedRowHeight, -1, 1, false,""));

                    //Remark
                    rowTable_D.addView(makeTableColWithEditText(true,"", HD_REMARK, fixedRowHeight, -1, 1, true,_TAG_REMARK+i));

                    //Cav#
                    String text_yn = data[i][54].toString();
                    int CharOrNumber;
                    String tag=_TAG_CAV;
                    if(text_yn.endsWith("Y")){
                        CharOrNumber = 1;
                        tag+=TAG_TEXT;
                    }else{
                        CharOrNumber = 2;
                        tag+=TAG_NUMBER;
                    }

                    indexCavityInDataBase = 20;
                    for (int l = 0; l < CAVITY_LIST_PK.size(); l++) {
                        rowTable_D.addView(makeTableColWithEditText(true, "", HD_REMARK, fixedRowHeight, CharOrNumber, CharOrNumber, true,tag));
                        indexCavityInDataBase+=3;
                    }

                    //Result
                    rowTable_D.addView(makeTableColWithEditText(false, "", 100, fixedRowHeight, -1, 1, false,_TAG_RESULT+i));

                    //PK INPECTION
                    rowTable_D.addView(makeTableColWithEditText(false, NullOrString(data[i][15], "-"), 0, fixedRowHeight, 0, 1, false,""));

                    //SA
                    checkBox = new CheckBox(gwMActivity.getApplication());
                    checkBox.setWidth(35);
                    checkBox.setHeight(35);

                    checkBox.setBackgroundColor(-1);

                    checkBox.setLayoutParams(lpCheckBox);

                    checkBox.setGravity(Gravity.CENTER);

                    checkBox.setMaxLines(1);

                    //set value
                    if(data[i][49].equals("1")){
                        checkBox.setChecked(true);
                    }
                    else{
                        checkBox.setChecked(false);
                    }

                    //set permission
                    if(data[i][53].equals("1")){
                        checkBox.setEnabled(true);
                    }
                    else{
                        checkBox.setEnabled(false);
                    }

                    checkBox.setId(idButton);
                    idButton++;

                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                    {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                        {
                            int id = buttonView.getId();
                            EditText et = (EditText)rootView.findViewById(id-1);
                            String result = et.getText().toString().trim();
                            if(result.length() == 0 || result.trim().equals(_OK)){
                                if(isChecked){
                                    CheckBox cb = (CheckBox)rootView.findViewById(id);
                                    cb.setChecked(false);
                                }
                            }
                        }
                    });

                    rowTable_D.addView(checkBox);

                    rowTable_D.addView(makeTableColWithEditText(true, data[i][50], 0, fixedRowHeight, 0, 2, false,"Min"));
                    rowTable_D.addView(makeTableColWithEditText(true, data[i][51], 0, fixedRowHeight, 0, 2, false,"Max"));

                    tbC.addView(rowTable_C);
                    tbD.addView(rowTable_D);
                }

                if(fgShowJudgement == false){
                    if(!tbD.isColumnCollapsed(1))
                        tbD.setColumnCollapsed(1, !tbD.isColumnCollapsed(1));

                    TextView tvJudgement = (TextView)rootView.findViewById(R.id.hd_Judgement);
                    tvJudgement.setVisibility(View.GONE);
                    Log.e("Hidden Column Judgement", "");
                }else{
                    tbD.setColumnCollapsed(1, tbD.isColumnCollapsed(1));

                    TextView tvJudgement = (TextView)rootView.findViewById(R.id.hd_Judgement);
                    tvJudgement.setVisibility(View.VISIBLE);
                    Log.e("Show Column Judgement", "");
                }
            }
        });
    }

    public void UpdateStatusReqNo_InSQLITE(final String status, final String ReqNo){
        try {
            db = gwMActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String sql = "UPDATE INV_TR SET STATUS = '" + status + "'  WHERE QC_REQ_NO = '" + ReqNo + "'";
            db.execSQL(sql);
        } catch (Exception ex) {
            Log.e("Ex Update Status: ", ex.getMessage());
        } finally {
            db.close();
        }
    }

    //endregion

    //region IMAGE

    private File createImageFile() throws IOException {

        // Create an image file name
        String timeStamp 		= new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName 	= "JPEG_" + timeStamp + "_";
        File storageDir 		= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image 			= File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        gwMActivity.sendBroadcast(mediaScanIntent);
    }

    private void setPic() {

    /* There isn't enough memory to open up more than a couple camera photos */
    /* So pre-scale the target bitmap into which the file is decoded */

    /* Get the size of the ImageView */
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

    /* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

    /* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        }

    /* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds 	= false;
        bmOptions.inSampleSize 			= scaleFactor;
        bmOptions.inPurgeable 			= true;

    /* Decode the JPEG file into a Bitmap */
        imageBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

    /* Associate the Bitmap to the ImageView */
        mImageView.setImageBitmap(imageBitmap);
    }

    private File setUpPhotoFile() throws IOException {

        File f 				= createImageFile();
        mCurrentPhotoPath 	= f.getAbsolutePath();

        return f;
    }

    private void dispatchTakePictureIntent_() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File f = null;

        try {
            f = setUpPhotoFile();
            mCurrentPhotoPath = f.getAbsolutePath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        } catch (IOException e) {
            e.printStackTrace();
            f = null;
            mCurrentPhotoPath = null;
        }
        startActivityForResult(takePictureIntent, ACTION_TAKE_PHOTO_B);
    }

    private int COLUMN_CAVITY = 10;
    Map<String, String> hashMapSlipNoByInsTime = new HashMap<String,String>();

    private String INSPECTION_PK_BY_TIME_NO = "INSPECTION_PK_BY_TIME_NO";
    /**
     * Insert To Database By 2 Option
     * 1: No TimeNo
     * 2: Have TimeNo PK
     *   - Save TimeNo PK and para String in HashMap
     * @param timeNoPK
     */

    /***
     * timeNoPK = 0 : Inpection Type = 1,2,3.
     * timeNoPk # 0 : PK of checkbox
     * @param timeNoPK
     */

    public void InsertDetail_Image(int timeNoPK){
        JUDGMENT = 1;

        String Para 		= "";
        TableLayout tableC 	= (TableLayout) rootView.findViewById(R.id.tbC);
        TableLayout tableD 	= (TableLayout) rootView.findViewById(R.id.tbD);

        if(tableD.getChildCount() == 0 || tableC.getChildCount() == 0) return;

        for(int i = 0; i<tableD.getChildCount(); i++){
            rowDetail++;
            TableRow row_tb_C 	=	(TableRow)tableC.getChildAt(i);
            TableRow row_tb_D 	=	(TableRow)tableD.getChildAt(i);
            CheckBox cbSA 		= 	null;

            //find checkbox in row
            for (int l = 0; l < row_tb_D.getChildCount(); l++) {
                Object o = row_tb_D.getChildAt(l);
                if(o instanceof CheckBox){
                    cbSA = (CheckBox)row_tb_D.getChildAt(l);
                    break;
                }
            }

            //pk, remark, result, sa.
            String sa       = cbSA.isChecked()?"1":"0";
            String strCav   = "";
            int k;
            for(k = 0; k<CAVITY_LIST_PK.size(); k++){
                strCav+=    CAVITY_LIST_PK.get(k) + "|";                                                     //pk
                strCav+=    CAVITY_LIST_NAME.get(k) + "|";                                                     //name
                strCav+=    ((TextView)row_tb_D.getChildAt(k+3)).getText().toString()             + "|";    //value
            }

            //hardcode 10 column cavity.
            for(int l =k; l<COLUMN_CAVITY; l++ ){
                strCav+= "|";     //pk
                strCav+= "|";     //name
                strCav+= "|";    //value
            }

            String result = ((TextView)row_tb_D.getChildAt(CAVITY_LIST_PK.size()+3)).getText().toString();
            if(!result.equals("OK")){
                JUDGMENT = 0;
                APPROVED_INCOME="NG";
            }

            //Check Inspect Time, Mark . We'll replace it when QC
            String InspectionPK = _TLG_INSPECTION_ENTRY_M_PK;
            if(timeNoPK!=0){
                InspectionPK = INSPECTION_PK_BY_TIME_NO;
            }
            Para+=  user + "|" +
                    InspectionPK + "|" +
                    //p_tlg_inspect_req_d_pk
                    ((TextView)row_tb_C.getChildAt(0)).getText().toString()             + "|" +
                    //Remark
                    ((TextView)row_tb_D.getChildAt(2)).getText().toString()             + "|" +
                    //Cav#1
                    strCav +
                    //Result
                    result + "|";
            //PK
            if(((EditText)rootView.findViewById(R.id.etSlipNo)).getText().toString().length() == 0){//insert
                Para+= "0"+"|";
            }else{//update
                Para+= ((TextView)row_tb_D.getChildAt(CAVITY_LIST_PK.size()+4)).getText().toString() +"|";
            }
            //SA
            Para+= sa+"|";

            //Insection Time
            if(timeNoPK == 0){
                Para+=""+"|"+"";
            }else{
                List lstDate = (List) lstInspectionTime.get(timeNoPK);
                if(lstDate!=null){
                    Para+=String.valueOf(timeNoPK)+"|";
                    Para+=lstDate.get(0)+" "+lstDate.get(1);
                }else{
                    Para+=String.valueOf(timeNoPK)+"|"+"";
                }
            }
            Para+="*|*";
        }

        String exeBySP = Para + "|!"+ "drivtldrlgtl0001_u_04";
        if(timeNoPK == 0){
            dataDetail					=   new String [1];
            int j 						=   0;
            dataDetail[j++] 			=   exeBySP;
            Detail_AsyncTask 	=   new QCManagementAsyncTask(gwMActivity);
            Detail_AsyncTask.execute(dataDetail);

            Log.e("InsertDetail: " ,"drivtldrlgtl0001_u_04 : " + exeBySP);
            UpLoadImage();
        }else{

            hashMapSlipNoByInsTime.put(String.valueOf(timeNoPK), Para);
        }
    }

    public void UpdateMasterBySlipNo(String slipNo){
        int j = 0;
        dataMaster=new String [1];
        String remark = ((EditText)rootView.findViewById(R.id.etRemark)).getText().toString();
        String paraMaster	= slipNo + "|" + remark + "*|*";

        paraMaster 	+= 	"|!"+ "drivtldrlgtl0001_u_15";
        dataMaster[j++] = paraMaster;
        QC_AsyncTask = new QCManagementAsyncTask(gwMActivity);
        if (CNetwork.isInternet(tmpIP, checkIP)) {

            QC_AsyncTask.execute(dataMaster);
            if(QC_AsyncTask.getPk_Master().length() > 0 && QC_AsyncTask.getSlipNo().length() > 0){
                //insert detail
                ((EditText)rootView.findViewById(R.id.etSlipNo)).setText(QC_AsyncTask.getSlipNo().toString());

                flagInserDetail = true;
            }
        } else{
            gwMActivity.alertToastLong("Network is broken. Please check network again !");
        }
    }

    public void UpdateInsertDetailByInsTime(){
        //Find PK Check box current of Inspec Time
        final LinearLayout layoutTimeNo = (LinearLayout) rootView.findViewById(R.id.layoutTimeNo);
        int curCheckBoxPK = 0;
        for (int p = 0; p < layoutTimeNo.getChildCount(); p++) {
            View v = layoutTimeNo.getChildAt(p);
            if(v instanceof  CheckBox){
                CheckBox cbTeamp    = new CheckBox(gwMActivity.getApplication());
                cbTeamp 			= (CheckBox)layoutTimeNo.getChildAt(p);
                if(cbTeamp.isChecked()){
                    curCheckBoxPK = Integer.valueOf(cbTeamp.getTag().toString().split(Tag_Time_No)[1]);
                    break;
                }
            }
        }

        //Update Check box current of Inspec Time
        InsertDetail_Image(curCheckBoxPK);

        String Para = "";
        for (Map.Entry<String,String> entry : hashMapSlipNoByInsTime.entrySet()) {
            String value = entry.getValue();
            Para+=value;
        }

        String exeBySP = Para + "|!"+ "drivtldrlgtl0001_u_04";
        //Update SP By Inspection PK
        exeBySP = exeBySP.replace(INSPECTION_PK_BY_TIME_NO, _TLG_INSPECTION_ENTRY_M_PK);
        dataDetail					=   new String [1];
        int j 						=   0;
        dataDetail[j++] 			=   exeBySP;
        QCManagementAsyncTask qc2 	=   new QCManagementAsyncTask(gwMActivity);
        qc2.execute(dataDetail);
        //update image
        UpLoadImage();
        Log.e("QC_UpdInsertByInsTime", " drivtldrlgtl0001_u_04: " + exeBySP);
    }

    //endregion

    //endregion

    //region FREZZER TABLE

    ScrollView scrollViewC;
    ScrollView scrollViewD;

    private void setScrollViewAndHorizontalScrollViewTag(){

        this.scrollViewD = (ScrollView)rootView.findViewById(R.id.scroll_tbD);
        this.scrollViewC = (ScrollView)rootView.findViewById(R.id.scroll_tbC);
        final TableLayout tbD = (TableLayout)rootView.findViewById(R.id.tbD);

        scrollViewD.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                if(tbD.getHeight() <= scrollViewD.getMeasuredHeight())
                    return;

                int scrollX = scrollViewD.getScrollX(); //for horizontalScrollView
                int scrollY = scrollViewD.getScrollY(); //for verticalScrollView
                //DO SOMETHING WITH THE SCROLL COORDINATES
                scrollViewD.scrollTo(scrollX, scrollY);
                scrollViewC.scrollTo(scrollX, scrollY);
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tvReqNo:
                onSeqNo(view);
                break;
            case R.id.tvSlipNo:
               onSlipNo(view);
                break;
            case R.id.tvSlipNoIncome:
                onSlipNoIncome(view);
                break;
            case R.id.tvDetail:
               onShowDetail(view);
                break;
            case R.id.btnQC:
                try {
                    onQCSlipNo(view);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnCapture:
              onCapture(view);
                break;
            case R.id.btnShowCapture:
               onShowCapture(view);
                break;
            case R.id.btnDelAll:
          onDelAll(view);
                break;
            case R.id.btnCancel:
              onCancel(view);
                break;
            case R.id.btnApprove:
               onApprove(view);
                break;
            default:
                break;
        }
    }

    //endregion


    /*public void UpdateDetail_Image(){
        Log.i("QC_Mana_Call Function: ", "UpdateDetail_Image()");

        String para 		= "";
        //get Table C, D
        TableLayout table_C = (TableLayout) rootView.findViewById(R.id.tbC);
        TableLayout table_D = (TableLayout) rootView.findViewById(R.id.tbD);


        //check data table
        if(table_D.getChildCount() == 0 || table_C.getChildCount() == 0) return;


        for(int i = 0; i<table_D.getChildCount(); i++){
            TableRow row_tb_C 	=(TableRow)table_C.getChildAt(i);
            TableRow row_tb_D 	=(TableRow)table_D.getChildAt(i);

            CheckBox cbSA 		= (CheckBox)row_tb_D.getChildAt(CAVITY_LIST_PK.size()+4);

            String sa 			= "";
            if(cbSA.isChecked()) sa = "1"; else sa = "0";

            para+=      user + "|" +
                    _TLG_INSPECTION_ENTRY_M_PK + "|" +

                    //p_tlg_inspect_req_d_pk
                    ((TextView)row_tb_C.getChildAt(0)).getText().toString()		+ "|" +

                    //Remark
                    ((TextView)row_tb_D.getChildAt(2)).getText().toString()		+ "|" +

                    //Cav#1
                    ((TextView)row_tb_D.getChildAt(3)).getText().toString()		+ "|" +
                    ((TextView)row_tb_D.getChildAt(4)).getText().toString()		+ "|" +
                    ((TextView)row_tb_D.getChildAt(5)).getText().toString()		+ "|" +
                    ((TextView)row_tb_D.getChildAt(6)).getText().toString()		+ "|" +
                    ((TextView)row_tb_D.getChildAt(7)).getText().toString()		+ "|" +
                    ((TextView)row_tb_D.getChildAt(8)).getText().toString()		+ "|" +
                    ((TextView)row_tb_D.getChildAt(9)).getText().toString()		+ "|" +
                    ((TextView)row_tb_D.getChildAt(10)).getText().toString()	+ "|" +
                    ((TextView)row_tb_D.getChildAt(11)).getText().toString()	+ "|" +

                    //Cav#10
                    ((TextView)row_tb_D.getChildAt(12)).getText().toString()	+ "|" +
                    //Result
                    ((TextView)row_tb_D.getChildAt(13)).getText().toString() 	+ "|" +
                    //PK
                    ((TextView)row_tb_D.getChildAt(14)).getText().toString()	+ "|" +
                    //SA
                    sa;
            para+="*|*";
        }
        para 		+= 	"|!"+ "drivtldrlgtl0001_u_04";

        dataDetail	=	new String [1];
        int j 		= 	0;
        dataDetail[j++] = para;
        QCManagementAsyncTask qc2 = new QCManagementAsyncTask(this);
        qc2.execute(dataDetail);

        UpLoadImage();
    }*/
    //endregion

    //region CLASS DATE - TIME PICKER FRAGMENT
    @SuppressLint("ValidFragment")
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {


        private String strDate;

        private void setStrDate(String strDate) {
            this.strDate = strDate;
        }

        private String getStrDate() {
            return strDate;
        }

        private boolean GetTime = false;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            if(GetTime == false){
                final Calendar c    = Calendar.getInstance();
                int year            = c.get(Calendar.YEAR);
                int month           = c.get(Calendar.MONTH);
                int day             = c.get(Calendar.DAY_OF_MONTH);

                // Create a new instance of DatePickerDialog and return it
                return new DatePickerDialog(getActivity(), this, year, month, day);
            }

            return null;
        }

        public String a;

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {

        }
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        private String strTime;

        private void setStrTime(String strTime) {
            this.strTime = strTime;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c 	= Calendar.getInstance();
            int hour 			= c.get(Calendar.HOUR_OF_DAY);
            int minute 			= c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            Calendar c = Calendar.getInstance();
            c.set(hourOfDay, minute);

            setStrTime(c.getTime().toString());
        }
    }
    private TimePickerDialog timePickDialog = null;
    private class TimePickHandler implements TimePickerDialog.OnTimeSetListener {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String date = ((TextView)rootView.findViewById(R.id.tvDate_)).getText().toString();

            String h = hourOfDay<10?"0"+String.valueOf(hourOfDay):String.valueOf(hourOfDay);
            String m = minute<10?"0"+String.valueOf(minute):String.valueOf(minute);
            strTime 	=  h + ":" + m;
            List lstDateTime = new ArrayList();
            List lstCurrent =  (List)lstInspectionTime.get(getIDCheckedCheckBox(TIME_NO));

            if(lstCurrent != null && lstCurrent.get(0) != null){
                strDate = (String) lstCurrent.get(0);
            }

            lstDateTime.add(0, strDate);
            lstDateTime.add(1, strTime);

            lstInspectionTime.put(getIDCheckedCheckBox(TIME_NO),lstDateTime);

            tvDate_Time.setText(strDate + " " + strTime);
            timePickDialog.hide();
        }
    }
    //endregion

    public void onApproveIncome() {
        //progress = new ProgressDialog(this);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                //progress.setMessage("Processing, Please Wait...");
                //progress.setCancelable(false);//not close when touch
                //progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
               // progress.setProgress(0);
               // progress.show();


            }

            @Override
            protected Void doInBackground(Void... params) {
                String para = _pkSlipNoIncome+"|"+user;
                String result = HandlerWebService.InsertTableArg("LG_MPOS_QC_APPROVE_INCOME",para);
                Log.i("QC_Mana_onApprove: ", "LG_MPOS_QC_APPROVE_INCOME  " + para);
                String[] kqResult = result.split("\\|!");
                if(result!= null && kqResult[1].equals("OK")){
                    _pkSlipNoIncome="";

                }else{
                    //alertToastShort("Approve Don't Complete");
                    Log.i("QC_Mana_onApprove: ", " Not complete");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void args) {
                super.onPreExecute();
                //progress.dismiss();
                //this.cancel(true);
                flagGetData = true;
            }
        }.execute();
    }
}