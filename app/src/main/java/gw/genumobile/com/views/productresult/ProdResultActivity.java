package gw.genumobile.com.views.productresult;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;

public class ProdResultActivity extends AppCompatActivity {
    public Spinner myLstWH;
    public EditText edWIDate , edt_Line, edt_Person_in_charge,edt_PO_No,edt_ItemCode , edt_Style  ;
    public EditText edt_Color, edt_Size,  edt_WI_Qty, edt_Prod_Qty ,edt_Balance     ;
    public Button btnGood,btn_Defect,btn_Sample;
    public List _scoreList = new ArrayList();
    String wi_pk = "",wh_name = "";
    HashMap hashMapWH = new HashMap();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prod_result);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        myLstWH     = (Spinner) findViewById(R.id.edt_WINo);
//          edWIDate= (EditText) findViewById(R.id.edt_WIDate);
//          edt_Line= (EditText) findViewById(R.id.edt_Line);
//          edt_Person_in_charge= (EditText) findViewById(R.id.edt_Person_in_charge);
//          edt_PO_No= (EditText) findViewById(R.id.edt_PO_No);
//          edt_ItemCode= (EditText) findViewById(R.id.edt_ItemCode);
//          edt_Style= (EditText) findViewById(R.id.edt_Style);
//          edt_Color= (EditText) findViewById(R.id.edt_Color);
//          edt_Size= (EditText) findViewById(R.id.edt_Size);
//          edt_WI_Qty= (EditText) findViewById(R.id.edt_WI_Qty);
//          edt_Prod_Qty= (EditText) findViewById(R.id.edt_Prod_Qty);
//           edt_Balance= (EditText) findViewById(R.id.edt_Balance);
//
//        btnGood= (Button) findViewById(R.id.btn_Good);
//        btn_Defect= (Button) findViewById(R.id.btn_Defect);
//        btn_Sample= (Button) findViewById(R.id.btn_Sample);
//        btnGood.setSelected(true);
//        //
//        InputStream inputStream = getResources().openRawResource(R.raw.product_result);
//        CSVFile csvFile = new CSVFile(inputStream);
//        List scoreList = csvFile.read();
//                  _scoreList=   scoreList;
//        LoadWH(scoreList);
    }
    private void LoadWH( List scoreList) {
//        try{
//            List<String> lstGroupName = new ArrayList<String>();
//            for (int i = 1; i < scoreList.size(); i++) {
//                String[] arr= (String[]) scoreList.get(i);
//                lstGroupName.add(arr[0]);
//                hashMapWH.put(i,arr[0]);
//            }
//
//            if(scoreList!= null && scoreList.size() > 0){
//                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
//                        android.R.layout.simple_spinner_item, lstGroupName);
//
//                // Drop down layout style - list view with radio button
//                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//                // attaching data adapter to spinner
//                myLstWH.setAdapter(dataAdapter);
//                wh_name = myLstWH.getItemAtPosition(0).toString();
//            }else {
//                myLstWH.setAdapter(null);
//                wi_pk="0";
//            }
//        }catch(Exception ex){
//          //  alertToastLong(ex.getMessage().toString());
//            Log.e("WH Error: ", ex.getMessage());
//        }
//        //-----------
//
//        // onchange spinner wh
//        myLstWH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
//                // your code here
//                wh_name = myLstWH.getItemAtPosition(i).toString();
//                Log.e("WH_name",wh_name);
//                Object pkGroup = hashMapWH.get(i);
//                if(wh_name!=null){
//                    wi_pk =wh_name;// String.valueOf(pkGroup);
//                    Log.e("wi_pk: ",wi_pk);
//                    if(!wi_pk.equals("0")){
//
//                           for (int r = 1; r < _scoreList.size(); r++) {
//                               String[] arr= (String[]) _scoreList.get(r);
//                               if( wi_pk.equals(arr[0])){
//                                   edWIDate.setText(arr[1]);
//                                   edt_Line.setText(arr[2]);
//                                     edt_Style.setText(arr[3]);
//                                     edt_ItemCode.setText(arr[4]);
//                                     edt_PO_No.setText(arr[5]);
//                                     edt_Color.setText(arr[6]);
//                                     edt_Size.setText(arr[7]);
//                                    edt_WI_Qty.setText(arr[8]);
//                                    edt_Prod_Qty.setText(arr[9]);
//                                   edt_Balance.setText(String.valueOf(Float.valueOf(arr[8])-Float.valueOf(arr[9])));
//
//                               }
//
//                           }
//
//                       //dosomehitng
//                    }
//                }else {
//                    wi_pk="0";
//                    Log.e("wi_pk: ",wi_pk);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//                // your code here
//            }
//
//        });
    }
    public void OnPro_Good(View v) throws InterruptedException,ExecutionException {

        btnGood.setSelected(true);
        btn_Defect.setSelected(false);
        btn_Sample.setSelected(false);
    }
    public void OnPro_Defect(View v) throws InterruptedException,ExecutionException {
        btnGood.setSelected(false);
        btn_Defect.setSelected(true);
        btn_Sample.setSelected(false);

    }
    public void OnPro_Simple(View v) throws InterruptedException,ExecutionException {
        btnGood.setSelected(false);
        btn_Defect.setSelected(false);
        btn_Sample.setSelected(true);

    }
}
