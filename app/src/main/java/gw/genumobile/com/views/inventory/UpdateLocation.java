package gw.genumobile.com.views.inventory;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import gw.genumobile.com.R;
import gw.genumobile.com.interfaces.ToastExp;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.ServerAsyncTask;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.utils.CNetwork;

import gw.genumobile.com.views.gwFragment;
import gw.genumobile.com.views.gwMainActivity;

public class UpdateLocation extends gwFragment {
    protected Boolean flagUpload=true;
    public static final int REQUEST_CODE = 1;  // The request code
    public static final int RESULT_CODE=1;
    private static final String formID="21";
    Queue queue = new LinkedList();
    Calendar c;
    SimpleDateFormat df;

    SQLiteDatabase db = null;
    SQLiteDatabase db2 = null;
    Cursor cursor, cursor2;
    private Handler customHandler = new Handler();
    Button _btnApprove;
    TextView recyclableTextView, _txtDate, _txtError, _txtTT, _txtSent, _txtRemain,_txtTotalGridBot,_txtTotalQtyBot,_txtTotalGridMid,_txtTime;
    EditText myBC, bc2, code, name, lotNo,edQty,edBarcod ;
    public Spinner myLstWH,myLstLoc, myLstLine;
    List<String> arrLoc;
    String sql = "", tr_date = "",scan_date="",scan_time="",unique_id="";
    String wh_pk = "",wh_name = "",wh_name_grid = "", line_pk = "",line_name="", loc_pk = "",loc_name="",approveYN="N";
    String  data[] = new String[0];
    String dlg_pk="",dlg_barcode="",dlg_qty="";
    int timeUpload=0;
    HashMap hashMapWH = new HashMap();
    HashMap hashMapLoc = new HashMap();
    View rootView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_update_location, container, false);
      //  gwActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        myBC        = (EditText) rootView.findViewById(R.id.editBC);
        myLstWH     = (Spinner) rootView.findViewById(R.id.lstWH);
        myLstLoc   = (Spinner) rootView.findViewById(R.id.lstLoc);
        //myLstLine   = (Spinner) findViewById(R.id.lstLine);

        _txtRemain   = (TextView) rootView.findViewById(R.id.txtRemain );
        _txtDate     = (TextView) rootView.findViewById(R.id.txtDate);
        _txtError  = (TextView) rootView.findViewById(R.id.txtError);
        _txtError.setText("");

        scan_date = CDate.getDateyyyyMMdd();
        String formattedDate = CDate.getDateIncline();
        _txtDate.setText(formattedDate);
        _txtTime=(TextView) rootView.findViewById(R.id.txtTime);

     /*   if(gwActivity.Measuredwidth <=600) {
            _txtDate.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(15, 0, 0, 0);
            _txtTime.setLayoutParams(params);
        }*/

        OnShowGridHeader();
        OnShowScanLog();
        //OnShowScanIn();
        OnShowScanAccept();
        CountSendRecord();
        LoadWH();

        myBC.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if(wh_pk.equals("0") ){
                            myBC.getText().clear();
                            myBC.requestFocus();

                            _txtError.setTextColor(Color.RED);
                            _txtError.setText("Warehouse was wrong!!");
                            ((gwMainActivity) gwActivity).alertToastShort("Warehouse was wrong. Plz check Wifi !!!");

                        }else if(loc_pk.equals("0")){
                            myBC.getText().clear();
                            myBC.requestFocus();

                            _txtError.setTextColor(Color.RED);
                            _txtError.setText("Warehouse was wrong!!");
                            ((gwMainActivity) gwActivity).alertToastShort("Warehouse was wrong. Plz mapping Wh Location !!!");
                        }else{
                            OnSaveBC();
                        }
                    }
                    return true;//important for event onKeyDown
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // this is for backspace
                    myBC.clearFocus();
                   // Thread.interrupted();
                    // System.exit(0);
                  //  gwMActivity.BackPressed();
                    //Log.e("IME_TEST", "BACK VIRTUAL");
                }
                return false;
            }
        });

        timeUpload= hp.GetTimeAsyncData();

        _txtTime.setText("Time: " + timeUpload + "s");
        customHandler.postDelayed(updateDataToServer, timeUpload*1000);
        // Inflate the layout for this fragment
        return rootView;
    }

    private Runnable updateDataToServer = new Runnable() {
        public void run()
        {
            Log.e("flagUpload",flagUpload.toString());
            if(flagUpload) {
                doStart();
                Log.e("flagUpload1",flagUpload.toString());
            }
            SystemClock.sleep(100);
            customHandler.postDelayed(this, timeUpload*1000);
        }
    };

    private void doStart() {
        flagUpload=false;
        db2=gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
        cursor2 = db2.rawQuery("select PK, ITEM_BC,TR_WH_IN_PK,TLG_IN_WHLOC_IN_PK  from INV_TR where DEL_IF=0 and TR_TYPE='"+formID+"' and sent_yn = 'N' order by PK asc LIMIT 20",null);
        if (cursor2.moveToFirst()) {

            // Write your code here to invoke YES event
            System.out.print("\n\n**********doStart********\n\n\n");
            //set value message
            _txtError=(TextView)gwActivity.findViewById(R.id.txtError);
            _txtError.setTextColor(Color.RED);
            _txtError.setText("");

            //  data=new String [cursor2.getCount()];
            data=new String [1];
            int j=0;
            String para ="";
            boolean flag=false;
            do {
                flag=true;
                for(int i=0; i < cursor2.getColumnCount();i++)
                {
                    if(para.length() <= 0)
                    {
                        if(cursor2.getString(i)!= null)
                            para += cursor2.getString(i)+"|";
                        else
                            para += "|";
                    }
                    else
                    {

                        if(flag==true){
                            if(cursor2.getString(i)!= null) {
                                para += cursor2.getString(i);
                                flag=false;
                            }
                            else {
                                para += "|";
                                flag=false;
                            }
                        }
                        else{
                            if(cursor2.getString(i)!= null)
                                para += "|" + cursor2.getString(i);
                            else
                                para += "|";
                        }
                    }
                }
                para += "|" + deviceID;
                para += "|" + bsUserID;
                para += "*|*";
            } while (cursor2.moveToNext());
            //////////////////////////
            para += "|!"+ "LG_MPOS_UPL_UPDATE_LOC";
            data[j++]=para;
            System.out.print("\n\n\n para upload: " + para);
            Log.e("para upload: ", para);

            if (CNetwork.isInternet(tmpIP, checkIP)) {
                ServerAsyncTask task = new ServerAsyncTask(gwMActivity,this);
                task.execute(data);
               gwMActivity.alertToastShort("Send to Server");
            } else
            {
                flagUpload=true;
                gwMActivity.alertToastLong("Network is broken. Please check network again !");
            }
        }
        else{
            flagUpload=true;
        }
        cursor2.close();
        db2.close();
    }
    ////////////////////////////////////////////////
    public void OnSaveBC(){
        if (myBC.getText().toString().equals("")) {
            _txtError.setTextColor(Color.RED);
            _txtError.setText("Pls Scan barcode!");
            myBC.getText().clear();
            myBC.requestFocus();
            return;
        }
        else{

            String str_scanBC = myBC.getText().toString().toUpperCase();

            try
            {
                if(str_scanBC.length()>20) {
                    _txtError.setText("Barcode has length more 20 char !");
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                }

                boolean isExists= hp.isExistBarcode(str_scanBC, formID);// check barcode exist in data
                if (isExists)// exist data
                {
                    alertRingMedia();
                    _txtError.setText("Barcode exist in database!");
                    myBC.getText().clear();
                    myBC.requestFocus();
                    return;
                }
                else
                {
                    if(str_scanBC.indexOf("L")==0)
                    {
                        String str_loc=str_scanBC.substring(1, str_scanBC.length());
                        if(arrLoc.indexOf(str_loc)< 0){
                            alertRingMedia2();
                           gwMActivity.onPopAlert("Location không hợp lệ !!!");
                        }else {
                            myLstLoc.setSelection(arrLoc.indexOf(str_loc));
                        }
                    }else {
                        db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                        scan_date = CDate.getDateyyyyMMdd();
                        scan_time = CDate.getDateYMDHHmmss();

                        db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK, TLG_IN_WHLOC_IN_NAME,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,TR_TYPE) "
                                + "VALUES('"
                                + str_scanBC + "',"
                                + wh_pk + ",'"
                                + wh_name + "','"
                                + loc_pk + "','"
                                + loc_name + "','"

                                + scan_date + "','"
                                + scan_time + "','"
                                + "N" + "','"
                                + " " + "','"
                                + formID + "');");
                    }
                }
            }
            catch (Exception ex)
            {
                _txtError.setText("Save Error!!!");
                Log.e("OnSaveBC", ex.getMessage());
            }
            finally {
                db.close();
                myBC.getText().clear();
                myBC.requestFocus();
            }
        }
        OnShowScanLog();
        CountSendRecord();
    }
    ////////////////////////////////////////////////
    public void onDelAll(View view){
        // if(!checkRecordGridView(R.id.grdScanAccept) && !checkRecordGridView(R.id.grdScanIn)) return;
        if(!hp.isCheckDelete(formID)) return;
        String title="Confirm Delete..";
        String mess="Are you sure you want to delete ???";
        alertDialogYN(title,mess,"onDelAll");
    }

    public void  ProcessDelete(){
        onDeleteAllFinish();
    }

    public  void onDeleteAllFinish(){
        try{
            db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                db.execSQL("DELETE FROM INV_TR where  TR_TYPE ='"+formID+"';");

        }catch (Exception ex){
            Log.e("Error Delete All :", ex.getMessage());
        }
        finally {
            db.close();
            OnShowScanLog();
            //OnShowScanIn();
            OnShowScanAccept();
        }
    }

    public void alertDialogYN(String title,String mess,final String _type){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke YES event

                if(_type.equals("onDelAll")){
                    ProcessDelete();
                }

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    ////////////////////////////////////////////////
    private void LoadWH() {
        String l_para = "1,LG_MPOS_M010_GET_WH_USER," +  bsUserPK+"|wh_ord_2";
        try{
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String dataWHGroup[][] = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();

            for (int i = 0; i < dataWHGroup.length; i++) {
                lstGroupName.add(dataWHGroup[i][1].toString());
                hashMapWH.put(i, dataWHGroup[i][0]);
            }

            if(dataWHGroup!= null && dataWHGroup.length > 0){
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                myLstWH.setAdapter(dataAdapter);
                wh_name = myLstWH.getItemAtPosition(0).toString();
            }else {
                myLstWH.setAdapter(null);
                wh_pk="0";
            }
        }catch(Exception ex){
            gwMActivity.alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
        //-----------

        // onchange spinner wh
        myLstWH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                wh_name = myLstWH.getItemAtPosition(i).toString();
                Log.e("WH_name",wh_name);
                Object pkGroup = hashMapWH.get(i);
                if(pkGroup!=null){
                    wh_pk = String.valueOf(pkGroup);
                    Log.e("WH_PK: ",wh_pk);
                    if(!wh_pk.equals("0")){
                        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {
                            TextView tvLoc =(TextView)rootView.findViewById(R.id.tvLoc);
                            tvLoc.setVisibility(View.VISIBLE);
                            myLstLoc.setVisibility(View.VISIBLE);
                        }
                        LoadLocation();
                    }
                }else {
                    wh_pk="0";
                    Log.e("WH_PK: ",wh_pk);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    ////////////////////////////////////////////////


    ////////////////////////////////////////////////
    private void LoadLocation() {
        if (appPrefs.getBoolean("enableLocYN", Boolean.FALSE)) {

            String   l_para = "1,LG_MPOS_GET_LOC," + wh_pk;
            try{
                gwService networkThread = new gwService(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
                JResultData dtLocGroup = networkThread.execute(l_para).get();
                List<String> lstLocName = new ArrayList<String>();
                List<JDataTable> jdt = dtLocGroup.getListODataTable();
                JDataTable dt=jdt.get(0);

                for (int i = 0; i < jdt.size(); i++) {

                    lstLocName.add(jdt.get(i) .toString());
                 //   hashMapLoc.put(i, dtLocGroup[i][0]);
                }


                /*
                ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
                String dtLocGroup[][] = networkThread.execute(l_para).get();
                List<String> lstLocName = new ArrayList<String>();

                for (int i = 0; i < dtLocGroup.length; i++) {
                    lstLocName.add(dtLocGroup[i][1].toString());
                    hashMapLoc.put(i, dtLocGroup[i][0]);
                }

                if(dtLocGroup!= null && dtLocGroup.length > 0){
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                            android.R.layout.simple_spinner_item, lstLocName);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    myLstLoc.setAdapter(dataAdapter);
                    loc_name = myLstLoc.getItemAtPosition(0).toString();
                }else {
                    myLstLoc.setAdapter(null);
                    loc_pk="0";
                    Log.e("Loc_PK: ",loc_pk);
                }
                */
            }catch(Exception ex){
                gwMActivity.alertToastLong(ex.getMessage().toString());
                Log.e("Loc Error: ", ex.getMessage());
            }
            //-----------
            // onchange spinner LOC
            myLstLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView,
                                           View selectedItemView, int i, long id) {
                    // your code here
                    loc_name = myLstLoc.getItemAtPosition(i).toString();
                    Log.e("Loc_name",loc_name);
                    Object pkGroup = hashMapLoc.get(i);
                    if(pkGroup!=null){
                        loc_pk = String.valueOf(pkGroup);
                        Log.e("Loc_PK: ",loc_pk);

                    }else {
                        loc_pk="0";
                        Log.e("Loc_PK: ",loc_pk);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }
            });



        }
        else{
            TextView tvLoc =(TextView)rootView.findViewById(R.id.tvLoc);
            tvLoc.setVisibility(View.INVISIBLE);
            myLstLoc.setVisibility(View.INVISIBLE);
        }

    }
    public void OnShowGridHeader()// show gridview Header
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout)rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), scrollableColumnWidths[4], fixedHeaderHeight));
        scrollablePart.addView(row);



        // Accept Scan
        scrollablePart = (TableLayout)rootView.findViewById(R.id.grdData3);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[1], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemBC), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("Location", scrollableColumnWidths[3], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvWhName), scrollableColumnWidths[4], fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvStatus), scrollableColumnWidths[2], fixedHeaderHeight));

        scrollablePart.addView(row);
    }

    // show data scan log
    public void OnShowScanLog()
    {
        try {

            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout)rootView.findViewById(R.id.grdScanLog);
            scrollablePart.removeAllViews();//remove all view child

            db =gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            cursor = db.rawQuery("SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+formID+"' and SENT_YN='N' order by PK desc LIMIT 20",null);// TLG_LABEL

            int count = cursor.getCount();

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);

                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), scrollableColumnWidths[4], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db =gwActivity.openOrCreateDatabase("gasp",gwMActivity.MODE_PRIVATE, null);
            sql = "SELECT PK,ITEM_BC,SCAN_TIME FROM INV_TR where TR_TYPE='"+formID+"' and SENT_YN='N' ; ";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtRemain=(TextView) rootView.findViewById(R.id.txtRemain);
            _txtRemain.setText("Re: " + count);

        } catch (Exception ex) {
            gwMActivity.alertToastLong(ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan in
    public void OnShowScanIn()
    {
        try {
            int date_previous=Integer.parseInt(CDate.getDatePrevious(2));
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout)rootView.findViewById(R.id.grdScanIn);
            scrollablePart.removeAllViews();//remove all view child

            db =gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            sql = "select ITEM_BC, ITEM_CODE,  STATUS, SLIP_NO, INCOME_DATE, CHARGER, TR_WH_IN_NAME, LINE_NAME, TLG_POP_INV_TR_PK " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SENT_YN='Y' AND SCAN_DATE > '" + date_previous + "' AND STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk desc LIMIT 10 ";


            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    String seq= String.valueOf(count-i);
                    row.addView(makeTableColWithText(seq, scrollableColumnWidths[1], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SLIP_NO")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("INCOME_DATE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("CHARGER")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[5], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("LINE_NAME")), scrollableColumnWidths[5], fixedRowHeight, -1));

                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            //total Grid Scan in
            db =gwActivity.openOrCreateDatabase("gasp",gwActivity.MODE_PRIVATE, null);
            sql = "select pk FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE > '" + date_previous + "' AND TR_TYPE = '"+formID+"' AND SENT_YN = 'Y' AND STATUS NOT IN('000', ' ');";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();

            _txtTotalGridMid=(TextView) rootView.findViewById(R.id.txtTotalMid);
            _txtTotalGridMid.setText("Total: " + count);
            _txtTotalGridMid.setTextColor(Color.BLUE);
            _txtTotalGridMid.setTextSize((float) 17.0);
        } catch (Exception ex) {
           gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    // show data scan accept
    public void OnShowScanAccept()
    {
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdScanAccept);
            scrollablePart.removeAllViews();//remove all view child

            db =gwActivity.openOrCreateDatabase("gasp",gwActivity.MODE_PRIVATE, null);
            sql = "  SELECT PK,ITEM_BC, TR_ITEM_PK,  ITEM_CODE,TR_QTY, TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK,TLG_IN_WHLOC_IN_NAME,STATUS  " +
                    " FROM INV_TR  WHERE DEL_IF = 0 AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND TR_TYPE = '"+formID+"' AND STATUS IN('001', '000') "+
                    " ORDER BY PK desc  LIMIT 30 ";

            cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();

            queue.clear();
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")), scrollableColumnWidths[3], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_QTY")), scrollableColumnWidths[2], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_LOT_NO")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_NAME")), scrollableColumnWidths[3], fixedRowHeight,0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")), scrollableColumnWidths[4], fixedRowHeight,-1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("STATUS")), scrollableColumnWidths[2], fixedRowHeight,-1));


                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }

            db =gwActivity.openOrCreateDatabase("gasp",gwActivity.MODE_PRIVATE, null);
            sql = "select pk,TR_QTY FROM INV_TR  WHERE DEL_IF = 0 AND TR_TYPE = '"+formID+"' AND SCAN_DATE = '" + scan_date + "' AND SENT_YN = 'Y' AND STATUS='000'";
            cursor = db.rawQuery(sql, null);
            count = cursor.getCount();
            float _qty=0;
            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    _qty=_qty+ Float.parseFloat(cursor.getString(cursor.getColumnIndex("TR_QTY")));
                    cursor.moveToNext();
                }
            }
            _txtTotalGridBot=(TextView)rootView.findViewById(R.id.txtTotalBot);
            _txtTotalGridBot.setText("Total: " + count+ " ");
            _txtTotalGridBot.setTextColor(Color.BLUE);
            _txtTotalGridBot.setTextSize((float) 16.0);

            _txtTotalQtyBot=(TextView)rootView.findViewById(R.id.txtTotalQtyBot);
            _txtTotalQtyBot.setText("Qty: " + String.format("%.02f", _qty)+ " ");
            _txtTotalQtyBot.setTextColor(Color.BLACK);
            _txtTotalQtyBot.setTextSize((float) 16.0);

        } catch (Exception ex) {
           gwMActivity.alertToastLong("GridScanAccept: " + ex.getMessage());
            Log.e("GridScanAccept: ", ex.getMessage());
        } finally {
            db.close();
            cursor.close();
        }
    }

    public void CountSendRecord()
    {
        flagUpload=true;
        // total scan log
        _txtSent=(TextView)rootView.findViewById(R.id.txtSent);
        _txtTotalGridBot=(TextView)rootView.findViewById(R.id.txtTotalBot);
        int countBot=Integer.parseInt(_txtTotalGridBot.getText().toString().replaceAll("[\\D]", ""));
        ///int k=Integer.parseInt("1h2el3lo".replaceAll("[\\D]",""));

        _txtSent.setText("Send: "+(countBot));

        _txtRemain=(TextView)rootView.findViewById(R.id.txtRemain);
        int countRe = Integer.valueOf(_txtRemain.getText().toString().replaceAll("[\\D]",""));

        _txtTT=(TextView) rootView.findViewById(R.id.txtTT);
        _txtTT.setText("TT: " + ( countBot + countRe));

    }
}
