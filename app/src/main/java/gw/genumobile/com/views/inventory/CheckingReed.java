package gw.genumobile.com.views.inventory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import gw.genumobile.com.R;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.views.gwFragment;

public class CheckingReed extends gwFragment {

    private Handler handler = new Handler();
    private Handler handlerUI = new Handler();
    private Button btnClear;
    private EditText etBarcode ;
    private EditText etCurBarcode ;
    private EditText etWINO ;
    private EditText etReedID ;
    View rootView;

    private String wi_pk = "";
    private String wi_no = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_checking_reed, container, false);

        btnClear = (Button) rootView.findViewById(R.id.btnClear);
        etBarcode = (EditText) rootView.findViewById(R.id.ed_BC);
        etCurBarcode = (EditText) rootView.findViewById(R.id.ed_CurrentBC);
        etWINO = (EditText) rootView.findViewById(R.id.ed_WINO);
        etReedID = (EditText) rootView.findViewById(R.id.ed_REEDID);

        OnShowGW_Header();
        OnShowChild();
        doClear();
//        OnShowScanLog();
//        OnShowScanIn();
//        OnShowScanAccept();
//        CountSendRecord();
//        LoadWH();
//        LoadLine();
//        OnPermissionApprove();
//        init_color();
//
//
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = "Confirm Delete...";
                String mess = "Are you sure you want to delete all";
                alertDialogYN(title, mess, new alertDialogCallBack() {
                    @Override
                    public void onConfirm(boolean result) {
                        if(result){
                            doClear();
                        }
                    }
                });
            }
        });

        etBarcode.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        String barcode = etBarcode.getText().toString();
                        etBarcode.getText().clear();
                        etBarcode.requestFocus();

                        if(!barcode.equals("")){
                            OnSaveBC(barcode);
                        }

                    }
                    return true;//important for event onKeyDown
                }

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    etBarcode.getText().clear();
                    etBarcode.requestFocus();
                    return true;

                }


                return false;
            }
        });




        handler.postDelayed(updateDataToServer, hp.GetTimeAsyncData() * 1000);

        return rootView;
    }
    private boolean flagUpload = false;

    private Runnable updateDataToServer = new Runnable() {
        public void run() {
            handler.postDelayed(this, hp.GetTimeAsyncData() * 1000);
            doStart();
        }
    };

    private void doStart() {
        if(flagUpload) return;

        if (wi_pk.equals("")) return ;

        flagUpload = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                SQLiteDatabase db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

                //REED BARCODE  , WI_PK     , WI_NO         , REED ID PK        , REED NAME             , WI/REED 0/1
                //id       ,TR_WH_IN_PK,TR_WH_IN_NAME  ,TLG_IN_WHLOC_IN_PK , TLG_IN_WHLOC_IN_NAME  ,TR_LINE_PK,SCAN_DATE,SCAN_TIME,STATUS,TR_TYPE

                String sql = "select PK,ITEM_BC       ,TR_WH_IN_PK,TR_WH_IN_NAME  ,TLG_IN_WHLOC_IN_PK , TLG_IN_WHLOC_IN_NAME  ,TR_LINE_PK,SCAN_DATE,SCAN_TIME,STATUS,TR_TYPE, MAPPING_YN " +
                        " FROM INV_TR  " +
                        " WHERE DEL_IF = 0  AND MAPPING_YN is null  AND TR_TYPE='" + formID + "' AND TR_LINE_PK='1' " +
                        " ORDER BY PK asc";

                Cursor cursor = db.rawQuery(sql, null);
                int count = cursor.getCount();
                // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

                if (cursor != null && cursor.moveToFirst()) {
                    for (int i = 0; i < count; i++) {

                        try{
                            String wi_pk = cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")) ;
                            String reed_id = cursor.getString(cursor.getColumnIndex("ITEM_BC")) ;
                            String pk  = cursor.getString(cursor.getColumnIndex("PK")) ;
                            String scan_date  = cursor.getString(cursor.getColumnIndex("SCAN_DATE")) ;

                            String scan_time  = cursor.getString(cursor.getColumnIndex("SCAN_TIME")) ;

                            JDataTable dt = GetDataOnServer1("LG_MPOS_CHECKING_REED_ID", wi_pk+"|"+ reed_id+"|"+ scan_date+"|"+scan_time+"|"+pk+"|"+bsUserID +"|"+deviceID);
                            if(dt==null || dt.totalrows <=0) {

                            }else {

                                String cb_pk = dt.records.get(0).get("local_pk").toString();
                                String cb_asset_pk = dt.records.get(0).get("tlg_ma_asset_pk").toString();
                                String cb_asset_nm = dt.records.get(0).get("tlg_ma_asset_nm").toString();
                                String cb_mat = dt.records.get(0).get("matched_yn").toString();
                                SQLiteDatabase db2 = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                                String sql2 = "UPDATE INV_TR set TLG_IN_WHLOC_IN_PK='" + cb_asset_pk + "', " +
                                                    "TLG_IN_WHLOC_IN_NAME='" + cb_asset_nm + "', " +
                                                    "MAPPING_YN='" + cb_mat + "' " +

                                        "  where PK  = '" + cb_pk  +"'";
                                db2.execSQL(sql2);

                                db2.close();
                            }
                        }catch (Exception eRow){}







                        cursor.moveToNext();
                    }
                }
                db.close();
                cursor.close();
                handlerUI.post(new Runnable() {
                    @Override
                    public void run() {
                        OnShowChild();
                    }
                });
                flagUpload = false;
            }
        }).start();

    }

    public void OnShowGW_Header()// show data gridview
    {
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] fixedColumnWidths = new int[]{20, 20, 20, 20, 20, 20};
        //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

        TableRow row = new TableRow(gwMActivity);
        TableLayout scrollablePart;

        // Log Scan
        scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1);
        row = new TableRow(gwMActivity);
        row.setLayoutParams(wrapWrapTableRowParams);
        row.setGravity(Gravity.CENTER);
        row.setBackgroundColor(bsColors[1]);
        row.setVerticalGravity(50);

        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), 10, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("REED ID", 40, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("REED NAME", 40, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvTimeScan), 30, fixedHeaderHeight));
        row.addView(makeTableColHeaderWithText("MATCHED", 20, fixedHeaderHeight));

        scrollablePart.addView(row);


    }

    private boolean onUpdateRow = false;

    public void OnShowChild() {

        if (onUpdateRow) return ;

        onUpdateRow = true;
        try {
            TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

            //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

            TableRow row = new TableRow(gwMActivity);
            TableLayout scrollablePart = (TableLayout) rootView.findViewById(R.id.grdData1Rows);
            scrollablePart.removeAllViews();//remove all view child
            //REED BARCODE  , WI_PK     , WI_NO         , REED ID PK        , REED NAME             , WI/REED 0/1
            //ITEM_BC       ,TR_WH_IN_PK,TR_WH_IN_NAME  ,TLG_IN_WHLOC_IN_PK , TLG_IN_WHLOC_IN_NAME  ,TR_LINE_PK,SCAN_DATE,SCAN_TIME,STATUS,TR_TYPE
            SQLiteDatabase db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            String sql = "select PK,ITEM_BC       ,TR_WH_IN_PK,TR_WH_IN_NAME  ,TLG_IN_WHLOC_IN_PK , TLG_IN_WHLOC_IN_NAME  ,TR_LINE_PK,SCAN_DATE,SCAN_TIME,STATUS,TR_TYPE, MAPPING_YN " +
                    " FROM INV_TR  " +
                    " WHERE DEL_IF = 0   AND TR_TYPE='" + formID + "' AND TR_LINE_PK='1' " +
                    " ORDER BY PK  asc";

            Cursor cursor = db.rawQuery(sql, null);
            int count = cursor.getCount();
            // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5

            if (cursor != null && cursor.moveToFirst()) {
                for (int i = 0; i < count; i++) {
                    row = new TableRow(gwMActivity);
                    row.setLayoutParams(wrapWrapTableRowParams);
                    row.setGravity(Gravity.CENTER);
                    row.setBackgroundColor(Color.LTGRAY);
                    //  String seq = String.valueOf(count - i);
                    String seq = String.valueOf(i+1);
                    row.addView(makeTableColWithText(seq, 10, fixedRowHeight, 1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("ITEM_BC")), 40, fixedRowHeight, 0));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_NAME")), 40, fixedRowHeight, -1));
                    row.addView(makeTableColWithText(cursor.getString(cursor.getColumnIndex("SCAN_TIME")), 30, fixedRowHeight, -1));

                    if(cursor.getString(cursor.getColumnIndex("MAPPING_YN")) == null){
                        row.addView(makeTableColWithText("-", 20, fixedRowHeight, 0));

                    }else if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("Y")) {
                        row.addView(makeTableColWithText("YES", 20, fixedRowHeight, 0));
                        row.setBackgroundColor(Color.BLUE);
                    }
                    else if (cursor.getString(cursor.getColumnIndex("MAPPING_YN")).equals("N")) {
                        row.addView(makeTableColWithText("NO", 20, fixedRowHeight, 0));
                        row.setBackgroundColor(Color.RED);
                    }
                    else {
                        row.addView(makeTableColWithText("-", 20, fixedRowHeight, 0));

                    }


                    scrollablePart.addView(row);
                    cursor.moveToNext();
                }
            }
            db.close();
            cursor.close();

        } catch (Exception ex) {
            gwMActivity.alertToastLong("GridScanIn: " + ex.getMessage());
        } finally {

        }


        onUpdateRow = false;

    }

    public void OnSaveBC(String barcode){
        String strBC = barcode.toUpperCase();
        if(wi_pk.equals("")){
            //get wi info
            try{
                JDataTable dt = GetDataOnServer1("LG_MPOS_CHECKING_REED_WI", strBC+"|"+deviceID);
                if(dt==null || dt.totalrows <=0) {
                    alertRingMedia2();

                    return;
                }else{
                   //dt.records.get(0).
                    String wno  = dt.records.get(0).get("wi_no").toString();
                    String pk = dt.records.get(0).get("pk").toString();
                    String ma_pk  = dt.records.get(0).get("tlg_ma_asset_pk").toString();
                    String asset_nm  = dt.records.get(0).get("asset_nm").toString();

                    String scan_date = CDate.getDateyyyyMMdd();
                    String scan_time = CDate.getDateYMDHHmmss();

                    //TAN DUNG TABLET CU
                    SQLiteDatabase db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
                    //WI_PK, WI_PK, WI_NO, REED ID PK, REED NAME, WI/REED 0/1
                    db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK, TLG_IN_WHLOC_IN_NAME,TR_LINE_PK,SCAN_DATE,SCAN_TIME,STATUS,TR_TYPE) "
                            + "VALUES('"
                            + strBC + "','"
                            + pk + "','"
                            + wno + "','"
                            + ma_pk + "','"
                            + asset_nm + "','"
                            + 0 + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "N" + "','"
                            + formID + "');");
                    db.close();

                    wi_pk = pk;
                    wi_no = wno;
                    etCurBarcode.setText(pk);
                    etWINO.setText(wi_no);
                    etReedID.setText(asset_nm );
                }
            }catch (Exception eWI){
                alertRingMedia2();
            };


        }else{

            String scan_date = CDate.getDateyyyyMMdd();
            String scan_time = CDate.getDateYMDHHmmss();
            //TAN DUNG TABLET CU
            SQLiteDatabase db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);
            //WI_PK, WI_PK, WI_NO, REED ID PK, REED NAME, WI/REED 0/1
            db.execSQL("INSERT INTO INV_TR(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,TLG_IN_WHLOC_IN_PK, TLG_IN_WHLOC_IN_NAME,TR_LINE_PK,SCAN_DATE,SCAN_TIME,STATUS,TR_TYPE) "
                    + "VALUES('"
                    + strBC + "','"
                    + wi_pk + "','"
                    + wi_no + "','"
                    + "" + "','"
                    + "" + "','"
                    + 1 + "','"
                    + scan_date + "','"
                    + scan_time + "','"
                    + "N" + "','"
                    + formID + "');");
            db.close();

            OnShowChild();
        }

        // test

    }

    public void  doClear(){
        wi_pk = "";
        wi_no = "";
        etCurBarcode.setText(wi_pk);
        etWINO.setText(wi_no);
        etReedID.setText("");

        try {
            SQLiteDatabase db = gwActivity.openOrCreateDatabase("gasp", gwActivity.MODE_PRIVATE, null);

            db.execSQL("DELETE FROM INV_TR where TR_TYPE ='" + formID + "';");
            db.close();

        } catch (Exception ex) {
            Log.e("Error Delete All :", ex.getMessage());
        } finally {

            OnShowChild();
            gwMActivity.alertToastShort("Delete success..");
        }
    }

    interface alertDialogCallBack {

        void onConfirm (boolean result);
    }

    public void alertDialogYN(String title, String mess, final alertDialogCallBack callback  ) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(gwMActivity);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(mess);
        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.cfm_save);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                callback.onConfirm(true);

            }
        });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                callback.onConfirm(false);
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public JDataTable GetDataOnServer1(String proc, String para) {
        JDataTable dt = null;

        try {
            // JResultData jresultkq = HandlerWebService.GetData(proc, 1, para);

            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            String l_para = "1,"+proc+",1," + para ;
            JResultData jresultkq =     networkThread.execute(l_para).get();
            if(jresultkq ==null)
            {   gwMActivity.alertToastLong("Pls check internet!!" + "- " + para);
                return  null;
            }
            if (jresultkq.getTotals() > 0) {
                List<JDataTable> jdt = jresultkq.getListODataTable();
                dt = jdt.get(0);
            }
        } catch (Exception ex) {
            gwMActivity.alertToastLong(proc + ": " + ex.getMessage());
        }
        return  dt;
    }
}
