package gw.genumobile.com.views.mold;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.Queue;

import gw.genumobile.com.views.gwcore.BaseGwActive;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.R;

/** Created by HNDGiang on 08/01/2016. **/

/*
*   Load Detail With: PK, ITEM CODE
*   Update: 09/01/2016
*   Author: HNDGiang
*/
public class Mold_Status_Popup extends BaseGwActive {

    //region Argument



    protected SharedPreferences appPrefs;

    public static final int REQUEST_CODE = 1;
    String pkPartner = "", custID = "", custName = "";
    String p_company_pk = "", p_lg_partner_type = "", p_customer = "", p_chkar_yn = "Y", p_chkap_yn = "Y";

    int indexPrevious = -1;

    Queue queue = new LinkedList();
    int orangeColor = Color.rgb(255,99,71);
    //endregion

    //region Application Circle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mold_status_popup);

        appPrefs = this.getSharedPreferences("myConfig", this.MODE_PRIVATE);

        //region Set Screen Size
        int Measuredheight = 0;
        Point size = new Point();
        WindowManager w = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
            w.getDefaultDisplay().getSize(size);
            Measuredheight = size.y;

            final LinearLayout root = (LinearLayout) findViewById(R.id.Asset_Search);
            FrameLayout.LayoutParams rootParams = (FrameLayout.LayoutParams)root.getLayoutParams();
            rootParams.height = Measuredheight*50/100;
            rootParams.width = size.x;
            System.out.println("H: " + rootParams.height + "   W: " + rootParams.width);

        }
        //endregion

        CreatGridHeader();
        //ShowAll();

        //endregion


    }
    //endregion

    private void CreatGridHeader(){
        //Init varible
        TableRow.LayoutParams trParam = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        TableRow tRow = new TableRow(this);
        TableLayout tLayout;
        int height = 30;

        //Design Grid
        tLayout = (TableLayout)findViewById(R.id.grdHeader);
        tRow = new TableRow(this);
        tRow.setLayoutParams(trParam);
        tRow.setGravity(Gravity.CENTER);
        tRow.setBackgroundColor(bsColors[1]);
        tRow.setVerticalGravity(50);

        tRow.addView(makeTableColHeaderWithText("SEQ", scrollableColumnWidths[1], height));
        tRow.addView(makeTableColHeaderWithText("Column_1", scrollableColumnWidths[3], height));
        tRow.addView(makeTableColHeaderWithText("Column_2", scrollableColumnWidths[4], height));
        tRow.addView(makeTableColHeaderWithText("F Column_3", scrollableColumnWidths[4], height));
        tRow.addView(makeTableColHeaderWithText("F Column_4", scrollableColumnWidths[4], height));
        tRow.addView(makeTableColHeaderWithText("Column_5", scrollableColumnWidths[5], height));

        tLayout.addView(tRow);
    }

    public void onChoose(View view){
        Intent intent = getIntent();
        Bundle bundle = new Bundle();

        bundle.putString("PK", pkPartner);
        bundle.putString("ID", custID);
        bundle.putString("NAME", custName);

        intent.putExtra("ResultPartner", bundle);

        setResult(2, intent);
        finish();
    }

    public void ShowAll(){

        String para = p_company_pk + "|" + p_lg_partner_type + "|"+p_customer +"|"+p_chkar_yn+"|"+p_chkap_yn;
        String l_para = "1,LG_ASSET_SEARCH_SUPPLIER," + para;

        try{
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String resultSearch[][] = networkThread.execute(l_para).get();

            TableLayout scrollablePart = (TableLayout) findViewById(R.id.grdSupplierInfo);
            scrollablePart.removeAllViews();

            if(resultSearch.length > 0){
                ShowResultSearch(resultSearch, scrollablePart);
            }
        }catch(Exception ex){
            Log.e("Searh Error: ", ex.getMessage());
        }
    }

    public void onSearch(View view){

        String infoSearch = ((EditText) findViewById(R.id.etInfoSupplier)).getText().toString();


        p_lg_partner_type = infoSearch;
        String para = p_company_pk + "|" + p_lg_partner_type + "|"+p_customer +"|"+p_chkar_yn+"|"+p_chkap_yn;

        String l_para = "";
        l_para = "1,LG_ASSET_SEARCH_SUPPLIER," + para;

        try{
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String resultSearch[][] = networkThread.execute(l_para).get();

            TableLayout scrollablePart = (TableLayout) findViewById(R.id.grdSupplierInfo);
            scrollablePart.removeAllViews();

            if(resultSearch.length > 0){
                ShowResultSearch(resultSearch, scrollablePart);
            }
        }catch(Exception ex){
            Log.e("Searh Error: ", ex.getMessage());
        }
    }

    public void ShowResultSearch(String result[][], final TableLayout scrollablePart){
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        //int[] scrollableColumnWidths = new int[]{5, 10, 15, 20, 30, 40};

        TableRow row = new TableRow(this);
        //remove all view child
        int count = result.length;
        for(int i = 0;i < result.length; i++){
            ///// TODO: 2016-02-04 ADD ROWS TO GRID
            row = new TableRow(this);
            row.setLayoutParams(wrapWrapTableRowParams);
            row.setGravity(Gravity.CENTER);
            row.setBackgroundColor(Color.LTGRAY);
            row.addView(makeTableColWithText(result[i][0], 0, fixedRowHeight, -1));
            row.addView(makeTableColWithText(String.valueOf(count - i), scrollableColumnWidths[1], fixedRowHeight, 0));
            row.addView(makeTableColWithText(result[i][1], scrollableColumnWidths[3], fixedRowHeight, -1));
            row.addView(makeTableColWithText(result[i][2], scrollableColumnWidths[4], fixedRowHeight, -1));
            row.addView(makeTableColWithText(result[i][3], scrollableColumnWidths[4], fixedRowHeight, -1));
            row.addView(makeTableColWithText(result[i][4], scrollableColumnWidths[4], fixedRowHeight, -1));
            row.addView(makeTableColWithText(result[i][5], scrollableColumnWidths[5], fixedRowHeight, -1));


            row.setOnClickListener(new View.OnClickListener() {
                TextView tvTeamp;
                TextView tvCustId;
                TextView tvCustName;

                @Override
                public void onClick(View v) {


                    TableRow tr1 = (TableRow) v;
                    tvTeamp = (TextView) tr1.getChildAt(0); //SLIP_NO
                    tvCustId = (TextView) tr1.getChildAt(2);
                    tvCustName = (TextView) tr1.getChildAt(3);

                    pkPartner = tvTeamp.getText().toString();
                    custID = tvCustId.getText().toString();
                    custName = tvCustName.getText().toString();

                    for (int i = 0; i < tr1.getChildCount(); i++) {
                        tvTeamp = (TextView) tr1.getChildAt(i);
                        tvTeamp.setTextColor(orangeColor);
                    }

                    if (indexPrevious >= 0
                            && indexPrevious != scrollablePart.indexOfChild(v)) {
                        TableRow trP = (TableRow) scrollablePart.getChildAt(indexPrevious);
                        for (int k = 0; k < trP.getChildCount(); k++) {
                            tvTeamp = (TextView) trP.getChildAt(k);
                            tvTeamp.setTextColor(Color.BLACK);
                        }
                    }
                    //Update indexPrevious
                    indexPrevious = scrollablePart.indexOfChild(v);
                }
            });
            scrollablePart.addView(row);
        }
    }

    public void onClose(View view){

        finish();
    }

    //region Even

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_grid_list_view_bot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //Do stuff
        finish();
        System.exit(0);
    }

    //endregion

}
