package gw.genumobile.com.views.qc_management;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;

import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import gw.genumobile.com.R;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.views.gwFragmentDialog;
import gw.genumobile.com.views.gwcore.BaseGwActive;

public class Popup_QC_Management_SlipNo_Income extends gwFragmentDialog implements View.OnClickListener {
    //region Avarible

    public static final int REQUEST_CODE = 1;  // The request code
    String pkSlipNo = "", custID = "", nameSlipNo = "", date = "", qty = "", lotNo = "", lotNoSupplier = "";
    String p_company_pk = "", p_lg_partner_type = "", p_customer = "", p_chkar_yn = "Y", p_chkap_yn = "Y";
    int indexPrevious = -1;

    int orangeColor = Color.rgb(255,99,71);
    String item_pk = "";

    private final int RESULT_COPPY = 5;
    Button btn_Choose,btn_Close;
    //endregion
    View vContent;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //type = getArguments().getString("type");
        //gwAc=getActivity();
        item_pk = getArguments().getString("ITEM_PK");
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vContent = inflater.inflate(R.layout.activity_popup__qc__management__slip_no__income, container, false);

        Toolbar toolbar = (Toolbar) vContent.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                    Intent i = new Intent()
                            .putExtra("month", "trang");
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                    dismiss();
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);
        toolbar.setTitle("SLIP NO INCOM");
        btn_Choose = (Button) vContent.findViewById(R.id.btn_Choose);
        btn_Choose.setOnClickListener(this);
        btn_Close = (Button) vContent.findViewById(R.id.btn_Close);
        btn_Close.setOnClickListener(this);
        //region Set Screen Size
     /*   int Measuredheight = 0, Measuredwidth = 0;
        Point size = new Point();
        WindowManager w = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
            w.getDefaultDisplay().getSize(size);
            Measuredheight = size.y;
            Measuredwidth = size.x;
            final LinearLayout root = (LinearLayout) vContent.findViewById(R.id.POPUP_QC_SLIPNO);
            FrameLayout.LayoutParams rootParams = (FrameLayout.LayoutParams)root.getLayoutParams();
            rootParams.height = Measuredheight*70/100;
            rootParams.width =Measuredwidth*95/100;
        }*/
        //endregion
        CreatGridHeader();
        ShowAll();
        //endregion
        return  vContent;
    }

    private void CreatGridHeader(){
        //Init varible
        TableRow.LayoutParams trParam = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        TableRow tRow = new TableRow(gwMActivity);
        TableLayout tLayout;

        //Design Grid
        tLayout = (TableLayout)vContent.findViewById(R.id.grdHeader);
        tRow = new TableRow(gwMActivity);
        tRow.setLayoutParams(trParam);
        tRow.setGravity(Gravity.CENTER);
        tRow.setBackgroundColor(bsColors[1]);
        tRow.setVerticalGravity(50);

        //Add Column
        ///// TODO: 2016-02-04 design grid

        tRow.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSeq), scrollableColumnWidths[0], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tv_date), scrollableColumnWidths[2], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSlipNo), scrollableColumnWidths[3], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemCode), scrollableColumnWidths[3], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvItemName), scrollableColumnWidths[4], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvQty), scrollableColumnWidths[2], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvLotNo), scrollableColumnWidths[2], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText(this.getResources().getString(R.string.tvSupplierLotNo), scrollableColumnWidths[2], fixedRowHeight));
        tRow.addView(makeTableColHeaderWithText("PK", 0, fixedRowHeight));
        tLayout.addView(tRow);
    }

    public void ShowAll(){

        String para = item_pk;
        String l_para = "1,LG_MPOS_GET_SLIPNO_INCOME," + para;

        try{
            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""),appPrefs.getString("company", ""),appPrefs.getString("dbName", ""),appPrefs.getString("dbUser", ""));
            String resultSearch[][] = networkThread.execute(l_para).get();

            TableLayout scrollablePart = (TableLayout) vContent.findViewById(R.id.grdQCSlipNo);
            scrollablePart.removeAllViews();

            if(resultSearch!= null && resultSearch.length > 0){
                ShowResultSearch(resultSearch, scrollablePart);
            }
        }catch(Exception ex){
            Log.e("Searh Error: ", ex.getMessage());
        }
    }

    public void ShowResultSearch(String result[][], final TableLayout scrollablePart){
        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);

        TableRow row = null;
        //remove all view child
        int count = result.length;
        for(int i = 0;i < result.length; i++){
            row = new TableRow(gwMActivity);
            row.setLayoutParams(wrapWrapTableRowParams);
            row.setGravity(Gravity.CENTER);
            row.setBackgroundColor(Color.LTGRAY);

            //seq
            row.addView(makeTableColWithText(String.valueOf(count-i), scrollableColumnWidths[0], fixedRowHeight, -1));
            //Date
            row.addView(makeTableColWithText(result[i][0], scrollableColumnWidths[2], fixedRowHeight, -1));
            //Slip No
            row.addView(makeTableColWithText(result[i][1], scrollableColumnWidths[3], fixedRowHeight, -1));
            //ITEM CODE
            row.addView(makeTableColWithText(result[i][2], scrollableColumnWidths[3], fixedRowHeight, -1));
            //ITEM NAME
            row.addView(makeTableColWithText(result[i][3], scrollableColumnWidths[4], fixedRowHeight, -1));
            //QTY
            row.addView(makeTableColWithText(result[i][4], scrollableColumnWidths[2], fixedRowHeight, -1));
            //LOT NO
            row.addView(makeTableColWithText(result[i][5], scrollableColumnWidths[2], fixedRowHeight, -1));
            //LOT NO SUPPLIER
            row.addView(makeTableColWithText(result[i][6], scrollableColumnWidths[2], fixedRowHeight, -1));
            //pk
            row.addView(makeTableColWithText(result[i][7], 0, fixedRowHeight, -1));

            row.setOnClickListener(new View.OnClickListener() {
                TextView tvTeamp,tvQty,tvLotNo,tvLotSupp,tvDate;
                TextView tvSlipNo;

                @Override
                public void onClick(View v) {
                    TableRow tr1 = (TableRow) v;
                    tvDate = (TextView) tr1.getChildAt(1);// DATE
                    tvSlipNo = (TextView) tr1.getChildAt(2);// SLIP NO
                    tvQty = (TextView) tr1.getChildAt(5);// QTY
                    tvLotNo = (TextView) tr1.getChildAt(6);// LOT NO
                    tvLotSupp = (TextView) tr1.getChildAt(7);// LOT NO SUPPLIER
                    tvTeamp = (TextView) tr1.getChildAt(8); //PK SLIP_NO
                    //set to string
                    pkSlipNo = tvTeamp.getText().toString();
                    nameSlipNo = tvSlipNo.getText().toString();
                    date = tvDate.getText().toString();
                    qty = tvQty.getText().toString();
                    lotNo = tvLotNo.getText().toString();
                    lotNoSupplier = tvLotSupp.getText().toString();

                    for (int i = 0; i < tr1.getChildCount(); i++) {
                        tvTeamp = (TextView) tr1.getChildAt(i);
                        tvTeamp.setTextColor(orangeColor);
                    }

                    if (indexPrevious >= 0 && indexPrevious != scrollablePart.indexOfChild(v)) {
                        TableRow trP = (TableRow) scrollablePart.getChildAt(indexPrevious);
                        for (int k = 0; k < trP.getChildCount(); k++) {
                            tvTeamp = (TextView) trP.getChildAt(k);
                            tvTeamp.setTextColor(Color.BLACK);
                        }
                    }
                    //Update indexPrevious
                    indexPrevious = scrollablePart.indexOfChild(v);
                }
            });
            scrollablePart.addView(row);
        }
    }

    public void onClose(View view){
        dismiss();
        gwMActivity.overridePendingTransition(R.anim.push_out_left, R.anim.push_out_left);
        //this.overridePendingTransition(R.anim.abc_slide_out_bottom, R.anim.abc_slide_in_top);
    }

    public void onChoose(View view){
        Intent intent = gwMActivity.getIntent();
        Bundle bundle = new Bundle();

        bundle.putString("PK_SLIP_NO_INCOME", pkSlipNo);
        bundle.putString("NAME_SLIP_NO", nameSlipNo);
        bundle.putString("DATE", date);
        bundle.putString("QTY_INCOME", qty);
        bundle.putString("LOT_NO", lotNo);
        bundle.putString("LOT_NO_SUPPLIER", lotNoSupplier);
        intent.putExtra("ResultSlipNo", bundle);

        if(pkSlipNo.length() > 0){
            gwMActivity.setResult(Activity.RESULT_OK, intent);
        }else{
            gwMActivity.setResult(Activity.RESULT_CANCELED, intent);
        }

        dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_Choose:
                onChoose(view);
                break;
            case R.id.btn_Close:
               onClose(view);
                break;
        }
    }
}
