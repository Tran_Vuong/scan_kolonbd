package gw.genumobile.com.views;

/**
 * Created by Ravi on 29/07/15.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import gw.genumobile.com.R;
import gw.genumobile.com.adapters.NavigationMenuAdapter;
import gw.genumobile.com.models.JDataTable;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.models.NavDrawerItem;
import gw.genumobile.com.adapters.NavigationDrawerAdapter;
import gw.genumobile.com.services.gwService;

public class FragmentDrawer extends Fragment {

    private static String TAG = FragmentDrawer.class.getSimpleName();

    private RecyclerView recyclerView;
    private TextView txfullname;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerAdapter adapter;
    private View containerView;
    private static String[] titles = null;
    private FragmentDrawerListener drawerListener;
    ImageButton imgLogout,imgInfo;
   // protected SharedPreferences appPrefs;
   protected gwMainActivity gwMAct;
    protected SharedPreferences appPrefs;
   static   List<NavigationMenuAdapter.Item> data = new ArrayList<>();
    public FragmentDrawer() {

    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    public static List<NavigationMenuAdapter.Item>  getData() {
        return  data;
        /*List<NavDrawerItem> data = new ArrayList<>();


        // preparing navigation drawer items
        for (int i = 0; i < titles.length; i++) {
            NavDrawerItem navItem = new NavDrawerItem();
            navItem.setTitle(titles[i]);
            data.add(navItem);
        }
        return data;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPrefs = getActivity().getSharedPreferences("myConfig", getActivity().MODE_PRIVATE);

        // drawer labels
        titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
        gwMAct= ((gwMainActivity)getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout

        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
        txfullname = (TextView) layout.findViewById(R.id.txt_menu);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //   recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

/*
        data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.HEADER, "INVENTORY",R.drawable.ic_box,"",""));
        data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Stock  In",R.drawable.ic_box,"",""));
        data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Stoc Out",R.drawable.ic_box,"",""));
        data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Update LOC",R.drawable.ic_box,"",""));

        data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.HEADER, "PRODUCT RESULT",R.drawable.ic_box,"",""));
        data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Line result",R.drawable.ic_box,"",""));
        data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Monitoring",R.drawable.ic_box,"",""));

        NavigationMenuAdapter.Item places = new NavigationMenuAdapter.Item(NavigationMenuAdapter.HEADER, "QC MANAGEMENT",R.drawable.ic_box,"","");
        places.invisibleChildren = new ArrayList<>();
        places.invisibleChildren.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Scan",R.drawable.ic_box,"",""));
        places.invisibleChildren.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, "Barcode",R.drawable.ic_box,"",""));
        data.add(places);*/

        String userInfo=appPrefs.getString("userInfo", "");
        String[] strArray = userInfo.split("\\|");
        String bsUserID=strArray[0];
        txfullname.setText(bsUserID);
        String l_para = "1,LG_MPOS_GET_AUTHORITY_ALL_V2,2," + bsUserID + "|1" + "";
        gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
        JResultData dtLocGroup = null;
        try {
            dtLocGroup = networkThread.execute(l_para).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        List<JDataTable> jdt = dtLocGroup.getListODataTable();
        JDataTable dtRoot = jdt.get(0);
        JDataTable dtChilf = jdt.get(1);
        data = new ArrayList<>();
        for (int i = 0; i < dtRoot.totalrows; i++) {
            String ic_form = dtRoot.records.get(i).get("icon_form").toString();
            data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.HEADER, dtRoot.records.get(i).get("icon_form").toString(),
                    R.drawable.ic_box,
                    dtRoot.records.get(i).get("icon_form").toString(),
                    dtRoot.records.get(i).get("function_id").toString(),
                    dtRoot.records.get(i).get("function_name").toString()));
            for (int j = 0; j < dtChilf.totalrows; j++) {
                if (dtChilf.records.get(j).get("parent_form").toString().equals(ic_form)) {
                    data.add(new NavigationMenuAdapter.Item(NavigationMenuAdapter.CHILD, dtChilf.records.get(j).get("icon_form").toString(),
                            R.drawable.ic_box,
                            dtChilf.records.get(j).get("icon_form").toString(),
                            dtChilf.records.get(j).get("function_id").toString(),
                            dtChilf.records.get(j).get("function_name").toString()));


                }
            }
        }
        recyclerView.setAdapter(new NavigationMenuAdapter(data));



    /*  recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
        adapter = new NavigationDrawerAdapter(getActivity(), getData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
      */
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                drawerListener.onDrawerItemSelected(view, position);
                if (data.get(position).type == 1)
                    mDrawerLayout.closeDrawer(containerView);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        imgLogout = (ImageButton) layout.findViewById(R.id.imgLogout);
        imgInfo = (ImageButton) layout.findViewById(R.id.imgInfo);
        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(gwMAct)
                        .setTitle("Logout")
                        .setMessage("Do you really want to Logout system?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                gwMAct.LogOut();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();

            }
        });
        // recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
        return layout;
    }
        public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
               getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
              //  getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }
}
