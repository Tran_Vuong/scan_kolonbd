package gw.genumobile.com.views.inventory;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

import gw.genumobile.com.R;
import gw.genumobile.com.models.JResultData;
import gw.genumobile.com.models.gwfunction;
import gw.genumobile.com.services.ConnectThread;
import gw.genumobile.com.services.gwService;
import gw.genumobile.com.utils.CDate;
import gw.genumobile.com.views.gwFragmentDialog;
import gw.genumobile.com.views.productresult.EditCapture;


public class MappingV2_Pop21 extends gwFragmentDialog implements View.OnClickListener {

    static TextView tvStatus;
    private static final int ACTION_TAKE_PHOTO_B = 1;
    private static final int SIGNATURE_ACTIVITY = 2;
    public int REQUEST_CODE = 1;
    public int RESULT_CODE = 1;

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    protected int fixedRowHeight = 50; //70|30
    protected int fixedHeaderHeight = 60; //80|40
    protected int[] scrollableColumnWidths = new int[]{17, 11,14, 23, 10, 10, 10, 0};
    SQLiteDatabase db = null;
    Queue queue = new LinkedList();

    int indexPrevious=-1;
    String line_detail[] = new String[0];
    View vContent;
    Activity gwAc;
    HashMap hashMapGrade= new HashMap();
    String[][] ls_grade = new String[][]{
            {"A", "A"},
            {"B", "B"},
            {"C", "C"}};
    public Spinner spn_sewing;
    String grade_item="",pk;
    TextView _txtTotalGridBot;
    EditText edt_item_code,edt_item_name,edt_bc,edt_weight,edt_grosswt;
    Button btn_edit;
    ArrayAdapter<String> dataAdapter;
    TableLayout scrollablePart;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gwAc=getActivity();
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vContent = inflater.inflate(R.layout.activity_mapping_edit, container, false);
        Toolbar toolbar = (Toolbar) vContent.findViewById(R.id.pop_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_close) {
                   OnComeBackResult(null);
                }

                return true;
            }
        });
        toolbar.inflateMenu(R.menu.menu_grid_list_view_mid);
        toolbar.setTitle("EDIT");

        btn_edit = (Button) vContent.findViewById(R.id.btn_edit);
        btn_edit.setOnClickListener(this);
        spn_sewing = (Spinner) vContent.findViewById(R.id.spn_sewing);
        edt_item_code =(EditText) vContent.findViewById(R.id.edt_item_code);
        edt_item_name =(EditText) vContent.findViewById(R.id.edt_item_name);
        edt_bc =(EditText) vContent.findViewById(R.id.edt_bc);
        edt_weight =(EditText) vContent.findViewById(R.id.edt_weight);
        edt_grosswt=(EditText) vContent.findViewById(R.id.edt_gross_weight);


        LoadGrade(ls_grade);
        String[] arr_para = getArguments().getStringArray("LINE_DETAIL");
        edt_bc.setText(arr_para[0]);
        edt_item_code.setText(arr_para[2]);
        edt_item_name.setText(arr_para[3]);
        if (arr_para[5] != null) {
            int spinnerPosition = dataAdapter.getPosition(arr_para[5]);
            spn_sewing.setSelection(spinnerPosition);
        }
        edt_weight.setText(arr_para[6]);
        pk=arr_para[7];
        return vContent;
//        if (shouldAskPermission()) {
//            ActivityCompat.requestPermissions(gwMActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
//        }
    }

    private void LoadGrade(String[][] _data) {
        try {
            String lg_code="LGPC0071";
            String l_para = "1,LG_MPOS_GET_LG_CODE," + lg_code;

            ConnectThread networkThread = new ConnectThread(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
           _data = networkThread.execute(l_para).get();
            List<String> lstGroupName = new ArrayList<String>();
            //
            for (int i = 0; i < _data.length; i++) {
                lstGroupName.add(_data[i][1]);
                hashMapGrade.put(i, _data[i][0]);
            }

            if (_data != null && _data.length > 0) {
                dataAdapter = new ArrayAdapter<String>(gwMActivity,
                        android.R.layout.simple_spinner_item, lstGroupName);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spn_sewing.setAdapter(dataAdapter);
                //  wh_name = myLstWH.getItemAtPosition(0).toString();
            } else {
                spn_sewing.setAdapter(null);
                grade_item = "0";
            }
        } catch (Exception ex) {
            //  alertToastLong(ex.getMessage().toString());
            Log.e("WH Error: ", ex.getMessage());
        }
        //-----------

        // onchange spinner wh
        spn_sewing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                // your code here
                // wh_name = myLstWH.getItemAtPosition(i).toString();
                // Log.e("WH_name",wh_name);
                Object pkGroup = hashMapGrade.get(i);
                if (pkGroup != null) {
                    grade_item = String.valueOf(pkGroup);
                    if (!grade_item.equals("0")) {

                        //dosomehitng

                    }
                } else {
                    grade_item = "0";
                    Log.e("grade_item: ", grade_item);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }private  void onSave(View v){
        try {
            String l_para =String.format("2,%s,%s|%s|%s|%s|%s", "LG_MPOS_UPD_MAPP",pk,
                    edt_weight.getText().toString(), edt_grosswt.getText().toString(),grade_item,bsUserID);
            gwService networkThread = new gwService(appPrefs.getString("server", ""), appPrefs.getString("company", ""), appPrefs.getString("dbName", ""), appPrefs.getString("dbUser", ""));
            JResultData dtLocGroup = networkThread.execute(l_para).get();
           if(dtLocGroup.getListODataTable().get(0).records.get(0).get("status").equals("OK"))
           {
               OnComeBackResult(null);
           }
           else {
               gwMActivity.alertToastLong("BC DA VO KHO!!!");

           }
        } catch (Exception ex) {
            Log.e("Update bc: ", ex.toString());
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_edit:
                onSave(v);
                break;

            default:
                break;
        }

    }

    public void OnComeBackResult(View v) {
      /*  Intent i = new Intent();
        i.putExtra("line", LINE_DETAIL);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);*/
        dismiss();
       MappingV2_Pop2 fr =(MappingV2_Pop2) getTargetFragment();
       fr.onSeach(null);
    }
    @Override
    public void onStart()
    {
        super.onStart();

    }
    @Override
    public void onResume() {
        super.onResume();
        Point size = new Point();
        WindowManager w =  gwMActivity.getWindowManager();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
        w.getDefaultDisplay().getSize(size);
        Measuredheight = size.y;
            getDialog().getWindow().setLayout(size.x,  Measuredheight*95/100);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTION_TAKE_PHOTO_B && resultCode == Activity.RESULT_OK) {

            Intent intent = new Intent(gwMActivity, EditCapture.class);
         //   intent.putExtra("URLPhotoPath", mCurrentPhotoPath);
            startActivityForResult(intent, SIGNATURE_ACTIVITY);

            //region ACTION_TAKE_PHOTO_B
            //setPic();
            //galleryAddPic();
            //mCurrentPhotoPath = null;
            //mImageView = new ImageView(gwMActivity);
            //mImageView.setImageBitmap(imageBitmap);
            //endregion

        }
    }

}
