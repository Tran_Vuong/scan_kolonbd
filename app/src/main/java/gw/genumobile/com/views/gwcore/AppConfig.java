package gw.genumobile.com.views.gwcore;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

import gw.genumobile.com.R;

public class AppConfig extends PreferenceActivity  {
    String formID = "",user="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_app_config);
        formID = getIntent().getStringExtra("type");
        user = getIntent().getStringExtra("user");
        if(formID.equals("10")){
            PreferenceManager prefMgr = getPreferenceManager();
            prefMgr.setSharedPreferencesName("MappingConfig");
            //---load the preferences from an XML file---
            addPreferencesFromResource(R.xml.mappingconfig);
        }else{
            PreferenceManager prefMgr = getPreferenceManager();
            prefMgr.setSharedPreferencesName("myConfig");
            //---load the preferences from an XML file---
            addPreferencesFromResource(R.xml.myconfig);
        }
        /* //setPreferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("silentMode", mSilentMode);

        // Commit the edits!
        editor.commit();
        */
    }
}
