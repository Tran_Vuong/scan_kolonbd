/**
 * Automatically generated file. DO NOT MODIFY
 */
package gw.genumobile.com;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "gw.genumobile.com";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 53;
  public static final String VERSION_NAME = "Genuwin_KolonBD_v53";
}
